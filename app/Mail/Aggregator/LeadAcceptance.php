<?php

namespace App\Mail\Aggregator;

use App\Entities\Patient;
use App\Entities\Lead;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class LeadAcceptance extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The Careplan instance.
     *
     * @var Careplan
     */
    public $patient;
    public $lead;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Patient $patient,Lead $lead)
    {
        $this->patient = $patient;
        $this->lead = $lead;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@smarthealthconnect.com' , 'Greetings From Smart Health Connect')
                    ->view('mails.cases.aggregator.accepted',compact($this->patient,$this->lead))
                    ->subject('Lead Acceptance Confirmation');
    }
}
