<?php

namespace App\Mail\Aggregator;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DailyStats extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The Data instance.
     *
     * @var Data variables
     */
    public $data;
    public $fromDate;
    public $toDate;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data,$fromDate,$toDate)
    {
      $this->data = $data;
      $this->fromDate = date("d-m-Y",strtotime($fromDate));
      $this->toDate = date("d-m-Y",strtotime($toDate));
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@smarthealthconnect.com' , 'Lead Status Report'.' for '.$this->fromDate.' to '.$this->toDate)
                    ->view('mails.cases.aggregator.dailyStats',compact($this->data))
                    ->subject('Lead Stats Report'.' for '.$this->fromDate.' to '.$this->toDate);
    }
}
