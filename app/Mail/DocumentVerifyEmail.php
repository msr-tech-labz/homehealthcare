<?php

namespace App\Mail;

use App\Entities\Caregiver\Caregiver;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DocumentVerifyEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The Caregiver instance.
     *
     * @var Caregiver
     */
    public $caregiver;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Caregiver $caregiver)
    {
        $this->caregiver = $caregiver;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $upload_path = public_path("uploads/provider/".session('tenant_id')."/caregivers/".\Helper::encryptor('encrypt',$this->caregiver->id)."/document");

        $filesInFolder = \File::files($upload_path);

        $zipname = public_path("uploads/provider/".session('tenant_id')."/caregivers/".\Helper::encryptor('encrypt',$this->caregiver->id)."/documents.zip");
        $zip = new \ZipArchive;
        if (file_exists($zipname)) {
            $zip->open($zipname, \ZipArchive::OVERWRITE);
        } else {
            $zip->open($zipname, \ZipArchive::CREATE);
        }

        foreach ($filesInFolder as $index => $file) {
            $new_filename = substr($file,strrpos($file,'/') + 1);
            $zip->addFile($file,$new_filename);
        }
        $zip->close();

        return $this->from('no-reply@smarthealthconnect.com' , 'Greetings From '.session('organization_name'))
                    ->view('mails.agency.verifyDocuments',compact($this->caregiver))
                    ->attach($zipname)
                    ->subject('Documents For Employee Verification of '.session('organization_name'));
    }
}
