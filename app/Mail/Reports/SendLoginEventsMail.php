<?php

namespace App\Mail\Reports;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendLoginEventsMail extends Mailable
{
    use Queueable;
    public $data;
    public $pdfContent;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data,$pdfOutput)
    {
        $this->mailData = $data;
        $this->pdfContent = $pdfOutput;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.logineventsmail',(array)$this->data)
                    ->from('no-reply@smarthealthconnect.com','Smart Health Connect')
                    ->to('uppala61937@gmail.com')
                    ->subject('Users Login')
                    ->attachData($this->pdfContent, 'events.pdf');
    }
}
