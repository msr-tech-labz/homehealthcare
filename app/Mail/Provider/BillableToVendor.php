<?php

namespace App\Mail\Provider;

use App\Entities\Masters\BillingProfile;
use App\Entities\CaseBillables;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BillableToVendor extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The Billable instance.
     *
     * @var Billable
     */
    public $vendor;
    public $billables;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(BillingProfile $vendor,$billables)
    {
        $this->vendor = $vendor;
        $this->billables = $billables;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@smarthealthconnect.com' , session('organization_name'))
                    ->view('mails.cases.provider.billable-to-vendor',compact($this->vendor,$this->billables))
                    ->subject('Case Consumables - Requirement');
    }
}
