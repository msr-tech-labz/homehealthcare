<?php

namespace App\Mail\Provider;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Entities\PatientMailers;

class SendActiveUsersMail extends Mailable
{
    use Queueable;

    public $data;
    public $pdfContent;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $pdfOutput)
    {
        $this->data = $data;
        $this->pdfContent = $pdfOutput;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.activeusers',(array)$this->data)
                    ->from('no-reply@smarthealthconnect.com','Smart Health Connect')
                    
                    ->to('uppala61937@gmail.com')
                    ->cc('kodavaliyasaswi1@gmail.com')
                    ->subject($this->data['subscriber']->org_code.' Active Users')
                    ->attachData($this->pdfContent, $this->data['subscriber']->org_code.' '.$this->data['yesterday']->format('M').'-'.$this->data['yesterday']->format('Y').' Active Users'.'.pdf');
    }
}
