<?php

namespace App\Mail\Provider;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Entities\PatientMailers;

class SendInvoice extends Mailable
{
    use Queueable;

    public $data;
    public $pdfContent;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $pdfOutput)
    {
        $this->data = $data;
        $this->pdfContent = $pdfOutput;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mailers = PatientMailers::wherePatientId($this->data['invoice']->patient->id)->whereStatus('active')->get();
        if(count($mailers)){
            foreach ($mailers as $mailer) {
                if(filter_var($mailer->email,FILTER_VALIDATE_EMAIL)){
                    $this->setAddress($mailer->email,$mailer->name,'cc');
                }
            }
        }

        return $this->view('mails.invoice',$this->data)
                    ->from('no-reply@smarthealthconnect.com', $this->data['profile']->organization_name)
                    ->to($this->data['invoice']->patient->email)
                    ->bcc($this->data['profile']->email)
                    ->subject('Invoice - '.$this->data['inv_prefix'].$this->data['invoice']->invoice_no.' - '.$this->data['invoice']->patient->full_name.$this->data['invoiceRange'])
                    ->attachData($this->pdfContent, "Invoice-".$this->data['inv_prefix'].$this->data['invoice']->invoice_no.".pdf");
    }
}
