<?php

namespace App\Mail\Provider;

use App\Entities\Profile;
use App\Entities\Console\SubscriptionInvoicePayments;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubscriptionInvoicePayment extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The SubscriptionInvoicePayments instance.
     *
     * @var SubscriptionInvoicePayments
     */
    public $profile;
    public $sip;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Profile $profile,SubscriptionInvoicePayments $sip)
    {
        $this->profile = $profile;
        $this->sip = $sip;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subscribers = \Helper::getSubscribersByType('Subscription Payment');
        if(count($subscribers)){
            foreach ($subscribers as $subscriber) {
                //$this->addAddresses($subscriber->email, $subscriber->name, 'Cc');
            }
        }
        return $this->from('no-reply@smarthealthconnect.com' , 'Greetings From Smart Health Connect')
                    ->view('mails.subscription.apnafocusInvoice',compact($this->profile,$this->sip))
                    ->subject('SMART HEALTH CONNECT RECEIPT FOR - '.date("F Y", strtotime($this->sip->invoiceGenerated->created_at)));
    }
}
