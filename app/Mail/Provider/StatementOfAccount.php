<?php

namespace App\Mail\Provider;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Contracts\Queue\ShouldQueue;

class StatementOfAccount extends Mailable
{
    use Queueable;

    public $data;
    public $pdfContent;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $pdfOutput)
    {
        $this->data = $data;
        $this->pdfContent = $pdfOutput;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subscribers = \Helper::getSubscribersByType('Statement Of Account');
        if(count($subscribers)){
            foreach ($subscribers as $subscriber) {
                //$this->addAddresses($subscriber->email, $subscriber->name, 'Cc');
            }
        }

        return $this->view('mails.soa',$this->data)
                    ->from('no-reply@smarthealthconnect.com', $this->data['profile']->organization_name)
                    ->to($this->data['patient']->email)
                    ->subject('Statement of Account - '.$this->data['patient']->patient_id.' - '.$this->data['patient']->full_name)
                    ->attachData($this->pdfContent, "Statement-Of-Account.pdf");
    }
}
