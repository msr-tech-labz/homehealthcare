<?php

namespace App\Mail\Provider;

use App\Helpers\Helper;
use App\Entities\Lead;
use App\Entities\Caregiver\Caregiver;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class StaffAssign extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The Careplan instance.
     *
     * @var Careplan
     */
    public $lead;
    public $caregiver;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Lead $lead,Caregiver $caregiver)
    {
        $this->lead = $lead;
        $this->caregiver = $caregiver;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subscribers = Helper::getSubscribersByType('Case Conversion');
        if(count($subscribers)){
            foreach ($subscribers as $subscriber) {
                $this->addAddresses($subscriber->email, $subscriber->name, 'cc');
            }
        }
        $verifications = $this->caregiver->verifications;
	    $message = $this->from('no-reply@smarthealthconnect.com' , 'Greetings From '.session('organization_name'))
            ->view('mails.cases.provider.staff-assigned',compact($this->lead,$this->caregiver))
            ->subject('Case Acceptance Confirmation');
	    foreach ($verifications as $row) { 
	        $message = $message->attach(public_path('uploads/provider/'.session('tenant_id').'/caregivers/'.Helper::encryptor('encrypt',$this->caregiver->id).'/document/'.$row->doc_path));
	    }
	    return $message;
    }
}
