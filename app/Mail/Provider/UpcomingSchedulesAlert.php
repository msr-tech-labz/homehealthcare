<?php

namespace App\Mail\Provider;

use Illuminate\Mail\Mailable;
use Illuminate\Http\Request;

class UpcomingSchedulesAlert extends Mailable
{
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // $subscribers = \Helper::getSubscribersByType('Upcoming Schedules');
        // if(count($subscribers)){
        //     foreach ($subscribers as $subscriber) {
        //         $this->addAddresses($subscriber->email, $subscriber->name, 'Cc');
        //     }
        // }

        return $this->from('no-reply@smarthealthconnect.com' , 'Smart Health Connect')
                    ->view('mails.cases.provider.upcoming-schedules',$this->data)
                    ->subject('Field Staff Details - '.date('d-m-Y'));
    }
}
