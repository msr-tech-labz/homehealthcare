<?php

namespace App\Mail\Provider;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendServiceOrderAlert extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subscribers = \Helper::getSubscribersByType('Service Order Expiry');
        if(count($subscribers)){
            foreach ($subscribers as $subscriber) {
                //$this->addAddresses($subscriber->email, $subscriber->name, 'Cc');
            }
        }

        return $this->from('no-reply@smarthealthconnect.com' , 'Service Expiry Alert')
                    ->view('mails.serviceorders.serviceOrdersAlert',$this->data)
                    ->subject('Service Expiry Alert');
    }
}
