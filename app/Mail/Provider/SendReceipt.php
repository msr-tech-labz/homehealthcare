<?php

namespace App\Mail\Provider;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendReceipt extends Mailable
{
    use Queueable;

    public $receipt;
    public $profile;
    public $pdfContent;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($receipt, $profile, $pdfOutput)
    {
        $this->receipt = $receipt;
        $this->profile = $profile;
        $this->pdfContent = $pdfOutput;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.cases.provider.receipt',(array)$this->receipt)
                    ->from('no-reply@smarthealthconnect.com', $this->profile->organization_name)
                    ->to($this->receipt->patient->email)
                    ->bcc($this->profile->email)
                    ->subject('Receipt - '.$this->receipt->receipt_no.' - '.$this->receipt->patient->full_name)
                    ->attachData($this->pdfContent, "Receipt-".$this->receipt->receipt_no.".pdf");
    }
}
