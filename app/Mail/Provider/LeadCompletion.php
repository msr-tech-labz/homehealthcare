<?php

namespace App\Mail\Provider;

use App\Entities\Lead;
use App\Entities\Patient;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class LeadCompletion extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The Careplan instance.
     *
     * @var Lead
     */
    public $lead;
    public $patient;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Lead $lead,Patient $patient)
    {
        $this->lead = $lead;
        $this->patient = $patient;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subscribers = \Helper::getSubscribersByType('Case Completion');
        if(count($subscribers)){
            foreach ($subscribers as $subscriber) {
                //$this->addAddresses($subscriber->email, $subscriber->name, 'Cc');
            }
        }
        return $this->from('no-reply@smarthealthconnect.com' , 'Greetings From '.session('organization_name'))
                    ->view('mails.cases.provider.completed',compact($this->lead,$this->patient))
                    ->subject('Thank you for Choosing '.session('organization_name'));
    }
}
