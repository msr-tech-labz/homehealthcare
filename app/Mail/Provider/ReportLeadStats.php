<?php

namespace App\Mail\Provider;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReportLeadStats extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The Data instance.
     *
     * @var Data variables
     */
    public $data;
    public $type;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data,$type)
    {
      $this->data = $data;
      $this->type = $type;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->type == 'Daily'){
            $subscribers = \Helper::getSubscribersByType('Daily Stats');
            if(count($subscribers)){
                foreach ($subscribers as $subscriber) {
                    //$this->addAddresses($subscriber->email, $subscriber->name, 'Cc');
                }
            }
            return $this->from('no-reply@smarthealthconnect.com' , 'Daily Leads Report by Smart Health Connect'.' for '.date("d-m-Y"))
                        ->view('reports.activity.overall-daily-summary',compact($this->data))
                        ->subject('Daily Leads Report by Smart Health Connect'.' for '.date("d-m-Y"));
        }else if($this->type == 'Weekly'){
            $subscribers = \Helper::getSubscribersByType('Weekly Stats');
            if(count($subscribers)){
                foreach ($subscribers as $subscriber) {
                    //$this->addAddresses($subscriber->email, $subscriber->name, 'Cc');
                }
            }
            return $this->from('no-reply@smarthealthconnect.com' , 'Weekly Leads Report by Smart Health Connect')
                        ->view('reports.activity.overall-weekly-summary',compact($this->data))
                        ->subject('Weekly Leads Report by Smart Health Connect');
        }else{
            $subscribers = \Helper::getSubscribersByType('Monthly Stats');
            if(count($subscribers)){
                foreach ($subscribers as $subscriber) {
                    //$this->addAddresses($subscriber->email, $subscriber->name, 'Cc');
                }
            }
            return $this->from('no-reply@smarthealthconnect.com' , 'Monthly Leads Report by Smart Health Connect'.' for '.date("M"))
                        ->view('reports.activity.overall-monthly-summary',compact($this->data))
                        ->subject('Monthly Leads Report by Smart Health Connect'.' for '.date("M"));
        }
    }
}
