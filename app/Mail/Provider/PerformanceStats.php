<?php

namespace App\Mail\Provider;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PerformanceStats extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The Data instance.
     *
     * @var Data variables
     */
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
      $this->data = $data;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@smarthealthconnect.com' , 'Performance Report'.' for '.date("d-m-Y"))
                    ->view('reports.activity.overall-summary',compact($this->data))
                    ->subject('Performance Report'.' for '.date("d-m-Y"));
    }
}
