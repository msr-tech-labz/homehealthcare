<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Entities\PatientMailers;

class TestMail extends Mailable
{
    use Queueable;

    public $pdfContent;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($pdfOutput)
    {
        $this->pdfContent = $pdfOutput;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        return $this->view('testt')
                    ->from('no-reply@smarthealthconnect.com', 'Satish')
                    ->to('uppala61937@gmail.com')
                    ->cc('shid@smarthealthglobal.in')
                    ->subject('test')
                    ->attachData($this->pdfContent, "test".".pdf");
    }
}
