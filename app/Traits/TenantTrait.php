<?php
namespace App\Traits;

use App\Scopes\TenantScope;

trait TenantTrait
{
    private static $tenantID;
    private static $userType;

    /**
     * Boot the soft deleting trait for a model.
     *
     * @return void
     */
    public static function bootTenantTrait()
    {
        self::$tenantID = \Helper::getTenantID();
        self::$userType = \Helper::encryptor('decrypt',session('utype'));

        // Check for api calls
        // if(\Request::header('Accept') != 'application/x.apnacare.v1+json'){
        //     static::addGlobalScope(new TenantScope);
        // }

        static::creating(function($model){
            if(self::$userType != 'Aggregator' && \Request::header('Accept') != 'application/x.apnacare.v1+json' && self::$tenantID != 0){
                self::setTenantId($model);
            }
        });

        static::updating(function($model){
            if(self::$userType != 'Aggregator' && \Request::header('Accept') != 'application/x.apnacare.v1+json' && self::$tenantID != 0){
                $model->tenant_id = self::$tenantID;
            }
        });

        static::saving(function($model){
            if(self::$userType != 'Aggregator' && \Request::header('Accept') != 'application/x.apnacare.v1+json' && self::$tenantID != 0){
                $model->tenant_id = self::$tenantID;
            }
        });

        static::deleting(function($model){
            if(self::$tenantID != 0){
                $model->tenant_id = self::$tenantID;
                $model->forceDeleting = false;
            }
        });
    }

    protected static function setTenantId($model)
    {
        if(self::$tenantID != 0){
            $model->setRawAttributes(array_merge($model->attributes, ['tenant_id' => self::$tenantID]), true);
        }
    }

}
