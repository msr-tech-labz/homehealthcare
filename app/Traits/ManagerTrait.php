<?php
namespace App\Traits;

use App\Scopes\ManagerScope;
use App\Entities\Masters\ManagementComposition;

trait ManagerTrait
{
    private static $userRoleManager;

    /**
     * Boot the soft deleting trait for a model.
     *
     * @return void
     */
    public static function bootManagerTrait()
    {
        if(\Helper::encryptor('decrypt',session('admin_id')) != 0){
            $cadre = explode(',',ManagementComposition::whereType('leads_viewable_by')->value('composition_cadre'));
            $staffCadre = \Helper::getStaffCadre(\Helper::encryptor('decrypt',session('admin_id')));
            $staffDeployable = \Helper::getStaffDeployable(\Helper::encryptor('decrypt',session('admin_id')));

            self::$userRoleManager = \Helper::getStaffRole(\Helper::encryptor('decrypt',session('admin_id')));

            if(self::$userRoleManager == 'Manager' && !in_array($staffCadre,$cadre)){
                static::addGlobalScope(new ManagerScope);
            }

            if(self::$userRoleManager == 'Manager' && in_array($staffCadre,$cadre) && $staffDeployable == 'Yes'){
                static::addGlobalScope(new ManagerScope);
            }
        }
    }
}
