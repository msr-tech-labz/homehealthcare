<?php
namespace App\Traits;

use App\Scopes\BranchScope;

trait BranchTrait
{
    private static $branchID;
    private static $uType;

    /**
     * Boot the soft deleting trait for a model.
     *
     * @return void
     */
    public static function bootBranchTrait()
    {
        self::$branchID = \Helper::encryptor('decrypt',session('branch_id'));
        self::$uType = \Helper::encryptor('decrypt',session('utype'));

        // Check for api calls
        if(\Request::header('Accept') != 'application/x.apnacare.v1+json'){
            static::addGlobalScope(new BranchScope);
        }

        static::creating(function($model){
            if(self::$uType != 'Admin' && self::$branchID > 0 && \Request::header('Accept') != 'application/x.apnacare.v1+json'){
                self::setBranchId($model);
            }
        });

        static::updating(function($model){
            if(self::$uType != 'Admin' && self::$branchID > 0 && \Request::header('Accept') != 'application/x.apnacare.v1+json'){
                $model->branch_id = self::$branchID;
            }
        });

        static::saving(function($model){
            if(self::$uType != 'Admin' && self::$branchID > 0 && \Request::header('Accept') != 'application/x.apnacare.v1+json'){
                $model->branch_id = self::$branchID;
            }
        });

        static::deleting(function($model){
            $model->branch_id = self::$branchID;
            $model->forceDeleting = false;
        });
    }

    protected static function setBranchId($model)
    {
        $model->setRawAttributes(array_merge($model->attributes, ['branch_id' => self::$branchID]), true);
    }

}
