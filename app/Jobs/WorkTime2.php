<?php

namespace App\Jobs;

use App\Entities\User;
use App\Entities\Caregiver\Caregiver;
use App\Entities\Attendance;
use App\Entities\Leaves;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class WorkTime2 implements FromCollection, WithHeadings
{
    use Exportable;

    public function collection()
    {
        $filter_month = '03';
        $filter_year = '2019';
        $data3 = collect();
        $caregivers = Caregiver::get();
        $employeeWorkTime = collect();
        foreach ($caregivers as $key => $caregiver) {
            $data2 = collect();
            for($i=1;$i<=31;$i++) {
                if ($i < 10) {
                    $i = str_pad($i, 2, "0", STR_PAD_LEFT);
                }
                $data2['day_'.$i] = '-';
                $userAttendance = Attendance::whereCaregiverId($caregiver->id)->where('date',date($filter_year.'-'.$filter_month.'-'.$i))->get();
                if(count($userAttendance)){
                    $multipleShifts = '';
                    foreach ($userAttendance as $index => $value) {
                        if($value['schedule_id'] != null){
                            if(isset($value['punch_in']) && isset($value['punch_out'])){
                                $from = strtotime($value['punch_in']);
                                $to = strtotime($value['punch_out']);

                                $init = ($to - $from);
                                $hours = floor($init / 3600);
                                $minutes = floor(($init / 60) % 60);

                                if($init == 0 ){
                                    $hrs = 24;
                                }else if($init < 0 ){
                                    if($minutes == 0){
                                        $hrs = 24 - abs($hours);
                                    }else{
                                        $hrs = (24 - abs($hours)).':'.(60 - abs($minutes));
                                    }
                                }else{
                                    if($minutes == 0){
                                        $hrs = $hours;
                                    }else{
                                        $hrs = $hours.':'.$minutes;
                                    }
                                }

                                if($hrs < 15){
                                    if($from <= strtotime('12:01:00') && $to >= strtotime('12:01:00')){
                                        $shift = 'd';
                                    }else{
                                        $shift = 'n';
                                    }
                                }else{
                                    $shift = '';
                                }
                            }else{
                                $hrs = '-';
                                $shift = '';
                            }
                            if($index!=0) $comaSeperate = ',';
                            else $comaSeperate = '';
                            $multipleShifts = $multipleShifts.$comaSeperate.(string) $hrs.$shift;
                            $data2['day_'.$i] = $multipleShifts;
                        }else{
                            $data2['day_'.$i] = $value['status'];
                        }                            
                    }
                }else{
                    $userLeave = Leaves::whereCaregiverId($caregiver->id)->where('date',date($filter_year.'-'.$filter_month.'-'.$i))->whereStatus('Approved')->pluck('status')->first();
                    if($userLeave == 'Approved') $data2['day_'.$i] = 'L';
                }
            }
            if(count($data2)){
                $data3['name']  = $caregiver->full_name.' - '.$caregiver->employee_id;
                $employeeWorkTime->push(array_merge(end($data3),end($data2)));
            }
        }
        return $employeeWorkTime;
    }

    public function headings(): array
    {
        return [
            'Name',
            '01',
            '02',
            '03',
            '04',
            '05',
            '06',
            '07',
            '08',
            '09',
            '10',
            '11',
            '12',
            '13',
            '14',
            '15',
            '16',
            '17',
            '18',
            '19',
            '20',
            '21',
            '22',
            '23',
            '24',
            '25',
            '26',
            '27',
            '28',
            '29',
            '30',
            '31',
        ];
    }
}