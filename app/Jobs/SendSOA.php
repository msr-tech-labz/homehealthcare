<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Entities\Profile;
use App\Entities\Patient;
use App\Entities\Settings;
use App\Entities\Schedule;
use App\Entities\SOAMailers;
use App\Entities\CreditMemo;
use App\Entities\CasePayment;
use App\Entities\Receipt;
use App\Helpers\Report\Financial;

use App\Mail\Provider\StatementOfAccount;

class SendSOA implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;    

    public $patient;
    public $fromDate;
    public $toDate;
    public $dbName;
    public $tenantID;
    public $branchID;

    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Patient $patient, $fromDate, $toDate, $tenantID, $branchID)
    {
        $this->patient = $patient;
        $this->fromDate = $fromDate;
        $this->toDate = $toDate;
        $this->tenantID = $tenantID;
        $this->branchID = $branchID;
        $this->dbName = \Helper::encryptor('decrypt',session('dbName'));

        if(!empty($this->dbName)){
            \Config::set('database.connections.tenant.database', $this->dbName);
            \DB::setDefaultConnection('tenant');
            \DB::reconnect();
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if(filter_var($this->patient->email, FILTER_VALIDATE_EMAIL)){
            \Config::set('database.connections.tenant.database', $this->dbName);
            \DB::setDefaultConnection('tenant');
            \DB::reconnect();

            // Create a soa mailer entry
            SOAMailers::create([
                'job_id' => $this->job->getJobId(),
                'patient_id' => $this->patient->id,
                'from_date' => $this->fromDate,
                'to_date' => $this->toDate,
                'payload' => $this->job->getRawBody(),
                'status' => 'Pending',
                'user_id' => \Helper::getUserID()
            ]);

            // Process the statement of account
            $schedules = Schedule::with(['serviceRequest','service','ratecard'])->wherePatientId($this->patient->id)->whereStatus('Completed');

            $payments = CasePayment::select('total_amount')->wherePatientId($this->patient->id)->whereStatus('Success');

            $receipts = Receipt::wherePatientId($this->patient->id)->whereReceiptType('Memo');

            if(!empty($this->fromDate) && !empty($this->toDate)){
                $fDate = date_format(new \DateTime($this->fromDate),'Y-m-d');
                $eDate = date_format(new \DateTime($this->toDate),'Y-m-d');
                $data['from_period'] = $fDate;
                $data['to_period'] = $eDate;

                $schedules->whereRaw(\DB::raw('DATE(schedule_date) BETWEEN "'.$fDate.'" AND "'.$eDate.'"'));

                $payments->whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fDate.'" AND "'.$eDate.'"'));
            }
            $temp = clone $schedules;

            //$tempPayment = clone $payments;
            $data['summary'] = \Helper::getSchedulesByMonth($temp->get());
            // http_response_code(500);
            $data['current_sum'] = collect($data['summary'])->flatten(1)->sum('amount');

            $data['schedules'] = $schedules->orderBy('schedule_date','Asc')->get();
            $data['payments'] = $payments->orderBy('created_at','Asc')->get();
            $data['receipts'] = $receipts->orderBy('created_at','Asc')->get();

            $data['patient'] = Patient::select('id','patient_id','first_name','last_name','enquirer_name','contact_number','alternate_number','email','city','area')->whereId($this->patient->id)->with(['creditMemo' => function($q) {
                                            $q->addSelect('date','amount');
                                        }])->first();

            $data['credit_memos'] = CreditMemo::wherePatientId($this->patient->id)->whereType('Others')->get();

            $data['profile'] = Profile::select('tenant_id','organization_name','organization_logo','organization_address','organization_area','organization_city','phone_number','website')->first();
            $data['settings'] = Settings::first();

            $data['patient_id'] = $this->patient->id;

            $data['fromDate'] = $this->fromDate;
            $data['toDate'] = $this->toDate;
            $data['tenant_id'] = $this->tenantID;
            $data['branch_id'] = $this->branchID;

            $pdf = \PDF::loadView('reports.financial.soa-pdf', $data);


            \Mail::send(new StatementOfAccount($data, $pdf->output()));

            // Update soa mailer status
            $soaMailer = SOAMailers::whereJobId($this->job->getJobId())->first();
            if($soaMailer){
                $soaMailer->update(['status' => 'Processed']);
            }

            unset($payments);
            unset($schedules);
            unset($data);
        }
    }

    public function failed()
    {
        if($this->job){
            $soaMailer = SOAMailers::whereJobId($this->job->getJobId())->first();
            if($soaMailer){
                $soaMailer->update(['status' => 'Failed']);
            }
        }
    }
}
