<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Entities\WorkTimeMailers;
use Exception;

class SendWorkTimeMail implements ShouldQueue
{
    use InteractsWithQueue, Queueable, Dispatchable;

    public $request;
    public $tenantID;
    public $dbName;
    public $tries = 1;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request,$tenantID)
    {
        $this->request = $request;
        $this->tenantID = $tenantID;
        $this->dbName = \Helper::encryptor('decrypt',session('dbName'));

        if(!empty($this->dbName)){
            \Config::set('database.connections.tenant.database', $this->dbName);
            \DB::setDefaultConnection('tenant');
            \DB::reconnect();
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            if(filter_var($this->request['email'], FILTER_VALIDATE_EMAIL)){
                \Config::set('database.connections.tenant.database', $this->dbName);
                \DB::setDefaultConnection('tenant');
                \DB::reconnect();

                // Create a soa mailer entry
                WorkTimeMailers::create([
                    'job_id' => $this->job->getJobId(),
                    'payload' => $this->job->getRawBody(),
                    'status' => 'Pending',
                ]);

                $counter = 0;
                $data = array();
                for($i=1;$i<=31;$i++) {
                    if ($i < 10) {
                        $i = str_pad($i, 2, "0", STR_PAD_LEFT);
                    }
                    $userAttendance = \App\Entities\Attendance::with('caregiver','schedule')->whereNotNull('schedule_id')->where('date',date($this->request['filter_year'].'-'.$this->request['filter_month'].'-'.$i))->get();
                    if(count($userAttendance)){
                        foreach ($userAttendance as $index => $value) {
                            if(isset($value['punch_in']) && isset($value['punch_out'])){
                                $from = strtotime($value['punch_in']);
                                $to = strtotime($value['punch_out']);

                                $init = ($to - $from);
                                $hours = floor($init / 3600);
                                $minutes = floor(($init / 60) % 60);

                                if($init == 0 ){
                                    $res = 24;
                                }else if($init < 0 ){
                                    if($minutes == 0){
                                        $res = 24 - abs($hours);
                                    }else{
                                        $res = (24 - abs($hours)).':'.(60 - abs($minutes));
                                    }
                                }else{
                                    if($minutes == 0){
                                        $res = $hours;
                                    }else{
                                        $res = $hours.':'.$minutes;
                                    }
                                }
                            }else{
                                $res = 'NA';
                            }

                            $data[] = [
                                'Sl no' => (string) ++$counter,
                                'Date'  => date($i.'-'.$this->request['filter_month'].'-'.$this->request['filter_year']),
                                'ID'    => $value->caregiver->employee_id,
                                'Name'  => $value->caregiver->full_name,
                                'Cadre'    => isset($value->caregiver->professional->designations)?$value->caregiver->professional->designations->designation_name:'NA',
                                'Service'    => $value->schedule->service->service_name,
                                'Chargeable'    => ($value->schedule->chargeable == 1)?'Yes':'No',
                                'From'  => isset($value->punch_in)?\Carbon\Carbon::parse($value->punch_in)->format('H:i A'):'NA',
                                'To'    => isset($value->punch_out)?\Carbon\Carbon::parse($value->punch_out)->format('H:i A'):'NA',
                                'Hours' => (string) $res,
                            ];
                        }
                    }
                }

                $fileName = 'EmployeeWorkTime_'.str_replace(' ','_',\App\Entities\Profile::whereId(1)->value('organization_name')).date('_d-m-Y_h:i:s_A');

                \Excel::download(new UsersExport, 'EmployeeWorkTime1.xlsx');

                $file = \Excel::store($fileName, function($excel) use ($data) {
                    $excel->sheet('EmployeeWorkTime', function($sheet) use ($data){
                        $sheet->freezeFirstRow();
                        $sheet->fromArray($data);
                    });
                });

                $param = $this->request;

                \Mail::send('mails.reports.worktime-report',[],function($m) use($param,$file){
                    $m->to($param['email'])->subject('Report Worktime');
                    $m->attach($file->store("xls",false,true)['full']);
                });

                $workTimeMailer = WorkTimeMailers::whereJobId($this->job->getJobId())->first();
                if($workTimeMailer){
                    $workTimeMailer->update(['status' => 'Processed']);
                }
            }
        }catch(Exception $e){
            if($this->job){
                $workTimeMailer = WorkTimeMailers::whereJobId($this->job->getJobId())->first();
                if($workTimeMailer){
                    $workTimeMailer->update(['status' => 'Failed','exception' => $e]);
                }
            }
        }
    }

    public function failed()
    {
        if($this->job){
            $workTimeMailer = WorkTimeMailers::whereJobId($this->job->getJobId())->first();
            if($workTimeMailer){
                $workTimeMailer->update(['status' => 'Failed']);
            }
        }
    }
}
