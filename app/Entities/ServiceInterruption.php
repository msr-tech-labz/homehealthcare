<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use App\Traits\TenantTrait;

class ServiceInterruption extends Model implements AuditableContract
{
    use SoftDeletes;
    use Auditable;
    use TenantTrait;

    protected $table = 'case_service_interruptions';

    protected $fillable = ['tenant_id','lead_id','schedule_id','from_date','to_date','reason','user_id'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['from_date','to_date','deleted_at'];

    public function caserecord()
    {
        return $this->belongsTo(Careplan::class,'id','lead_id');
    }

    public function schedule()
    {
        return $this->belongsTo(Schedule::class,'id','schedule_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
}
