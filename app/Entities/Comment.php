<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Comment extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'case_comments';

    protected $fillable = ['tenant_id','lead_id','schedule_id','user_id','user_type','user_name','comment'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function careplan()
    {
        return $this->belongsToMany(Careplan::class,'caregiver_skills','caregiver_id','skill_id')
                    ->withTimestamps();
    }
}
