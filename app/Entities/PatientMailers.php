<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class PatientMailers extends Model
{
    use SoftDeletes, TenantTrait;

    protected $fillable = ['tenant_id','patient_id','name','email','status'];
    protected $table = 'patient_mailers';
    protected $guarded = 'id';
    public $timestamps = true;

    public function patient(){
        return $this->belongsTo(Patient::class,'patient_id','id');
    }
}
