<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Attendance extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'employee_attendance';

    protected $fillable = ['tenant_id','caregiver_id','schedule_id','date','punch_in','punch_out','punch_in_server_timestamp','punch_out_server_timestamp','location_coordinates','source','marked_by','status','on_training'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['date','deleted_at'];

    public function caregiver()
    {
        return $this->belongsTo(\App\Entities\Caregiver\Caregiver::class);
    }
    public function schedule()
    {
        return $this->belongsTo(\App\Entities\Schedule::class,'schedule_id','id');
    }
}
