<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class AggregatorLeads extends Model
{
    use SoftDeletes;

    protected $connection = 'central';

    protected $table = 'aggregator_leads';

    protected $fillable = ['provider_id','branch_id','lead_id','lead_request_id','patient_id','service_id','rate_agreed','user_id','lead_source_id','source','status','accepted_declined_date'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['accepted_declined_date','deleted_at'];
}
