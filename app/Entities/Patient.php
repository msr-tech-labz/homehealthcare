<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Patient extends Model
{
    use SoftDeletes, TenantTrait;

    protected $fillable = ['tenant_id','branch_id','first_name','patient_id','last_name','date_of_birth','patient_age','patient_weight','gender','contact_number','email','alternate_number','enquirer_name','relationship_with_patient','street_address','area','city','zipcode','state','country','latitude','longitude','enquirer_profession','client_sig','date','place','familia_app_link','link_responce_date'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $hidden = ['deleted_at'];
    protected $dates = ['date_of_birth','deleted_at'];

    public function getFullNameAttribute($value)
    {
        return $this->attributes['first_name'].' '.$this->attributes['last_name'];
    }

    public function getAddressAttribute($value)
    {
        return $this->attributes['city'].','.$this->attributes['city'];
    }

    public function setContactNumberAttribute($value)
    {
        $this->attributes['contact_number'] = preg_replace('/\s+/','',$value);
    }

    public function setAlternateNumberAttribute($value)
    {
        $this->attributes['alternate_number'] = preg_replace('/\s+/','',$value);
    }

    public function lead()
    {
        return $this->hasMany(Lead::class,'patient_id','id');
    }

    public function mailers()
    {
        return $this->hasMany(PatientMailers::class,'patient_id','id');
    }

    public function address()
    {
        return $this->hasMany(PatientAddress::class,'patient_id','id');
    }

    public function services()
    {
        return $this->hasMany(CaseServices::class,'patient_id','id');
    }

    public function serviceOrders()
    {
        return $this->hasMany(CaseServiceOrder::class,'patient_id','id');
    }

    public function schedules()
    {
        return $this->hasMany(Schedule::class,'patient_id','id');
    }

    public function billables()
    {
        return $this->hasMany(CaseBillables::class,'patient_id','id');
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class,'patient_id','id');
    }

    public function creditMemo()
    {
        return $this->hasMany(CreditMemo::class,'patient_id','id');
    }

    public function unbilledCreditMemo()
    {
        return $this->creditMemo()->whereStatus('Unbilled');
    }

    public function unbilledCreditMemoService()
    {
        return $this->creditMemo()->whereType('Others')->whereServiceStatus('Unbilled');
    }

    public function payments()
    {
        return $this->hasMany(CasePayment::class,'patient_id','id');
    }
}
