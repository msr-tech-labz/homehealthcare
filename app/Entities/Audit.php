<?php
namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{
    protected $table = 'audits';

    protected $fillable = ['user_id','event','auditable_id','auditable_type','old_values','new_values','url','ip_address','user_agent'];

    protected $guarded = 'id';
    public $timestamps = true;
}
