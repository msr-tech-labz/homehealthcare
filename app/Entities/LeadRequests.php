<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeadRequests extends Model
{
    use SoftDeletes;

    protected $table = 'lead_requests';

    protected $connection = 'central';

    protected $fillable = ['customer_id','lead_id','name','phone_number','email','city','requirement_type','requirement','exotel_sid','source','status'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function sources()
    {
    	return $this->belongsTo(\App\Entities\AggregatorLeads::class,'id','lead_request_id');
    }
}
