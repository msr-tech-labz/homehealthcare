<?php

namespace App\Entities\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Department extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'master_departments';

    public $createdByColumn = true;

    protected $fillable = [
        'tenant_id',
        'branch_id',
        'department_name',
        'description',
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function scopeBranch($query)
    {   
        if(\Helper::encryptor('decrypt',session('utype')) != 'Admin')
        return $query->whereBranchId(\App\Helpers\Helper::getSession('branch_id'));
    }
}
