<?php

namespace App\Entities\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeSource extends Model
{
    use SoftDeletes;

    protected $table = 'master_employee_source';

    protected $fillable = [
        'tenant_id',
        'branch_id',
        'name',
        'description',
        'status'
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
