<?php

namespace App\Entities\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Trainings extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'master_trainings';

    public $createdByColumn = true;

    protected $fillable = [
        'tenant_id',
        'branch_id',
        'trainer_name',
        'training_name',
        'trainer_email',
        'trainer_number',
        'start_date',
        'end_date',
        'employee_id',
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function caregiver()
    {
        return $this->belongsTo(\App\Entities\Caregiver\Caregiver::class,'employee_id','id');
    }
}
