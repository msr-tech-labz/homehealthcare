<?php

namespace App\Entities\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Designation extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'master_designations';

    public $createdByColumn = true;

    protected $fillable = [
        'tenant_id',
        'branch_id',
        'designation_name',
        'description',
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
