<?php

namespace App\Entities\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class ReferralSource extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'master_referral_source';

    protected $fillable = [
        'tenant_id',
        'category_id',
        'source_name',
        'phone_number',
        'description',
        'charge_type',
        'referral_value',
        'bank_name',
        'bank_address',
        'account_number',
        'ifsc_code'
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function category()
    {
        return $this->belongsTo(ReferralCategory::class,'category_id','id');
    }
}
