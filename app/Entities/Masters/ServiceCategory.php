<?php

namespace App\Entities\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class ServiceCategory extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'master_service_category';

    public $createdByColumn = true;

    protected $fillable = [
        'tenant_id',
        'branch_id',
        'created_by',
        'category_name',
        'status',
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function service()
    {
        return $this->belongsTo(\App\Entities\Masters\Service::class,'id','category_id');
    }
}
