<?php

namespace App\Entities\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class CostCentre extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'master_costcentre';

    public $createdByColumn = true;

    protected $fillable = [
        'tenant_id',
        'specialization_id',
        'cost_centre',
        'description',
        'status'
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function specialization()
    {
        return $this->belongsTo(Specialization::class,'specialization_id','id');
    }
}
