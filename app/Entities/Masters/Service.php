<?php

namespace App\Entities\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Service extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'master_services';

    public $createdByColumn = false;

    protected $fillable = [
        'tenant_id',
        'branch_id',
        'category_id',
        'service_type',
        'service_name',
        'hsn',
        'description',
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    
    public function category()
    {
        return $this->belongsTo(\App\Entities\Masters\ServiceCategory::class,'category_id','id');
    }

    public function ratecard()
    {
        return $this->belongsTo(\App\Entities\Masters\RateCard::class,'id','service_id')
                    ->where('status',1);
    }
}
