<?php

namespace App\Entities\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Holidays extends Model
{
    use SoftDeletes;

    protected $table = 'master_holidays';

    protected $fillable = [
        'tenant_id',
        'branch_id',
        'holiday_name',
        'holiday_date',
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['holiday_date','deleted_at'];
}
