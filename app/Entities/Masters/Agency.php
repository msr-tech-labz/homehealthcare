<?php

namespace App\Entities\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Agency extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'master_agency';

    public $createdByColumn = true;

    protected $fillable = [
        'tenant_id',
        'agency_name',
        'description',
        'agency_email'
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
