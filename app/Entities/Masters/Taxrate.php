<?php

namespace App\Entities\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Taxrate extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'master_taxrates';

    public $createdByColumn = true;

    protected $fillable = [
        'tenant_id',
        'branch_id',
        'created_by',
        'tax_name',
        'tax_rate',
        'type',
        'order',
        'status'
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
