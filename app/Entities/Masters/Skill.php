<?php

namespace App\Entities\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Skill extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'master_skills';

    public $createdByColumn = true;

    protected $fillable = [
        'tenant_id',
        'branch_id',
        'created_by',
        'skill_name',
        'skill_type',
        'description',
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function employees()
    {
        return $this->belongsToMany(\App\Entities\HR\Employee::class);
    }
}
