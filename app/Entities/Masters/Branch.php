<?php

namespace App\Entities\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Branch extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'master_branches';

    protected $fillable = [
        'tenant_id',
        'branch_name',
        'branch_code',
        'branch_email',
        'branch_address',
        'branch_area',
        'branch_city',
        'branch_zipcode',
        'branch_state',
        'branch_country',
        'branch_contact_person',
        'branch_contact_number',
        'auth_signatory',
        'signtory_pos',
        'proforma_invoice_inits',
        'proforma_invoice_start_no',
        'invoice_inits',
        'invoice_start_no',
        'receipt_inits',
        'receipt_start_no',
        'credit_inits',
        'credit_start_no',
        'cutoff_period',
        'patient_id_prefix',
        'patient_id_start_no',
        'employee_id_prefix',
        'employee_id_start_no',
        'invoice_payee',
        'bank_name',
        'bank_address',
        'acc_num',
        'ifsc_code',
        'pan_number',
        'gstin',
        'cin',
        'payment_due_date',
        'status'
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
