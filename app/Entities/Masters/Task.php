<?php

namespace App\Entities\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Task extends Model
{
    use TenantTrait;

    protected $table = 'master_tasks';

    protected $fillable = [
        'tenant_id',
        'branch_id',
        'category',
        'task_name',
        'description'
    ];

    protected $guarded = 'id';
    public $timestamps = true;
}
