<?php

namespace App\Entities\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class EmailSubscriber extends Model
{
    use SoftDeletes, TenantTrait;

    protected $table = 'master_email_subscribers';

    protected $fillable = [
        'tenant_id',
        'mailer_types',
        'email',
        'name',
        'status',
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function scopeMailer($query, $type)
    {
        return $query->whereRaw(\DB::raw('FIND_IN_SET("'.$type.'",mailer_types)'));
    }
}
