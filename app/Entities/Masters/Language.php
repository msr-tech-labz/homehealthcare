<?php

namespace App\Entities\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Language extends Model
{
    use TenantTrait;

    public $table = 'master_languages';

    public $createdByColumn = true;

    protected $fillable = [
        'tenant_id',
        'language_name',
        'country',
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function scopeSortByAsc($query)
    {
        return $query->orderBy('language_name','Asc');
    }

    public function scopeSortByDesc($query)
    {
        return $query->orderBy('language_name','Desc');
    }
}
