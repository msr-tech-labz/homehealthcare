<?php

namespace App\Entities\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PluginSettings extends Model
{
    use SoftDeletes;

    protected $table = 'plugin_settings';

    protected $connection = 'central';

    protected $fillable = [
        'tenant_id',
        'category',
        'plugin_name',
        'settings'
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
