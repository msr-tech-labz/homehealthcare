<?php

namespace App\Entities\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class ServiceLab extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'master_services_lab';

    public $createdByColumn = false;

    protected $fillable = [
        'tenant_id',
        'branch_id',
        'service_name',
        'description',
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function careplan()
    {
        return $this->hasMany(\App\Entities\Careplan::class);
    }
}
