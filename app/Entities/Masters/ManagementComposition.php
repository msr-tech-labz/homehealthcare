<?php

namespace App\Entities\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class ManagementComposition extends Model
{
    use TenantTrait;

    public $table = 'master_management_compositions';

    public $createdByColumn = true;

    protected $fillable = [
        'tenant_id',
        'type',
        'composition_name',
        'composition_cadre',
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
