<?php

namespace App\Entities\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class BillingProfile extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'billing_profile';
    protected $fillable = ['tenant_id','branch_id','name','email','address','area','city','state','country','zipcode','phone','logo','pan_number','service_tax_no','gstin'];

    protected $guarded = 'id';
    public $timestamps = true;
}
