<?php

namespace App\Entities\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class ReferralCategory extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'master_referral_category';

    protected $fillable = [
        'tenant_id',
        'branch_id',
        'category_name',
        'description',
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function source()
    {
        return $this->hasMany(ReferralSource::class,'category_id','id');
    }
}
