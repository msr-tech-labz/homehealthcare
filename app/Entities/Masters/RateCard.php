<?php

namespace App\Entities\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class RateCard extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'master_ratecards';

    protected $fillable = [
        'tenant_id',
        'branch_id',
        'service_id',
        'tax_id',
        'amount',
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function tax()
    {
        return $this->belongsTo(Taxrate::class,'tax_id','id');
    }
}
