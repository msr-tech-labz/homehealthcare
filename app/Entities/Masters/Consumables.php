<?php

namespace App\Entities\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Consumables extends Model
{
    use SoftDeletes, TenantTrait;

    protected $table = 'master_consumables';

    public $createdByColumn = true;

    protected $fillable = [
        'tenant_id',
        'branch_id',
        'consumable_name',
        'description',
        'consumable_price',
        'tax_id'
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function tax()
    {
        return $this->belongsTo(Taxrate::class,'tax_id','id');
    }
}
