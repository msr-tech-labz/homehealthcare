<?php

namespace App\Entities\Masters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

use App\Entities\Caregiver\ProfessionalDetails;


class Specialization extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'master_specializations';

    public $createdByColumn = true;

    protected $fillable = [
        'tenant_id',
        'branch_id',
        'created_by',
        'specialization_name',
        'specialization_type',
        'description',
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function professional()
    {
        return $this->hasOne(ProfessionalDetails::class,'specialization','id');
    }
}
