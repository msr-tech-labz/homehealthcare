<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

use App\Entities\Caregiver\Caregiver;
use App\Entities\Lead;
use App\Entities\LeadRequests;
use App\Entities\Masters\Service;
use App\Entities\CaseDisposition;
use App\Entities\AggregatorLeads;

class Dashboard extends Model
{
    public static function getStats()
    {
        $stats = ['users' => 0, 'cases' => 0, 'caregivers' => 0, 'patients' => 0];

        $stats['users'] = User::count();
        $stats['cases'] = Lead::count();
        $stats['caregivers'] = Caregiver::count();
        $stats['patients'] = Patient::whereTenantId(\Helper::getTenantID())->count();
        return $stats;
    }

    public static function getCaseStats()
    {
        $stats = ['pending' => 0, 'converted' => 0, 'dropped' => 0];

        if(\Helper::encryptor('decrypt',session('utype')) == 'Aggregator'){
                $stats['pending'] = Lead::whereMonth('created_at','=',date('m'))
                                    ->whereYear('created_at','=',date('Y'))
                                    ->whereIn('status',['Pending','Assessment Pending','Assessment Completed'])->count()+
                                    AggregatorLeads::whereMonth('created_at','=',date('m'))
                                    ->whereYear('created_at','=',date('Y'))
                                    ->whereStatus('Pending')->count()+
                                    LeadRequests::whereMonth('created_at','=',date('m'))
                                    ->whereYear('created_at','=',date('Y'))
                                    ->whereStatus('Not Checked')->count();

                $stats['converted']=Lead::whereMonth('created_at','=',date('m'))
                                    ->whereYear('created_at','=',date('Y'))
                                    ->whereStatus('Converted')->count()+
                                    AggregatorLeads::whereMonth('created_at','=',date('m'))
                                    ->whereYear('created_at','=',date('Y'))
                                    ->whereStatus('Accepted')->count();

                $stats['dropped'] = Lead::whereMonth('created_at','=',date('m'))
                                    ->whereYear('created_at','=',date('Y'))
                                    ->whereStatus('Dropped')->count()+
                                    AggregatorLeads::whereMonth('created_at','=',date('m'))
                                    ->whereYear('created_at','=',date('Y'))
                                    ->whereStatus('Dropped')->count();
        }else{
	            $stats['pending'] = Lead::whereMonth('created_at','=',date('m'))
                                    ->whereYear('created_at','=',date('Y'))
                                    ->whereIn('status',['Pending','Assessment Pending','Assessment Completed'])
                                    ->count();
                $stats['converted'] = Lead::whereMonth('created_at','=',date('m'))
                                    ->whereYear('created_at','=',date('Y'))
                                    ->where('status','Converted')
                                    ->count();

                $stats['dropped'] = Lead::whereMonth('created_at','=',date('m'))
                                    ->whereYear('created_at','=',date('Y'))
                                    ->where('status','Dropped')
                                    ->count();
        }

        return $stats;
    }

    public static function getCaregiverStats()
    {
        $stats = ['total' => 0, 'onduty' => 0, 'bench' => 0, 'replacement' => 0];

        $stats['total'] = Caregiver::active()->whereTenantId(\Helper::getTenantID())->count();
        $stats['onduty'] = Caregiver::active()->whereTenantId(\Helper::getTenantID())->whereWorkStatus('On Duty')->count();
        $stats['available'] = Caregiver::active()->whereTenantId(\Helper::getTenantID())->whereWorkStatus('Available')->count();
        $stats['leave'] = Caregiver::active()->whereTenantId(\Helper::getTenantID())->whereWorkStatus('Leave')->count();
        $stats['training'] = Caregiver::active()->whereTenantId(\Helper::getTenantID())->whereWorkStatus('Training')->count();

        return $stats;
    }

    public static function DailyStats()
    {
        $data['leadstotal']           = Lead::where('created_at', '>=' ,date('Y-m-d'))->count();

        $data['leadswebsite']         = Lead::where('created_at', '>=' ,date('Y-m-d'))
                                            ->where('source','Website')->whereNotIn('status' ,['Duplicate'])->count();

        $data['leadsexotel']          = Lead::where('created_at', '>=' ,date('Y-m-d'))
                                            ->where('source','Exotel')->whereNotIn('status' ,['Duplicate'])->count();

        $data['leadsRequestsjob']     = LeadRequests::where('created_at', '>=' ,date('Y-m-d'))
                                            ->where('requirement_type','JOB')->count();

        $data['leadsRequestspartner'] = LeadRequests::where('created_at', '>=' ,date('Y-m-d'))
                                            ->where('requirement_type','PARTNERSHIP')->count();
        $res = Service::withoutGlobalScopes()->with('careplan')->get();
        $result = [];
        foreach ($res as $r) {
            $result[] = [
                'service_name' => $r->service_name,
                //'count' => isset($r->careplan)?$r->careplan->where('created_at','>=',date('Y-m-d'))->count():0
                'count' => Lead::withoutGlobalScopes()->whereServiceId($r->id)->where('created_at','>=',date('Y-m-d'))->count()
            ];
        }

        $mdata['data'] = $data;
        $mdata['result'] = $result;

        return $mdata;
    }
}
