<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FeedbackCall extends Model
{
    use SoftDeletes;    

    protected $table = 'feedback_call_logs';
    protected $connection = 'central';

    protected $fillable = ['tenant_id','lead_id','comment','time','user_id','user_name'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function lead()
    {
        return $this->belongsTo(Lead::class,'lead_id','id');
    }
}