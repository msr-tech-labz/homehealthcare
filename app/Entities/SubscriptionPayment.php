<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class SubscriptionPayment extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $connection = 'central';

    protected $table = 'subscription_payments';

    protected $fillable = ['tenant_id','user_type','transaction_id','merchant_txn_id','transaction_date','credit_count','actual_amount','gst','tds','total_amount','pg_response','status'];


    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['transaction_date','deleted_at'];

    public function scopeSuccess($query)
    {
        return $query->whereStatus('SUCCESS');
    }
}
