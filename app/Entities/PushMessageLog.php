<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class PushMessageLog extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'push_messages_log';

    protected $fillable = ['tenant_id','type','lead_id','user_id','message_date','token','data','extra'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}

