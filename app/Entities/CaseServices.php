<?php

namespace App\Entities;

use App\Entities\AuditableModel;
use App\Traits\TenantTrait;

class CaseServices extends AuditableModel
{
    use TenantTrait;

    protected $table = 'case_service_requests';

    protected $fillable = ['tenant_id', 'branch_id', 'user_id', 'lead_id', 'patient_id', 'service_id', 'from_date', 'to_date', 'custom_dates', 'frequency_period', 'frequency', 'gross_rate', 'discount', 'discount_type', 'discount_value', 'net_rate', 'status','cancel_reason','cancel_comment'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['from_date','to_date','deleted_at'];

    public function getPeriodAttribute($value)
    {
        $customDates = [];

        $fromDate = date_format(new \DateTime($this->attributes['from_date']),'d-m-Y');
        $toDate = date_format(new \DateTime($this->attributes['to_date']),'d-m-Y');

        if($this->attributes['custom_dates'] != ''){
            $customDates = explode(',',trim($this->attributes['custom_dates']));
            $fromDate = $customDates[0];
            $toDate = end($customDates);
        }

        return $fromDate.' to '.$toDate;
    }

    public function scopeActive($query)
    {
        return $query->whereStatus(1);
    }

    public function lead()
    {
        return $this->belongsTo(Lead::class,'lead_id','id');
    }

    public function patient()
    {
        return $this->belongsTo(Patient::class,'patient_id','id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function service()
    {
        return $this->belongsTo(\App\Entities\Masters\Service::class,'service_id','id');
    }

    public function caseServiceOrder()
    {
        return $this->hasMany(CaseServiceOrder::class,'service_request_id','id');
    }

    public function schedules()
    {
        return $this->hasMany(Schedule::class,'service_request_id','id');
    }

    public function discountLog()
    {
        return $this->hasMany(DiscountsLog::class,'service_request_id','id');
    }

    public function documents()
    {
        return $this->hasMany(CaseServices::class,'service_request_id','id');
    }

    public function scheduleCount($status)
    {
        return $this->schedules()->where('status',$status)->count();
    }

    public function payments()
    {
        return $this->hasMany(InvoiceItems::class,'schedule_id','id');
    }

    public function schedulePaymentCount($status)
    {
        return $this->payments()->where('status',$status)->count();
    }
}
