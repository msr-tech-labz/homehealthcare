<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class LeavesOld extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'employee_leaves_old';
    
    protected $fillable = ['tenant_id','branch_id','caregiver_id','name','date_received','date_from','date_to','reason','type','status'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['date_received','date_from','date_to','deleted_at'];

    public function scopePendingApproval($query)
    {
        return $query->whereStatus('PENDING');
    }

    public function scopeBranch($query)
    {
        if(\Helper::encryptor('decrypt',session('utype')) != 'Admin' && \Helper::encryptor('decrypt',session('admin_id')) != '0')
            return $query->whereBranchId(\App\Helpers\Helper::getSession('branch_id'));
    }

    public function caregiver()
    {
        return $this->belongsTo(\App\Entities\Caregiver\Caregiver::class,'caregiver_id','id');
    }

    public function branchName()
    {
        return $this->belongsTo(\App\Entities\Masters\Branch::class,'branch_id','id');
    }

    public function approver()
    {
        return $this->belongsTo(\App\Entities\LeaveApproval::class,'id','leave_id');
    }
}
