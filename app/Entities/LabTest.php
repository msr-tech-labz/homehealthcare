<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class LabTest extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'case_labtest';

    protected $fillable = ['tenant_id','careplan_id','user_id','test_details','total_price'];

    protected $guarded = 'id';
    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
}
