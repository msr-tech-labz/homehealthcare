<?php

namespace App\Entities;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

use App\Helpers\Helper;
use App\Entities\Console\Subscriber;
use App\Entities\LoggingEvents;
use App\Traits\TenantTrait;

class User extends Authenticatable implements AuditableContract
{
    use Notifiable;
    use EntrustUserTrait;
    use TenantTrait;
    use Auditable;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_type', 'tenant_id', 'created_by', 'user_type', 'branch_id', 'user_id','full_name', 'email', 'password', 'role_id', 'status', 'user_deleted', 'last_session'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $auditExclude = [
            'last_session','remember_token'
        ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    
    public function setPasswordAttribute($password)
    {
        if(\Hash::needsRehash($password)) {
            $this->attributes['password'] = bcrypt($password);
        }else{
            $this->attributes['password'] = $password;
        }
    }

    public function role()
    {
        return $this->belongsTo(\App\Entities\Role::class);
    }

    public function provider()
    {
        return $this->belongsTo(Profile::class,'tenant_id','tenant_id');
    }

    public function branch()
    {
        return $this->belongsTo(\App\Entities\Masters\Branch::class,'branch_id','id');
    }

    public function caregiver()
    {
        return $this->belongsTo(\App\Entities\Caregiver\Caregiver::class,'user_id','id');
    }

    public static function validateCredentials($orgCode, $email, $user_password, $renew=false)
    {
        $user = null;
        if(!empty($orgCode) && !empty($email) && !empty($user_password)){
            // Check for ORG ID and Fetch database_name field
            $subscriber = Subscriber::active()->whereOrgCode(str_replace(' ','',$orgCode))->first();
            if($subscriber){
                // Fetch database_name field
                $dbName = $subscriber->database_name;

                // Set the tenant connection database name to fetched value
                \Config::set('database.connections.tenant.database',$dbName);
                session()->put('dbName',\Helper::encryptor('encrypt',$dbName));
                session()->put('orgType',$subscriber->user_type);
                session()->put('corp_id',\Helper::encryptor('encrypt',$subscriber->corp_id));
                isset($subscriber->corporate)?session()->put('corpDbName',\Helper::encryptor('encrypt',$subscriber->corporate->db_name)):'';
                session()->put('subscriber_id',$subscriber->subscriber_id);
                session()->put('instance_type', $subscriber->instance_type);

                $now = \Carbon\Carbon::now('Asia/Kolkata');

                // if($renew == false){
                // // Check for trial period completion
                //     if($subscriber->instance_type == 'Trial' && !empty($subscriber->trial_end_date)){
                //         session()->put('trial_end_date', $subscriber->trial_end_date);
                //         $daysLeft = 0;
                //     // Check if trail end date is not today and is greater than 0
                //         if($now->format('d-m-Y') != $subscriber->trial_end_date->format('d-m-Y') && $now->diffInDays($subscriber->trial_end_date, false) >= 0){
                //             $daysLeft = $now->diffInDays($subscriber->trial_end_date, false) + 1;
                //         }

                //     // If trail period expired, redirect to login page with error message
                //     if($daysLeft <= 0){
                //         session()->put('trail_expired','Your subscription has expired.');
                //         return null;
                //     }
                //     if($daysLeft <= 5){
                //         session()->put('trail_period_warning','Your subscription will expire in '.$daysLeft.' day(s).');
                //     }
                // }

                // // Check for pending subscription payment
                //     if($subscriber->instance_type == 'Paid'){
                //         $lastPayment = SubscriptionPayment::select('tenant_id','transaction_date','status')
                //         ->whereTenantId($subscriber->id)->success()
                //         ->whereRaw(\DB::raw('MONTH(transaction_date) = MONTH(NOW())'))
                //         ->orderBy('transaction_date','Desc')->take(1)->count();
                //         if($lastPayment <= 0){
                //         // If subscription has ended, redirect to login page with error message
                //             if(date('d') == 30 || date('d') == 31 || (date('m') == 2 && date('d') == 28)){
                //                 session()->put('subscription_expired','Your subscription has expired.');
                //                 return null;
                //             }else{
                //                 $daysLeft = $now->daysInMonth - $now->day;
                //                 session()->put('subscription_payment_warning','Your subscription will expire in '.$daysLeft.' day(s).');
                //             }
                //         }
                //     } 
                // }

                // Set the tenant as default connection
                \DB::setDefaultConnection('tenant');

                if(filter_var($email, FILTER_VALIDATE_EMAIL)){
                    $user = User::withoutGlobalScopes()->whereTenantId($subscriber->id)
                                    ->whereEmail($email)
                                    ->get();
                }else{
                    $user = User::withoutGlobalScopes()->whereTenantId($subscriber->id)
                                    ->whereHas('caregiver',function($q) use ($email) {
                                        $q->whereMobileNumber($email);
                                    })
                                    ->get();
                }

                if(count($user) > 1){
                    $user = 'Duplicate';
                    return $user;
                }elseif (count($user)) {
                    $user = $user->first();
                    if($user->status == 0){
                        $user = "DisabledUser";
                        return $user;
                    }
                }else{
                    $user = $user->first();
                }

                if($user){
                    $userData = [
                        'user_id'       =>  $user->id,
                        'user_agent'    =>  \Illuminate\Support\Facades\Request::header('User-Agent'),
                        'ip_address'    =>  \Illuminate\Support\Facades\Request::ip(),
                        'login_time'    =>  date("h:i:s"),
                        'login_status'  =>  'Success'
                    ];
                    if(\Hash::check($user_password,$user->password)){
                        $logevent = LoggingEvents::create($userData);
                        session()->put('login_id',\Helper::encryptor('encrypt',$logevent->id));
                        $user->update(['last_session' => session()->getId()]);
                        session()->put('last_session',session()->getId());
                        return $user;
                    }else{
                        $userData['login_status']  =  'Failure';
                        $logevent = LoggingEvents::create($userData);
                        session()->put('login_id',\Helper::encryptor('encrypt',$logevent->id));
                        $user = "InvalidPassword";
                    }
                }else{
                    $user = "NoUserFound";
                }
            }else{
                $user = "NoSubscriber";
            }
        }
        return $user;
    }
}
