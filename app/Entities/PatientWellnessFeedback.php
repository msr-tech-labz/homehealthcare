<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class PatientWellnessFeedback extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'patient_wellness_feedback';

    protected $fillable = ['tenant_id','lead_id','schedule_id','patient_id','caregiver_id','hapiness','appetite','physical_activity','communication','anger'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function lead()
    {
        return $this->belongsTo(Lead::class,'lead_id','id');
    }

    public function schedule()
    {
        return $this->belongsTo(Schedule::class,'schedule_id','id');
    }

    public function patient()
    {
        return $this->belongsTo(Patient::class,'patient_id','id');
    }
}