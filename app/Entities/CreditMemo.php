<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class CreditMemo extends Model
{
    use SoftDeletes, TenantTrait;

    protected $table = 'billing_credit_memo';
    protected $fillable = ['tenant_id','branch_id','credit_memo_no','patient_id','type','sub_type','schedule_id','date','amount','comment','user_id','status','service_status'];

    protected $guarded = 'id';
    public $dates = ['date','deleted_at'];
    public $timestamps = true;

    public function patient()
    {
        return $this->belongsTo(Patient::class,'patient_id','id');
    }

    public function invoices()
    {
        return $this->hasMany(InvoiceItems::class,'item_id','id')
                    ->whereCategory('Credit Memo');
    }

    public function receipt()
    {
        return $this->hasOne(Receipt::class,'item_id','id')->whereReceiptType('Memo');
    }  
}
