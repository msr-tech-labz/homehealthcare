<?php

namespace App\Entities;

use App\Entities\AuditableModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class StatementOfAccount extends AuditableModel
{
    use SoftDeletes, TenantTrait;

    protected $table = 'statement_of_account';
    protected $fillable = ['tenant_id','branch_id','patient_id','service_amount_raised','credit_memo_raised','billables_amount_raised','refunds_raised','payments_received'];

    protected $guarded = 'id';
    public $timestamps = true;
    public $dates = ['deleted_at'];

    public function patient()
    {
        return $this->belongsTo(Patient::class,'patient_id','id');
    }
}
