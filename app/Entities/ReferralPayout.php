<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class ReferralPayout extends Model
{
    use SoftDeletes;

    protected $table = 'case_referral_payout';

    protected $fillable = [
        'tenant_id',
        'lead_id',
        'patient_id',
        'source_id',
        'type',
        'value'
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function lead()
    {
        return $this->belongsTo(Lead::class,'lead_id','id');
    }

    public function source()
    {
        return $this->belongsTo(\App\Entities\Masters\ReferralSource::class,'source_id','id');
    }

    public function patient()
    {
        return $this->belongsTo(Patient::class,'patient_id','id');
    }
}
