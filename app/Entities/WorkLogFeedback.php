<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class WorkLogFeedback extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'case_worklogs_feedback';

    protected $fillable = ['tenant_id','lead_id','worklog_id','patient_id','customer_id','caring_and_patience','knowledge','cleanliness','reliabilty_and_punctuality','communication_with_patient','over_all_rating','additional_comments','action_by','status','action'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function worklog()
    {
        return $this->belongsTo(WorkLog::class,'worklog_id','id');
    }

    public function lead()
    {
        return $this->belongsTo(Lead::class,'lead_id','id');
    }

    public function patient()
    {
        return $this->belongsTo(Patient::class,'patient_id','id');
    }
}