<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PatientCheckList extends Model
{
    use SoftDeletes;

    protected $fillable = ['tenent_id','patient_id','lead_id','care_attendant_intro_check','care_attendant_intro','understand_medical_history_check','understand_medical_history','medicine_handover_check','medicine_handover','suggestingvalue_check','suggestingvalue','officeshifttimings_check','officeshifttimings','serviceworking_check','serviceworking','plannedfollow_check','plannedfollow','paymentpolicy_check','paymentpolicy','trailperiod_check','trailperiod','servicestarted_check','servicestarted','servicestation_check','servicestation','uniformstatus_check','uniformstatus','logbook_check','logbook','clarifyterms_check','clarifyterms','providingtea_check','providingtea','directcommunication_check','directcommunication','housekeping_check','housekeping','intimationemail_check','intimationemail','nightshift_check','nightshift','staffpolicy_check','staffpolicy','femaleshift_check','femaleshift','medicalcondition_check','medicalcondition',
    'stockmedicine_check','stockmedicine','careplan_check','careplan','safetyaudit_check','safetyaudit','escalation_check','escalation','problems_check','problems','providemanagerno_check','providemanagerno','servicemanagerphone_check','servicemanagerphone','companionship_check','companionship','omplementaryservices_check','omplementaryservices','mobileapp_check','mobileapp','keyfamily','identifyinvoice','nameandsig','smname','date_time','managersig'];
    protected $table = 'patient_checklist_details';
    protected $guarded = 'id';
    public $timestamps = true;
    public $dates = ['deleted_at','date_time'];

    public function patient(){
        return $this->belongsTo(Patient::class,'patient_id','id');
    }

}
