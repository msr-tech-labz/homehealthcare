<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Receipt extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'case_receipts';
    protected $fillable = ['tenant_id','patient_id','item_id','receipt_type','payment_mode','reference_no','receipt_no','receipt_date','receipt_amount'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['receipt_date'];

    public function patient()
    {
        return $this->belongsTo(Patient::class,'patient_id','id');
    }

    public function creditMemo()
    {
        return $this->hasOne(CreditMemo::class,'id','item_id');
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class,'item_id','id');
    }
}
