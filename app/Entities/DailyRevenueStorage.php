<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DailyRevenueStorage extends Model
{
	use SoftDeletes;

    protected $table = "daily_revenue_storage";
    protected $fillable = ['date','gross_todate','discount_todate','revenue_todate','taxed_todate','mcr_todate','flag'];
    protected $guarded = 'id';
    
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
