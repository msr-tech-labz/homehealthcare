<?php

namespace App\Entities;

use App\Entities\AuditableModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Schedule extends AuditableModel
{
    use SoftDeletes, TenantTrait;

    protected $table = 'case_schedules';

    protected $fillable = ['tenant_id','branch_id','user_id','lead_id','patient_id','caregiver_id','service_request_id','service_order_id','schedule_id','schedule_date','start_time','end_time','service_required','notes','assigned_tasks','chargeable','amount','ratecard_id','road_distance','delivery_address','status','cancel_reason','cancel_comment'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['schedule_date','deleted_at'];

    public function scopeConverted($query)
    {
        return $query->whereStatus('Completed');
    }

    public function scopeDeployed($query)
    {
        if(\Helper::encryptor('decrypt',session('admin_id')) != 0){
            $userRoleCaregiver = \Helper::getStaffRole(\Helper::encryptor('decrypt',session('admin_id')));
            if($userRoleCaregiver == 'Caregiver'){
                return $query->where('caregiver_id',\Helper::encryptor('decrypt',session('admin_id')));
            }
        }
    }

    public function lead()
    {
        return $this->belongsTo(Lead::class,'lead_id','id');
    }

    public function caserecord()
    {
        return $this->belongsTo(Lead::class,'lead_id','id');
    }

    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }

    public function caregiver()
    {
        return $this->belongsTo(\App\Entities\Caregiver\Caregiver::class,'caregiver_id','id');
    }

    public function service()
    {
        return $this->belongsTo(\App\Entities\Masters\Service::class,'service_required','id');
    }

    public function serviceRequest()
    {
        return $this->belongsTo(CaseServices::class,'service_request_id','id');
    }

    public function serviceOrder()
    {
        return $this->belongsTo(CaseServiceOrder::class,'service_order_id','id');
    }

    public function billables()
    {
        return $this->hasMany(CaseBillables::class,'lead_id','lead_id');
    }
    
    public function invoice()
    {
        return $this->hasMany(InvoiceItems::class,'item_id','id')->whereCategory('Schedule');
    }

    public function worklog()
    {
        return $this->hasMany(WorkLog::class,'schedule_id','id');
    }

    public static function getFirstScheduleDate($scheduleID)
    {
        $schedule = Schedule::whereServiceRequestId($scheduleID)->whereStatus('Completed')->orderBy('id','Asc')->first();
        if($schedule){
            return $schedule->schedule_date->format('d-m-Y');
        }
        return '-';
    }

    public static function getLastScheduleDate($scheduleID)
    {
        $schedule = Schedule::whereServiceRequestId($scheduleID)->whereStatus('Completed')->orderBy('id','Desc')->first();
        if($schedule){
            return $schedule->schedule_date->format('d-m-Y');
        }
        return '-';
    }

    public function getLastInvoiceDate($scheduleID)
    {
        $invoice = InvoiceItems::whereScheduleId($scheduleID)->orderBy('id','Desc')->first();
        if($invoice){
            return $invoice->created_at->format('d-m-Y');
        }
        return '-';
    }

    public function ratecard()
    {
        return $this->belongsTo(\App\Entities\Masters\RateCard::class,'ratecard_id','id');
    }
}
