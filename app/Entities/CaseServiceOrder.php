<?php

namespace App\Entities;

use App\Entities\AuditableModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class CaseServiceOrder extends AuditableModel
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'case_service_orders';

    protected $fillable = ['tenant_id','branch_id','user_id','lead_id','patient_id','service_request_id','gross_rate','tax_rate','from_date','to_date','custom_dates','payment_status'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['from_date','to_date','deleted_at'];

    public function getPeriodAttribute($value)
    {
        $fromDate = date_format(new \DateTime($this->attributes['from_date']),'d-m-Y');
        $toDate = date_format(new \DateTime($this->attributes['to_date']),'d-m-Y');

        return $fromDate.' to '.$toDate;
    }

    public function scopePending($query)
    {
        return $query->wherePaymentStatus('Pending');
    }

    public function lead()
    {
        return $this->belongsTo(Lead::class,'lead_id','id');
    }

    public function patient()
    {
        return $this->belongsTo(Patient::class,'patient_id','id');
    }

    public function serviceRequest()
    {
        return $this->belongsTo(CaseServices::class,'service_request_id','id');
    }

    public function invoiceItems()
    {
        return $this->belongsTo(invoiceItems::class,'service_order_id','id');
    }

    public function schedules()
    {
        return $this->hasMany(Schedule::class,'service_order_id','id');
    }

    public function scheduleCount($status)
    {
        return $this->schedules()->where('status',$status)->count();
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function getStatusUnbilledProformaAttribute($value)
    {
        $unbilled = false;
        $ids = \DB::table('case_invoice_items')->select('item_id')->wherePatientId($this->patient_id)->whereCategory('Schedule')
                ->distinct('item_id')->whereNotNull('item_id')->whereNull('deleted_at')
                ->pluck('item_id')->toArray();

        $schedules = Schedule::whereServiceOrderId($this->id)->whereIn('status',['Pending','Completed'])->whereChargeable(1)->whereNotIn('id', $ids)->count();

        if($schedules){
            $unbilled = true;
        }

        return $unbilled;
    }

    public function getStatusUnbilledServiceAttribute($value)
    {
        $unbilled = false;
        $ids = \DB::table('case_service_invoice_items')->select('item_id')->wherePatientId($this->patient_id)->whereCategory('Schedule')
                ->distinct('item_id')->whereNotNull('item_id')->whereNull('deleted_at')
                ->pluck('item_id')->toArray();

        $schedules = Schedule::whereServiceOrderId($this->id)->whereStatus('Completed')->whereChargeable(1)->whereNotIn('id', $ids)->count();

        if($schedules){
            $unbilled = true;
        }

        return $unbilled;
    }
}
