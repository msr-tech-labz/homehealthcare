<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class CaseForm extends Model
{
	use SoftDeletes;
	//use TenantTrait;

	protected $table = 'case_forms';

	protected $fillable = ['branch_id','tenant_id','lead_id','user_id','type','form_type','form_data','status','signature_file'];

	protected $guarded = 'id';
	public $timestamps = true;

	public function user()
	{
		return $this->belongsTo(User::class,'user_id','user_id');
	}
}
