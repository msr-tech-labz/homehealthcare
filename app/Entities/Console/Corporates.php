<?php

namespace App\Entities\Console;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Corporates extends Model
{
    use SoftDeletes;

    protected $connection = 'central';
    protected $table = 'corporates';

    protected $fillable = ['corporate_id','db_name','type','expiry','organization','status'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['expiry','deleted_at'];

    public function subscriber()
    {
        return $this->hasMany(Subscriber::class,'corp_id','id');
    }
}
