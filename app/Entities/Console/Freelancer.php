<?php

namespace App\Entities\Console;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\User;
use App\Entities\Profile;

class Freelancer extends Model
{
    use SoftDeletes;

    protected $connection = 'central';

    protected $fillable = [
        'physio_id',
        'user_type',
        'database_name',
        'status'
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function scopeActive($query)
    {
        return $query->whereStatus('Active');
    }

    public function profile()
    {
      return $this->belongsTo(\App\Entities\Profile::class,'tenant_id','id');
    }

    public static function getFreelancers()
    {
        $results = false;
        $name = $email = null;

        $freelancers = Freelancer::get();

        if(count($freelancers)){
            foreach ($freelancers as $f) {
                $name = $email = null;
                \App\Http\Controllers\Controller::setTenantConnection($s->id);
                $provider = Profile::first();
                if($provider){
                    $name = $provider->organization_name;
                    $email = $provider->email;
                }

                $results[] = (object) [
                    'id' => $s->id,
                    'physio_id' => $s->subscriber_id,
                    'user_type' => $s->user_type,
                    'status' => $s->status,
                    'created_at' => $s->created_at->format('d-m-Y h:i:s')
                ];

                \DB::purge('tenant');
            }
        }
        return $results;
    }
}
