<?php

namespace App\Entities\Console;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Controllers\Controller;
class SubscriberActiveUsers extends Model
{
    use SoftDeletes;

    protected $connection = 'central';

    protected $fillable = [
        'subscriber_id',
        'date',
        'users'
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['date','deleted_at'];
}
