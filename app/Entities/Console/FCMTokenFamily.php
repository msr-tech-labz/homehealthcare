<?php

namespace App\Entities\Console;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FCMTokenFamily extends Model
{
    use SoftDeletes;

    protected $table = 'fcm_tokens_family';

    protected $fillable = ['customer_id','token'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];    
}
