<?php

namespace App\Entities\Console;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class SubscriptionChangeLogs extends Model
{
    use SoftDeletes;

    protected $connection = 'central';
    
    protected $table = 'subscription_change_logs';

    protected $fillable = [
        'tenant_id',
        'user_id',
        'users_limit',
        'charge_per_user',
        'amount_userbase',
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
