<?php

namespace App\Entities\Console;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\User;
use App\Entities\Profile;
use App\Entities\Caregiver\Caregiver;

use App\Http\Controllers\Controller;
class Subscriber extends Model
{
    use SoftDeletes;

    protected $connection = 'central';

    protected $fillable = [
        'corp_id',
        'user_type',
        'database_name',
        'org_code',
        'users_limit',
        'charge_per_user',
        'amount_userbase',
        'instance_type',
        'trial_end_date',
        'status',
        'active_users',
        'mail_status'
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['trial_end_date','deleted_at'];

    public function scopeActive($query)
    {
        return $query->whereStatus('Active');
    }
    
    public function scopeTrial($query)
    {
        return $query->whereInstanceType('Trial');
    }
    
    public function scopePaid($query)
    {
        return $query->whereInstanceType('Paid');
    }

    public function scopeOrganization($query)
    {
        return $query->whereIn('user_type',['Provider','Aggregator']);
    }

    public function scopeProfessional($query)
    {
        return $query->whereUserType('Professional');
    }

    public function provider()
    {
        return $this->belongsTo(\App\Entities\Profile::class,'tenant_id','id');
    }

    public static function getSubscribers()
    {
        $results = false;
        $name = $email = null;

        $subscribers = Subscriber::whereUserType('Provider')->get();

        if(count($subscribers)){
            foreach ($subscribers as $s) {
                $name = $email = null;
                Controller::setTenantConnection($s->id);
                $provider = Profile::first();
                if($provider){
                    $name = $provider->organization_name;
                    $email = $provider->email;
                }

                $results[] = (object) [
                    'id' => $s->id,
                    'subscriber_id' => $s->subscriber_id,
                    'user_type' => $s->user_type,
                    'name' => $name,
                    'email' => $email,
                    'status' => $s->status,
                    'created_at' => $s->created_at->format('d-m-Y h:i:s')
                ];

                \DB::purge('api');
            }
        }
        return $results;
    }

    public static function getSubscriberProfile($id)
    {
        if($id){
            $result = false;
            $name = $email = null;

            $s = Subscriber::find($id);
            if(count($s)){
               Controller::setTenantConnection($s->id);

                $name = $email = null;
                if($s->user_type == 'Professional'){
                    $profile = Caregiver::on('api')->withoutGlobalScopes()->whereTenantId($s->id)->first();
                }else{
                    $profile = Profile::on('api')->first();
                }
                $u = User::on('api')->withoutGlobalScopes()->whereTenantId($s->id)->first();

                if($profile){
                    $result['profile'] = $profile;
                    $result['subscription'] = (object) [
                        'tenant_id' => $s->id,
                        'subscriber_id' => $s->subscriber_id,
                        'user_type' => $s->user_type,
                        'subscription_status' => $s->status,
                        'subscription_date' => $s->created_at,
                        'database_name' => $s->database_name,
                        'admin_email' => isset($u)?$u->email:'',
                        'admin_name' => isset($u)?$u->full_name:'',
                        'users_limit' => isset($s)?$s->users_limit:'',
                        'charge_per_user' => isset($s)?$s->charge_per_user:'',
                        'amount_userbase' => isset($s)?$s->amount_userbase:'',
                        'instance_type' => isset($s)?$s->instance_type:'',
                        'trial_end_date' => isset($s)?$s->trial_end_date:'',
                    ];
                }
                \DB::purge('api');
            }

            return $result;
        }
    }
    
    public static function stats()
    {
        $stats = ['paid_accounts' => 0, 'paid_users' => 0, 'trial_accounts' => 0, 'trial_users' => 0];
        
        $subscribers = Subscriber::select('id','users_limit','instance_type')->whereUserType('Provider')->whereNotNull('org_code')->get();
        if($subscribers){
            $stats['paid_accounts'] = $subscribers->where('instance_type','Paid')->count();
            $stats['paid_users'] = $subscribers->where('instance_type','Paid')->sum('users_limit');
            $stats['trial_accounts'] = $subscribers->where('instance_type','Trial')->count();
            $stats['trial_users'] = $subscribers->where('instance_type','Trial')->sum('users_limit');
        }
        return $stats;
    }
    
    public static function getAccounts($type=null, $take=0)
    {
        $accounts = [];
        $subscribers = Subscriber::select('id','users_limit','trial_end_date')->whereUserType('Provider')->whereNotNull('org_code');
        
        if($type != null)
            $subscribers->whereInstanceType($type);
            
        if($take != 0)
            $subscribers->orderBy('users_limit','Desc')->take(5);
            
        $subscribers = $subscribers->get();
       
        if(count($subscribers)){
            foreach ($subscribers as $s) {
                Controller::setTenantConnection($s->id);
                $profile = Profile::on('api')->first();                
                
                if($type == 'Paid'){
                    $accounts[] = [
                        'account_name' => $profile->organization_name,
                        'licenses' => $s->users_limit?$s->users_limit:0
                    ];
                }

                if($type == 'Trial'){
                    $daysRemaining = \Carbon\Carbon::now('Asia/Kolkata')->diffInDays($s->trial_end_date,false);               
                    $accounts[] = [
                        'id' => $s->id,
                        'account_name' => $profile->organization_name,
                        'trial_end_date' => isset($s->trial_end_date)?$s->trial_end_date->format('d-m-Y'):'',
                        'days_remaining' => $daysRemaining,
                        'licenses' => $s->users_limit?$s->users_limit:0
                    ];
                }
                
                //\DB::purge('api');
            }
        }
        return $accounts;
    }

    public function corporate()
    {
        return $this->belongsTo(Corporates::class,'corp_id','id');
    }
}
