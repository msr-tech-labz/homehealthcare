<?php

namespace App\Entities\Console;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CCAvenueTransactionLog extends Model
{
    protected $table = 'ccavenue_transactions_log';

    protected $connection = 'central';

    protected $fillable = [
        'tenant_id',
        'invoice_id',
        'transaction_timestamp',
        'ip_address',
        'user_agent',
        'order_id',
        'tracking_id',
        'request_params',
        'response_params',
        'response_timestamp',
        'payment_mode',
        'amount',
        'status'
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['transaction_timestamp','response_timestamp'];
}
