<?php

namespace App\Entities\Console;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Http\Controllers\Controller;

class SubscriptionInvoice extends Model
{
    use SoftDeletes;

    protected $connection = 'central';
    protected $table = 'subscription_invoice';
    
    protected $fillable = [
        'id','corp_id','subscriber_id','usage_duration','invoice_no','invoice_month','invoice_year','invoice_amount','amount_paid','status'
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function getStatusAttribute($attribute){
        return [
            'Pending' => '#eab3b3',
            'Partial' => '#fbba44',
            'Paid' => '#9df188',
        ][$attribute];
    }

    public function organization()
    {
        return $this->belongsTo(Corporates::class,'corp_id','id');
    }

    public function subcriber()
    {
        return $this->belongsTo(Subscriber::class,'subscriber_id','id');
    }
}
