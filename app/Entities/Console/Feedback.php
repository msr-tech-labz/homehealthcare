<?php

namespace App\Entities\Console;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\User;
use App\Entities\Profile;

class Feedback extends Model
{
    use SoftDeletes;

    protected $table = 'feedbacks';

    protected $connection = 'central';

    protected $fillable = [
        'tenant_id',
        'type',
        'module',
        'message',
        'document_path',
        'user_id',
        'status'
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function scopeActive($query)
    {
        return $query->whereStatus('Active');
    }
}
