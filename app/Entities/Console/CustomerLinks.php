<?php

namespace App\Entities\Console;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerLinks extends Model
{
    use SoftDeletes;

    protected $connection = 'central';
    protected $table = 'customer_links';

    protected $fillable = ['tenant_id','org_hash','patient_id','customer_id'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
