<?php

namespace App\Entities\Console;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Http\Controllers\Controller;

class SubscriptionInvoicePayments extends Model
{
    use SoftDeletes;

    protected $connection = 'central';
    protected $table = 'subscription_invoice_payments';
    
    protected $fillable = [
        'tenant_id',
        'subscription_invoice_id',
        'transaction_id',
        'merchant_txn_id',
        'transaction_date',
        'total_amount',
        'pg_response',
        'status'
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function invoiceGenerated()
    {
        return $this->belongsTo(SubscriptionInvoice::class, 'subscription_invoice_id','id');
    }

}
