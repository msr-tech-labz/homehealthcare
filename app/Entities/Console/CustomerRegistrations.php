<?php

namespace App\Entities\Console;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerRegistrations extends Model
{
    use SoftDeletes;

    protected $connection = 'central';
    protected $table = 'customer_registrations';

    protected $fillable = ['full_name','email','password','mobile_number','profile_image'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function customerLinks()
    {
        return $this->belongsTo(CustomerLinks::class,'id','customer_id');
    }
}
