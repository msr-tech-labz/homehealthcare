<?php

namespace App\Entities;

use App\Entities\AuditableModel;

class SOAMailers extends AuditableModel
{
    protected $table = 'soa_mailers';
    protected $fillable = ['job_id','patient_id','from_date','to_date','payload','status','user_id'];

    protected $guarded = 'id';
    public $dates = ['from_date','to_date'];
    public $timestamps = true;

    public function patient()
    {
        return $this->belongsTo(Patient::class,'patient_id','id');
    }
    
    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
}
