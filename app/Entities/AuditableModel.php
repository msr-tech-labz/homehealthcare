<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class AuditableModel extends Model implements AuditableContract
{
    use Auditable;

    public function user()
    {
        return $this->belongsTo(\Config::get('audit.user.model'),'user_id');
    }
}
