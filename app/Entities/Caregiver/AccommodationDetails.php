<?php

namespace App\Entities\Caregiver;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class AccommodationDetails extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'caregiver_accommodation_details';

    protected $fillable = ['tenant_id','caregiver_id', 'arranged_by', 'accommodation_address', 'accommodation_city', 'accommodation_state', 'accommodation_from_date', 'accommodation_to_date'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['accommodation_from_date','accommodation_to_date','deleted_at'];

    public function caregiver()
    {
        return $this->belongsTo(Caregiver::class, 'caregiver_id');
    }
}
