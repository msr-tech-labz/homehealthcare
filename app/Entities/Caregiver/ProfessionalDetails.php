<?php

namespace App\Entities\Caregiver;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class ProfessionalDetails extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'caregiver_professional_details';

    protected $fillable = ['tenant_id','caregiver_id', 'branch_id', 'specialization', 'qualification', 'college_name', 'experience', 'experience_level', 'languages_known', 'workdays', 'achievements','working_hours_from','working_hours_to', 'role', 'deployable', 'employment_type', 'timings', 'designation', 'department', 'manager','source','source_name','employment_type','date_of_joining','resignation_date','resignation_type','resignation_reason','uniform_issued','warning','warning_date_one','warning_date_two','warning_date_three'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['date_of_joining','resignation_date','warning_date_one','warning_date_two','warning_date_three','deleted_at'];

    public function caregiver()
    {
        return $this->belongsTo(Caregiver::class, 'caregiver_id');
    }

    public function reportingmanager()
    {
        return $this->belongsTo(Caregiver::class, 'manager','id');
    }

    public function branch()
    {
        return $this->belongsTo(\App\Entities\Masters\Branch::class, 'branch_id');
    }

    public function specializations()
    {
        return $this->belongsTo(\App\Entities\Masters\Specialization::class,'specialization');
    }

    public function designations()
    {
        return $this->belongsTo(\App\Entities\Masters\Designation::class, 'designation');
    }

    public function departments()
    {
        return $this->belongsTo(\App\Entities\Masters\Department::class, 'department');
    }
}
