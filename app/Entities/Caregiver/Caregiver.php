<?php

namespace App\Entities\Caregiver;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Caregiver extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'caregivers';

    protected $fillable = ['tenant_id','branch_id','employee_id','first_name','middle_name','last_name','date_of_birth','gender','blood_group','marital_status','religion','food_habits','gender_preference','mobile_number','alternative_number','email','personal_email','current_address','current_area','current_city','current_zipcode','current_state','current_country','latitude','longitude','permanent_address','permanent_area','permanent_city','permanent_zipcode','permanent_state','permanent_country','prof_folder','prof_verification','profile_image','work_status','availability_status','imported'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['date_of_birth','deleted_at'];

    public function getFullNameAttribute($value)
    {
        return $this->attributes['first_name'].' '.$this->attributes['middle_name'].' '.$this->attributes['last_name'];
    }

    public function scopeActive($query)
    {
        return $query->whereHas('user', function($q){ $q->withoutGlobalScopes()->whereStatus(1);});
    }

    public function scopeBranch($query)
    {
        if(\Helper::encryptor('decrypt',session('utype')) != 'Admin')
        return $query->whereBranchId(\App\Helpers\Helper::getSession('branch_id'));
    }

    public function professional()
    {
        return $this->hasOne(ProfessionalDetails::class, 'caregiver_id');
    }

    public function payroll()
    {
        return $this->hasOne(PayrollDetails::class, 'caregiver_id');
    }

    public function account()
    {
        return $this->hasOne(AccountDetails::class, 'caregiver_id');
    }

    public function accommodation()
    {
        return $this->hasOne(AccommodationDetails::class, 'caregiver_id');
    }

    public function employments()
    {
        return $this->hasMany(EmploymentHistory::class, 'caregiver_id');
    }

    public function verifications()
    {
        return $this->hasMany(Verification::class, 'caregiver_id','id');
    }

    public function caregiver_skills()
    {
        return $this->belongsToMany(CaregiverSkill::class,'caregiver_skills','caregiver_id','skill_id')
                    ->withTimestamps();
    }

    public function caregiver_skills_filter()
    {
        return $this->hasMany(CaregiverSkill::class,'caregiver_id','id');
    }

    public function schedule()
    {
        return $this->belongsTo(\App\Entities\Schedule::class,'id','caregiver_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Entities\User::class,'id','user_id');
    }

    public function attendance()
    {
        return $this->hasMany(\App\Entities\Attendance::class,'caregiver_id','id');
    }

    public function provider()
    {
    return $this->belongsTo(\App\Entities\Profile::class,'tenant_id','tenant_id');
    }

    public function branchName()
    {
        return $this->belongsTo(\App\Entities\Masters\Branch::class,'branch_id','id');
    }
    public function leave()
    {
        return $this->hasMany(\App\Entities\Leaves::class,'caregiver_id','id');
    }

    public function appraisal()
    {
        return $this->hasMany(\App\Entities\Appraisal::class,'caregiver_id','id');
    }
}
