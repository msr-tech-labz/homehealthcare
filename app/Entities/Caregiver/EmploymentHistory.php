<?php

namespace App\Entities\Caregiver;

use Illuminate\Database\Eloquent\Model;
use App\Traits\TenantTrait;

class EmploymentHistory extends Model
{
    use TenantTrait;

    protected $table = 'caregiver_employment_history';

    protected $fillable = ['tenant_id','caregiver_id', 'company_name', 'period'];

    protected $guarded = 'id';
    public $timestamps = true;

    public function caregiver()
    {
        return $this->belongsTo(Caregiver::class, 'caregiver_id');
    }
}
