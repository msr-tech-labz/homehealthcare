<?php

namespace App\Entities\Caregiver;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class PayrollDetails extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'caregiver_payroll_details';

    protected $fillable = ['tenant_id','employee_id', 'basic_salary', 'hra', 'conveyance', 'food_and_accommodation', 'medical_allowance', 'lta', 'other_allowances', 'stipend', 'pf_deduction', 'esi_deduction', 'other_deduction', 'effective_date_of_salary'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function employee()
    {
        return $this->belongsTo(Caregiver::class, 'employee_id');
    }
}
