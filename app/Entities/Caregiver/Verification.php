<?php

namespace App\Entities\Caregiver;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;


class Verification extends Model
{
	// use SoftDeletes;
	use TenantTrait;

	protected $table = 'caregiver_verifications';

	protected $fillable = [
		'tenant_id',
		'caregiver_id',
		'doc_type',
		'doc_name',
		'description',
		'doc_path',
		'doc_verification_status',
		'doc_verification_comment'
	];

	public $timestamps = true;

	protected $guarded = "id";
	// protected $dates = ["deleted_at"];

	public function caregiver()
	{
		return $this->belongsTo(Caregiver::class, 'caregiver_id','id');
	}
}
