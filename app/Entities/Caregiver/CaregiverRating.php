<?php

namespace App\Entities\Caregiver;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class WorklogRating extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'caregiver_ratings';

    protected $fillable = ['tenant_id', 'caregiver_id', 'worklog_id', 'patient_id', 'comment', 'no_of_stars'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function caregiver()
    {
        return $this->belongsTo(Caregiver::class,'caregiver_id','id');
    }
}
