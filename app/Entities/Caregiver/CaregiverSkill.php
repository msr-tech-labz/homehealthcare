<?php

namespace App\Entities\Caregiver;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class CaregiverSkill extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $fillable = ['tenant_id','caregiver_id', 'skill_id', 'proficiency'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function skill(){
		return $this->hasOne(\App\Entities\Masters\Skill::class, 'id', 'skill_id');
	}

    public function skills()
    {
        return $this->belongsToMany(\App\Entities\Master\Skill::class,'caregiver_skills','caregiver_id','skill_id')
                    ->withTimestamps();
    }

    public function caregiver()
    {
        return $this->belongsToMany(Caregiver::class);
    }
}
