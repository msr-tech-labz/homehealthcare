<?php

namespace App\Entities\Caregiver;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class AccountDetails extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'caregiver_account_details';

    protected $fillable = ['tenant_id','caregiver_id', 'pan_number', 'aadhar_number', 'account_name', 'account_number', 'bank_name', 'bank_branch', 'ifsc_code'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function caregiver()
    {
        return $this->belongsTo(Caregiver::class, 'caregiver_id');
    }
}
