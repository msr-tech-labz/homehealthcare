<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class LeadDistributionHistory extends Model
{
    use SoftDeletes;

    protected $connection = 'central';

    protected $table = 'lead_distribution_history';

    protected $fillable = ['tenant_id','branch_name','patient_name','patient_number','email','city','requirement','status','status_date'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['status_date','deleted_at'];
}
