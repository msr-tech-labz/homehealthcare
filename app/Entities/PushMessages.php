<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class PushMessages extends Model
{
    protected $table = 'push_messages';

    protected $fillable = ['token','title','body','data','success_count','failure_count','modification_count'];

    protected $guarded = 'id';
    public $timestamps = true;
}
