<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeaveApproval extends Model
{
    use SoftDeletes;

    protected $table = 'employee_leave_approvals';

    protected $fillable = ['tenant_id','leave_id','approver_id','status','comment'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function leave()
    {
        return $this->belongsTo(Leaves::class,'leave_id','id');
    }

    public function approver()
    {
        return $this->belongsTo(\App\Entities\Caregiver\Caregiver::class,'approver_id','id');
    }
}
