<?php

namespace App\Entities;

use App\Entities\AuditableModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Invoice extends AuditableModel
{
    use SoftDeletes, TenantTrait;

    protected $table = 'case_invoices';

    protected $fillable = ['tenant_id','branch_id','user_id','lead_id','patient_id','invoice_no','invoice_date','invoice_due_date','taxable_amount','less_amount','taxed_amount','total_amount','amount_paid','email_invoice','status','comment','cancel_comment'];

    protected $guarded = 'id';
    public $dates = ['invoice_date','invoice_due_date','email_invoice','deleted_at'];
    public $timestamps = true;

    public function scopeOfStatus($query, $status)
    {
        return $query->whereStatus($status);
    }

    public function lead()
    {
        return $this->belongsTo(Lead::class,'lead_id','id');
    }

    public function patient()
    {
        return $this->belongsTo(Patient::class,'patient_id','id');
    }

    public function items()
    {
        return $this->hasMany(InvoiceItems::class,'invoice_id','id');
    }

    public function payment()
    {
        return $this->belongsTo(CasePayment::class,'id','invoice_id');
    }

    //Used only in invoice reports in Financial.php
    public function payments()
    {
        return $this->hasMany(CasePayment::class,'invoice_id','id');
    }

    public function receipts()
    {
        return $this->hasMany(Receipt::class,'item_id','id')->where('receipt_type','Payment');
    }

    public function getItemsArray()
    {
        $result = [];
        $items = InvoiceItems::whereInvoiceId($this->attributes['id'])->get();
        if(count($items)){
            // Get Advance category
            $advances = $items->where('category','Schedule')->where('type','Advance');
            if(count($advances)){
                foreach ($advances as $key => $advance) {
                    $result[] = (object) [
                        'description' => 'Advance - '.$advance->serviceRequest->service->service_name,
                        'period' => '-',
                        'duration' => $advance->duration,
                        'rate' => $advance->rate,
                        'discount' => $advance->discount_amount,
                        'tax_percentage' => $advance->tax_slab,
                        'tax_amount' => $advance->tax_amount,
                    ];
                }
            }

            // Get Schedule Category - Unbilled and Completed
            $result = $this->getServiceCategoryItems($items, 'Unbilled', $result);
            $result = $this->getServiceCategoryItems($items, 'Completed', $result);

            // Get Billables category
            $billables = $items->where('category','Billable');
            if(count($billables)){
                foreach ($billables as $key => $billable) {
                    $result[] = (object) [
                        'description' => \Helper::getBillableName($billable->item_id),
                        'period' => '-',
                        'duration' => $billable->duration,
                        'rate' => $billable->rate,
                        'discount' => $billable->discount_amount,
                        'tax_percentage' => $billable->tax_slab,
                        'tax_amount' => $billable->tax_amount,
                    ];
                }
            }

            // Get Custom category
            $customs = $items->where('category','Custom');
            if(count($customs)){
                foreach ($customs as $key => $custom) {
                    $result[] = (object) [
                        'description' => $custom->custom_description,
                        'period' => $custom->custom_period,
                        'duration' => $custom->duration,
                        'rate' => $custom->rate,
                        'discount' => $custom->discount_amount,
                        'tax_percentage' => $custom->tax_slab,
                        'tax_amount' => $custom->tax_amount,
                    ];
                }
            }
        }

        return $result;
    }

    public function getServiceCategoryItems($items, $type, $result)
    {
        $itemCategory = $items->where('category','Schedule')->where('type',$type);
        if(count($itemCategory)){
            $scheduleIDs = $itemCategory->pluck('item_id');
            $schedules = Schedule::whereIn('id',$scheduleIDs->toArray())->orderBy('schedule_date','Asc')->get();
            foreach ($schedules as $schedule) {
                $schedule->gross_rate = $schedule->serviceOrder->gross_rate;
            }
            $grouped = $schedules->groupBy('gross_rate')->transform(function($item, $k) {
                            return $item->groupBy('ratecard_id');
                        });
            if(count($grouped)){
                foreach ($grouped as $keys => $group) {
                    foreach ($group as $key => $subgroup) {
                        $schedule_ids = $subgroup->unique('id')->pluck('id');
                        $serviceOrder_id = $subgroup->unique('service_order_id')->pluck('service_order_id');
                        // $taxId = \App\Entities\Masters\RateCard::whereId($key)->value('tax_id');
                        $taxId = \App\Entities\CaseServiceOrder::whereId($serviceOrder_id)->value('tax_rate');
                        // $rate = \App\Entities\Masters\RateCard::whereId($key)->value('amount');
                        $rate = \App\Entities\CaseServiceOrder::whereId($serviceOrder_id)->value('gross_rate');
                        $duration = count($subgroup);
                        $invoiceItemRate = $itemCategory->whereIn('item_id',$subgroup->pluck('id'))->sum('rate');
                        $invoiceItemDiscount = $itemCategory->whereIn('item_id',$subgroup->pluck('id'))->sum('discount_amount');
                        $taxRate = \App\Entities\Masters\Taxrate::whereId($taxId)->value('tax_rate');
                        $taxAmount =  $itemCategory->whereIn('item_id',$subgroup->pluck('id'))->sum('tax_amount');

                        $result[] = (object) [
                            'description' => \Helper::getServiceName($subgroup->pluck('service_required')->unique()->first()),
                            'hsn' => \Helper::getServiceHsn($subgroup->pluck('service_required')->unique()->first()),
                            'period' => $subgroup->min('schedule_date')->format('d-m-Y').' - '.$subgroup->max('schedule_date')->format('d-m-Y'),
                            'duration' => $duration,
                            'rate' => $rate,
                            'discount' => ($rate * $duration) - ( $invoiceItemRate - $invoiceItemDiscount ),
                            'tax_percentage' => $taxRate,
                            'tax_amount' => $taxAmount
                        ];
                    }
                }
            }
        }
        return $result;
    }
}
