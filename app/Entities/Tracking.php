<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Tracking extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'caregiver_tracking';

    protected $fillable = ['tenant_id','caregiver_id','lat','lng','status','server_timestamp','app_timestamp'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function caregiver()
    {
        return $this->belongsTo(\App\Entities\Caregiver\Caregiver::class);
    }
}
