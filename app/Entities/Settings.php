<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Settings extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'settings';
    protected $fillable = ['tenant_id','auth_signatory','signtory_pos','proforma_invoice_inits','proforma_invoice_start_no','invoice_inits','invoice_start_no','receipt_inits','receipt_start_no','credit_inits','credit_start_no','cutoff_period','patient_id_prefix','patient_id_start_no','employee_id_prefix','invoice_payee','bank_name','bank_address','acc_num','ifsc_code','pan_number','gstin','cin','payment_due_date','schedule_creation_for_outstanding_customer','patient_email_mandatory','auto_complete_schedules'];

    protected $guarded = 'id';
    public $timestamps = true;
}
