<?php
namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class LoggingEvents extends Model
{
	use SoftDeletes;
    protected $table = 'logging_events';

    protected $fillable = ['user_id','user_agent','ip_address','login_time','logout_time','login_status'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    public function users()
    {
    	return $this->belongsTo(\App\Entities\User::class,'user_id','id');
    }
}
