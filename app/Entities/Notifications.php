<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notifications extends Model
{
	use SoftDeletes;

    protected $table = 'notifications';
    protected $fillable = ['message','status'];

    protected $guarded = 'id';
    public $timestamps = true;
    public $dates = ['deleted_at'];
}
