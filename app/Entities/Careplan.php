<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Careplan extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'case_records';

    protected $fillable = ['tenant_id','lead_id','branch_id','crn_number','patient_id','user_id','careplan_name','careplan_description','medical_conditions','medications','service_id','no_of_hours','case_charges','gender_preference','language_preference','source','referrer_name','apnacare_lead','provider_notes','status'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function scopeActive($query)
    {
        return $query->whereNull('deleted_at');
    }

    public function scopeAggregator($query)
    {
        return $query->whereApnacareLead(1);
    }

    public function branch()
    {
        return $this->belongsTo(\App\Entities\Masters\Branch::class);
    }

    public function provider()
    {
        return $this->belongsTo(Provider::class,'tenant_id','tenant_id');
    }

    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }

    public function service()
    {
        return $this->belongsTo(\App\Entities\Masters\Service::class);
    }

    public function schedules()
    {
        return $this->hasMany(Schedule::class,'careplan_id','id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class,'careplan_id','id');
    }

    public function disposition()
    {
        return $this->hasMany(CaseDisposition::class,'careplan_id','id');
    }

    public function manager()
    {
        return $this->hasOne(User::class,'id','user_id');
    }
    public function comment()
    {
        return $this->hasMany(Comment::class,'careplan_id','id');
    }

    public function assessment()
    {
        return $this->hasMany(CaseForm::class,'careplan_id','id');
    }

    // public function labtests()
    // {
    //     return $this->hasMany(LabTest::class,'careplan_id','id');
    // }

    public function worklog()
    {
        return $this->hasMany(WorkLog::class,'careplan_id','id');
    }

    public function charges()
    {
        return $this->belongsTo(CaseCharges::class,'id','careplan_id');
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class,'careplan_id','id');
    }

    public function receipt()
    {
        return $this->belongsTo(Receipt::class,'id','careplan_id');
    }

    public static function getAggregatorCases($status=null, $viewType=null)
    {
        $providerID = \Helper::encryptor('decrypt',session('uid'));
        $results = [];
        $provider = Profile::withoutGlobalScopes()->get(); //->where('id','!=',$providerID)->get();
        if($provider){
            foreach ($provider as $p) {
                if($p->tenant_id == 90)
                {
                $careplans = Careplan::withoutGlobalScopes()
                            ->whereIn('tenant_id',[0,$p->tenant_id])
                            ->whereApnacareLead(1);
                }
                else{
                $careplans = Careplan::withoutGlobalScopes()
                            ->whereTenantId($p->tenant_id)
                            ->whereApnacareLead(1);
                }
                if($status)
                    $careplans->whereStatus($status);

                if($viewType){
                    if($viewType == 'self')
                        $careplans->whereUserId(\Helper::encryptor('decrypt',session('user_id')));
                    else
                        $careplans->whereNotIn('user_id',[\Helper::encryptor('decrypt',session('user_id'))]);
                }

                $careplans = $careplans->get();

                if($careplans->count()){
                    foreach ($careplans as $c) {
                        $c['provider_id'] = $p->id;
                        $c['provider_name'] = $p->organization_name;

                        $patient = Patient::withoutGlobalScopes()->whereId($c->patient_id)->first();
                        if($c->tenant_id == 0){
                            $results[] = (object) [
                            'careplan' => $c,
                            'patient' => $patient,
                            'provider_id' => 0,
                            'provider_name' => '-',
                            'provider_short_name' => '-',
                            ];
                        }
                        if($c->tenant_id != 0){
                            $results[] = (object) [
                            'careplan' => $c,
                            'patient' => $patient,
                            'provider_id' => $p->tenant_id,
                            'provider_name' => $p->organization_name,
                            'provider_short_name' => $p->organization_short_name,
                            ];
                        }

                    }
                }
            }
        }

        return $results;
    }

    public static function getPendingInvoicesOld()
    {
        // 1. Case Status should be either Started or Completed
        // 2. No invoice has been generated in last 'x' days

        $cases = Careplan::withoutGlobalScopes()->whereTenantId(\Helper::getTenantID())
                    ->whereIn('status',['Service Started', 'Service Completed'])
                    ->get();
                    // ->whereRaw(\DB::raw('id NOT IN (Select careplan_id From case_invoices Where DATE_SUB(NOW() ,INTERVAL 10 DAY) < invoice_date Group By careplan_id Order By id Desc)'))->get();

        //dd($cases[2]);

        return $cases;
    }

    public static function getPendingInvoices()
    {
        // 1. Case Status should be either Started or Completed
        // 2. No invoice has been generated in last 'x' days

        $cases = Schedule::whereIn('status',['Started', 'Completed'])
                          ->whereHas('invoice', function($q){
                              $q->whereNull('deleted_at');
                            //   $q->whereDate('invoice_period_from','>=',date('Y-m-d'))
                            //     ->whereDate('invoice_period_to','<=',date('Y-m-d'))
                            //     ->whereNotIn('status',['Pending','Partial']);
                          })
                          ->get();

        //dd($cases[2]);

        return $cases;
    }
}
