<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Profile extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'profile';

    public $createdByColumn = false;

    protected $fillable = [
        'tenant_id',
        'organization_name',
        'organization_short_name',
        'organization_address',
        'organization_area',
        'organization_city',
        'organization_zipcode',
        'organization_state',
        'organization_country',
        'latitude',
        'longitude',
        'organization_logo',
        'phone_number',
        'landline_number',
        'email',
        'website',
        'contact_person'
    ];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function subscriber()
    {
        return $this->belongsTo(\App\Entities\Console\Subscriber::class,'tenant_id','id');
    }
}
