<?php

namespace App\Entities;

use App\Entities\AuditableModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class CasePayment extends AuditableModel
{
    use SoftDeletes, TenantTrait;

    protected $table = 'case_payments';
    protected $fillable = ['tenant_id','branch_id','user_id','invoice_id','patient_id','transaction_id','merchant_txn_id','bank_txn','bank_name','payment_mode','actual_amount','card_number','cheque_dd_no','cheque_dd_date','reference_no','taxes','invoice_amount','total_amount','pg_response','status'];

    protected $guarded = 'id';
    public $dates = ['deleted_at'];
    public $timestamps = true;

    public function patient()
    {
        return $this->belongsTo(Patient::class,'patient_id','id');
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class,'invoice_id','id');
    }
}
