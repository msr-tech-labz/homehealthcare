<?php

namespace App\Entities\Physio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class SubscriptionPlan extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'subscription_plans';

    protected $fillable = ['name_plan','description','price','validity','trial_period','commission_percentage'];


    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
