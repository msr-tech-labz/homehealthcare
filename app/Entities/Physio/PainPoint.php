<?php
namespace App\Entities\Physio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class PainPoint extends Model
{
    use SoftDeletes, TenantTrait;

    public $table = "physio_pain_points";

	protected $fillable = ['id','tenant_id','patient_id','posture','xyvalues'];
    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    // public function getCreatedAtAttribute($date)
    // {
    //     return $this->attributes['created_at'] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    // }
    //
    // public function getUpdatedAtAttribute($date)
    // {
    //     return $this->attributes['updated_at'] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    // } 
}
