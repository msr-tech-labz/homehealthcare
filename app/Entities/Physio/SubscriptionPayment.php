<?php

namespace App\Entities\Physio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class SubscriptionPayment extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'subscription_payments';

    protected $fillable = ['tenant_id','plan_id','transaction_date','transaction_id','amount_paid','credits_used','total_amount','status','payment_response'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
