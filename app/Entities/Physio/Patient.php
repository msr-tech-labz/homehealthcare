<?php

namespace App\Entities\Physio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Patient extends Model
{
    use SoftDeletes, TenantTrait;

    protected $fillable = ['tenant_id','first_name','patient_id','last_name','date_of_birth','patient_age','patient_weight','gender','contact_number','email','alternate_number','enquirer_name','relationship_with_patient','street_address','area','city','zipcode','state','country','latitude','longitude','photo_path','occupation','referred_by'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $hidden = ['deleted_at'];
    protected $dates = ['date_of_birth','deleted_at'];

    public function getFullNameAttribute($value)
    {
        return $this->attributes['first_name'].' '.$this->attributes['last_name'];
    }

    /*public function getCreatedAtAttribute($date)
    {
        return $this->attributes['created_at'] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }

    public function getUpdatedAtAttribute($date)
    {
        return $this->attributes['updated_at'] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }*/
}
