<?php

namespace App\Entities\Physio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class ReferralCode extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $connection = 'physio';
    protected $table = 'referral_codes';

    protected $fillable = ['tenant_id','referral_type','referral_code','referrer_id','referrer_id_type','subscription_credits_used','status'];

    
    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
