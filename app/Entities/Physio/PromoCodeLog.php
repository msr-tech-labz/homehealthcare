<?php
namespace App\Entities\Physio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class PromoCodeLog extends Model
{
    use SoftDeletes, TenantTrait;

    public $table = "promo_code_logs";

	protected $fillable = ['tenant_id','promo_code_id','date_of_use'];
    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
