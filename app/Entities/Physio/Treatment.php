<?php

namespace App\Entities\Physio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Treatment extends Model
{
	use SoftDeletes, TenantTrait;

	public $table = "physio_treatments";

	protected $fillable = ['id','tenant_id','patient_id','lead_id','user_id','treatmentDiagnosed','treatmentDetails','treatmentDate','status'];

	protected $guarded = 'id';
	public $timestamps = true;
	protected $dates = ['deleted_at'];

	// public function getCreatedAtAttribute($date)
 //    {
 //        return $this->attributes['created_at'] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
 //    }

 //    public function getUpdatedAtAttribute($date)
 //    {
 //        return $this->attributes['updated_at'] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
 //    }
	
	public function caregiver()
	{
	    return $this->belongsTo(\App\Entities\Caregiver\Caregiver::class,'caregiver_id','id');
	}

	public function lead()
	{
	    return $this->belongsTo(Lead::class,'id','lead_id');
	}

	public function schedule()
	{
	    return $this->belongsTo(Schedule::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class,'user_id','user_id');
	}
}
