<?php

namespace App\Entities\Physio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Assessment extends Model
{
	use SoftDeletes, TenantTrait;

	protected $table = 'physio_assessments';

	protected $fillable = ['tenant_id','patient_id','appointment_id','visit_date','user_id','form_type','form_data'];

	protected $guarded = 'id';
	public $timestamps = true;
	public $hidden = ['created_at','updated_at','deleted_at'];

	public function patient()
	{
		return $this->belongsTo(\App\Entities\Patient::class,'user_id','user_id');
	}

	// public function getCreatedAtAttribute($date)
 //    {
 //        return $this->attributes['created_at'] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
 //    }

 //    public function getUpdatedAtAttribute($date)
 //    {
 //        return $this->attributes['updated_at'] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
 //    } 
}
