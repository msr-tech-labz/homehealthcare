<?php
namespace App\Entities\Physio;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class ChannelPartner extends Authenticatable
{
    use SoftDeletes, TenantTrait;

    protected $connection = 'physio';
    public $table = "channel_partners";

	protected $fillable = ['id','partner_name','contact_number','alternate_number','email','password','postal_address','pan_number','aadhaar_number','bank_name','account_number','branch_name','ifsc_code','commission_type','commission_value'];
    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
