<?php
namespace App\Entities\Physio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class DoctorReport extends Model
{
    use SoftDeletes, TenantTrait;

    public $table = "physio_doctor_reports";

	protected $fillable = ['id','tenant_id','patient_id','report_name','file_path'];
    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
