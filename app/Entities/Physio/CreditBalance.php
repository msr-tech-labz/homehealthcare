<?php
namespace App\Entities\Physio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class CreditBalance extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $connection = 'physio';
    protected $table = 'credit_balance';

    protected $fillable = ['tenant_id','tenant_type','credits'];


    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
