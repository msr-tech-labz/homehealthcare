<?php
namespace App\Entities\Physio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class PromoCode extends Model
{
    use SoftDeletes, TenantTrait;

    public $table = "promo_codes";

	protected $fillable = ['code','giveaway_credts','description','expiry'];
    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
