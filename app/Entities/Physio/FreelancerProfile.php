<?php

namespace App\Entities\Physio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class FreelancerProfile extends Model
{
    use SoftDeletes;
    use TenantTrait;
    
    protected $connection = 'physio';
    protected $table = 'physio_profiles';

    protected $fillable = ['tenant_id','first_name','last_name','date_of_birth','gender','mobile_number','alternative_number','email','address','area','city','state','country','profile_image','specialization','working_days','working_hours','referrer_code','expiry_date','password','remember_token'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['date_of_birth','deleted_at'];


    public function getFullNameAttribute($value)
    {
        return $this->attributes['first_name'].' '.$this->attributes['last_name'];
    }

    // public function getCreatedAtAttribute($date)
    // {
    //     return $this->attributes['created_at'] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    // }
    // 
    // public function getUpdatedAtAttribute($date)
    // {
    //     return $this->attributes['updated_at'] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    // }

    public function freelancer()
    {
        return $this->belongsTo(\App\Entities\Console\Freelancer::class,'tenant_id','id');
    }
}
