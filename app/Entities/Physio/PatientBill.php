<?php

namespace App\Entities\Physio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class PatientBill extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'physio_billings';

    protected $fillable = ['tenant_id','patient_id','appointment_id','amount','invoice_number','bill_date','status','sync_to_server'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
