<?php

namespace App\Entities\Physio;

use App\Entities\AuditableModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Appointment extends AuditableModel
{
    use SoftDeletes, TenantTrait;

    protected $table = 'physio_appointments';

    protected $fillable = ['tenant_id','patient_id','appointment_date','from_time','to_time','notes','billing_status','status'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    // public function getCreatedAtAttribute($date)
    // {
    //     return $this->attributes['created_at'] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    // }

    // public function getUpdatedAtAttribute($date)
    // {
    //     return $this->attributes['updated_at'] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    // }
    
    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }
}
