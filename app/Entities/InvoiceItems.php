<?php

namespace App\Entities;

use App\Entities\AuditableModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class InvoiceItems extends AuditableModel
{
    use SoftDeletes, TenantTrait;

    protected $table = 'case_invoice_items';
    protected $fillable = ['tenant_id','branch_id','patient_id','invoice_id','category','type','item_id','tax_slab','tax_amount','rate','duration','total_amount','amount_paid','status','user_id'];

    protected $guarded = 'id';
    public $dates = ['deleted_at'];
    public $timestamps = true;

    public function invoice()
    {
        return $this->belongsTo(Invoice::class,'invoice_id','id');
    }

    public function itemInvoice()
    {
        return $this->belongsTo(Invoice::class,'item_id','id');
    }

    public function patient()
    {
        return $this->belongsTo(Patient::class,'patient_id','id');
    }

    public function serviceRequest()
    {
        return $this->belongsTo(CaseServices::class,'item_id','id');
    }

    public function schedule()
    {
        return $this->belongsTo(Schedule::class,'item_id','id');
    }

    public function creditMemo()
    {
        return $this->belongsTo(CreditMemo::class,'item_id','id');
    }
}
