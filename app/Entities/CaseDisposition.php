<?php

namespace App\Entities;

use App\Entities\AuditableModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class CaseDisposition extends AuditableModel
{
    use SoftDeletes;
    use TenantTrait;

    protected $fillable = ['tenant_id','lead_id','user_id','status','disposition_date','comment','caselosscomment','follow_up_datetime','follow_up_comment','converted'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['disposition_date','deleted_at','follow_up_datetime'];    

    public function lead()
    {
        return $this->hasMany(Lead::class,'id','lead_id');
    }
    
    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
}
