<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class CaseRatecard extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'case_ratecards';

    protected $fillable = ['tenant_id','case_id','service_id','rate','discount_type','discount_applicable','discount_value','period_from','period_to','total_amount','status'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['period_from','period_to','deleted_at'];

    public function scopeActive($query)
    {
        return $query->whereStatus(1);
    }

    public function caserecord()
    {
        return $this->belongsTo(Careplan::class,'id','lead_id');
    }

    public function service()
    {
        return $this->belongsTo(\App\Entities\Masters\Service::class,'service_id','id');
    }
}
