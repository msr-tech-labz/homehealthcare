<?php

namespace App\Entities;

use App\Entities\AuditableModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;
use App\Scopes\CaregiverScope;

class Lead extends AuditableModel
{
    use SoftDeletes;
    use TenantTrait;

    protected $fillable =
    ['tenant_id','branch_id','user_id','patient_id','episode_id','case_description','case_type','medical_conditions','medications','procedures','hospital_name','primary_doctor_name','special_instructions','service_category','service_required','service_offered','manager_id','current_rack_rate','estimated_duration','gender_preference','language_preference','assessment_done','assessment_date','assessment_time','assessment_notes','rate_agreed','registration_amount','payment_mode','payment_notes','referral_category','referral_source','referrer_name','converted_by','manager_id','referral_value','referral_type','status','dropped_reason','dropped_reason_other','aggregator_lead','aggregator_lead_request_id','aggregator_manager_id','aggregator_service','aggregator_rate','aggregator_rate_negotiable','aggregator_lead_status','aggregator_accepted_declined_date','remarks','dr_contact_number','doctor_address','tenure','advance','acknowledgement','mobility','expected_time_line','terms_and_conditions','terms_and_conditions_date','terms_and_conditions_place'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['aggregator_accepted_declined_date','deleted_at','terms_and_conditions_date'];

    public function scopeAggregator($query)
    {
        return $query->where('aggregator_lead',1);
    }

    public function scopeDeployed($query)
    {
        if(\Helper::encryptor('decrypt',session('admin_id')) != 0){
            $userRoleCaregiver = \Helper::getStaffRole(\Helper::encryptor('decrypt',session('admin_id')));
            if($userRoleCaregiver == 'Caregiver'){
                return $query->whereHas('schedules', function($q){
                    $q->where('caregiver_id',\Helper::encryptor('decrypt',session('admin_id')));
                });
            }
        }
    }

    public function scopeOngoing($query)
    {
        return $query->whereIn('status',['Converted','Ongoing','Assessment Completed']);
    }

    public function scopeBranch($query)
    {
        if(\Helper::encryptor('decrypt',session('utype')) != 'Admin' && \Helper::encryptor('decrypt',session('admin_id')) != '0')
        return $query->whereBranchId(\App\Helpers\Helper::getSession('branch_id'));
    }

    public function scopeConverted($query)
    {
        return $query->whereStatus('Converted');
    }

    public function branch()
    {
        return $this->belongsTo(\App\Entities\Masters\Branch::class);
    }

    public function patient()
    {
        return $this->belongsTo(Patient::class,'patient_id','id');
    }

    public function manager()
    {
        return $this->belongsTo(\App\Entities\User::class,'manager_id','user_id');
    }

    public function aggregatorManager()
    {
        return $this->belongsTo(\App\Entities\User::class,'aggregator_manager_id','id');
    }

    public function serviceRequired()
    {
        return $this->belongsTo(\App\Entities\Masters\Service::class,'service_required','id');
    }

    public function serviceOrders()
    {
        return $this->hasMany(CaseServiceOrder::class,'lead_id','id');
    }

    public function serviceRequests()
    {
        return $this->hasMany(CaseServices::class,'lead_id','id');
    }

    public function schedules()
    {
        return $this->hasMany(Schedule::class,'lead_id','id');
    }

    public function assessment()
    {
        return $this->hasMany(CaseForm::class,'lead_id','id');
    }

    public function followUp()
    {
        return $this->hasMany(CaseFollowUp::class,'lead_id','id');
    }

    public function billables()
    {
        return $this->hasMany(CaseBillables::class,'lead_id','id');
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class,'lead_id','id');
    }

    public function worklog()
    {
        return $this->hasMany(WorkLog::class,'lead_id','id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class,'lead_id','id');
    }

    public function dispositions()
    {
        return $this->hasMany(CaseDisposition::class,'lead_id','id')->orderBy('created_at','Desc');
    }

    public function documents()
    {
        return $this->hasMany(CaseDocument::class,'lead_id','id');
    }

    public function referralCategory()
    {
        return $this->belongsTo(\App\Entities\Masters\ReferralCategory::class,'referral_category','id');
    }

    public function referralSource()
    {
        return $this->belongsTo(\App\Entities\Masters\ReferralSource::class,'referral_source','id');
    }

    public function referralPayout()
    {
        return $this->hasOne(ReferralPayout::class,'lead_id','id');
    }
    public function convertedBy()
    {
        return $this->belongsTo(\App\Entities\Caregiver\Caregiver::class,'converted_by','id');
    }

    public function profile()
    {
        return $this->belongsTo(\App\Entities\Profile::class,'tenant_id','tenant_id');
    }
}
