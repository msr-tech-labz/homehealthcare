<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class WorkLog extends Model
{
    use SoftDeletes;
    //use TenantTrait;

    protected $table = 'case_worklogs';

    protected $fillable = ['tenant_id','lead_id','schedule_id','caregiver_id','worklog_date','vitals','routines','user_id'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function caregiver()
    {
        return $this->belongsTo(\App\Entities\Caregiver\Caregiver::class,'caregiver_id','id');
    }

    public function lead()
    {
        return $this->belongsTo(Lead::class,'lead_id','id');
    }

    public function schedule()
    {
        return $this->belongsTo(Schedule::class);
    }

    public function feedback()
    {
        return $this->belongsTo(WorkLogFeedback::class,'id','worklog_id');
    }
}
