<?php

namespace App\Entities;

use App\Entities\AuditableModel;

class WorkTimeMailers extends AuditableModel
{
    protected $table = 'worktime_mailers';
    protected $fillable = ['job_id','payload','status','exception'];

    protected $guarded = 'id';
    public $timestamps = true;
}
