<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Zizaco\Entrust\EntrustRole;
use App\Traits\TenantTrait;

class Role extends EntrustRole
{
    use TenantTrait;

    protected $table = 'auth_roles';

    protected $fillable = [
        'tenant_id',
        'branch_id',
        'created_by',
        'name',
        'display_name',
        'description'
    ];

    protected $guarded = 'id';
    public $timestamps = true;
}
