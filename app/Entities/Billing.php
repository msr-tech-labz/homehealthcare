<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Billing extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'billings';
    protected $fillable = ['tenant_id','visit_id','crn_number','bill_no','bill_date','bill_amount','status'];

    protected $guarded = 'id';
    public $timestamps = true;

    public function careplan()
    {
        return $this->belongsTo(Careplan::class,'crn_number','crn_number');
    }

    public function schedule()
    {
        return $this->belongsTo(Schedule::class,'visit_id','id');
    }
}
