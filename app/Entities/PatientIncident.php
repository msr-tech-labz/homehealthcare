<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PatientIncident extends Model
{
    use SoftDeletes;

    protected $fillable = ['patient_id','complain_for_caregiver_id','complain_type','manager','complain_details','action_taken','status'];
    protected $table = 'patient_incidents';
    protected $guarded = 'id';
    public $timestamps = true;

    public function patient(){
        return $this->belongsTo(Patient::class,'patient_id','id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'manager','id');
    }

    public function caregiver()
    {
        return $this->belongsTo(\App\Entities\Caregiver\Caregiver::class,'complain_for_caregiver_id','id');
    }
}
