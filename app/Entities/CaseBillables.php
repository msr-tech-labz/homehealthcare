<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;
use App\Entities\Masters\Consumables;
use App\Entities\Masters\Surgicals;
use App\Entities\Masters\LabTests;

class CaseBillables extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'case_billables';

    protected $fillable = ['tenant_id','branch_id','patient_id','lead_id','category','item_id','item','quantity','rate','amount','user_id','payment_status'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['date','deleted_at'];

    public function scopeConsumables($query)
    {
        return $query->where('category','Consumables');
    }

    public function scopeEquipments($query)
    {
        return $query->where('category','Equipments');
    }

    public function patient()
    {
        return $this->belongsTo(Patient::class,'patient_id','id');
    }

    public function consumable()
    {
        return $this->belongsTo(Consumables::class,'item_id','id');
    }

    public function surgical()
    {
        return $this->belongsTo(Surgicals::class,'item_id','id');
    }

    public function tests()
    {
        return $this->belongsTo(LabTests::class,'item_id','id');
    }

    public function lead()
    {
        return $this->belongsTo(Lead::class,'lead_id','id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
}
