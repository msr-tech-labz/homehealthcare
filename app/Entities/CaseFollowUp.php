<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class CaseFollowUp extends Model
{
	use SoftDeletes;

	protected $table = 'case_followups';

	protected $fillable = ['branch_id','tenant_id','lead_id','user_id','followup_date','form_data'];

	protected $guarded = 'id';
	public $timestamps = true;
    protected $dates = ['followup_date','deleted_at'];

	public function user()
	{
		return $this->belongsTo(User::class,'user_id','user_id');
	}
}