<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Appraisal extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'employee_appraisals';

    protected $fillable = ['tenant_id','caregiver_id','star','date','marked_by'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['date','deleted_at'];

    public function caregiver()
    {
        return $this->belongsTo(\App\Entities\Caregiver\Caregiver::class);
    }
}
