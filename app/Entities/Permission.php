<?php

namespace App\Entities;

use Zizaco\Entrust\EntrustPermission;
use App\Traits\TenantTrait;

class Permission extends EntrustPermission
{
    use TenantTrait;    

    protected $table = 'auth_permissions';

    protected $fillable=['tenant_id','name','description','display_name','group'];
}
