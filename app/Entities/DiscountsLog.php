<?php

namespace App\Entities;

use App\Entities\AuditableModel;
use App\Traits\TenantTrait;

class DiscountsLog extends AuditableModel
{
    use TenantTrait;

    protected $table = 'case_discounts_log';

    protected $fillable = ['tenant_id', 'branch_id', 'user_id','lead_id', 'patient_id', 'service_request_id', 'from_date', 'to_date', 'discount_type', 'discount_value', 'amount'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];


    public function lead()
    {
        return $this->belongsTo(Lead::class,'lead_id','id');
    }

    public function patient()
    {
        return $this->belongsTo(Patient::class,'patient_id','id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','user_id');
    }

    public function serviceRequest()
    {
        return $this->belongsTo(CaseServices::class,'service_request_id','id');
    }
}
