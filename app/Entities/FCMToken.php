<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FCMToken extends Model
{
    use SoftDeletes;

    protected $table = 'fcm_tokens';

    protected $fillable = ['user_id','user_type','token'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];    
}
