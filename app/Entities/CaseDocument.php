<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class CaseDocument extends Model
{
    use SoftDeletes;
    use TenantTrait;

    protected $table = 'case_documents';

    protected $fillable = ['tenant_id','lead_id','document_name','service_request_id','document_path','user_id'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function lead()
    {
        return $this->belongsTo(Lead::class,'id','lead_id');
    }
    
    public function serviceRequest()
    {
        return $this->belongsTo(CaseServices::class,'service_request_id','id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
}
