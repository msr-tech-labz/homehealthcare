<?php

namespace App\Entities\Corporates;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CaregiverLog extends Model
{
    use SoftDeletes;

    protected $table = 'caregivers_log';

    protected $fillable = ['employee_id','email'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
