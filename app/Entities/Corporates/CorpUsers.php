<?php

namespace App\Entities\Corporates;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Entities\Console\Corporates;
use App\Entities\Corporates\CorpUser;

class CorpUsers extends Model
{
    use SoftDeletes;

    protected $table = 'users';

    protected $fillable = ['full_name','email','gender','password','status'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function setPasswordAttribute($password)
    {
        if(\Hash::needsRehash($password)) {
            $this->attributes['password'] = bcrypt($password);
        }else{
            $this->attributes['password'] = $password;
        }
    }

    public static function validateCredentials($corporate_id, $email, $user_password, $renew=false)
    {
        $user = null;
        if(!empty($corporate_id) && !empty($email) && !empty($user_password)){
            // Check for ORG ID and Fetch database_name field
            $corpDb = Corporates::whereCorporateId($corporate_id)->first();
            if($corpDb){
                if($corpDb->status != 'Active'){
                    $user = "DisabledCorp";
                    return $user;
                }
                // Fetch database_name field
                $dbName = $corpDb->db_name;

                // Set the tenant connection database name to fetched value
                \Config::set('database.connections.tenant.database',$dbName);

                // removing all the sessions before assigning new sessions 
                session()->flush();

                session()->put('corporate.corp_id',\Helper::encryptor('encrypt',$corpDb->id));
                session()->put('corporate.dbName',\Helper::encryptor('encrypt',$dbName));
                session()->put('corporate.corporate_id',\Helper::encryptor('encrypt',$corpDb->corporate_id));

                // Set the tenant as default connection
                \DB::setDefaultConnection('tenant');

                if(filter_var($email, FILTER_VALIDATE_EMAIL)){
                    $user = CorpUsers::whereEmail($email)->get();
                }else{
                    $user = "NoUserFound";
                    return $user;
                }

                if(count($user) > 1){
                    $user = 'Duplicate';
                    return $user;
                } elseif (count($user)) {
                    $user = $user->first();
                    if($user->status != 'Active'){
                        $user = "DisabledUser";
                        return $user;
                    }
                }else{
                    $user = $user->first();
                }

                if($user){
                    if(\Hash::check($user_password,$user->password)){
                        $user->update(['corporate.last_session' => session()->getId()]);
                        session()->put('corporate.last_session',session()->getId());
                        return $user;
                    }else{
                        $user = "InvalidPassword";
                    }
                }else{
                    $user = "NoUserFound";
                }
            }else{
                $user = "NoSubscriber";
            }
        }
        return $user;
    }
}
