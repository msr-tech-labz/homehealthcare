<?php

namespace App\Entities\Corporates;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CorpProfile extends Model
{
    use SoftDeletes;

    protected $table = 'profile';

    protected $fillable = ['email','name','address','state','logo','landline_number'];

    protected $guarded = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
