<?php

namespace App\DataTables;

use App\Entities\Lead;
use Yajra\Datatables\Services\DataTable;

class CasesDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('created_at', function ($case) {
                return isset($case->created_at)?$case->created_at->format('d-M-Y'):'';
            })
            ->editColumn('crn_number', function ($case) {
                $link = route('case.view').'/'.\Helper::encryptor('encrypt',$case->id);
                $redirect = '';
                if(\Helper::encryptor('decrypt',session('utype')) == 'Aggregator'){
                    $redirect = ' target="_blank"';
                }
                if($case->tenant_id != 0){
                    $link .= '?pid='.\Helper::encryptor('encrypt',$case->provider_id);
                }
                $url = '<a href="'.$link.'" '.$redirect.'>'.$case->crn_number.'</a>';

                return $url;
            })
            ->editColumn('no_of_hours', function ($case) {
                return !empty($case->no_of_hours)?$case->no_of_hours:'-';
            })
            ->editColumn('user_id', function ($case) {
                return (isset($case->manager) && !empty($case->manager->full_name))?$case->manager->full_name:'-';
            })
            ->editColumn('patient.first_name', function ($case) {
                return isset($case->patient)?$case->patient->first_name.' '.$case->patient->last_name:'-';
            })
            ->editColumn('patient.enquirer_name', function ($case) {
                return isset($case->patient)?$case->patient->enquirer_name.'<br>'.$case->patient->enquirer_phone:'-';
            })
            ->editColumn('service.service_name', function ($case) {
                return (isset($case->service) && !empty($case->service->service_name))?$case->service->service_name:'-';
            })
            ->editColumn('provider.organization_short_name', function ($case) {
                return (isset($case->provider) && !empty($case->provider->organization_short_name))?$case->provider->organization_short_name:'-';
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Lead::query()->withoutGlobalScopes()
                    ->with('patient')->with('user')->with('serviceRequired');

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    //->addAction(['width' => '80px'])
                    ->parameters($this->getCustomParameters());
    }

    public function getCustomParameters()
    {
        $parameters = [
            "order" => [[3, 'asc']],
            "lengthMenu" => [ [50, 100, 200, -1], [50, 100, 200, "All"] ]
        ];

        return array_merge($this->getBuilderParameters(), $parameters);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'Episode Id' => ['data' => 'crn_number', 'name' => 'crn_number'],
            'Date' => ['data' => 'created_at', 'name' => 'created_at'],
            'Manager' => ['data' => 'user_id', 'name' => 'user_id'],
            'Patient' => ['data' => 'patient.first_name', 'name' => 'patient.first_name'],
            'Enquirer' => ['data' => 'patient.enquirer_name', 'name' => 'patient.enquirer_name'],
            'Location' => ['data' => 'patient.city', 'name' => 'patient.city'],
            'Service' => ['data' => 'serviceRequired.service_name', 'name' => 'serviceRequired.service_name'],
            'No. of Hours' => ['data' => 'no_of_hours', 'name' => 'no_of_hours'],
            'Status' => ['data' => 'status', 'name' => 'status'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'caseslist_' . time();
    }
}
