<?php

namespace App\DataTables;

use App\Entities\Caregiver\Caregiver;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class EmployeesDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function dataTable($query)
    {   
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('professional.role', function ($employee) {                
                return (!empty($employee->professional) && $employee->professional->role == 'Caregiver')?'Yes':'<span style="color: red;font-weight: bold;">No</span>';
            })            
            ->editColumn('professional.employment_type', function ($employee) {
                return !empty($employee->professional)?$employee->professional->employment_type:'-';
            })
            ->editColumn('profile_image', function ($employee) {
                if($employee->profile_image && file_exists(public_path('uploads/provider/'.session('tenant_id').'/caregivers/'.\Helper::encryptor('encrypt',$employee->id)).'/'.$employee->profile_image) ){
                    $cImg = asset('uploads/provider/'.session('tenant_id').'/caregivers/'.\Helper::encryptor('encrypt',$employee->id).'/'.$employee->profile_image);
                }else{
                    $cImg = asset('img/default-avatar.jpg');
                }
                $image = '<img src="'.$cImg.'" width="40" height="40" class="img-circle" />';
                return $image;
            })
            ->editColumn('first_name', function ($employee) {
                return $employee->full_name;
            })
            ->editColumn('professional.specializations.specialization_name', function ($employee) {                
                return !empty($employee->professional->specializations)?$employee->professional->specializations->specialization_name:'-';
            })
            ->editColumn('professional.experience', function($employee) {
                return !empty($employee->professional->experience)?$employee->professional->experience.' yr(s)':'-';
            })
            ->editColumn('work_status', function($employee) {
                if(\Entrust::hasRole('admin') || \Entrust::can('employees-edit')){
                    $html = '<div class="btn-group">
                        <button type="button" class="btn btn-info waves-effect workstatus" data-id="'. \Helper::encryptor('encrypt',$employee->id) .'">'.$employee->work_status.'</button>
                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);" data-status="Available" data-id="'. \Helper::encryptor('encrypt',$employee->id).'" class="work-status waves-effect waves-block">Available</a></li>
                            <li><a href="javascript:void(0);" data-status="Training" data-id="'. \Helper::encryptor('encrypt',$employee->id).'" class="work-status waves-effect waves-block">Training</a></li>
                        </ul>
                    </div>';
                }else{
                    $html = $employee->work_status;
                }
                return $html;
            })
            ->addColumn('action', function ($employee) {
                $html = '';
                if(\Entrust::hasRole('admin') || \Entrust::can('employees-edit')){
                    $html = '<a href="'.route('employee.edit',\Helper::encryptor('encrypt',$employee->id)).'" class="btn btn-warning btn-sm"><i class="material-icons">edit</i></a>';
                }

                return $html;
            })            
            ->rawColumns(['professional.role','profile_image','work_status','action']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query(Caregiver $modal)
    {        
        $query = Caregiver::withoutGlobalScopes()->whereRaw('caregivers.deleted_at IS NULL')
                        ->active()->with('professional')->with('professional.specializations')
                        ->select('caregivers.*');
         return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')                    
                    ->parameters($this->getCustomParameters());
    }

    public function getCustomParameters()
    {
        $parameters = [    
            'dom' => 'lifrptip',
            'buttons' => ['csv', 'excel', 'pdf', 'print', 'reset', 'reload'],
            'lengthMenu' => [ [50, 100, 200, -1], [50, 100, 200, "All"] ],
            'order' => [[ 1, "desc" ]],         
        ];

        return array_merge($this->getBuilderParameters(), $parameters);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [            
            'Id' => ['data' => 'employee_id', 'name' => 'employee_id'],
            'Field Staff' => ['data' => 'professional.role', 'name' => 'professional.role'],
            'Type' => ['data' => 'professional.employment_type', 'name' => 'professional.employment_type'],
            // 'Branch' => ['data' => 'branchName.branch_name', 'name' => 'branchName.branch_name'],
            'Image' => ['data' => 'profile_image', 'name' => 'profile_image'],          
            'Name' => ['data' => 'first_name', 'name' => 'first_name'],
            // 'Gender' => ['data' => 'gender', 'name' => 'gender'],
            'Phone' => ['data' => 'mobile_number', 'name' => 'mobile_number'],
            'Email' => ['data' => 'email', 'name' => 'email'],
            'Specialization' => ['data' => 'professional.specializations.specialization_name', 'name' => 'professional.specializations.specialization_name'],
            'Exp' => ['data' => 'professional.experience', 'name' => 'professional.experience'],
            'Work Status' => ['data' => 'work_status', 'name' => 'work_status', 'width' => '10% !important'],
            'Action' => ['data' => 'action', 'name' => 'action']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'employees_export_' . time();
    }
}
