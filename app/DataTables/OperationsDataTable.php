<?php

namespace App\DataTables;

use App\Entities\Lead;
use Yajra\Datatables\Services\DataTable;
use Yajra\Datatables\Datatables;
use Illuminate\Contracts\View\Factory;

class OperationsDataTable extends DataTable
{
    protected $fromDate ='';
    protected $toDate = '';

    function __construct(Datatables $datatables, Factory $viewFactory)
    {
        $this->fromDate = session('from_date_operations');
        $this->toDate = session('to_date_operations');  
        
        parent::__construct($datatables, $viewFactory);   
    }

    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    { 
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('leads.status', function ($lead) {
                $class = "bg-orange";
                if($lead->status == 'Pending') $class = "bg-orange";
                if($lead->status == 'Closed') $class = "bg-red";
                if($lead->status == 'Converted') $class = "bg-green";

                $str = '<span style="width: 140px; display: block;" class="label '.$class.'">'.$lead->status.'</span>';
                if($lead->registration_amount > 0)
                    $str .= '<span style="width: 140px; display: block;" class="label bg-deep-purple">Registered</span>';                
                
                return $str;
            })
            ->editColumn('leads.created_at', function ($lead) {
                return $lead->created_at->format('d-m-Y h:i A');
            })            
            ->editColumn('leads.episode_id', function ($lead) {
                return !empty($lead->episode_id)?$lead->episode_id:'TEMPLD0AF'.$lead->id;
            })
            ->editColumn('referralSource.source_name', function ($lead) {
                return !empty($lead->referralSource)?$lead->referralSource->source_name:'-';
            })         
            ->editColumn('patient.first_name', function ($lead) {        
                $str = !empty($lead->patient->full_name)?$lead->patient->full_name:'-';
                return $str;
            })
            ->editColumn('patient.enquirer_name', function ($lead) {        
                $str = !empty($lead->patient->enquirer_name)?$lead->patient->enquirer_name:'-';
                return $str;
            })
            ->editColumn('patient.contact_number', function ($lead) {
                if($lead->aggregator_lead && $lead->aggregator_lead_status == 'Pending'){
                    $str = isset($lead->patient->contact_number)?str_pad(substr($lead->patient->contact_number,0,3),10,'X',STR_PAD_RIGHT):'';
                    $str .= !empty($lead->patient->alternate_number)?'<br><small>Enquirer: '.str_pad(substr($lead->patient->alternate_number,0,3),10,'X',STR_PAD_RIGHT).'</small>':'';
                } else {                
                    $str = isset($lead->patient->contact_number)?$lead->patient->contact_number.'<br>':'';
                    $str .= !empty($lead->patient->alternate_number)?'<small>Enquirer: '.$lead->patient->alternate_number.'</small>':'';                    
                }
                
                return $str;
            })
            ->editColumn('patient.city', function ($lead) {
                return (isset($lead->patient) && !empty($lead->patient->city))?$lead->patient->city:'-';
            })
            // ->editColumn('serviceRequired.service_name', function ($lead) {                
            //     if($lead->aggregator_lead && $lead->status == 'Pending')
            //         return $lead->aggregator_service;
            //     else
            //         return (isset($lead->service) && !empty($lead->service->service_name))?$lead->service->service_name:'-';
            // })
            // ->editColumn('leads.status', function($lead) {
            //     return $lead->status;
            // })
            ->addColumn('action', function ($lead) {
                $html = '';
                if($lead->aggregator_lead && $lead->aggregator_lead_status == 'Pending'){
                    if(\Entrust::hasRole('admin') || \Entrust::can('leads-view')){
                        $patientData = json_encode(collect($lead->patient)->only('first_name','last_name','patient_age','patient_weight','gender','street_address','area','city'));
                        $leadData = json_encode(collect($lead)->only('case_description','medical_conditions','medications','procedures','aggregator_service','language_preference','gender_preference','aggregator_rate','aggregator_rate_negotiable'));
                        $html = '<a href="javascript:void(0);" target="_blank" class="btn btn-md bg-primary btnViewLead" data-id="'. \Helper::encryptor('encrypt',$lead->id).'" data-patient-id="'.\Helper::encryptor('encrypt',$lead->patient_id).'" data-patient="'.str_replace("\"", "'", $patientData).'" data-case="'.str_replace("\"", "'", $leadData).'" title="View Lead Details">View</a>';
                    }
                }elseif($lead->aggregator_lead && $lead->aggregator_lead_status == 'Accepted'){
                    if(\Entrust::hasRole('admin') || \Entrust::can('leads-view')){
                        $html = '<a href="'.route('lead.view',\Helper::encryptor('encrypt',$lead->id)).'" class="btn btn-md bg-primary" data-id="'.\Helper::encryptor('encrypt',$lead->id).'" title="Create Lead">View</a>';
                    }
                }elseif(!$lead->aggregator_lead && $lead->aggregator_lead_status == 'Pending'){
                    if(\Entrust::hasRole('admin') || \Entrust::can('leads-view')){
                        $html = '<a href="'.route('lead.view',\Helper::encryptor('encrypt',$lead->id)).'" class="btn btn-md bg-primary" data-id="'.\Helper::encryptor('encrypt',$lead->id).'" title="View Lead">View</a>';
                    }
                }

                return $html;
            })
            ->setRowClass(function ($lead) {
                return $lead->aggregator_lead == 1 ? 'bgagglead' : '';
            })
            ->rawColumns(['leads.status','patient.first_name','patient.contact_number','patient.city','action'])
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        if(empty($this->request()->get('search')['value'])){
            $query = Lead::deployed()->whereRaw('leads.deleted_at IS NULL')
                            ->whereRaw(\DB::raw('DATE(leads.created_at) BETWEEN "'.$this->fromDate.'" AND "'.$this->toDate.'"'))
                            ->whereNotIn('status',['Pending','Follow Up'])
                            ->with('patient')->with('serviceRequired')->with('referralSource')->select('leads.*');
        }else{
            $query = Lead::deployed()->whereRaw('leads.deleted_at IS NULL')
                            ->whereNotIn('status',['Pending','Follow Up'])
                            ->with('patient')->with('serviceRequired')->with('referralSource')->select('leads.*');
        }     
        //removed withoutGlobalScopes() from Lead
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')                    
                    ->parameters($this->getCustomParameters());
    }

    public function getCustomParameters()
    {
        $parameters = [    
            'dom' => 'lifrptip',
            'buttons' => ['csv', 'excel', 'pdf', 'print', 'reset', 'reload'],
            'lengthMenu' => [ [50, 100, 200, -1], [50, 100, 200, "All"] ],
            'order' => [[ 1, "desc" ]],
            "columnDefs" => [
                [ "width" => "12%", "targets" => 1 ]
            ]      
        ];

        return array_merge($this->getBuilderParameters(), $parameters);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [            
            'Status' => ['data' => 'leads.status', 'name' => 'leads.status', 'searchable' => true, 'visible' => true],
            'Date' => ['data' => 'leads.created_at', 'name' => 'leads.created_at'],
            'Source' => ['data' => 'referralSource.source_name', 'name' => 'referralSource.source_name'],
            'Episode Id' => ['data' => 'leads.episode_id', 'name' => 'leads.episode_id'],
            'Patient' => ['data' => 'patient.first_name', 'name' => 'patient.first_name'],
            'Enquirer' => ['data' => 'patient.enquirer_name', 'name' => 'patient.enquirer_name'],
            'Phone' => ['data' => 'patient.contact_number', 'name' => 'patient.contact_number'],
            'Location' => ['data' => 'patient.city', 'name' => 'patient.city'],
            // 'Service' => ['data' => 'serviceRequired.service_name', 'name' => 'serviceRequired.service_name'],
            'Action' => ['data' => 'action', 'name' => 'action']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'leadslist_' . time();
    }
}