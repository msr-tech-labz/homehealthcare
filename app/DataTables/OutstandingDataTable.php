<?php

namespace App\DataTables;

use App\Entities\Patient;
use App\Entities\Schedule;
use App\Entities\CreditMemo;
use App\Entities\CasePayment;
use App\Entities\CaseBillables;
use App\Entities\Lead;

use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Arr;

class OutstandingDataTable extends DataTable
{   
    protected $totalServiceAmount = 0;
    protected $totalBillablesAmount = 0;
    protected $creditTotal = 0;
    protected $debitTotal = 0;
    protected $totalPayments = 0;
    protected $outstandingAmount = 0;
    protected $tillDate ='';

    function __construct()
    {
        $this->totalServiceAmount = 0;
        $this->totalBillablesAmount = 0;
        $this->creditTotal = 0;
        $this->debitTotal = 0;
        $this->totalPayments = 0;
        $this->outstandingAmount = 0;
        
        if(session('till_date') == null)
            $this->tillDate = date('Y-m-d');
        else
            $this->tillDate = session('till_date');
    }
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('patients.patient_id', function($patient){
                return $patient->patient_id;
            })
            ->editColumn('patients.first_name', function($patient){
                return $patient->full_name;
            })
            ->editColumn('service_amount',function($patient){
                $this->totalServiceAmount = 0;
                $schedules = Schedule::select('id','lead_id','schedule_date','service_required','service_request_id','chargeable','amount','chargeable','ratecard_id')
                                        ->whereDate('schedule_date','<=',$this->tillDate)
                                        ->with(['serviceRequest' => function($q){
                                            $q->addSelect('id','from_date','to_date','net_rate','gross_rate','discount_type','discount_value');
                                        },'service' => function($q){
                                            $q->addSelect('id','service_name');
                                        }])->wherePatientId($patient->id)->whereChargeable(1)->whereIn('status',['Completed']);
                $temp = clone $schedules;
                $summary = \Helper::getSchedulesByMonth($temp->get());
                foreach ($summary as $month => $serviceItem) {
                    if(count(collect($serviceItem))){
                        foreach($serviceItem as $item){
                        $this->totalServiceAmount += floatval($item['amount']);
                        }
                    }
                }
                return $this->totalServiceAmount;
            })
            ->editColumn('billable_amount',function($patient){
                $this->totalBillablesAmount = 0;
                $billables = CaseBillables::whereIn('lead_id', Lead::wherePatientId($patient->id)->whereDate('created_at','<=',$this->tillDate)->pluck('id')->toArray())->get();

                if(count(collect($billables))){
                    $taxSlab = 0;
                    foreach($billables as $billable){
                        $item = $billable->item;
                        if($billable->item_id != null){
                            switch ($billable->category) {
                                case 'Consumables': $item = $billable->consumable->consumable_name; $taxSlab = $billable->consumable->tax->tax_rate; break;
                                case 'Equipments': $item = $billable->surgical->surgical_name; $taxSlab = $billable->surgical->tax->tax_rate; break;
                            }
                        }
                        $this->totalBillablesAmount += floatval($billable->amount);
                    }
                }

                return $this->totalBillablesAmount;
            })
            ->editColumn('credit_amount',function($patient){
                $this->creditTotal = 0;

                $creditMemos = CreditMemo::wherePatientId($patient->id)->whereDate('date','<=',$this->tillDate)->get();
                if(count(collect($creditMemos))){
                    foreach ($creditMemos as $credit){
                        if($credit->type != 'Schedule'){
                                if(empty($credit->receipt)){
                                    if($credit->amount >= 0){
                                        $this->creditTotal += floatVal($credit->amount);
                                    }
                                }
                        }
                        if($credit->type == 'Schedule' && $credit->invoices->count() == 0 && $credit->receipt){
                            $this->creditTotal -= floatVal($credit->amount);
                        }
                    }
                }

                return $this->creditTotal;
            })
            ->editColumn('debit_amount',function($patient){
                $this->debitTotal = 0;

                $creditMemos = CreditMemo::wherePatientId($patient->id)->whereDate('date','<=',$this->tillDate)->get();
                if(count(collect($creditMemos))){
                    foreach ($creditMemos as $credit){
                        if($credit->type != 'Schedule'){
                            if(empty($credit->receipt)){
                                if($credit->amount < 0){
                                    $this->debitTotal += floatVal($credit->amount);
                                }
                            }
                        }
                    }
                }

                return $this->debitTotal;
            })
            ->editColumn('payment_amount',function($patient){
                $this->totalPayments = 0;
                $payments = CasePayment::select('total_amount')->wherePatientId($patient->id)->whereStatus('Success')->whereDate('created_at','<=',$this->tillDate);
                if(count(collect($payments))){
                    if($payments->sum('total_amount') > 0){
                        $this->totalPayments += floatVal($payments->sum('total_amount'));
                    }
                }

                return $this->totalPayments;
            })
            ->editColumn('outstanding_amount',function($patient){
                $this->outstandingAmount = 0;
                $this->outstandingAmount = $this->totalServiceAmount + $this->totalBillablesAmount - $this->totalPayments - $this->creditTotal + abs($this->debitTotal);
                return $this->outstandingAmount;
            });
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query(Patient $modal)
    {
        $query = Patient::withoutGlobalScopes()->whereNotNull('patients.patient_id')->select('patients.*');

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->parameters([
                        'dom'     => 'Bfrtip',
                        'order'   => [[0, 'desc']],
                        'lengthMenu' => [ [50, 100, 200, -1], [50, 100, 200, "All"] ],
                        'buttons' => [
                            'csv'
                        ],
                        "columnDefs" => [
                            [ "width" => "30%", "targets" => 1 ] // 1 => 2nd column of the table 
                        ],
                        'preDrawCallback' => 'function() { $(".loader").html(`<div class="page-loader-wrapper">
                            <div class="loader">
                                <div class="md-preloader pl-size-md">
                                    <svg viewbox="0 0 75 75">
                                        <circle cx="37.5" cy="37.5" r="33.5" class="pl-red" stroke-width="4" />
                                    </svg>
                                </div>
                                <p>Please wait...</p>
                            </div>
                        </div>`); }',
                        "drawCallback" => 'function() { 
                            $(".loader").html("");
                            $("html, body").stop().animate({
                                "scrollTop": $(".outstandingTable").offset().top-20
                            }, 700, "swing"); }',
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'Patient Id' => ['data' => 'patients.patient_id', 'name' => 'patients.patient_id', 'searchable' => true, 'orderable' => true],
            'Patient' => ['data' => 'patients.first_name', 'name' => 'patients.first_name', 'searchable' => true, 'orderable' => true],
            'Service' => ['data' => 'service_amount', 'name' => 'service_amount','searchable' => false, 'orderable' => false],
            'Billable' => ['data' => 'billable_amount', 'name' => 'billable_amount','searchable' => false, 'orderable' => false],
            'Credit' => ['data' => 'credit_amount', 'name' => 'credit_amount','searchable' => false, 'orderable' => false],
            'Debit' => ['data' => 'debit_amount', 'name' => 'debit_amount','searchable' => false, 'orderable' => false],
            'Payment' => ['data' => 'payment_amount', 'name' => 'payment_amount','searchable' => false, 'orderable' => false],
            'Outstanding' => ['data' => 'outstanding_amount', 'name' => 'outstanding_amount','searchable' => false, 'orderable' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'outstandingdatatable_' . time();
    }
}
