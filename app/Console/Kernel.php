<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\GenerateMISReportCharts::class,
        Commands\SendMISReport::class,
        Commands\DeleteCharts::class,
        Commands\DailyLeadsReport::class,
        Commands\StoreDailyRevenue::class,
        Commands\SendActiveUsers::class,
        Commands\SendLoginEvents::class,
        // Commands\StoreLeadsFromMyoperator::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('charts:generate')->timezone('Asia/Kolkata')->dailyAt('01:00')->withoutOverlapping();
        $schedule->command('mis:send')->timezone('Asia/Kolkata')->dailyAt('05:00')->withoutOverlapping();
        $schedule->command('charts:delete')->timezone('Asia/Kolkata')->dailyAt('06:00')->withoutOverlapping();
        $schedule->command('store:dailyrevenue')->timezone('Asia/Kolkata')->dailyAt('18:00')->withoutOverlapping();
        $schedule->command('leads:report')->timezone('Asia/Kolkata')->dailyAt('21:00')->withoutOverlapping();
        $schedule->command('activeusers:send')->timezone('Asia/Kolkata')->dailyAt('16:30')->withoutOverlapping();
        $schedule->command('send:loggingevents')->timezone('Asia/Kolkata')->mondays()->at('7:00')->withoutOverlapping();
        // $schedule->command('store:leadsfrommyoperator')->timezone('Asia/Kolkata')->everyFifteenMinutes()->withoutOverlapping();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
