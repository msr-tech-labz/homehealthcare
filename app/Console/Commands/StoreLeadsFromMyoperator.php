<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Patient;
use App\Entities\Lead;
use App\Entities\CaseDisposition;
use Carbon\Carbon;


class StoreLeadsFromMyoperator extends Command
{
    /**
    * The name and signature of the console command.
    *
    * @var string
    */
    protected $signature = 'store:leadsfrommyoperator';

    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Store Leads From Myoperator';

    /**
    * Create a new command instance.
    *
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
    }

    /**
    * Execute the console command.
    *
    * @return mixed
    */
    public function handle()
    {
        $url = 'https://developers.myoperator.co/search';
        $jsonData = array(
            'token' => 'caa33e90c25aaecccd070dabfb6117df',
            'from' => strtotime(Carbon::now()->subHour('1')->format('Y-m-d H:i:s')),
            'to' => strtotime(Carbon::now()->format('Y-m-d H:i:s'))
        );
        $jsonDataEncoded = json_encode($jsonData);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);

        if($response){
            $res = json_decode($response , true);
            if($res['status'] == 'success'){
                // connecting to aajicare database
                \Config::set('database.connections.api.database','shc_branch269');
                \DB::setDefaultConnection('api');

                foreach($res['data']['hits'] as $i => $value){
                    $tenantId = 269;
                    $branchId = '1';
                    $userId = 0;
                    $patientID = null;
                    $lead = null;
                    $contact_number = $value['_source']['caller_number_raw'];
                    $contactNoCheck = Patient::whereContactNumber($contact_number)->first();
                    // if contact no exists it will not create the lead
                    if(!$contactNoCheck){
                        $patientDetails['branch_id'] = $branchId;
                        $patientDetails['tenant_id'] = $tenantId;
                        $patientDetails['first_name'] = 'Name unknown';
                        $patientDetails['last_name'] = '';
                        $patientDetails['contact_number'] = preg_replace('/\s+/','',$contact_number);
                        $patientDetails['gender'] = 'Other';
                        $patientDetails['date_of_birth'] = null;
                        $patientDetails['patient_age'] = $patientDetails['patient_weight'] = $patientDetails['street_address'] = $patientDetails['area'] = $patientDetails['city'] = $patientDetails['zipcode'] = $patientDetails['state'] = $patientDetails['country'] = $patientDetails['latitude'] = $patientDetails['longitude'] = $patientDetails['enquirer_name'] = $patientDetails['alternate_number'] = $patientDetails['relationship_with_patient'] = '';
                        $patientDetails['created_at'] = $patientDetails['updated_at'] = date('Y-m-d H:i:s');
                        $patientID = Patient::insertGetId($patientDetails);
                    }

                    if($patientID){
                        $caseDetails['tenant_id'] = $tenantId;
                        $caseDetails['branch_id'] = $branchId;
                        $caseDetails['patient_id'] = $patientID;
                        $caseDetails['case_description'] = $caseDetails['case_type'] = $caseDetails['medical_conditions'] = $caseDetails['medications'] = $caseDetails['procedures'] = $caseDetails['hospital_name'] = $caseDetails['primary_doctor_name'] = $caseDetails['special_instructions'] = $caseDetails['assessment_date'] = $caseDetails['assessment_time'] = $caseDetails['assessment_notes'] = $caseDetails['rate_agreed'] = $caseDetails['payment_mode'] = $caseDetails['payment_notes'] = $caseDetails['referral_category'] = $caseDetails['referrer_name'] = $caseDetails['converted_by'] = $caseDetails['manager_id'] = $caseDetails['referral_type'] = '';
                        $caseDetails['service_category'] = $caseDetails['service_required'] = $caseDetails['estimated_duration'] = $caseDetails['registration_amount'] = 0;
                        $caseDetails['gender_preference'] = 'Any';
                        $caseDetails['referral_value'] = '0';
                        $caseDetails['remarks'] = 'This lead is through myoperator';
                        $caseDetails['status'] = 'Pending';
                        $caseDetails['created_at'] = $caseDetails['updated_at'] = date('Y-m-d H:i:s');
                        $lead = Lead::create($caseDetails);
                        $leadID = $lead->id;
                    }

                    if($lead){
                        $dispoData['tenant_id'] = $tenantId;
                        $dispoData['lead_id'] = $leadID;
                        $dispoData['user_id'] = $userId;
                        $dispoData['status'] = 'Pending';
                        $dispoData['disposition_date'] = date('Y-m-d H:i:s');
                        CaseDisposition::firstOrCreate($dispoData);
                    }
                }

                \DB::purge('api');
            }
        }

        $this->info('Lead Stored From Myoperator Successfully!');
    }
}
