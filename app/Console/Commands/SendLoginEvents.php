<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Console\Subscriber;
use App\Entities\LoggingEvents;
use App\Mail\Reports\SendLoginEventsMail;
use Carbon\Carbon;

class SendLoginEvents extends Command
{
    /**
    * The name and signature of the console command.
    *
    * @var string
    */
    protected $signature = 'send:loggingevents';

    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Send login events for vcare';

    /**
    * Create a new command instance.
    *
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
    }

    /**
    * Execute the console command.
    *
    * @return mixed
    */
    public function handle()
    {
        $data['lastMonday'] = date("Y-m-d H:i:s", strtotime("-7 day"));
        $data['today'] = Carbon::now();
        \Config::set('database.connections.api.database','shc_branch271');
        \DB::setDefaultConnection('api');
            $data['users'] = LoggingEvents::where('created_at','>=',$data['lastMonday'])->where('created_at','<=',$data['today'])->get();
        \DB::purge('api');
        $pdf = \PDF::loadView('mails.loggingevents', $data);

        \Mail::send(new SendLoginEventsMail($data, $pdf->output()));

            
        $this->info('Mail sent Successfully!');
    }
}