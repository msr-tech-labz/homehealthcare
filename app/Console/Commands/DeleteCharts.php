<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DeleteCharts extends Command
{
    /**
    * The name and signature of the console command.
    *
    * @var string
    */
    protected $signature = 'charts:delete';

    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Delete Generates Charts of MIS';

    /**
    * Create a new command instance.
    *
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
    }

    /**
    * Execute the console command.
    *
    * @return mixed
    */
    public function handle()
    {
        // self::delete_files(public_path().DIRECTORY_SEPARATOR.'MISCHARTS'.DIRECTORY_SEPARATOR);
        \File::deleteDirectory(public_path().DIRECTORY_SEPARATOR.'MISCHARTS'.DIRECTORY_SEPARATOR,true);
        $this->info('All available charts deleted successfully!');
    }
}
