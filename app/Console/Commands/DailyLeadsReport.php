<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Console\Subscriber;
use App\Mail\Reports\LeadsReport;
use App\Entities\Lead;

class DailyLeadsReport extends Command
{
    /**
    * The name and signature of the console command.
    *
    * @var string
    */
    protected $signature = 'leads:report';

    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Collect Daily Leads Data and Send EMail to configured users.';

    /**
    * Create a new command instance.
    *
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
    }

    /**
    * Execute the console command.
    *
    * @return mixed
    */
    public function handle()
    {
        $subscribers = Subscriber::active()->whereUserType('Provider')->get();
        if(count($subscribers)){
            $data = [];
            foreach ($subscribers as $subscriber) {
                \Config::set('database.connections.api.database',$subscriber->database_name);
                \DB::setDefaultConnection('api');

                $mailList = [];
                $mailers = \Helper::getSubscribersByType('Daily Leads Report');
                if(count($mailers)){
                    foreach ($mailers as $mailer) {
                        if(filter_var($mailer['email'], FILTER_VALIDATE_EMAIL)){
                            $mailList[] = $mailer['email'];
                        }
                    }
                }

                if(!empty($mailList)){
                    $data = [];
                    $data['received'] = Lead::whereDate('created_at',date('Y-m-d'))->get();
                    $data['converted'] = Lead::whereHas('dispositions',function($q){
                        $q->whereDate('disposition_date',date('Y-m-d'))->whereStatus('Converted');
                    })->get();
                    $data['pending'] = Lead::whereDate('created_at',date('Y-m-d'))->whereStatus('Pending')->get();
                    $data['dropped'] = Lead::whereHas('dispositions',function($q){
                        $q->whereDate('disposition_date',date('Y-m-d'))->whereStatus('Dropped');
                    })->get();

                    \Mail::to($mailList)->send(new LeadsReport($data));
                    unset($data);
                }
                \DB::purge('api');
            }
        }

        $this->info('Mails sent successfully!');
    }
}
