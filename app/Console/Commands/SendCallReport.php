<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Dashboard;

class SendCallReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:callreport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Email Regarding Todays Call Report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $result = Dashboard::DailyStats();

         \Mail::send('calls.reports', $result, function ($mail) use ($result) {
            $mail->to('nitish@apnacare.in')
                ->from('noreply@apnacare.in', 'Apnacare Team')
                ->subject('Call Report for '.date('d-m-Y'));
        });

         $this->info('Call reports sent successfully!');
    }
}
