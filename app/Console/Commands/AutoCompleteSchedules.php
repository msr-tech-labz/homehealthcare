<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Settings;
use App\Entities\Schedule;
use App\Entities\Leaves;
use App\Entities\Console\Subscriber;

class AutoCompleteSchedules extends Command
{
    /**
    * The name and signature of the console command.
    *
    * @var string
    */
    protected $signature = 'schedules:complete';
    
    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Auto complete today\'s schedules';
    
    /**
    * Create a new command instance.
    *
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
    * Execute the console command.
    *
    * @return mixed
    */
    public function handle()
    {
        $subscribers = Subscriber::whereStatus('Active')->whereDate('trial_end_date','<=',date('Y-m-d'))->get();
        if(count($subscribers)){
            foreach ($subscribers as $subscriber) {
                \Config::set('database.connections.api.database',$subscriber->database_name);
                \DB::setDefaultConnection('api');
                
                $isSettingEnabled = Settings::first()->value('auto_complete_schedules');
                if($isSettingEnabled){
                    $schedules = Schedule::whereScheduleDate(date('Y-m-d',strtotime("-1 days")))
                                    ->whereStatus('Pending')->get();
                    if(count($schedules)){
                        foreach ($schedules as $schedule) {
                            // Update schedule status
                            $schedule->update(['status' => 'Completed']);
                            
                            $isScheduled = Schedule::whereScheduleDate(date('Y-m-d'))
                                                ->whereCaregiverId($schedule->caregiver_id)->count()?true:false;
                            $isOnLeave = Leaves::whereRaw(\DB::raw('DATE('.date('Y-m-d').') BETWEEN date_from AND date_to'))
                                                ->whereCaregiverId($schedule->caregiver_id)->count()?true:false;
                            
                            // Update caregiver work status
                            if(!$isScheduled && !$isOnLeave){
                                $caregiver = Caregiver::whereId($schedule->caregiver_id)->first();
                                if($caregiver){
                                    $caregiver->update(['work_status' => 'Available']);
                                }
                            }
                        }
                    }
                }
                
                \DB::purge('api');
            }
        }
        
        $this->info('Schedules completed successfully!');
    }
}
