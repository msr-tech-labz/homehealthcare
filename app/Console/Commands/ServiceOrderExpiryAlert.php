<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Entities\Profile;
use App\Entities\Lead;
use App\Entities\CaseServices;
use App\Entities\Console\Subscriber;
use App\Mail\Provider\SendServiceOrderAlert;
use App\Notifications\SMSNotificationLead;

class ServiceOrderExpiryAlert extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'serviceorder:alert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Alert for Service Expiry';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $subscribers = Subscriber::whereStatus('Active')->whereDate('trial_end_date','>=',date('Y-m-d'))->get();        
        if(count($subscribers)){
            foreach ($subscribers as $subscriber) {
                \Config::set('database.connections.api.database',$subscriber->database_name);
                \DB::setDefaultConnection('api');
                $profile = Profile::first();
                $expDate =  \Carbon\Carbon::now()->addDays(4)->format('Y-m-d');
                $data = [];
                $caseServices = CaseServices::with('patient')->with(['lead' => function ($q) {
                                $q->withoutGlobalScopes();
                                }])->with('service')
                                ->whereStatus('Pending')
                                ->whereDate('from_date','!=','to_date')
                                ->whereDate('to_date', '<=', $expDate)
                                ->whereDate('to_date', '>=', \Carbon\Carbon::now()->format('Y-m-d'))
                                ->get();
                if(isset($caseServices)){
                    foreach ($caseServices as $cs) {
                        if(isset($cs->patient->email)){
                            $data = [
                                'service'           => $cs->service->service_name,
                                'patient'           => $cs->patient->fullname,
                                'from_date'         => $cs->from_date->format('d-m-Y'),
                                'to_date'           => $cs->to_date->format('d-m-Y'),
                                'organization_name' => $profile->organization_name,
                                'amount'            => ($cs->net_rate)*(count($cs->schedules)),
                                'episode'           => $cs->lead->episode_id
                            ];
                            $mail = \Mail::to('nitish@apnacare.in')->cc($profile->email);
                            $mail->queue(new SendServiceOrderAlert($data));
                            \Nexmo::message()->send([
                                'to'   => '918688279500',
                                'from' => '918688279500',
                                'text' => 'Your service '.$data['service'].' is going to Expire on '.$data['to_date'].' .Please make the payment of '.$data['amount'].' before the overdue date.Ignore if already paid.'
                            ]);
                            unset($data);
                        }
                    }          
                }
                
                \DB::purge('api');
            }
        }
        
        $this->info('Service Order Expiry ALert Sent Successfully!');
    }
}
