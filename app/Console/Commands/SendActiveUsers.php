<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Console\Subscriber;
use App\Entities\Console\SubscriberActiveUsers;
use App\Mail\Reports\MisReport;
use App\Mail\Provider\SendActiveUsersMail;
use App\Entities\PatientMailers;
use App\Entities\Schedule;
use Carbon\Carbon;

class SendActiveUsers extends Command
{
    /**
    * The name and signature of the console command.
    *
    * @var string
    */
    protected $signature = 'activeusers:send';

    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Active Users';

    /**
    * Create a new command instance.
    *
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
    }

    /**
    * Execute the console command.
    *
    * @return mixed
    */
    public function handle()
    {
        if(date('d') == 4){
            $subscriber = Subscriber::whereStatus('Active')->whereActiveUsers('Yes')->whereMailStatus('Not Send')->first();
            if($subscriber){
                $data['yesterday'] = Carbon::yesterday();
                $year = $data['yesterday']->format('Y');
                $month = $data['yesterday']->format('m');
                $checkrecord = SubscriberActiveUsers::whereSubscriberId($subscriber->id)->whereMonth('date',$month)->whereYear('date',$year)->first();
                if(!$checkrecord){
                    \Config::set('database.connections.api.database',$subscriber->database_name);
                    \DB::setDefaultConnection('api');
                        $caregivers = Schedule::select('caregiver_id')->whereDate('schedule_date','>=',$year.'-'.$month.'-01')
                        ->whereDate('schedule_date','<=',$year.'-'.$month.'-31')
                        ->where('caregiver_id','>',0)
                        ->distinct('caregiver_id')->get();
                    \DB::purge('api');
                    $data['caregivers'] = $caregivers;
                    $data['subscriber'] = $subscriber;
                    $pdf = \PDF::loadView('console.active_users.printactiveusers', $data);

                    \Mail::send(new SendActiveUsersMail($data, $pdf->output()));

                    $subscriber->update(['mail_status' => 'Send']);
                    //Record saving
                    $record = new SubscriberActiveUsers;
                    $record->subscriber_id     = $subscriber['id'];
                    $record->date              = date('Y-m-d');
                    $record->users              = count($data['caregivers']);
                    $record->save();
                }  
            }else{
                $subscribers = Subscriber::whereStatus('Active')->whereActiveUsers('Yes')->whereMailStatus('Send')->get();
                if(count($subscribers)){
                    foreach($subscribers as $s){
                        $s->update(['mail_status' => 'Not Send']);
                    }
                }
            }
        }
            
        $this->info('Statement created Successfully!');
    }
}