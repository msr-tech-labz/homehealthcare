<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Settings;
use App\Entities\Caregiver\Caregiver;
use App\Entities\Schedule;
use App\Entities\Leaves;
use App\Entities\Console\Subscriber;
use App\Mail\Provider\UpcomingSchedulesAlert;

class UpcomingSchedules extends Command
{
    /**
    * The name and signature of the console command.
    *
    * @var string
    */
    protected $signature = 'schedules:mail';

    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Send upcoming schedules to mail';

    /**
    * Create a new command instance.
    *
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
    }

    /**
    * Execute the console command.
    *
    * @return mixed
    */
    public function handle()
    {
        $subscribers = Subscriber::whereId(142)->get();
        if(count($subscribers)){
            $data = [];
            foreach ($subscribers as $subscriber) {
                \Config::set('database.connections.api.database',$subscriber->database_name);
                \DB::setDefaultConnection('api');

                // Get schedules
                $schedules = Schedule::withoutGlobalScopes()
                                ->with(['patient' => function ($q){
                                    $q->select('id','patient_id','first_name','last_name');
                                },'caregiver' => function ($q){
                                    $q->withoutGlobalScopes()->select('id','first_name','last_name','mobile_number');
                                }])
                                ->whereScheduleDate(date('Y-m-d',strtotime("+1 days")))
                                ->whereStatus('Pending')->get();
                if(count($schedules)){
                    $groupByPatient = $schedules->groupBy('patient_id');
                    $data['total_clients'] = $schedules->unique('patient_id')->count();
                    $data['total_schedules'] = $schedules->count();
                    if(count($groupByPatient)){
                        foreach ($groupByPatient as $patient => $schedule) {
                            $caregiverStr = "";
                            if(count($schedule)){
                                foreach ($schedule as $s) {
                                    if(isset($s->caregiver)){
                                        $caregiverStr .= trim($s->caregiver->full_name).' ('.$s->caregiver->mobile_number.'), ';
                                    }
                                }
                                $data['schedules'][ucwords(trim($s->patient->full_name))] = rtrim($caregiverStr,", ");
                            }
                        }
                    }
                }

                // Get Leaves
                $leaves = Leaves::withoutGlobalScopes()->whereStatus('Approved')
                                ->whereRaw(\DB::raw('CAST("'.date('Y-m-d').'" as DATE) BETWEEN date_from AND date_to'))
                                ->with(['caregiver' => function ($q){
                                    $q->withoutGlobalScopes()->select('id','employee_id','first_name','last_name','mobile_number');
                                }])->get();
                if(count($leaves)){
                    $leaveType = ['CL' => 'Casual Leave','SL' => 'Sick Leave','Off' => 'Off','NS' => 'No Show','ML' => 'Maternity Leave', 'Absent' => 'Absent','Leave' => 'Leave','Bench' => 'Bench'];
                    $groupByLeave = $leaves->groupBy('type');
                    if(count($groupByLeave)){
                        foreach ($groupByLeave as $type => $leave) {
                            $leaveStr = "";
                            if(count($leave)){
                                foreach ($leave as $l) {
                                    if(isset($l->caregiver)){
                                        $leaveStr .= trim($l->caregiver->full_name).' ( '.$l->caregiver->mobile_number.'), ';
                                    }
                                }
                                $data['leaves'][$leaveType[trim($type)]] = rtrim($leaveStr,", ");
                            }
                        }
                    }
                }

                // Bench staff
                $ids = $lids = [];
                if(count($schedules)){
                    $ids = $schedules->pluck('caregiver_id')->toArray();
                }

                if(count($leaves)){
                    $lids = $leaves->pluck('caregiver_id')->toArray();
                }

                $mergedArr = array_unique(array_merge($ids, $lids));
                if(count($mergedArr)){
                    $caregivers = Caregiver::withoutGlobalScopes()->active()
                                    ->select('id','first_name','last_name','mobile_number')
                                    ->whereHas('professional', function ($q) use($mergedArr){
                                        $q->whereRole('Caregiver')->whereNotIn('caregiver_id',$mergedArr);
                                    })
                                    ->whereNotIn('id',$mergedArr)->get();

                    if(count($caregivers)){
                        $benchStr = "";
                        foreach ($caregivers as $caregiver) {
                            $benchStr .= trim($caregiver->full_name).' ('.$caregiver->mobile_number.'), ';
                        }
                        $data['bench'] = rtrim($benchStr,", ");
                    }
                }
                //dd($data);

                $mail = \Mail::to('suhas@apnacare.in');
                $mail->send(new UpcomingSchedulesAlert($data));

                \DB::purge('api');
            }
        }

        $this->info('Schedules list sent successfully!');
    }
}
