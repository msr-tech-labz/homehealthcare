<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Console\Subscriber;
use App\Helpers\Report\MIS;
use App\Mail\Reports\MisReport;

class SendMISReport extends Command
{
    /**
    * The name and signature of the console command.
    *
    * @var string
    */
    protected $signature = 'mis:send';

    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Attach Generated Charts into mail and send MIS Report';

    /**
    * Create a new command instance.
    *
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
    }

    /**
    * Execute the console command.
    *
    * @return mixed
    */
    public function handle()
    {
        $subscribers = Subscriber::active()->whereUserType('Provider')->get();
        if(count($subscribers)){
            $data = [];
            foreach ($subscribers as $subscriber) {
                \Config::set('database.connections.api.database',$subscriber->database_name);
                \DB::setDefaultConnection('api');

                $mailList = [];
                $mailers = \Helper::getSubscribersByType('Daily MIS Report');
                if(count($mailers)){
                    foreach ($mailers as $mailer) {
                        if(filter_var($mailer['email'], FILTER_VALIDATE_EMAIL)){
                            $mailList[] = $mailer['email'];
                        }
                    }
                }

                if(!empty($mailList)){
                    //Attaching respective charts to email and send logic
                    $data['r-todate'] = MIS::getRevenueByPeriod('Today', '');
                    $data['r-mtdArr'] = MIS::getRevenueByPeriod('MTD', '');
                    $data['r-ytdArr'] = MIS::getRevenueByPeriod('YTD', '');

                    $data['l-todate'] = MIS::getLeadsByPeriod('Today', '');
                    $data['l-mtdArr'] = MIS::getLeadsByPeriod('MTD', '');
                    $data['l-ytdArr'] = MIS::getLeadsByPeriod('YTD', '');

                    $data['hrStats'] = MIS::hrStats(['date' => date('Y-m-d',strtotime('-1 days'))]);
                    $data['servicesStats'] = MIS::servicesStats(array('date' => \Carbon\Carbon::yesterday()->format('Y-m-d')));
                    $data['subID'] = $subscriber->id;
                    \Mail::to($mailList)->send(new MisReport($data,''));
                }

                \DB::purge('api');
            }
        }

        $this->info('Mails sent successfully!');
    }
}
