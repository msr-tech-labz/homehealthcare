<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Console\Subscriber;
use App\Mail\Reports\MisReport;
use App\Mail\Provider\SendActiveUsersMail;
use App\Entities\PatientMailers;
use App\Entities\Schedule;

class SendActiveUsers extends Command
{
    /**
    * The name and signature of the console command.
    *
    * @var string
    */
    protected $signature = 'activeusers:send';

    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Active Users';

    /**
    * Create a new command instance.
    *
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
    }

    /**
    * Execute the console command.
    *
    * @return mixed
    */
    public function handle()
    {
        $year = date('Y');
        $month = date('m');

        $subscriber = Subscriber::whereStatus('Active')->whereId(266)->whereActiveUsers('Yes')->first();
        
        \Config::set('database.connections.api.database',$subscriber->database_name);
        \DB::setDefaultConnection('api');
            $caregivers = Schedule::select('caregiver_id')->whereDate('schedule_date','>=',$year.'-'.$month.'-01')
            ->whereDate('schedule_date','<=',$year.'-'.$month.'-31')
            ->where('caregiver_id','>',0)
            ->distinct('caregiver_id')->get();
        \DB::purge('api');
        $data['caregivers'] = $caregivers;
        $data['subscriber'] = $subscriber;
        $pdf = \PDF::loadView('console.active_users.printactiveusers', $data);

        \Mail::send(new SendActiveUsersMail($data, $pdf->output()));


        $subscriber = Subscriber::whereStatus('Active')->whereId(269)->whereActiveUsers('Yes')->first();
        
        \Config::set('database.connections.api.database',$subscriber->database_name);
        \DB::setDefaultConnection('api');
            $caregivers = Schedule::select('caregiver_id')->whereDate('schedule_date','>=',$year.'-'.$month.'-01')
            ->whereDate('schedule_date','<=',$year.'-'.$month.'-31')
            ->where('caregiver_id','>',0)
            ->distinct('caregiver_id')->get();
        \DB::purge('api');
        $data['caregivers'] = $caregivers;
        $data['subscriber'] = $subscriber;
        $pdf = \PDF::loadView('console.active_users.printactiveusers', $data);
    
        \Mail::send(new SendActiveUsersMail($data, $pdf->output()));
       
        $this->info('Statement created Successfully!');
    }
}
