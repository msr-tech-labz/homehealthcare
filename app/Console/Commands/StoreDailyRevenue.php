<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Helpers\Report\MIS;
use App\Entities\DailyRevenueStorage;
use App\Entities\Console\Subscriber;


class StoreDailyRevenue extends Command
{
    /**
    * The name and signature of the console command.
    *
    * @var string
    */
    protected $signature = 'store:dailyrevenue';

    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Store Daily Revenues';

    /**
    * Create a new command instance.
    *
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
    }

    /**
    * Execute the console command.
    *
    * @return mixed
    */
    public function handle()
    {
        $subscribers = Subscriber::active()->whereUserType('Provider')->get();
        if(count($subscribers)){
            foreach ($subscribers as $subscriber) {
                \Config::set('database.connections.api.database',$subscriber->database_name);
                \DB::setDefaultConnection('api');
                
                $todate = MIS::getRevenueByPeriod('Today','');
                //Start storing the data in Revenue Storage
                $drs    = new DailyRevenueStorage;
                $drs->date            = date('Y-m-d', strtotime('-1 days'));
                $drs->gross_todate    = $todate[0];
                $drs->discount_todate = $todate[1];
                $drs->revenue_todate  = $todate[2];
                $drs->taxed_todate    = $todate[3];
                $drs->mcr_todate      = $todate[4];
                $drs->save();
                //End storing the data in Revenue Storage

                //Rectifing Unsorted Daily Revenue Rows Started
                $drsRectifiables = DailyRevenueStorage::whereFlag('Unsorted')->get();
                if(count($drsRectifiables) && isset($drsRectifiables))
                {
                    foreach ($drsRectifiables as $drsRectifiable) {
                        // dd($drsRectifiable);
                        $rectifiableData = MIS::getRevenueByPeriod('Today',$drsRectifiable->date);
                        //Start storing the data in Revenue Storage
                        $drsRectifiable->gross_todate    = $rectifiableData[0];
                        $drsRectifiable->discount_todate = $rectifiableData[1];
                        $drsRectifiable->revenue_todate  = $rectifiableData[2];
                        $drsRectifiable->taxed_todate    = $rectifiableData[3];
                        $drsRectifiable->mcr_todate      = $rectifiableData[4];
                        $drsRectifiable->updated_at      = date('Y-m-d H:i:s');
                        $drsRectifiable->flag            = 'Sorted';

                        $drsRectifiable->save();
                        //End storing the data in Revenue Storage
                    }
                }
                //Rectifing Unsorted Daily Revenue Rows Completed

                \DB::purge('api');
            }
        }

        $this->info('Revenues Stored and Rectified Successfully!');
    }
}
