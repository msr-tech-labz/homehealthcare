<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\MailController;
use App\Entities\Console\Subscriber;

class GenerateMISReportCharts extends Command
{
    /**
    * The name and signature of the console command.
    *
    * @var string
    */
    protected $signature = 'charts:generate';

    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Generate Charts of MIS';

    /**
    * Create a new command instance.
    *
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
    }

    /**
    * Execute the console command.
    *
    * @return mixed
    */
    public function handle()
    {
        $subscribers = Subscriber::active()->whereUserType('Provider')->get();
        if(count($subscribers)){
            foreach ($subscribers as $subscriber) {
                \Config::set('database.connections.api.database',$subscriber->database_name);
                \DB::setDefaultConnection('api');
                $mailers = \Helper::getSubscribersByType('Daily MIS Report');
                if(count($mailers)){
                    MailController::generateMISCharts($subscriber->id);
                }
                \DB::purge('api');
            }
        }

        $this->info('Charts generated successfully!');
    }
}
