<?php
namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class BranchScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {   
        if(\Helper::encryptor('decrypt',session('utype')) != 'Admin' && \Request::header('Accept') != 'application/x.apnacare.v1+json'){
            $builder->where(''.$model['table'].'.branch_id', \Helper::encryptor('decrypt',session('branch_id')));
        }

        $model->addHidden(['branch_id']);
    }
}
