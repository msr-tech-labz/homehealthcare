<?php
namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class TenantScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        if(\Helper::encryptor('decrypt',session('utype')) != 'Aggregator' && \Request::header('Accept') != 'application/x.apnacare.v1+json'){
            $builder->where('tenant_id', \Helper::getTenantID());
        }

        if(isset($model->createdByColumn) && $model->createdByColumn){
            $builder->orWhere('created_by','Default');
        }
        $model->addHidden(['tenant_id']);
    }
}
