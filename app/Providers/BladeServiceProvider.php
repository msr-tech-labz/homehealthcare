<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Blade;

class BladeServiceProvider extends ServiceProvider
{
    public function boot()
    {
		Blade::extend(function($value) {
			return preg_replace('/\{\?(.+)\?\}/', '<?php ${1} ?>', $value);
		});

        Blade::directive('caseNotClosed', function ($expression) {
            return "<?php if(strtoupper($expression) != 'CLOSED'): ?>";
        });

        Blade::directive('endCaseNotClosed', function ($expression) {
            return "<?php endif; ?>";
        });
    }

    public function register()
    {
        //
    }
}
