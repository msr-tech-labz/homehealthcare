<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use App\Entities\Notifications;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        $branchViews = ['*'];

        // Using Closure based composers...
        View::composer($branchViews, function ($view) {
            if(\Auth::user()){
                if(!session(\Helper::getTenantID().'_branches')){
                    $branches = collect(\App\Entities\Masters\Branch::on('tenant')->get());
                    session()->put(\Helper::getTenantID().'_branches',$branches);
                }
                $view->with('branches', session(\Helper::getTenantID().'_branches'));

                $notifications = Notifications::orderBy('created_at','desc')->get();
                View::share('notifications',$notifications);
            }
        });

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
