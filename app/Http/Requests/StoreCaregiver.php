<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCaregiver extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:255',
            'gender' => 'required',
            'mobile_number' => 'required',
            'email' => 'email',
            'current_address' => 'required',
            'current_city' => 'required',
            'current_state' => 'required',
            'permanent_address' => 'required',
            'permanent_city' => 'required',
            'permanent_state' => 'required',
            'experience' => 'required|numeric',
            'role' => 'required',
        ];
    }
}
