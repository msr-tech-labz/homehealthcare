<?php

namespace App\Http\Requests\Administration;

use Illuminate\Foundation\Http\FormRequest;

class StoreReferralSource extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'source_name' => 'required|max:255',
            // 'phone_number' => 'required|unique:master_referral_source,phone_number,'.\Helper::encryptor('decrypt',$this->id).',id'
        ];
    }
}
