<?php

namespace App\Http\Controllers;

define("PCHART_PATH", './app/Pchart');
set_include_path(get_include_path() . PATH_SEPARATOR . PCHART_PATH);

require_once "class/pDraw.class.php";
require_once "class/pImage.class.php";
require_once "class/pData.class.php";
require_once "class/pPie.class.php";

use App\Helpers\Report\MIS;

use Illuminate\Http\Request;

class MailController extends Controller
{
   public static function generateMISCharts($subID){
      //FOR REVENUE
      $todate = MIS::getRevenueByPeriod('Today','');
      $mtd    = MIS::getRevenueByPeriod('MTD','');
      $ytd    = MIS::getRevenueByPeriod('YTD','');
      $myData = new \pData(); 
      $myData->addPoints($todate,"TODATE");
      $myData->addPoints($mtd,"MTD");
      $myData->addPoints($ytd,"YTD");
      $myData->setAxisName(0,"Amount in Rupees");
      $myData->addPoints(array("Gross","Discount","Revenue","Taxed","Credit Memo"),"Labels");
      $myData->setAbscissa("Labels");

      $myData->setSerieDescription("My Serie 1","TODATE");
      $myData->setSerieDescription("My Serie 2","MTD");
      $myData->setSerieDescription("My Serie 3","YTD");

      $myData->setPalette("TODATE",array("R"=>255,"G"=>153,"B"=>51));
      $myData->setPalette("MTD",array("R"=>56,"G"=>133,"B"=>157));
      $myData->setPalette("YTD",array("R"=>60,"G"=>179,"B"=>113));

      $myImage = new \pImage(600, 300, $myData);
      /* Write a legend box */ 

      $myImage->setFontProperties(array(
      "FontName" => PCHART_PATH . "/fonts/GeosansLight.ttf",
      "FontSize" => 10));
      $myImage->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));
      //Legend position in the first two params
      $myImage->drawLegend(200,10,array("Style"=>LEGEND_ROUND,"Mode"=>LEGEND_HORIZONTAL));
      //set graph area by margin to accomodate Y-axis name 
      $myImage->setGraphArea(75,75,575,275);
      $myImage->drawScale(array("Mode"=>SCALE_MODE_START0,"GridR"=>0,"GridG"=>0,"GridB"=>0));
      $myImage->drawBarChart(array("DisplayValues" => FALSE));
      header("Content-Type: image/png");
      //Render Null to Render directly on the screen
      if (!file_exists(public_path().DIRECTORY_SEPARATOR.'MISCHARTS'.DIRECTORY_SEPARATOR.$subID)) {
          mkdir(public_path().DIRECTORY_SEPARATOR.'MISCHARTS'.DIRECTORY_SEPARATOR.$subID, 0777, true);
          chmod(public_path().DIRECTORY_SEPARATOR.'MISCHARTS'.DIRECTORY_SEPARATOR.$subID,0777);
      }
      $filename = date('Y-m-d').'-RevenueChart.png';
      $myImage->Render(public_path().DIRECTORY_SEPARATOR.'MISCHARTS'.DIRECTORY_SEPARATOR.$subID.DIRECTORY_SEPARATOR.$filename);
      chmod(public_path().DIRECTORY_SEPARATOR.'MISCHARTS'.DIRECTORY_SEPARATOR.$subID.DIRECTORY_SEPARATOR.$filename, 0777);

      //FOR LEADS
      $todate = MIS::getLeadsByPeriod('Today','');
      $mtd    = MIS::getLeadsByPeriod('MTD','');
      $ytd    = MIS::getLeadsByPeriod('YTD','');
      $myData = new \pData(); 
      $myData->addPoints($todate,"TODATE");
      $myData->addPoints($mtd,"MTD");
      $myData->addPoints($ytd,"YTD");
      $myData->setAxisName(0,"Count");
      $myData->addPoints(array("Pending","Converted","Closed","Dropped"),"Labels");
      $myData->setAbscissa("Labels");

      $myData->setSerieDescription("My Serie 1","TODATE");
      $myData->setSerieDescription("My Serie 2","MTD");
      $myData->setSerieDescription("My Serie 3","YTD");

      $myData->setPalette("TODATE",array("R"=>255,"G"=>153,"B"=>51));
      $myData->setPalette("MTD",array("R"=>56,"G"=>133,"B"=>157));
      $myData->setPalette("YTD",array("R"=>60,"G"=>179,"B"=>113));

      $myImage = new \pImage(600, 300, $myData);
      /* Write a legend box */ 

      $myImage->setFontProperties(array(
      "FontName" => PCHART_PATH . "/fonts/GeosansLight.ttf",
      "FontSize" => 10));
      $myImage->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));
      //Legend position in the first two params
      $myImage->drawLegend(200,10,array("Style"=>LEGEND_ROUND,"Mode"=>LEGEND_HORIZONTAL));
      //set graph area by margin to accomodate Y-axis name 
      $myImage->setGraphArea(75,75,575,275);
      $myImage->drawScale(array("Mode"=>SCALE_MODE_START0,"GridR"=>0,"GridG"=>0,"GridB"=>0));
      $myImage->drawBarChart(array("DisplayValues" => FALSE));
      header("Content-Type: image/png");
      //Render Null to Render directly on the screen
      if (!file_exists(public_path().DIRECTORY_SEPARATOR.'MISCHARTS'.DIRECTORY_SEPARATOR.$subID)) {
          mkdir(public_path().DIRECTORY_SEPARATOR.'MISCHARTS'.DIRECTORY_SEPARATOR.$subID, 0777, true);
          chmod(public_path().DIRECTORY_SEPARATOR.'MISCHARTS'.DIRECTORY_SEPARATOR.$subID,0777);
      }
      $filename = date('Y-m-d').'-LeadsChart.png';
      $myImage->Render(public_path().DIRECTORY_SEPARATOR.'MISCHARTS'.DIRECTORY_SEPARATOR.$subID.DIRECTORY_SEPARATOR.$filename);
      chmod(public_path().DIRECTORY_SEPARATOR.'MISCHARTS'.DIRECTORY_SEPARATOR.$subID.DIRECTORY_SEPARATOR.$filename, 0777);

      //FOR HR
      $data = MIS::hrStats(['date' => date('Y-m-d',strtotime('-1 days'))]);
      if($data['total'] != 0){
            $myData = new \pData();

            /* Data definition */
            $myData->addPoints(array($data['available'],
                                     $data['onduty'],
                                     $data['training'],
                                     $data['leave'],
                                     $data['absent']),"Value");  
            /* Labels definition */
            $myData->addPoints(array("Available","Onduty","Training","Leave","Absent"),"Labels");
            $myData->setAbscissa("Labels");

            $myImage = new \pImage(600, 300, $myData);

            $myImage->setFontProperties(array(
            "FontName" => PCHART_PATH . "/fonts/GeosansLight.ttf",
            "FontSize" => 15,
            "R"=>80,"G"=>80,"B"=>80));
            
            $myImage->setFontProperties(array(
            "FontName"=> PCHART_PATH . "/fonts/GeosansLight.ttf",
            "FontSize"=>15,
            "R"=>80,"G"=>80,"B"=>80));

            $PieChart = new \pPie($myImage,$myData);

            $PieChart->draw2DPie(340,170,array("Radius"=>100,"DrawLabels"=>TRUE,"Border"=>TRUE,"WriteValues"=>PIE_VALUE_PERCENTAGE,"ValueR"=>255,"ValueG"=>255,"ValueB"=>255,"ValueAlpha"=>100,"ValuePosition"=>PIE_VALUE_INSIDE));
            $PieChart->drawPieLegend(15,40,array("Alpha"=>20));

            header("Content-Type: image/png");
            //Render Null to Render directly on the screen
            if (!file_exists(public_path().DIRECTORY_SEPARATOR.'MISCHARTS'.DIRECTORY_SEPARATOR.$subID)) {
                mkdir(public_path().DIRECTORY_SEPARATOR.'MISCHARTS'.DIRECTORY_SEPARATOR.$subID, 0777, true);
                chmod(public_path().DIRECTORY_SEPARATOR.'MISCHARTS'.DIRECTORY_SEPARATOR.$subID,0777);
            }
            $filename = date('Y-m-d').'-HrChart.png';
            $myImage->Render(public_path().DIRECTORY_SEPARATOR.'MISCHARTS'.DIRECTORY_SEPARATOR.$subID.DIRECTORY_SEPARATOR.$filename);
            chmod(public_path().DIRECTORY_SEPARATOR.'MISCHARTS'.DIRECTORY_SEPARATOR.$subID.DIRECTORY_SEPARATOR.$filename, 0777);
      }
   }
}