<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Entities\WorkTimeMailers;
use App\Entities\Masters\Service;
use App\Entities\Masters\Language;
use App\Entities\Masters\Specialization;
use App\Entities\Masters\Mailers;

use App\Entities\User;
use App\Jobs\SendWorkTimeMail;
use App\Jobs\WorkTime2;
use App\Entities\LoggingEvents;
use App\Entities\Lead;
use App\Entities\CaseServices;
use App\Entities\AggregatorLeads;
use App\Entities\Patient;
use App\Entities\Schedule;
use App\Entities\Invoice;
use App\Entities\ServiceInvoice;
use App\Entities\CasePayment;
use App\Entities\CreditMemo;
use App\Entities\CaseServiceOrder;
use App\Entities\Careplan;
use App\Entities\CaseDisposition;
use App\Entities\LeadRequests;
use App\Entities\Profile;
use App\Entities\Caregiver\Caregiver;
use App\Entities\Attendance;
use App\Entities\LeavesOld;
use App\Entities\Leaves;
use App\Entities\Appraisal;
use App\Entities\Masters\ReferralSource;
use App\Entities\ReferralPayout;
use App\Entities\Masters\Department;
use App\Entities\Console\Subscriber;

use App\DataTables\OutstandingDataTable;

use App\Mail\Aggregator\DailyStats;

use App\Helpers\Report\MIS;
use DB;
use Carbon\Carbon;

class ReportsController extends Controller
{
    public function index()
    {
        return view('reports.index');
    }

    function resourceStats(Request $request)
    {
        $data['providers'] = Profile::withoutGlobalScopes()->whereNull('deleted_at')->get();
        $data['caregivers'] = [];
        $data['provider_id'] = 0;

        if($request->isMethod('post')){
            $caregivers = Caregiver::withoutGlobalScopes()->whereNull('deleted_at');

            if(\Helper::encryptor('decrypt',session('utype')) == 'Aggregator'){
                if($request->filter_provider && $request->filter_provider != ""){
                    $data['provider_id'] = $request->filter_provider;
                    $caregivers->whereTenantId($request->filter_provider);
                }
            }

            $data['caregivers'] = $caregivers->get();
        }

        return view('reports.resourceStats', $data);
    }

    public function casesConsolidatedReport(Request $request)
    {
        $schedules = Schedule::withoutGlobalScopes();

        if($request->start_date && $request->end_date){
            $fromDate = date_format(new \DateTime($request->start_date),"Y-m-d");
            $toDate = date_format(new \DateTime($request->end_date),"Y-m-d");

            $schedules->whereRaw(\DB::raw('DATE(schedule_date) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'));
        }

        if($request->filter_status && $request->filter_status != ""){
            $schedules->where('status',$request->filter_status);
        }

        $schedules = $schedules->get();
        return view('reports.casesConsolidated', compact('schedules'));
    }

    /*
    *   Financial report functions
    */
    public function outstanding(Request $request, OutstandingDataTable $dataTable)
    {
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 3600);
        
        if(!empty($request->till_date)){
            $filterTillDate = date_format(new \DateTime($request->till_date),"Y-m-d");
        }else if(!empty(session('till_date'))){
            $filterTillDate = session('till_date');
        }else{
            $filterTillDate = date('Y-m-d');   
        }

        session()->put('till_date',$filterTillDate);

        return $dataTable->render('reports.financial.outstanding',compact('filterTillDate'));
    }

    public function revenueWithSchedules(Request $request)
    {
        $patients = Patient::select('id','patient_id','first_name','last_name')->whereNotNull('patient_id')->get();

        if($request->ajax){
            return \App\Helpers\Report\Financial::revenueWithSchedules($request);
        }

        return view('reports.financial.revenue-schedules',compact('patients'));
    }
    
    public function manualCreditMemos(Request $request)
    {
        $patients = Patient::select('id','patient_id','first_name','last_name')->whereNotNull('patient_id')->get();

        if($request->ajax){
            return \App\Helpers\Report\Financial::manualCreditMemoReport($request);
        }

        return view('reports.financial.manual-creditmemo',compact('patients'));
    }

    public function statementOfAccount(Request $request)
    {
        $data = [];
        $period = "";
        if($request->isMethod('post')){
            $period = $request->filter_period;
            if(!$period)
                return back()->with('alert_info','Please select period!');
            
            $data = \App\Helpers\Report\Financial::statementOfAccount($period);
        }
        
        return view('reports.financial.statement-of-account', compact('data','period'));
    }
    
    public function invoiceReport(Request $request)
    {
        $patients = Patient::select('id','patient_id','first_name','last_name')->whereNotNull('patient_id')->get();
        if($request->ajax){
            return \App\Helpers\Report\Financial::invoiceReport($request);
        }
        return view('reports.financial.invoiceReport',compact('patients'));
    }

    public function invoiceReportWithService(Request $request)
    {
        $patients = Patient::select('id','patient_id','first_name','last_name')->whereNotNull('patient_id')->get();
        if($request->ajax){
            return \App\Helpers\Report\Financial::invoiceReportWithService($request);
        }
        return view('reports.financial.invoiceReportWithService',compact('patients'));
    }

    public function receiptReport(Request $request)
    {
        $patients = Patient::select('id','patient_id','first_name','last_name')->whereNotNull('patient_id')->get();
        if($request->ajax){
            return \App\Helpers\Report\Financial::receiptReport($request);
        }
        return view('reports.financial.receiptReport',compact('patients'));
    }
    
    public function soaEmail(Request $request)
    {
        if($request->ajax()){
            $fromDate = $toDate = "";
            $patientIDs = $request->patient_ids;
            if($request->has('period') && !empty($request->period)){
                $fromDate = explode("_",$request->period)[0];
                $toDate = explode("_",$request->period)[1];
            }
            if(count($patientIDs)){
                foreach ($patientIDs as $patient) {
                    \App\Jobs\SendSOA::dispatch(Patient::on('tenant')->whereId($patient)->first(), $fromDate, $toDate, \Helper::getTenantID(), \Helper::getBranchID());
                }                
            }
        }
        
        return response()->json(true, 200);
    }

    /*
    *   HR report functions
    */
    public function hrMasterDataReport(Request $request)
    {
        $data['specializations'] = \App\Entities\Masters\Specialization::select('specialization_name')->get();
        $data['designations'] = \App\Entities\Masters\Designation::select('designation_name')->get();
        $data['departments'] = \App\Entities\Masters\Department::select('department_name')->get();
        $data['managers'] = \App\Entities\Caregiver\Caregiver::select('employee_id','first_name','middle_name','last_name')
                                ->active()->whereHas('professional', function ($q){
                                    $q->whereRole('Manager');
                                })->get();
        $data['employees'] = \App\Helpers\Report\HR::masterData($request);

        return view('reports.hr.master-data', $data);
    }

    public function attendanceReport(Request $request)
    {
        $data = \App\Helpers\Report\HR::attendanceReport($request);

        return view('reports.hr.attendance-report',$data);
    }

    public function employeeWorkTimeReport1(Request $request)
    {
        $worktimeJobs = WorkTimeMailers::orderBy('created_at','desc')->get();
        if($request->ajax){
            ini_set('memory_limit', '1024M'); // or you could use 1G
            ini_set('max_execution_time', '3000'); // or you could use 1G
            $data = collect();
            if($request->filter_month && $request->filter_year) {
                $counter = 0;
                for($i=1;$i<=31;$i++) {
                    if ($i < 10) {
                        $i = str_pad($i, 2, "0", STR_PAD_LEFT);
                    }
                    $userAttendance = \App\Entities\Attendance::with('caregiver','schedule')->whereNotNull('schedule_id')
                        ->where('date',date($request->filter_year.'-'.$request->filter_month.'-'.$i))
                        // ->whereNotNull('punch_in')->whereNotNull('punch_out')
                        ->get();
                    if(count($userAttendance)){
                        foreach ($userAttendance as $index => $value) {
                            if(isset($value['punch_in']) && isset($value['punch_out'])){
                                $from = strtotime($value['punch_in']);
                                $to = strtotime($value['punch_out']);

                                $init = ($to - $from);
                                $hours = floor($init / 3600);
                                $minutes = floor(($init / 60) % 60);

                                if($init == 0 ){
                                    $res = 24;
                                }else if($init < 0 ){
                                    if($minutes == 0){
                                        $res = 24 - abs($hours);
                                    }else{
                                        $res = (24 - abs($hours)).':'.(60 - abs($minutes));
                                    }
                                }else{
                                    if($minutes == 0){
                                        $res = $hours;
                                    }else{
                                        $res = $hours.':'.$minutes;
                                    }
                                }
                            }else{
                                $res = 'NA';
                            }

                            $data->push([
                                'srno' => (string) ++$counter,
                                'date'  => date($i.'-'.$request->filter_month.'-'.$request->filter_year),
                                'id'    => $value->caregiver->employee_id,
                                'name'  => $value->caregiver->full_name,
                                'cadre'    => isset($value->caregiver->professional->designations)?$value->caregiver->professional->designations->designation_name:'NA',
                                'service'    => $value->schedule->service->service_name,
                                'chargeable'    => ($value->schedule->chargeable == 1)?'Yes':'No',
                                'from'  => isset($value->punch_in)?\Carbon\Carbon::parse($value->punch_in)->format('h:i A'):'NA',
                                'to'    => isset($value->punch_out)?\Carbon\Carbon::parse($value->punch_out)->format('h:i A'):'NA',
                                'hours' => (string) $res,
                            ]);
                        }
                    }
                }
            }
            return \DataTables::of($data)->make(true);
        }
        return view('reports.hr.employee-worktime-report1',compact('worktimeJobs'));
    }

    public function employeeWorkTimeReport2(Request $request)
    {
        $employeeWorkTime = collect();
        $filter_month = $filter_year = '';
        if($request->filter_month && $request->filter_year) {
            ini_set('memory_limit', '1024M'); // or you could use 1G
            ini_set('max_execution_time', '3000'); // or you could use 1G
            $filter_month = $request->filter_month;
            $filter_year = $request->filter_year;
            $data3 = collect();
            $caregivers = Caregiver::get();
            foreach ($caregivers as $key => $caregiver) {
                $data2 = collect();
                for($i=1;$i<=31;$i++) {
                    if ($i < 10) {
                        $i = str_pad($i, 2, "0", STR_PAD_LEFT);
                    }
                    $userAttendance = Attendance::whereCaregiverId($caregiver->id)->where('date',date($filter_year.'-'.$filter_month.'-'.$i))->get();
                    if(count($userAttendance)){
                        $multipleShifts = '';
                        foreach ($userAttendance as $index => $value) {
                            if($value['schedule_id'] != null){
                                if(isset($value['punch_in']) && isset($value['punch_out'])){
                                    $from = strtotime($value['punch_in']);
                                    $to = strtotime($value['punch_out']);

                                    $init = ($to - $from);
                                    $hours = floor($init / 3600);
                                    $minutes = floor(($init / 60) % 60);

                                    if($init == 0 ){
                                        $hrs = 24;
                                    }else if($init < 0 ){
                                        if($minutes == 0){
                                            $hrs = 24 - abs($hours);
                                        }else{
                                            $hrs = (24 - abs($hours)).':'.(60 - abs($minutes));
                                        }
                                    }else{
                                        if($minutes == 0){
                                            $hrs = $hours;
                                        }else{
                                            $hrs = $hours.':'.$minutes;
                                        }
                                    }

                                    if($hrs < 15){
                                        if($from <= strtotime('12:01:00') && $to >= strtotime('12:01:00')){
                                            $shift = 'd';
                                        }else{
                                            $shift = 'n';
                                        }
                                    }else{
                                        $shift = '';
                                    }
                                }else{
                                    $hrs = '-';
                                    $shift = '';
                                }
                                if($index!=0) $comaSeperate = ',';
                                else $comaSeperate = '';
                                $multipleShifts = $multipleShifts.$comaSeperate.(string) $hrs.$shift;
                                $data2['day_'.$i] = $multipleShifts;
                            }else{
                                $data2['day_'.$i] = $value['status'].' '.\Helper::checkEmpType($caregiver->id,$i,$filter_month,$filter_year,$value['status']);
                            }                            
                        }
                    }else{
                        $userLeave = Leaves::whereCaregiverId($caregiver->id)->where('date',date($filter_year.'-'.$filter_month.'-'.$i))->whereStatus('Approved')->pluck('status')->first();
                        if($userLeave == 'Approved') $data2['day_'.$i] = 'L';
                    }
                }
                if(count($data2)){
                    $data3['name']  = $caregiver->full_name;
                    $data3['empolyee_id'] = $caregiver->employee_id;
                    $designation = $caregiver->professional->designation;
                    if($designation){
                        $data3['designation'] = $caregiver->professional->designations->designation_name;
                    }else{
                        $data3['designation'] = "-";
                    }
                    $data3['id'] = $caregiver->id;
                    $employeeWorkTime->push(array_merge(end($data3),end($data2)));
                }
                
            }
        }
        return view('reports.hr.employee-worktime-report2',compact('filter_month','filter_year','employeeWorkTime'));
    }

    public function employeeWorkTimeReportOld(Request $request)
    {
        $worktimeJobs = WorkTimeMailers::orderBy('created_at','desc')->get();
        if($request->filter_month && $request->filter_year && $request->email){
            SendWorkTimeMail::dispatch($request->all(),\Helper::getTenantID());
            sleep(3);
            return redirect()->back()->with('alert_success','We have initiated an email to the id...Please check the status below');
        }
        return view('reports.hr.employee-worktime-report-old',compact('worktimeJobs'));
    }

    public function leavesStatsReport(Request $request)
    {
        $data = \App\Helpers\Report\HR::leavesStatsReport($request);

        return view('reports.hr.leaves-stats-report',$data);
    }

    public function monthlyAttendanceReport(Request $request)
    {
        ini_set('memory_limit', '1024M'); // or you could use 1G
        ini_set('max_execution_time', '3000'); // or you could use 1G
        if($request->filter_month && $request->filter_year){
            $month = $request->filter_month;
            $year = $request->filter_year;
        }else{
            $month = date('m');
            $year = date('Y');
        }
        $caregivers = collect();
        $employees = Caregiver::select('id','employee_id','first_name','middle_name','last_name')->get();
        foreach ($employees as $srno => $employee){
            $present_on_schedule = array();
            $present_on_bench = array();
            $absent = array();
            $on_leave = array();

            $attendances = Attendance::select('date','schedule_id','status')
                // ->with('schedule')
                ->whereCaregiverId($employee->id)
                ->whereMonth('date',$month)
                ->whereYear('date',$year)
                ->whereNull('deleted_at')
                ->whereIn('status',['P','A'])
                ->orderBy('date','asc')
                ->get();
            if(count($attendances)){
                foreach ($attendances as $attendance){
                    if($attendance->schedule_id != null){
                        $present_on_schedule[] = Carbon::parse($attendance->date)->format('d');
                    } else {
                        if($attendance->status == 'P'){
                            $present_on_bench[] = Carbon::parse($attendance->date)->format('d');
                        }
                        if($attendance->status == 'A'){
                            $absent[] = Carbon::parse($attendance->date)->format('d');
                        }
                    }                            
                }
            }

            $leaves = Leaves::select('date','type')
                ->whereCaregiverId($employee->id)
                ->whereBetween('date',[date($year.'-'.$month.'-1'),date($year.'-'.$month.'-31')])
                ->whereStatus('Approved')
                ->where('deleted_at','=',null)
                ->get();
            if(count($leaves)){
                foreach ($leaves as $leave){
                    if(isset($leave->type)){
                        $leave_type = $leave->type;
                        $on_leave[] = Carbon::parse($leave->date)->format('d-M').'('.$leave_type.')';
                    }
                }
            }

            if(count($present_on_schedule) == 0 && count($present_on_bench) == 0 && count($absent) == 0 && count($on_leave) == 0){
                continue;
            }
            if(count($present_on_schedule) == 0){$present_on_schedule[] = ['-'];}
            if(count($present_on_bench) == 0){$present_on_bench[] = ['-'];}
            if(count($absent) == 0){$absent[] = ['-'];}
            if(count($on_leave) == 0){$on_leave[] = ['-'];}

            $caregivers->push([
                'srno' => ++$srno,
                'id' => $employee->employee_id,
                'name' => $employee->full_name,
                'present_on_schedule' => implode(', ',array_flatten($present_on_schedule)),
                'present_on_bench' => implode(', ',array_flatten($present_on_bench)),
                'absent' => implode(', ',array_flatten($absent)),
                'on_leave' => implode(', ',array_flatten($on_leave)),
            ]);
        }
        return view('reports.hr.monthly-attendance-report',compact('caregivers','month','year'));
    }

    public function attendanceSummary(Request $request)
    {
        $data = \App\Helpers\Report\HR::attendanceSummary($request);

        return view('reports.hr.attendance-summary',compact('data'));
    }


    /*
    *   Leads report functions
    */
    public function leadsMasterDataReport(Request $request)
    {
        $leads = \App\Helpers\Report\Leads::masterData($request);

        return view('reports.leads.master-data',compact('leads'));
    }
    
    public function dailyStats(Request $request)
    {

        if(!empty($request->filter_period)){
            $period = explode("_",$request->filter_period);                
            $fromDate = $period[0];
            $toDate = $period[1];
        }else{
            $fromDate = $toDate = date('Y-m-d');
        }

        $data = \App\Helpers\Report\Leads::dailyStats($fromDate,$toDate);
        if($request->mail){
            $mail = \Mail::to('nitish@apnacare.in');
            $mail->send(new DailyStats($data,$fromDate,$toDate));
            return response()->json(200);
        }
        return view('reports.leads.daily-stats',compact('data','fromDate','toDate'));
    }

    /*
    *   Clinical report functions
    */
    public function scheduleData(Request $request)
    {
        $patients = Patient::select('id','patient_id','first_name','last_name')->whereNotNull('patient_id')->get();

        if($request->ajax){
            return \App\Helpers\Report\Clinical::allSchedulesData($request);
        }

        return view('reports.clinical.schedules-data',compact('patients'));
    }

    public function serviceSummary(Request $request)
    {
        $summary = \App\Helpers\Report\Clinical::serviceSummary($request);
        return view('reports.clinical.service-summary',compact('summary'));
    }

    public function renewableServiceOrders(Request $request)
    {
        if($request->ajax){
            return \App\Helpers\Report\Clinical::renewableServiceOrders($request, true);
        }
        return view('reports.clinical.renewable-service-orders');
    }

    public function uncnfirmedSchedules(Request $request)
    {
        if($request->ajax){
            return \App\Helpers\Report\Clinical::unconfirmedSchedules($request, true);
        }

        return view('reports.clinical.unconfirmed-schedules',compact('leads'));
    }

    public function billables(Request $request)
    {
        $patients = Patient::select('id','patient_id','first_name','last_name')->whereNotNull('patient_id')->get();
        if($request->ajax){
            return \App\Helpers\Report\Clinical::getBillablesReport($request);
        }
        return view('reports.clinical.billables',compact('patients'));
    }
    
    public function loginSummary(Request $request)
    {
        if(!empty($request->filter_period)){
            $period = explode("_",$request->filter_period);                
            $fromDate = $period[0];
            $toDate = $period[1];
        }else{
            $fromDate = $toDate = date('Y-m-d');
        }

        $tenantId = \Helper::encryptor('decrypt',$request->tenant_id);

        $summary = \App\Helpers\Report\Activity::loginSummary($tenantId,$fromDate,$toDate);
        $subscriberList = Subscriber::whereStatus('Active')->get();
        foreach($subscriberList as $subscriber){
            self::setTenantConnection($subscriber->id);
            $profile = Profile::withoutGlobalScopes()->whereTenantId($subscriber->id)->first();
            $profileArray[] = [
                'tenant_id' => $profile->tenant_id,
                'organization_name' => $profile->organization_name,
            ];

            \DB::purge('api');
        }
        return view('reports.activity.login-summary',compact('summary','tenantId','fromDate','toDate','profileArray'));
    }

    public function misReport(Request $request)
    {       
        if($request->isMethod('post')){
            $gType = $request->graph;
            $result = ['result' => false];
            if($gType){
                switch ($gType) {
                    case 'revenue': $data = \App\Helpers\Report\MIS::revenueStatsNew($request->only('date')); break;
                    case 'lead': $data = \App\Helpers\Report\MIS::leadStats($request->only('date')); break;
                    case 'hr': $data = \App\Helpers\Report\MIS::hrStats($request->only('date')); break;
                    case 'services': $data = \App\Helpers\Report\MIS::servicesStats($request->only('date')); break;
                    default: $data = []; break;
                }

                if(count($data)){
                    $result['result'] = true;
                    $result['data'] = $data;
                }
            }
            
            return response()->json($result, 200);
        }
        
        return view('reports.mis');
    }

    public function logineventReport(Request $request)
    {
           
        $start = new \DateTime(date('Y').'-'.date('m').'-01 00:00:00');
        $end = new \DateTime(date('Y-m-d H:i:s'));
        $events = LoggingEvents::where('created_at', '>=', $start)->where('created_at', '<=', $end)->get();

        if($request->filter_period || $request->filter_user){
            $events = LoggingEvents::select('*');
            if(!empty($request->filter_user)){
                $events->whereUserId($request->filter_user);
            }

            if(!empty($request->filter_period)){
                $period = explode("_",$request->filter_period);                
                $fromDate = $period[0];
                $start = new \DateTime($fromDate.' 00:00:00');
                $toDate = $period[1];
                $end = new \DateTime($toDate.' 23:59:59');
                $events->where('created_at', '>=', $start)->where('created_at', '<=', $end);
            }
            $events  = $events->get();
        }
    
        
        $users = User::where('user_id','!=',0)->get();
        return view('reports.loginevnets',compact('events','users'));
    }

    public function benchForecast(Request $request)
    {   
        $start = new \DateTime(date('Y-m-d'));
        $end   = new \DateTime(date('Y-m-d',strtotime('+4 days')));

        $period = new \DatePeriod($start,new \DateInterval('P1D'),$end);
        $benchData = [];

        $caregivers = Caregiver::select('id','first_name','middle_name','last_name','employee_id')->active()->
            where('work_status','!=','Training')->
            whereHas('professional', function($q){
                    $q->whereRoleOrDeployable('Caregiver','Yes');
            });
        if(\Helper::getTenantId() == 266){ // if onelife
            $caregivers->whereHas('professional.designations', function($q){
                    $q->whereIn('id',[12,16,17,18]);
            });
        }elseif(\Helper::getTenantId() == 269){ // if aajicare
            $caregivers->whereHas('professional.designations', function($q){
                    $q->whereIn('id',[23]);
            });
        }        
        foreach ($period as $key => $date) {
            //Clone obj to retain the original ids
            $dayCaregivers = clone $caregivers;
            //Get Scheduled IDs, On Leave IDs and Absent IDs
            $day_scheduled_ids = Schedule::select('caregiver_id')->distinct('caregiver_id')->whereDate('schedule_date',$date)->whereIn('status',['Pending'])
                                ->whereNotNull('caregiver_id')->where('caregiver_id','!=','0')->pluck('caregiver_id')->toArray();
            $on_leave_ids = Leaves::select('caregiver_id')->whereDate('date',$date)->whereIn('status',['Pending','Approved'])->pluck('caregiver_id')->toArray();
            $absent_ids = Attendance::select('caregiver_id')->whereDate('date',$date)->whereStatus('A')->pluck('caregiver_id')->toArray();
            //Merge all the IDs
            $merged_Ids_Arr = array_unique(array_merge($day_scheduled_ids, $on_leave_ids, $absent_ids));
            //Remove the merged IDs from the cloned obj to get the bench staff data
            $dayCaregivers->whereNotIn('caregivers.id',array_values($merged_Ids_Arr));
            
            $caregiversArr = $dayCaregivers->get();
            if(count($caregiversArr)){
                $benchData[$date->format('d-m-Y').'_'.count($caregiversArr)] = [];
                foreach($caregiversArr as $r){
                    $designation = (isset($r->professional) && isset($r->professional->designations))?$r->professional->designations->designation_name:'NA';
                    $benchData[$date->format('d-m-Y').'_'.count($caregiversArr)]['benchers'][] = '('.$r->employee_id.') '.$r->full_name.' - '.$designation;
                }
            }
        }

        return view('reports.activity.bench-forecast',compact('benchData'));
    }
    
    public function uploadChartImg(Request $request)
    {
        $tenantID = $request->tenant_id;
        $type = $request->type;
        $img = $request->imgBase64;
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $fileData = base64_decode($img);
        //saving
        $path = public_path('uploads/provider/'.$tenantID.'/temp_chart/');
        if(!is_dir($path)){
            \File::makeDirectory($path,0777,true);
        }
        $fileName = $type.'_'.date('Ymd').'.png';
        file_put_contents($path.$fileName, $fileData);
        
        return response()->json(['path' => asset('uploads/provider/'.$tenantID.'/temp_chart/'.$fileName)],200);
    }

    public function referralCommissionReport(Request $request)
    {        
        $source_names = ReferralSource::select('id','source_name')->get();

        if($request->ajax){
            $data = collect();

            if($request->filter_month && $request->filter_year) {
                $service_invoices = ServiceInvoice::whereMonth('invoice_date',$request->filter_month)
                    ->whereYear('invoice_date',$request->filter_year)
                    ->distinct('patient_id')->get();

                foreach ($service_invoices as $service_invoice) {
                    $case_referral_payout = ReferralPayout::with(['source','source.category'])
                        ->wherePatientId($service_invoice->patient_id);
                    if($request->filter_source) {
                        $case_referral_payout->whereSourceId($request->filter_source);
                    }
                    $case_referral_payouts = $case_referral_payout->get();
                    foreach ($case_referral_payouts as $crp){

                        if($crp->type == 'P') {
                            $referral_value = $crp->value.'(%)';
                            $commission_amount = ($service_invoice->total_amount * $crp->value)/100;
                        } else {
                            $referral_value = $crp->value.'(Rs.)';
                            $commission_amount = $crp->value;
                        }

                        $data->push([
                            'source_name' => $crp->source->source_name,
                            'referrer_name' => isset($crp->source->referrer_name)?$crp->source->referrer_name:'NA',
                            'full_name' => $crp->patient->full_name,
                            'category_name' => $crp->source->category->category_name,
                            'invoice_date' => $service_invoice->invoice_date->format('d-m-Y'),
                            'invoice_amount' => $service_invoice->total_amount,
                            'referral_value' => $referral_value,
                            'commission_amount' => $commission_amount,
                        ]);
                    }
                }
            }
            return \DataTables::of($data)->make(true);
        }
        return view('reports.billing.referralCommission', compact('source_names'));
    }

    public function conveyance(Request $request)
    {
        if($request->ajax){
            $data = collect();
            if($request->filter_month && $request->filter_year) {
                $schedules = Schedule::whereStatus('Completed')
                    ->with(['lead','patient','caregiver','service'])
                    ->whereMonth('schedule_date',$request->filter_month)
                    ->whereYear('schedule_date',$request->filter_year)
                    ->get();
                foreach ($schedules as $schedule){
                    $data->push([
                        'date' => $schedule->schedule_date->format('d-m-Y'),
                        'episode_id' => $schedule->lead->episode_id,
                        'designation' => isset($schedule->caregiver->professional->designations)?$schedule->caregiver->professional->designations->designation_name:'NA',
                        'patient_name' => $schedule->patient->full_name,                        
                        'service_offered' => $schedule->service->service_name,
                        'employee_id' => $schedule->caregiver->employee_id,
                        'employee_name' => $schedule->caregiver->full_name,
                        'chargeable' => ($schedule->chargeable==1)?'Yes':'No',
                        'revenue' => ($schedule->chargeable==1)?$schedule->amount:'0.00',
                        'staff_address' => $schedule->caregiver->current_address.' '.$schedule->caregiver->current_area.' '.$schedule->caregiver->current_city,
                        'patient_address' => $schedule->patient->street_address.' '.$schedule->patient->area.' '.$schedule->patient->city,
                        'delivery_address' => \Helper::getDeliveryAddress($schedule->patient_id,$schedule->delivery_address),
                        'distance' => isset($schedule->road_distance)?$schedule->road_distance:'NA',
                    ]);
                }
            }
            return \DataTables::of($data)->make(true);
        }
        return view('reports.leads.conveyance');
    }

    public function leavesReport(Request $request)
    {
        $filters = $request->all('filter_user','filter_month','filter_year');

        $filter = explode("_",$filters['filter_user']);

        $month = !empty($filters['filter_month'])?$filters['filter_month']:date('m');
        $year = !empty($filters['filter_year'])?$filters['filter_year']:date('Y');

        $caregiver = Caregiver::branch()->select('id','first_name','middle_name','last_name');

        $employees = $caregiver->get();

        $departments = Department::branch()->get();

        if(!empty($filters['filter_user'])){
            if($filter[0] == 'dept'){
                $caregiver->whereHas('professional', function($q) use($filter){
                    $q->whereDepartment($filter[1]);
                });
            }

            if($filter[0] == 'emp'){
                $caregiver->whereId($filter[1]);
            }
        }

        $caregivers = $caregiver->get();

        $leaves = Leaves::branch()->whereIn('caregiver_id',$caregivers->pluck('id')->toArray())->whereMonth('date',$month)->whereYear('date',$year)->get();
        return view('reports.hr.leavesReport',compact('leaves','departments','employees','caregivers','filters'));
    }

    public function oldLeavesReport(Request $request)
    {
        $filters = $request->all('filter_user','filter_month','filter_year');

        $filter = explode("_",$filters['filter_user']);

        $month = !empty($filters['filter_month'])?$filters['filter_month']:date('m');
        $year = !empty($filters['filter_year'])?$filters['filter_year']:date('Y');

        $caregiver = Caregiver::branch()->select('id','first_name','middle_name','last_name');

        $employees = $caregiver->get();

        $departments = Department::branch()->get();

        if(!empty($filters['filter_user'])){
            if($filter[0] == 'dept'){
                $caregiver->whereHas('professional', function($q) use($filter){
                    $q->whereDepartment($filter[1]);
                });
            }

            if($filter[0] == 'emp'){
                $caregiver->whereId($filter[1]);
            }
        }

        $caregivers = $caregiver->get();

        $leaves = LeavesOld::branch()->whereIn('caregiver_id',$caregivers->pluck('id')->toArray())->whereMonth('date_from',$month)->whereYear('date_from',$year)->get();
        return view('reports.hr.oldLeavesReport',compact('leaves','departments','employees','caregivers','filters'));
    }

    public function warningStarUniformDocumentReport()
    {
        $caregivers = Caregiver::with('professional','verifications','appraisal')->get();
        return view('reports.hr.warningStarUniformDocument', compact('caregivers'));
    }
    public function clientNoServiceDays(Request $request){
        $patients = Patient::select('id','patient_id','first_name','last_name')->whereNotNull('patient_id')->get();

        if($request->ajax){
            
            return \App\Helpers\Report\Clinical::getClientNoServiceDays($request);
        }
        return view('reports.clinical.clientnoservicedays',compact('patients'));

    }

    public function convertedLeadsReport(Request $request)
    {
        if($request->ajax){
            return \App\Helpers\Report\Clinical::convertedLeadsReport($request);
        }
        return view('reports.leads.leads-converted');
    }

    public function customerFeedback(Request $request)
    {
        if($request->ajax){
            return \App\Helpers\Report\Hr::CustomerFeedback($request);
        }
        return view('reports.hr.customer-feedback');
    }

}
