<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\Helper;

use App\Entities\Dashboard;
use App\Entities\User;
use App\Entities\Console\Provider;

use App\Entities\Lead;
use App\Entities\CaseServiceRequest;
use App\Entities\CaseRatecard;
use App\Entities\CaseServiceOrder;
use App\Entities\Schedule;
use App\Entities\Patient;
use App\Entities\Careplan;
use App\Entities\Masters\Department;
use App\Mail\TestMail;
use App\Entities\Profile;
use App\Entities\Masters\Designation;
use App\Entities\Caregiver\Caregiver;
use App\Entities\Caregiver\ProfessionalDetails;
use App\Entities\Caregiver\AccountDetails;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    public function test()
    {
        return redirect('/');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {        
        if(!session('uid') || !session('utype')){
            return redirect('/login');
        }
        else{
            return view('dashboard');
        }
    }

    public function unauthorized()
    {
	    \App::abort(403);
    }

    public function login()
    {
	    return redirect('login');
    }

    public function administration()
    {
	    return view('administration.index');
    }

    public function flush()
    {
        session()->flush();
        return redirect('login');
    }

    public function refreshToken()
    {
        return csrf_token();
    }

    public function dashboard()
    {
        return redirect('dashboard');
    }

    public function profile()
    {
        $p = User::find(\Auth::id());

        return view('profile', compact('p'));
    }

    public function saveProfile(Request $request)
    {
        $id = \Helper::encryptor('decrypt',$request->id);

        $profile = User::find($id);
        $data = $request->only('full_name','email','mobile');
        if( $request->password != "" || $request->password != null){
            $data['password'] = $request->password;
        }
        $profile->update($data);

        // Update Auth user instance
        \Auth::user()->update($data);

        return back()->with('alert_success','Profile updated successfully');
    }

    public function indexSearch()
    {
        // Patient::get()->searchable();
        // Caregiver::get()->searchable();
        // Careplan::get()->searchable();

        return view('modules.search');
    }

    public function search(Request $request)
    {
        $res = Patient::search($request->search)->paginate();

        return view('modules.search', compact('res'));
    }
    
    public function bulkImportOnelife(Request $request)
    {
        if($request->isMethod('post')){
            $count = 0;
            $file = $request->file('excel_file');
            $path = $file->getRealPath();

            $reader = \Excel::selectSheetsByIndex(0)->load($path, function($reader) {});
            $data = $reader->get();

            if(!empty($data) && $data->count())
            {
                dd($data);
                \Config::set('database.connections.tenant.database','apnacare_onelife');
                //dd(\Config::get('database.connections.tenant.database'));
                $data = $data->toArray();
                $count = count($data);
                foreach ($data as $key => $value)
                {
                    if(count($value))
                    {
                        if(Caregiver::whereEmail($value['email'])->count() <= 0)
                        {
                            if(isset($value['employee_id']) && !empty($value['employee_id']) && $value['employee_id'] != '')
                            {
                                $caregiver = new Caregiver;
                                if(isset($value['date_of_birth']) && !empty($value['date_of_birth']))
                                    $dob = date_format(new \DateTime($value['date_of_birth']),'Y-m-d');
                                else
                                    $dob = null;                        

                                $caregiver->tenant_id = \Helper::getTenantID();
                                $caregiver->employee_id = $value['employee_id'];
                                $caregiver->first_name = $value['first_name'];
                                $caregiver->middle_name = $value['middle_name'];
                                $caregiver->last_name = $value['last_name'];
                                $caregiver->date_of_birth = $dob;
                                $caregiver->gender = $value['gender'];                                
                                $caregiver->marital_status = $value['marital_status'];
                                $caregiver->mobile_number = sprintf("%.0f", $value['mobile_number']);
                                $caregiver->alternate_number = sprintf("%.0f", $value['alternate_number']);
                                $caregiver->email = $value['email'];
                                $caregiver->current_address = $value['current_address'];
                                $caregiver->current_area = $value['current_area'];
                                $caregiver->current_city = $value['current_city'];
                                $caregiver->current_state = $value['current_state'];
                                $caregiver->current_zipcode = sprintf("%.0f", $value['current_zipcode']);
                                $caregiver->permanent_address = $value['permanent_address'];
                                $caregiver->permanent_area = $value['permanent_area'];
                                $caregiver->permanent_city = $value['permanent_city'];
                                $caregiver->permanent_state = $value['permanent_state'];
                                $caregiver->permanent_zipcode = $value['permanent_zipcode'];
                                $caregiver->imported = 1;

                                $caregiver->save();

                                $user = new User;
                                $user->tenant_id = \Helper::getTenantID();
                                $user->user_type = 'Caregiver';
                                $user->created_by = 'Custom';
                                $user->user_id = $caregiver->id;                                
                                $user->role_id = 1;
                                $user->full_name = $value['first_name'].' '.$value['middle_name'].' '.$value['last_name'];
                                $user->email = $value['email'];

                                $user->save();

                                // Add Professional Details
                                if(isset($value['date_of_joining']) && !empty($value['date_of_joining']))
                                    $doj = date_format(new \DateTime($value['date_of_joining']),'Y-m-d');
                                else
                                    $doj = null;
                                $details = new ProfessionalDetails;
                                $details->caregiver_id = $caregiver->id;
                                $details->specialization = intval($value['specialization']);
                                $details->department = intval($value['department']);
                                $details->designation = intval($value['designation']);
                                $details->qualification = $value['qualification'];
                                $details->experience = $value['experience'];
                                $details->languages_known = str_replace(' ','',$value['languages_known']);
                                $details->date_of_joining = $doj;
                                $details->role = $value['role'];
                                $details->save();

                                // Add Account Details
                                $adetails = new AccountDetails;
                                $adetails->caregiver_id = $caregiver->id;
                                $adetails->pan_number = $value['pan_number'];
                                $adetails->aadhar_number = $value['aadhar_number'];
                                $adetails->account_name = $value['account_name'];
                                $adetails->account_number = $value['account_number'];
                                $adetails->bank_name = $value['bank_name'];
                                $adetails->bank_branch = $value['bank_branch'];
                                $adetails->ifsc_code = $value['ifsc_code'];
                                $adetails->save();
                            }
                        }
                    }
                }
            }

            dd($data);
            return back()->with('alert_success',$count.' caregiver(s) has been successfully imported.');
        }

        return view('test');

        return back();
    }
    
    public function patientImportOnelife(Request $request)
    {
        if($request->isMethod('post')){
            $count = 0;
            $file = $request->file('excel_file');
            $path = $file->getRealPath();

            $reader = \Excel::selectSheetsByIndex(0)->load($path, function($reader) {});
            $data = $reader->get();

            if(!empty($data) && $data->count())
            {
                dd($data);
                \Config::set('database.connections.tenant.database','apnacare_onelife');
                //dd(\Config::get('database.connections.tenant.database'));
                $data = $data->toArray();
                $count = count($data);
                foreach ($data as $key => $value)
                {
                    if(count($value))
                    {
                        $patient = new Patient;
                        if(isset($value['date_of_birth']) && !empty($value['date_of_birth']))
                            $dob = date_format(new \DateTime($value['date_of_birth']),'Y-m-d');
                        else
                            $dob = null;
                            
                        $patient->tenant_id = \Helper::getTenantID();
                        $patient->patient_id = $value['patient_id'];
                        $patient->first_name = $value['first_name'];
                        $patient->last_name = $value['last_name'];
                        $patient->date_of_birth = $dob;
                        $patient->gender = $value['gender'];
                        $patient->patient_age = intval($value['patient_age']);
                        $patient->contact_number = sprintf("%.0f", $value['contact_number']);
                        $patient->alternate_number = sprintf("%.0f", $value['alternate_number']);
                        $patient->street_address = $value['street_address'];
                        $patient->area = $value['area'];
                        $patient->city = $value['city'];
                        $patient->state = $value['state'];
                        $patient->zipcode = sprintf("%.0f", $value['zipcode']);
                        $patient->created_at = $value['created_at'];

                        $patient->save();                                            
                    }
                }
            }

            dd($data);
            return back()->with('alert_success',$count.' patient(s) has been successfully imported.');
        }

        return view('test');

        return back();
    }
    
    public function leadsImportOnelife(Request $request)
    {
        if($request->isMethod('post')){
            $count = 0;
            $file = $request->file('excel_file');
            $path = $file->getRealPath();

            $reader = \Excel::selectSheetsByIndex(0)->load($path, function($reader) {});
            $data = $reader->get();

            if(!empty($data) && $data->count())
            {
                dd($data);
                \Config::set('database.connections.tenant.database','apnacare_onelife');
                //dd(\Config::get('database.connections.tenant.database'));
                $data = $data->toArray();
                $count = count($data);
                foreach ($data as $key => $value)
                {
                    if(count($value))
                    {
                        $lead = new Lead;
                        if(isset($value['created_at']) && !empty($value['created_at']))
                            $createdAt = date_format(new \DateTime($value['created_at']),'Y-m-d');
                        else
                            $createdAt = null;
                            
                        if(isset($value['assessment_date']) && !empty($value['assessment_date']))
                            $assessmentDate = date_format(new \DateTime($value['assessment_date']),'Y-m-d');
                        else
                            $assessmentDate = null;
                            
                        $lead->tenant_id = \Helper::getTenantID();
                        $lead->patient_id = $value['patient_id'];
                        $lead->episode_id = $value['episode_id'];
                        $lead->case_description = $value['case_description'];                        
                        $lead->assessment_date = $assessmentDate;
                        $lead->assessment_time = $value['assessment_time']; ;                        
                        $lead->special_instructions = $value['special_instructions'];
                        $lead->assessment_notes = $value['assessment_notes'];
                        $lead->status = $value['status'];                        
                        $lead->created_at = $createdAt;

                        $lead->save();                                            
                    }
                }
            }

            dd($data);
            return back()->with('alert_success',$count.' patient(s) has been successfully imported.');
        }

        return view('test');

        return back();
    }
    
    public function importSchedulesOneLife(Request $request)
    {
        if($request->isMethod('post'))
        {
            $count = 0;
            $file = $request->file('excel_file');
            $path = $file->getRealPath();
            
            $reader = \Excel::selectSheetsByIndex(0)->load($path, function($reader) {});
            $data = $reader->get();

            if(!empty($data) && $data->count())
            {
                // \Config::set('database.connections.tenant.database','apnacare_onelife');
                \DB::setDefaultConnection('do');
                $data = $data->toArray();
                $count = count($data);
                $cnt = 0;
                $serviceArray = [];
                $prevLead = $prevPatient = $prevService = null;
                foreach ($data as $row)
                {
                    if(count($row))
                    {
                        $row['amount'] = floatVal($row['gross_rate']).'|'.floatVal($row['net_rate']).'|'.floatVal($row['discount']);
                        $serviceArray[intval($row['lead_id']).'_'.intval($row['service_id']).'_'.$row['amount']]['dates'][] = $row['schedule_date'].'_'.intval($row['caregiver_id']).'_'.intval($row['chargeable']);
                        $serviceArray[intval($row['lead_id']).'_'.intval($row['service_id']).'_'.$row['amount']]['patient_id'] = intval($row['patient_id']);
                        $serviceArray[intval($row['lead_id']).'_'.intval($row['service_id']).'_'.$row['amount']]['amount'] = $row['amount'];
                    }
                }
                // dd($serviceArray);
                
                if(count($serviceArray)){
                    foreach ($serviceArray as $key => $value) {
                        $keyArr = explode("_",$key);
                        $leadID = $keyArr[0];
                        $serviceID = $keyArr[1];
                        $patientID = $value['patient_id'];
                        $amtArr = explode("|",$keyArr[2]);
                        $grossRate = abs($amtArr[0]);
                        $netRate = abs($amtArr[1]);
                        $discount = abs($amtArr[2]);
                        
                        //dump($leadID.' - '.$serviceID.' - '.$patientID.' - '.count($value['dates']).' From: '.current($value['dates']).' To: '.end($value['dates']));
                        // 1. Create case service request - 40926
                        $csrData = [
                            'tenant_id' => 142,
                            'branch_id' => 1,
                            'lead_id' => $leadID,
                            'patient_id' => $patientID,
                            'service_id' => $serviceID,
                            'from_date' => explode("_",current($value['dates']))[0],
                            'to_date' => explode("_",end($value['dates']))[0],
                            'frequency_period' => 1,
                            'frequency' => 'Daily',
                            'gross_rate' => floatVal($grossRate),
                            'discount' => $discount == 0?'N':'Y',
                            'discount_type' => 'F',
                            'discount_value' => floatval($discount),
                            'net_rate' => floatVal($netRate),
                            'status' => 'Completed',
                            'created_at' => explode("_",current($value['dates']))[0],
                            'updated_at' => explode("_",current($value['dates']))[0],
                        ];
                        $serviceRequestId = \App\Entities\CaseServices::insertGetId($csrData);
                        
                        // 2. Create case service orders
                        $csoData = [
                            'tenant_id' => 142,
                            'branch_id' => 1,
                            'lead_id' => $leadID,
                            'patient_id' => $patientID,
                            'service_request_id' => $serviceRequestId,
                            'from_date' => explode("_",current($value['dates']))[0],
                            'to_date' => explode("_",end($value['dates']))[0],                        
                            'payment_status' => 'Paid'
                        ];
                        \App\Entities\CaseServiceOrder::create($csoData);
                        
                        // 3. Create case schedules
                        if(count($value['dates'])){
                            $csData = [];
                            $attData = [];
                            foreach ($value['dates'] as $date) {
                                $csData[] = [
                                    'tenant_id' => 142,
                                    'branch_id' => 1,
                                    'lead_id' => $leadID,
                                    'patient_id' => $patientID,
                                    'caregiver_id' => explode("_",$date)[1],
                                    'service_request_id' => $serviceRequestId,
                                    'schedule_date' => explode("_",$date)[0],
                                    'service_required' => $serviceID,
                                    'chargeable' => intval(explode("_",$date)[2])>=1?1:0,
                                    'status' => 'Completed',
                                    'created_at' => explode("_",$date)[0],
                                    'updated_at' => explode("_",$date)[0],
                                ];
                                
                                // 4. Mark Attendance
                                if(explode("_",$date)[1] != null){
                                    $attData[] = [
                                        'tenant_id' => 142,
                                        'branch_id' => 1,                                
                                        'caregiver_id' => explode("_",$date)[1],
                                        'punch_in' => explode("_",$date)[0].' 00:00:00',
                                        'punch_out' => explode("_",$date)[0].' 18:00:00',
                                        'punch_in_server_timestamp' => explode("_",$date)[0].' 00:00:00',
                                        'punch_out_server_timestamp' => explode("_",$date)[0].' 18:00:00',
                                        'status' => 'P',
                                        'created_at' => explode("_",$date)[0],
                                        'updated_at' => explode("_",$date)[0],                               
                                    ];                                    
                                }                                
                            }
                            \App\Entities\Schedule::insert($csData);
                            \App\Entities\Attendance::insert($attData);
                        }                                             
                    }
                }
            }

            dd($serviceArray);
            return back()->with('alert_success',$count.' leads, service orders & schedules has been successfully imported.');
        }
        
        return view('test');

        return back();
    }

    public function test1(Request $request)
    {
        /*$data['profile'] = Profile::first();
        $pdf = \PDF::loadView('test1', $data);
        \Mail::send(new TestMail($pdf->output()));
        $profile = Profile::first();
        return view('test1',compact('profile'));*/
    }
    
    public function paymentsImportOnelife(Request $request)
    {
        if($request->isMethod('post')){
            $count = 0;
            $file = $request->file('excel_file');
            $path = $file->getRealPath();

            $reader = \Excel::selectSheetsByIndex(0)->load($path, function($reader) {});
            $data = $reader->get();

            if(!empty($data) && $data->count())
            {
                //dd($data->toArray());
                \Config::set('database.connections.tenant.database','apnacare_onelife');
                //dd(\Config::get('database.connections.tenant.database'));
                $data = $data->toArray();
                $count = count($data);
                foreach ($data as $key => $value)
                {
                    if(count($value))
                    {                        
                        if(isset($value['created_at']) && !empty($value['created_at']))
                            $createdAt = date_format(new \DateTime($value['created_at']),'Y-m-d');
                        else
                            $createdAt = null;
                        
                        $data = [
                            'tenant_id' => \Helper::getTenantID(),
                            'user_id' => 1,
                            'patient_id' => intval($value['patient_id']),
                            'payment_mode' => $value['payment_mode'],                            
                            'bank_name' => $value['bank_name'],
                            'actual_amount' => $value['total_amount'],
                            'total_amount' => $value['total_amount'],
                            'status' => 'Success',
                            'created_at' => $createdAt,
                            'updated_at' => $createdAt
                        ];
                        if($value['payment_mode'] == 'Cheque')
                            $data['cheque_dd_no'] = sprintf("%.0f", $value['reference_no']);
                        else
                            $data['reference_no'] = sprintf("%.0f", $value['reference_no']);
                            
                        \App\Entities\CasePayment::insert($data);
                    }
                }
            }

            dd($data);
            return back()->with('alert_success',$count.' payments has been successfully imported.');
        }

        return view('test');

        return back();
    }
    
    public function consumablesImportOnelife(Request $request)
    {
        if($request->isMethod('post')){
            $count = 0;
            $file = $request->file('excel_file');
            $path = $file->getRealPath();

            $reader = \Excel::selectSheetsByIndex(0)->load($path, function($reader) {});
            $data = $reader->get();

            if(!empty($data) && $data->count())
            {
                // dd($data->toArray());
                \Config::set('database.connections.tenant.database','apnacare_onelife');
                //dd(\Config::get('database.connections.tenant.database'));
                $data = $data->toArray();
                $count = count($data);
                $dcount = 0;
                foreach ($data as $key => $value)
                {
                    if(count($value))
                    {                        
                        if(isset($value['created_at']) && !empty($value['created_at']))
                            $createdAt = date_format(new \DateTime($value['created_at']),'Y-m-d H:i:s');
                        else
                            $createdAt = null;                                                
                            
                        $data = [
                            'tenant_id' => \Helper::getTenantID(),
                            'user_id' => 1,
                            'lead_id' => intval($value['lead_id']),
                            'category' => $value['category'],
                            'item' => $value['item'],
                            'quantity' => 1,
                            'rate' => floatVal($value['amount']),
                            'amount' => floatVal($value['amount']),
                            'created_at' => $createdAt,
                            'updated_at' => $createdAt
                        ];
                        $dcount++;
                        \App\Entities\CaseBillables::insert($data);
                        
                        // $data = [
                        //     'tenant_id' => \Helper::getTenantID(),
                        //     'user_id' => 1,
                        //     'patient_id' => intval($value['patient_id']),                            
                        //     'date' => $createdAt,
                        //     'amount' => $value['amount'],
                        //     'comment' => $value['comment'],
                        //     'created_at' => $createdAt,
                        //     'updated_at' => $createdAt
                        // ];
                        // \App\Entities\CreditMemo::insert($data);
                    }
                }
            }

            dd([$dcount, $data]);
            return back()->with('alert_success',$count.' payments has been successfully imported.');
        }

        return view('test');

        return back();
    }
    
    public function bulkImportOld(Request $request)
    {
        if($request->isMethod('post'))
        {
            $count = 0;
            $file = $request->file('excel_file');
            $path = $file->getRealPath();

            $reader = \Excel::selectSheetsByIndex(0)->load($path, function($reader) {});
                    // $reader->setDateColumns(['dob','date_of_joining','effective_date_of_salary','accommodation_from_date','accommodation_to_date']);
                    // $reader->setDateFormat('Y-m-d');

            $data = $reader->get();

            if(!empty($data) && $data->count())
            {
                dd($data);
                \DB::setDefaultConnection('onelife');
                $data = $data->toArray();
                $count = count($data);
                foreach ($data as $key => $value)
                {
                    if(count($value))
                    {
                        if(Caregiver::whereEmail($value['email'])->count() <= 0)
                        {
                            if(isset($value['employee_id']) && !empty($value['employee_id']) && $value['employee_id'] != '')
                            {
                                $users = User::where('status','1')->count();
                                // \Config::set('database.default','central');
                                // $subscriber = Subscriber::whereId(\Helper::getTenantID())->first();

                                $caregiver = new Caregiver;
                                if(isset($value['dob']) && !empty($value['dob']))
                                    $dob = date_format(new \DateTime($value['dob']),'Y-m-d');
                                else
                                    $dob = null;

                                // Get Department ID from Name
                                $departmentID = Department::whereDepartmentName($value['department'])->value('id');

                                // Get Designation ID from Name
                                $designationID = Designation::whereDesignationName($value['designation'])->value('id');

                                $caregiver->tenant_id = \Helper::getTenantID();
                                $caregiver->employee_id = \Helper::getSetting('employee_id_prefix').sprintf("%.0f", $value['employee_id']);
                                $caregiver->first_name = $value['first_name'];
                                $caregiver->middle_name = $value['middle_name'];
                                $caregiver->last_name = $value['last_name'];
                                $caregiver->date_of_birth = $dob;
                                $caregiver->gender = $value['gender'];
                                //$caregiver->blood_group = $value['blood_group'];
                                $caregiver->marital_status = $value['marital_status'];
                                $caregiver->mobile_number = sprintf("%.0f", $value['mobile_number']);
                                $caregiver->email = $value['email'];
                                $caregiver->current_address = $value['current_address'];
                                $caregiver->current_area = $value['current_area'];
                                $caregiver->current_city = $value['current_city'];
                                $caregiver->current_state = $value['current_state'];
                                $caregiver->current_zipcode = $value['current_zipcode'];
                                $caregiver->permanent_address = $value['permanent_address'];
                                $caregiver->permanent_area = $value['permanent_area'];
                                $caregiver->permanent_city = $value['permanent_city'];
                                $caregiver->permanent_state = $value['permanent_state'];
                                $caregiver->permanent_zipcode = $value['permanent_zipcode'];
                                $caregiver->imported = 1;

                                $caregiver->save();

                                $user = new User;
                                $user->tenant_id = \Helper::getTenantID();
                                $user->user_type = 'Caregiver';
                                $user->created_by = 'Custom';
                                $user->user_id = $caregiver->id;
                                // if( $subscriber->users_limit <= $users ){
                                //     $user->status = 0;
                                // }
                                $user->role_id = 1;
                                $user->full_name = $value['first_name'].' '.$value['middle_name'].' '.$value['last_name'];
                                $user->email = $value['email'];

                                $user->save();

                                // Add Professional Details
                                if(isset($value['doj']) && !empty($value['doj']))
                                    $doj = date_format(new \DateTime($value['doj']),'Y-m-d');
                                else
                                    $doj = null;
                                $details = new ProfessionalDetails;
                                $details->caregiver_id = $caregiver->id;
                                $details->department = $departmentID;
                                $details->designation = $designationID;
                                $details->qualification = $value['qualification'];
                                $details->experience = $value['experience'];
                                $details->languages_known = str_replace(' ','',$value['languages_known']);
                                $details->date_of_joining = $doj;
                                $details->role = $value['role'];
                                $details->save();

                                // Add Account Details
                                $adetails = new AccountDetails;
                                $adetails->caregiver_id = $caregiver->id;
                                $adetails->pan_number = $value['pan_number'];
                                $adetails->aadhar_number = $value['aadhar_number'];
                                $adetails->account_name = $value['first_name'].' '.$value['middle_name'].' '.$value['last_name'];
                                $adetails->account_number = $value['account_number'];
                                $adetails->bank_name = $value['bank_name'];
                                $adetails->bank_branch = $value['branch_name'];
                                $adetails->ifsc_code = $value['ifsc_code'];
                                $adetails->save();
                            }
                        }
                    }
                }
            }

            dd($data);
            return back()->with('alert_success',$count.' caregiver(s) has been successfully imported.');
        }

        return view('test');

        return back();
    }

    public function importLeadsOld(Request $request)
    {
        if($request->isMethod('post'))
        {
            $count = 0;
            $file = $request->file('excel_file');
            $path = $file->getRealPath();

            $reader = \Excel::selectSheetsByIndex(0)->load($path, function($reader) {});

            $data = $reader->get();

            if(!empty($data) && $data->count())
            {
                //dd($data);
                \DB::setDefaultConnection('onelife');
                $data = $data->toArray();
                $count = count($data);
                foreach ($data as $key => $value)
                {
                    if(count($value))
                    {
                        if(isset($value['from_date']) && !empty($value['from_date']))
                            $fromDate = date_format(new \DateTime($value['from_date']),'Y-m-d');
                        else
                            $fromDate = null;

                        if(isset($value['to_date']) && !empty($value['to_date']))
                            $toDate = date_format(new \DateTime($value['to_date']),'Y-m-d');
                        else
                            $toDate = null;

                        if($fromDate && $toDate){
                            // Insert date into case_service_requests table
                            $serviceRequest = new CaseServiceRequest;
                            $serviceRequest->tenant_id = \Helper::getTenantID();
                            $serviceRequest->branch_id = 1;
                            $serviceRequest->user_id = 1;
                            $serviceRequest->lead_id = $value['lead_id'];
                            $serviceRequest->patient_id = $value['patient_id'];
                            $serviceRequest->service_requested_id = $value['service_id'];
                            $serviceRequest->service_recommended_id = $value['service_id'];
                            $serviceRequest->from_date = $fromDate;
                            $serviceRequest->to_date = $toDate;
                            $serviceRequest->frequency_period = 1;
                            $serviceRequest->frequency = 'Daily';
                            $serviceRequest->status = 1;
                            $serviceRequest->created_at = $fromDate;
                            $serviceRequest->updated_at = $fromDate;
                            $serviceRequest->save();

                            // Insert date into case_service_orders table
                            $serviceOrder = new CaseServiceOrder;
                            $serviceOrder->tenant_id = \Helper::getTenantID();
                            $serviceOrder->branch_id = 1;
                            $serviceOrder->user_id = 1;
                            $serviceOrder->lead_id = $value['lead_id'];
                            $serviceOrder->patient_id = $value['patient_id'];
                            $serviceOrder->service_id = $serviceRequest->id;
                            $serviceOrder->from_date = $fromDate;
                            $serviceOrder->to_date = $toDate;
                            $serviceOrder->rate = $value['total'];
                            $serviceOrder->ratecard_rate = floatval($value['rate']) * floatval($value['duration']);
                            $serviceOrder->status = 1;
                            $serviceOrder->created_at = $fromDate;
                            $serviceOrder->updated_at = $fromDate;
                            $serviceOrder->save();

                            // Insert date into case_schedules table
                            $start = new \DateTime();
                            $end = new \DateTime();
                            $end = $end->modify('+1 day');

                            $period = new \DatePeriod($start,new \DateInterval('P1D'),$end);
                            if(count($period)){
                                foreach ($period as $p) {
                                    // Insert date into case_schedules table
                                    $schedule = new Schedule;
                                    $schedule->tenant_id = \Helper::getTenantID();
                                    $schedule->branch_id = 1;
                                    $schedule->lead_id = $value['lead_id'];
                                    $schedule->patient_id = $value['patient_id'];
                                    $schedule->caregiver_id = $value['caregiver_id'];
                                    $schedule->service_order_id = $serviceOrder->id;
                                    $schedule->schedule_date = $p->format('Y-m-d');
                                    $schedule->service_required = $value['service_id'];
                                    $schedule->chargeable = 1;
                                    $schedule->status = 'Completed';
                                    $schedule->created_at = $p->format('Y-m-d');
                                    $schedule->created_at = $p->format('Y-m-d');
                                    $schedule->save();
                                }
                            }
                        }
                    }
                }
            }

            dd($data);
            return back()->with('alert_success',$count.' leads, service orders & schedules has been successfully imported.');
        }

        return view('test');

        return back();
    }

    public function importLeads(Request $request)
    {
        if($request->isMethod('post'))
        {
            $count = 0;
            $file = $request->file('excel_file');
            $path = $file->getRealPath();

            $reader = \Excel::selectSheetsByIndex(0)->load($path, function($reader) {});

            $data = $reader->get();

            if(!empty($data) && $data->count())
            {
                \DB::setDefaultConnection('onelife');
                $data = $data->toArray();
                $count = count($data);
                $cnt = 0;
                foreach ($data as $key => $value)
                {
                    if(count($value))
                    {
                        // if($value['patient_id'] == '23'){
                        //     $start = new \DateTime($value['from_date']);
                        //     $end = new \DateTime($value['to_date']);
                        //     $end = $end->modify('+1 day');
                        //     $period = new \DatePeriod($start,new \DateInterval('P1D'),$end);
                        //
                        //     //echo '<br>';
                        //     //echo 'Patient ID: '.$value['patient_id'].'<br>';
                        //     //echo 'Period: '.$start->format('d-m-Y').' to '.$end->format('d-m-Y').'<br>';
                        //     $dates = [];
                        //     foreach ($period as $p) {
                        //         $dates[] = $p->format('d-m-Y');
                        //     }
                        //     $cnt += count($dates);
                        //     echo 'Count : '.count($dates).'<br>';
                        //     //echo '-------------------------------------';
                        // }
                        // else{
                        //     echo 'Total: '.$cnt;
                        //     dd("END");
                        // }

                        if(isset($value['from_date']) && !empty($value['from_date']))
                            $fromDate = date_format(new \DateTime($value['from_date']),'Y-m-d');
                        else
                            $fromDate = null;

                        if(isset($value['to_date']) && !empty($value['to_date']))
                            $toDate = date_format(new \DateTime($value['to_date']),'Y-m-d');
                        else
                            $toDate = null;

                        if($fromDate && $toDate){
                            // Insert date into case_service_requests table
                            $serviceRequest = new CaseServiceRequest;
                            $serviceRequest->tenant_id = \Helper::getTenantID();
                            $serviceRequest->branch_id = 1;
                            $serviceRequest->user_id = 1;
                            $serviceRequest->lead_id = $value['lead_id'];
                            $serviceRequest->patient_id = $value['patient_id'];
                            $serviceRequest->service_requested_id = $value['service_id'];
                            $serviceRequest->service_recommended_id = $value['service_id'];
                            $serviceRequest->from_date = $fromDate;
                            $serviceRequest->to_date = $toDate;
                            $serviceRequest->frequency_period = 1;
                            $serviceRequest->frequency = 'Daily';
                            $serviceRequest->status = 1;
                            $serviceRequest->created_at = $fromDate;
                            $serviceRequest->updated_at = $fromDate;
                            $serviceRequest->save();

                            // Insert date into case_ratecards table
                            $rateCard = new CaseRatecard;
                            $rateCard->tenant_id = \Helper::getTenantID();
                            $rateCard->lead_id = $value['lead_id'];
                            $rateCard->service_id = $value['service_id'];
                            $rateCard->discount_applicable = 'Y';
                            $rateCard->discount_type = 'F';
                            $serviceRate = Helper::getServiceRate($value['service_id']);
                            $rateCard->rate = $serviceRate;
                            if(floatVal($serviceRate) == floatVal($value['rate'])){
                                $rateCard->discount_value = 0;
                            }else{
                                $rateCard->discount_value = floatVal($serviceRate) - floatVal($value['rate']);
                            }
                            $rateCard->total_amount = floatVal($value['rate']);
                            $rateCard->created_at = $fromDate;
                            $rateCard->updated_at = $fromDate;
                            $rateCard->save();

                            // Insert date into case_service_orders table
                            $serviceOrder = new CaseServiceOrder;
                            $serviceOrder->tenant_id = \Helper::getTenantID();
                            $serviceOrder->branch_id = 1;
                            $serviceOrder->user_id = 1;
                            $serviceOrder->lead_id = $value['lead_id'];
                            $serviceOrder->patient_id = $value['patient_id'];
                            $serviceOrder->service_id = $serviceRequest->id;
                            $serviceOrder->from_date = $fromDate;
                            $serviceOrder->to_date = $toDate;
                            $serviceOrder->rate = floatVal($value['rate']) * intval($value['count']);
                            $serviceOrder->ratecard_rate = floatval($value['rate']);
                            $serviceOrder->status = 1;
                            $serviceOrder->created_at = $fromDate;
                            $serviceOrder->updated_at = $fromDate;
                            $serviceOrder->save();
                        }

                        //     // Insert date into case_schedules table
                        //     $start = new \DateTime($fromDate);
                        //     $end = new \DateTime($toDate);
                        //     $end = $end->modify('+1 day');
                        //
                        //     $period = new \DatePeriod($start,new \DateInterval('P1D'),$end);
                        //     echo '<br>---------------------------Schedule Starts------------------------<br>';
                        //     if(count($period)){
                        //         foreach ($period as $p) {
                        //             echo 'Patient: '.$value['patient_id'].' | Schedule: '.$p->format('d-m-Y').'<br>';
                        //             // Insert date into case_schedules table
                        //             $schedule = new Schedule;
                        //             $schedule->tenant_id = \Helper::getTenantID();
                        //             $schedule->branch_id = 1;
                        //             $schedule->lead_id = $value['lead_id'];
                        //             $schedule->patient_id = $value['patient_id'];
                        //             $schedule->caregiver_id = $value['caregiver_id'];
                        //             $schedule->service_order_id = $serviceOrder->id;
                        //             $schedule->schedule_date = $p->format('Y-m-d');
                        //             $schedule->service_required = $value['service_id'];
                        //             $schedule->chargeable = 1;
                        //             $schedule->status = 'Completed';
                        //             $schedule->created_at = $p->format('Y-m-d');
                        //             $schedule->created_at = $p->format('Y-m-d');
                        //             $schedule->save();
                        //         }
                        //     }
                        //     echo '<br>---------------------------Schedule Ends------------------------<br>';
                        // }
                    }
                }
            }

            dd($data);
            return back()->with('alert_success',$count.' leads, service orders & schedules has been successfully imported.');
        }

        return view('test');

        return back();
    }

    public function importSchedules(Request $request)
    {
        if($request->isMethod('post'))
        {
            $count = 0;
            $file = $request->file('excel_file');
            $path = $file->getRealPath();

            $reader = \Excel::selectSheetsByIndex(0)->load($path, function($reader) {});

            $data = $reader->get();
            dd($data->count());
            if(!empty($data) && $data->count())
            {
                \DB::setDefaultConnection('onelife');
                $data = $data->toArray();
                $count = count($data);
                foreach ($data as $key => $value)
                {
                    if(count($value))
                    {
                        if(isset($value['schedule_date']) && !empty($value['schedule_date']))
                            $scheduleDate = date_format(new \DateTime($value['schedule_date']),'Y-m-d');
                        else
                            $scheduleDate = null;

                        if($scheduleDate){
                            $schedule = new Schedule;
                            $schedule->tenant_id = \Helper::getTenantID();
                            $schedule->branch_id = 1;
                            $schedule->lead_id = $value['lead_id'];
                            $schedule->patient_id = $value['patient_id'];
                            $schedule->caregiver_id = $value['caregiver_id'];
                            $schedule->service_order_id = $value['service_order_id'];
                            $schedule->schedule_date = $scheduleDate;
                            $schedule->service_required = $value['service_id'];
                            $schedule->chargeable = 1;
                            $schedule->status = 'Completed';
                            $schedule->created_at = $value['created_at'];
                            $schedule->created_at = $value['updated_at'];
                            $schedule->save();
                        }
                    }
                }
            }

            dd($data);
            return back()->with('alert_success',$count.' schedules has been successfully imported.');
        }

        return view('test');

        return back();
    }

    public function patientRegForm($id)
    {
        \Config::set('database.connections.tenant.database','shc_branch271');
        \DB::setDefaultConnection('tenant');

        $patient = Patient::find(Helper::encryptor('decrypt',$id));
        $lead = Lead::wherePatientId(Helper::encryptor('decrypt',$id))->get()->last();
        return view('leads.patient-reg-form',compact('patient','lead'));
    }

    public function patientTermsConditions($id)
    {
        \Config::set('database.connections.tenant.database','shc_branch271');
        \DB::setDefaultConnection('tenant');

        $patient = Patient::find(Helper::encryptor('decrypt',$id));
        $lead = Lead::wherePatientId(Helper::encryptor('decrypt',$id))->get()->last();
        return view('leads.patient-term-cond',compact('patient','lead'));
    }

    public function familaLink($id,$db)
    {
        \Config::set('database.connections.tenant.database',Helper::encryptor('decrypt',$db));
        \DB::setDefaultConnection('tenant');

        $patient = Patient::find(Helper::encryptor('decrypt',$id));
        if($patient){
            $data['familia_app_link'] = 'Yes';
            $data['link_responce_date'] = date('Y-m-d H:i:s');
            $patient->update($data);
        }

        return redirect()->away('https://play.google.com/store/apps/details?id=com.shg.familia');
    }

    public function patientTermsConditionsSave(Request $request)
    {
        \Config::set('database.connections.tenant.database','shc_branch271');
        \DB::setDefaultConnection('tenant');

        $lead = Lead::find($request->lead_id);
        $data['terms_and_conditions'] = "Accepted";
        $data['terms_and_conditions_place'] = null;
        $data['terms_and_conditions_date'] = date('Y-m-d H:i:s');
        $lead->update($data);

        $image_64 = $request->signed;
        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];
        $replace = substr($image_64, 0, strpos($image_64, ',')+1);
        $image = str_replace($replace, '', $image_64);
        $image = str_replace(' ', '+', $image);
        $imageName = 'T&C_'.$lead->id.'.'.$extension;
       // \File::put('uploads/provider/shc_branch269/patient_sig/' .$imageName, base64_decode($image));
        Storage::disk('public')->put($imageName, base64_decode($image));

        return view('successfully_msg');
    }

    public function patientRegFormSave(Request $request)
    {

        \Config::set('database.connections.tenant.database','shc_branch271');
        \DB::setDefaultConnection('tenant');
        $patient = Patient::find($request->patient_id);
        $data['contact_number'] =$request->contact_number;
        $data['full_name'] = $request->patient_full_name;
        $data['gender'] = $request->gender;
        $data['contact_number'] =$request->contact_number;
        $data['email'] = $request->email;
        $data['street_address'] =$request->street_address;
        $data['area'] = $request->area;
        $data['city'] =$request->city;
        $data['zipcode'] = $request->zip_code;
        $data['state'] =$request->state;
        $data['patient_age'] = $request->patient_age;
        $data['alternate_number'] =$request->alternate_number;
        $data['relationship_with_patient'] = $request->relationship_with_patient;
        $data['contact_number'] =$request->contact_number;
        $data['enquirer_name'] =$request->client_full_name;
        $patient->update($data);

        $lead = Lead::find($request->lead_id);
        $data['mobility'] =$request->mobility;
        $data['medical_conditions'] = $request->medical_conditions;
        $data['primary_doctor_name'] =$request->primary_doctor_name;
        $data['dr_contact_number'] =$request->dr_contact_number;
        $data['doctor_address'] = $request->doctor_address;
        $data['acknowledgement'] =$request->acknowledgement;
        $data['advance'] =$request->advance_amount;
        $data['tenure'] =$request->tenure;
        $data['acknowledgement'] =$request->acknowledgement;
        $lead->update($data);

        /*$folderPath = public_path('uploads/provider/shc_branch269/patient_sig/');
        $image_parts = explode(";base64,", $request->signed); 
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $image = str_replace('data:image/jpeg;base64,', '', $request->signed);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10).'.'.'png';
        //$file = $folderPath . $request->lead_id . '.'.$image_type;
        \File::put(storage_path(). '/' . $imageName, base64_decode($image));*/

        $image_64 = $request->signed;
        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];
        $replace = substr($image_64, 0, strpos($image_64, ',')+1);
        $image = str_replace($replace, '', $image_64);
        $image = str_replace(' ', '+', $image);
        $imageName = $lead->id.'.'.$extension;
       // \File::put('uploads/provider/shc_branch269/patient_sig/' .$imageName, base64_decode($image));
        Storage::disk('public')->put($imageName, base64_decode($image));

        $profile = $request->patient_picture;
        $patient_id = Helper::encryptor('encrypt',$request->patient_id);
        if($profile){
            ini_set('display_error',1);
            
            $extension = $profile->extension();            

            $path = public_path('uploads/provider/d1pqYnRRcm85TEFPZDA2ajBST0N3QT09/case_documents/'.Helper::encryptor('encrypt',$request->lead_id));
            if(!is_dir($path)){
                \File::makeDirectory($path, 0777, true);
            }

            $tempFileName = $patient_id.'_temp.jpg';
            $fileName = $patient_id.'.jpg';
            
            $path = $path.'/'.$fileName;            
            \Image::make($profile->getRealPath())->resize(300,300)->save($path, 80);
        }

        $profile = $request->file('client_picture');
        if($profile){
            ini_set('display_error',1);
            
            $extension = $profile->extension();            

            $path = public_path('uploads/provider/d1pqYnRRcm85TEFPZDA2ajBST0N3QT09/case_documents/client_pictures');
            if(!is_dir($path)){
                \File::makeDirectory($path, 0777, true);
            }

            $tempFileName = $patient_id.'_temp.jpg';
            $fileName = $patient_id.'.jpg';
            
            $path = $path.'/'.$fileName;            
            \Image::make($profile->getRealPath())->resize(300,300)->save($path, 80);
        }
        
        return view('successfully_msg');
    }
}
