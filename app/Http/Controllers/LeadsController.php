<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Helpers\Helper;

use App\DataTables\LeadsDataTable;

use App\Entities\Masters\Branch;
use App\Entities\Masters\LabTests;
use App\Entities\Masters\Service;
use App\Entities\Masters\ServiceCategory;
use App\Entities\Masters\Language;
use App\Entities\Masters\Skill;
use App\Entities\Masters\Specialization;
use App\Entities\Masters\ReferralCategory;
use App\Entities\Masters\ReferralSource;
use App\Entities\Masters\Holidays;
use App\Entities\Masters\Task;
use App\Entities\Masters\RateCard;
use App\Entities\Masters\Taxrate;

use App\Entities\Masters\Consumables;
use App\Entities\Masters\Surgicals;
use App\Entities\Masters\BillingProfile;
use App\Entities\PatientCheckList;
use App\Entities\CaseBillables;
use App\Entities\CaseServiceRequest;
use App\Entities\CaseServices;
use App\Entities\CaseServiceOrder;
use App\Entities\Patient;
use App\Entities\PatientIncident;
use App\Entities\CreditMemo;
use App\Entities\Lead;
use App\Entities\DiscountsLog;
use App\Entities\CaseForm;
use App\Entities\CaseFollowUp;
use App\Entities\LeadRequests;
use App\Entities\Schedule;
use App\Entities\Comment;
use App\Entities\FCMToken;
use App\Entities\User;
use App\Entities\CaseDisposition;
use App\Entities\Caregiver\Caregiver;
use App\Entities\CaseRatecard;
use App\Entities\ServiceInterruption;
use App\Entities\CaseDocument;
use App\Entities\Settings;
use App\Entities\Attendance;
use App\Entities\AggregatorLeads;
use App\Entities\Masters\ManagementComposition;
use App\Entities\ReferralPayout;
use App\Entities\PatientAddress;
use App\Entities\DailyRevenueStorage;

use Mail;
use Carbon\Carbon;

use App\Mail\Provider\LeadConvertion;
use App\Mail\Provider\LeadCompletion;
use App\Mail\Provider\LeadLoss;
use App\Mail\Provider\BillableToVendor;
use App\Mail\Provider\StaffAssign;

class LeadsController extends Controller
{    
    public function index(Request $request)
    {   
        /*for($i=1; $i<=10; $i++){
            $username = "shgotp";
            $password = "@pn@c@r3";
            $number = "9440177266";
            $sender = "vision";
            $message = "I Love You Lee ";
            $url = "login.bulksmsgateway.in/sendmessage.php?user=".urlencode($username)."&password=".urlencode($password)."&mobile=".urlencode($number)."&sender=".urlencode($sender)."&message=".urlencode($message)."&type=".urlencode('3'); 
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $myObj = curl_exec($ch);
            curl_close($ch);
        }*/
        if(!empty($request->from_date) && !empty($request->to_date)){
            $filterFromDate = date_format(new \DateTime($request->from_date),"Y-m-d");
            $filterToDate = date_format(new \DateTime($request->to_date),"Y-m-d");
            $filterStatus = $request->status_filter;
            $filterPerPage = $request->status_perPage;
        }else if(!empty(session('from_date_leads')) && !empty(session('to_date_leads'))){
            $filterFromDate = session('from_date_leads');
            $filterToDate = session('to_date_leads');
            $filterStatus = session('status_leads');
            $filterPerPage = session('perpage_leads');
        }else{
            $filterFromDate = date('Y-m-01');
            $filterToDate = date('Y-m-t');
            $filterStatus = 'All';
            $filterPerPage = '50';
        }

        session()->put('from_date_leads',$filterFromDate);
        session()->put('to_date_leads',$filterToDate);
        session()->put('status_leads',$filterStatus);
        session()->put('perpage_leads',$filterPerPage);
        $leads = Lead::with(['patient' => function($q){
                        $q->addSelect('id','first_name','last_name','enquirer_name','contact_number','alternate_number','city','street_address','area','zipcode','state');
                    },'referralSource','serviceRequired'])
                    ->whereRaw(\DB::raw('DATE(leads.created_at) BETWEEN "'.$filterFromDate.'" AND "'.$filterToDate.'"'))
                    ->whereIn('status',['Pending','Follow Up'])
                    ->orderBy('leads.created_at','Desc');
        if($filterStatus != 'All'){
            $leads = $leads->whereStatus($filterStatus);
        }
       
        $tableLeads = clone $leads;
        $leads = $leads->get();
        if(Helper::isAggregator()){
            return redirect()->route('case.index');
        }

        // Follow-up Leads list
        $followupArray = array();
        foreach($leads->whereIn('status',['Follow Up']) as $lead){
            $followupArray[] = [
                'patient'  => $lead->patient->full_name,
                'enquirer' => $lead->patient->enquirer_name,
                'phone'    => $lead->patient->contact_number,
                'date'     => count($lead->dispositions)?date_format(new \DateTime($lead->dispositions->last()->follow_up_datetime),'d-m-Y'):'',
                'time'     => count($lead->dispositions)?date_format(new \DateTime($lead->dispositions->last()->follow_up_datetime),'h:i:s A'):'',
                'comment'  => count($lead->dispositions)?$lead->dispositions->last()->follow_up_comment:'',
                'id'       => \Helper::encryptor('encrypt',$lead->id),
            ];
        }        

        if($filterPerPage == 'All'){
            $pageLength = $tableLeads->count();
        }else{
            $pageLength = $filterPerPage;
        }
        $tableLeads = $tableLeads->paginate($pageLength);

        return view('leads.datatable-leads-index', compact('tableLeads','leads','followupArray','filterFromDate','filterToDate','filterStatus','filterPerPage'));
    }

    public function operations(Request $request)
    {
        ini_set('memory_limit', '150M');
        if(!empty($request->from_date) && !empty($request->to_date)){
            $filterFromDate = date_format(new \DateTime($request->from_date),"Y-m-d");
            $filterToDate = date_format(new \DateTime($request->to_date),"Y-m-d");
            $filterStatus = $request->status_filter;
            $filterPerPage = $request->status_perPage;
        }else if(!empty(session('from_date_operations')) && !empty(session('to_date_operations'))){
            $filterFromDate = session('from_date_operations');
            $filterToDate = session('to_date_operations');
            $filterStatus = session('status_operations');
            $filterPerPage = session('perpage_operations');
        }else{
            $filterFromDate = date('Y-m-01');
            $filterToDate = date('Y-m-t');
            $filterStatus = 'All';
            $filterPerPage = '50';
        }

        session()->put('from_date_operations',$filterFromDate);
        session()->put('to_date_operations',$filterToDate);
        session()->put('status_operations',$filterStatus);
        session()->put('perpage_operations',$filterPerPage);
        $leads = Lead::with(['patient' => function($q){
                        $q->addSelect('id','first_name','last_name','enquirer_name','contact_number','alternate_number','city');
                    },'referralSource','serviceRequired'])
                    ->whereRaw(\DB::raw('DATE(leads.created_at) BETWEEN "'.$filterFromDate.'" AND "'.$filterToDate.'"'))
                    ->whereNotIn('status',['Pending','Follow Up'])
                    ->orderBy('leads.created_at','Desc');

        if($filterStatus != 'All'){
            $leads = $leads->whereStatus($filterStatus);
        }

        $tableOperations = clone $leads;
        $leads = $leads->get();

        if(Helper::isAggregator()){
            return redirect()->route('case.index');
        }

        // Pending Staff Allocation List
        $allocation = false;
        $link = array();
        $statusSupply = $statusAllocation = '';
        $allocationArray = array();
        foreach($leads->whereIn('status',['Converted']) as $lead){
            if(isset($lead->serviceRequests)){
                foreach ($lead->serviceRequests as $service) {
                    $allocation = false;

                    if(!empty($service->custom_dates)){
                        $cDates = explode(',',$service->custom_dates);
                        if(count($cDates)){
                            $period = array_map(function($item){
                                return \Carbon\Carbon::parse(trim($item));
                            },$cDates);
                        }
                        $toDate = end($period)->format('Y-m-d');
                    }
                    else{
                        $toDate = $service->to_date->format('Y-m-d');
                    }

                    if($service->status != 'Cancelled' && $toDate >= date('Y-m-d')){
                        foreach($service->schedules as $schedule){
                            if($schedule->caregiver_id == 0 && $schedule->status != 'Cancelled'){
                                $allocation = true;
                            }
                        }
                        if(count($service->schedules) == 0 || $service->status == 'No Supply'){
                            $allocation = true;
                        }
                    }
                    if($allocation){
                        if(!in_array($lead->id,$link,true)){
                            $link[] = $lead->id;
                            foreach ($lead->serviceRequests as $service) {
                                if($service->status != 'Cancelled'){
                                    if(count($service->schedules) == 0 || $service->status == 'No Supply'){
                                            $statusSupply = ' No Supply ';
                                    }

                                    if(count($service->schedules) != 0){
                                        foreach ($service->schedules->where('status','!=','Cancelled') as $sch) {
                                            if($sch->caregiver_id == 0)
                                                $statusAllocation = ' Allocation Pending ';
                                        }
                                    }
                                }
                            }

                            $allocationArray[] = [
                                'patient'  => $lead->patient->full_name,
                                'enquirer' => $lead->patient->enquirer_name,
                                'phone'    => $lead->patient->contact_number,
                                'id'       => \Helper::encryptor('encrypt',$lead->id),
                                'status'   => $statusSupply.''.$statusAllocation,
                            ];
                        }
                    }
                }
            }
        }

        if($filterPerPage == 'All'){
            $pageLength = $tableOperations->count();
        }else{
            $pageLength = $filterPerPage;
        }
        $tableOperations = $tableOperations->paginate($pageLength);

        $pendingIncident = PatientIncident::whereStatus('Pending')->count();
        return view('leads.datatable-operations-index', compact('tableOperations','leads','allocationArray','filterFromDate','filterToDate','filterStatus','filterPerPage','pendingIncident'));
    }

    public function getLeadSearch(Request $request){

        $filterFromDate = session('from_date_leads');
        $filterToDate   = session('to_date_leads');
        $filterStatus   = session('status_leads');
        $filterPerPage  = session('perpage_leads');

        $tableLeads = Lead::with(['patient' => function($q){
                        $q->addSelect('id','first_name','last_name','enquirer_name','contact_number','alternate_number');
                    },'referralSource','serviceRequired'])
                    ->whereRaw(\DB::raw('DATE(leads.created_at) BETWEEN "'.$filterFromDate.'" AND "'.$filterToDate.'"'))
                    ->whereIn('status',['Pending','Follow Up'])
                    ->orderBy('created_at','Desc');

        if($filterStatus != 'All'){
            $tableLeads = $tableLeads->whereStatus($filterStatus)->orderBy('leads.created_at','Desc');
        }

        if(!empty($request->searchString)){
            $tableLeads->whereHas('patient', function($q) use ($request){
                $q->where('first_name','like','%'.$request->searchString.'%')
                ->orWhere('last_name','like','%'.$request->searchString.'%');
            });
        }

        if($filterPerPage == 'All'){
            $pageLength = $tableLeads->count();
        }else{
            $pageLength = $filterPerPage;
        }

        $tableLeads = $tableLeads->paginate($pageLength);

        return view('partials.leads.paginator', compact('tableLeads'));
    }

    public function getOperationSearch(Request $request){
        $filterFromDate = session('from_date_operations');
        $filterToDate   = session('to_date_operations');
        $filterStatus   = session('status_operations');
        $filterPerPage  = session('perpage_operations');

        $tableOperations = Lead::with(['patient' => function($q){
                                $q->addSelect('id','first_name','last_name','enquirer_name','contact_number','alternate_number');
                            },'referralSource','serviceRequired'])
                            ->whereRaw(\DB::raw('DATE(leads.created_at) BETWEEN "'.$filterFromDate.'" AND "'.$filterToDate.'"'))
                            ->whereNotIn('status',['Pending','Follow Up'])
                            ->orderBy('created_at','Desc');

        if($filterStatus != 'All'){
            $tableOperations = $tableOperations->whereStatus($filterStatus)->orderBy('leads.created_at','Desc');
        }

        if(!empty($request->searchString)){
            $tableOperations = $tableOperations->whereHas('patient', function($q) use ($request){
                $q->where('first_name','like','%'.$request->searchString.'%')
                ->orWhere('last_name','like','%'.$request->searchString.'%');
            })->orWhere('episode_id','like','%'.$request->searchString.'%')
            ->whereRaw(\DB::raw('DATE(leads.created_at) BETWEEN "'.$filterFromDate.'" AND "'.$filterToDate.'"'));
        }

        if($filterPerPage == 'All'){
            $pageLength = $tableOperations->count();
        }else{
            $pageLength = $filterPerPage;
        }

        $tableOperations = $tableOperations->paginate($pageLength);

        return view('partials.operations.paginator', compact('tableOperations'));
    }

    public function saveOtherRequests(Request $request)
    {
        $data = $request->except('_token');
        $data['lead_id'] = Helper::encryptor('decrypt',$request->lead_id);

        $res = LeadRequests::updateorCreate(['lead_id' => $data['lead_id']],$data);

        if($request->has('lead_id') && $request->lead_id){
            $id = Helper::encryptor('decrypt',$request->lead_id);
            $lead = Lead::withoutGlobalScopes()->whereId($id)->first();
            if($lead){
                $lead->update(['status' => 'Checked']);
            }
        }

        return back()->with('alert_success','Lead data saved successfully');
    }

    public function checkForExistingCase(Request $request)
    {
        $res = [];
        $results = [];

        $patient = Patient::select('*');

        if(!empty($request->m)){
            $patient->whereMobileNumber($request->m);
        }
        if(!empty($request->e)){
            $patient->whereEmail($request->e);
        }
        if(!empty($request->f)){
            $patient->where('first_name','like','%'.$request->f.'%')
            ->orWhere('last_name','like','%'.$request->f.'%');
        }
        if(!empty($request->l)){
            $patient->where('first_name','like','%'.$request->l.'%')
            ->orWhere('last_name','like','%'.$request->l.'%');
        }
        if(!empty($request->d)){
            $patient->whereDateOfBirth($request->d);
        }

        $result = $patient->get();
        if($result){
            foreach($patient as $p){
                $lead = Lead::wherePatientId($p->id)->get();
                if($lead){
                    $results[] = $lead;
                }
            }
        }

        return response()->json($results, 200);
    }

    public function searchPatient(Request $request)
    {
        $res = [];
        $results = [];

        $patient = Patient::select('id','patient_id','first_name','last_name','date_of_birth','patient_age','patient_weight','gender','contact_number','email','alternate_number','enquirer_name','relationship_with_patient','street_address','area','city','zipcode','state','country','latitude','longitude');
        $number = preg_replace('~^[0\D]++|\D++~','', $request->t);

        if(!empty($request->c)){
            switch ($request->c) {
                case 'ID':
                    $patient->wherePatientId($request->t);
                    break;

                case 'Mobile':
                    $patient->whereRaw(\DB::raw("replace(`contact_number`,' ','') like '%$number%'"))
                            ->orWhereRaw(\DB::raw("replace(`alternate_number`,' ','') like '%$number%'"));
                    break;

                case 'Email':
                    $patient->where('email','like','%'.$request->t.'%');
                    break;
            }
        }
        if(!empty($request->e)){
            $patient->whereEmail($request->e);
        }
        if(!empty($request->f)){
            $patient->where('first_name','like','%'.$request->f.'%')
                    ->orWhere('last_name','like','%'.$request->f.'%');
        }
        if(!empty($request->l)){
            $patient->where('first_name','like','%'.$request->l.'%')
                    ->orWhere('last_name','like','%'.$request->l.'%');
        }

        $results = $patient->get();

        return response()->json($results, 200);
    }

    public function create(Request $request)
    {   
        $cadre = explode(',',ManagementComposition::whereType('leads_converted_by')->value('composition_cadre'));
        $data['languages'] = Language::get();
        $data['services'] = Service::get();
        $data['leadRequestId'] = 0;
        if(Helper::isAggregator()){
            $data['l'] = Lead::whereId(Helper::encryptor('decrypt',$request->id))->first();
            if($request->req_id){
                $requestId = \Helper::encryptor('decrypt',$request->req_id);
                $data['l'] = LeadRequests::whereId($requestId)->first();
                $data['leadRequestId'] = $requestId;
            }
            return view('leads.aggregator-create', $data);
        }

        $data['servicecategories'] = ServiceCategory::get();
        if(session('orgType') == 'Physiotherapy'){
            $data['services'] = Service::where('service_type','PHYSIOTHERAPY')->orderBy('category_id','Asc')->get();
        }else{
            $data['services'] = Service::orderBy('category_id','Asc')->get();
        }
        $data['specializations'] = Specialization::get();
        $data['managers'] = Caregiver::active()->whereHas('professional', function($q){
                                        $q->whereRole('Manager');
                                    })->get();

        $data['categories'] = ReferralCategory::get();

        $data['staffs'] = Caregiver::active()->select('id','employee_id','first_name','middle_name','last_name')
                                     ->whereHas('professional', function($q) use ($cadre){
                                        $q->whereIn('department',$cadre);
                                     })->get();

        $data['sources'] = ReferralSource::orderBy('category_id','Asc')->get();

        $holidays = Holidays::get()->pluck('holiday_date');
        if(count($holidays) != 0){

            $data['holidays'] = [];
            $data['holidaysr'] = [];

            foreach ($holidays as $h) {
                array_push($data['holidays'],$h->format('d-m-Y'));
                array_push($data['holidaysr'],$h->format('Y-m-d'));
            }
            $data['holidays'] = implode('","',$data['holidays']);
            $data['holidaysr'] = implode('","',$data['holidaysr']);
        }else{
            $data['holidays'] = '20-03-1992';
            $data['holidaysr'] = '1992-03-20';
        }

        return view('leads.create', $data);
    }

    public function saveLead(Request $request)
    {
        if(isset($request->email) && $request->email != null){
            if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
              $emailErr = "Invalid email format"; 
              return redirect()->back()->with('alert_error', $emailErr);
            }
        }

        $id = ($request->id!='0')?\Helper::encryptor('decrypt',$request->id):0;
        $patientID = ($request->patient_id!='0')?(!is_numeric($request->patient_id))?\Helper::encryptor('decrypt',$request->patient_id):$request->patient_id:0;
        $saveLeadOnly = \Helper::encryptor('decrypt',$request->mode);
        if($saveLeadOnly != 'L'){
            $patientDetails = $request->only('first_name','last_name','date_of_birth','patient_age','patient_weight','gender','contact_number','email','alternate_number','enquirer_name','relationship_with_patient','street_address','area','city','zipcode','state','country','latitude','longitude');

            if($patientID > 0){
                $patient = Patient::find($patientID);
            }else{
                $patient = null;
            }

            if(!empty($patientDetails['date_of_birth'])){
                $date = new \DateTime($patientDetails['date_of_birth']);
                $patientDetails['date_of_birth'] = date_format($date,'Y-m-d');
            }else{
                $patientDetails['date_of_birth'] = null;
            }

            $patientDetails['tenant_id'] = Helper::getTenantID();

            // Create or Update Patient data
            if($patient){
                if(empty($patient->patient_id) && $request->status == 'Converted'){
                    $prefix = Branch::whereId($request->branch_id)->pluck('patient_id_prefix')->first();
                    if($prefix){
    		            $lastPatientId = Patient::whereNotNull('patient_id')->whereBranchId($request->branch_id)->orderBy(\DB::raw('length(patient_id),patient_id'))->get()->last();
                        if($lastPatientId)
                            $lastID = substr($lastPatientId->patient_id, strlen($prefix));
                        else
                            $lastID = Branch::whereId($request->branch_id)->pluck('patient_id_start_no')->first();

                        $patientDetails['patient_id'] = $prefix.str_pad(intval($lastID + 1),strlen($lastID),0,STR_PAD_LEFT);
                    }
                }
                $patient->update($patientDetails);
                $patientID = $patient->id;
            }else{
                $patientDetails['created_at'] = date('Y-m-d H:i:s');
                // Generate Unique Patient ID
                if(empty($patient->patient_id) && $request->status == 'Converted'){
                    $prefix = Branch::whereId($request->branch_id)->pluck('patient_id_prefix')->first();
                    
                    if($prefix){
                        $lastPatientId = Patient::whereNotNull('patient_id')->whereBranchId($request->branch_id)->orderBy(\DB::raw('length(patient_id),patient_id'))->get()->last();
                        if(!$lastPatientId){
                            $lastID = Branch::whereId($request->branch_id)->pluck('patient_id_start_no')->first();
                        }else{
                            $lastID = substr($lastPatientId->patient_id, strlen($prefix));
                        }
                        $patientDetails['patient_id'] = $prefix.str_pad(intval($lastID + 1),strlen($lastID),0,STR_PAD_LEFT);
                    }
                }
                if (isset($patientDetails['contact_number'])) {
                    $patientDetails['contact_number'] = preg_replace('/\s+/','',$patientDetails['contact_number']);
                }
                if (isset($patientDetails['alternate_number'])) {
                    $patientDetails['alternate_number'] = preg_replace('/\s+/','',$patientDetails['alternate_number']);
                }
                $patientDetails['branch_id'] = $request->branch_id;
                $patientID = Patient::insertGetId($patientDetails);
            }
        }

        // Create lead details
        $caseDetails = $request->only('case_description','case_type','medical_conditions','medications','procedures','hospital_name','primary_doctor_name','special_instructions','service_category','service_required','service_offered','manager_id','current_rack_rate','estimated_duration','gender_preference','language_preference','assessment_date','assessment_time','assessment_notes','rate_agreed','registration_amount','payment_mode','payment_notes','referral_category','referral_source','referrer_name','converted_by','referral_value','referral_type','branch_id','status','dropped_reason','dropped_reason_other','remarks');

        if($patientID > 0){
            $caseDetails['patient_id'] = $patientID;
            $caseDetails['branch_id'] = $request->branch_id;

            $caseDetails['tenant_id'] = Helper::getTenantID();

            if(!empty($caseDetails['dropped_reason']))
            $caseDetails['dropped_reason'] = json_encode($caseDetails['dropped_reason']);

            if(!empty($caseDetails['dropped_reason_other']))
            $caseDetails['dropped_reason_other'] = $request->dropped_reason_other;

            if(!empty($caseDetails['language_preference']))
                $caseDetails['language_preference'] = implode(",",$caseDetails['language_preference']);

            if($id == 0){
                $caseDetails['created_at'] = $caseDetails['updated_at'] = date('Y-m-d H:i:s');

                // Generate Unique Episode ID
                if($request->status == 'Converted'){
                    $caseDetails['episode_id'] = Patient::find($patientID)->patient_id.str_pad(Lead::wherePatientId($patientID)->count() + 1,2,0,STR_PAD_LEFT);
                }

                $lead = Lead::create($caseDetails);
                $leadID = $lead->id;
            }else{
                $lead = Lead::find($id);
                if(empty($lead->episode_id) && $request->status == 'Converted'){
                    // Generate Unique Episode ID
                    $caseDetails['episode_id'] = Patient::find($patientID)->patient_id.str_pad(Lead::wherePatientId($patientID)->count() + 1,2,0,STR_PAD_LEFT);
                }

                $caseDetails['updated_at'] = date('Y-m-d H:i:s');
                $leadID = $id;
                $lead->update($caseDetails);
            }

            if($caseDetails['status'] == 'Follow Up'){
                $followupData = $request->only('status','follow_up_datetime','follow_up_comment');
                $followupData['lead_id'] = $leadID;
                $followupData['user_id'] = \Helper::getUserID();
                $followupData['disposition_date'] = date('Y-m-d H:i:s');
                CaseDisposition::firstOrCreate($followupData);
            }else if($caseDetails['status'] == 'Dropped'){
                $droppedData['lead_id'] = $leadID;
                $droppedData['status'] = $request->status;
                $droppedData['caselosscomment'] = isset($request->dropped_reason)?implode(',',$request->dropped_reason):null;
                $droppedData['user_id'] = \Helper::getUserID();
                $droppedData['disposition_date'] = date('Y-m-d H:i:s');
                CaseDisposition::firstOrCreate($droppedData);
            }else{
                $dispoData['lead_id'] = $leadID;
                $dispoData['user_id'] = \Helper::getUserID();
                $dispoData['status'] = $request->status;
                $dispoData['disposition_date'] = date('Y-m-d H:i:s');
                if($caseDetails['status'] == "Converted"){
                    $check = CaseDisposition::whereLeadId($leadID)->whereStatus('Converted')->first();
                    if(!$check){
                        $dispoData['converted'] = 1;

                    }
                }
                CaseDisposition::firstOrCreate($dispoData);
            }

            $commission = ReferralPayout::whereLeadId($leadID)->first();
             // && $caseDetails['status'] == 'Converted'
            if(!$commission && $request->referral_source > 0){
                $newCommission = new ReferralPayout;

                $newCommission->lead_id = $leadID;
                $newCommission->tenant_id = Helper::getTenantID();
                $newCommission->source_id = $request->referral_source;
                $newCommission->value = ReferralSource::whereId($request->referral_source)->value('referral_value');
                $newCommission->type = $request->referral_type;
                $newCommission->patient_id = $patientID;

                $newCommission->save();
            }

            // if($caseDetails['manager_id'] > 0 && $caseDetails['status'] == 'Converted'){
            //     $extraData['lead_id'] = $leadID;

            //     // Send Push Notification to Manager
            //     $this->sendNotificationToManager($caseDetails['manager_id'],$extraData);
            // }
        }

        
        if($lead->patient->email != null && $id == 0 && $request->status == 'Converted' && filter_var($lead->patient->email, FILTER_VALIDATE_EMAIL)){
            $mail = Mail::to($lead->patient->email);
            $mail->send(new LeadConvertion($lead,$lead->patient));
        }

        if($saveLeadOnly == 'L')
            return back()->with('alert_success','Details saved successfully');

        return redirect()->route('lead.index')->with('alert_success','Lead saved successfully');
    }

    public function edit(Request $request, $id)
    {
        $id = Helper::encryptor('decrypt',$id);
        $data['leadRequestId'] = null;
        $data['servicecategories'] = ServiceCategory::get();
        if(session('orgType') == 'Physiotherapy'){
            $data['services'] = Service::where('service_type','PHYSIOTHERAPY')->orderBy('category_id','Asc')->get();
        }else{
            $data['services'] = Service::orderBy('category_id','Asc')->get();
        }
        $data['languages'] = Language::get();
        $data['specializations'] = Specialization::get();
        $data['managers'] = Caregiver::whereHas('professional', function($q){
                                        $q->whereRole('Manager');
                                    })->get();

        $data['categories'] = ReferralCategory::get();
        $data['sources'] = ReferralSource::orderBy('category_id','Asc')->get();

        $data['l'] = Lead::find($id);
        $data['dropped_reason']=json_decode($data['l']['dropped_reason']);

        $holidays = Holidays::get()->pluck('holiday_date');
        if(count($holidays) != 0){

            $data['holidays'] = [];
            $data['holidaysr'] = [];

            foreach ($holidays as $h) {
                array_push($data['holidays'],$h->format('d-m-Y'));
                array_push($data['holidaysr'],$h->format('Y-m-d'));
            }
            $data['holidays'] = implode('","',$data['holidays']);
            $data['holidaysr'] = implode('","',$data['holidaysr']);
        }else{
            $data['holidays'] = '20-03-1992';
            $data['holidaysr'] = '1992-03-20';
        }

        return view('leads.create', $data);
    }

    public function view(Request $request, $id)
    {
        $id = Helper::encryptor('decrypt',$id);
        $data['l'] = Lead::whereId($id)
                        ->with(['patient', 'serviceRequests', 'schedules',
                        'assessment' => function($q){
                            $q->addSelect('id','lead_id','form_type','form_data','created_at');
                        },
                        'billables' => function($q){
                            $q->addSelect('id','lead_id','created_at','category','item_id','item','rate','quantity','amount');
                        },
                        'billables.consumable' => function($q){
                            $q->addSelect('id','consumable_name','consumable_price','tax_id');
                        },
                        'billables.surgical' => function($q){
                            $q->addSelect('id','surgical_name','surgical_price','tax_id');
                        },
                        'billables.tests' => function($q){
                            $q->addSelect('id','test_name','test_price','tax_id');
                        },
                        'comments' => function($q){
                            $q->addSelect('id','lead_id','user_name','comment','created_at');
                        },
                        'serviceRequests.service' => function($q){
                            $q->addSelect('id','service_name');
                        },'serviceRequired' => function($q){
                            $q->addSelect('id','service_name');
                        }])
                        ->first();

        if(!$data['l']){
            return back()->with('alert_error','Unable to find lead!');
        }

        if(session('orgType') == 'Physiotherapy'){
            $data['services'] = Service::select('id','service_type','service_name')->where('service_type','PHYSIOTHERAPY')->with('ratecard')->get();
        }else{
            $data['services'] = Service::select('id','service_type','service_name')->with('ratecard')->get();
        }

        if(\Helper::getSetting('schedule_creation_for_outstanding_customer') ==  1 && !\Entrust::ability('admin','leads-schedules-authorize'))
            $data['outstanding'] = ['total_charges' => 0,'total_payments' => 0,'outstanding_balance' => 0];
        else
            $data['outstanding'] = ['total_charges' => 0,'total_payments' => 0,'outstanding_balance' => 0];

        $data['servicecategories'] = ServiceCategory::select('id','category_name')->get();
        $data['consumables'] = Consumables::select('id','consumable_name','consumable_price','tax_id')->get();
        $data['surgicals'] = Surgicals::select('id','surgical_name','surgical_price','tax_id')->get();
        $data['tests'] = LabTests::get();

        $data['categories'] = ReferralCategory::select('id','category_name')->get();
        $data['languages'] = Language::select('id','language_name')->get();
        $data['skills'] = Skill::select('id','skill_name')->get();
        $data['sources'] = ReferralSource::select('id','source_name')->get();
        $data['managers'] = Caregiver::whereHas('professional', function($q){
                                        $q->whereRole('Manager');
                                    })->get();
        $data['specializations'] = Specialization::select('id','specialization_name')->get();
        $data['feedbackcomments'] = [];
        $data['losscomment'] = [];
        $data['persons'] = Caregiver::select('id','first_name','middle_name','last_name','employee_id')
                            ->whereHas('professional',function($q){
                                $q->whereRole('Manager');
                            })->get();

        $data['vendors'] = BillingProfile::get();
        $data['tasks'] = Task::select('id','category','task_name','description')->get();

        $holidays = Holidays::select('holiday_name','holiday_date')->get()->pluck('holiday_date');
        if(count($holidays) != 0){
            $data['holidays'] = [];
            $data['holidaysr'] = [];

            foreach ($holidays as $h) {
                if(!empty($h)){
                    array_push($data['holidays'],$h->format('d-m-Y'));
                    array_push($data['holidaysr'],$h->format('Y-m-d'));
                }
            }
            $data['holidays'] = implode('","',$data['holidays']);
            $data['holidaysr'] = implode('","',$data['holidaysr']);
        }else{
            $data['holidays'] = '20-03-1992';
            $data['holidaysr'] = '1992-03-20';
        }
        $data['sources'] = ReferralSource::orderBy('category_id','Asc')->get();
        $data['extraAddress'] = PatientAddress::wherePatientId($data['l']->patient->id)->whereStatus('active')->get();
        $data['cl'] = PatientCheckList::wherePatientId($data['l']->patient->id)->first();
        return view('leads.view-lead', $data);
    }

    public function addDetailedAssessment(Request $request, $id=null)
    {
        if(isset($id)){
            $lead = Lead::find(\Helper::encryptor('decrypt',$id));
            if(!$id)
                return back()->with('alert_error','Could not open assessment form!');

            return view('leads.detailed-assessment',compact('id'));
        }

        return back()->with('alert_error','Could not open assessment form!');
    }

    public function saveDetailedAssessment(Request $request)
    {
        $data['lead_id'] = \Helper::encryptor('decrypt',$request->lead_id);
        $data['branch_id'] = \Helper::getBranchID();
        $data['tenant_id'] = \Helper::getTenantID();
        $data['user_id'] = \Helper::getAdminID();
        $data['type'] = 'Detailed';
        $data['form_type'] = 'Assessment';
        $data['form_data'] = $request->form_data;
        $res = false;
        if($data){
            CaseForm::create($data);
            $res = true;
        }

        return response()->json(['res' => $res],200);
    }

    public function updateDetailedAssessment(Request $request)
    {
        $case_forms_id = $request->case_forms_id;
        $data['form_data'] = $request->form_data;
        $res = false;
        if($data){
            CaseForm::where('id',$case_forms_id)->update($data);
            $res = true;
        }
        return response()->json(['res' => $res],200);
    }

    public function viewDetailedAssessment(Request $request, $id)
    {
        $form = CaseForm::find(\Helper::encryptor('decrypt',$id));

        return view('leads.detailed-assessment', compact('form'));
    }

    public function viewDetailedAssessmentEdit(Request $request, $id)
    {
        $form = CaseForm::find(\Helper::encryptor('decrypt',$id));
        $case_forms_id = \Helper::encryptor('decrypt',$id);
        return view('leads.detailed-assessment', compact('form','case_forms_id'));
    }

    public function deleteAssessment(Request $request, $id)
    {   
        $cf = CaseForm::find(\Helper::encryptor('decrypt',$id));
        if(isset($cf)){
            $cf->delete();
            session()->flash('alert_success', 'Assessment Deleted Successfully.');
        } else {
            session()->flash('alert_warning', 'Assessment Not Found.');
        }        
        return redirect()->back();
    }

    public function deleteCaseFollowUp(Request $request, $id)
    {   
        $cf = CaseFollowUp::find(\Helper::encryptor('decrypt',$id));
        if(isset($cf)){
            $cf->delete();
            session()->flash('alert_success', 'Follow-Up Deleted Successfully.');
        } else {
            session()->flash('alert_warning', 'Follow-Up Not Found.');
        }        
        return redirect()->back();
    }

    public function getSchedulesData(Request $request)
    {
        if($request->has('tid'))
            self::setTenantConnection(\Helper::encryptor('decrypt',$request->tid));
        $schedules = Schedule::query()->withoutGlobalScopes()->whereLeadId($request->lead_id)->with('patient')->with('caregiver')->with('service');

        return \Datatables::of($schedules)
            ->editColumn('schedule_date', function ($schedule) {
                return $schedule->schedule_date->format('d-m-Y').'<small style="font-weight:bold;text-align: center; float: right;">'.date("l", strtotime($schedule->schedule_date->format('d-m-Y'))).'</small>';
            })
            ->editColumn('service_required', function ($schedule) {
                return isset($schedule->service)?$schedule->service->service_name:'-';
            })
            ->editColumn('caregiver.first_name', function ($schedule) {

                if(isset($schedule->caregiver_id) && $schedule->caregiver_id != 0){
                    $caregiver = Caregiver::with('professional')->withoutGlobalScopes()->where('id',$schedule->caregiver_id)->first();
                    $str = $caregiver->first_name.' '.$caregiver->middle_name.' '.$caregiver->last_name;
                }
                else
                    $str = '-';

                if($schedule->caregiver_id != 0 && isset($schedule->schedule_date) && isset($str) && !in_array(date("D", strtotime($schedule->schedule_date->format('d-m-Y'))), explode(',',$caregiver->professional->working_days), true)){
                    $str .= '<small style="color: Red;font-weight:bold;text-align: center; float: right;">  Not Available on '. date("l", strtotime($schedule->schedule_date->format('d-m-Y'))).'s. Replacement Required.</small>';
                }

                return $str;
            })
            ->editColumn('chargeable', function ($schedule) {
                return $schedule->chargeable?'Yes':'No';
            })
            ->addColumn('action', function ($schedule) {
                return '<a class="btn btn-primary btnViewSchedule" data-id="'.$schedule->id.'" data-cid="'.\Helper::encryptor('encrypt',$schedule->caregiver_id).'">View</a>';
            })
            ->rawColumns(['schedule_date','caregiver.first_name'])
            ->make(true);
    }

    public function addBillables(Request $request)
    {
        $data = $request->all('patient_id','lead_id','billable_category','item','item_name','other_name','quantity','rate','amount');
        $cc = new CaseBillables;
        $cc->patient_id = \Helper::encryptor('decrypt',$data['patient_id']);
        $cc->tenant_id = \Helper::getTenantID();
        $cc->user_id = \Helper::getUserID();
        $cc->lead_id = \Helper::encryptor('decrypt',$data['lead_id']);
        $cc->category = $data['billable_category'];
        $cc->item_id = $data['item']=='0'?null:$data['item'];
        $cc->item = $data['billable_category'] == 'Pharmaceuticals'?$data['item_name']:$data['other_name'];
        $cc->quantity = $data['quantity'];
        $cc->rate = $data['rate'];
        $cc->amount = $data['amount'];
        $cc->save();

        return back()->with('billable_alert','Item added successfully');
    }

    public function removeBillableItem(Request $request)
    {
        $id = \Helper::encryptor('decrypt',$request->id);

        $billable = CaseBillables::find($id);

        if($billable){
            $billable->delete();
            return response()->json(true,200);
        }

        return response()->json(false,200);
    }

    public function sendBillablesToVendor(Request $request)
    {
        $vendorID = $request->vendor_id;
        $items = $request->items;

        if($vendorID && $items){
            $vendor = \App\Entities\Masters\BillingProfile::find($vendorID);
            $billables = CaseBillables::whereIn('id',$items)->get();
            if($vendor && $billables){
                if(filter_var($vendor->email, FILTER_VALIDATE_EMAIL)){
                    $mail = Mail::to($vendor->email);
                    $mail->send(new BillableToVendor($vendor, $billables));
                }
            }
        }

        return response()->json(true, 200);
    }

    public function saveCaseServices(Request $request)
    { 
        $id               = $request->id;
        $leadID           = Helper::encryptor('decrypt',$request->lead_id);
        $patientID        = Helper::encryptor('decrypt',$request->patient_id);
        $serviceRequested = $request->service_requested_id;
        $periodType       = $request->period_type;
        $customDates      = $request->custom_dates;
        $fromDate         = $request->service_request_from_date;
        $toDate           = $request->service_request_to_date;
        $fromTime         = $request->service_request_start_time;
        $toTime           = $request->service_request_end_time;
        $frequencyPeriod  = $request->frequency_period;
        $frequency        = $request->frequency;
        $grossRate        = $request->gross_rate;
        $discount         = $request->discount_applicable;
        $discountType     = $request->discount_type;
        $discountValue    = $request->discount_value;
        $netRate          = $request->net_rate;
        $caregiverID      = $request->caregiver_id;
        $roadDistance     = (empty($request->road_distance))?null:$request->road_distance;
        $notes            = $request->notes;
        $status           = 'To be Assigned';
        // $createSchedule   = $request->create_schedules;
        $assignTasks      = $request->assign_schedule_tasks;

        if($leadID && $patientID && $serviceRequested){
            $data = [];
            if($periodType == 'Period'){
                $customDates = null;
                if(!empty($fromDate))
                    $fromDate = date_format(new \DateTime($fromDate), 'Y-m-d');
                else
                    $fromDate = null;
                if(!empty($toDate))
                    $toDate = date_format(new \DateTime($toDate), 'Y-m-d');
                else
                    $toDate = null;
            }
            else{
                $fromDate = null;
                $toDate = null;
            }

            if(!empty($fromTime))
                $fromTime = date_format(new \DateTime($fromTime), 'H:i:s');
            else
                $fromTime = null;
            if(!empty($toTime))
                $toTime = date_format(new \DateTime($toTime), 'H:i:s');
            else
                $toTime = null;

            $data = [
                'tenant_id'        => Helper::getTenantID(),
                'branch_id'        => Helper::getBranchID(),
                'user_id'          => Helper::getUserID(),
                'lead_id'          => $leadID,
                'patient_id'       => $patientID,
                'service_id'       => $serviceRequested,
                'from_date'        => $fromDate,
                'to_date'          => $toDate,
                'custom_dates'     => $customDates,
                'frequency_period' => $frequencyPeriod,
                'frequency'        => $frequency,
                'gross_rate'       => $grossRate,
                'discount'         => $discount,
                'discount_type'    => $discountType,
                'discount_value'   => $discountValue,
                'net_rate'         => $netRate,
                'status'           => $status,
            ];

            if($id == 0){
                $res = CaseServices::updateOrCreate($data);
                $serviceRequestID = $res->id;
            }
            else{
                $res = CaseServices::whereId($id)->update($data);
                $serviceRequestID = $id;
            }

            // Add entry in case_service_orders
            $serviceOrder                     = new CaseServiceOrder;
            $serviceOrder->tenant_id          = $data['tenant_id'];
            $serviceOrder->branch_id          = $data['branch_id'];
            $serviceOrder->user_id            = $data['user_id'];
            $serviceOrder->lead_id            = $data['lead_id'];
            $serviceOrder->patient_id         = $data['patient_id'];
            $serviceOrder->from_date          = $data['from_date'];
            $serviceOrder->to_date            = $data['to_date'];
            $serviceOrder->custom_dates       = $data['custom_dates'];
            
            $serviceOrder->service_request_id = $serviceRequestID;
            $serviceOrder->payment_status     = 'Pending';
            $serviceOrder->gross_rate         = $grossRate;
            
            $taxId                            = RateCard::whereServiceId($serviceRequested)->whereStatus(1)->value('tax_id');
            $taxValue                         = isset($taxId)?Taxrate::whereId($taxId)->whereStatus(1)->value('tax_rate'):0.00;
            
            $serviceOrder->tax_rate           = $taxValue;
            $serviceOrder->created_at         = date('Y-m-d H:i:s');
            $serviceOrder->updated_at         = date('Y-m-d H:i:s');
            
            $serviceOrder->save();
            $csoId = $serviceOrder->id;

            if($periodType == 'Period'){
                $start = new \DateTime($fromDate);
                $end   = new \DateTime($toDate);
                $end   = $end->modify('+1 day');

                $period = new \DatePeriod($start,new \DateInterval('P1D'),$end);
            }else{
                $cDates = explode(',',trim($customDates));
                if(count($cDates)){
                    $period = array_map(function($item){
                        return \Carbon\Carbon::parse($item);
                    },$cDates);
                }
            }

            if(isset($period)){
                // Change the status to Pending
                CaseServices::whereId($serviceRequestID)->update(['status' => 'Pending']);
                foreach ($period as $p) {
                    $sdata = [
                        'tenant_id'          => Helper::getTenantID(),
                        'branch_id'          => Helper::getBranchID(),
                        'user_id'            => Helper::getUserID(),
                        'lead_id'            => $leadID,
                        'patient_id'         => $patientID,
                        'caregiver_id'       => $caregiverID,
                        'service_request_id' => $serviceRequestID,
                        'service_order_id'   => $csoId,
                        'schedule_date'      => $p->format('Y-m-d'),
                        'start_time'         => $fromTime,
                        'end_time'           => $toTime,
                        'service_required'   => $serviceRequested,
                        'amount'             => $netRate,
                        'ratecard_id'        => RateCard::whereServiceId($serviceRequested)->whereStatus(1)->value('id'),
                        'road_distance'      => $roadDistance,
                        'notes'              => $notes,
                        'chargeable'         => 1,
                        'delivery_address'   => $request->delivery_address,
                        'status'             => 'Pending',
                        'created_at'         => date('Y-m-d H:i:s'),
                        'updated_at'         => date('Y-m-d H:i:s'),
                    ];

                    if($assignTasks != null){
                        if($request->assigned_tasks){
                            $sdata['assigned_tasks'] = $request->assigned_tasks;
                        }
                    }

                    $checkid = Schedule::insertGetId($sdata);
                    Schedule::whereId($checkid)->update(['schedule_id' => 'SCH'.$checkid]);
                    if($fromDate == $toDate){
                        $schedule          = Schedule::find($checkid);
                        $categoryService   = isset($schedule->service->category)?$schedule->service->category->category_name:null;
                        $checkIVRSTenant   = \Helper::getTenantId() == 266;
                        $checkIVRSCategory = $categoryService == 'Visit Based';
                        if($checkIVRSTenant && $checkIVRSCategory){
                            //WARNING - Tenant ID in production mode to be set to 266 Only
                            $type = $request->ivrs_type;
                            \App\Helpers\IVRS::visitBasedCreation($type,$schedule);
                        }
                    }
                }
                if($caregiverID > 0){
                    $c              = Caregiver::whereId($caregiverID)->first();
                    $c->work_status = "On Duty";
                    $c->save();

                    // sending mail to patient about caregiver
                    $lead = Lead::whereId($leadID)->first();
                    $caregiver = Caregiver::whereId($caregiverID)->first();
                    if($lead->patient->email != null && filter_var($lead->patient->email, FILTER_VALIDATE_EMAIL)){
                        $mail = Mail::to($lead->patient->email);
                        $mail->send(new StaffAssign($lead,$caregiver));
                    }
                }
            }

            if($discount == 'Y'){
                $logData = [
                    'tenant_id'          => \Helper::encryptor('decrypt',session('tenant_id')),
                    'user_id'            => \Helper::encryptor('decrypt', session('admin_id')),
                    'patient_id'         => $patientID,
                    'lead_id'            => $leadID,
                    'service_request_id' => $serviceRequestID,
                    'from_date'          => isset($start)?$start->format('Y-m-d'):null,
                    'to_date'            => isset($end)?$end->modify('-1 day')->format('Y-m-d'):null,
                    'discount_type'      => $discountType,
                    'discount_value'     => $discountValue,
                    'amount'             => $netRate,
                    'created_at'         => date('Y-m-d H:i:s'),
                    'updated_at'         => date('Y-m-d H:i:s'),
                ];

            }else{
                $logData = [
                    'tenant_id'          => \Helper::encryptor('decrypt',session('tenant_id')),
                    'user_id'            => \Helper::encryptor('decrypt', session('admin_id')),
                    'patient_id'         => $patientID,
                    'lead_id'            => $leadID,
                    'service_request_id' => $serviceRequestID,
                    'from_date'          => isset($start)?$start->format('Y-m-d'):null,
                    'to_date'            => isset($end)?$end->modify('-1 day')->format('Y-m-d'):null,
                    'discount_type'      => 'F',
                    'discount_value'     => 0,
                    'amount'             => $netRate,
                    'created_at'         => date('Y-m-d H:i:s'),
                    'updated_at'         => date('Y-m-d H:i:s'),
                ];
            }
            DiscountsLog::insert($logData);

            return back()->with('alert_success','Service updated successfully');
        }

        return back();
    }

    public function getServiceOrderForDiscount(Request $request)
    {
        if($request->serviceRequestId != null){
            $serviceOrderForDiscount = [];
            $serviceOrders = CaseServiceOrder::select('id','gross_rate','from_date','to_date')->whereServiceRequestId($request->serviceRequestId)->get();
            foreach ($serviceOrders as $serviceOrder) {
                $schedules = Schedule::select('schedule_date')->whereServiceOrderId($serviceOrder->id)->whereStatus('Pending')->get();
                
                $scheduledDates = [];
                foreach ($schedules as $schedule) {
                    $scheduledDates[] = $schedule->schedule_date->format('Y-m-d');
                }
                $scheduledDates = implode(',',$scheduledDates);
                
                $serviceOrderForDiscount[] =[
                    'service_order_id' => $serviceOrder->id,
                    'gross_rate' => $serviceOrder->gross_rate,
                    'service_order_date' => $serviceOrder->from_date->format('d-m-Y').' to '.$serviceOrder->to_date->format('d-m-Y'),
                    'schedule_dates' => $scheduledDates,
                ];
            }
        }
        return response()->json($serviceOrderForDiscount,200);
    }

    public function addDiscount(Request $request)
    {
        if($request->apply_discount != ''){
            $serviceId = $request->service_id;
            $dates = explode(" to ",$request->apply_discount);

            $start = new \DateTime($dates[0]);
            if(isset($dates[0]) && !isset($dates[1])){
                $end = new \DateTime($dates[0]);
                $period = array(new \DateTime($dates[0]));
            }else{
                $end = new \DateTime($dates[1]);
                $end->setTime(0,0,1);
                $period = new \DatePeriod($start,new \DateInterval('P1D'),$end);
            }

            $data = $request->all();
            $serviceOrderParam = 'service_order_'.$request->service_id;
            $serviceOrderId = $request[$serviceOrderParam];
            foreach ($period as $p) {
                $schedules = Schedule::whereServiceRequestId($serviceId)->whereServiceOrderId($serviceOrderId)->whereStatus('Pending')->whereScheduleDate($p->format('Y-m-d'))->get();
                foreach ($schedules as $schedule) {
                    if(count($schedule->invoice)){
                        $previousRate = $schedule->amount;
                        $discountedAmount = $previousRate - $data['net_rate_after_'.$serviceId];
                        
                        $creditInit = \Helper::getSetting('credit_inits');
                        $creditStart = \Helper::getSetting('credit_start_no');
                        $creditCount = CreditMemo::count() + 1;

                        $cMemo['patient_id'] = $schedule->patient_id;
                        $cMemo['date'] = $schedule->schedule_date;
                        $cMemo['user_id'] = \Helper::getUserID();
                        $cMemo['schedule_id'] = $schedule->id;
                        $cMemo['amount'] = $discountedAmount;
                        $cMemo['comment'] = 'Discounted Item';
                        $cMemo['type'] = 'Schedule';
                        $cMemo['sub_type'] = 'Discount';
                        $cMemo['credit_memo_no'] = $creditInit.''.($creditStart+$creditCount);

                        CreditMemo::create($cMemo);
                    }

                    $schedule->update(['amount' => $data['net_rate_after_'.$serviceId]]);
                }
            }

            $logData = [
                'tenant_id'          => \Helper::encryptor('decrypt',session('tenant_id')),
                'user_id'            => \Helper::encryptor('decrypt', session('admin_id')),
                'patient_id'         => \Helper::encryptor('decrypt',$data['patient_id']),
                'lead_id'            => \Helper::encryptor('decrypt',$data['lead_id']),
                'service_request_id' => $data['service_id'],
                'service_order_id'   => $data['service_order_'.$serviceId],
                'from_date'          => $start->format('Y-m-d'),
                'to_date'            => $end->format('Y-m-d'),
                'discount_type'      => $data['discount_type_after_'.$serviceId],
                'discount_value'     => $data['discount_value_after_'.$serviceId],
                'amount'             => $data['net_rate_after_'.$serviceId],
                'created_at'         => date('Y-m-d H:i:s'),
                'updated_at'         => date('Y-m-d H:i:s'),
            ];

            DiscountsLog::insert($logData);

            return back()->with('alert_success','New Rates successfully Applied!');
        }else{
            return back()->with('alert_warning','A date range was not supplied properly!');
        }
    }
    
    public function createSchedulesFromService(Request $request)
    {
        if($request->has('mode') && $request->mode == 'Schedule'){
            $data = $request->only('create_schedule_period_from','create_schedule_period_to','create_schedule_start_time','create_schedule_end_time','service_request_id','service_required','caregiver_id','lead_id','patient_id');

            if(!empty($data['create_schedule_period_from']) && !empty($data['create_schedule_period_to'])){
                $start = new \DateTime($data['create_schedule_period_from']);
                $end   = new \DateTime($data['create_schedule_period_to']);
                $end   = $end->modify('+1 day');

                $period = new \DatePeriod($start,new \DateInterval('P1D'),$end);

                $fromTime = $request->create_schedule_start_time;
                $toTime   = $request->create_schedule_end_time;

                if(!empty($fromTime))
                    $fromTime = date_format(new \DateTime($fromTime), 'H:i:s');
                else
                    $fromTime = null;
                if(!empty($toTime))
                    $toTime = date_format(new \DateTime($toTime), 'H:i:s');
                else
                    $toTime = null;

                $serviceId = CaseServices::whereId(Helper::encryptor('decrypt',$data['service_request_id']))->value('service_id');
                $amount    = RateCard::whereServiceId($serviceId)->whereStatus(1)->value('amount');

                // Add entry in case_service_orders
                $serviceOrder                     = new CaseServiceOrder;
                $serviceOrder->tenant_id          = Helper::getTenantID();
                $serviceOrder->branch_id          = Helper::getBranchID();
                $serviceOrder->user_id            = Helper::getUserID();
                $serviceOrder->lead_id            = Helper::encryptor('decrypt',$data['lead_id']);
                $serviceOrder->patient_id         = Helper::encryptor('decrypt',$data['patient_id']);
                $serviceOrder->service_request_id = Helper::encryptor('decrypt',$data['service_request_id']);
                $serviceOrder->from_date          = Carbon::parse($data['create_schedule_period_from'])->format('Y-m-d');
                $serviceOrder->to_date            = Carbon::parse($data['create_schedule_period_to'])->format('Y-m-d');
                $serviceOrder->gross_rate         = $amount;
                $serviceOrder->payment_status     = 'Pending';
                $taxId                            = RateCard::whereServiceId(Helper::encryptor('decrypt',$data['service_required']))->whereStatus(1)->value('tax_id');
                $taxValue                         = isset($taxId)?Taxrate::whereId($taxId)->whereStatus(1)->value('tax_rate'):0.00;
                $serviceOrder->tax_rate           = $taxValue;
                $serviceOrder->created_at         = date('Y-m-d H:i:s');
                $serviceOrder->updated_at         = date('Y-m-d H:i:s');
                $serviceOrder->save();
                $cso                              = $serviceOrder->id;

                if(count((array)$period)){
                    foreach ($period as $p) {
                        $sdata = [
                            'tenant_id'          => Helper::getTenantID(),
                            'branch_id'          => Helper::getBranchID(),
                            'user_id'            => Helper::getUserID(),
                            'lead_id'            => Helper::encryptor('decrypt',$data['lead_id']),
                            'patient_id'         => Helper::encryptor('decrypt',$data['patient_id']),
                            'caregiver_id'       => $data['caregiver_id'],
                            'service_request_id' => Helper::encryptor('decrypt',$data['service_request_id']),
                            'service_order_id'   => $cso,
                            'schedule_date'      => $p->format('Y-m-d'),
                            'start_time'         => $fromTime,
                            'end_time'           => $toTime,
                            'amount'             => $request->extended_price_rate,
                            'ratecard_id'        => RateCard::whereServiceId(Helper::encryptor('decrypt',$data['service_required']))->whereStatus(1)->value('id'),
                            'road_distance'      => (empty($request->road_distance))?null:$request->road_distance,
                            'service_required'   => Helper::encryptor('decrypt',$data['service_required']),
                            'chargeable'         => 1,
                            'delivery_address'   => $request->delivery_address_schedule,
                            'status'             => 'Pending',
                            'created_at'         => date('Y-m-d H:i:s'),
                            'updated_at'         => date('Y-m-d H:i:s'),
                        ];

                        $checkid = Schedule::insertGetId($sdata);
                        Schedule::whereId($checkid)->update(['schedule_id' => 'SCH'.$checkid]);
                        if($data['create_schedule_period_from'] == $data['create_schedule_period_to']){
                            $schedule          = Schedule::find($checkid);
                            $categoryService   = isset($schedule->service->category)?$schedule->service->category->category_name:null;
                            $checkIVRSTenant   = \Helper::getTenantId() == 266;
                            $checkIVRSCategory = $categoryService == 'Visit Based';

                            if($checkIVRSTenant && $checkIVRSCategory){
                                //WARNING - Tenant ID in production mode to be set to 266 Only
                                $type = $request->ivrs_type;
                                \App\Helpers\IVRS::visitBasedCreation($type,$schedule);
                            }
                        }
                    }

                    if($request->extended_price_rate != $amount){
                        $logData = [
                            'tenant_id'          => Helper::encryptor('decrypt',session('tenant_id')),
                            'user_id'            => Helper::encryptor('decrypt', session('admin_id')),
                            'patient_id'         => Helper::encryptor('decrypt',$data['patient_id']),
                            'lead_id'            => Helper::encryptor('decrypt',$data['lead_id']),
                            'service_request_id' => Helper::encryptor('decrypt',$data['service_request_id']),
                            'from_date'          => $start->format('Y-m-d'),
                            'to_date'            => $end->modify('-1 day')->format('Y-m-d'),
                            'discount_type'      => 'F',
                            'discount_value'     => abs($request->extended_price_rate - $amount),
                            'amount'             => $request->extended_price_rate,
                            'created_at'         => date('Y-m-d H:i:s'),
                            'updated_at'         => date('Y-m-d H:i:s'),
                        ];

                        DiscountsLog::insert($logData);
                    }

                    if($data['caregiver_id'] > 0){
                        $c              = Caregiver::whereId($data['caregiver_id'])->first();
                        $c->work_status = "On Duty";
                        $c->save();

                        // sending mail to patient about caregiver
                        $lead = Lead::whereId(Helper::encryptor('decrypt',$data['lead_id']))->first();
                        $caregiver = Caregiver::whereId($data['caregiver_id'])->first();
                        if($lead->patient->email != null && filter_var($lead->patient->email, FILTER_VALIDATE_EMAIL)){
                            $mail = Mail::to($lead->patient->email);
                            $mail->send(new StaffAssign($lead,$caregiver));
                        }
                    }

                    // Change the Service status to Pending and Start-End Dates
                    $caseService = CaseServices::whereId(Helper::encryptor('decrypt',$data['service_request_id']))->first();
                        
                    if($start < $caseService->from_date )
                        $caseService->from_date = $start->format('Y-m-d');
                        
                    if($end > $caseService->to_date)
                        $caseService->to_date   = $end->modify('-1 day')->format('Y-m-d');

                    $caseService->status = 'Pending';
                    $caseService->save();
                }

                return back()->with('alert_success','Schedule(s) created successfully!');
            }
        }

        return back();
    }

    public function schedulesByServiceRequest(Request $request)
    {
        $data = [];
        $leadID = Helper::encryptor('decrypt',$request->data['leadId']);
        $serviceRequestId = Helper::encryptor('decrypt',$request->data['serviceRequestId']);

        if($leadID && $serviceRequestId){
            $cs = CaseServices::whereId($serviceRequestId)->first();
            $type = 'Regular';
            if($cs->custom_dates){
                $type = 'Custom';
            }
            $schedules = Schedule::whereLeadId($leadID)->whereServiceRequestId($serviceRequestId)
                            ->orderBy('schedule_date','Asc')
                            ->get();
            if(count($schedules)){
                foreach ($schedules as $schedule) {

                    $data[] = [
                        'id'                 => Helper::encryptor('encrypt',$schedule->id),
                        'schedule_no'        => $schedule->schedule_id,
                        'lead_id'            => Helper::encryptor('encrypt',$schedule->lead_id),
                        'service_request_id' => Helper::encryptor('encrypt',$schedule->service_request_id),
                        'date'               => $schedule->schedule_date->format('Y-m-d'),
                        'schedule_date'      => $schedule->schedule_date?$schedule->schedule_date->format('d-m-Y'):'',
                        'timings'            => (isset($schedule->start_time)?date_format(new \DateTime($schedule->start_time),'h:i A'):'NA').' - '.(isset($schedule->end_time)?date_format(new \DateTime($schedule->end_time),'h:i A'):'NA'),
                        'service_name'       => $schedule->service?$schedule->service->service_name:'',
                        'caregiver_id'       => Helper::encryptor('encrypt',$schedule->caregiver_id),
                        'employee_id'        => $schedule->caregiver?$schedule->caregiver->employee_id:'',
                        'employee_name'      => $schedule->caregiver?trim($schedule->caregiver->full_name):'',
                        'employee_phone'     => $schedule->caregiver?$schedule->caregiver->mobile_number:'',
                        'amount'             => $schedule->amount?$schedule->amount:'-',
                        'delivery_address_id'=> $schedule->delivery_address,
                        'delivery_location'  => Helper::getDeliveryAddress($schedule->patient_id,$schedule->delivery_address),
                        'chargeable'         => $schedule->chargeable,
                        'status'             => $schedule->status,
                        'changeability'      => ($schedule->schedule_date->format('Y-m-d') <= date('Y-m-d'))?'Y':'N',
                        'assigned_tasks'     => $schedule->assigned_tasks
                    ];
                }
            }

            $serviceId = $cs->service_id;
            $amount = RateCard::whereServiceId($serviceId)->whereStatus(1)->value('amount');
            if(count($schedules)){
                $lastScheduleAmount = Schedule::whereServiceRequestId($schedule->service_request_id)
                                                ->orderBy('schedule_date','Desc')
                                                ->orderBy('created_at','Desc')
                                                ->value('amount');
            }else{
                $lastScheduleAmount = $amount;
            }
            $price = [
                'ratecardAmount'=> $amount,
                'lastScheduleAmount'=> $lastScheduleAmount,
            ];
        }
        return response()->json([$data,$type,$price], 200);
    }

    public function updateServiceStatus(Request $request)
    {

        $id = Helper::encryptor('decrypt',$request->id);
        $leadID = Helper::encryptor('decrypt',$request->lead_id);
        $status = $request->status;
        $res = false;

        if($id && $status){
            $service = CaseServices::find($id);
            if(!$service){
                return response()->json(['result' => false],200);
            }

            if($status == 'Pending' || $status == 'No Supply'){
                $service->update(['status' => $status]);
                return response()->json(['result' => true],200);
            }

            if($status == 'Completed'){
                $schedules = Schedule::whereLeadId($leadID)->whereServiceRequestId($id)->whereStatus('Pending')->whereDate('schedule_date','<=',date('Y-m-d'))->where('caregiver_id','>',0)->get();
                if(count($schedules)){
                    foreach ($schedules as $schedule) {
                        $schedule->update(['status' => 'Completed']);
                        $taxRate = $schedule->service->ratecard->tax->tax_rate;
                        $finalAmount = $schedule->amount + ($schedule->amount)*(($taxRate)/100);
                    }
                }
            }

            if($status == 'Complete Service'){
                $schedulesComplete = Schedule::whereLeadId($leadID)->whereServiceRequestId($id)->whereStatus('Pending')->whereDate('schedule_date','<=',date('Y-m-d'))->where('caregiver_id','>',0)->get();
                if(count($schedulesComplete)){
                    foreach ($schedulesComplete as $schedule) {
                        $schedule->update(['status' => 'Completed']);
                        $taxRate = $schedule->service->ratecard->tax->tax_rate;
                        $finalAmount = $schedule->amount + ($schedule->amount)*(($taxRate)/100);
                    }
                }

                $schedulesCancel = Schedule::whereLeadId($leadID)->whereStatus('Pending')->whereServiceRequestId($id)->get();
                if(count($schedulesCancel)){
                    foreach ($schedulesCancel as $schedule) {
                        $schedule->update(['status' => 'Cancelled']);
                        if($schedule->invoice){
                            $data['patient_id'] = $schedule->patient_id;
                            $data['date'] = $schedule->schedule_date;
                            $data['user_id'] = \Helper::getUserID();
                            $data['schedule_id'] = $schedule->id;
                            $data['amount'] = $schedule->amount;
                            $data['comment'] = $request->cancelComment;
                            $data['type'] = 'Schedule';

                            $creditMemo = CreditMemo::whereScheduleId($schedule->id)->first();
                            if(!$creditMemo){
                                $creditInit = \Helper::getSetting('credit_inits');
                                $creditStart = \Helper::getSetting('credit_start_no');
                                $creditCount = CreditMemo::count() + 1;

                                $data['credit_memo_no'] = $creditInit.''.($creditStart+$creditCount);

                                CreditMemo::create($data);
                            }
                        }
                    }
                }

                if(count($schedulesComplete) || count($schedulesCancel)){
                    $service->update(['status' => 'Completed']);
                    $status = 'Completed';
                }
            }

            if($status == 'Cancelled'){
                $schedules = Schedule::whereLeadId($leadID)->whereStatus('Pending')->whereServiceRequestId($id)->get();
                if(count($schedules)){
                    foreach ($schedules as $schedule) {
                        $schedule->update(['status' => 'Cancelled','cancel_reason' => $request->cancelType,'cancel_comment' => $request->cancelComment]);
                        $invoiceitem = $schedule->invoice;
                        if(count($invoiceitem)){
                            $data['patient_id'] = $schedule->patient_id;
                            $data['date'] = $schedule->schedule_date;
                            $data['user_id'] = \Helper::getUserID();
                            $data['schedule_id'] = $schedule->id;
                            $data['amount'] = $schedule->amount;
                            $data['comment'] = $request->cancelComment;
                            $data['type'] = 'Schedule';

                            $creditMemo = CreditMemo::whereScheduleId($schedule->id)->first();
                            if(!$creditMemo){
                                $creditInit = \Helper::getSetting('credit_inits');
                                $creditStart = \Helper::getSetting('credit_start_no');
                                $creditCount = CreditMemo::count() + 1;

                                $data['credit_memo_no'] = $creditInit.''.($creditStart+$creditCount);

                                CreditMemo::create($data);
                            }
                        }
                    }
                }
            }

            $schedulesDiff = Schedule::whereLeadId($leadID)->whereServiceRequestId($id)->whereStatus('Pending')->count();

            if($schedulesDiff == '0'){
                $serviceStatus = $status;
            }else{
                $serviceStatus = 'Pending';
            }

            if($status == 'Cancelled'){
                $res = $service->update(['status' => $serviceStatus,'cancel_reason' => $request->cancelType,'cancel_comment' => $request->cancelComment]);

                // Update Service Order status to Cancelled
                $serviceOrders = CaseServiceOrder::whereLeadId($service->lead_id)->wherePatientId($service->patient_id)
                                    ->whereServiceRequestId($service->id)->wherePaymentStatus('Pending')->get();
                if(count($serviceOrders)){
                    foreach ($serviceOrders as $so) {
                        $so->update(['payment_status' => 'Cancelled']);
                    }
                }
                return back()->with('alert_success','Status Changed Successfully');
            }else{
                $res = $service->update(['status' => $serviceStatus]);
            }
        }

        return response()->json(['result' => (bool) $res,'schedulesDiff' => $schedulesDiff],200);
    }

    public function updateAssessmentStatus(Request $request)
    {

        $id = Helper::encryptor('decrypt',$request->id);
        $leadID = Helper::encryptor('decrypt',$request->lead_id);
        $status = $request->status;
        $result = false;

        if($id && $status){
            $assessment = CaseForm::find($id);
            if(!$assessment){
                return response()->json(['result' => false],200);
            }else{
                $assessment->update(['status' => $status]);
                return response()->json(['result' => true],200);
            }
        }

        return response()->json(['result' => (bool) $res],200);
    }

    public function updateAssessmentDoneStatus(Request $request)
    {

        $leadID = $request->lead;
        $assessmentStatus = $request->value;
        $result = false;

        if($leadID && $assessmentStatus){
            $lead = Lead::whereId($leadID)->first();
            if(!$lead){
                return response()->json(['result' => false],200);
            }else{
                $lead->update(['assessment_done' => $assessmentStatus]);
                return response()->json(['result' => true],200);
            }
        }

        return response()->json(['result' => (bool) $res],200);
    }

    public function completeSchedule(Request $request){
       $id = Helper::encryptor('decrypt',$request->id);
       $status = $request->status;
       $res = false;
       if($id && $status){
         $schedule = Schedule::find($id);
         if(!$schedule || $schedule->caregiver_id == 0 || $schedule->caregiver_id == null){
             return response()->json(['result' => false],200);
         }
         $res = $schedule->update(['status' => $status]);

         if($status == 'Completed' && $schedule->caregiver_id != 0 && $schedule->caregiver_id != null){
             $schedule = Schedule::find($id);
             if(!$schedule){
                 return response()->json(['result' => false],200);
             }
             //Mark Attendance
             $att = Attendance::whereDate('date',$schedule->schedule_date)->whereCaregiverId($schedule->caregiver_id)->whereScheduleId($schedule->id)->first();
             if(!$att){
                 $att_data = [
                     'caregiver_id' => (int) $schedule->caregiver_id,
                     'schedule_id' => (int) $schedule->id,
                     'date' => $schedule->schedule_date->format('Y-m-d'),
                     'punch_in' => isset($schedule->start_time)?Carbon::parse($schedule->start_time)->format('H:i:s'):null,
                     'punch_out' => isset($schedule->end_time)?Carbon::parse($schedule->end_time)->format('H:i:s'):null,
                     'source' => 'Web',
                     'marked_by' => (int) Helper::encryptor('decrypt',session('user_id')),
                     'status' => 'P',
                 ];
                 Attendance::create($att_data);
             }

             $attBench = Attendance::whereDate('date',$schedule->schedule_date)->whereCaregiverId($schedule->caregiver_id)->whereNull('schedule_id')->get();

             if(count($attBench)){
                 foreach ($attBench as $ab) {
                     $ab->delete();
                 }
             }

             $res = true;
         }

         $drs = DailyRevenueStorage::whereDate('date',$schedule->schedule_date)->first();

         if($drs){
            $drs->flag = 'Unsorted';
            $drs->updated_at = date('Y-m-d H:i:s');

            $drs->save();
         }
       }

       return response()->json(['result' => (bool) $res],200);
    }
    
    public function cancelSchedules(Request $request)
    {
        if(count($request->schedules) > 0 && $request->cancelType != '')
        {
            foreach($request->schedules as $schedule)
            {
                $schedule = Schedule::whereId(\Helper::encryptor('decrypt',$schedule))->first();
                if($schedule->status == 'Pending')
                {
                    $schedule->update(['status' => 'Cancelled','cancel_reason' => $request->cancelType,'cancel_comment' => $request->cancelScheduleComment]);
                    //check for any proforma invoice is raised or not when th cancellation is done
                    $invoiceitem = $schedule->invoice;
                    if(count($invoiceitem)){
                        $taxRate = $schedule->ratecard->tax->tax_rate;
                        $finalAmount = $schedule->amount + ($schedule->amount)*(($taxRate)/100);
                        $data['patient_id'] = $schedule->patient_id;
                        $data['date'] = $schedule->schedule_date;
                        $data['user_id'] = \Helper::getUserID();
                        $data['schedule_id'] = $schedule->id;
                        $data['amount'] = $finalAmount;
                        $data['comment'] = $request->cancelScheduleComment;
                        $data['type'] = 'Schedule';
                        //Pass a credit memo to the patient for the schedule amount if invoice is already raised
                        $creditMemo = CreditMemo::whereScheduleId($schedule->id)->first();
                        if(!$creditMemo){
                            $creditInit = \Helper::getSetting('credit_inits');
                            $creditStart = \Helper::getSetting('credit_start_no');
                            $creditCount = CreditMemo::count() + 1;

                            $data['credit_memo_no'] = $creditInit.''.($creditStart+$creditCount);
                            
                            CreditMemo::create($data);
                        }
                    }

                    if(count($request->schedules) == 1)
                    {
                     $categoryService = isset($schedule->service->category)?$schedule->service->category->category_name:null;

                     $checkIVRSTenant = \Helper::getTenantId() == 266;
                     $checkIVRSCategory = $categoryService == 'Visit Based';

                         if($checkIVRSTenant && $checkIVRSCategory)
                        {
                            $type = $request->ivrs_type;
                            \App\Helpers\IVRS::visitBasedCancellation($type,$schedule);
                        }
                    }
                }
            }

            // return response()->json(200);
            return json_encode(200);
        }
    }
    
    public function unconfirmSchedules(Request $request)
    {
        if(count($request->schedules) > 0)
        {
            foreach($request->schedules as $schedule)
            {
                $caseSchedule = Schedule::whereId(\Helper::encryptor('decrypt',$schedule))->first();
                if($caseSchedule->status == 'Completed')
                {
                    $caseSchedule->update(['status' => 'Pending']);
                    $att = Attendance::whereScheduleId(\Helper::encryptor('decrypt',$schedule))->first();
                    if($att){
                        $att->delete();
                    }

                    $drs = DailyRevenueStorage::whereDate('date',$caseSchedule->schedule_date)->first();

                    if($drs){
                       $drs->flag = 'Unsorted';
                       $drs->updated_at = date('Y-m-d H:i:s');

                       $drs->save();
                    }

                }
            }

            return response()->json(200);
        }
    }

    public function updateScheduleChargeable(Request $request)
    {
        $id = Helper::encryptor('decrypt',$request->id);
        $value = $request->value;
        $res = false;

        if($id && isset($value)){
            $schedule = Schedule::find($id);
            if(!$schedule){
                return response()->json(['result' => false],200);
            }
            $res = $schedule->update(['chargeable' => $value]);

            $drs = DailyRevenueStorage::whereDate('date',$schedule->schedule_date)->first();

            if($drs){
               $drs->flag = 'Unsorted';
               $drs->updated_at = date('Y-m-d H:i:s');

               $drs->save();
            }
            
            if($value == '0' && count($schedule->invoice)){
                $creditMemo = CreditMemo::whereScheduleId($id)->first();
                if(!$creditMemo){
                    $taxRate = $schedule->service->ratecard->tax->tax_rate;
                    $finalAmount = $schedule->amount + ($schedule->amount)*(($taxRate)/100);
                    
                    $data['patient_id']  = $schedule->lead->patient_id;
                    $data['date']        = $schedule->schedule_date;
                    $data['user_id']     = \Helper::getUserID();
                    $data['schedule_id'] = $schedule->id;
                    $data['amount']      = $finalAmount;
                    $data['comment']     = 'Non Chargeable Item';
                    $data['type']        = 'Schedule';
                    
                    $creditInit  = \Helper::getSetting('credit_inits');
                    $creditStart = \Helper::getSetting('credit_start_no');
                    $creditCount = CreditMemo::count() + 1;

                    $data['credit_memo_no'] = $creditInit.''.($creditStart+$creditCount);

                    CreditMemo::create($data);
                }
            }
        }

        return response()->json(['result' => (bool) $res],200);
    }

    public function updateScheduleTasks(Request $request)
    {
        $data['task_date'] = date_format(new \DateTime($request->task_date),'Y-m-d');
        $data['task_to_date'] = !empty($request->task_to_date)?date_format(new \DateTime($request->task_to_date),'Y-m-d'):$data['task_date'];
        $serviceID = \Helper::encryptor('decrypt',$request->service_id);
        $schedules = Schedule::whereServiceRequestId($serviceID)->whereDate('schedule_date','>=',$data['task_date'])->whereDate('schedule_date','<=',$data['task_to_date'])->get();
        foreach ($schedules as  $s) {
                $s->assigned_tasks = $request->assigned_tasks;
                $s->save();
            }

        return back()->with('alert_success','Task assignment updated successfully'); //return response()->json($res, 200);
    }

    public function saveServiceOrder(Request $request)
    {
        $sessionDates = [];
        $id = $request->id;
        $leadID = Helper::encryptor('decrypt',$request->lead_id);
        $patientID = Helper::encryptor('decrypt',$request->patient_id);
        $service = explode("_",$request->order_service_id)[1];
        if($request->mdatesList != ''){
            $mdates = $request->mdatesList;
            $mdates = array_map('trim', explode(',', $mdates));
            foreach ($mdates as $m) {
                $sessionDates[] = date_format(new \DateTime($m), 'Y-m-d');
            }
            $sessionDates = $request->mdatesList;
        }else{
            $fromDate = $request->order_from_date;
            $toDate = $request->order_to_date;
        }

        $rate = $request->order_rate;
        $ratecard_rate = $request->ratecard_rate;
        $status = $request->service_order_status;

        if($leadID && $patientID && $service){
            // Check if service order exists for same service, then disable it
            $prevOrders = CaseServiceOrder::whereLeadId($leadID)->whereServiceId($service)->get();
            if($prevOrders){
                foreach ($prevOrders as $po) {
                    $po->update(['status' => 0]);
                }
            }

            $data = [];
            if(empty($sessionDates)){
                if(!empty($fromDate)){
                    $fromDate = date_format(new \DateTime($fromDate), 'Y-m-d');
                }
                if(!empty($toDate)){
                    $toDate = date_format(new \DateTime($toDate), 'Y-m-d');
                }else
                $toDate = null;

                $data = [
                    'user_id' => Helper::getUserID(),
                    'lead_id' => $leadID,
                    'patient_id' => $patientID,
                    'service_id' => $service,
                    'from_date' => $fromDate,
                    'to_date' => $toDate,
                    'rate' => $rate,
                    'ratecard_rate' =>$ratecard_rate,
                    'status' => $status,
                ];
            }else{
                $data = [
                    'user_id' => Helper::getUserID(),
                    'lead_id' => $leadID,
                    'patient_id' => $patientID,
                    'service_id' => $service,
                    'session_dates' => $sessionDates,
                    'rate' => $rate,
                    'ratecard_rate' =>$ratecard_rate,
                    'status' => $status,
                ];
            }


            if($id == 0)
                $res = CaseServiceOrder::create($data);
            else
                $res = CaseServiceOrder::find($id)->update($data);

            return back()->with('alert_success','Service order saved successfully');
        }

        return back();
    }

    public function saveComment(Request $request)
    {
        $res = false;
        $leadID = Helper::encryptor('decrypt',$request->lead_id);
        $comment = $request->comment;

        if($leadID && $comment){
            $data = [
            'lead_id' => $leadID,
            'user_id' => Helper::getUserID(),
            'user_type' => 'User',
            'user_name' => \Auth::user()->full_name,
            'comment' => $comment
            ];

            $res = Comment::create($data);
        }

        return back()->with('alert_success','Comment added successfully');
    }

    public function updateDisposition(Request $request)
    {
        $res = false;

        if($request->ajax() && $request->id){
            $id = Helper::encryptor('decrypt',$request->id);

            $lead = Lead::find($id);
            $patient = Patient::whereId($lead->patient_id)->get();

            if($lead){
                // Update leads table
                $leadData['status'] = $request->status;

                $date = null;
                if($request->date){
                    $date = date_format(new \DateTime($request->date),'Y-m-d');
                }

                if($request->status == 'Dropped'){
                    $leadData['dropped_reason'] = $caselosscomment = $request->caselosscomment;
                }else{
                    $caselosscomment = '-';
                }

                $lead->update($leadData);

                // Add to case disposition table
                $dispositionData = [
                    'lead_id'          => $id,
                    'user_id'          => Helper::getUserID(),
                    'status'           => $request->status,
                    'disposition_date' => $date,
                    'comment'          => $request->comment,
                    'caselosscomment'  => $caselosscomment
                ];

                if(!empty($request->followupdate)){
                    $dispositionData += [
                        'follow_up_datetime' => date_format(new \DateTime($request->followupdate),'Y-m-d H:i:s'),
                        'follow_up_comment'  => $request->followupcomment,
                    ];
                }

                if($request->status == "Converted"){
                    $check = CaseDisposition::whereLeadId($id)->whereStatus('Converted')->first();
                    if(!$check){
                        $dispositionData += [
                            'converted' => 1,
                        ];
                    }
                }

                $res = CaseDisposition::create($dispositionData);

                if($patient){
                    $lead = Lead::whereId($id)->first();
                    $patient = Patient::whereId($lead->patient_id)->first();

                    if(empty($patient->patient_id) && $request->status == 'Converted'){
                        $patientDetails = [];

                        $prefix = Branch::whereId($lead->branch_id)->pluck('patient_id_prefix')->first();
                        if($prefix){
                            $lastPatient = Patient::whereNotNull('patient_id')->whereBranchId($lead->branch_id)->orderBy(\DB::raw('length(patient_id),patient_id'))->get()->last();
                            if($lastPatient){
                                $lastID = substr($lastPatient->patient_id, strlen($prefix));
                            }else{
                                $lastID = Branch::whereId($lead->branch_id)->pluck('patient_id_start_no')->first();
                            }

                            $patientDetails['patient_id'] = $prefix.str_pad(intval($lastID + 1),strlen($lastID),0,STR_PAD_LEFT);
                        }
                        $patient->update($patientDetails);
                        $patientID = $patient->id;
                    }
                    if(empty($lead->episode_id) && $request->status == 'Converted'){
                        $caseDetails = [];
                        $caseDetails['episode_id'] = $patient->patient_id.str_pad(Lead::wherePatientId($patient->id)->count(),2,0,STR_PAD_LEFT);
                        $lead->update($caseDetails);
                    }
                    if($lead->aggregator_lead && $request->status == 'Dropped'){
                        AggregatorLeads::whereLeadId($lead->id)->update(['status' => 'Dropped']);
                    }
                    if($patient->email != null && $request->status == 'Converted' && filter_var($patient->email, FILTER_VALIDATE_EMAIL)){
                        $mail = Mail::to($patient->email);
                        $mail->send(new LeadConvertion($lead,$patient));
                    }
                    if($patient->email != null && $request->status == 'Closed' && filter_var($patient->email, FILTER_VALIDATE_EMAIL)){
                        $mail = Mail::to($patient->email);
                        $mail->send(new LeadCompletion($lead,$patient));
                    }
                    if($patient->email != null && $request->status == 'Dropped' && filter_var($patient->email, FILTER_VALIDATE_EMAIL)){
                        $mail = Mail::to($patient->email);
                        $mail->send(new LeadLoss($lead,$patient));
                    }
                }
            }
        }

        return response()->json((bool) $res,200);
    }

    public function saveCaseRateCard(Request $request)
    {
        $res = [];
        $data = $request->only('lead_id','service_id','rate','discount_type','discount_value','period_from','period_to','total_amount');
        if($data){
            $data['lead_id'] = Helper::encryptor('decrypt',$data['lead_id']);
            $data['status'] = 1;
            // Create date object from start and end date
            if(!empty($data['period_from']) && !empty($data['period_to'])){
                $data['period_from'] = date_format(new \DateTime($data['period_from']),'Y-m-d');
                $data['period_to'] = date_format(new \DateTime($data['period_to']),'Y-m-d');
            }else{
                $data['period_from'] = null;
                $data['period_to'] = null;
            }

            // Inactive all ratecards
            $cr = CaseRatecard::whereLeadId($data['lead_id'])->whereServiceId($data['service_id'])->first();
            if($cr){
                $cr->update(['status' => 0]);
            }
            // Add the new ratecard
            if($data['discount_value'] > 0)
                $data['discount_applicable'] = 'Y';
            $res = CaseRatecard::firstOrCreate($data);
        }

        return response()->json($res->load('service'), 200);
    }

    public function deleteCaseRateCard($id)
    {
        $id = Helper::encryptor('decrypt',$id);
        if($id){
            CaseRatecard::find($id)->delete();

            return back()->with('alert_info','Rate Card deleted successfully.');
        }

        return back();
    }

    public function saveSchedule(Request $request)
    {
        $sessionDates = [];
        $data = $request->only('service_order_id','start_date','end_date','start_time','end_time','shift','service_required','caregiver_id','notes','chargeable','status','session_dates');

        $id = intval($request->id);
        $leadID = Helper::encryptor('decrypt',$request->lead_id);
        $patientID = Helper::encryptor('decrypt',$request->patient_id);

        if($leadID && $patientID){
            $data['lead_id'] = $leadID;
            $data['patient_id'] = $patientID;

            if($id == 0){
                if(!empty($data['session_dates'])){
                // Insert Multiple Entries logic for Interrupting Services
                    $mdates = array_map('trim', explode(',', $data['session_dates']));
                    foreach ($mdates as $m) {
                        $sessionDates[] = date_format(new \DateTime($m), 'Y-m-d');
                    }
                    foreach ($sessionDates as $date) {
                        $data['schedule_date'] = $date;

                        $res = Schedule::create($data);
                        if($request->caregiver_id){
                            $c = Caregiver::whereId($request->caregiver_id)->first();
                            $c->work_status = "On Duty";

                            $c->save();
                        }
                    }
                }else{
                // Insert Multiple Entries logic for Uninterrupting Services
                    $begin = new \DateTime($data['start_date']);
                    if(!empty($data['end_date'])){
                        $end = new \DateTime($data['end_date']);
                    }else{
                        $end = new \DateTime($data['start_date']);
                    }
                    $end = $end->modify( '+1 day' );

                    $interval = new \DateInterval('P1D');
                    $daterange = new \DatePeriod($begin, $interval ,$end);

                    $schedules = [];
                    foreach($daterange as $date){
                        $data['schedule_date'] = $date->format("Y-m-d");
                        $schedules[] = $data;

                        $res = Schedule::create($data);
                        if($request->caregiver_id){
                            $c = Caregiver::whereId($request->caregiver_id)->first();
                            $c->work_status = "On Duty";

                            $c->save();
                        }
                    }
                }
            }else{
                unset($data['start_date']);
                unset($data['end_date']);
                unset($data['session_dates']);

                $res = Schedule::whereId($id)->update($data);

                if($request->caregiver_id){
                    if($request->status == 'Completed'){
                        $c = Caregiver::whereId($request->caregiver_id)->first();
                        $c->work_status = "Available";

                        $c->save();

                        // Mark Attendance for particular staff on schedule date
                        $sDate = Schedule::whereId($id)->first()->schedule_date;
                        $att_data = [
                            'caregiver_id' => (int) $request->caregiver_id,
                            'punch_in' => $sDate,
                            'punch_in_server_timestamp' => $sDate,
                            'source' => 'Web',
                            'marked_by' => (int) Helper::encryptor('decrypt',session('user_id')),
                            'status' => 'P'
                        ];

                        $att = Attendance::whereDate('punch_in','=',$sDate)
                                            ->whereCaregiverId($request->caregiver_id)
                                            ->first();

                        if($att){
                            $att->update($att_data);
                        }else{
                            $res = Attendance::create($att_data);
                        }
                    }
                }
            }

            return back()->with('alert_success','Schedule created successfully');
        }

        return back()->with('alert_error','Unable to create Schedule!');
    }

    public function getSchedule(Request $request)
    {
        $s = Schedule::find($request->id);
        if($s){
            $data['schedule'] = $s;
            $data['caregiver'] = Caregiver::whereId($s->caregiver_id)->first();
            $data['ratecards'] = CaseRatecard::whereLeadId($s->lead_id)->get();

            return response()->json($data, 200);
        }

        return response()->json(false, 401);
    }

    public function getServiceInterruption(Request $request)
    {
        $interruptions = ServiceInterruption::with('user')->with('audits')->whereLeadId(Helper::encryptor('decrypt',$request->lead_id))
        ->whereScheduleId($request->schedule_id)->orderBy('id','Desc')->get();

        return response()->json($interruptions, 200);
    }

    public function saveServiceInterruption(Request $request)
    {
        $data = $request->only('from_date','to_date','reason');

        $leadID = Helper::encryptor('decrypt',$request->lead_id);
        $scheduleID = $request->schedule_id;

        if($leadID && $scheduleID){
            $data['lead_id'] = $leadID;
            $data['schedule_id'] = $scheduleID;
            $data['user_id'] = Helper::encryptor('decrypt',session('user_id'));

            // Create date object from from_date and to_date
            $data['from_date'] = date_format(new \DateTime($data['from_date']),'Y-m-d');
            $data['to_date'] = date_format(new \DateTime($data['to_date']),'Y-m-d');

            $res = ServiceInterruption::create($data);

            return response()->json(['res' => true, 'message' => 'Service Interruption log added successfully'], 200);
        }

        return response()->json(['res' => false, 'message' => 'Unable to add service interruption log !'], 401);
    }

    public function uploadCaseDocument(Request $request)
    {
        if($request->hasFile('case_document')){
            $documentName = $request->document_name;

            $file = $request->file('case_document');
            if($file->isValid()){
                $filePath = public_path().'/uploads/provider/'.session('tenant_id').'/'.'case_documents/'.$request->lead_id;
                if(!is_dir($filePath)){
                    \File::makeDirectory($filePath, 0777, true);
                }

                $data = [
                    'lead_id' => Helper::encryptor('decrypt',$request->lead_id),
                    'document_name' => $request->document_name,
                    'service_request_id' => $request->service_request_id,
                    'user_id' => Helper::encryptor('decrypt',session('user_id')),
                    'created_at' => date('Y-m-d H:i:s')
                ];
                $caseDocumentID = CaseDocument::insertGetId($data);
                if($caseDocumentID){
                    $fileName = Helper::encryptor('encrypt',$caseDocumentID).'.'.$file->extension();

                    $path = $file->move($filePath, $fileName);

                    $res = CaseDocument::find($caseDocumentID)->update(['document_path' => asset('uploads/provider/'.session('tenant_id').'/'.'case_documents/'.$request->lead_id.'/'.$fileName)]);

                    return back()->with('alert_success','Document Updated Successfully');
                }
            }
        }

        return back();
    }

    public function removeCaseDocument(Request $request)
    {
        if($request->id){
            $caseDocument = CaseDocument::find(Helper::encryptor('decrypt',$request->id));
            if($caseDocument){
                $docPath = $caseDocument->document_path;
                $arr = explode("/",$docPath);
                $documentName = end($arr);

                // Remove Document
                if($documentName){
                    $path = public_path().'uploads/case_documents/';
                    if(file_exists($path.$documentName)){
                        unlink($path.$documentName);
                    }
                }

                // Remove entry from case_documents table
                $caseDocument->forceDelete();

                return back()->with('alert_info','Document removed successfully');
            }
        }

        return back();
    }

    public function saveWorklogByDate(Request $request)
    {
        $data = $request->only('caregiver_id','lead_id','schedule_id','worklog_date','vitals','routines');

        $data['tenant_id'] = \Helper::getTenantID();
        $data['lead_id'] = !empty($data['lead_id'])?\Helper::encryptor('decrypt',$data['lead_id']):null;
        $data['schedule_id'] = !empty($data['schedule_id'])?\Helper::encryptor('decrypt',$data['schedule_id']):null;
        $data['caregiver_id'] = !empty($data['caregiver_id'])?\Helper::encryptor('decrypt',$data['caregiver_id']):null;
        $data['worklog_date'] = date_format(new \DateTime($data['worklog_date']),'d-m-Y');
        $data['user_id'] = \Helper::getUserID();

        $worklog = \App\Entities\WorkLog::whereWorklogDate($data['worklog_date'])->first();
        if($worklog){
            $worklog->update($data);
        }else{
            \App\Entities\Worklog::create($data);
        }

        return response()->json(true, 200);
    }

    public function importLead()
    {
        return view('leads.import');
    }

    public function bulkImportLeads(Request $request)
    {
        $count = 0;
        $file = $request->file('excel_file');

        $path = $file->getRealPath();

        $reader = \Excel::selectSheetsByIndex(0)->load($path, function($reader) {});

        $data = $reader->get();

        if(!empty($data) && $data->count())
        {
            $data = $data->toArray();
            $count = count($data);
            foreach ($data as $key => $value)
            {
                if(count($value))
                {
                    //creation of patient
                    if(isset($value['first_name']) && !empty($value['first_name']) && $value['first_name'] != '')
                    {
                        $patient = new Patient;
                        $patient->tenant_id = Helper::getTenantID();
                        // Generate Unique Patient ID
                        $prefixpp = Settings::value('patient_id_prefix');

                        if($prefixpp){
                            $lastPatientId = Patient::distinct('patient_id')->whereNotNull('patient_id')->orderBy(\DB::raw('LENGTH(`patient_id`)'),'Desc')->orderBy('patient_id','Desc')->first();
                            if(!$lastPatientId){
                                $lastID = 0;
                            }else{
                                $lastID = substr($lastPatientId->patient_id, strlen($prefixpp));
                            }
                            if($value['status'] == 'Converted')
                                $patient->patient_id = $prefixpp.str_pad(intval($lastID + 1),strlen($lastID),0,STR_PAD_LEFT);
                            else
                                $patient->patient_id = null;
                        }

                        if(isset($value['date_of_birth']) && !empty($value['date_of_birth']))
                            $dob = date_format(new \DateTime($value['date_of_birth']),'Y-m-d');
                        else
                            $dob = null;

                        $patient->first_name = $value['first_name'];
                        $patient->last_name = $value['last_name'];
                        $patient->contact_number = sprintf("%.0f", $value['contact_number']);
                        $patient->gender = $value['gender'];
                        $patient->date_of_birth = $dob;
                        $patient->patient_age = sprintf("%.0f", $value['patient_age']);
                        $patient->patient_weight = sprintf("%.0f", $value['patient_weight']);
                        $patient->street_address = $value['street_address'];
                        $patient->area = $value['area'];
                        $patient->city = $value['city'];
                        $patient->zipcode = sprintf("%.0f", $value['zipcode']);
                        $patient->state = $value['state'];
                        $patient->country = $value['country'];
                        $patient->enquirer_name = $value['enquirer_name'];
                        $patient->alternate_number = sprintf("%.0f", $value['alternate_number']);
                        $patient->email = $value['email'];
                        $patient->relationship_with_patient = $value['relationship_with_patient'];
                        $patient->save();

                        //creation of case
                        if($patient->id){
                            $lead = new Lead;
                            // Generate Unique Episode ID if the lead is converted
                            if($value['status'] == 'Converted'){
                                $lead->episode_id = Patient::find($patient->id)->patient_id.str_pad(Lead::wherePatientId($patient->id)->count() + 1,2,0,STR_PAD_LEFT);
                            }else{
                                $lead->episode_id = null;
                            }

                            $lead->tenant_id = Helper::getTenantID();
                            $lead->patient_id = $patient->id;
                            $lead->case_description = $value['case_description'];
                            $lead->medical_conditions = $value['medical_conditions'];
                            $lead->hospital_name = $value['hospital_name'];
                            $lead->primary_doctor_name = $value['primary_doctor_name'];
                            $lead->special_instructions = $value['special_instructions'];
                            $lead->language_preference = $value['language_preference'];
                            $lead->gender_preference = $value['gender_preference'];
                            $lead->assessment_notes = $value['assessment_notes'];
                            $lead->rate_agreed = $value['rate_agreed'];
                            $lead->registration_amount = $value['registration_amount'];
                            $lead->payment_mode = $value['payment_mode'];
                            $lead->payment_notes = $value['payment_notes'];
                            $lead->referrer_name = $value['referrer_name'];
                            $lead->converted_by = $value['converted_by'];
                            $lead->status = $value['status'];
                            $lead->dropped_reason_other = $value['dropped_reason_other'];
                            // $lead->no_of_hours = sprintf("%.0f", $value['no_of_hours']);

                            $lead->save();
                        }
                    }
                }
            }
        }
        return back()->with('alert_success',$count.' Lead(s) has been successfully imported.');
    }

    public function saveForm(Request $request)
    {
        $form_array=[];
        $assessment_status = $request['assessment_status'];
        $request = $request->except('assessment_status');

        if($request['form_type'] == 'nursing')
        $form_array = $this->saveNursingAssessment($request);
        elseif($request['form_type'] == 'physiotherapy')
        $form_array = $this->savePhysioAssessment($request);
        elseif($request['form_type'] == 'labtest')
        $form_array = $this->saveLabTest($request);
        else{
        session()->flash('alert_warning', 'Please Select the type of Form.');
        return redirect()->back();
        }

        $form_data=json_encode($form_array);
        if(isset($request['case_forms_id'])){
            CaseForm::find($request['case_forms_id'])->update(['form_data' => $form_data,'status' => $assessment_status]);
            session()->flash('alert_success', 'Assessment Updated Successfully.');
        } else {
            $assessment = new CaseForm;
            $assessment->tenant_id = Helper::getTenantID();
            $assessment->lead_id = Helper::encryptor('decrypt',$request['lead_id']);
            $assessment->user_id = $request['user_id'];
            $assessment->form_data = $form_data;
            $assessment->status = $assessment_status;
            $assessment->save();
            session()->flash('alert_success', 'Assessment Added Successfully.');
        }
        return redirect()->back();
    }

    public function saveFollowUpForm(Request $request)
    {
        $form_array=[];
        $request = $request->except('assessment_status');
        $request['followupManager'] = empty($request['followup_manager'])?$request['followup_manager_view_only']:$request['followup_manager'];
        $form_array = $this->saveCaseFollowUp($request);
        $followup_date = empty($request['followup_date'])?date('Y-m-d'):date_format(new \DateTime($request['followup_date']),'Y-m-d');

        $form_data=json_encode($form_array);
        if(!empty($request['followup_id'])){
            CaseFollowUp::find($request['followup_id'])->update(['form_data' => $form_data,'followup_date' => $followup_date]);
            session()->flash('alert_success', 'Follow-Up Updated Successfully.');
        } else {
            $assessment = new CaseFollowUp;
            $assessment->tenant_id = Helper::getTenantID();
            $assessment->lead_id = Helper::encryptor('decrypt',$request['lead_id']);
            $assessment->user_id = \Helper::getUserID();
            $assessment->followup_date = $followup_date;
            $assessment->form_data = $form_data;
            $assessment->save();
            session()->flash('alert_success', 'Follow-Up Added Successfully.');
        }
        return redirect()->back();
    }

    //Saving Assessments based on specialization
    public function saveNursingAssessment($request)
    {
        $form_array=[];
        $form_array['assessment_date']        = isset($request['assessment_date_nursing'])?$request['assessment_date_nursing']:'';
        $form_array['assessment_method']      = isset($request['assessment_method'])?$request['assessment_method']:'';
        $form_array['assessor_name']          = isset($request['schedule_details'])?$request['schedule_details']:'';
        $form_array['patient_mobility']       = isset($request['patient_mobility'])?$request['patient_mobility']:'';
        $form_array['patient_consciousness']  = isset($request['patient_consciousness'])?$request['patient_consciousness']:'';
        $form_array['medical_procedures']     = isset($request['medical_procedures'])?$request['medical_procedures']:'';
        $form_array['suggested_professional'] = isset($request['suggested_home_care_professional'])?$request['suggested_home_care_professional']:'';
        $form_array['comment']                = isset($request['notes'])?$request['notes']:'';

        return $form_array;
    }

    public function saveCaseFollowUp($request)
    {
        $form_array=[];
        $form_array['followup_manager'] = $request['followupManager'];
        $form_array['nails_clean_and_short'] = isset($request['nails_clean_and_short'])?$request['nails_clean_and_short']:'';
        $form_array['wearing_hair_band'] = isset($request['wearing_hair_band'])?$request['wearing_hair_band']:'';
        $form_array['clean_cloth'] = isset($request['clean_cloth'])?$request['clean_cloth']:'';
        $form_array['odor'] = isset($request['odor'])?$request['odor']:'';
        $form_array['toilet_usage_and_sanitary_mgmt'] = isset($request['toilet_usage_and_sanitary_mgmt'])?$request['toilet_usage_and_sanitary_mgmt']:'';
        $form_array['bed_making_done_properly'] = isset($request['bed_making_done_properly'])?$request['bed_making_done_properly']:'';
        $form_array['room_hygiene_maintained'] = isset($request['room_hygiene_maintained'])?$request['room_hygiene_maintained']:'';
        $form_array['oral_care_done_properly'] = isset($request['oral_care_done_properly'])?$request['oral_care_done_properly']:'';
        $form_array['back_care_done_properly'] = isset($request['back_care_done_properly'])?$request['back_care_done_properly']:'';
        $form_array['bed_sores_monitoring_or_check_observation'] = isset($request['bed_sores_monitoring_or_check_observation'])?$request['bed_sores_monitoring_or_check_observation']:'';
        $form_array['catheter_care_done_properly'] = isset($request['catheter_care_done_properly'])?$request['catheter_care_done_properly']:'';
        $form_array['rt_and_peg_care_done_properly'] = isset($request['rt_and_peg_care_done_properly'])?$request['rt_and_peg_care_done_properly']:'';
        $form_array['hair_wash_done_properly'] = isset($request['hair_wash_done_properly'])?$request['hair_wash_done_properly']:'';
        $form_array['nail_care_observation'] = isset($request['nail_care_observation'])?$request['nail_care_observation']:'';
        $form_array['bp_and_temperature_checking_done_properly'] = isset($request['bp_and_temperature_checking_done_properly'])?$request['bp_and_temperature_checking_done_properly']:'';
        $form_array['exercise_done_properly'] = isset($request['exercise_done_properly'])?$request['exercise_done_properly']:'';
        $form_array['patient_work_schedule_checks'] = isset($request['patient_work_schedule_checks'])?$request['patient_work_schedule_checks']:'';
        $form_array['patient_hand_over_to_next_shift_person_done'] = isset($request['patient_hand_over_to_next_shift_person_done'])?$request['patient_hand_over_to_next_shift_person_done']:'';
        $form_array['end_of_day_patient_feedback_to_client'] = isset($request['end_of_day_patient_feedback_to_client'])?$request['end_of_day_patient_feedback_to_client']:'';
        $form_array['miss_call_in_time_and_out_time_done_by_staff_and_client'] = isset($request['miss_call_in_time_and_out_time_done_by_staff_and_client'])?$request['miss_call_in_time_and_out_time_done_by_staff_and_client']:'';
        $form_array['followup_comments'] = isset($request['followup_comments'])?$request['followup_comments']:'';

        return $form_array;
    }

    public function savePhysioAssessment($request)
    {
        $form_array=[];
        $form_array['assessment_date']    = $request->assessment_date;
        $form_array['assessor_name']      = $request->assessor_name;
        $form_array['chief_complaints']   = $request->chief_complaints;
        $form_array['sites']              = $request->sites;
        $form_array['sides']              = $request->sides;
        $form_array['duration']           = $request->duration;
        $form_array['aggrevating_factors']= $request->aggrevating_factors;
        $form_array['relieving_factors']  = $request->relieving_factors;
        $form_array['assessform']         = $request->assessform;

        //check whether a schedule is selected
        if($request->schedule_details == ""){
            session()->flash('alert_error', 'Please Select a Person and the Assessment Form');
            return redirect()->back();
        }
        //check for proper form data inputs
        if($form_array['assessform'] == "ortho")
        {
            $form_array['rom']                = $request->rom;
            $form_array['gait']               = $request->gait;
            $form_array['posture']            = $request->posture;
            $form_array['deformity']          = $request->deformity;
            $form_array['ortho_tone']         = $request->ortho_tone;
            $form_array['swelling']           = $request->swelling;
        }
        //check for proper form data inputs
        if($form_array['assessform'] == "neuro")
        {
            $form_array['loc']                = $request->loc;
            $form_array['orientation']        = $request->orientation;
            $form_array['comprehension']      = $request->comprehension;
            $form_array['memory']             = $request->memory;
            $form_array['cne']                = $request->cne;
            $form_array['pressure_sore']      = $request->pressure_sore;
            $form_array['balance']            = $request->balance;
            $form_array['speech']             = $request->speech;
            $form_array['neuro_tone']         = $request->neuro_tone;
        }

        $form_array['treatment_plan']     = $request->treatment_plan;
        $form_array['precautions']        = $request->precautions;
        $form_array['exercise_program']   = $request->exercise_program;
        $form_array['health_progress']    = $request->health_progress;
        $form_array['feedback']           = $request->feedback;

        return $form_array;
    }

    public function sendNotificationToManager($managerID, $extra)
    {
        if($managerID){
            $manager = Caregiver::withoutGlobalScopes()->whereNull('deleted_at')->whereId($managerID)->first();
            if($manager){
                $managerName = $manager->full_name;
                $userID = User::withoutGlobalScopes()->whereUserId($managerID)->whereUserType('Caregiver')->value('id');
                $tokens = FCMToken::withoutGlobalScopes()->where('user_id',$userID)
                                ->whereUserType('Manager')->pluck('token')->toArray();

                if($tokens){
                    $pdata = [
                        'type' => 'notification',
                        'message' => 'You have been assigned a case.',
                        'category' => 'Case',
                        'lead_id' => $extra['lead_id'],
                    ];

                    $response = \AppHelpers\PushMessaging::send($tokens, 'New Case','You have been assigned a case.',$pdata);
                }
            }
        }
    }

    public function addActivityLog($type, $data)
    {
        if($type && $data){
            switch ($type) {
                case 'value':
                    # code...
                    break;

                default:
                    # code...
                    break;
            }
        }
    }

    public function checkemail(Request $request)
    {
        $email= $request->user_email;
        $checkpatient = Patient::where('email',$email)->count();
        if($checkpatient){
            echo "Email Already Exist.";
        }
        else
        {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
                echo "OK";
            }else{
                echo "Enter a valid email id.";
            }
        }
        exit();
    }

    public function assignStaffFromSchedule(Request $request)
    {
        if(count($request->schedules) > 0 && $request->staff_id != null){
            foreach($request->schedules as $key => $scheduleCounter){
                $checkIVRSTenant   = \Helper::getTenantId() == 271;
                $schedule = Schedule::whereId(\Helper::encryptor('decrypt',$scheduleCounter))->whereStatus('Pending')->first();
                if($key==0 && isset($schedule)){
                    $leadid  = $schedule->lead_id; // getting lead id
                }
                if(isset($schedule)){
                    if($checkIVRSTenant){
                        //WARNING - Tenant ID in production mode to be set to 266 Only
                        $type = $request->ivrs_type;
                        \App\Helpers\IVRS::scheduling($type,$schedule,$request->staff_id);
                    }     
                    $schedule->update(['caregiver_id' => $request->staff_id, 'road_distance' => (empty($request->road_distance))?null:$request->road_distance, 'delivery_address' => $request->delivery_address_assign]);
                }
            }
            $lead = Lead::whereId($leadid)->first();
            $caregiver = Caregiver::whereId($request->staff_id)->first();
            if($lead->patient->email != null && filter_var($lead->patient->email, FILTER_VALIDATE_EMAIL)){
                $mail = Mail::to($lead->patient->email);
                $mail->send(new StaffAssign($lead,$caregiver));
            }

            // return response()->json(200);
            return json_encode(200);
        }
    }

    public function prevServiceRecords(Request $request, $id)
    {
        $services = CaseServices::wherePatientId(\Helper::encryptor('decrypt',$id))
                    ->latest()->get()
                    ->groupBy(function($date) {
                        return \Carbon\Carbon::parse($date->created_at)->format('m-Y');
                    });
        $patient = Patient::whereId(\Helper::encryptor('decrypt',$id))->first();

        return view('leads.prev-service-records', compact('services', 'patient'));
    }

    public function auditLog(Request $request, $id)
    {
        $lead = Lead::whereId(\Helper::encryptor('decrypt',$id))->first();
        return view('leads.audit-log', compact('lead'));
    }

    public function getDistanceFromMapMyIndia(Request $request)
    {
        $url = $request->url;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10); // Tell cURL that it should only spend 10 seconds trying to connect to the URL in question
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // A given cURL operation should only take 30 seconds max
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($curl);

        // also get the error and response code
        //$errors = curl_error($curl);
        //echo $errors;
        curl_close($curl);
        return $result;
    }

    public function manuallyUpdateRoadDistanceFromMapMyIndia($date)
    {
        // run this url- https://smarthealthconnect.com/leads/manually-update-road_distance-from-MapMyIndia/2018-10-06
        ini_set('max_execution_time', 600); //300 seconds = 5 minutes
        $schedules = Schedule::whereNull('road_distance')->whereDate('schedule_date', '=', date($date))->get();
        foreach ($schedules as $key => $schedule) {
            $Caregiver = Caregiver::select('latitude','longitude')->whereId($schedule->caregiver_id)->first();
            $Patient = Patient::select('latitude','longitude')->whereId($schedule->patient_id)->first();
            if(isset($Caregiver->latitude) && isset($Caregiver->longitude) && isset($Patient->latitude) && isset($Patient->longitude)) {
                if(trim($Caregiver->latitude)!='' && trim($Caregiver->longitude)!='' && trim($Patient->latitude)!='' && trim($Patient->longitude)!='') {
                    $url = 'https://apis.mapmyindia.com/advancedmaps/v1/89oegcgbhdv1il54kcry8hrlfwet71kf/distance_matrix/driving/'.trim($Caregiver->longitude).','.trim($Caregiver->latitude).';'.trim($Patient->longitude).','.trim($Patient->latitude).'';
                    sleep(5); //sleep for 5 seconds
                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_URL, $url);
                    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10); // Tell cURL that it should only spend 10 seconds trying to connect to the URL in question
                    curl_setopt($curl, CURLOPT_TIMEOUT, 30); // A given cURL operation should only take 30 seconds max
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                    $mapmyindias = json_decode(curl_exec($curl));
                    curl_close($curl);
                    if($mapmyindias->responseCode == 200){
                        if(!empty($mapmyindias->results->distances)){
                            $road_distance = substr((($mapmyindias->results->distances[0][1]) / 1000),0,4).' km';
                            Schedule::find($schedule->id)->update(['road_distance' => $road_distance]);
                            dump('SrNo. '.$key.'> Schedule Id: '.$schedule->id.' has been updated with road distance '.$road_distance);
                        } else {dump('SrNo. '.$key.'> Schedule Id: '.$schedule->id.' | Empty result inside api');}
                    } else {
                        dump('SrNo. '.$key.'> Schedule Id: '.$schedule->id.' | No response from mapmyindia');
                    }
                }
            }
        }
        dump('Finished updating');
    }

    public function allIncidents()
    {
        $pendingIncident = PatientIncident::whereStatus('Pending')->get();
        $completedIncident = PatientIncident::whereStatus('Completed')->get();
        $patients = Patient::get();
        $caregivers = Caregiver::get();
        return view('leads.view-patient-incidents', compact('pendingIncident','patients','completedIncident','caregivers'));
    }

    public function addIncident(Request $request)
    {
        $data['patient_id'] = $request->patient_id;
        $data['complain_type'] = $request->complain_type;
        $data['complain_details'] = $request->complain_details;
        $data['manager'] = \Helper::getUserID();
        $data['complain_for_caregiver_id'] = empty($request->complain_for_caregiver_id)?null:$request->complain_for_caregiver_id;
        $data['action_taken'] = empty($request->action_taken)?null:$request->action_taken;

        if(!empty($request['incident_id'])){
            $data['patient_id'] = $request->patient_id_view_only;
            $data['status'] = $request->status;
            PatientIncident::find($request['incident_id'])->update($data);
            session()->flash('alert_success', 'Incident Updated Successfully.');
        }else{
            PatientIncident::create($data);
            session()->flash('alert_success', 'Incident Added Successfully.');
        }
        return redirect()->back();
    }

    public function storeCheckList(Request $request){

        $data = $request->except(['_token','checklist_id','patient_id','lead_id']);
        $data['patient_id'] = Helper::encryptor('decrypt',$request->patient_id);
        $data['lead_id'] = Helper::encryptor('decrypt',$request->lead_id);
        $data['tenant_id'] = Helper::getTenantID();
        if(!empty($request->date_time)){
            $data['date_time'] = date_format(new \DateTime($request->date_time),'Y-m-d H:i:s');
        }else{
            $data['date_time'] = date('Y-m-d H:i:s');
        }
        $check = PatientCheckList::find($request->checklist_id);
        if($check){
            $check->delete();
            PatientCheckList::create($data);
            return back()->with('alert_success','Data Updated Successfully');
        }
        PatientCheckList::create($data);
        return back()->with('alert_success','Data Saved Successfully');
    }
}