<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Mail\Aggregator\LeadPropagation;
use App\Mail\Aggregator\LeadAcceptance;
use App\Mail\Aggregator\LeadDeclination;

use App\Helpers\Helper;
use Mail;

use App\Entities\Masters\Service;
use App\Entities\Masters\Skill;
use App\Entities\Masters\Language;
use App\Entities\Masters\Specialization;
use App\Entities\Settings;

use App\Entities\Provider;
use App\Entities\Profile;
use App\Entities\User;
use App\Entities\FCMToken;
use App\Entities\Masters\Branch;

use App\Entities\Lead;
use App\Entities\LeadRequests;
use App\Entities\AggregatorLeads;
use App\Entities\LeadDistributionHistory;
use App\Entities\CaseFeedback;
use App\Entities\Patient;
use App\Entities\Careplan;
use App\Entities\Schedule;
use App\Entities\CaseForm;
use App\Entities\LabTest;
use App\Entities\Comment;
use App\Entities\CaseDisposition;
use App\Entities\Caregiver\Caregiver;
use App\Entities\WorkLog;
use App\Entities\WorkLogFeedback;
use App\Entities\CustomerLogin;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use App\DataTables\CasesDataTable;

class CasesController extends Controller
{
    protected static $isAggregator = false;

    public function __construct()
    {
        $this->middleware(function($request, $next){
            if(!self::$isAggregator){
                $utype = Helper::encryptor('decrypt',session('utype'));
                self::$isAggregator = $utype=='Aggregator'?true:false;
            }

            return $next($request);
        });
    }

    public function indexOld(Request $request, CasesDataTable $casesDataTable)
    {
        //$providers = Profile::withoutGlobalScopes()->get();

        return $casesDataTable->render('cases.datatable', compact('providers'));
    }

    public function index(Request $request,$type='pending')
    {

        $data['leads'] = Lead::orderBy('created_at','Desc')->get();
        $data['distributions'] = LeadDistributionHistory::orderBy('created_at','Desc')->get();
        $cases = [];
        $aggLeads = AggregatorLeads::orderBy('provider_id','Asc')->get();

        $prevProviderID = null;
        $aggregatorManager = null;
        $profile = null;
        if(count($aggLeads)){
            foreach ($aggLeads as $aggLead) {
                if($prevProviderID != $aggLead->provider_id){
                    \DB::purge('api');
                    // Set Tenant Connection
                    self::setTenantConnection($aggLead->provider_id);
                    $profile = Profile::whereTenantId($aggLead->provider_id)->first();
                }

                $lead = Lead::withoutGlobalScopes()->aggregator()->whereId($aggLead->lead_id)->withTrashed()->first();
                if($lead->aggregator_manager_id != null){
                    $aggregatorManager = User::on('aggregator')->withoutGlobalScopes()->whereId($lead->aggregator_manager_id)->value('full_name');
                }
                $cases[] = (object) [
                    'id' => $aggLead->id,
                    'lead_id' => $aggLead->lead_id,
                    'aggregator_manager' => $aggregatorManager?$aggregatorManager:0,
                    'aggregator_service' => $lead->aggregator_service,
                    'provider_id' => $profile->tenant_id,
                    'provider_name' => $profile->organization_name,
                    'provider_short_name' => $profile->organization_short_name,
                    'branch' => $lead->branch?$lead->branch->branch_city:'',
                    'episode_id' => $lead->episode_id,
                    'patient_id' => $lead->patient_id,
                    'patient_name' => $lead->patient->full_name,
                    'patient_phone' => $lead->patient->contact_number,
                    'patient_email' => $lead->patient->email,
                    'patient_enquirer' => $lead->patient->enquirer_name,
                    'patient_alternate_number' => $lead->patient->alternate_number,
                    'patient_area' => $lead->patient->area,
                    'patient_city' => $lead->patient->city,
                    'status' => $lead->status,
                    'created_at' => $lead->created_at,
                ];

                \DB::purge('api');
            }
        }

        $data['cases'] = collect($cases);
        $data['requests'] = LeadRequests::whereStatus('Not Checked')->orderBy('created_at','Desc')->get();
        \DB::setDefaultConnection('aggregator');

        return view('leads.aggregator-index', $data);
    }

    public function create(Request $request)
    {
        $data['services'] = Service::get();
        $data['languages'] = Language::get();
        $data['specializations'] = Specialization::get();
        $data['branches'] = Branch::get();

        if($request->has('ref') && $request->ref){
            $id = Helper::encryptor('decrypt',$request->ref);
            $data['lead'] = Lead::withoutGlobalScopes()->whereId($id)->first();
        }

        $ciewFile = 'cases.create';
        if(self::$isAggregator)
            $ciewFile = 'cases.aggregator-create';

        return view($ciewFile, $data);
    }

    public function edit(Request $request){
        $id = \Helper::encryptor('decrypt',$request->id);
        $lead_id = \Helper::encryptor('decrypt',$request->lead_id);
        $data['leadRequestId'] = 0;

        $data['services'] = Service::get();
        $data['languages'] = Language::get();
        $data['specializations'] = Specialization::get();
        $data['branches'] = Branch::get();

        self::setTenantConnection($id);

        if(Helper::isAggregator()){
            $data['l'] = Lead::withoutGlobalScopes()->whereId($lead_id)->first();
            return view('leads.aggregator-create', $data);
        }
    }

    public function checkForExistingCase(Request $request)
    {
        $res = [];
        $results = [];

        $patient = Patient::withoutGlobalScopes();

        if(!empty($request->m)){
            $patient->where('mobile_number',$request->m);
        }
        if(!empty($request->e)){
            $patient->where('email',$request->e);
        }
        if(!empty($request->f)){
            $patient->where('first_name','like','%'.$request->f.'%')
                    ->orWhere('last_name','like','%'.$request->f.'%');
        }
        if(!empty($request->l)){
            $patient->where('first_name','like','%'.$request->l.'%')
                    ->orWhere('last_name','like','%'.$request->l.'%');
        }
        if(!empty($request->d)){
            $patient->whereRaw(\DB::raw('DATE_FORMAT(date_of_birth,"%d-%m-%Y") = "'.date_format(new \DateTime($request->d),'d-m-Y').'"'));
        }

        $patients = $patient->get();

        if($patients){
            foreach($patients as $p){
                $careplan = Careplan::wherePatientId($p->id)->get();
                if($careplan){
                    foreach ($careplan as $c) {
                        $results[] = [
                            'id' => $c->id,
                            'careplan_name' => $c->careplan_name,
                            'service' => $c->service->service_name,
                            'medical_conditions' => $c->medical_conditions,
                            'medications' => $c->medications,
                            'created_at' => $c->created_at->format('Y-m-d H:i:s'),
                            'visits' => $c->schedules()?$c->schedules()->count():0,
                            'status' => $c->status
                        ];
                    }
                }
            }
        }

        return response()->json($results, 200);
    }

    public function markLeadAsOtherRequest(Request $request)
    {
        $id = \Helper::encryptor('decrypt',$request->lead_request_id);
        $data = [
            'status'           =>'Checked',
            'requirement_type' => $request->requirement_type,
            'requirement'      => $request->requirement,
            'name'             => $request->name,
            'phone_number'     => $request->phone_number,
            'email'            => $request->email,
            'city'             => $request->city
        ];

        LeadRequests::whereId($id)->update($data);

        return back()->with('alert_success','Lead data saved successfully');
    }

    /*-----------------------------------------------
    | Save Case as aggregator
    |-----------------------------------------------*/

    public function saveCaseAsAggregator(Request $request)
    {
        $patient = $request->patient;
        $careplan = $request->careplan;
        if($request->lReq > 0)
            $leadRequestID = $request->lReq;
        else
            $leadRequestID = null;

        $result = [];
        // Below If() Section is only for removing leads from tenant database while assigning leads to a different provider in case of a lead declination
        // (or) when an Aggregator lead is assigned to a provider.
        if (isset($request->pacd['previous_provider_id']) && isset($request->pacd['previous_lead_id'])) {
            $prevPID = Helper::encryptor('decrypt',$request->pacd['previous_provider_id']);
            $prevLID = Helper::encryptor('decrypt',$request->pacd['previous_lead_id']);

            if(\Helper::checkCaseToAggregator($prevPID) || $request->pid == '')
            {
                if(!\Helper::checkCaseToAggregator($prevPID)){
                    self::setTenantConnection($prevPID);
                    $prevPatientId = Lead::withoutGlobalscopes()->withTrashed()->aggregator()->whereId($prevLID)->value('patient_id');
                    Lead::withoutGlobalscopes()->withTrashed()->aggregator()->whereId($prevLID)->restore();
                    Lead::withoutGlobalscopes()->aggregator()->whereId($prevLID)->forceDelete();
                    Patient::whereId($prevPatientId)->forceDelete();
                    AggregatorLeads::whereProviderId($prevPID)->whereLeadId($prevLID)->wherePatientId($prevPatientId)->forceDelete();
                    \DB::setDefaultConnection('aggregator');
                }else{
                    $prevPatientId = Lead::whereId($prevLID)->value('patient_id');
                    Lead::whereId($prevLID)->forceDelete();
                    Patient::whereId($prevPatientId)->forceDelete();
                }
            }else
            {
                    self::setTenantConnection($prevPID);
                    $prevPatientId = Lead::withoutGlobalscopes()->withTrashed()->aggregator()->whereId($prevLID)->value('patient_id');
                    Lead::withoutGlobalscopes()->withTrashed()->aggregator()->whereId($prevLID)->restore();
                    Lead::withoutGlobalscopes()->aggregator()->whereId($prevLID)->forceDelete();
                    Patient::whereId($prevPatientId)->forceDelete();
                    AggregatorLeads::whereProviderId($prevPID)->whereLeadId($prevLID)->wherePatientId($prevPatientId)->forceDelete();
            }
        }
        //Below if() block is intended for saving lead in provider's database
        // and else() block for saving into aggregator database.
        if($request->pid && $request->bid){
            $providerID = Helper::encryptor('decrypt',$request->pid);
            $branchID = Helper::encryptor('decrypt',$request->bid);
            if(!empty($providerID) && $providerID != 0){
                // 1. Set Tenant Connection
                \DB::purge('api');
                self::setTenantConnection($providerID);
                $providerBranchEmail = Branch::whereId($branchID)->value('branch_email');

                // 2. Store patient details in tenant DB
                if($patient)
                {
                    $patient['tenant_id'] = $providerID;
                    // Check for existing record
                    $patientRecord = Patient::whereContactNumber($patient['contact_number'])
                    ->where('alternate_number',$patient['alternate_number'])
                    ->where('email',$patient['email'])
                    ->first();

                    // Create or Update Patient data
                    if($patientRecord){
                        $patientRecord->update($patient);
                        $patientID = $patientRecord->id;
                    }else{
                        $patient['created_at'] = date('Y-m-d H:i:s');

                        $patientID = Patient::withoutGlobalScopes()->insertGetId($patient);
                    }

                    if(!empty($patient['date_of_birth'])){
                        $date = new \DateTime($patient['date_of_birth']);
                        $patient['date_of_birth'] = date_format($date,'Y-m-d');
                    }else{
                        $patient['date_of_birth'] = null;
                    }
                    $result['patient'] = $patientID;
                }

                // 3. Store lead details in tenant DB
                if($careplan && isset($patient))
                {
                    $lead = [];
                    $lead['tenant_id'] = $providerID;
                    $lead['branch_id'] = $branchID;
                    $lead['patient_id'] = $patientID;
                    $lead['case_description'] = $careplan['case_description'];
                    $lead['medical_conditions'] = $careplan['medical_conditions'];
                    $lead['medications'] = $careplan['medications'];
                    $lead['procedures'] = $careplan['procedures'];
                    $lead['estimated_duration'] = $careplan['no_of_hours'];
                    $lead['gender_preference'] = $careplan['gender_preference'];
                    $lead['language_preference'] = $careplan['language_preference'];
                    $lead['referrer_name'] = 'ApnaCare';
                    $lead['aggregator_lead'] = 1;
                    $lead['aggregator_lead_request_id'] = $leadRequestID;
                    $lead['aggregator_manager_id'] = \Helper::encryptor('decrypt',session('user_id'));
                    $lead['aggregator_service'] = $careplan['service_name'];
                    $lead['aggregator_rate'] = $careplan['rate_agreed'];
                    $lead['aggregator_rate_negotiable'] = $careplan['rate_negotiable'];
                    $lead['aggregator_lead_status'] = 'Pending';
                    $lead['status'] = 'Pending';
                    $lead['created_at'] = date('Y-m-d H:i:s');

                    $leadID = Lead::withoutGlobalScopes()->insertGetId($lead);
                    $lead = Lead::withoutGlobalScopes()->whereId($leadID)->first();
                    $patient = Patient::whereId($patientID)->first();
                }

                // 4. Store leadID and patientID in aggregator_leads table
                $aggregatorLeads = [
                'provider_id' => $providerID,
                'branch_id' => $branchID,
                'patient_id' => $patientID,
                'lead_id' => $leadID,
                'lead_request_id' => $leadRequestID,
                'service_id' => $careplan['service_id'],
                'rate_agreed' => $careplan['rate_agreed'],
                'rate_negotiable' => $careplan['rate_negotiable'],
                'lead_source_id' => $careplan['lead_id'],
                'user_id' => \Helper::encryptor('decrypt',session('user_id')),
                'source' => $careplan['source'],
                'status' => 'Pending'
                ];
                AggregatorLeads::create($aggregatorLeads);
                if($leadRequestID != null)
                {
                    LeadRequests::whereId($leadRequestID)->update(['status' => 'Checked']);
                }
                $mail = Mail::to($providerBranchEmail);
                $mail->send(new LeadPropagation($patient,$lead));
            }
        }else{
            if($patient)
            {
                $providerID = $patient['tenant_id'] = \Helper::getTenantID();
                // Check for existing record
                $patientRecord = Patient::whereContactNumber($patient['contact_number'])
                ->where('alternate_number',$patient['alternate_number'])
                ->where('email',$patient['email'])
                ->first();
                // Create or Update Patient data
                if($patientRecord){
                    $patientRecord->update($patient);
                    $patientID = $patientRecord->id;
                }else{
                    $patient['created_at'] = date('Y-m-d H:i:s');
                    $patientID = Patient::withoutGlobalScopes()->insertGetId($patient);
                }

                if(!empty($patient['date_of_birth'])){
                    $date = new \DateTime($patient['date_of_birth']);
                    $patient['date_of_birth'] = date_format($date,'Y-m-d');
                }else{
                    $patient['date_of_birth'] = null;
                }
                $result['patient'] = $patientID;
            }
            // 3. Store lead details in tenant DB
            if($careplan && isset($patient))
            {
                $lead = [];
                $lead['tenant_id'] = $providerID;
                $lead['branch_id'] = 0;
                $lead['patient_id'] = $patientID;
                $lead['case_description'] = $careplan['case_description'];
                $lead['medical_conditions'] = $careplan['medical_conditions'];
                $lead['medications'] = $careplan['medications'];
                $lead['procedures'] = $careplan['procedures'];
                $lead['estimated_duration'] = $careplan['no_of_hours'];
                $lead['gender_preference'] = $careplan['gender_preference'];
                $lead['language_preference'] = $careplan['language_preference'];
                $lead['referrer_name'] = 'ApnaCare';
                $lead['service_required'] = $careplan['service_id'];
                $lead['rate_agreed'] = $careplan['rate_agreed'];
                $lead['status'] = 'Pending';
                $lead['aggregator_lead_request_id'] = $leadRequestID;
                $lead['aggregator_manager_id'] = \Helper::encryptor('decrypt',session('user_id'));
                $lead['created_at'] = date('Y-m-d H:i:s');

                $leadID = Lead::withoutGlobalScopes()->insertGetId($lead);
                $lead = Lead::withoutGlobalScopes()->whereId($leadID)->first();
                $patient = Patient::whereId($patientID)->first();
            }
            if($leadRequestID != null)
            {
                LeadRequests::whereId($leadRequestID)->update(['status' => 'Checked']);
            }
        }
        return response()->json($result,200);
    }

    public function aggregatorLeadStatus(Request $request)
    {
        $leadID = Helper::encryptor('decrypt',$request->lead_id);
        $patientID = Helper::encryptor('decrypt',$request->patient_id);

        if($request->has('leadStatus') && $leadID && $patientID){
            if($request->input('leadStatus') == '1')
                $status = 'Accepted';
            else
                $status = 'Declined';

            // Update Aggregator Leads table
            $aggLead = AggregatorLeads::whereProviderId(Helper::getTenantID())->whereLeadId($leadID)->wherePatientId($patientID)->first();
            if($aggLead){
                $aggLead->status = $status;
                $aggLead->accepted_declined_date = date('Y-m-d H:i:s');
                $aggLead->save();
            }

            // Update Provider Leads Table
            $lead = Lead::whereId($leadID)->wherePatientId($patientID)->first();

            if($lead){
                $lead->aggregator_lead_status = $status;
                $lead->aggregator_accepted_declined_date = date('Y-m-d H:i:s');
                if($status == 'Declined'){
                    $lead->status = $status;
                    $lead->deleted_at = date('Y-m-d H:i:s');
                }

                $lead->save();
            }

            //Send Mails
            $patient = Patient::whereId($patientID)->first();
            $providerBranchEmail = Branch::whereId($lead->branch_id)->value('branch_email');

            if($lead->aggregator_lead_status == 'Accepted'){
                    $mail = Mail::to($providerBranchEmail);
                    $mail->send(new LeadAcceptance($patient,$lead));
            }
            if($lead->aggregator_lead_status == 'Declined'){
                    $mail = Mail::to($providerBranchEmail);
                    $mail->send(new LeadDeclination($patient,$lead));
            }
            //Update Aggregator Distribution
            $ldh = new LeadDistributionHistory;
                $ldh->tenant_id = \Helper::getTenantID();
                $ldh->branch_name = Profile::withoutGlobalScopes()->value('organization_name').'-'.Branch::withoutGlobalscopes()->whereId($lead->branch_id)->value('branch_city');
                $ldh->patient_name = $lead->patient->first_name.' '.$lead->patient->last_name;
                $ldh->patient_number = $lead->patient->contact_number;
                $ldh->email = $lead->patient->email;
                $ldh->city = $lead->patient->city;
                $ldh->requirement = $lead->aggregator_service;
                $ldh->status = $lead->aggregator_lead_status;
                $ldh->status_date = date('Y-m-d H:i:s');

                $ldh->save();
        }

        return back()->with('alert_info','You have '.$status.' the lead.');
    }

    public function retractLeadFromProvider(Request $request)
    {
        $leadID = Helper::encryptor('decrypt',$request->lead_id);
        $patientID = Helper::encryptor('decrypt',$request->patient_id);
        $providerID = Helper::encryptor('decrypt',$request->provider_id);
        $status = 'Declined';

        // Update Aggregator Leads table
        $aggLead = AggregatorLeads::whereProviderId($providerID)->whereLeadId($leadID)->wherePatientId($patientID)->first();
        if($aggLead){
            $aggLead->status = $status;
            $aggLead->accepted_declined_date = date('Y-m-d H:i:s');
            $aggLead->save();
        }

        // Update Provider Leads Table
        Controller::setTenantConnection($providerID);
        $lead = Lead::withoutGlobalScopes()->whereId($leadID)->wherePatientId($patientID)->first();

        if($lead){
            Lead::withoutGlobalScopes()->whereId($leadID)->wherePatientId($patientID)->update(['aggregator_lead_status'=>$status,'aggregator_accepted_declined_date'=> date('Y-m-d H:i:s'),'status'=>$status,'deleted_at'=> date('Y-m-d H:i:s')]);
        }

        //Send Mails to Provider
        $patient = Patient::whereId($patientID)->first();
        $providerBranchEmail = Branch::whereId($lead->branch_id)->value('branch_email');

        $mail = Mail::to($providerBranchEmail);
        $mail->send(new LeadDeclination($patient,$lead));

        //Update Aggregator Distribution
        $ldh = new LeadDistributionHistory;
        $ldh->tenant_id = $providerID;
        $ldh->branch_name = Profile::withoutGlobalScopes()->value('organization_name').'-'.Branch::withoutGlobalscopes()->whereId($lead->branch_id)->value('branch_city');
        $ldh->patient_name = $lead->patient->first_name.' '.$lead->patient->last_name;
        $ldh->patient_number = $lead->patient->contact_number;
        $ldh->email = $lead->patient->email;
        $ldh->city = $lead->patient->city;
        $ldh->requirement = $lead->aggregator_service;
        $ldh->status = 'Declined';
        $ldh->status_date = date('Y-m-d H:i:s');

        $ldh->save();

        \DB::purge('api');
        \DB::setDefaultConnection('aggregator');
        return back()->with('alert_info','You have retracted the lead from the provider.');
    }

    public function view(Request $request)
    {
        $id = Helper::encryptor('decrypt',$request->id);
        $leadID = Helper::encryptor('decrypt',$request->lead_id);

        // Set tenant connection
        self::setTenantConnection($id);

        $data['services'] = Service::get();
        $data['skill'] = Skill::get();

        $data['languages'] = Language::get();
        $data['skills'] = Skill::get();
        $data['specializations'] = Specialization::get();
        $data['feedbackcomments'] = [];
        $data['losscomment'] = [];
        $data['persons'] = Caregiver::get();

        $data['l'] = Lead::withoutGlobalScopes()->aggregator()->find($leadID);
        return view('cases.view-case', $data);
    }

    public function saveCaseDetails(Request $request)
    {
        $response = [];
        $careplanID = Helper::encryptor('decrypt',$request->careplan_id);
        $data = $request->only('crn_number','careplan_name','careplan_description','case_charges','medical_conditions','medications','source','referrer_name','branch_id','service_id','no_of_hours','gender_preference','language_preference','tenant_id');

        if($data){
            if(isset($data['tenant_id'])){
        if(!is_numeric($data['tenant_id']))
                    $data['tenant_id'] = Helper::encryptor('decrypt',$data['tenant_id']);
            }

            if($data['tenant_id'] == '' || $data['tenant_id'] == 0){
        if($data['tenant_id'] != 0){
                   $providerID = Helper::encryptor('decrypt',$data['tenant_id']);
                   $data['tenant_id'] = $providerID;
        }else{
           $data['tenant_id'] = 0;
        }
            }

            $data['language_preference'] = $data['language_preference']?implode(",",$data['language_preference']):null;

            // If the Branch ID is encrypted value
            if(!is_numeric($data['branch_id'])){
                $data['branch_id'] = Helper::encryptor('decrypt',$data['branch_id']);
            }

            // If CRN No is null, generate CRN
            if(empty($data['crn_number'])){
                $data['crn_number'] = str_pad(date('m'),2,'0',STR_PAD_LEFT).str_pad(date('d'),2,'0',STR_PAD_LEFT).str_pad(date('h'),2,'0',STR_PAD_LEFT).str_pad(date('s'),2,'0',STR_PAD_LEFT).str_pad(mt_rand(0, 99), 2, '0', STR_PAD_LEFT);
            }

            if(self::$isAggregator){
                $providerID = Helper::encryptor('decrypt',$request->provider_id);

                $res = Careplan::withoutGlobalScopes()
                                ->whereTenantId($providerID)
                                ->whereId($careplanID)
                                ->update($data);
            }else{
                $res = Careplan::find($careplanID)->update($data);
            }
            $response['cid'] = $request->careplan_id;
            if($data['tenant_id'] == '' || $data['tenant_id'] == 0){
                $response['pid'] = 0;
            }
            else{
                $response['pid'] = Helper::encryptor('encrypt' , $data['tenant_id']);
            }

            $patient = Patient::where('id',Helper::encryptor('decrypt' , $request->patient_id))->first();
            $patient->tenant_id = $data['tenant_id'];
            $patient->save();

            if(Helper::encryptor('decrypt',session('utype')) == 'Aggregator'){
                $response['redirect'] = 'https://beta.apnacare.in/cases/view/'.$response['cid'];
              if($response['pid'])
              $response['redirect'] .= '?pid='.$response['pid'];
        }
            else
                $response['redirect'] = 'https://beta.apnacare.in/cases/view/'.$response['cid'];
        }

        return response()->json($response,200);
    }

    public function updateDisposition(Request $request)
    {
        $res = false;

        if($request->ajax() && $request->cid){
            $id = Helper::encryptor('decrypt',$request->cid);

            if(self::$isAggregator && !empty($request->pid)){
                $careplan = Careplan::withoutGlobalScopes()
                                    ->whereTenantId(Helper::encryptor('decrypt',$request->pid))
                                    ->whereId($id)
                                    ->first();
            }
            else{
                $careplan = Careplan::find($id);
            }

            if($careplan){
                // Update Careplan table
                $careplan->status = $request->status;
                $careplan->save();

                $date = null;
                if($request->date){
                    $date = date_format(new \DateTime($request->date),'Y-m-d');
                }

                if($request->status == 'CASE LOSS'){
                    $caselosscomment = $request->caselosscomment;
                }else{
                    $caselosscomment = '-';
                }
                // Add to case disposition table
                $dispositionData = [
                    'careplan_id' => $id,
                    'user_id' => \Auth::id(),
                    'status' => $request->status,
                    'disposition_date' => $date,
                    'comment' => $request->comment,
                    'caselosscomment'=> $caselosscomment
                ];

                if(self::$isAggregator && !empty($request->pid)){
                    $dispositionData['tenant_id'] = Helper::encryptor('decrypt',$request->pid);
                    $dispositionData['created_at'] = $dispositionData['updated_at'] = date('Y-m-d H:i:s');

                    $res = CaseDisposition::withoutGlobalScopes()->insert($dispositionData);
                }else{
                    $res = CaseDisposition::create($dispositionData);
                }
            }
        }

        return response()->json((bool) $res,200);
    }

    public function saveVisitSchedule(Request $request)
    {
        $res = [];
        $careplanID = $request->careplan_id;
        $patientID = $request->patient_id;
        $visit = $request->visit;

        // Store Schedules Details
        if(isset($careplanID) && isset($patientID)){
            if($visit){
                $visit['patient_id'] = Helper::encryptor('decrypt',$patientID);
                $visit['careplan_id'] = Helper::encryptor('decrypt',$careplanID);

                // If the Caregiver ID is encrypted value
                if(!is_numeric($visit['caregiver_id'])){
                    $visit['caregiver_id'] = Helper::encryptor('decrypt',$visit['caregiver_id']);
                }

                $visit['start_date'] = ($visit['start_date']!="")?date_format(new \DateTime($visit['start_date']),'Y-m-d'):null;
                $visit['end_date'] = ($visit['end_date'] != "")?date_format(new \DateTime($visit['end_date']),'Y-m-d'):null;

                if(isset($visit['id']) && $visit['id'] != null){
                    $res = Schedule::find(Helper::encryptor('decrypt',$visit['id']))->update($visit);
                    if($visit['status'] == 'SERVICE COMPLETED'){
                        $c = Caregiver::whereId($visit['caregiver_id'])->first();
                        $c->work_status = "Available";
                        $c->availability_status = "NA";

                        $c->save();
                    }
                }else{
                    $res = Schedule::create($visit);
                    $c = Caregiver::whereId($visit['caregiver_id'])->first();
                    $c->work_status = "On Duty";

                    $c->save();
                }
            }
        }

        return response()->json((bool) $res,200);
    }

    public function saveComment(Request $request)
    {
        $res = false;
        $careplanID = Helper::encryptor('decrypt',$request->careplan_id);
        $comment = $request->comment;

        if($careplanID && $comment){
            $data = [
                'careplan_id' => $careplanID,
                'user_id' => \Auth::id(),
                'user_type' => 'User',
                'user_name' => \Auth::user()->full_name,
                'comment' => $comment
            ];

            if(self::$isAggregator && $request->provider_id){
                $data['tenant_id'] = Helper::encryptor('decrypt',$request->provider_id);
                $data['user_name'] = $data['user_name'].' - ApnaCare';
                $data['created_at'] = $data['updated_at'] = date('Y-m-d H:i:s');

                $res = Comment::withoutGlobalScopes()
                              ->whereTenantId($request->provider_id)
                              ->insert($data);
            }else{
                $res = Comment::create($data);
            }
        }

        return back()->with('success','Comment added successfully');
    }

    public function saveFeedbackComment(Request $request)
    {
        $data = [
            'tenant_id'   => \Helper::getTenantID(),
            'careplan_id' => \Helper::encryptor('decrypt',$request->careplan_id),
            'comment'     => $request->feedback_comment,
            'time'        => $request->feedback_time,
            'user_id'     => \Helper::encryptor('decrypt',session('user_id')),
            'user_name'   => session('user_name')
        ];

        $data = CaseFeedback::create($data);
        return back()->with('success','Feedback Comment added successfully');
    }

    public function saveForm(Request $request)
    {
        $form_array=[];

        if($request->form_type == 'nursing')
            $form_array = $this->saveNursingAssessment($request);
        elseif($request->form_type == 'physiotherapy')
            $form_array = $this->savePhysioAssessment($request);
        elseif($request->form_type == 'labtest')
            $form_array = $this->saveLabTest($request);
        else{
            session()->flash('alert_warning', 'Please Select the type of Form.');
            return redirect()->back();
        }

        $form_data=json_encode($form_array);
        $assessment = new Assessment;
        $assessment->tenant_id = \Helper::getTenantID();
        $assessment->careplan_id = \Helper::encryptor('decrypt',$request->careplan_id);
        $assessment->user_id = $request->user_id;
        $assessment->form_data = $form_data;
        $assessment->save();

        session()->flash('alert_success', 'Assessment Added Successfully Against the Schedule.');

        return redirect()->back();
    }

    //Saving Assessments based on specialization

    public function saveNursingAssessment($request){
        $form_array=[];
        $form_array['assessment_date']    = $request->assessment_date;
        $form_array['assessor_name']        = $request->assessor_name;
        $form_array['patient_mobility'] = $request->Patient_Mobility;
        $form_array['patient_consciousness'] = $request->Patient_Consciousness;
        $form_array['medical_procedures'] = $request->Medical_Procedures;
        $form_array['suggested_professional'] = $request->Suggested_Home_Care_Professional;
        $form_array['comment'] = $request->notes;

        return $form_array;
    }

    public function savePhysioAssessment($request){
        $form_array=[];
        $form_array['assessment_date']    = $request->assessment_date;
        $form_array['assessor_name']        = $request->assessor_name;
        $form_array['chief_complaints']   = $request->chief_complaints;
        $form_array['sites']              = $request->sites;
        $form_array['sides']              = $request->sides;
        $form_array['duration']           = $request->duration;
        $form_array['aggrevating_factors']= $request->aggrevating_factors;
        $form_array['relieving_factors']  = $request->relieving_factors;
        $form_array['assessform']         = $request->assessform;

        //check whether a schedule is selected
        if($request->schedule_details == ""){
            session()->flash('alert_error', 'Please Select a Person and the Assessment Form');
            return redirect()->back();
        }
        //check for proper form data inputs
        if($form_array['assessform'] == "ortho")
        {
            $form_array['rom']                = $request->rom;
            $form_array['gait']               = $request->gait;
            $form_array['posture']            = $request->posture;
            $form_array['deformity']          = $request->deformity;
            $form_array['ortho_tone']         = $request->ortho_tone;
            $form_array['swelling']           = $request->swelling;
        }
        //check for proper form data inputs
        if($form_array['assessform'] == "neuro")
        {
            $form_array['loc']                = $request->loc;
            $form_array['orientation']        = $request->orientation;
            $form_array['comprehension']      = $request->comprehension;
            $form_array['memory']             = $request->memory;
            $form_array['cne']                = $request->cne;
            $form_array['pressure_sore']      = $request->pressure_sore;
            $form_array['balance']            = $request->balance;
            $form_array['speech']             = $request->speech;
            $form_array['neuro_tone']         = $request->neuro_tone;
        }

        $form_array['treatment_plan']     = $request->treatment_plan;
        $form_array['precautions']        = $request->precautions;
        $form_array['exercise_program']   = $request->exercise_program;
        $form_array['health_progress']    = $request->health_progress;
        $form_array['feedback']           = $request->feedback;

        return $form_array;
    }

    public function monthlyReport(Request $request){
        $careplans = [];

        if($request->isMethod('post')){
            $fromDate = null;
            $toDate = null;
            if($request->filter_status != 'Pending'){

                if(\Helper::encryptor('decrypt',session('utype')) == 'Aggregator'){
                $cases = CaseDisposition::withoutGlobalScopes()
                                ->select('careplan_id')
                                ->leftJoin('case_records','case_records.id','=','case_dispositions.careplan_id')
                                ->where('case_records.apnacare_lead',1);
                }else{
                $cases = CaseDisposition::withoutGlobalScopes()
                                ->select('careplan_id')
                                ->leftJoin('case_records','case_dispositions.careplan_id','=','case_records.id')
                                ->where('case_dispositions.tenant_id',\Helper::getTenantID());
                }
                if($request->start_date && $request->end_date){
                    $fromDate = date_format(new \DateTime($request->start_date),"Y-m-d");
                    $toDate = date_format(new \DateTime($request->end_date),"Y-m-d");

                    $cases->whereRaw(\DB::raw('DATE(case_dispositions.disposition_date) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'));
                }

                if($request->filter_status && $request->filter_status != ""){
                    // Filter By Status
                    if($request->filter_status == 'Converted'){
                        $cases->whereIn('case_dispositions.status',['SERVICE STARTED','SERVICE COMPLETED']);
                    }else{
                        $cases->where('case_dispositions.status',$request->filter_status);
                    }
                }

                $cases->groupBy('case_dispositions.careplan_id'); //->orderBy('case_dispositions.disposition_date','Desc');

                $casesList = collect($cases->get());

                if($casesList){
                    $leadIDs = $casesList->pluck('careplan_id');

                    $careplans = Careplan::withoutGlobalScopes()
                                        ->whereIn('id',$leadIDs)
                                        ->get();
                }
            }else{
                $careplans = Careplan::withoutGlobalScopes()
                                    ->whereStatus('Pending');

                if($request->start_date && $request->end_date){
                    $fromDate = date_format(new \DateTime($request->start_date),"Y-m-d");
                    $toDate = date_format(new \DateTime($request->end_date),"Y-m-d");

                    $careplans->whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'));
                }

                $careplans = $careplans->get();
            }

        }

        $data['c'] = $careplans;
        $data['providers'] = Profile::withoutGlobalScopes()->whereNull('deleted_at')->get();

        return view('cases.monthlyReport',$data);
    }

    /*
    |-----------------------------------------------
    | API Functions
    |-----------------------------------------------
    |*/

    public function getCaseData(Request $request)
    {
        $id = $request->user_id;
        $caregiverId = $request->caregiver_id;
        $type = $request->type;
        $newCaseIDs = $request->new_lead_ids;
        $existingCaseIDs = $request->existing_lead_ids;

        $referrer = $request->referrer;

        $results = false;
        $cases = $visits = $comments = [];

        if($id && $type){
            $newCaseIDList = [];
            $existingCaseIDList = [];

            if($newCaseIDs){
                $newCaseIDList = explode(",",$newCaseIDs);
                $newCaseIDList = str_replace("'","",$newCaseIDList);
            }
            if($existingCaseIDs){
                $existingCaseIDList = explode(",",$existingCaseIDs);
                $existingCaseIDList = str_replace("'","",$existingCaseIDList);
            }

            switch ($type) {
                case 'Aggregator':
                    $res = Profile::withoutGlobalScopes()->whereTenantId($id)->first();
                    if($res){
                        // Cases
                        $cases = CarePlan::withoutGlobalScopes()->whereApnacareLead(1);
                        if(count($newCaseIDList)){
                            $cases->whereIn('crn_number',$newCaseIDList);
                        }
                        if(count($existingCaseIDList)){
                            $cases->whereNotIn('crn_number',$existingCaseIDList);
                        }
                        $cases = $cases->get();
                    }

                    break;
                case 'Provider':
                        $res = Profile::withoutGlobalScopes()->whereTenantId($id)->first();
                        if($res){
                            // Cases
                            $cases = CarePlan::withoutGlobalScopes()->whereTenantId($res->tenant_id);
                            if(count($newCaseIDList)){
                                $cases->whereIn('crn_number',$newCaseIDList);
                            }
                            if(count($existingCaseIDList)){
                                $cases->whereNotIn('crn_number',$existingCaseIDList);
                            }
                            $cases = $cases->get();
                        }

                        break;

                case 'Caregiver':
                case 'Professional':
                        $caregiverID = Caregiver::withoutGlobalScopes()->whereId($id)->value('id');
                        if($caregiverID){
                            // Cases
                            $cases = CarePlan::withoutGlobalScopes()->whereHas('schedules',function($q) use($caregiverID){
                                $q->whereCaregiverId($caregiverID);
                            });
                            if(count($newCaseIDList)){
                                $cases->whereIn('crn_number',$newCaseIDList);
                            }
                            if(count($existingCaseIDList)){
                                $cases->whereNotIn('crn_number',$existingCaseIDList);
                            }
                            $cases = $cases->get();
                        }

                        break;

                case 'Customer':
                        $customerEmail = CustomerLogin::whereId($id)->value('email');
                        if($customerEmail){
                            $patientIDs = [];

                            $patients = Patient::whereEnquirerEmail($customerEmail)->get();
                            if($patients){
                                foreach ($patients as $p) {
                                    $patientIDs[] = $p->id;
                                }

                                if($patientIDs){
                                    // Cases
                                    $cases = CarePlan::withoutGlobalScopes()->whereNull('deleted_at')->whereIn('patient_id',$patientIDs);
                                    if(count($newCaseIDList)){
                                        $cases->whereIn('crn_number',$newCaseIDList);
                                    }
                                    if(count($existingCaseIDList)){
                                        $cases->whereNotIn('crn_number',$existingCaseIDList);
                                    }
                                    $cases = $cases->get();
                                }
                            }
                        }
                        break;
            }

            $results['cases'] = [];
            $results['patients'] = [];
            $results['caregivers'] = [];
            $results['assessments'] = [];
            $results['worklogs'] = [];
            $results['feedbacks'] = [];
            $results['visits'] = [];
            $results['comments'] = [];
            $results['dispositions'] = [];

            if(count($cases)){
                $patients = [];
                $assessments = [];
                $worklogs = [];
                $feedbacks = [];
                $visitsLists = [];
                $commentsLists = [];
                $dispositionsLists = [];
                $caregiversList = [];

                foreach($cases as $c){
                    $casesList[] = [
                        'id' => $c->id,
                        'crn_number' => Helper::str($c->crn_number),
                        'branch_id' => Helper::str($c->branch_id),
                        'patient_id' => Helper::str($c->patient_id),
                        'careplan_name' => isset($c->careplan_name)?Helper::str($c->careplan_name):'',
                        'medical_conditions' => Helper::str($c->medical_conditions),
                        'medications' => Helper::str($c->medications),
                        'gender_preference' => Helper::str($c->gender_preference),
                        'language_preference' => Helper::str($c->language_preference),
                        'service_id' => Helper::str($c->service_id),
                        'service_name' => $c->service?Helper::str($c->service->service_name):'-',
                        'no_of_hours' => Helper::str($c->no_of_hours),
                        'case_charges' => floatval(Helper::str($c->case_charges)),
                        'apnacare_lead' => Helper::str($c->apnacare_lead),
                        'status' => Helper::str($c->status),
                        'created_at' => Helper::getDate($c->created_at)
                    ];

                    $patientID = null;

                    if($patientID != $c->patient_id && isset($c->patient)){
                        $patients[] = [
                            'id' => Helper::str($c->patient->id),
                            'first_name' => Helper::str($c->patient->first_name),
                            'last_name' => Helper::str($c->patient->last_name),
                            'gender' => Helper::str($c->patient->gender),
                            'patient_age' => Helper::str($c->patient->patient_age),
                            'patient_weight' => Helper::str($c->patient->patient_weight),
                            'street_address' => Helper::str($c->patient->street_address),
                            'area' => Helper::str($c->patient->area),
                            'city' => Helper::str($c->patient->city),
                            'zipcode' => Helper::str($c->patient->zipcode),
                            'state' => Helper::str($c->patient->state),
                            'enquirer_name' => Helper::str($c->patient->enquirer_name),
                            'enquirer_phone' => Helper::str($c->patient->enquirer_phone),
                            'enquirer_email' => Helper::str($c->patient->enquirer_email),
                            'alternate_number' => Helper::str($c->patient->alternate_number),
                        ];
                    }

                    $patientID = $c->patient_id;

                    // Load assessments if any
                    if(isset($c->assessment)){
                        foreach ($c->assessment as $a) {
                            $assessments[] = [
                                'id' => Helper::str($a->id),
                                'tenant_id' => $a->tenant_id,
                                'careplan_id' => Helper::str($a->careplan_id),
                                'user_id' => Helper::str($a->caregiver_id),
                                'assess_form_data' => Helper::str($a->assess_form_data),
                                'created_at' => Helper::getDate($a->created_at),
                            ];
                        }
                    }

                    // Load worklogs if any
                    if(isset($c->worklog)){
                        foreach ($c->worklog as $w) {
                            $worklogs[] = [
                                'id' => Helper::str($w->id),
                                'tenant_id' => $w->tenant_id,
                                'careplan_id' => Helper::str($w->careplan_id),
                                'caregiver_id' => Helper::str($w->caregiver_id),
                                'caregiver_name' => isset($w->caregiver)?(Helper::str($w->caregiver->first_name).' '.Helper::str($w->caregiver->middle_name).' '.Helper::str($w->caregiver->last_name)):'',
                                'worklog_date' => Helper::str($w->worklog_date),
                                'vitals' => Helper::str($w->vitals),
                                'routines' => Helper::str($w->routines),
                                'created_at' => Helper::getDate($w->created_at),
                            ];

                            // Load worklog feedback if any
                            $feedback = WorkLogFeedback::whereWorklogId($w->id)->get();
                            if(isset($feedback)){
                                foreach ($feedback as $f) {
                                    $feedbacks[] = [
                                        'id' => Helper::str($f->id),
                                        'tenant_id' => $f->tenant_id,
                                        'careplan_id' => Helper::str($f->careplan_id),
                                        'worklog_id' => Helper::str($f->worklog_id),
                                        'customer_name' => Helper::str($f->customer_name),
                                        'rating' => floatval($f->rating),
                                        'comment' => Helper::str($f->comment),
                                        'created_at' => Helper::getDate($f->created_at),
                                    ];
                                }
                            }
                        }
                    }

                    // Load Visits/Schedules
                    if(isset($c->schedules)){
                        foreach ($c->schedules as $v) {
                            $visitsLists[] = [
                                'id' => $v->id,
                'tenant_id' => $v->tenant_id,
                                'careplan_id' => Helper::str($v->careplan_id),
                                'patient_id' => Helper::str($v->patient_id),
                                'caregiver_id' => Helper::str($v->caregiver_id),
                                'caregiver_name' => isset($v->caregiver)?(Helper::str($v->caregiver->first_name)." ".Helper::str($v->caregiver->middle_name)." ".Helper::str($v->caregiver->last_name)):'',
                                'notes' => isset($v->notes)?Helper::str($v->notes):'',
                                'start_date' => isset($v->start_date)?Helper::getDate($v->start_date):'',
                                'end_date' => isset($v->end_date)?Helper::getDate($v->end_date):'',
                                'status' => Helper::str($v->status),
                                'created_at' => Helper::getDate($v->created_at)
                            ];

                            if($type == 'Customer'){
                                $cr = $v->caregiver;
                                if($cr){
                                    $caregiversList[] = [
                                        'id' => $cr->id,
                                        'employee_id' => Helper::str($cr->employee_id),
                                        'first_name' => Helper::str($cr->first_name),
                                        'middle_name' => Helper::str($cr->middle_name),
                                        'last_name' => Helper::str($cr->last_name),
                                        'date_of_birth' => isset($cr->date_of_birth)?Helper::getDate($cr->date_of_birth):'',
                                        'gender' => Helper::str($cr->gender),
                                        'mobile_number' => Helper::str($c->mobile_number),
                                        'email' => Helper::str($c->email),
                                        'current_address' => Helper::str($cr->current_address),
                                        'current_city' => Helper::str($cr->current_city),
                                        'current_state' => Helper::str($cr->current_state),
                                        'current_country' => Helper::str($cr->current_country),
                                        'profile_image' => asset('uploads/caregivers/'.Helper::str($cr->profile_image)),
                                        'work_status' => Helper::str($cr->work_status),
                                        'specialization' => isset($cr->professional->specialization)?Helper::str($cr->professional->specializations->specialization_name):'-',
                    'specialization_id' => isset($c->professional)?Helper::str($c->professional->specialization):0,
                                        'qualification' => Helper::str($cr->professional->qualification),
                                        'experience' => Helper::str($cr->professional->experience),
                                        'languages_known' => Helper::str($cr->professional->languages_known),
                                        'achievements' => Helper::str($cr->professional->achievements),
                                        'college_name' => Helper::str($cr->professional->college_name),
                                    ];
                                }
                            }
                        }
                    }

                    // Load Comments, If any
                    if(isset($c->comment)){
                        foreach($c->comment as $comment){
                            $commentsLists[] = [
                                'id' => $comment->id,
                                'careplan_id' => Helper::str($comment->careplan_id),
                                'user_id' => Helper::str($comment->user_id),
                                'user_type' => Helper::str($comment->user_type),
                                'user_name' => Helper::str($comment->user_name),
                                'comment' => Helper::str($comment->comment),
                                'created_at' => Helper::getDate($comment->created_at)
                            ];
                        }
                    }

                    // Load Dispositions, If any
                    if(isset($c->disposition)){
                        foreach($c->disposition as $d){
                            $dispositionsLists[] = [
                                'id' => $d->id,
                                'careplan_id' => Helper::str($d->careplan_id),
                                'user_id' => Helper::str($d->user_id),
                                'user_name' => isset($d->user)?Helper::str($d->user->full_name):'-',
                                'status' => Helper::str($d->status),
                                'disposition_date' => Helper::getDate($d->disposition_date, true),
                                'comment' => Helper::str($d->comment),
                                'created_at' => Helper::getDate($d->created_at)
                            ];
                        }
                    }
                }

                $results['cases'] = $casesList;
                $results['patients'] = $patients;
                $results['caregivers'] = $caregiversList;
                $results['visits'] = $visitsLists;
                $results['assessments'] = $assessments;
                $results['worklogs'] = $worklogs;
                $results['feedbacks'] = $feedbacks;
                $results['comments'] = $commentsLists;
                $results['dispositions'] = $dispositionsLists;
            }
        }

        $data['status_code'] = 200;
        $data['result'] = $results;

        return response()->json($data, 200);
    }

    public function getCaseComments(Request $request)
    {
        $id = $request->user_id;
        $type = $request->type;
        $caregiverId = $request->caregiver_id;

        $results = false;

        if($id && $type){
            $res = Provider::find($id);
            if($res){
                switch ($type) {
                    case 'Provider':
                            $comments = Comment::get();
                            break;

                    case 'Professional':
                            $comments = Comment::select('*');
                            if($caregiverId){
                                $comments->whereUserType('Caregiver')->whereUserId($caregiverId);
                            }
                            $comments = $comments->get();
                            break;
                }

                if(count($comments)){
                    foreach($comments as $c){
                        $results[] = [
                            'id' => $c->id,
                            'careplan_id' => Helper::str($c->careplan_id),
                            'user_id' => Helper::str($c->user_id),
                            'user_type' => Helper::str($c->user_type),
                            'user_name' => Helper::str($c->user_name),
                            'comment' => Helper::str($c->comment),
                            'created_at' => Helper::getDate($c->created_at)
                        ];
                    }
                }
            }
        }

        $data['status_code'] = 200;
        $data['result'] = $results;

        return response()->json($data, 200);
    }

    public function addCaseComment(Request $request)
    {
        $commentData = $request->input('comment');
        $result = false;
        $id = 0;

        if($commentData){
            $commentData['created_at'] = date('Y-m-d H:i:s');
            $id = $commentData['id'];
            unset($commentData['id']);
            $result = Comment::withoutGlobalScopes()->insert($commentData);
        }

        $data['status_code'] = 200;
        if($id > 0 && $result)
            $data['result'] = $id;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function updateCaseDisposition(Request $request)
    {
        $dispositionData = $request->input('disposition');
        $result = false;
        $id = 0;

        if($dispositionData){
            $dispositionData['created_at'] = date('Y-m-d H:i:s');
            $id = $dispositionData['id'];
            unset($dispositionData['id']);
            $result = CaseDisposition::withoutGlobalScopes()->insert($dispositionData);
        }

        $data['status_code'] = 200;
        if($id > 0 && $result)
            $data['result'] = $id;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function addAssessment(Request $request)
    {
        $assessmentData = $request->input('assessment');
        $result = false;
        $id = 0;

        if($assessmentData){
            $assessmentData['created_at'] = date('Y-m-d H:i:s');
            $id = $assessmentData['id'];
            unset($assessmentData['id']);
            $result = CaseForm::withoutGlobalScopes()->insert($assessmentData);
        }

        $data['status_code'] = 200;
        if($id > 0 && $result)
            $data['result'] = $id;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function saveWorklog(Request $request)
    {
        $worklogData = $request->input('worklog');
        $result = false;
        $id = 0;

        if($worklogData){
            $worklogData['created_at'] = date('Y-m-d H:i:s');
            $id = $worklogData['id'];
            unset($worklogData['id']);
            if(isset($worklogData['worklog_date'])){
                $worklog = WorkLog::withoutGlobalScopes()->whereCareplanId($worklogData['careplan_id'])->whereCaregiverId($worklogData['caregiver_id'])->whereWorklogDate($worklogData['worklog_date'])->first();
                if($worklog){
                    $worklogData['updated_at'] = date('Y-m-d H:i:s');
                    unset($worklogData['created_at']);
                    $result = $worklog->update($worklogData);
                }
                else
                    $result = WorkLog::withoutGlobalScopes()->insert($worklogData);

        $crnNumber = Careplan::withoutGlobalScopes()->whereId($worklogData['careplan_id'])->first();
        $extraData = [
                    'id' => $worklogData['careplan_id'],
                    'crn_number' => $crnNumber->crn_number,
                ];
                // Send Push Notification to Manager
                $this->sendNotificationToManager($worklogData['caregiver_id'],$extraData);
            }
        }

        $data['status_code'] = 200;
        if($id > 0 && $result)
            $data['result'] = $id;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function saveSchedule(Request $request)
    {
        $visitData = $request->input('schedule');
        $result = false;
        $id = 0;

        if($visitData){
            $visitData['created_at'] = date('Y-m-d H:i:s');
            $id = $visitData['id'];
            unset($visitData['id']);
            if(isset($visitData['start_date']) && !empty($visitData['start_date'])){
                $visitData['start_date'] = date_format(new \DateTime($visitData['start_date']), "Y-m-d");
            }

            if(isset($visitData['end_date']) && !empty($visitData['end_date'])){
                $visitData['end_date'] = date_format(new \DateTime($visitData['end_date']), "Y-m-d");
            }

            $result = Schedule::withoutGlobalScopes()->insert($visitData);

        // Update Caregiver's Work Status
            $caregvier = Caregiver::withoutGlobalScopes()->whereNull('deleted_at')->whereId($visitData['caregiver_id'])->first();
            if($caregvier){
                $caregvier->work_status = 'On Duty';
                $caregvier->save();
            }
        }

        $data['status_code'] = 200;
        if($id > 0 && $result)
            $data['result'] = $id;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function updateVisitCaregiver(Request $request)
    {
        $careplanID = $request->careplan_id;
        $visitID = $request->visit_id;
        $caregiverID = $request->caregiver_id;
        $id = 0;

        if($careplanID && $visitID && $caregiverID){
            $visit = Schedule::withoutGlobalScopes()->whereCareplanId($careplanID)->whereId($visitID)->first();
            if($visit){
                $visit->caregiver_id = $caregiverID;
                $visit->save();

        $id = $visit->id;

        // Send Notification to Mobile
                $this->sendNotificationToCaregiver($visit['caregiver_id']);
            }
        }

        $data['status_code'] = 200;
        if($id > 0)
            $data['result'] = $id;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function getDetailsById(Request $request)
    {
        $type = $request->type;
        $crnNumber = $request->crn_number;
        $result = [];

        if($type && $crnNumber != ''){
            $case = Careplan::withoutGlobalScopes()->whereNull('deleted_at')->where('crn_number',$crnNumber)->first();

            switch($type){
                case 'Task': $worklog = WorkLog::withoutGlobalScopes()->whereNull('deleted_at')->whereCareplanId($case->id)->get();
                 if(isset($worklog)){
                                foreach ($worklog as $w) {
                                    $result[] = [
                                        'id' => Helper::str($w->id),
                                        'tenant_id' => $w->tenant_id,
                                        'careplan_id' => Helper::str($w->careplan_id),
                                        'caregiver_id' => Helper::str($w->caregiver_id),
                                        'caregiver_name' => isset($w->caregiver)?(Helper::str($w->caregiver->first_name).' '.Helper::str($w->caregiver->middle_name).' '.Helper::str($w->caregiver->last_name)):'',
                                        'worklog_date' => Helper::str($w->worklog_date),
                                        'vitals' => Helper::str($w->vitals),
                                        'routines' => Helper::str($w->routines),
                                        'created_at' => Helper::getDate($w->created_at),
                                    ];
                                }
                             }
                             break;

                 case 'Comment': $comments = Comment::withoutGlobalScopes()->whereCareplanId($case->id)->get();
                                 if(isset($comments)){
                                     foreach ($comments as $comment) {
                                         $result[] = [
                                             'id' => $comment->id,
                                             'careplan_id' => Helper::str($comment->careplan_id),
                                             'user_id' => Helper::str($comment->user_id),
                                             'user_type' => Helper::str($comment->user_type),
                                             'user_name' => Helper::str($comment->user_name),
                                             'comment' => Helper::str($comment->comment),
                                             'created_at' => Helper::getDate($comment->created_at)
                                         ];
                                     }
                                 }
                              break;

        case 'Disposition': $dispositions = Disposition::withoutGlobalScopes()->whereCareplanId($case->id)->get();
                                   if(isset($dispositions)){
                                       foreach ($dispositions as $d) {
                                           $result[] = [
                                               'id' => $d->id,
                                               'careplan_id' => Helper::str($d->careplan_id),
                                               'user_id' => Helper::str($d->user_id),
                                               'user_name' => isset($d->user)?Helper::str($d->user->full_name):'-',
                                               'status' => Helper::str($d->status),
                                               'disposition_date' => Helper::getDate($d->disposition_date, true),
                                               'comment' => Helper::str($d->comment),
                                               'created_at' => Helper::getDate($d->created_at)
                                           ];
                                       }
                                   }
                                break;

                 case 'Schedule': $schedules = Schedule::withoutGlobalScopes()->whereCareplanId($case->id)->get();
                                  if(isset($schedules)){
                                      foreach ($schedules as $v) {
                                          $result[] = [
                                              'id' => $v->id,
                                              'tenant_id' => $v->tenant_id,
                                              'careplan_id' => Helper::str($v->careplan_id),
                                              'patient_id' => Helper::str($v->patient_id),
                                              'caregiver_id' => Helper::str($v->caregiver_id),
                                              'caregiver_name' => isset($v->caregiver)?(Helper::str($v->caregiver->first_name)." ".Helper::str($v->caregiver->middle_name)." ".Helper::str($v->caregiver->last_name)):'',
                                              'notes' => isset($v->notes)?Helper::str($v->notes):'',
                                              'start_date' => isset($v->start_date)?Helper::getDate($v->start_date):'',
                                              'end_date' => isset($v->end_date)?Helper::getDate($v->end_date):'',
                                              'status' => Helper::str($v->status),
                                              'created_at' => Helper::getDate($v->created_at)
                                          ];
                                      }
                                  }
                               break;
            }
        }

        $data['status_code'] = 200;
        if($result)
            $data['result']['data'] = $result;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    // Send Notification to Mobile
    public function sendNotificationToCaregiver($caregiverID)
    {
        if($caregiverID){
            $user = User::withoutGlobalScopes()->whereNull('deleted_at')->whereUserId($caregiverID)->first();
            if($user){
                $token = FCMToken::withoutGlobalScopes()->whereUserId($user->id)->whereUserType('Caregiver')->orderBy('created_at','Desc')->orderBy('updated_at','Desc')->first();
                if($token){
                    $pdata = [
                        'type' => 'notification',
                        'message' => 'You have been assigned a new case',
                        'category' => 'Case',
                    ];

                    $response = \App\Helpers\PushMessaging::send($token->token, 'New Case Assigned','You have been assigned a new case.',$pdata);
                }
            }
        }
    }

    public function sendNotificationToManager($caregiverID, $extra)
    {
        if($caregiverID){
            $user = User::withoutGlobalScopes()->whereNull('deleted_at')->whereUserId($caregiverID)->first();
            if($user){
                $caregiverName = $user->full_name;
                $managerIDs = User::withoutGlobalScopes()->whereNull('deleted_at')
                                    ->whereTenantId($user->tenant_id)
                                    ->whereUserType('Provider')
                                    ->get()
                                    ->pluck('id')->toArray();
                if($managerIDs){
                    $tokens = FCMToken::withoutGlobalScopes()->whereIn('user_id',$managerIDs)->whereUserType('Provider')->get()->pluck('token')->toArray();
                    if($tokens){
                        $pdata = [
                            'type' => 'notification',
                            'message' => $caregiverName.' has updated the daily tasks and routines.',
                            'category' => 'Task',
                'lead_id' => $extra['id'],
                'crn_number' => $extra['crn_number']
                        ];

                        $response = \App\Helpers\PushMessaging::send($tokens, 'Caregiver Tasks',$caregiverName.' has updated the daily tasks and routines.',$pdata);
                    }
                }
            }
        }
    }
}
