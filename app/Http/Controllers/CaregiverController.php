<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\DataTables\EmployeesDataTable;

use App\Helpers\Helper;
use App\Entities\Profile;
use App\Entities\Leaves;
use App\Entities\Attendance;
use App\Entities\Schedule;

use App\Http\Requests\StoreCaregiver;

use App\Entities\Console\Subscriber;

use App\Entities\Corporates\CaregiverLog;

use App\Entities\Caregiver\Caregiver;
use App\Entities\Caregiver\CaregiverSkill;
use App\Entities\Caregiver\ProfessionalDetails;
use App\Entities\Caregiver\AccountDetails;
use App\Entities\Caregiver\PayrollDetails;
use App\Entities\Caregiver\AccommodationDetails;
use App\Entities\Caregiver\Verification;
use App\Entities\Caregiver\WorkLogFeedback;

use App\Entities\Patient;

use App\Entities\Masters\Department;
use App\Entities\Masters\Designation;
use App\Entities\Masters\Language;
use App\Entities\Masters\Skill;
use App\Entities\Masters\Branch;
use App\Entities\Masters\Specialization;
use App\Entities\Masters\Agency;

use App\Entities\User;
use App\Entities\Role;

use Validator;
use Carbon\Carbon;
use Hash;

//use SimpleExcel\SimpleExcel;

class CaregiverController extends Controller
{

    public function index(EmployeesDataTable $dataTable)
    {
        return $dataTable->render('caregivers.datatable-index');
    }

    public function checkemail(Request $request)
    {
        // connecting to corporate db
        \Config::set('database.connections.tenant.database',\Helper::encryptor('decrypt',session('corpDbName')));

        $email= $request->user_email;
        \DB::setDefaultConnection('tenant');
        $checkcaregiver = CaregiverLog::where('email',$email)->count();
        \DB::disconnect('tenant');

        if($checkcaregiver){
            echo "Email Already Exist. Please Choose a unique email id.";
        }else{
            if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
                echo "OK";
            }else{
                echo "Enter a valid email id.";
            }
        }
        exit();
    }

    public function checkEmpID(Request $request)
    {
        \DB::transaction(function() use ($request){
            $eid= $request->employee_id;
            $cid= $request->currEmpId;

            \DB::disconnect('tenant');
            // connecting to corporate db
            \Config::set('database.connections.tenant.database',\Helper::encryptor('decrypt',session('corpDbName')));
            \DB::setDefaultConnection('tenant');
            $checkEmpID = CaregiverLog::where('employee_id',$eid)->count();

            if($checkEmpID && $eid != $cid){
                echo "Employee Id already Taken. Please Choose a unique id.";
            }else{
                CaregiverLog::where('employee_id',$cid)->update(['employee_id' => $eid]);
                \DB::disconnect('tenant');

                // connecting to centralconsole db
                $subscribers = Subscriber::select('database_name')->whereCorpId(\Helper::encryptor('decrypt',session('corp_id')))->get();

                // connecting to all branch db
                foreach ($subscribers as $subscriber) {
                    \Config::set('database.connections.tenant.database',$subscriber->database_name);
                    \DB::setDefaultConnection('tenant');
                    Caregiver::where('employee_id',$cid)->update(['employee_id' => $eid]);
                    \DB::disconnect('tenant');
                }

                echo "Updated Successfully";
            }
        });
        exit();
    }

    public function create()
    {
        $data['departments'] = Department::get();
        $data['designations'] = Designation::get();
        $data['languages'] = Language::sortByAsc()->get();
        if(session('orgType') == 'Physiotherapy'){
            $data['specializations'] = Specialization::where('specialization_type','PHYSIOTHERAPY')->get();
        }else{
            $data['specializations'] = Specialization::get();
        }
        $data['managers'] = Caregiver::whereHas('professional', function($q){
            $q->where('role','Manager');
        })->get();
        if(session('orgType') == 'Physiotherapy'){
            $data['skills'] = Skill::where('skill_type','PHYSIOTHERAPY')->get();
        }else{
            $data['skills'] = Skill::get();
        }
        $data['branches'] = Branch::get();
        $data['roles'] = Role::where('name','!=','admin')->where('id','!=',1)->get();
        $data['uploaded_document_categories'] = [];
        $data['emp_id_count'] = Caregiver::count()+1;
        return view('caregivers.form', $data);
    }

    public function store(StoreCaregiver $request)
    {
        \DB::transaction(function() use ($request){
            $users = User::where('status','1')->count();
            \Config::set('database.default','central');
            $subscriber = Subscriber::whereId(\Helper::getTenantID())->first();
            \DB::setDefaultConnection('tenant');

            $empID = 0;

            $caregiver_details = $request->all('first_name','middle_name','last_name','date_of_birth','gender','blood_group','marital_status','religion','food_habits','gender_preference','mobile_number','alternate_number','email','personal_email','current_address','current_area','current_city','current_zipcode','current_state','current_country','latitude','longitude','permanent_address','permanent_city','permanent_zipcode','permanent_state','permanent_country');

            if($caregiver_details){
                $bd = self::saveBasicDetails($caregiver_details, 0);
                $empID = $bd['empID'];
                $employeeId = $bd['employeeId'];
            }

            $user = new User;
            $user->tenant_id = \Helper::getTenantID();
            $user->user_type = 'Caregiver';
            $user->created_by = 'Custom';
            $user->user_id = $empID;
            $user->branch_id = $request->branch_id;
            if( $subscriber->users_limit <= $users ){
                $user->status = 0;
            }
            $user->role_id = $request->access_role;
            $user->full_name = $request->first_name.' '.$request->middle_name.' '.$request->last_name;
            $user->email = $request->email;
            $user->password = $request->password;
            if($request->employment_status != '' && $request->employment_status != null){
                $user->status = $request->employment_status;
            }

            $user->save();
            $user->attachRole($request->access_role);

            if($empID > 0)
            {
                if($request->hasFile('profile_picture'))
                {
                    $profile = $request->file('profile_picture');
                    $extension = $profile->extension();

                    $path = public_path('uploads/provider/'.session('tenant_id').'/caregivers/'.\Helper::encryptor('encrypt',$empID));
                    if(!is_dir($path)){
                       \File::makeDirectory($path, 0777, true);
                    }

                   $fileName = \Helper::encryptor('encrypt',$empID).'.'.$extension;

                   $res = $profile->move($path, $fileName);

                   Caregiver::find($empID)->update([
                    'profile_image' => $fileName
                    ]);
                }

                $professional_details = $request->all('specialization','qualification','experience_level','college_name','experience','languages_known','workdays','work_from_time','work_to_time','achievements','specialization','branch_id','role','deployable','designation','department','manager','source','source_name','employment_type','date_of_joining','resignation_date','resignation_type','resignation_reason','uniform_issued');
                if($professional_details)
                {
                   $id = $request->professional_details_id;
                   $professional_details['caregiver_id'] = $empID;

                   self::saveProfessionalDetails($professional_details, $id);
                }

               $skill_details = $request->all('skills');
               if($skill_details && count($skill_details))
               {
                    $skill_data = [];
                    foreach($skill_details as $s){
                        if(!empty($s)){
                            foreach(json_decode($s,true) as $skill){
                                if($skill){
                                    $skill_data[$skill['skill_id']] = ['tenant_id' => \Helper::getTenantID(),'caregiver_id' => $empID, 'proficiency' => $skill['proficiency']];
                                }
                            }
                        }
                    }
                    $emp = Caregiver::find($empID);
                    $emp->caregiver_skills()->sync($skill_data);
               }

               $employment_details = $request->all('employments');
                if($employment_details && count($employment_details)){
                    $employments_data = [];
                    foreach($employment_details as $s){
                        if(!empty($s)){
                            foreach(json_decode($s,true) as $employment){
                                if($employment){
                                    \App\Entities\Caregiver\EmploymentHistory::updateOrCreate(['tenant_id' => \Helper::getTenantID(),'caregiver_id' => $empID, 'company_name' => $employment['company_name'], 'period' => $employment['period'], 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
                                }
                            }
                        }
                    }
                }

                $account_details = $request->all('pan_number','aadhar_number','account_name','account_number','bank_name','bank_branch','ifsc_code');
                if($account_details)
                {
                   $id = $request->account_details_id;
                   $account_details['caregiver_id'] = $empID;

                   self::saveAccountDetails($account_details, $id);
                }

                 $payroll_details = $request->all('basic_salary','hra','conveyance','food_and_accommodation','medical_allowance','lta','other_allowance','stipend','pf_deduction','esi_deduction','other_deduction','effective_date_of_salary');
                if($payroll_details)
                {
                     $id = $request->payroll_details_id;
                     $payroll_details['caregiver_id'] = $empID;

                     self::savePayrollDetails($payroll_details, $id);
                }

                 $accommodation_details = $request->all('arranged_by','accommodation_address','accommodation_city','accommodation_state','accommodation_from_date','accommodation_to_date');
                if($accommodation_details)
                {
                    $id = $request->accommodation_details_id;
                    $accommodation_details['caregiver_id'] = $empID;

                    self::saveAccommodationDetails($accommodation_details, $id);
                }

                 if($request->uploaded_doc_ids)
                {
                    $ids = explode(",",$request->uploaded_doc_ids);
                    if(count($ids))
                    {
                        $path = \Helper::encryptor('encrypt',$empID);
                        if(!is_dir($path))
                        {
                             \File::makeDirectory(public_path("uploads/provider/".session('tenant_id')."/caregivers/".$path."/document"."/"),0777,true);
                        }
                        foreach ($ids as $v)
                        {
                            $verification = Verification::whereId($v)->first();
                            if($verification)
                            {
                                $verification->caregiver_id = $empID;
                                $verification->save();

                                $content = public_path("uploads/provider/".session('tenant_id')."/caregivers/".$path."/temp/".$verification->doc_path);
                                $movepath = public_path("uploads/provider/".session('tenant_id')."/caregivers/".$path."/document"."/".$verification->doc_path);
                                \File::move($content, $movepath);


                                $document_without_ext = substr($verification->doc_path, 0, strrpos($verification->doc_path, "."));
                                $content_mobile = public_path("uploads/provider/".session('tenant_id')."/caregivers/".$path."/tempmobile/".$document_without_ext.".jpg");
                                $mobile_path = public_path("uploads/provider/".session('tenant_id')."/caregivers/".$path."/mobile");

                                if(!is_dir($mobile_path))
                                {
                                    \File::makeDirectory($mobile_path, 0777, true);
                                }

                                $movepath_mobile = public_path("uploads/provider/".session('tenant_id')."/caregivers/".$path."/mobile/".$document_without_ext.".jpg");
                                \File::move($content_mobile, $movepath_mobile);
                            }
                        }
                    }
                }
            }
            \DB::disconnect('tenant');

            // connecting to corporate db
            \Config::set('database.connections.tenant.database',\Helper::encryptor('decrypt',session('corpDbName')));
            \DB::setDefaultConnection('tenant');
            $caregiverLog = new CaregiverLog;
            $caregiverLog->employee_id = $employeeId;
            $caregiverLog->email = $request->email;
            $caregiverLog->save();
            \DB::disconnect('tenant');
        });
        return redirect()->route('employee.index')->with('alert_success','Employee added Successfully');
    }

    public function edit($id)
    {        
        $data['departments'] = Department::get();
        $data['designations'] = Designation::get();
        $data['languages'] = Language::sortByAsc()->get();
        $data['specializations'] = Specialization::get();
        $data['managers'] = Caregiver::whereHas('professional', function($q){
            $q->where('role','Manager');
        })->whereNotIn('id',[Helper::encryptor('decrypt',$id)])->get();
        $data['skills'] = Skill::get();
        $data['branches'] = Branch::get();
        $data['agencies'] = Agency::get();
        $data['e'] = Caregiver::find(Helper::encryptor('decrypt',$id));
        $data['caregiver_skills'] = [];
        if($data['e']){
            $data['caregiver_skills'] = CaregiverSkill::whereCaregiverId($data['e']->id)->get();
            $data['uploaded_document_categories'] = $data['e']->verifications->pluck('doc_type')->unique()->toArray();
        }        
        
        return view('caregivers.form', $data);
    }

    public function update(StoreCaregiver $request, $id)
    {
        \DB::transaction(function() use ($request, $id){
            $empID = Helper::encryptor('decrypt',$id);

            // Basic Details
            $caregiver_details = $request->all('first_name','middle_name','last_name','date_of_birth','gender','blood_group','marital_status','religion','food_habits','gender_preference','mobile_number','alternate_number','email','personal_email','current_address','current_area','current_city','current_zipcode','current_state','current_country','latitude','longitude','permanent_address','permanent_city','permanent_zipcode','permanent_state','permanent_country');

            if($caregiver_details){
                $bd = self::saveBasicDetails($caregiver_details, Helper::encryptor('decrypt',$id));
                $empID = $bd['empID'];
            }

            $user = User::where('user_id',Helper::encryptor('decrypt',$id))
                        ->where('tenant_id',Helper::getTenantID())
                        ->first();
            if($user){
                $user->tenant_id = Helper::getTenantID();
                $user->user_type = 'Caregiver';
                $user->created_by = 'Custom';
                $user->user_id = Helper::encryptor('decrypt',$id);
                $user->full_name = $request->first_name.' '.$request->middle_name.' '.$request->last_name;
                $user->email = $request->email;
                if($request->password != '' || $request->password != null){
                    $user->password = $request->password;
                }
                if($request->employment_status != '' && $request->employment_status != null){
                    $user->status = $request->employment_status;
                }
                $user->save();
            }

            if($empID > 0){
                    // Upload Profile Picture
                if($request->hasFile('profile_picture')){
                    $profile = $request->file('profile_picture');
                    $extension = $profile->extension();

                    $path = public_path('uploads/provider/'.session('tenant_id').'/caregivers/'.$id);
                    if(!is_dir($path)){
                        \File::makeDirectory($path, 0777, true);
                    }

                    $fileName = Helper::encryptor('encrypt',$empID).'.'.$extension;

                    $res = $profile->move($path, $fileName);

                        // Update File Name to DB
                    Caregiver::find($empID)->update([
                        'profile_image' => $fileName
                        ]);
                }

                // Professional Details
                $professional_details =
                $request->all('specialization','qualification','experience_level','college_name','experience','languages_known','workdays','work_from_time','work_to_time','achievements','specialization','branch_id','role','deployable','designation','department','manager','source','source_name','employment_type','date_of_joining','resignation_date','resignation_type','resignation_reason','uniform_issued');

                if($professional_details){
                    $id = Helper::encryptor('decrypt',$request->professional_details_id);
                    $professional_details['caregiver_id'] = $empID;

                    self::saveProfessionalDetails($professional_details, $id);
                }

                // Caregiver Skill Details
                $skill_details = $request->all('skills');
                if($skill_details && count($skill_details)){
                    $skill_data = [];
                    foreach($skill_details as $s){
                        if(!empty($s)){
                            foreach(json_decode($s,true) as $skill){
                                if($skill){
                                    $skill_data[$skill['skill_id']] = ['tenant_id' => \Helper::getTenantID(),'caregiver_id' => $empID, 'proficiency' => $skill['proficiency']];
                                }
                            }
                        }
                    }
                    $emp = Caregiver::find($empID);
                    $emp->caregiver_skills()->sync($skill_data);
                }

                // Caregiver Previuos Employment Details
                $employment_details = $request->all('employments');
                if($employment_details && count($employment_details)){
                    $employments_data = [];
                    foreach($employment_details as $s){
                        if(!empty($s)){
                            foreach(json_decode($s,true) as $employment){
                                if($employment){
                                    \App\Entities\Caregiver\EmploymentHistory::updateOrCreate(['tenant_id' => \Helper::getTenantID(),'caregiver_id' => $empID, 'company_name' => $employment['company_name'], 'period' => $employment['period']]);
                                }
                            }
                        }
                    }
                }

                // Bank Account Details
                $account_details = $request->all('pan_number','aadhar_number','account_name','account_number','bank_name','bank_branch','ifsc_code');
                if($account_details){
                    $id = Helper::encryptor('decrypt',$request->account_details_id);
                    $account_details['caregiver_id'] = $empID;

                    self::saveAccountDetails($account_details, $id);
                }

                // Salary Details
                $payroll_details = $request->all('basic_salary','hra','conveyance','food_and_accommodation','medical_allowance','lta','other_allowance','stipend','pf_deduction','esi_deduction','other_deduction','effective_date_of_salary');
                if($payroll_details){
                    $id = Helper::encryptor('decrypt',$request->payroll_details_id);
                    $payroll_details['caregiver_id'] = $empID;

                    self::savePayrollDetails($payroll_details, $id);
                }

                // Accommodation Details
                $accommodation_details = $request->all('arranged_by','accommodation_address','accommodation_city','accommodation_state','accommodation_from_date','accommodation_to_date');
                if($accommodation_details){
                    $id = Helper::encryptor('decrypt',$request->accommodation_details_id);
                    $accommodation_details['caregiver_id'] = $empID;

                    self::saveAccommodationDetails($accommodation_details, $id);
                }
            }
        });
        return back()->with('alert_success','Employee updated Successfully');
    }

    public function updateStatus(Request $request)
    {
        $id = Helper::encryptor('decrypt',$request->id);
        if($id){
            $res = Caregiver::find($id)->update(['work_status' => $request->status]);
        }

        return response()->json($res,200);
    }

    public function updateAvailability(Request $request)
    {
        $id = Helper::encryptor('decrypt',$request->id);
        if($id){
            $res = Caregiver::find($id)->update(['availability_status' => $request->status]);
        }

        return response()->json($res,200);
    }

    public function import()
    {
        $caregivers = Caregiver::where('imported',0)->get();

        return view('caregivers.import', compact('caregivers'));
    }    

    public function bulkImport(Request $request)
    {
        if($request->isMethod('post'))
        {
            $count = 0;
            $file = $request->file('excel_file');
            $path = $file->getRealPath();

            $reader = \Excel::selectSheetsByIndex(0)->load($path, function($reader) {});


            $data = $reader->get();

            if(!empty($data) && $data->count())
            {
                $data = $data->toArray();
                $count = count($data);
                foreach ($data as $key => $value)
                {
                	if(count($value))
                	{
                		$users = User::where('status','1')->count();
                		\Config::set('database.default','central');
                		$subscriber = Subscriber::whereId(\Helper::getTenantID())->first();
                		\DB::setDefaultConnection('tenant');

                		$caregiver = new Caregiver;
                		if(isset($value['dob']) && !empty($value['dob'])){
                			$dob = Carbon::parse($value['dob'])->format('Y-m-d');
                		}
                		else
                			$dob = null;

                		$caregiver->tenant_id = \Helper::getTenantID();
                        $caregiver->branch_id = 1;
                		$caregiver->employee_id = \Helper::getSetting('employee_id_prefix').(Caregiver::count()+1);
                		$caregiver->first_name = $value['first_name'];
                        $caregiver->middle_name = $value['middle_name'];
                		$caregiver->last_name = $value['last_name'];
                		$caregiver->date_of_birth = $dob;
                		$caregiver->gender = $value['gender'];
                		$caregiver->marital_status = $value['marital_status'];
                        $caregiver->blood_group = $value['blood_group'];
                		$caregiver->mobile_number = sprintf("%.0f", $value['mobile_number']);
                		$caregiver->email = trim($value['email']);
                		$caregiver->current_address = $value['current_address'];
                		$caregiver->current_city = $value['current_city'];
                		$caregiver->current_state = $value['current_state'];
                        $caregiver->current_country = 'India';
                		$caregiver->permanent_address = $value['permanent_address'];
                		$caregiver->permanent_city = $value['permanent_city'];
                		$caregiver->permanent_state = $value['permanent_state'];
                        $caregiver->permanent_country = 'India';
                		$caregiver->imported = 1;

                		$caregiver->save();

                		$user = new User;
                        $user->tenant_id = \Helper::getTenantID();
                        $user->user_type = 'Caregiver';
                        $user->branch_id = 1;
                		$user->created_by = 'Custom';
                		$user->user_id = $caregiver->id;
                		if( $subscriber->users_limit <= $users ){
                			$user->status = 0;
                		}
                		$user->role_id = 1;
                		$user->full_name = $value['first_name'].' '.$value['middle_name'].' '.$value['last_name'];
                		$user->email = trim($value['email']);
                		$user->password = $value['password'];

                		$user->save();

                        // Add Professional Details
                		if(isset($value['doj']) && !empty($value['doj']))
                			$doj = date_format(new \DateTime($value['doj']),'Y-m-d');
                		else
                			$doj = null;
                        
                		$details = new ProfessionalDetails;
                		$details->caregiver_id = $caregiver->id;
                        $details->branch_id = 1;
                		$details->qualification = $value['qualification'];
                		$details->experience = $value['experience'];
                		$details->languages_known = str_replace(' ','',$value['languages_known']);
                		$details->role = $value['role'];
                        $details->date_of_joining = $doj;

                		$details->save();

                        // // Add Payroll Details
                        // $odetails = new PayrollDetails;
                        // if(isset($value['effective_date_of_salary']) && !empty($value['effective_date_of_salary']))
                        //     $eds = date_format(new \DateTime($value['effective_date_of_salary']),'Y-m-d');
                        // else
                        //     $eds = null;
                        // $odetails->caregiver_id = $caregiver->id;
                        // $odetails->basic_salary = floatval($value['basic_salary']);
                        // $odetails->hra = floatval($value['hra']);
                        // $odetails->conveyance = floatval($value['conveyance']);
                        // $odetails->food_and_accommodation = floatval($value['food_and_accommodation']);
                        // $odetails->medical_allowance = floatval($value['medical_allowance']);
                        // $odetails->lta = floatval($value['lta']);
                        // $odetails->other_allowance = floatval($value['other_allowance']);
                        // $odetails->stipend = floatval($value['stipend']);
                        // $odetails->pf_deduction = floatval($value['pf_deduction']);
                        // $odetails->esi_deduction = floatval($value['esi_deduction']);
                        // $odetails->other_deduction = floatval($value['other_deduction']);
                        // $odetails->effective_date_of_salary = $value['effective_date_of_salary'];
                        // $odetails->save();

                                // Add Account Details
                        // $adetails = new AccountDetails;
                        // $adetails->caregiver_id = $caregiver->id;
                        // $adetails->pan_number = $value['pan_number'];
                        // $adetails->aadhar_number = $value['aadhar_number'];
                        // $adetails->account_name = $value['first_name'].' '.$value['middle_name'].' '.$value['last_name'];
                        // $adetails->account_number = $value['account_number'];
                        // $adetails->bank_name = $value['bank_name'];
                        // $adetails->bank_branch = $value['branch_name'];
                        // $adetails->ifsc_code = $value['ifsc_code'];
                        // $adetails->save();

                                // Add Accommodation Details
                        if(isset($value['accommodation_from_date']) && !empty($value['accommodation_from_date'])){
                            $afd = date_format(new \DateTime($value['accommodation_from_date']),'Y-m-d');
                        }
                        else{
                            $afd = null;
                        }

                        if(isset($value['accommodation_to_date']) && !empty($value['accommodation_to_date'])){
                            $atd = date_format(new \DateTime($value['accommodation_to_date']),'Y-m-d');
                        }
                        else{
                            $atd = null;
                        }

                        $adetails = new AccommodationDetails;
                        $adetails->caregiver_id = $caregiver->id;
                        $adetails->arranged_by = $value['accommodation_arranged_by'];
                        $adetails->accommodation_address = $value['accommodation_address'];
                        $adetails->accommodation_city = $value['accommodation_city'];
                        $adetails->accommodation_state = $value['accommodation_state'];
                        $adetails->accommodation_from_date = $afd;
                        $adetails->accommodation_to_date = $atd;
                        $adetails->save();
                	}
                }
            }
            return back()->with('alert_success',$count.' caregiver(s) has been successfully imported.');
        }

        return back();
    }

    public function destroy($id)
    {
        $e = Caregiver::find(Helper::encryptor('decrypt',$id));
        if($e){
                    // Delete Professional Details
            $e->professional->delete();
                    // Delete Account Details
            $e->account->delete();
                    // Delete Payroll Details
            $e->payroll->delete();
                    // Delete Accommodation Details
            $e->accommodation->delete();

            $e->delete();
        }

        return back()->with('alert_info','Caregiver deleted successfully');
    }

    static function saveBasicDetails($data, $id)
    {
        $empID = $id;

        if($data){
            if(isset($id) && $id > 0){
                $caregiver = Caregiver::find($id);
                $empID = $id;
            }else{
                $caregiver = new Caregiver;
            }

            if(isset($data['date_of_birth']) && !empty($data['date_of_birth']))
                $dob = date_format(new \DateTime($data['date_of_birth']),'Y-m-d');
            else
                $dob = null;

            $caregiver->tenant_id = \Helper::getTenantID();

            if($id == 0){                
                \DB::disconnect('tenant');
                // connecting to corporate db
                \Config::set('database.connections.tenant.database',\Helper::encryptor('decrypt',session('corpDbName')));
                \DB::setDefaultConnection('tenant');
                $caregiver_count = CaregiverLog::count();
                \DB::disconnect('tenant');
                
                // connecting to branch db
                \Config::set('database.connections.tenant.database',\Helper::encryptor('decrypt',session('dbName')));
                \DB::setDefaultConnection('tenant');

                do{
                    $caregiver->employee_id = \Helper::getSetting('employee_id_prefix').($caregiver_count+1);
                    \DB::disconnect('tenant');

                    // connecting to corporate db to check for duplicate employee_id
                    \Config::set('database.connections.tenant.database',\Helper::encryptor('decrypt',session('corpDbName')));
                    \DB::setDefaultConnection('tenant');
                    $checkEmpID = CaregiverLog::where('employee_id',$caregiver->employee_id)->count();
                    \DB::disconnect('tenant');

                    // connecting to branch db
                    \Config::set('database.connections.tenant.database',\Helper::encryptor('decrypt',session('dbName')));
                    \DB::setDefaultConnection('tenant');

                    $caregiver_count++;
                }while($checkEmpID);

                $caregiver->work_status = 'Available';
            }
            $caregiver->first_name = $data['first_name'];
            $caregiver->middle_name = $data['middle_name'];
            $caregiver->last_name = $data['last_name'];
            $caregiver->date_of_birth = $dob;
            $caregiver->gender = $data['gender'];
            $caregiver->blood_group = $data['blood_group'];
            $caregiver->marital_status = $data['marital_status'];
            $caregiver->religion = $data['religion'];
            $caregiver->food_habits = $data['food_habits'];
            $caregiver->gender_preference = $data['gender_preference'];
            $caregiver->mobile_number = $data['mobile_number'];
            $caregiver->alternate_number = empty($data['alternate_number'])?null:$data['alternate_number'];
            $caregiver->email = $data['email'];
            $caregiver->personal_email = $data['personal_email'];
            $caregiver->current_address = $data['current_address'];
            $caregiver->current_area = $data['current_area'];
            $caregiver->current_city = $data['current_city'];
            $caregiver->current_zipcode = $data['current_zipcode'];
            $caregiver->current_state = $data['current_state'];
            $caregiver->current_country = $data['current_country'];
            $caregiver->latitude = $data['latitude'];
            $caregiver->longitude = $data['longitude'];
            $caregiver->permanent_address = $data['permanent_address'];
            $caregiver->permanent_city = $data['permanent_city'];
            $caregiver->permanent_zipcode = $data['permanent_zipcode'];
            $caregiver->permanent_state = $data['permanent_state'];
            $caregiver->permanent_country = $data['permanent_country'];

            $caregiver->save();

            $empID = $caregiver->id;
            $employeeId = $caregiver->employee_id;
        }

        return array('empID' => $empID, 'employeeId' => $employeeId);
    }

    static function saveProfessionalDetails($data, $id)
    {
        if($data){
            if(isset($id) && $id > 0){
                $details = ProfessionalDetails::find($id);
            }else{
                $details = new ProfessionalDetails;
            }

            if(isset($data['date_of_joining']) && !empty($data['date_of_joining']))
                $doj = date_format(new \DateTime($data['date_of_joining']),'Y-m-d');
            else
                $doj = null;
                
            if(isset($data['resignation_date']) && !empty($data['resignation_date']))
                $dor = date_format(new \DateTime($data['resignation_date']),'Y-m-d');
            else
                $dor = null;

            $details->caregiver_id = $data['caregiver_id'];
            $details->branch_id = $data['branch_id'];
            $details->specialization = $data['specialization'];
            $details->qualification = $data['qualification'];
            $details->experience_level = $data['experience_level'];
            $details->college_name = $data['college_name'];
            $details->experience = $data['experience'];
            $details->languages_known = $data['languages_known']?implode(",",$data['languages_known']):'';
            $details->working_days = $data['workdays']?implode(",",$data['workdays']):'';
            $details->achievements = $data['achievements'];
            $details->role = $data['role'];
            $details->deployable = isset($data['deployable'])?$data['deployable']:'No';
            $details->designation = $data['designation'];
            $details->department = $data['department'];
            $details->manager = $data['manager'];
            $details->source = $data['source'];
            $details->source_name = $data['source_name'];
            $details->date_of_joining = $doj;
            $details->employment_type = $data['employment_type'];            
            $details->resignation_date = $dor;
            $details->resignation_type = $data['resignation_type'];
            $details->resignation_reason = $data['resignation_reason'];
            $details->uniform_issued = $data['uniform_issued']; 

            if(isset($data['work_from_time']) && !empty($data['work_from_time']))
                $details->working_hours_from = Carbon::parse($data['work_from_time'])->format('H:i');
            else
                $details->working_hours_from = null;

            if(isset($data['work_to_time']) && !empty($data['work_to_time']))
                $details->working_hours_to = Carbon::parse($data['work_to_time'])->format('H:i');
            else
                $details->working_hours_to = null;                      

            $details->save();

            Caregiver::find($data['caregiver_id'])->update(['branch_id' => $data['branch_id']]);
            User::whereUserId($data['caregiver_id'])->update(['branch_id' => $data['branch_id']]);
        }
    }

    static function saveAccountDetails($data, $id)
    {
        if($data){
            if(isset($id) && $id > 0){
                $details = AccountDetails::find($id);
            }else{
                $details = new AccountDetails;
            }

            $details->caregiver_id = $data['caregiver_id'];
            $details->pan_number = $data['pan_number'];
            $details->aadhar_number = $data['aadhar_number'];
            $details->account_name = $data['account_name'];
            $details->account_number = $data['account_number'];
            $details->bank_name = $data['bank_name'];
            $details->bank_branch = $data['bank_branch'];
            $details->ifsc_code = $data['ifsc_code'];

            $details->save();
        }
    }

    static function savePayrollDetails($data, $id)
    {
        if($data){
            if(isset($id) && $id > 0){
                $details = PayrollDetails::find($id);
            }else{
                $details = new PayrollDetails;
            }

            if(isset($data['effective_date_of_salary']) && !empty($data['effective_date_of_salary']))
                $edos = date_format(new \DateTime($data['effective_date_of_salary']),'Y-m-d');
            else
                $edos = null;

            $details->caregiver_id = $data['caregiver_id'];
            $details->basic_salary = floatval($data['basic_salary']);
            $details->hra = floatval($data['hra']);
            $details->conveyance = floatval($data['conveyance']);
            $details->food_and_accommodation = floatval($data['food_and_accommodation']);
            $details->medical_allowance = floatval($data['medical_allowance']);
            $details->lta = floatval($data['lta']);
            $details->other_allowance = floatval($data['other_allowance']);
            $details->stipend = floatval($data['stipend']);
            $details->pf_deduction = floatval($data['pf_deduction']);
            $details->esi_deduction = floatval($data['esi_deduction']);
            $details->other_deduction = floatval($data['other_deduction']);
            $details->effective_date_of_salary = $edos;

            $details->save();
        }
    }

    static function saveAccommodationDetails($data, $id)
    {
        if($data){
            if(isset($id) && $id > 0){
                $details = AccommodationDetails::find($id);
            }else{
                $details = new AccommodationDetails;
            }

            if(isset($data['accommodation_from_date']) && !empty($data['accommodation_from_date']))
                $fdate = date_format(new \DateTime($data['accommodation_from_date']),'Y-m-d');
            else
                $fdate = null;

            if(isset($data['accommodation_to_date']) && !empty($data['accommodation_to_date']))
                $tdate = date_format(new \DateTime($data['accommodation_to_date']),'Y-m-d');
            else
                $tdate = null;

            $details->caregiver_id = $data['caregiver_id'];
            $details->arranged_by = $data['arranged_by'];
            $details->accommodation_address = $data['accommodation_address'];
            $details->accommodation_city = $data['accommodation_city'];
            $details->accommodation_state = $data['accommodation_state'];
            $details->accommodation_from_date = $fdate;
            $details->accommodation_to_date = $tdate;

            $details->save();
        }
    }

    public function tracking()
    {
        $provider = Profile::where('id',\Helper::getTenantID())->first();
        return view('trackings.tracking');
    }

    public function liveTracking()
    {

        $provider = Profile::where('id',\Helper::getTenantID())->first();
        $caregivers = Caregiver::active()
                                ->with('professional')
                                ->whereHas('user', function($q){
                                    $q->whereStatus('1')->whereUserDeleted('0');
                                })
                                ->whereHas('professional',function($role){
                                    $role->whereRole('Caregiver');  
                                });

        if(Helper::encryptor('decrypt',session('branch_id')) != 0){
            $caregivers->whereHas('professional', function($q){
                $q->whereBranchId(Helper::encryptor('decrypt',session('branch_id')));
            });
        }
        $caregivers = $caregivers->get();
        return view('trackings.live-tracking', compact('caregivers'));
    }

    public function getEmployeeLocationCoordinates(Request $request)
    {
        $provider_id = \Helper::getTenantID();
        $city = $request->city;

        $caregivers = Caregiver::whereTenantId($provider_id)->whereHas('user', function($q){
            $q->whereStatus('1')->whereUserDeleted('0');
        })->get(['id'])->toArray();

        $tracking = [];

        foreach ($caregivers as $c) {
            $temp = Tracking::join('professionals', 'tracking.professional_id', '=', 'professionals.id')
            ->where('professional_id', $p['id'])
            ->orderBy('tracking.id', 'Desc')
            ->first(['tracking.id', 'professional_id', 'lat', 'lng', 'professionals.prof_fname', 'professionals.prof_lname','professionals.prof_gender','professionals.profile_image_path','professionals.prof_mobile','professionals.prof_city','tracking.server_timestamp as timestamp']);

            if($temp){
                array_push($tracking,$temp);
            }
        }
            // $professionals = array_flatten($professionals);
            //
            // $tracking = Tracking::join('professionals', 'tracking.professional_id', '=', 'professionals.id')
            //  ->whereIn('professional_id', $professionals)
            //  ->get(['tracking.id', 'professional_id', 'lat', 'lng', 'professionals.prof_fname', 'professionals.prof_lname'])
            //  ->groupBy('professional_id');

        return response()->json($tracking,200);
    }

    /*-----------------------------------------------
    | API Functions
    |-----------------------------------------------
    |*/

    public function getCaregivers(Request $request)
    {
        $id = $request->user_id;
        $results = false;

        if($id){

            $res = \App\Entities\Console\Provider::find($id);

            if($res){
                $dbName = $res->database_name;

                \Config::set('database.connections.api.database',$dbName);
                \DB::setDefaultConnection('api');

                $caregivers = Caregiver::whereHas('professional', function($q){
                    $q->whereRole('Caregiver');
                })->get();

                if(count($caregivers)){
                    foreach($caregivers as $c){
                        $results[] = [
                        'id' => $c->id,
                        'employee_id' => Helper::str($c->employee_id),
                        'first_name' => Helper::str($c->first_name),
                        'middle_name' => Helper::str($c->middle_name),
                        'last_name' => Helper::str($c->last_name),
                        'date_of_birth' => isset($c->date_of_birth)?Helper::getDate($c->date_of_birth):'',
                        'gender' => Helper::str($c->gender),
                        'mobile_number' => Helper::str($c->mobile_number),
                        'alternate_number' => Helper::str($c->alternate_number),
                        'email' => Helper::str($c->email),
                        'personal_email' => Helper::str($c->personal_email),
                        'current_address' => Helper::str($c->current_address),
                        'current_city' => Helper::str($c->current_city),
                        'current_state' => Helper::str($c->current_state),
                        'current_country' => Helper::str($c->current_country),
                        'profile_image' => asset('uploads/caregivers/'.Helper::str($c->profile_image)),
                        'work_status' => Helper::str($c->work_status),
                        'specialization' => isset($c->professional->specialization)?Helper::str($c->professional->specializations->specialization_name):'-',
                        'qualification' => Helper::str($c->professional->qualification),
                        'experience_level' => Helper::str($c->professional->experience_level),
                        'experience' => Helper::str($c->professional->experience),
                        'languages_known' => Helper::str($c->professional->languages_known),
                        'achievements' => Helper::str($c->professional->achievements)
                        ];
                    }
                }
            }
        }

        $data['status_code'] = 200;
        $data['result'] = $results;

        return response()->json($data, 200);
    }

    public function editProfile(Request $request)
    {
        $id = \Helper::encryptor('decrypt', session('tenant_id'));

        $caregiver = Caregiver::with('professional')->first();
        $data['specializations'] = Specialization::get();
        $data['skills'] = Skill::get();
        $data['languages'] = Language::sortByAsc()->get();
        $data['timings'] = [];

        if($caregiver->professional){
            $data['timings'] = json_decode($caregiver->professional->timings);
        }

        $data['caregiver_skills'] = [];
        $data['uploaded_document_categories'] = $caregiver->verifications->pluck('doc_type')->unique()->toArray();
        if($caregiver){
            $data['caregiver_skills'] = CaregiverSkill::where('tenant_id' , $id)->get();
        }
        $result = [];

        foreach ($data['caregiver_skills'] as $skills) {
            $result[] = [
            'skill_name' => $skills->skill->skill_name,
            'proficiency' => $skills->proficiency
            ];
        }
        return view('caregivers.profilemanagement',compact('caregiver'))->with($data);
    }

    public function update_personal_details(Request $request)
    {
        // Check for Validations
        $rules = [
        "first_name"        => "required",
        "middle_name"       => "required",
        "last_name"         => "required",
        "mobile_number"     => "required",
        "email"             => "required",
        "current_city"      => "required",
        "current_state"     => "required",
        "current_address"   => "required",
        "permanent_city"    => "required",
        "permanent_state"   => "required",
        "permanent_address" => "required",
        'date_of_birth'     => 'required',
        'gender'            => 'required',
        ];

        $nicenames = [
        'first_name'        => 'First Name',
        'middle_name'       => 'Middle Name',
        'last_name'         => 'Last Name',
        'mobile_number'     => 'Mobile',
        'email'             => 'Email',
        'current_city'      => 'Current City',
        'current_state'     => 'Current State',
        'current_address'   => 'Current Address',
        'permanent_city'    => 'Permanent City',
        'permanent_state'   => 'Permanent State',
        'permanent_address' => 'Permanent Address',
        'date_of_birth'     => 'Date of Birth',
        'gender'            => 'Gender',
        ];

        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($nicenames);

        if($validator->fails()){
            return redirect()
            ->route('profilemanagement')
            ->withErrors($validator)
            ->withInput();
        }

        $input                        = $request->all();
        $user                         = User::first();
        $caregiver                    = Caregiver::first();
        $user->full_name              = $request->first_name.' '.$request->middle_name.' '.$request->last_name;
        $caregiver->first_name        = $request->first_name;
        $caregiver->middle_name       = $request->middle_name;
        $caregiver->last_name         = $request->last_name;
        $caregiver->mobile_number     = $request->mobile_number;
        $caregiver->alternate_number  = $request->alternate_number;
        $caregiver->email             = $request->email;
        $caregiver->current_area      = $request->current_area;
        $caregiver->current_city      = $request->current_city;
        $caregiver->current_state     = $request->current_state;
        $caregiver->current_address   = $request->current_address;
        $caregiver->permanent_city    = $request->permanent_city;
        $caregiver->permanent_state   = $request->permanent_state;
        $caregiver->permanent_address = $request->permanent_address;
        $caregiver->date_of_birth     = $request->date_of_birth;
        $caregiver->gender            = $request->gender;
        // Upload caregiver Profile

        if($request->hasFile('prof_pic')){
            $profile_image = $request->file('prof_pic');
            $extension = $profile_image_path->getClientOriginalExtension();
            $path = public_path('uploads/caregivers/'.$caregiver->prof_folder);
            if(!is_dir($path)){
                \File::makeDirectory($path,0755,true);
            }
            $fileName = \Helper::encryptor('encrypt',$profID).'.'.$extension;
            $res = $profile_image_path->move($path,$fileName);

            // Update caregiver Profile file path
            $caregiver->profile_image = $fileName;
        }

        if($request->curr_password || $request->new_password || $request->new_password_confirmation){
            $current_password = $request->curr_password;
            $new_password = $request->new_password;
            $confirm_new_password = $request->new_password_confirmation;
            {
                $rules = array(
                    "curr_password"             => "required",
                    "new_password"              => "required|confirmed",
                    "new_password_confirmation" => "required"
                    );
                $nicenames = array(
                    "curr_password"             => "Current Password",
                    "new_password"              => "New Password",
                    "new_password_confirmation" => "Confirm New Password"
                    );
            }

            $validator = Validator::make($request->all(), $rules);
            $validator->setAttributeNames($nicenames);
            $validator->after(function($validator) use($current_password,$new_password,$caregiver,$user){
                if(!Hash::check($current_password,$user->password)){
                    $validator->errors()->add('curr_password','Incorrect Old Password');
                }else{
                    $user->password = $new_password;
                    $user->save();
                }
            });

            if($validator->fails()){
                return redirect()
                ->route('profilemanagement')
                ->withErrors($validator)
                ->withInput();
            }
        }
        $user->save();
        $caregiver->save();

        // Change Session Values
        session()->put('first_name',$input['first_name']);
        session()->put('middle_name',$input['middle_name']);
        session()->put('last_name',$input['last_name']);
        session()->put('user_name',$user->full_name);

        session()->flash('message','Personal Details Updated');
        return redirect()->route('profilemanagement');
    }

    public function update_professional_details(Request $request)
    {
        $rules = [
        "specialization"  => "required",
        "qualification"   => "required",
        "experience_level"   => "required",
        "achievements"    => "required",
        "experience"      => "required|numeric",
        "languages_known" => "required"
        ];

        $nicenames = [
        "specialization"  => "Specialization",
        "qualification"   => "Qualification",
        "experience_level"   => "Experience Level",
        "achievements"    => "Achievements",
        "experience"      => "Experience",
        "languages_known" => "Languages Known"
        ];

        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($nicenames);

        if($validator->fails()){
            return redirect()
            ->route('profilemanagement')
            ->withErrors($validator)
            ->withInput();
        }

        $input = $request->all();
        $caregiver = Caregiver::first();
        $prof_details = ProfessionalDetails::first();
        if($prof_details == null){
            $prof_details = new ProfessionalDetails;
            $input['id'] = $caregiver->id;
        }

        $input['languages_known'] = implode(',',$request->languages_known);
        $prof_details->fill($input)->save();

        // Caregiver Skill Details
        $skill_details = $request->all('skills');
        if($skill_details && count($skill_details)){
            $skill_data = [];
            foreach($skill_details as $s){
                if(!empty($s)){
                    foreach(json_decode($s,true) as $skill){
                        if($skill){
                            $skill_data[$skill['skill_id']] = ['tenant_id' => \Helper::encryptor('decrypt', session('tenant_id')),'caregiver_id' => $caregiver->id, 'proficiency' => $skill['proficiency']];
                        }
                    }
                }
            }
            $caregiver->caregiver_skills()->sync($skill_data);
        }

        session()->flash('message','Professional Details Updated');
        return redirect()->route('profilemanagement');
    }

    public function update_timings(Request $request)
    {
        foreach(range(1,7) as $day){
            $current_day = strtolower(strftime('%A', strtotime("last saturday +$day day")));
            $days[$day - 1] = $current_day;
        }

        $rules = [];
        foreach($days as $day){
            $rules[$day.'_start'] = 'required_if:'.$day.'_status,working';
            $rules[$day.'_end'] = 'required_if:'.$day.'_status,working';
        }

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return redirect()
            ->back()
            ->withErrors($validator)
            ->withInput();
        }

        $timings = [];

        foreach($days as $day){
            $timings[$day]['status'] = $request->{$day.'_status'};
            $timings[$day]['start_time'] = "";
            $timings[$day]['end_time'] = "";

            if($timings[$day]['status'] == 'Working'){
                $timings[$day]['start_time'] = $request->{$day.'_start'};
                $timings[$day]['end_time'] = $request->{$day.'_end'};
            }
        }
        $caregiver = Caregiver::first();
        $prof_details  = ProfessionalDetails::first();
        $prof_details->timings = json_encode($timings);
        $prof_details->save();

        session()->flash('message', 'Timings Updated');
        return redirect()->route('profilemanagement');
    }

    public function update_documents(Request $request)
    {
        $document = 'verification_documents';
        $caregiverID = $request->caregiver_id;
        $document_upload = false;
        $type = $request->type;
        $data = [];

        $document_name = $document.'_document';

        if(!empty($request->$document) || !empty($request->verification_documents_comment))
        {
           if($request->file('documents')->getSize()/1024 < 2048){
                $document_upload = true;
           }else{
                $data['message'] = 'Documents Size should be less than 2MB';
           }
        }

        if($document_upload){
            if($type == 'e'){
                $caregiver = Caregiver::whereId($caregiverID)->first();
                $upload_path = public_path("uploads/provider/".session('tenant_id')."/caregivers/".\Helper::encryptor('encrypt',$caregiver->id)."/"."document");
            }else{
                $upload_path = public_path("uploads/provider/".session('tenant_id')."/caregivers/"."/"."temp");
            }

            if(!is_dir($upload_path)){
                \File::makeDirectory($upload_path, 0777, true);
            }

            //explode to check document duplication
            $temp = explode("_",$request->verification_documents);
            if($type == 'a'){
                $verification = Verification::whereNull('caregiver_id')->where('doc_name', $temp[1])->get();
            }else{
                $verification = Verification::whereCaregiverId($caregiver->id)->where('doc_name', $temp[1])->get();
            }

            //upload document
            if($verification->isEmpty()){
                if(!empty($request->$document) && !empty($request->$document.'_document'))
                {
                    $document_extension = $request->file('documents')->getClientOriginalExtension();
                    $temp = explode("_",$request->verification_documents);

                    if(count($temp)){
                        $documents = $temp[0];
                        $document_uploaded=$temp[1];
                    }

                    $verification                          = new Verification;
                    if($type == 'e'){
                        $verification->caregiver_id        = $caregiver->id;
                    }else{
                        $verification->caregiver_id        = null;
                    }

                    $verification->tenant_id               = \Helper::getTenantID();
                    $verification->doc_type                = $documents;
                    $verification->doc_name                = $document_uploaded;
                    $verification->description             = $request->verification_documents_comment;
                    $verification->doc_verification_status = 'Pending';

                    $verification->push();

                    // Update Document Path

                    $document_without_ext   = md5($verification->id)."_".str_replace(' ','_',strtolower($temp[1]));
                    $document_name          = $document_without_ext.".".$document_extension;
                    $request->file('documents')->move($upload_path, $document_name);
                    $verification->doc_path = $document_name;
                    $verification->save();

                    $image = $upload_path.DIRECTORY_SEPARATOR.$document_name;
                    // a new imagick object
                    $im = new \Imagick();
                    // set rsolution
                    $im->setResolution(300,300);
                    // ping the image
                    $im->pingImage($image);
                    // read the image into the object
                    $im->readImage($image);
                    // convert to png
                    $im->setImageFormat("jpg");
                    // other settings
                    $im->setImageCompression(\Imagick::COMPRESSION_JPEG);
                    $im->setImageCompressionQuality(90);
                    $im->setImageUnits(\Imagick::RESOLUTION_PIXELSPERINCH);


                    if($type == 'e'){
                        $mobile_path = public_path("uploads/provider/".session('tenant_id')."/caregivers/".\Helper::encryptor('encrypt',$caregiver->id)."/"."mobile");
                    }else{
                        $mobile_path = public_path("uploads/provider/".session('tenant_id')."/caregivers/"."/"."tempmobile");
                    }
                    if(!is_dir($mobile_path)){
                        \File::makeDirectory($mobile_path, 0777, true);
                    }

                    // write image to disk
                    $im->writeImage($mobile_path."/".$document_without_ext.".jpg");

                    $data['doc_id'] = $verification->id;
                    $data['message'] = 'Documents Updated';
                }
            }else{
                $data['message'] = 'The document that you are trying to upload has already been uploaded,please remove it first before re-uploading';
                $data['duplicate'] = true;
            }
        }
        
        return response()->json($data,200);
    }

    public function documentRemove(Request $request)
    {
        $caregiver_doc_id   = $request->doc_Id;
        $caregiver_id       = $request->caregiver_Id;
        $caregiver_folder   = $request->doc_Folder;
        $caregiver_doc_name = $request->doc_Name;
        $data               = $request->except('_token');

        $upload_path = public_path("uploads/provider/".session('tenant_id')."/caregivers/".$caregiver_folder."/document/".$caregiver_doc_name);
        $caregiver_doc_name_without_extension = substr($caregiver_doc_name, 0, strrpos($caregiver_doc_name, "."));
        $mobile_path = public_path("uploads/provider/".session('tenant_id')."/caregivers/".$caregiver_folder."/mobile/".$caregiver_doc_name_without_extension.".jpg");
        $res = [];
        if($data){
            $res = Verification::where('id',$caregiver_doc_id)->where('doc_path',$caregiver_doc_name)->delete();
            if (file_exists($upload_path)){
                unlink($upload_path);
            }
            if (file_exists($mobile_path)){
                unlink($mobile_path);
            }
        }
        return response()->json($res,200);
    }

    public function caregiverScheduling(Request $request)
    {
        $caregivers = Caregiver::whereHas('professional', function($query){
                                    $query->where('role','Caregiver');
                                        })
                                ->whereHas('user', function($q){
                                    $q->where('status',1);
                                })->get();

        return view('scheduling.caregiver',compact('caregivers'));
    }

    public function schedulingCaregiverfetch(Request $request)
    {
        if($request->caregiver_id){
            $events = [];
            $schedules = Schedule::withoutGlobalScopes()->whereHas('service')->whereCaregiverId($request->caregiver_id)->where('status','!=','Cancelled')->whereDate('schedule_date','>=',Carbon::now()->subDays(60))->get();
            $absents = Attendance::whereCaregiverId($request->caregiver_id)->whereStatus('A')->whereDate('date','>=',Carbon::now()->subDays(60))->get();
            $leaves = Leaves::whereCaregiverId($request->caregiver_id)->where('status','!=','Declined')->whereDate('date','>=',Carbon::now()->subDays(60))->get();

            if($schedules){
                foreach ($schedules as $s) {
                    if(isset($s->service)){
                        switch ($s->status) {
                            case 'Pending':
                            $color = 'orange';
                            break;
                            case 'Completed':
                            $color = 'green';
                            break;
                            default:
                            $color = 'black';
                        }
                        $caregiver = Caregiver::with('professional')->withoutGlobalScopes()->where('id',$s->caregiver_id)->first();
                        if(isset($s->schedule_date) && !in_array(date("D", strtotime($s->schedule_date->format('d-m-Y'))), explode(',',$caregiver->professional->working_days), true)){
                            $alert = 'Replacement required.';
                            $color ='red';
                        }else{
                            $alert = '';
                        }
                        $events[] = [
                        'title' => $s->service->service_name,
                        'time'  => Carbon::parse($s->start_time)->format('h:i a').' - '.Carbon::parse($s->end_time)->format('h:i a'),
                        'name'  => $s->patient->full_name,
                        'start' => $s->schedule_date->format('Y-m-d').'T'.$s->start_time,
                        'end'   => $s->schedule_date->format('Y-m-d').'T'.$s->end_time,
                        'color' => $color,
                        'alert' => $alert,
                        ];
                    }
                }
            }

            if($absents){
                foreach ($absents as $absent) {
                    $events[] = [
                    'title'   => 'Absent',
                    'name'    => $absent->caregiver->full_name,
                    'time'    => 'Training - '.$absent->on_training,
                    'date'    => $absent->date->format('Y-m-d'),
                    'color'   => 'darkgray',
                    'alert'   => '',
                    ];
                }
            }

            if($leaves){
                foreach ($leaves as $leave) {
                    switch ($leave->type) {
                        case 'CL':
                        $alert = 'Casual Leave - '.$leave->status;
                        break;
                        case 'SL':
                        $alert = 'Sick Leave - '.$leave->status;
                        break;
                        case 'ML':
                        $alert = 'Maternity Leave - '.$leave->status;
                        break;
                        case 'CO':
                        $alert = 'Comp-Off - '.$leave->status;
                        break;
                        case 'AC':
                        $alert = 'Abscond - '.$leave->status;
                        break;
                        default:
                        $alert = 'Others - '.$leave->status;
                    }

                    if($leave->status == "Approved"){
                        $events[] = [
                        'title'   => 'Leave Entry',
                        'time'    => 'Full Day',
                        'name'    => $leave->caregiver->full_name,
                        'date'    => $leave->date->format('Y-m-d'),
                        'color'   => 'red',
                        'alert'   => $alert,
                        ];
                    }else{
                        $events[] = [
                        'title'   => 'Leave Entry',
                        'time'    => 'Full Day',
                        'name'    => $leave->caregiver->full_name,
                        'date'    => $leave->date->format('Y-m-d'),
                        'color'   => 'darkgray',
                        'alert'   => $alert,
                        ];
                    }   
                }
            }
            
            $data['event'] = $events;
            return response()->json($events, 200);
        }
    }

    public function patientScheduling(Request $request)
    {
        $patients = Patient::whereNotNull('patient_id')->get();

        return view('scheduling.patient',compact('patients'));
    }

    public function schedulingPatientfetch(Request $request)
    {
        if($request->patient_id){
            $events = [];
            $schedules = Schedule::withoutGlobalScopes()->whereHas('service')->wherePatientId($request->patient_id)->where('status','!=','Cancelled')->whereDate('schedule_date','>=',Carbon::now()->subDays(60))->get();
            if($schedules){
                foreach ($schedules as $s) {
                    switch ($s->service->service_name) {
                        case 'Physiotherapy':
                        $color = 'green';
                        break;
                        case 'Short term nurse':
                        $color = 'orange';
                        break;
                        case 'Clinic visit':
                        $color = 'purple';
                        break;
                        case 'Home visit - nurse':
                        $color = 'blue';
                        break;
                        case 'Physiotherapy':
                        $color = 'yellow';
                        break;
                        default:
                        $color = 'black';
                    }
                    $caregiver = Caregiver::with('professional')->withoutGlobalScopes()->where('id',$s->caregiver_id)->first();

                    if(isset($caregiver) && isset($s->schedule_date) && !in_array(date("D", strtotime($s->schedule_date->format('d-m-Y'))), explode(',',$caregiver->professional->working_days), true)){
                        $alert = 'Replacement required.';
                        $color ='red';
                    }else{
                        $alert = '';
                    }

                    if(isset($caregiver)){
                        if(isset($s->schedule_date) && !in_array(date("D", strtotime($s->schedule_date->format('d-m-Y'))), explode(',',$caregiver->professional->working_days), true)){
                            $alert = 'Replacement required.';
                            $color ='red';
                        }else{
                            $alert = '';
                        }
                    }else{
                        $alert = 'Allocation required.';
                        $color ='orange';
                    }

                    $events[] = [
                    'title' => $s->service->service_name,
                    'time'  => Carbon::parse($s->start_time)->format('h:i a').' - '.Carbon::parse($s->end_time)->format('h:i a'),
                    'name'  => isset($s->caregiver)?$s->caregiver->full_name:'',
                    'start' => $s->schedule_date->format('Y-m-d').'T'.$s->start_time,
                    'end'   => $s->schedule_date->format('Y-m-d').'T'.$s->end_time,
                    'color' => $color,
                    'alert' => $alert,
                    ];
                }
            }
            $data['event'] = $events;

            return response()->json($events, 200);
        }
    }
}