<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Helpers\Helper;
use App\Entities\Leaves;

use App\Entities\User;
use App\Entities\Caregiver\Caregiver;

USE Validator;
use Hash;

class LeavesController extends Controller
{

    //FOR PROVIDER
    public function leaveslist(Request $request)
    {                             
        $data['leavesPending'] = Leaves::whereTenantId(\Helper::getTenantID())->whereStatus('PENDING')->get();
        $data['leavesApproved'] = Leaves::whereTenantId(\Helper::getTenantID())->whereStatus('APPROVED')->get();
        $data['leavesDeclined'] = Leaves::whereTenantId(\Helper::getTenantID())->whereStatus('DECLINED')->get();

        return view('leaves.index', $data);
    }

    public function leavesrespond(Request $request)
    {
        $user = Leaves::find(\Helper::encryptor('decrypt',$request->id));
        $user->status = $request->res;
        $user->save();
        if($request->res == 'APPROVED'){
            $caregiver = Caregiver::whereId($request->cid)->first();
            $caregiver->work_status = 'On Leave';

            $caregiver->save();
        }
       return back();
    }

    //FOR CAREGIVER
    public function leaveslistcaregiver(Request $request)
    {
        $data['user'] = User::find(\Helper::encryptor('decrypt',session('user_id')));

        $data['userleavelistPending'] = Leaves::where('caregiver_id',$data['user']->user_id)->whereStatus('PENDING')->get();
        $data['userleavelistApproved'] = Leaves::where('caregiver_id',$data['user']->user_id)->whereStatus('APPROVED')->get();
        $data['userleavelistDeclined'] = Leaves::where('caregiver_id',$data['user']->user_id)->whereStatus('DECLINED')->get();

        return view('leaves.apply', $data);
    }

    public function leavesapply(Request $request)
    {
        $user = User::find(\Helper::encryptor('decrypt',session('user_id')));
        $userleaves = Leaves::where('caregiver_id',$user->user_id);
        $leaves = new Leaves;

        $leaves->tenant_id = $user->tenant_id;
        $leaves->branch_id = $user->branch_id;
        $leaves->caregiver_id = $user->user_id;
        $leaves->name = $user->full_name;
        $leaves->date_received = $request->datereceived;
        $leaves->date_from = $request->datefrom;
        $leaves->date_to = $request->dateto;
        $leaves->reason = $request->reason;
        $leaves->type = $request->leavetype;
        $leaves->status = 'PENDING';

        $leaves->save();

        return back()->with('message','Your leave has been successfully submitted.');
    }
}
