<?php
namespace App\Http\Controllers\ChannelPartner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Entities\Physio\ChannelPartner;
use App\Entities\Physio\CreditBalance;
use App\Entities\Physio\ReferralCode;
use App\Entities\Physio\FreelancerProfile;


class ChannelPartnerController extends Controller
{
    public function index(Request $request)
    {   
        $channelPartner = ChannelPartner::whereEmail(session('cp.cp_email'))->first();
        $creditBalance = CreditBalance::whereTenantType('CP')->whereTenantId($channelPartner->id)->first();
        $referredPhysios = ReferralCode::whereReferralType('P')->whereReferrerId($channelPartner->id)->get();
        $data = array();
        foreach ($referredPhysios as $rP) {
            $data[] = FreelancerProfile::whereId($rP->tenant_id)->first();
        }
        $referralCode = ReferralCode::whereReferralType('CP')->whereTenantId($channelPartner->id)->first();

        return view('channelpartner.dashboard',compact('data','channelPartner','creditBalance','referralCode'));
    }

    public function login(Request $request)
    {
        return view('channelpartner.dashboard');
    }
}
