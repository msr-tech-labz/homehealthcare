<?php
namespace App\Http\Controllers\ChannelPartner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Entities\Physio\ChannelPartner;

class ChannelPartnerAuthentication extends Controller
{
    public function login()
    {
        if(session('cp.isLoggedIn')){
            return redirect('/channelpartner/dashboard');
        }
        return view('auth.channelpartner-login');
    }

    public function authenticate(Request $request)
    {
        $username = $request->email;
        $password = $request->password;

        $user = ChannelPartner::whereEmail($username)->first();
        if($user){
            if(\Hash::check($password, $user->password)){
                session()->put('cp.isLoggedIn',true);
                session()->put('cp.cp_fname',$user->partner_name);
                session()->put('cp.cp_email',$user->email);
                return redirect()->intended('/channelpartner/dashboard');
            }
        }
        return redirect()->route('channelpartner.login');
    }

    public function logout()
    {
        session()->forget('cp.isLoggedIn');
        session()->flush();

        return redirect()->route('channelpartner.login');
    }
}
