<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Entities\Console\Subscriber;
use App\Entities\Caregiver\Caregiver;
use App\Entities\User;

class ProfessionalController extends Controller
{
    public function createFromAppData(Request $request)
    {
        $res = false;
        $userID = 0;
        $udata = $request->only('first_name','last_name','date_of_birth','mobile_number','email','current_address','current_city','current_state','current_country');

        $dbPrefix = \Config::get('settings.db_prefix');
        $databaseName = \Config::get('settings.db_name');

        if($udata){
            $count = Caregiver::withoutGlobalScopes()->withTrashed()->count();
            $subscriber_id = 'APHHP'.str_pad(($count+1),4,'0',STR_PAD_LEFT);

            $subscriber = [
                'subscriber_id' => $subscriber_id,
                'user_type' => 'Professional',
                'database_name' => $databaseName,
                'status' => 'Active',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
            // Create entry in Subscribers table
            $tenant_id = Subscriber::insertGetId($subscriber);

            if($tenant_id){
                $udata['tenant_id'] = $tenant_id;

                // Change case
                if(isset($udata['date_of_birth']))
                    $udata['date_of_birth'] = date_format(new \DateTime($udata['date_of_birth']),'Y-m-d');

                // Save Professional details to centralconsole db
                $res = Caregiver::withoutGlobalScopes()->insertGetId($udata);
            }

            // Update Users Table
            if(isset($request->first_name) || isset($request->email)  || isset($request->password)){
                $user = [
                    'tenant_id' => $tenant_id,
                    'user_id' => $res,
                    'user_type' => 'Professional',
                    'created_by' => 'Custom',
                    'full_name' => $request->first_name.' '.$request->last_name,
                    'email' => $request->email,
                    'password' =>  bcrypt($request->password),
                    'role_id' => 1,
                    'status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
                $userID = User::withoutGlobalScopes()->insertGetId($user);
            }
        }

        $data['status_code'] = 200;
        if($res && $userID > 0){
            $data['result']['success'] = true;
            $data['result']['user_id'] = $userID;
            $data['result']['profile_id'] = $res;
            $data['result']['tenant_id'] = $tenant_id;
        }
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }
}
