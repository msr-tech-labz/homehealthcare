<?php
namespace App\Http\Controllers\Auth;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Entities\User;
use App\Entities\Profile;

class APIController extends Controller
{
    public function authenticate(Request $request)
    {
        $profile = [];
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            $user = User::validateCredentials($request->email, $request->password);
            if($user){
                // attempt to verify the credentials and create a token for the user
                if (! $token = JWTAuth::fromUser($user)) {
                    return response()->json(['error' => 'invalid_credentials'], 401);
                }

                switch ($user->user_type) {
                    case 'Provider':
                    case 'Aggregator':
                    case 'Caregiver':
                        $provider = Profile::withoutGlobalScopes()->whereTenantId($user->tenant_id)->first();
                        break;

                    default:
                        break;
                }
                $profile = [
                    'id' => $user->id,
                    'user_type' => $user->user_type,
                    'provider_id' => $user->tenant_id,
                    'provider_name' => isset($provider)?$provider->organization_name:'',
                    'full_name' => $user->full_name,
                    'email' => $user->email,
                    'role_id' => $user->role_id,
                ];
            }else{
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json(compact('token','profile'));
    }
}
