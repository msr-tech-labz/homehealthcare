<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validateCredentials($request);

        // Get tenant_id from org_code
        $subscriber = \App\Entities\Console\Subscriber::whereOrgCode($request->org_code)->first();
        if($subscriber){
            // Set the tenant connection
            \Config::set('database.connections.tenant.database',$subscriber->database_name);
            \DB::setDefaultConnection('tenant');            

            // We will send the password reset link to this user. Once we have attempted
            // to send the link, we will examine the response then see the message we
            // need to show to the user. Finally, we'll send out a proper response.
            $response = $this->broker()->sendResetLink(
                $request->only('email')
            );

            return $response == Password::RESET_LINK_SENT
                        ? $this->sendResetLinkResponse($request, $response)
                        : $this->sendResetLinkFailedResponse($request, $response);
        }
        session()->flash('status', 'Subscriber not found!');
        return back();
    }

    protected function validateCredentials(Request $request)
    {
        $this->validate($request, ['org_code' => 'required', 'email' => 'required|email']);
    }
}
