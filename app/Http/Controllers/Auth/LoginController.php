<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Entities\User;
use App\Entities\Profile;
use App\Entities\Settings;
use App\Entities\LoggingEvents;
use App\Entities\Console\Subscriber;


use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function showLoginForm()
    {
        if(session('uid') || session('utype')){
            return redirect('/dashboard');
        }
        // if mobile screen then redirect to mobile page
        /* $user_ag = $_SERVER['HTTP_USER_AGENT'];
        if(preg_match('/(android|Kindle|Silk.*Accelerated|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i',$user_ag)){
            session()->flush();
            return view('mobile');
        } */
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'org_code' => 'required|string',
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        $user = User::validateCredentials($request->org_code, $request->email, $request->password);

        if($user === 'Duplicate'){
            return back()->with('alert_error','Multiple Records exists with same Phone Number. Login as Admin to reset the accounts.');
        }
        if($user === 'NoSubscriber'){
            return back()->with('alert_error','No Organization with the provided OrgCode (or) The Organization is disabled. Please contact SHG.');
        }
        if($user === 'DisabledUser'){
            return back()->with('alert_error','User has been disabled by the Admin.');
        }
        if($user === 'InvalidPassword'){
            return back()->with('alert_error','Invalid Password. Please try again');
        }
        if($user === 'NoUserFound'){
            return back()->with('alert_error','No User found with the given Email.');
        }

        if($user){
            \Auth::login($user, $request->remember);

            // Authentication passed...
            $user = \Auth::user();
            // Set User details
            session()->put('user_id',\Helper::encryptor('encrypt',$user->id));
            session()->put('admin_id',\Helper::encryptor('encrypt',$user->user_id));
            session()->put('uid',\Helper::encryptor('encrypt',$user->tenant_id));
            session()->put('branch_id',\Helper::encryptor('encrypt',$user->branch_id));
            session()->put('email',$user->email);
            session()->put('user_name',$user->full_name);
            session()->put('utype',\Helper::encryptor('encrypt',$user->user_type));
            session()->put('organisationType',\Helper::encryptor('encrypt',Subscriber::whereId($user->tenant_id)->value('user_type')));

            session()->put('role', $user->role_id);

            if($user->user_type == 'Professional'){
                session()->put('tenant_id',\Helper::encryptor('encrypt',$user->tenant_id));
                session()->put('profile_image',$user->caregiver->profile_image);
                session()->put('prof_folder',$user->caregiver->prof_folder);
            }else{
                $profile = Profile::withoutGlobalScopes()->whereTenantId($user->tenant_id)->first();
                if($profile){
                    session()->put('organization_name',$profile->organization_name);
                    session()->put('organization_short_name',$profile->organization_short_name);
                    session()->put('organization_email',$profile->email);
                    session()->put('organization_logo',$profile->organization_logo);
                    $cookie1 = \Cookie::forever('cookieLogo', $profile->organization_logo);
                    $cookie2 = \Cookie::forever('orgcode', $profile->subscriber->org_code);
                    $cookie3 = \Cookie::forever('tenant_id', \Helper::encryptor('encrypt',$profile->tenant_id));
                    session()->put('website',$profile->website);
                    session()->put('contact_person',$profile->contact_person);

                    session()->put('organization_address',$profile->organization_address);
                    session()->put('organization_city',$profile->organization_city);
                    session()->put('organization_zipcode',$profile->organization_zipcode);
                    session()->put('organization_state',$profile->organization_state);
                    session()->put('organization_country',$profile->organization_country);
                    session()->put('phone_number',$profile->phone_number);

                    // Save Settings in session
                    $settings = Settings::first();
                    if($settings){
                        $settings = collect($settings)->except('id','tenant_id','created_at','updated_at','deleted_at');
                        foreach ($settings as $key => $value) {
                            session()->put('settings.'.$key,\Helper::encryptor('encrypt',$value));
                        }
                    }
                }
            }

            // Set Tenant ID
            session()->put('tenant_id',\Helper::encryptor('encrypt',$user->tenant_id));

            return redirect($this->redirectTo)->withCookie($cookie1)->withCookie($cookie2)->withCookie($cookie3);
        }else{
            return back()->with('alert_error','Unable to Login at the moment. Please Try again');
        }

        return view('auth.login');
    }

    public function authenticated($request,$user)
    {
        return redirect(session()->pull('from_'.session('tenant_id'),$this->redirectTo));
    }

    public function logout()
    {
        self::setTenantConnection(\Helper::getTenantID());
        LoggingEvents::whereUserId(\Helper::getUserID())
                        ->whereId(\Helper::encryptor('decrypt',session('login_id')))
                        ->whereLogoutTime(null)
                        ->update([
                            'logout_time'   =>  date("h:i:s")
                        ]);
        \Auth::logout();
        session()->flush();

        return redirect('login');
    }
}
