<?php
namespace App\Http\Controllers\Auth;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Entities\User;
use App\Entities\Profile;
use App\Entities\Patient;
use App\Entities\Console\CustomerRegistrations;
use App\Entities\Console\CustomerLinks;
use App\Entities\Console\Subscriber;

class APIAuthController extends Controller
{
    public function authenticate(Request $request)
    {
        $profile = [];
        // grab credentials from the request
        $credentials = $request->only('orgcode','email', 'password');

        try {
            $user = User::validateCredentials($request->orgcode, $request->email, $request->password);
            if($user === 'Duplicate'){
                return response()->json(['error' => 'Multiple Records exists with same Phone Number. Login as Admin to reset the accounts.'], 401);
            }
            if($user === 'DisabledUser'){
                return response()->json(['error' => 'User has been disabled by the Admin.'], 401);
            }
            if($user === 'InvalidPassword'){
                return response()->json(['error' => 'Invalid Password. Please try again'], 401);
            }
            if($user === 'NoUserFound'){
                return response()->json(['error' => 'No User found with the given Email.'], 401);
            }
            if($user === 'NoSubscriber'){
                return response()->json(['error' => 'No Organization with the provided OrgCode (or) The Organization is disabled. Please contact SHG.'], 401);
            }
            if($user){
                // attempt to verify the credentials and create a token for the user
                // if (! $token = JWTAuth::fromUser($user)) {
                //     return response()->json(['error' => 'invalid_credentials'], 401);
                // }
                $provider = Profile::withoutGlobalScopes()->whereTenantId($user->tenant_id)->first();

                if($user->caregiver->professional->role == $request->user_type){
                    $profile = [
                        'id' => $user->id,
                        'profile_id' => $user->user_id,
                        'caregiver_id' => $user->user_id,
                        'user_type' => $user->caregiver->professional->role,
                        'tenant_id' => $user->tenant_id,
                        'branch_id' => $user->branch_id,
                        'org_hash' => isset($provider)?\Helper::encryptor('encrypt',$provider->subscriber->database_name):'',
                        'organization_code' => isset($provider)?$provider->subscriber->subscriber_id:'',
                        'organization_name' => isset($provider)?$provider->organization_name:'',
                        'organization_city' => isset($provider)?$provider->organization_city:'',
                        'organization_state' => isset($provider)?$provider->organization_state:'',
                        'organization_logo' => isset($provider)?url('uploads/provider/'.\Helper::encryptor('encrypt',$provider->tenant_id).'/'.$provider->organization_logo):'',
                        'organization_type' => 'Home Health Care',
                        'full_name' => $user->full_name,
                        'email' => $user->email,
    		            'role_id' => $user->role_id,
                        'profile_image' => isset($user->caregiver)?"'".url('uploads/provider/'.\Helper::encryptor('encrypt',$provider->tenant_id).'/caregivers/'.\Helper::encryptor('encrypt',$user->user_id).'/'.$user->caregiver->profile_image)."'":''
                    ];
                }else{
                    return response()->json(['error' => 'invalid_credentials'], 401);
                }
            }else{
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json(compact('token','profile'));
    }

    public function token()
    {
        $token = JWTAuth::getToken();
        if(!$token){
            throw new BadRequestHtttpException('Token not provided');
        }
        try{
            $token = JWTAuth::refresh($token);
        }catch(TokenInvalidException $e){
            throw new AccessDeniedHttpException('The token is invalid');
        }
        return response()->json(['token'=>$token]);
    }

    public function customerSignup(Request $request)
    {
        $accountID = 0;
        $data = [];
        if($request->mobile_number && $request->email && $request->password && $request->full_name){
            $mobile_number = str_replace(' ','',$request->mobile_number);
            if(filter_var($request->email, FILTER_VALIDATE_EMAIL)){
                if(strlen((string)$mobile_number) == 10){
                    $customer = CustomerRegistrations::whereEmail($request->email)->orWhere('mobile_number',$mobile_number)->first();
                    if($customer){
                        $data['status_code'] = 409;
                        if($customer->email == $request->email)
                            $data['result']      = 'Record already Exists with same Email';
                        else if($customer->mobile_number == $mobile_number)
                            $data['result']      = 'Record already Exists with same Mobile Number';
                        $data['error']       = true;
                    }else{
                        $accountData = [
                            'email'         => $request->email,
                            'full_name'     => $request->full_name,
                            'mobile_number' => $request->mobile_number,
                            'password'      => bcrypt($request->password),
                            'created_at'    => date('Y-m-d H:i:s'),
                            'updated_at'    => date('Y-m-d H:i:s')
                        ];
                        $accountID = CustomerRegistrations::insertGetId($accountData);

                        $data['status_code'] = 200;
                        if($accountID > 0){
                            $data['result'] = 'Registration Successfully';
                            $data['error'] = false;
                        }else{
                            $data['result'] = 'Registration Failed';
                            $data['error'] = true;
                        }
                    }
                }else{
                    $data['result'] = 'Invalid Mobile Number';
                    $data['error'] = true;
                }
            }else{
                $data['result'] = 'Invalid Email';
                $data['error'] = true;
            }
        }
        return response()->json($data, 200);
    }

    public function customerLogin(Request $request)
    {
        $authenticatedUser = false;
        $result['links'] = [];
        $result['customer'] = [];

        if($request->email && $request->password){
            // Get database name from subscriber ID
            $customer = CustomerRegistrations::whereEmail($request->email)->first();
            if($customer){
                    if(\Hash::check($request->password, $customer->password)){
                        $authenticatedUser = true;
                        $linksCheck = CustomerLinks::whereCustomerId($customer->id)->get();
                        if(count($linksCheck)){
                            foreach ($linksCheck as $link) {
                                \Config::set('database.connections.tenant.database',\Helper::encryptor('decrypt',$link->org_hash));
                                \DB::setDefaultConnection('tenant');
                                $pt = Patient::whereId($link->patient_id)->first();
                                $pf = Profile::first();
                                if($pt){
                                     $result['links'][] = [
                                        'patient_id'        => $link->patient_id,
                                        'patient_name'      => $pt->fullname,
                                        'organization_name' => $pf->organization_name,
                                        'organization_city' => $pf->organization_city,
                                        'tenant_id'         => $link->tenant_id,
                                        'org_hash'          => $link->org_hash,
                                        'profile_image'     => $pt->profile_image
                                     ]; 
                                }
                                 unset($pt,$pf);
                                 \Config::set('database.default','central');
                                 \DB::setDefaultConnection('central');
                             } 
                        }

                        $result['customer'] = [
                            'id'            => $customer->id,
                            'full_name'     => $customer->full_name,
                            'email'         => $customer->email,
                            'mobile_number' => $customer->mobile_number,
                            'profile_image' => isset($customer->profile_image)?asset('uploads/customer_registrations/'.\Helper::encryptor('encrypt',$customer->id).'/'.$customer->profile_image):null,
                        ];

                        return response()->json(['status_code' => 200, 'result' => $result], 200);
                    }else{
                        return response()->json(['error' => 'Password does not match!'], 401);
                    }
            }else{
                return response()->json(['error' => 'Email does not exist!'], 401);
            }
        }
        return response()->json(['error' => 'Email or Password is empty!'], 401);
    }

    public function customerPasswordReset(Request $request)
    {
        if($request->mobile_number && $request->new_password){
            // Get database name from subscriber ID
            $customer = CustomerRegistrations::whereMobileNumber($request->mobile_number)->first();
            if($customer){
                $customer->password = bcrypt($request->new_password);
                $customer->save();
                $result = 'Password Successfully Updated';

                return response()->json(['status_code' => 200, 'result' => $result], 200);
            }
        }

        return response()->json(['error' => 'Phone number not found registered'], 401);
    }
}
