<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Entities\Lead;
use App\Entities\Notifications;
use App\Entities\Console\Subscriber;

use Pusher\Pusher;

class PusherController extends Controller
{
   public static function sendNotificationPendingLeads()
       {
           //Remember to change this with your cluster name.
           $options = array(
               'cluster' => 'ap2', 
               'encrypted' => true
           );
    
          //Remember to set your credentials below.
           $pusher = new Pusher(
               '18763c56b1c07bda4ced',
               'af42713ef83d33d58660',
               '455662',
               $options
           );
           $subscribers = Subscriber::whereUserType('Provider')->get();
           foreach($subscribers as $subscriber){
                \DB::purge('api');
                self::setTenantConnection($subscriber->id);
                $countPendingLeads = Lead::withoutGlobalScopes()->whereStatus('Pending')->count();
                if($countPendingLeads){
                  $message = "Greetings from Smart Health Connect. Make Sure to check all the Pending Leads.You Have ".$countPendingLeads." pending leads till date";
                }else{
                  $message = "Greetings from Smart Health Connect. Great!! You do not have any pending leads till date";
                }
                $data = [];
                $data['message'] = $message;
                Notifications::create($data);
                unset($data);
                $date = date('d-m-Y');
                $time = date('h:i A');
                $count = Notifications::whereStatus('Unread')->count();

                //Send a message to notify channel with an event name of notify-event
                $room_channel = \Helper::encryptor('encrypt',$subscriber->database_name).\Helper::encryptor('encrypt',$subscriber->id);
                $pusher->trigger($room_channel.'notify', 'notify-event', [$message,$date,$time,$count]);
                unset($countPendingLeads);
                \DB::purge('api');
           }
       }
}
