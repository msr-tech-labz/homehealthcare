<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Entities\Role;
use App\Entities\Permission;

class AccessControlListController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $roles = Role::all();

        return view('administration.acl.index',compact('roles'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $permissions = Permission::all()->groupBy('group');

        return view('administration.acl.form',compact('permissions'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $data = $request->except(['permission','_token']);
        $data['tenant_id'] = \Helper::getTenantID();

        $role = Role::create($data);
        if($request->permission){
            foreach ($request->permission as $key=>$value){
                $role->attachPermission($value);
            }
        }

        return redirect()->route('acl.index')->with('alert_success','Role Created');
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit(Request $request)
    {
        $role = Role::find($request->id);
        $permissions = Permission::all()->groupBy('group');
        $role_permissions = $role->perms()->pluck('id','id')->toArray();

        return view('administration.acl.form',compact(['role','role_permissions','permissions']));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request)
    {
        if($request->permission){
            $role = Role::find($request->id);
            $role->name = $request->name;
            $role->display_name = $request->display_name;
            $role->description = $request->description;
            $role->save();

            \DB::table('auth_permission_role')->whereRoleId($request->id)->delete();
            foreach ($request->permission as $key=>$value){
                $role->attachPermission($value);
            }
        }

        return redirect()->route('acl.index')->with('alert_success','Role Updated! Please Reload the Privileges from the Users Section.');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(Request $request)
    {
        \DB::table("auth_roles")->whereTenantId(\Helper::getTenantID())->where('id',$request->id)->delete();

        return back()->with('alert_success','Role Deleted');
    }
}
