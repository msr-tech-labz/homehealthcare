<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Entities\Masters\Taxrate;
use App\Entities\Masters\Surgicals;

class SurgicalsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $taxRates = Taxrate::all();
        $surgicals = Surgicals::whereTenantId(\Helper::getTenantID())->get();

        return view('administration.surgicals',compact('taxRates','surgicals'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('branch_id','surgical_name','description','surgical_price','tax_id');

        if($data){
            $data['tenant_id'] = \Helper::getTenantID();
            Surgicals::create($data);

            return redirect()->back()->with('alert_success','Surgical/Equipment added successfully');
        }

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only('branch_id','surgical_name','description','surgical_price','tax_id');
        if($data && $id){
            $id = \Helper::encryptor('decrypt',$id);
            $data['tenant_id'] = \Helper::getTenantID();

            Surgicals::find($id)->update($data);

            return redirect()->back()->with('alert_success','Surgical/Equipment updated successfully');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id){
            $id = \Helper::encryptor('decrypt',$id);
            Surgicals::find($id)->forceDelete();
        }

        return back()->with('alert_info','Surgical/Equipment removed successfully');
    }
}
