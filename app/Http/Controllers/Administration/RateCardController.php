<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Http\Requests\Administration\StoreRateCard;
use App\Entities\Masters\RateCard;
use App\Entities\Masters\Service;
use App\Entities\Masters\Taxrate;
use App\Entities\CaseRatecard;

class RateCardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ratecards = RateCard::whereTenantId(\Helper::getTenantID())->whereHas('service')
                                ->orderBy('service_id','Asc')->whereStatus(1)->get();
        if(session('orgType') == 'Physiotherapy'){
            $services = Service::where('service_type','PHYSIOTHERAPY')->orderBy('category_id','Asc')->get();
        }else{
            $services = Service::orderBy('category_id','Asc')->get();
        }

        $taxRates = Taxrate::whereStatus(1)->get();

        return view('administration.ratecard',compact('ratecards','services','taxRates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRateCard $request)
    {
        $data = $request->only('branch_id','service_id','amount','tax_id');

        $data['tenant_id'] = \Helper::getTenantID();
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');
        $data['status'] = 1;        
        if($data){
            RateCard::whereServiceId($data['service_id'])->whereStatus(1)->update(['status' => 0]);
            RateCard::insert($data);
            if(!session(\Helper::getTenantID().'_service_rates')){
                $ratecard = RateCard::all();
                session()->put(\Helper::getTenantID().'_service_rates',$ratecard);
            }
            return redirect()->back()->with('alert_success','Rate Card added successfully');
        }

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRateCard $request, $id)
    {
        $data = $request->only('branch_id','service_id','amount','tax_id');

        if($data && $id){
            $id = \Helper::encryptor('decrypt',$id);
            $data['tenant_id'] = \Helper::getTenantID();
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            RateCard::whereServiceId($data['service_id'])->whereStatus(1)->update(['status' => 0]);
            RateCard::insert($data);
            if(!session(\Helper::getTenantID().'_service_rates')){
                $ratecard = RateCard::all();
                session()->put(\Helper::getTenantID().'_service_rates',$ratecard);
            }
            return redirect()->back()->with('alert_success','Rate Card updated successfully');
        }

        return back();
    }

    public function updateStatus(Request $request)
    {
        $data = $request->only('id','status');
        if($data && $data['id']){
            $id = \Helper::encryptor('decrypt',$data['id']);
            $rateCard = RateCard::find($id);
            if($rateCard){
                $rateCard->status = $data['status'];
                $rateCard->save();
            }
        }

        return response()->json(true,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id){
            $id = \Helper::encryptor('decrypt',$id);
            RateCard::find($id)->delete();
        }

        return back()->with('alert_info','RateCard removed successfully');
    }
}
