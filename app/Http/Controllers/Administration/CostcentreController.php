<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Entities\Masters\CostCentre;
use App\Entities\Masters\Specialization;

class CostcentreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $costcentres = CostCentre::whereTenantId(\Helper::getTenantID())->get();
        $specializations = Specialization::get();

        return view('administration.costcentre',compact('costcentres','specializations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);

        $data = $request->only('specialization_id','cost_centre','description','status');
        if($data){
            $data['tenant_id'] = \Helper::getTenantID();
            CostCentre::create($data);

            return redirect()->back()->with('alert_success','Cost Centre added successfully');
        }

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only('specialization_id','cost_centre','description','status');
        if($data && $id){
            $id = \Helper::encryptor('decrypt',$id);
            $data['tenant_id'] = \Helper::getTenantID();

            CostCentre::find($id)->update($data);

            return redirect()->back()->with('alert_success','Cost Centre updated successfully');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id){
            $id = \Helper::encryptor('decrypt',$id);
            CostCentre::find($id)->forceDelete();
        }

        return back()->with('alert_info','Cost Centre removed successfully');
    }
}
