<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Entities\Masters\AssistedLiving;

class AssistedLivingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assistedLivings = AssistedLiving::whereTenantId(\Helper::getTenantID())->get();

        return view('administration.assisted-living',compact('assistedLivings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('branch_id','category','description','gross_rate','period','taxable','tax_percentage','net_rate');
        if($request->tax_percentage == null){
            $data['tax_percentage'] = "0.00";
        }
        if($data){
            $data['tenant_id'] = \Helper::getTenantID();
            AssistedLiving::create($data);

            return redirect()->back()->with('alert_success','Assisted Living room added successfully');
        }

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only('branch_id','category','description','gross_rate','period','taxable','tax_percentage','net_rate');
        if($data && $id){
            $id = \Helper::encryptor('decrypt',$id);
            $data['tenant_id'] = \Helper::getTenantID();

            AssistedLiving::find($id)->update($data);

            return redirect()->back()->with('alert_success','Assisted Living room updated successfully');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id){
            $id = \Helper::encryptor('decrypt',$id);
            AssistedLiving::find($id)->forceDelete();
        }

        return back()->with('alert_info','Assisted Living room removed successfully');
    }
}
