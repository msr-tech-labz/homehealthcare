<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Entities\Masters\BillingProfile;

class BillablesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settingsBillable = BillingProfile::whereTenantId(\Helper::getTenantID())->get();

        return view('administration.billingProfile',compact('settingsBillable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('name','email','address','area','city','zipcode','state','phone','country','pan_number','service_tax_no');
        $res = null;
        if($request->hasFile('logo'))
        {
            $profile = $request->file('logo');
            $fileName = $request->file('logo')->getClientOriginalName();

            $path = public_path('uploads/provider/'.session('tenant_id').'/billables');
            if(!is_dir($path)){
                \File::makeDirectory($path, 0777, true);
            }

            $res = $profile->move($path, $fileName);
        }

        if($res)
            $data['logo'] = $fileName;

        if($data){
            BillingProfile::create($data);

            return redirect()->back()->with('alert_success','Profile added successfully');
        }

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only('name','email','address','area','city','zipcode','state','phone','country','pan_number','service_tax_no');
        $res = null;
        if($request->hasFile('logo'))
        {
            $profile = $request->file('logo');
            $fileName = $request->file('logo')->getClientOriginalName();

            $path = public_path('uploads/provider/'.session('tenant_id').'/billables');
            if(!is_dir($path)){
                \File::makeDirectory($path, 0777, true);
            }

            $res = $profile->move($path, $fileName);
        }

        if($res)
            $data['logo'] = $fileName;

        if($data && $id ){
            $id = \Helper::encryptor('decrypt',$id);

            BillingProfile::find($id)->update($data);

            return redirect()->back()->with('alert_success','Profile updated successfully');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id){
            $id = \Helper::encryptor('decrypt',$id);
            BillingProfile::find($id)->delete();
        }

        return back()->with('alert_info','Profile removed successfully');
    }
}
