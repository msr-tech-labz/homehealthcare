<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Http\Requests\Administration\StoreDepartment;
use App\Entities\Masters\Department;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::whereTenantId(\Helper::getTenantID())->orWhere('created_by','Default')->get();

        return view('administration.department',compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDepartment $request)
    {
        //dd($request);

        $data = $request->only('branch_id','department_name','description');
        if($data){
            $data['tenant_id'] = \Helper::getTenantID();
            Department::create($data);

            return redirect()->back()->with('alert_success','Department added successfully');
        }

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreDepartment $request, $id)
    {
        $data = $request->only('branch_id','department_name','description');
        if($data && $id){
            $id = \Helper::encryptor('decrypt',$id);
            $data['tenant_id'] = \Helper::getTenantID();
            
            Department::find($id)->update($data);

            return redirect()->back()->with('alert_success','Department updated successfully');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id){
            $id = \Helper::encryptor('decrypt',$id);
            Department::find($id)->forceDelete();
        }

        return back()->with('alert_info','Department removed successfully');
    }
}
