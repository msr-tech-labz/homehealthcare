<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Entities\Console\Feedback;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feedbacks = Feedback::whereTenantId(\Helper::getTenantID())->get();

        return view('administration.feedback',compact('feedbacks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('type','module','message');
        if($data){
            $data['tenant_id'] = \Helper::getTenantID();
            $data['created_at'] = $data['updated_at'] = date('Y-m-d H:i:s');

            $id = Feedback::insertGetId($data);

            if($request->hasFile('document'))
            {
                $profile = $request->file('document');
                $extension = $profile->extension();

                $path = public_path('uploads/feedback/'.session('tenant_id'));
                if(!is_dir($path)){
                   \File::makeDirectory($path, 0777, true);
                }

               $fileName = \Helper::encryptor('encrypt',$id).'.'.$extension;

               $res = $profile->move($path, $fileName);

               // Update document path
               Feedback::find($id)->update(['document_path' => $fileName]);
            }

            return redirect()->back()->with('alert_success','Feedback saved successfully');
        }

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only('branch_id','feedback_name','feedback_date');
        if($data && $id){
            $id = \Helper::encryptor('decrypt',$id);
            $data['tenant_id'] = \Helper::getTenantID();

            Feedback::find($id)->update($data);

            return redirect()->back()->with('alert_success','Feedback updated successfully');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id){
            $id = \Helper::encryptor('decrypt',$id);
            Feedback::find($id)->forceDelete();
        }

        return back()->with('alert_info','Feedback removed successfully');
    }
}
