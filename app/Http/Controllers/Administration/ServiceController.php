<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Http\Requests\Administration\StoreService;
use App\Entities\Lead;
use App\Entities\Masters\Service;
use App\Entities\Masters\ServiceCategory;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session('orgType') == 'Physiotherapy'){
            $services = Service::where('service_type','PHYSIOTHERAPY')->orderBy('category_id','Asc')->get();
        }else{
            $services = Service::orderBy('category_id','Asc')->get();
        }
        $categories = ServiceCategory::all();

        return view('administration.service',compact('services','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreService $request)
    {
        $data = $request->all('branch_id','service_name','category_id','description','hsn');
        if($data){
            $data['service_name'] = ucfirst($data['service_name']);
            if(session('orgType') == 'Physiotherapy'){
                $data['service_type'] = 'PHYSIOTHERAPY';
            }
            $data['tenant_id'] = \Helper::getTenantID();
            Service::create($data);

            return redirect()->back()->with('alert_success','Service added successfully');
        }

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreService $request, $id)
    {
        $data = $request->all('branch_id','service_name','category_id','description','hsn');
        if($data && $id){
            $id = \Helper::encryptor('decrypt',$id);
            $data['service_name'] = ucfirst($data['service_name']);
            if(session('orgType') == 'Physiotherapy'){
                $data['service_type'] = 'PHYSIOTHERAPY';
            }
            $data['tenant_id'] = \Helper::getTenantID();

            Service::find($id)->update($data);

            return redirect()->back()->with('alert_success','Service updated successfully');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id){
            $id = \Helper::encryptor('decrypt',$id);
            $servicecount = Lead::whereServiceRequired($id)->count();
            if($servicecount){
                return back()->with('alert_warning','This Service is already associated with one/more Leads, please change the service from the respective cases and schedules before deleting it from the Masters.');
            }else{
            Service::find($id)->delete();
            }
        }

        return back()->with('alert_info','Service removed successfully');
    }
}
