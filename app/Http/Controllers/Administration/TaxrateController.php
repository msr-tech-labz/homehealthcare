<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Http\Requests\Administration\StoreTaxrate;
use App\Entities\Masters\Taxrate;

class TaxrateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $taxrates = Taxrate::whereTenantId(\Helper::getTenantID())
                            ->orWhere('created_by','Default')
                            ->orderBy('order','Asc')
                            ->get();

        return view('administration.taxrate',compact('taxrates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);

        $data = $request->only('tax_name','tax_rate','type','order','status');
        if($data){
            $data['tenant_id'] = \Helper::getTenantID();
            Taxrate::create($data);

            return redirect()->back()->with('alert_success','Taxrate added successfully');
        }

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only('tax_name','tax_rate','type','order','status');
        if($data && $id){
            $id = \Helper::encryptor('decrypt',$id);
            $data['tenant_id'] = \Helper::getTenantID();

            Taxrate::find($id)->update($data);

            return redirect()->back()->with('alert_success','Taxrate updated successfully');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id){
            $id = \Helper::encryptor('decrypt',$id);
            Taxrate::find($id)->forceDelete();
        }

        return back()->with('alert_info','Taxrate removed successfully');
    }
}
