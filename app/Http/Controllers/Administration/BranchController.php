<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Http\Requests\Administration\StoreBranch;
use App\Entities\Masters\Branch;

use App\Entities\Lead;

use App\Entities\Caregiver\Caregiver;
use App\Entities\Caregiver\ProfessionalDetails;


class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {      
        $branch = Branch::get(); 

        return view('administration.branch',compact('branch'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBranch $request)
    {
        $data = $request->except('_method','_token');

        if($data){
            $data['tenant_id'] = \Helper::getTenantID();
            Branch::create($data);

            session()->forget(\Helper::getTenantID().'_branches');
            session()->put(\Helper::getTenantID().'_branches',collect(Branch::get()));

            return redirect()->back()->with('alert_success','Branch added successfully');
        }

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreBranch $request, $id)
    {
        $data = $request->except('_method','_token');

        if($data && $id){
            $id = \Helper::encryptor('decrypt',$request->id);
            $data['tenant_id'] = \Helper::getTenantID();
            
            Branch::withoutGlobalScopes()->whereId($request->id)->update($data);

            session()->forget(\Helper::getTenantID().'_branches');
            session()->put(\Helper::getTenantID().'_branches',collect(Branch::get()));

            return redirect()->back()->with('alert_success','Branch updated successfully');
        }

        return back();
    }

    public function updateStatus(Request $request)
    {
        $data = $request->only('id','status');
        if($data && $data['id']){
            $id = \Helper::encryptor('decrypt',$data['id']);
            $branch = Branch::find($id);
            if($branch){
                $branch->status = $data['status'];
                $branch->save();
            }
        }

        return response()->json(true,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id){
            $id = \Helper::encryptor('decrypt',$id);
            $branchcountCaregiver = Caregiver::whereBranchId($id)->count();
            $branchcountLead = Lead::whereBranchId($id)->count();
            $branchcountCaregiverProf = ProfessionalDetails::whereBranchId($id)->count();
            if($branchcountCaregiver && $branchcountCaregiverProf && $branchcountLead){
                return back()->with('alert_warning','This Branch is already associated with one/more Employee, please update the branch from the respective employee(s) before deleting it from the Masters.');
            }elseif(Branch::count() == 1){
                return back()->with('alert_warning','Atleast one Branch is Required.');
            }else{
                Branch::where('id',$id)->forceDelete();
                session()->forget(\Helper::getTenantID().'_branches');
                session()->put(\Helper::getTenantID().'_branches',collect(Branch::get()));

                return back()->with('alert_info','Branch removed successfully');   
            }
        }else
        
            return back()->with('alert_info','No Branch Available');
    }
}
