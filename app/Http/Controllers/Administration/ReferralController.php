<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Http\Requests\Administration\StoreReferralSource;

use App\Http\Requests\Administration\StoreRateCard;
use App\Entities\Masters\ReferralCategory;
use App\Entities\Masters\ReferralSource;

class ReferralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = ReferralCategory::get();
        $sources = ReferralSource::get();

        return view('administration.refferal',compact('categories','sources'));
    }

    public function saveCategory(Request $request)
    {
        $id = \Helper::encryptor('decrypt',$request->id);
        $data = $request->only('branch_id','category_name','description');

        if($data){
            if($id == 0){
                $data['tenant_id'] = \Helper::getTenantID();
                ReferralCategory::create($data);
            }else{
                $referralCategory = ReferralCategory::find($id);
                if($referralCategory){
                    $referralCategory->update($data);
                }
            }
        }

        return back()->with('alert_success','Referral Category updated successfully');
    }

    public function saveSource(StoreReferralSource $request)
    {
        $id = \Helper::encryptor('decrypt',$request->id);
        $data = $request->only('branch_id','source_name','phone_number','category_id','description','charge_type','referral_value','bank_name','bank_address','account_number','ifsc_code');

        if($data){
            if($id == 0){
                $data['tenant_id'] = \Helper::getTenantID();
                ReferralSource::create($data);
            }else{
                $referralSource = ReferralSource::find($id);
                if($referralSource){
                    $referralSource->update($data);
                }
            }
        }

        return back()->with('alert_success','Referral Source updated successfully');
    }

    public function deleteCategory(Request $request)
    {
        if($request->id){
            $id = \Helper::encryptor('decrypt',$request->id);
            ReferralCategory::find($id)->delete();
        }

        return back()->with('alert_info','Referral Category removed successfully');
    }

    public function deleteSource(Request $request)
    {
        if($request->id){
            $id = \Helper::encryptor('decrypt',$request->id);
            ReferralSource::find($id)->delete();
        }

        return back()->with('alert_info','Referral Source removed successfully');
    }
}
