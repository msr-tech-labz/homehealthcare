<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Http\Requests\Administration\StoreDesignation;
use App\Entities\Masters\Designation;

class DesignationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $designations = Designation::whereTenantId(\Helper::getTenantID())->orWhere('created_by','Default')->get();

        return view('administration.designation',compact('designations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDesignation $request)
    {
        //dd($request);

        $data = $request->only('branch_id','designation_name','description');
        if($data){
            $data['tenant_id'] = \Helper::getTenantID();
            Designation::create($data);

            return redirect()->back()->with('alert_success','Designation added successfully');
        }

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreDesignation $request, $id)
    {
        $data = $request->only('branch_id','designation_name','description');
        if($data && $id){
            $id = \Helper::encryptor('decrypt',$id);
            $data['tenant_id'] = \Helper::getTenantID();
            
            Designation::find($id)->update($data);

            return redirect()->back()->with('alert_success','Designation updated successfully');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id){
            $id = \Helper::encryptor('decrypt',$id);
            Designation::find($id)->forceDelete();
        }

        return back()->with('alert_info','Designation removed successfully');
    }
}
