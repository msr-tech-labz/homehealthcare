<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Entities\Masters\ManagementComposition;

use App\Entities\Masters\Department;
use App\Entities\Masters\Designation;
use App\Entities\Masters\Service;


class CompositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $data = [];

        $designations = Designation::select('id','designation_name')->get();//hr
        $services     = Service::select('id','service_name')->get();//services
        $departments  = Department::select('id','department_name')->get();//convertedBy

        $data['cadreHR'] = explode(',',ManagementComposition::select('id','composition_cadre')->whereType('mis_hr')->value('composition_cadre'));
        $data['cadreLeadsVisibility'] = explode(',',ManagementComposition::select('id','composition_cadre')->whereType('leads_viewable_by')->value('composition_cadre'));
        $data['cadreLeadsConvertedBy'] = explode(',',ManagementComposition::select('id','composition_cadre')->whereType('leads_converted_by')->value('composition_cadre'));

        return view('administration.composition',compact('departments','designations','services','data'));
    }

    public function update(Request $request)
    {   
        if($request->type != null && $request->type != '' && $request->list != null || !empty($request->list)){
            ManagementComposition::whereType($request->type)->update(['composition_cadre' => implode(',', $request->list)]);
            return response()->json(200);
        }
        if(empty($request->list) || $request->list == null){
            ManagementComposition::whereType($request->type)->update(['composition_cadre' => null]);
            return response()->json(200);
        }
        return response()->json('Oops there was an error, please try after some time', 500);
    }
}
