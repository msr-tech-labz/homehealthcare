<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Entities\Masters\Trainings;
use App\Entities\Caregiver\Caregiver;
use App\Entities\Masters\Specialization;


class TrainingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['trainings'] = Trainings::whereTenantId(\Helper::getTenantID())->get();
        $data['caregivers'] = Caregiver::where('work_status','!=','Training')->get();
        $data['specialization'] = Specialization::get();

        return view('administration.trainings',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('trainer_name','training_name','start_date','end_date');
        if($data){
            foreach($request->employee_id as $e){
            $data['branch_id'] = Caregiver::where('id', $e)->first()->value('branch_id');
            $data['employee_id'] = $e;
            $data['tenant_id'] = \Helper::getTenantID();
            Trainings::create($data);

            $caregiver = Caregiver::whereId($e)->first();
            $caregiver->work_status = "Training";
            $caregiver->save();
            }

            return response()->json($data,200);
        }

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id){
            $id = \Helper::encryptor('decrypt',$id);
            $eid = Trainings::whereId($id)->value('employee_id');
            Trainings::find($id)->forceDelete();

            $caregiver = Caregiver::whereId($eid)->first();
            $caregiver->work_status = "Available";
            $caregiver->save();
        }

        return back()->with('alert_info','Employee removed from training successfully');
    }
}
