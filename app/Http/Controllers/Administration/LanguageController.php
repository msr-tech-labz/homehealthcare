<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Http\Requests\Administration\StoreLanguage;
use App\Entities\Masters\Language;

class LanguageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $languages = Language::whereTenantId(\Helper::getTenantID())->orWhere('created_by','Default')->get();

        return view('administration.language',compact('languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLanguage $request)
    {
        $data = $request->only('language_name','country');
        if($data){
            $data['tenant_id'] = \Helper::getTenantID();
            Language::create($data);

            return redirect()->back()->with('alert_success','Language added successfully');
        }

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only('language_name','country');
        if($data && $id){
            $id = \Helper::encryptor('decrypt',$id);
            $data['tenant_id'] = \Helper::getTenantID();
            
            Language::find($id)->update($data);

            return redirect()->back()->with('alert_success','Language updated successfully');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id){
            $id = \Helper::encryptor('decrypt',$id);
            Language::find($id)->forceDelete();
        }

        return back()->with('alert_info','Language removed successfully');
    }
}
