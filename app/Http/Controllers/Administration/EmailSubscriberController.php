<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;

use App\Entities\Masters\EmailSubscriber;

class EmailSubscriberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mailers = EmailSubscriber::get();

        return view('administration.email-subscriber',compact('mailers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('mailer_types','name','email');
        if($data){
            $data['tenant_id'] = \Helper::getTenantID();
            $id = EmailSubscriber::insertGetId($data);

            return redirect()->back()->with('alert_success','Subscriber entry saved successfully');
        }

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->only('id','mailer_types','name','email');
        if($data && $request->id){
            $id = \Helper::encryptor('decrypt',$request->id);
            $data['tenant_id'] = \Helper::getTenantID();

            EmailSubscriber::find($id)->update($data);

            return redirect()->back()->with('alert_success','Mailer Entry updated successfully');
        }

        return back();
    }

    public function updateStatus(Request $request)
    {
        $data = $request->only('id','status');
        if($data && $data['id']){
            $id = \Helper::encryptor('decrypt',$data['id']);
            $user = EmailSubscriber::find($id);
            if($user){
                $state = "statusChanged";
                $user->status = $data['status'];
                $user->save();
            }
        }
        return response()->json($state,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if($request->id){
            $id = \Helper::encryptor('decrypt',$request->id);
            EmailSubscriber::find($id)->forceDelete();
        }

        return back()->with('alert_info','Mailer Entry removed successfully');
    }
}
