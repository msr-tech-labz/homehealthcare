<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Entities\Masters\EmployeeSource;

class EmployeeSourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employeesources = EmployeeSource::whereTenantId(\Helper::getTenantID())->get();

        return view('administration.employeesource',compact('employeesources'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('branch_id','name','description','status');
        if($data){
            $data['tenant_id'] = \Helper::getTenantID();

            EmployeeSource::create($data);

            return redirect()->back()->with('alert_success','Employee Source added successfully');
        }

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only('branch_id','name','description','status');
        if($data && $id){
            $id = \Helper::encryptor('decrypt',$id);
            $data['tenant_id'] = \Helper::getTenantID();

            EmployeeSource::find($id)->update($data);

            return redirect()->back()->with('alert_success','Employee Source updated successfully');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id){
            $id = \Helper::encryptor('decrypt',$id);
            EmployeeSource::find($id)->forceDelete();
        }

        return back()->with('alert_info','Employee Source removed successfully');
    }
}
