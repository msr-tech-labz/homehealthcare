<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Entities\Caregiver\CaregiverSkill;


use App\Http\Requests\Administration\StoreSkill;
use App\Entities\Masters\Skill;

class SkillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        if(session('orgType') == 'Physiotherapy'){
            $skills = Skill::where('skill_type','PHYSIOTHERAPY')->get();
        }else{
            $skills = Skill::get();
        }

        return view('administration.skill',compact('skills'));
    }    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSkill $request)
    {
        //dd($request);

        $data = $request->only('skill_name','description');
        if($data){
            if(session('orgType') == 'Physiotherapy'){
                $data['skill_type'] = 'PHYSIOTHERAPY';
            }
            $data['tenant_id'] = \Helper::getTenantID();
            Skill::create($data);

            return redirect()->back()->with('alert_success','Skill added successfully');
        }

        return back();
    }    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreSkill $request, $id)
    {        
        $data = $request->only('skill_name','description');
        if($data && $id){
            $id = \Helper::encryptor('decrypt',$id);
            if(session('orgType') == 'Physiotherapy'){
                $data['skill_type'] = 'PHYSIOTHERAPY';
            }
            $data['tenant_id'] = \Helper::getTenantID();
            
            Skill::find($id)->update($data);

            return redirect()->back()->with('alert_success','Skill updated successfully');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id){
            $id = \Helper::encryptor('decrypt',$id);
            $skillcount = CaregiverSkill::whereSkillId($id)->count();
            if($skillcount){
                return back()->with('alert_warning','This Skill is already associated with one/more Employee, please delete the skill from the respective employees before deleting it from the Masters.');
            }else{
            Skill::find($id)->forceDelete();
            return back()->with('alert_info','Skill removed successfully');
            }
        }
        return back()->with('alert_danger','Skill not present in the database');;
    }
}
