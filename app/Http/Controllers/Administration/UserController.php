<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Http\Requests\Administration\StoreUser;
use App\Entities\User;
use App\Entities\Caregiver\Caregiver;
use App\Entities\Caregiver\ProfessionalDetails;

use App\Entities\Console\Subscriber;

use App\Entities\Corporates\CaregiverLog;

use App\Entities\Role;
use App\Entities\Masters\Branch;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::select('id','branch_id','user_id','user_type','full_name','email','role_id','status')->with(['branch' => function($q){
                            $q->addSelect('id','branch_name');
                       },'role' => function($q){
                           $q->addSelect('id','name','display_name');
                       },'caregiver' => function($q){ $q->addSelect('id','employee_id','mobile_number'); }])->get();

        if(\Helper::encryptor('decrypt',session('utype')) == 'Aggregator'){
            $users = User::whereTenantId(\Helper::getTenantID())
                          ->select('id','branch_id','user_id','user_type','full_name','email','role_id','status')->with(['branch' => function($q){
                                $q->addSelect('id','branch_name');
                           },'role' => function($q){
                               $q->addSelect('id','name','display_name');
                           },'caregiver' => function($q){ $q->addSelect('id','employee_id','mobile_number'); }])->get();
        }
        $roles  = Role::select('id','name','display_name')->get();

        return view('administration.user',compact('users','roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUser $request)
    {
        $data = $request->only('branch_id','full_name','email','mobile','password','role_id');

        $password = $data['password'];
        unset($data['password']);

        if(!empty($password)) $data['password'] = $password;

        if($data){
            if(\Helper::encryptor('decrypt',session('utype')) == 'Aggregator'){
                $data['tenant_id'] = \Helper::getTenantID();
                $data['user_type'] = 'Aggregator';
            }
            $user = User::create($data);

            $user->attachRole($data['role_id']);

            return redirect()->back()->with('alert_success','User added successfully');
        }

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->id;
        $data = $request->only('full_name','email','password','role_id');

        $password = $data['password'];
        unset($data['password']);

        if(!empty($password)) $data['password'] = $password;

        if($data && $id){
            $id = \Helper::encryptor('decrypt',$id);
            $user = User::find($id);
            $user->update($data);

            if($user->user_id > 0){
                if(!empty($request->email))
                    Caregiver::find($user->user_id)->update(['email' => $request->email]);
            }
            // Remove Existing Permissions
            \DB::table('auth_role_user')->where('user_id',$id)->delete();
            $user->attachRole($data['role_id']);

            
            $employee_id = Caregiver::whereId($user->user_id)->pluck('employee_id')->first();
            // connecting to centralconsole db
            $subscribers = Subscriber::select('database_name')->whereCorpId(\Helper::encryptor('decrypt',session('corp_id')))->get();
            \DB::disconnect('tenant');
            // connecting to corporate db
            \Config::set('database.connections.tenant.database',\Helper::encryptor('decrypt',session('corpDbName')));
            \DB::setDefaultConnection('tenant');
            CaregiverLog::where('employee_id',$employee_id)->update(['email' => $request->email]);
            // connecting to all branch db
            foreach ($subscribers as $subscriber) {
                if(session('dbName') != $subscriber->database_name){
                    \DB::disconnect('tenant');
                    \Config::set('database.connections.tenant.database',$subscriber->database_name);
                    \DB::setDefaultConnection('tenant');
                    Caregiver::where('employee_id',$employee_id)->update(['email' => $request->email]);
                }
            }
            \DB::disconnect('tenant');

            return redirect()->back()->with('alert_success','User updated successfully');
        }

        return back();
    }

    public function reloadRolePrivileges()
    {
        $users = User::whereStatus(1)->whereNotNull('role_id')->where('role_id','>',0)->get();
        if($users){
            foreach ($users as $user) {
                // Remove Existing Permissions
                \DB::table('auth_role_user')->where('user_id',$user->id)->delete();

                $user->attachRole($user->role_id);
            }

            return back()->with('alert_success','Role privileges has been reloaded');
        }

        return back();
    }

    public function updateStatus(Request $request)
    {
        $data = $request->all('id','status');
        $state = "error";
        if($data && $data['id'] != null){
            $id = \Helper::encryptor('decrypt',$data['id']);
            $user = User::find($id);
            if($user){
                $state = "statusChanged";
                $countActiveUsers = User::where('status','1')->count();
                \Config::set('database.default','central');
                $subscriber = Subscriber::whereId(\Helper::getTenantID())->first();
                //max reached and inactivating user
                if($data['status'] == 0){
                    \DB::setDefaultConnection('tenant');
                    $date = date('Y-m-d');
                    $countSchedules = isset($user->caregiver->schedule)?$user->caregiver->schedule->whereCaregiverId($user->user_id)->whereStatus('Pending')->where('schedule_date', '>=', $date)->count():'0';
                    if($countSchedules == 0){
                        $user->status = $data['status'];
                        $user->save();
                    }else{
                        $state = "userHasSchedule";
                        return response()->json($state,200);
                    }
                }elseif($subscriber->users_limit <= $countActiveUsers && $data['status'] == 1){
                    \DB::setDefaultConnection('tenant');
                    $state = "userReachedMaxActive";
                    return response()->json($state,200);
                }else{
                    \DB::setDefaultConnection('tenant');
                    $user->status = $data['status'];
                    $user->save();
                }
            }
        }

        return response()->json($state,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if($request->id){
            $id = \Helper::encryptor('decrypt',$request->id);
            User::find($id)->forceDelete();
        }

        return back()->with('alert_info','User removed successfully');
    }

    /*-----------------------------------------------
    | API Functions
    |-----------------------------------------------
    |*/

    public function getProfile(Request $request)
    {
        $id = $request->id;
        $results = false;

        $profile = User::whereId($id)->whereStatus(1)->first();
        if($profile){
            $results = [
                'id' => \Helper::str($profile->id),
                'full_name' => \Helper::str($profile->full_name),
                'email' => \Helper::str($profile->email),
                'mobile' => \Helper::str($profile->mobile),
                'status' => \Helper::str($profile->status),
            ];
        }

        $data['status_code'] = 200;
        $data['result'] = $results;

        return response()->json($data, 200);
    }

}
