<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Entities\Caregiver\ProfessionalDetails;

use App\Http\Requests\Administration\StoreSpecialization;
use App\Entities\Masters\Specialization;


class SpecializationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session('orgType') == 'Physiotherapy'){
            $specializations = Specialization::where('specialization_type','PHYSIOTHERAPY')->get();
        }else{
            $specializations = Specialization::get();
        }

        return view('administration.specialization',compact('specializations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSpecialization $request)
    {
        //dd($request);

        $data = $request->only('branch_id','specialization_name','description');
        if($data){
            if(session('orgType') == 'Physiotherapy'){
                $data['specialization_type'] = 'PHYSIOTHERAPY';
            }
            $data['tenant_id'] = \Helper::getTenantID();
            Specialization::create($data);

            return redirect()->back()->with('alert_success','Specialization added successfully');
        }

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreSpecialization $request, $id)
    {
        $data = $request->only('branch_id','specialization_name','description');
        if($data && $id){
            $id = \Helper::encryptor('decrypt',$id);
            if(session('orgType') == 'Physiotherapy'){
                $data['specialization_type'] = 'PHYSIOTHERAPY';
            }
            $data['tenant_id'] = \Helper::getTenantID();
            Specialization::find($id)->update($data);

            return redirect()->back()->with('alert_success','Specialization updated successfully');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id){
            $id = \Helper::encryptor('decrypt',$id);
            $specializationcount = ProfessionalDetails::whereSpecialization($id)->count();
            if($specializationcount){
                return back()->with('alert_warning','This Specialization is already associated with one/more Employee, please change the specialization from the respective employee(s) before deleting it from the Masters.');
            }else{
            Specialization::find($id)->forceDelete();
            return back()->with('alert_info','Specialization removed successfully');
            }
        }

        return back()->with('alert_info','Specialization not present in the database');
    }
}
