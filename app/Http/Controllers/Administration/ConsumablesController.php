<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Entities\Masters\Taxrate;
use App\Entities\Masters\Consumables;

class ConsumablesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $taxRates = Taxrate::all();
        $consumables = Consumables::whereTenantId(\Helper::getTenantID())->get();

        return view('administration.consumables',compact('taxRates','consumables'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('branch_id','consumable_name','description','consumable_price','tax_id');
        if($data){
            $data['tenant_id'] = \Helper::getTenantID();
            Consumables::create($data);

            return redirect()->back()->with('alert_success','Consumable added successfully');
        }

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only('branch_id','consumable_name','description','consumable_price','tax_id');
        if($data && $id){
            $id = \Helper::encryptor('decrypt',$id);
            $data['tenant_id'] = \Helper::getTenantID();

            Consumables::find($id)->update($data);

            return redirect()->back()->with('alert_success','Consumable updated successfully');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id){
            $id = \Helper::encryptor('decrypt',$id);
            Consumables::find($id)->forceDelete();
        }

        return back()->with('alert_info','Consumable removed successfully');
    }
}
