<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Entities\Profile;
use App\Entities\Settings;

class ProfileController extends Controller
{
    public static $id = 0;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $p = Profile::first();
        $b = Settings::first();

        if($p){
            return view('administration.profile', compact('p','b'));
        }

        return redirect('/home');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateCompanyDetails(Request $request)
    {
        $id = \Helper::encryptor('decrypt',session('uid'));

        $data = $request->only('organization_name','organization_short_name','organization_address','organization_area','organization_city','organization_zipcode','organization_state','organization_country','phone_number','landline_number','email','website','contact_person');

        $profile = Profile::withoutGlobalScopes()->whereTenantId($id)->first();

        if($data && $profile){
            $res = $profile->update($data);

            //dd([$data, $profile, $res,\DB::getQueryLog()]);

            // Upload Organization Logo
            if($request->hasFile('organization_logo')){
                $profile = Profile::first();

                $logo = $request->file('organization_logo');

                $path = public_path('uploads/provider/'.session('tenant_id'));
                if(!is_dir($path)){
                    \File::makeDirectory($path,0777,true);
                }
                $fileNameWithoutExt = \Helper::encryptor('encrypt',$profile->id);
                $fileName = \Helper::encryptor('encrypt',$profile->tenant_id).'.'.$logo->extension();
                $mask = $fileNameWithoutExt."."."*";
                array_map('unlink', glob($path.'/'.$mask));
                $res = $logo->move($path,$fileName);

                // Update organization logo file path
                $profile->organization_logo = $fileName;
                $profile->save();

                session()->forget('organization_logo');
                session()->put('organization_logo',$fileName);
            }
                session()->forget('organization_name');
                session()->put('organization_name',$request->organization_name);

            return redirect()->back()->with('alert_success','Profile updated successfully');
        }

        return back();
    }

    public function updateSettings(Request $request)
    {
        $id = \Helper::encryptor('decrypt',session('uid'));

        $data = $request->only('auth_signatory','signtory_pos','proforma_invoice_inits','proforma_invoice_start_no','invoice_inits','invoice_start_no','receipt_inits','receipt_start_no','credit_inits','credit_start_no','cutoff_period','patient_id_prefix','patient_id_start_no','employee_id_prefix','pan_number','gstin','cin','payment_due_date','schedule_creation_for_outstanding_customer','patient_email_mandatory');

        $profile = new Settings;
        if(Settings::count() > 0){
            $profile = Settings::first();
        }

        if($data && $profile)
        {
            $res = $profile->updateOrCreate(['tenant_id' => $id],$data);
            $this->updateSessionValues();
        }
        return redirect()->back()->with('alert_success','Profile updated successfully');
    }

    public function updateBankingDetails(Request $request)
    {
        $id = \Helper::encryptor('decrypt',session('uid'));

        $data = $request->only('invoice_payee','bank_name','bank_address','acc_num','ifsc_code');

        $profile = new Settings;
        if(Settings::count() > 0){
            $profile = Settings::first();
        }

        if($data && $profile)
        {
            $res = $profile->updateOrCreate(['tenant_id' => $id],$data);
            $this->updateSessionValues();
        }
        return redirect()->back()->with('alert_success','Profile updated successfully');
    }

    public function updateSessionValues()
    {
        $settings = Settings::first();
        if($settings){
            $settings = collect($settings)->except('id','tenant_id','created_at','updated_at','deleted_at');
            foreach ($settings as $key => $value) {
                session()->put('settings.'.$key,\Helper::encryptor('encrypt',$value));
            }
        }
    }
}
