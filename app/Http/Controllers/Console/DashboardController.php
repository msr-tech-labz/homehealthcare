<?php
namespace App\Http\Controllers\Console;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Entities\Console\Subscriber;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $stats = Subscriber::stats();
        $topAccounts = Subscriber::getAccounts('Paid',5);

        $trialAccounts = Subscriber::getAccounts('Trial');
 
        return view('console.dashboard', compact('stats','topAccounts','trialAccounts'));
    }

    public function login(Request $request)
    {
        return view('console.dashboard');
    }
}
