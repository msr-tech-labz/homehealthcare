<?php
namespace App\Http\Controllers\Console;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Entities\Console\Admin;

class ConsoleAuthentication extends Controller
{
    public function login()
    {
        if(session('console.isLoggedIn')){
            return redirect('/admin/dashboard');
        }
        return view('auth.console-login');
    }

    public function authenticate(Request $request)
    {
        $username = $request->email;
        $password = $request->password;
        session()->flush();
        $user = Admin::whereEmail($username)->first();        
        if($user){
            if(\Hash::check($password, $user->password)){
                session()->put('console.isLoggedIn',true);
                session()->put('console.admin_fname',$user->first_name);
                session()->put('console.admin_lname',$user->last_name);
                session()->put('console.admin_email',$user->email);

                return redirect()->intended('/admin/dashboard');
            }else{
                return back()->with('alert_error','Password Mismatch !');
            }
        }
        return back()->with('alert_error','User Not Found !');
    }

    public function logout()
    {
        session()->forget('console.isLoggedIn');
        session()->flush();

        return redirect()->route('console.login');
    }
}
