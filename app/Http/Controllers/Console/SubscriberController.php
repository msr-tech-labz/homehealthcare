<?php
namespace App\Http\Controllers\Console;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Entities\Console\Admin;
use App\Entities\Console\Subscriber;
use App\Entities\Console\SubscriptionChangeLogs;
use App\Entities\Console\SubscriptionInvoice;
use App\Entities\Console\SubscriptionInvoicePayments;

use App\Entities\SubscriptionPayment;

use App\Entities\Caregiver\Caregiver;
use App\Entities\Profile;
use App\Entities\Schedule;
use App\Entities\User;
use Helper;
use Carbon\Carbon;
//New Models
use App\Entities\Console\Corporates;
use App\Entities\Corporates\CorpProfile;
use App\Entities\Corporates\CorpUsers;

class SubscriberController extends Controller
{
    //Administration Index Page
    public function administrationIndex()
    {
        return view('console.utilities.index');
    }
    //Global SQL Query Executor Page
    public function administrationSqlExecutor()
    {   $provider = array();
        // $subscribers = Subscriber::active()->get();
        $subscribers = Subscriber::get();
        foreach($subscribers as $subscriber){
            \DB::purge('api');
            self::setTenantConnection($subscriber->id);
            $pr = Profile::withoutGlobalScopes()->whereTenantId($subscriber->id)->first();
            if($pr){
                $provider[] = [
                    'profile' => $pr->organization_name,
                    'database_name' => $subscriber->database_name,
                    'tenant_id' => $subscriber->id,
                ];
            }
            \DB::purge('api');
        }
        return view('console.utilities.sqlExecutor',compact('provider'));
    }
    //Run Global SQL Query
    public function administrationRunGlobalSqlQuery(Request $request)
    {
        $sql = $request->sql;
        $databases = $request->items;
        $error['error'] = false;

        foreach($databases as $db){
            $dbname = Subscriber::whereId($db)->value('database_name');
            \DB::purge('api');
            $conn = mysqli_connect('127.0.0.1','root','root123');
            //$conn = mysqli_connect('139.59.33.106','hhms','%p?Srd2?I[v;');
            self::setTenantConnection($db);

            mysqli_select_db($conn,$dbname);
            $result = mysqli_multi_query($conn,$sql);

            if(mysqli_errno($conn)){
                $error['error'] = true;
                $error['msg'] = mysqli_error($conn);

                return $error;
            }

            mysqli_close($conn);
            \DB::purge('api');
        }
    }
    // New Logic for Multi Branch data

    public function activeUSersList()
    {
        $subscribers = Subscriber::get();
        return view('console.utilities.active_users',compact('subscribers'));
    }

    public function activeUsersListStatus(Request $request)
    {
        $id = \Helper::encryptor('decrypt',$request->id);
        $data = Subscriber::find($id);
        $state = "error";
    
        if($request->status == 1){
            $data->status= "Active";
            $data->save();
            $state="success";
        }else{
            $data->status="Inactive";
            $data->save();
            $state="success";
        }
       
        return response()->json($state,200);
    }

    //Corporate Index
    public function corporateIndex()
    {
        $corporates = Corporates::all();        

        return view('console.subscribers.corporate-index', compact('corporates'));
    }
    //View Corporate
    public function viewCorporate($corpId)
    {
        $corporate = Corporates::find(Helper::encryptor('decrypt',$corpId));
        
        \Config::set('database.connections.api.database',$corporate->db_name);
        \DB::setDefaultConnection('api');
        $corpProfile = CorpProfile::first();
        \DB::purge('api');
        \DB::setDefaultConnection('central');

        $assoBranches = Subscriber::whereUserType('Provider')->whereCorpId($corporate->id)->get();
        foreach ($assoBranches as $key => $branch) {
            \Config::set('database.connections.api.database',$branch->database_name);
            \DB::setDefaultConnection('api');
            $assoBranches[$key]->total_users = User::count();
            $assoBranches[$key]->active_users = User::whereStatus(1)->count();
            $assoBranches[$key]->city = Profile::first()->value('organization_city');
            $assoBranches[$key]->state = Profile::first()->value('organization_state');
            \DB::purge('api');
            \DB::setDefaultConnection('central');
        }

        return view('console.subscribers.corporate-view',compact('corporate','corpProfile','assoBranches'));
    }
    //Update Corporate Basic Details
    public function updateCorporate(Request $request)
    {
        $corporate = Corporates::find(Helper::encryptor('decrypt',$request->corp_id));
        $corporate->organization = $request->corporate_name;
        $corporate->type = $request->corporate_type;
        $corporate->status = $request->corporate_status;
        $corporate->expiry = ($request->corporate_expiry == 'NA')?null:Carbon::parse($request->corporate_expiry)->format('Y-m-d');
        $corporate->save();

        if($request->corporate_status == 'Inactive'){
            Subscriber::whereCorpId(Helper::encryptor('decrypt',$request->corp_id))->update(['status'=>'Inactive']);
        }else{
            Subscriber::whereCorpId(Helper::encryptor('decrypt',$request->corp_id))->update(['status'=>'Active']);
        }

        \Config::set('database.connections.api.database',$corporate->db_name);
        \DB::setDefaultConnection('api');
        $corpProfile = CorpProfile::first();
        $corpProfile->email = $request->corporate_email;
        $corpProfile->address = $request->corporate_address;
        $corpProfile->state = $request->corporate_state;
        $corpProfile->landline_number = $request->corporate_landline;
        $corpProfile->save();
        \DB::purge('api');
        \DB::setDefaultConnection('central');

        return back()->with('alert_success','Corporate Details Updated !');
    }
    //Change Branch Status
    public function updateBranchStatus(Request $request)
    {
        if(empty($request->branchID)){
            return back()->with('alert_error','Branch not found, please try again !');
        }
        $subscriber = Subscriber::whereId(\Helper::encryptor('decrypt',$request->branchID))->first();
        if($request->status == "Active"){
            $subscriber->status = 'Active';
        }else{
            $subscriber->status = 'Inactive';
        }

        $subscriber->save();
        $state = 'statusChanged';

        return $state;
    }
    //View Corporate Form
    public function viewCorporateForm()
    {
        return view('console.subscribers.corporate-create-form');
    }
    //Check Corp ID availability
    public function checkCorpID(Request $request)
    {
        $state = 'Available';
        $corp = Corporates::whereCorporateId($request->corpID)->first();
        if(count($corp) && isset($corp)){
            $state = 'Unavailable';
        }

        return $state;
    }
    //Create Corporate
    public function createCorporate(Request $request)
    {
        \DB::beginTransaction();
        try{
            $corporate               = new Corporates;
            $corporate->corporate_id = $request->corporate_id;
            $corporate->type         = $request->corporate_type;
            $corporate->expiry       = ($request->corporate_expiry == 'NA')?null:Carbon::parse($request->expiry)->format('Y-m-d');
            $corporate->organization = $request->corporate_name;
            $corporate->save();
            $corporate->db_name      = 'corp'.$corporate->id;
            $corporate->update(['id' => $corporate->id],['db_name' => $corporate->db_name]);

            $link = mysqli_connect('127.0.0.1','root','root123');
            $res = $this->checkAndCreateCorporateDB($corporate->db_name);
            if($res && $link){
                \Config::set('database.connections.api.database',$corporate->db_name);
                \DB::setDefaultConnection('api');
                $corpProfile = new CorpProfile;
                $corpProfile->email = $request->corporate_profile_email;
                $corpProfile->name = $request->corporate_name;
                $corpProfile->address = $request->corporate_address;
                $corpProfile->state = $request->corporate_state;
                $corpProfile->landline_number = $request->corporate_landline;
                $corpProfile->save();

                $corpUser = new CorpUsers;
                $corpUser->full_name = $request->corporate_profile_full_name;
                $corpUser->email = $request->corporate_profile_email;
                $corpUser->password = $request->corporate_profile_password;
                $corpUser->save();
                \DB::commit();
                \DB::purge('api');
                \DB::setDefaultConnection('central');
            }else{
                \DB::rollBack();
            }
        } catch (\Exception $e){
            \DB::rollback();
        }

        return redirect()->route('console.corporates');
    }
    //Create Corp Database
    public function checkAndCreateCorporateDB($database)
    {
        if($database){
            $r = \DB::select('show databases where `Database` = "'.$database.'"');

            if(count($r) == 0){
                $res = \DB::connection()->statement('CREATE DATABASE '.filter_var($database, FILTER_SANITIZE_STRING).' DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci');

                $host = \Config::get('database.connections.central.host');
                $username = \Config::get('database.connections.central.username');
                $password = \Config::get('database.connections.central.password');

                $db = new \PDO("mysql:host=$host;dbname=".$database.";", $username, $password);
                $sql = file_get_contents(storage_path().'/db_structure/corporate_dbstructure.sql');
                $qr = $db->exec($sql);
                return true;
            }
        }
        return false;
    }
    //Branch Update
    public function branchUpdate(Request $request)
    {
        $subBranch = Subscriber::whereId(Helper::encryptor('decrypt',$request->branch_id))->first();
        $subBranch->users_limit = $request->users_limit;
        $subBranch->charge_per_user = $request->charge_per_user;
        $subBranch->save();

        return back()->with('alert_success','Branch Subscription status updated successfully !');
    }
    //Create Corporate Branch Form
    public function viewBranchForm($corpId)
    {
        $corp_id = Helper::encryptor('decrypt',$corpId);
        $corporate = Corporates::find($corp_id);
        return view('console.subscribers.corporate-create-branch',compact('corporate'));
    }
    //Check Branch ID availability
    public function checkBranchID(Request $request)
    {
        $state = 'Available';
        $corp = Subscriber::whereOrgCode($request->subscriberId)->first();
        if(isset($corp) && $corp){
            $state = 'Unavailable';
        }

        return $state;
    }
    //Create Corporate Branch
    public function createBranch(Request $request)
    {
        \DB::beginTransaction();
        try{
            $corp_id = \Helper::encryptor('decrypt',$request->corp_id);
            $data = $request->only('organization_name','organization_address','organization_area','organization_city','organization_zipcode','organization_state','organization_country','contact_person','phone_number','landline_number','email');

            $data['created_at'] = $data['updated_at'] = date('Y-m-d H:i:s');

            if($request->instance_type == 'Paid'){
                $trialEndDate = null;
            }else{
                $trialEndDate = Carbon::parse($request->trial_end_date)->format('Y-m-d');
            }

            // Create entry in Subscribers table
            $newBranch                  = new Subscriber;
            $newBranch->corp_id         = $corp_id;
            $newBranch->user_type       = 'Provider';
            $newBranch->org_code        = $request->org_code;
            $newBranch->users_limit     = $request->users_limit;
            $newBranch->charge_per_user = $request->charge_per_user;
            $newBranch->instance_type   = $request->instance_type;
            $newBranch->trial_end_date  = $trialEndDate;
            $newBranch->status          = 'Active';
            $newBranch->created_at      = date('Y-m-d H:i:s');
            $newBranch->updated_at      = date('Y-m-d H:i:s');

            $newBranch->save();
            $newBranch->database_name   = 'aphhp0'.$newBranch->id;
            $newBranch->save();

            $tenantId = $data['tenant_id'] = $newBranch->id;
            $user = [
                'tenant_id'  => $data['tenant_id'],
                'branch_id'  => 0,
                'user_id'    => 0,
                'user_type'  => 'Admin',
                'created_by' => 'Default',
                'full_name'  => $request->admin_name,
                'email'      => $request->admin_email,
                'password'   =>  bcrypt($request->admin_password),
                'role_id'    => 1,
                'status'     => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
            //$link = mysqli_connect('139.59.33.106','hhms','%p?Srd2?I[v;');
            $link   = mysqli_connect('127.0.0.1','root','root123');
            $res    = $this->checkAndCreateBranchDB($newBranch->database_name);

            $dataOrganization_name    = $data['organization_name'];
            $dataEmail                = $data['email'];
            $dataOrganization_address = $data['organization_address'];
            $dataOrganization_area    = $data['organization_area'];
            $dataOrganization_city    = $data['organization_city'];
            $dataOrganization_zipcode = $data['organization_zipcode'];
            $dataOrganization_state   = $data['organization_state'];
            $dataOrganization_country = $data['organization_country'];
            $dataContact_person       = $data['contact_person'];
            $dataPhone_number         = $data['phone_number'];

            // Save Provider details to respective provider's db
            \Config::set('database.connections.tenant.database',$newBranch->database_name);
            \Config::set('database.default','tenant');
            \DB::setDefaultConnection('tenant');
            //Insert Branch Profile Details
            Profile::insert($data);
            //Add Profile Image
            if($request->hasFile('organization_logo')){
                $profile = Profile::first();

                $logo = $request->file('organization_logo');

                $path = public_path('uploads/provider/'.Helper::encryptor('encrypt',$tenantId));
                if(!is_dir($path)){
                    \File::makeDirectory($path,0777,true);
                }
                $fileNameWithoutExt = \Helper::encryptor('encrypt',$profile->id);
                $fileName = \Helper::encryptor('encrypt',$profile->tenant_id).'.'.$logo->extension();
                $mask = $fileNameWithoutExt."."."*";
                array_map('unlink', glob($path.'/'.$mask));
                $res = $logo->move($path,$fileName);

                // Update organization logo file path
                $profile->organization_logo = $fileName;
                $profile->save();
            }
            //Insert Admin User Details
            User::insert($user);
            //Give the Inserted user an Admin Role ~~~ Admin role mapping is already done in the sql file
            $queryRole = "INSERT INTO auth_roles ".
            "(tenant_id,branch_id,name,created_by,display_name,description,created_at,updated_at) ".
            " VALUES ".
            "('$tenantId',0,'admin','Default','Administration','Admin',now(),now()),".
            "('$tenantId',0,'standard-user','Default','Standard User','Standard User with Least Privileges',now(),now())";

            $queryTax = "INSERT INTO master_taxrates ".
            "(id,tenant_id,created_by,tax_name,tax_rate,type,`order`,status,created_at,updated_at) ".
            " VALUES ".
            "(1,'$tenantId','Default','GST@0%',0.00,'Percentage',1,1,now(),now()),".
            "(2,'$tenantId','Default','GST@5%',5.00,'Percentage',2,1,now(),now()),".
            "(3,'$tenantId','Default','GST@12%',12.00,'Percentage',3,1,now(),now()),".
            "(4,'$tenantId','Default','GST@18%',18.00,'Percentage',4,1,now(),now()),".
            "(5,'$tenantId','Default','GST@28%',28.00,'Percentage',5,1,now(),now())";

            $queryBranch = "INSERT INTO master_branches ".
            "(id,tenant_id,branch_name,branch_code,branch_email,branch_address,branch_area,branch_city,branch_zipcode,branch_state,branch_country,branch_contact_person, branch_contact_number,created_at,updated_at) ".
            " VALUES ".
            "(1,'$tenantId','$dataOrganization_name','01','$dataEmail','$dataOrganization_address','$dataOrganization_area','$dataOrganization_city','$dataOrganization_zipcode','$dataOrganization_state','$dataOrganization_country','$dataContact_person','$dataPhone_number',now(),now())";

            mysqli_select_db($link,$newBranch->database_name);

            $retvalrole = $link->multi_query($queryRole);
            if(!$retvalrole)
            {
              \DB::rollback();
              return redirect()->route('console.corporates')->with('alert_error','Could not create table auth_roles. Transaction Rolled Back');
            }

            $retvaltax = $link->multi_query($queryTax);
            if(!$retvaltax)
            {
                \DB::rollback();
                return redirect()->route('console.corporates')->with('alert_error','Could not create table master_taxrates. Transaction Rolled Back');
            }


            $retvalbranch = $link->multi_query($queryBranch);
            if(!$retvalbranch)
            {
              \DB::rollback();
              return redirect()->route('console.corporates')->with('alert_error','Could not create table master_branches. Transaction Rolled Back');
            }
            \DB::commit();
            mysqli_close($link);
            //Move back to central connection
            \Config::set('database.default','central');
            \DB::setDefaultConnection('central');
        } catch (\Exception $e){
            \DB::rollback();
            return redirect()->route('console.corporates')->with('alert_error','Something went Wrong. Transaction Rolled Back');
        }

        return redirect()->route('console.corporates')->with('alert_success','New Branch added successfully');
    }
    //Create Branch Database
    public function checkAndCreateBranchDB($database)
    {
        if($database){
            $r = \DB::select('show databases where `Database` = "'.$database.'"');

            if(count($r) == 0){
                $res = \DB::connection()->statement('CREATE DATABASE '.filter_var($database, FILTER_SANITIZE_STRING).' DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci');

                $host     = \Config::get('database.connections.central.host');
                $username = \Config::get('database.connections.central.username');
                $password = \Config::get('database.connections.central.password');
                
                $db       = new \PDO("mysql:host=$host;dbname=".$database.";", $username, $password);
                $sql      = file_get_contents(storage_path().'/db_structure/branch_dbstructure.sql');
                $qr       = $db->exec($sql);
                return true;
            }
        }
        return false;
    }
    //Billing Index
    public function viewBillingPage(Request $request)
    {
        $corporates = Corporates::get();
        $branches = Subscriber::whereUserType('Provider')->get();
        $subInvs = SubscriptionInvoice::orderBy('created_at','desc')->get();
        return view('console.billing.index',compact('corporates','branches','subInvs'));
    }

    public function viewActiveUsers(Request $request)
    {
       
        if(!empty($request->usage_duration)){
            $subscriber = Subscriber::whereId($request->corporate_branch)->first();
            $period = explode(" - ",$request->usage_duration);

            $start = Carbon::parse($period[0])->format('Y-m-d');
            if(isset($period[0]) && !isset($period[1])){
                $end = Carbon::parse($period[0])->format('Y-m-d');
            }else{
                $end = Carbon::parse($period[1])->format('Y-m-d');
            }

            if(isset($subscriber) && $subscriber){
                \Config::set('database.connections.api.database',$subscriber->database_name);
                \DB::setDefaultConnection('api');
                $caregivers = Schedule::select('caregiver_id')->whereDate('schedule_date','>=',$start)
                                ->whereDate('schedule_date','<=',$end)
                                ->where('caregiver_id','>',0)
                                ->distinct('caregiver_id')->get();
                $data[] = [
                    'user' => Subscriber::whereId($request->corporate_branch)->pluck('org_code')->first(),
                    'period' => $start = Carbon::parse($period[0])->format('M').'-'.$start = Carbon::parse($period[0])->format('Y'),
                ];       
                \DB::purge('api');
                \DB::setDefaultConnection('central');
            }
        }else{
            $caregivers = [];
            $data[] = [
                'user' => '',
                'period' => '',
            ]; 
        }
        
        $corporates = Corporates::get();
        $branches = Subscriber::whereUserType('Provider')->get();
        $subInvs = SubscriptionInvoice::orderBy('created_at','desc')->get();
        return view('console.active_users.index',compact('corporates','branches','subInvs','caregivers','data'));
    }

    //Get Scheduled Staff Count
    public function getScheduledStaffCount(Request $request)
    {
        $subscriber = Subscriber::whereId($request->branchID)->first();
        $period = explode(" - ",$request->range);

        $start = Carbon::parse($period[0])->format('Y-m-d');
        if(isset($period[0]) && !isset($period[1])){
            $end = Carbon::parse($period[0])->format('Y-m-d');
        }else{
            $end = Carbon::parse($period[1])->format('Y-m-d');
        }

        if(isset($subscriber) && $subscriber){
            \Config::set('database.connections.api.database',$subscriber->database_name);
            \DB::setDefaultConnection('api');
            $count = Schedule::whereDate('schedule_date','>=',$start)
                            ->whereDate('schedule_date','<=',$end)
                            ->where('caregiver_id','>',0)
                            ->distinct('caregiver_id')
                            ->count('caregiver_id');
            \DB::purge('api');
            \DB::setDefaultConnection('central');
            return $count;
        }
    }
    //Add Invoice
    public function addInvoice(Request $request)
    {
        $subInv = new SubscriptionInvoice;
        $subInv->corp_id = $request->corporate_office_new;
        $subInv->subscriber_id = $request->corporate_branch_new;
        $subInv->usage_duration = $request->usage_duration_new;
        $subInv->invoice_no = $request->invoice_no_new;
        $subInv->invoice_month = $request->invoice_month_new;
        $subInv->invoice_year = $request->invoice_year;
        $subInv->invoice_amount = $request->invoice_amount_new;
        $subInv->save();

        return back()->with('alert_success','Invoice Added Successfully');
    }
    //Update Invoice
    public function updateInvoice(Request $request)
    {
        $subInv = SubscriptionInvoice::whereId(Helper::encryptor('decrypt',$request->id))->first();
        if($request->invoice_amount_received_update != '' && $request->invoice_amount_received_update > 0){
            $subInv->amount_paid = $subInv->amount_paid + $request->invoice_amount_received_update;
        }
        $subInv->status = $request->status;

        $subInv->save();

        return back()->with('alert_success','Invoice Updated Successfully');
    }
    //Report of Credits
    public function viewProviderCreditPayments(Request $request)
    {
        $paymentsList = [];
        return view('console.reports.index',compact('paymentsList'));
    }    
}
