<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Entities\Console\Subscriber;
use App\Entities\Caregiver\Caregiver;
use App\Entities\Careplan;
use App\Entities\Masters\RateCard;
use App\Entities\Masters\Agency;
use App\Entities\Provider;
use App\Entities\Profile;
use App\Entities\Leaves;
use App\Entities\Lead;
use App\Entities\Schedule;
use App\Entities\Patient;
use App\Entities\LabTest;
use App\Entities\CaseForm;
use App\Entities\Caregiver\Verification;
use App\Entities\LeadRequests;
use App\Entities\AggregatorLeads;
use App\Entities\Notifications;
use App\Mail\DocumentVerifyEmail;
use App\Entities\CaseServices;
use App\Entities\PatientAddress;
use App\Entities\Attendance;

use Mail;
use Carbon\Carbon;

class AJAXController extends Controller
{

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view('index');
    }

    public function filterCaregiver(Request $request)
    {
        $filter = $request->only('bid','fd','td','ft','tt','s','e','el','gp','c','l','sk','sods','flag','dates','replacement','sda');
        $filterDates =  array_map('trim', explode(',', $filter['dates']));

        $fdays = array();
        if($filter['flag'] == 'interrupted'){
            foreach ($filterDates as $idates) {
                if(!in_array(date("D", strtotime($idates)), $fdays, true)){
                    array_push($fdays,date("D", strtotime($idates)));
                }
            }
        }

        $onDutyStaff = [];
        $patientCoordinates = '';
        $patientID = \Helper::encryptor('decrypt',$request->pid);
        $patient = Patient::find($patientID);
        if(isset($patient) && !empty($patient->latitude)  && !empty($patient->longitude) && $request->sda == 'Default'){
            $patientCoordinates = $patient->latitude.'_'.$patient->longitude;
        }

        if(isset($patient) && $request->sda != 'Default'){
            $patientExtraAddr = PatientAddress::whereId($request->sda)->first();
            $patientCoordinates = $patientExtraAddr->latitude.'_'.$patientExtraAddr->longitude;
        }

        $caregivers = Caregiver::active()
                            ->select('id','employee_id','first_name','middle_name','last_name','mobile_number','email','current_city','current_state','latitude','longitude')
            			    ->whereHas('professional', function($q){
            				    $q->whereRoleOrDeployable('Caregiver','Yes');
            			    })
                            ->with(['professional' => function($q){
                                $q->addSelect('id','caregiver_id','specialization','role','deployable','experience','experience_level');
                            },'professional.specializations' => function($q){
                                $q->addSelect('id','specialization_name');
                            }, 'user' => function($q){
                                $q->addSelect('id','user_id','status');
                            }])
                            ->whereNotIn('caregivers.work_status',['Leave','Training'])
                            ->whereGenderPreference($filter['gp'])
                            ->whereNull('caregivers.deleted_at');

        if(isset($filter['replacement']) && $filter['replacement'] != '0'){
            $caregivers->where('id','!=', \Helper::encryptor('decrypt',$filter['replacement']));
        }

        if(isset($filter['fd']) && $filter['fd'] != ''){
            // this part is for allocation of staff on period range
            $fromDate = $toDate = \Helper::getDateObject($filter['fd'],'Y-m-d');
            if($filter['td'] != '')
                $toDate = \Helper::getDateObject($filter['td'],'Y-m-d');

            // Get $ids of scheduled caregivers and $lids of caregivers on leave
            $ids = Schedule::select('caregiver_id')->distinct('caregiver_id')
            ->whereBetween('schedule_date',[$fromDate,$toDate])
            ->whereIn('status',['Pending'])->whereNotNull('caregiver_id')->where('caregiver_id','!=','0')
            ->pluck('caregiver_id');

            $lids = Leaves::select('caregiver_id')->whereBetween('date',[$fromDate,$toDate])->whereIn('status',['Pending','Approved'])->pluck('caregiver_id')->toArray();
            $aids = Attendance::select('caregiver_id')->whereBetween('date',[$fromDate,$toDate])->whereStatus('A')->pluck('caregiver_id')->toArray();
        }else{
            // below part is for re-assign staff and for custom dates
            if($request->datesFromSchedules)
                $datesFromSchedules = explode(",",$request->datesFromSchedules);
            else if($request->dates)
                $datesFromSchedules = explode(",",$request->dates);

            $ids = Schedule::select('caregiver_id')->distinct('caregiver_id')
            ->whereIn('schedule_date',$datesFromSchedules)
            ->whereIn('status',['Pending'])->whereNotNull('caregiver_id')->where('caregiver_id','!=','0')
            ->pluck('caregiver_id');

            foreach ($datesFromSchedules as $date) {
                $lids = Leaves::select('caregiver_id')->whereDate('date',$date)->whereIn('status',['Pending','Approved'])->pluck('caregiver_id')->toArray();
                $aids = Attendance::select('caregiver_id')->whereDate('date',$date)->whereStatus('A')->pluck('caregiver_id')->toArray();
            }
        }

        $onDutyStaff = $ids->toArray();
        if($filter['sods'] == '0'){
            $mergedArr = array_unique(array_merge($ids->toArray(), $lids, $aids));
            $caregivers->whereNotIn('caregivers.id',array_values($mergedArr));
        }else if($filter['sods'] == '1'){
            $mergedArr = array_unique(array_merge($lids, $aids));
            $caregivers->whereNotIn('caregivers.id',array_values($mergedArr));
        }

        if(isset($filter['s']) && $filter['s'] != '0'){
            $caregivers->whereHas('professional', function($query) use ($filter){
                $query->where('specialization',$filter['s']);
            });
        }

        // if(!empty($fdays)){
        //     $caregivers->whereHas('professional',function($query) use ($fdays){
        //         foreach($fdays as $f){
        //             // $query->whereRaw('FIND_IN_SET(?,`working_days`)', $f);
        //             $query->where('working_days','like','%'.$f.'%');
        //         }
        //     });
        // }

        if(isset($filter['ft']) && isset($filter['tt']) && $filter['ft'] != '' && $filter['tt'] != ''){
                $parsedFromtime = Carbon::parse($filter['ft'])->format('H:i');
                $parsedTotime = Carbon::parse($filter['tt'])->format('H:i');
                $caregivers->whereHas('professional',function($query) use ($parsedFromtime,$parsedTotime){
                            $query->where('working_hours_from','<=',$parsedFromtime)
                                  ->where('working_hours_to','>=',$parsedTotime);
                });
        }

        if(isset($filter['e']) && $filter['e'] != '0'){
            $caregivers->whereHas('professional', function($query) use ($filter){
                $exp = explode("_",$filter['e']);
                $query->whereRaw(\DB::raw('CAST(experience AS UNSIGNED INTEGER) BETWEEN '.$exp[0].' AND '.$exp[1]));
            });
        }

        if(isset($filter['el']) && $filter['el'] != '0'){
            $caregivers->whereHas('professional', function($query) use ($filter){
                $exp_lv = $filter['el'];
                $query->where('experience_level',$exp_lv);
            });
        }

        if(isset($filter['l']) && !in_array('Any',$filter['l']) && $filter['l'] != ''){
            $caregivers->whereHas('professional', function($query) use ($filter){
                foreach($filter['l'] as $l){
                    $query->where('languages_known','like','%'.$l.'%');
                }
            });
        }

        if(isset($filter['sk']) && !in_array('0',$filter['sk']) && $filter['sk'] != ''){
            $query = $caregivers->with('caregiver_skills_filter');
            foreach($filter['sk'] as $sI){
                $query->whereHas('caregiver_skills_filter', function($q) use ($sI){
                    $q->where('skill_id', $sI);
                });
            }
        }

        $results = $caregivers->get();
        $result = [];

        if(count($results)){
            foreach($results as $r){
                $result[] = [
                    'id' => $r->id,
                    'employee_id' => $r->employee_id,
                    'full_name' => $r->first_name.' '.$r->middle_name.' '.$r->last_name,
                    'mobile' => $r->mobile_number,
                    'email' => $r->email,
                    'location' => $r->current_city.', '.$r->current_state,
                    'specialization' => isset($r->professional->specializations)?$r->professional->specializations->specialization_name:'-',
                    'experience' => isset($r->professional)?$r->professional->experience:'-',
                    'coordinates' => (!empty($r->latitude) && !empty($r->longitude))?$r->latitude.'_'.$r->longitude:'',
                    'level' => isset($r->professional->experience_level)?$r->professional->experience_level:'-',
                    'rating' => '3.2',
                    'onduty' => in_array($r->id, $onDutyStaff, TRUE)?true:false,
                ];
            }
        }
        return response()->json(['result' => $result, 'patient_location' => $patientCoordinates],200);
    }

    public function filterProvider(Request $request)
    {
        $result = [];
        $filter = $request->only('g','c','lat','lng');
        \Config::set('database.connections.tenant.database','apnacare_centralconsole');
        $providers = Subscriber::withoutGlobalScopes()->where('user_type','Provider')->get();

        if(count($providers)){
            foreach($providers as $p){
                \DB::purge('api');
                self::setTenantConnection($p->id);
                $pr = Profile::withoutGlobalScopes()->whereTenantId($p->id)->first();
                $branches = \App\Entities\Masters\Branch::withoutGlobalScopes()->whereTenantId($p->id);

                if(isset($filter['c']) && $filter['c'] != ''){
                    $branches->where('branch_city','like','%'.$filter['c'].'%');
                }

                if(isset($filter['a']) && $filter['a'] != ''){
                    $branches->where('branch_area','like','%'.$filter['a'].'%');
                }
                $branches = $branches->get();

                if(count($branches)){
                    foreach ($branches as $branch) {
                        // Provider Info
                        $cg = Caregiver::withoutGlobalScopes()->whereTenantId($pr->tenant_id)->whereBranchId($branch->id)
                                        ->whereHas('professional', function($q){
                                            $q->whereRole('Caregiver');
                                        })->whereWorkStatus('Available');
                        if(isset($filter['g']) && $filter['g'] != '0'){
                            $cg = $cg->where('gender',$filter['g']);
                        }
                        $cg = collect($cg->get());
                        $m_availability = $cg->where('gender','Male')->count();
                        $f_availability = $cg->where('gender','Female')->count();
                        $temp['details']['id'] = \Helper::encryptor('encrypt',$pr->tenant_id);
                        $temp['details']['branch_id'] = \Helper::encryptor('encrypt',$branch->id);
                        $temp['details']['db_id'] = \Helper::encryptor('encrypt',$pr->id).'_'.\Helper::encryptor('encrypt',$pr->database_name);
                        $temp['details']['orgcode'] = 'APHHP0'.$pr->tenant_id;
                        $temp['details']['organization_name'] = $pr->organization_name;
                        $temp['details']['organization_short_name'] = $pr->organization_name;
                        $temp['details']['branch_name'] = $branch->branch_name;
                        $temp['details']['logo'] = $pr?$pr->organization_logo:'';
                        $temp['details']['city'] = $branch?$branch->branch_city:'-';
                        $temp['details']['area'] = $branch?$branch->branch_area:'-';
                        $temp['details']['latitude'] = $branch?$branch->latitude:'-';
                        $temp['details']['longitude'] = $branch?$branch->longitude:'-';
                        $temp['details']['contact_person'] = $branch?$branch->branch_contact_person:'-';
                        $temp['details']['phone_number'] = $branch?$branch->branch_contact_number:'-';
                        $temp['details']['availability'] = ['m' => $m_availability, 'f' => $f_availability];

                        // Caregiver Info
                        $caregivers = Caregiver::withoutGlobalScopes()->whereTenantId($pr->tenant_id)->whereBranchId($branch->id)->with('professional')
                                        ->whereHas('professional', function($q){
                                            $q->whereRole('Caregiver');
                                        })->whereWorkStatus('Available');
                        if(isset($filter['g']) && $filter['g'] != '0')
                            $caregivers->where('gender',$filter['g']);
                        $caregivers = $caregivers->get();

                        $temp['caregivers'] = [];
                        if(count($caregivers)){
                            foreach($caregivers as $c){
                                $temp['caregivers'][] = [
                                    'caregiver_name' => $c->first_name.' '.$c->middle_name.' '.$c->last_name,
                                    'caregiver_gender' => $c->gender,
                                    'caregiver_age' => $c->date_of_birth?$c->date_of_birth->age:'-',
                                    'caregiver_specialization' => isset($c->professional->specializations)?$c->professional->specializations->specialization_name:'-',
                                    'caregiver_qualification' => $c->professional?$c->professional->qualification:'',
                                    'caregiver_experience' => $c->professional?$c->professional->experience:'',
                                    'caregiver_languages_known' => $c->professional?$c->professional->languages_known:'',
                                ];
                            }
                        }

                        // Rate Card Info
                        $temp['rates'] = [];
                        $rates = RateCard::withoutGlobalScopes()->whereTenantId($pr->tenant_id)->get();
                        if(count($rates)){
                            foreach($rates as $r){
                                if(isset($r->service)){
                                    $temp['rates'][] = [
                                    'service_name' => $r->service->service_name,
                                    'rate' => $r->amount
                                    ];
                                }
                            }
                        }

                        $result[] = $temp;
                        \DB::purge('api');
                    }
                }
            }
        }

        return response()->json($result,200);
    }

    public function searchPatientCases(Request $request)
    {
        $result = [];

        if($request->ajax()){
            $q = $request->q;
            $patients = Patient::search($q)->get();
            if(count($patients)){
                foreach ($patients as $p) {
                    $careplans = Careplan::wherePatientId($p->id)->get();
                    if(count($careplans)){
                        foreach ($careplans as $c) {
                            $result[] = $c->toArray();
                        }
                    }
                }
            }

            return response()->json($result,200);
        }
    }

    public function getProformaForServiceOrderDates(Request $request)
    {
        $dates = [];
        if($request->has('patient_id')){
            $dates = \App\Entities\Invoice::select(\DB::raw('CONCAT(invoice_period_from,"_",invoice_period_to) as period'))
                    ->whereInvoiceType('Proforma')->whereInvoiceFor('Order')->wherePatientId($request->patient_id)->pluck('period');
        }

        return response()->json($dates, 200);
    }

    public function markLeadAsDuplicate(Request $request)
    {
        $res = false;
        if($request->id){
            $id = \Helper::encryptor('decrypt',$request->id);
            $res = LeadRequests::find($id)->update(['status' => 'Duplicate']);
        }

        return response()->json((bool) $res,200);
    }

    public function getLeadsCurveData(Request $request)
    {
        if(\Helper::encryptor('decrypt',session('utype')) == 'Aggregator')
        {
         $data['dates'] = [];
         $data['valuesAggLead'] = $data['valuesLeadRequests'] = $data['valuesProviderLeads'] = $data['valuesLeadRequestsNotChecked'] = [];
         for($i = 1; $i<=date('d'); $i++){
             // $count =   Lead::whereYear('created_at', '>=' ,date('Y'))->whereMonth('created_at', '>=' ,date('m'))->whereDay('created_at', $i)->count()+
             //            LeadRequests::whereYear('created_at', '>=' ,date('Y'))->whereMonth('created_at', '>=' ,date('m'))->whereDay('created_at', $i)->count()+
             //            AggregatorLeads::whereYear('created_at', '>=' ,date('Y'))->whereMonth('created_at', '>=' ,date('m'))->whereDay('created_at', $i)->count();
             $countAggLead = Lead::whereYear('created_at', '>=' ,date('Y'))->whereMonth('created_at', '>=' ,date('m'))->whereDay('created_at', $i)->count();
             $countLeadRequests = LeadRequests::whereYear('created_at', '>=' ,date('Y'))->whereMonth('created_at', '>=' ,date('m'))->whereDay('created_at', $i)->count();
             $countProviderLeads = AggregatorLeads::whereYear('created_at', '>=' ,date('Y'))->whereMonth('created_at', '>=' ,date('m'))->whereDay('created_at', $i)->count();
             $countLeadRequestsNotChecked = LeadRequests::whereYear('created_at', '>=' ,date('Y'))->whereMonth('created_at', '>=' ,date('m'))->whereDay('created_at', $i)->whereStatus('Not Checked')->count();

             array_push($data['dates'],$i);
             array_push($data['valuesAggLead'],$countAggLead);
             array_push($data['valuesLeadRequests'],$countLeadRequests);
             array_push($data['valuesProviderLeads'],$countProviderLeads);
             array_push($data['valuesLeadRequestsNotChecked'],$countLeadRequestsNotChecked);
         }
        }else{
         $data['dates'] = [];
         $data['values'] = [];
         for($i = 1; $i<=date('d'); $i++){
             $count = Lead::whereYear('created_at', '>=' ,date('Y'))->whereMonth('created_at', '>=' ,date('m'))->whereDay('created_at', $i)->count();
             array_push($data['dates'],$i);
             array_push($data['values'],$count);
         }
        }

        return response()->json($data, 200);
    }

    public function getResourceCurveData(Request $request)
    {
        $data['dates'] = [];
        $data['no_of_caregiversonthisday'] = [];
        $data['no_of_caregiversdeployedonthisday'] = [];
        $data['no_of_caregivers'] = [];

        for($i = 1; $i<=date('d'); $i++){
         $countcaregiversonthisday = Caregiver::whereMonth('created_at','=', date('m'))->whereDay('created_at','=', $i)->count();
         $countcaregiverdeployedonthisday = Caregiver::whereHas('schedule',function($q) use ($i)
             { $q->whereMonth('created_at','=', date('m'))->whereDay('created_at', $i); })
         ->count();
         $countcaregivers = Caregiver::withoutGlobalScopes()->
                                         whereHas('user', function($q){
                                             $q->where('status',1);
                                         })->count();
         array_push($data['dates'],$i);
         array_push($data['no_of_caregiversonthisday'],$countcaregiversonthisday);
         array_push($data['no_of_caregiversdeployedonthisday'],$countcaregiverdeployedonthisday);
         array_push($data['no_of_caregivers'],$countcaregivers);
        }

        return response()->json($data, 200);
    }

    public function saveLabTest(Request $request)
    {
        $test_details = json_encode($request->testdetails);
        $total_price = $request->total_price;

        $labTest = new CaseForm;
        $labTest->tenant_id = \Helper::getTenantID();
        $labTest->careplan_id = \Helper::encryptor('decrypt',$request->careplan_id);
        $labTest->user_id = \Helper::encryptor('decrypt',session('user_id'));
        $labTest->form_type = $request->form_type;
        $labTest->form_data = $test_details;

        $labTest->save();

        session()->flash('alert_success', 'Lab Test Added Successfully Against the Case.');
    }

    public function ratecardLabTest(Request $request)
    {
        $data=[];$list=[];
        $labId = $request->labId;
        $data['pricelist'] = RateCard::whereServiceId($labId)->get();
        foreach ($data['pricelist'] as $p) {
            $list[] = array($p->amount);
        }
        $list = json_encode($list);
        return response()->json($list, 200);
    }

    public function apply_for_verification(Request $request)
    {
        $govt_verification = Verification::whereCaregiverId($request->caregiver_Id)->where('doc_type','Government Issued Identification')->count();
        $perm_add_verification = Verification::whereCaregiverId($request->caregiver_Id)->where('doc_type','Permanent Address')->count();
        $curr_add_verification = Verification::whereCaregiverId($request->caregiver_Id)->where('doc_type','Current Address')->count();
        $edu_verification = Verification::whereCaregiverId($request->caregiver_Id)->where('doc_type','Educational Document')->count();
        $imc_verification = Verification::whereCaregiverId($request->caregiver_Id)->where('doc_type','IMC Document')->count();

        if($govt_verification > 0 && $perm_add_verification > 0 && $curr_add_verification > 0 && $edu_verification > 0 && $imc_verification > 0){
            $caregiver = Caregiver::whereId($request->caregiver_Id)->first();
            if($caregiver){
                $agency = Agency::whereId($request->agency_Id)->first();

                if($agency){
                    $mail = Mail::to($agency->agency_email);
                    $mail->send(new DocumentVerifyEmail($caregiver));
                }

                $caregiver->prof_verification = 'Pending';
                $caregiver->save();
                $state = "Applied";
            }else{
                $state = "OOPS";
            }
        }else{
            if($imc_verification == 0)
                $state = "ImcMissing";
            if($edu_verification == 0)
                $state = "EduMissing";
            if($curr_add_verification == 0)
                $state = "CurrAddMissing";
            if($perm_add_verification == 0)
                $state = "PermAddMissing";
            if($govt_verification == 0)
                $state = "GovtMissing";
        }

        return response()->json($state,200);
    }

    public function getWorklogByDate(Request $request)
    {
        $data = ['result' => false, 'worklog' => false, 'tasks' => false];
        if($request->has('worklog_date') && !empty($request->worklog_date) && !empty($request->lead_id)){
            $worklog = \App\Entities\WorkLog::whereLeadId(\Helper::encryptor('decrypt', $request->lead_id))
                            ->whereWorklogDate(date_format(new \DateTime($request->worklog_date), 'd-m-Y'))->first();
            if($worklog){
                $data['result'] = true;
                $data['worklog']['vitals'] = $worklog->vitals;
                $data['worklog']['routines'] = $worklog->routines;
            }
            $schedule = \App\Entities\Schedule::whereLeadId(\Helper::encryptor('decrypt', $request->lead_id))
                            ->whereScheduleDate($request->worklog_date)->first();
            if($schedule){
                $data['result'] = true;
                $data['tasks'] = !empty($schedule->assigned_tasks)?$schedule->assigned_tasks:false;
                $data['schedule_id'] = \Helper::encryptor('encrypt', $schedule->id);
                $data['caregiver_id'] = \Helper::encryptor('encrypt', $schedule->caregiver_id);
                $data['caregiver_name'] = $schedule->caregiver->full_name;
            }
        }

        return response()->json($data, 200);
    }

    public function readNotifications()
    {
        Notifications::whereStatus('Unread')->update(['status' => 'Read']);

        return response()->json(200);
    }

    public function clearNotifications()
    {
        Notifications::query()->delete();

        return response()->json(200);
    }

    public function getSchedulesByStatus(Request $request)
    {
        ini_set('memory_limit', '1024M'); // or you could use 1G
        ini_set('max_execution_time', '3000'); // or you could use 1G
        
        $items = [];
        if($request->status == 'Unbilled'){
            $ids = \DB::table('case_invoice_items')->select('item_id')->whereCategory('Schedule')->wherePatientId($request->pt_id)
                        ->distinct('item_id')->whereNotNull('item_id')->whereNull('deleted_at')
                        ->pluck('item_id')->toArray();
        }else{
            $ids = \DB::table('case_service_invoice_items')->select('item_id')->whereCategory('Schedule')
                        ->wherePatientId($request->pt_id)->whereType('Completed')
                        ->distinct('item_id')->whereNotNull('item_id')->whereNull('deleted_at')
                        ->pluck('item_id')->toArray();
        }
        
        $schedules = Schedule::whereIn('service_order_id',$request->sr_id)->whereChargeable(1)->whereNotIn('id', $ids);
        if(!empty($request->status)){
            if($request->status == 'Completed') $schedules->whereStatus('Completed');
            if($request->status == 'Unbilled') $schedules->whereIn('status',['Pending','Completed']);
        }
        if(!empty($request->from_date) && !empty($request->to_date)){
            $schedules->whereBetween('schedule_date',[$request->from_date, $request->to_date]);
        }

        $dates = $schedules->orderBy('schedule_date','Asc')->pluck('schedule_date')->toArray();
        if(count($dates)){
            $items = [
                'from' => $dates[0]->format('d-m-Y'),
                'to' => $dates[count($dates) - 1]->format('d-m-Y'),
                'duration' => count($dates),
                'schedule_total_amount' => $schedules->where('status','!=','Cancelled')->sum('amount')
            ];
        }

        return $items;
    }

    public function getDeploymentsByDate(Request $request)
    {
        if($request->getByDate && $request->getByDate != ''){
            $expDate = Carbon::parse($request->getByDate)->format('Y-m-d');
        }else{
            $expDate = Carbon::now()->format('Y-m-d');
        }
        $data = [];
        $deployments = Schedule::deployed()->with('caregiver')->whereStatus('Pending')->whereScheduleDate($expDate)->orderBy('patient_id','desc')->get();

        if(!empty($deployments) && count($deployments)){
            foreach ($deployments as $deployment) {
                    $data[] = [
                        'id'           => \Helper::encryptor('encrypt',$deployment->lead_id),
                        'patient'      => $deployment->patient->first_name,
                        'professional' => ($deployment->caregiver_id != 0 )?$deployment->caregiver->first_name:'NA',
                        'service'      => ($deployment->service)?$deployment->service->service_name:'NA',
                    ];
                }
            }

        return response()->json($data, 200);
    }

    public function getReplacementsByDate(Request $request)
    {
        // Get Services expiring list
        if($request->getByDate && $request->getByDate != ''){
            $expDate = Carbon::parse($request->getByDate)->format('Y-m-d');
        }else{
            $expDate = Carbon::now()->format('Y-m-d');
        }
        $replacements = [];
        $data['replacementsChart'] = [];// Contains Schedule Instance

        $schedules = Schedule::deployed()
                ->select('id','lead_id','schedule_date','caregiver_id','patient_id','service_request_id','status')
                ->whereStatus('Pending')
                ->whereScheduleDate($expDate)
                ->with('serviceRequest')
                ->with(['caregiver' => function($q){
                    $q->addSelect('id','first_name','middle_name','last_name','employee_id');
                }, 'caregiver.professional' => function($q){
                    $q->addSelect('id','caregiver_id','working_days');
                },'patient' => function($q){
                    $q->addSelect('id','first_name','last_name','patient_id');
                }])
                ->get();        

        foreach($schedules as $s){
            // Get Employee Leaves
            $leaveDates = Leaves::whereCaregiverId($s->caregiver_id)->whereIn('status',['Approved','Pending'])
            ->where('date',$s->schedule_date->format('Y-m-d'))
            ->count();
            if(isset($s->schedule_date) && isset($s->caregiver) && (!in_array(date("D", strtotime($s->schedule_date->format('d-m-Y'))), explode(',',$s->caregiver->professional->working_days), true)) || $leaveDates > 0){
                $s->reason = 'Unavailable';
                $data['replacementsChart'][] = $s;
            }
            if(isset($s->schedule_date) && ($s->caregiver == null)){
                $s->reason = 'Unallocated';
                $data['replacementsChart'][] = $s;
            }
        }

        if(!empty($data['replacementsChart']) && count($data['replacementsChart'])){
            foreach ($data['replacementsChart'] as $rc) {
                if(isset($rc->serviceRequest->service)){
                    $replacements[] = [
                        'id' => \Helper::encryptor('encrypt',$rc->lead->id),
                        'patient' => $rc->patient->first_name,
                        'service_name' => $rc->serviceRequest->service->service_name,
                        'reason' => $rc->reason
                    ];
                }
            }
        }

        return response()->json($replacements, 200);
    }

    public function getFollowUpsByDate(Request $request)
    {
        if($request->getByDate && $request->getByDate != ''){
            $expDate = Carbon::parse($request->getByDate)->format('Y-m-d');
        }else{
            $expDate = Carbon::now()->format('Y-m-d');
        }
        $data = [];
        $leadFollowUps = Lead::deployed()->whereStatus('Follow Up')
                              ->whereHas('dispositions', function($query) use ($expDate){
                                $query->select('lead_id','follow_up_datetime','status')->whereStatus('Follow Up')->whereDate('follow_up_datetime',$expDate);
                              })->get();

        if(!empty($leadFollowUps) && count($leadFollowUps)){
            foreach ($leadFollowUps as $lead) {
                $latestDisposition = date_format(new \DateTime($lead->dispositions->sortByDesc('created_at')->pluck('follow_up_datetime')->first()),'Y-m-d h:i:s A');

                $latestDate = Carbon::parse($latestDisposition)->format('Y-m-d');
                $latestTime = Carbon::parse($latestDisposition)->format('h:i:s A');

                if($latestDate === $expDate){
                    $data[] = [
                        'id'      => \Helper::encryptor('encrypt',$lead->id),
                        'patient' => $lead->patient->first_name,
                        'phone'   => $lead->patient->contact_number,
                        'time'    => isset($latestTime)?$latestTime:'NA',
                    ];
                }
            }
        }

        return response()->json($data, 200);
    }

    public function getServiceExpiryByDate(Request $request)
    {
        // Get Services expiring list
        if($request->getByDate && $request->getByDate != ''){
            $expDate = Carbon::parse($request->getByDate)->format('Y-m-d');
        }else{
            $expDate = Carbon::now()->format('Y-m-d');
        }
        $data = [];

        $caseServices = CaseServices::whereStatus('Pending')
        ->whereToDate($expDate)
        ->get();

        if(!empty($caseServices) && count($caseServices)){
            foreach ($caseServices as $cs) {
                if(isset($cs->service)){
                    $data[] = [
                        'id' => \Helper::encryptor('encrypt',$cs->lead->id),
                        'patient' => $cs->patient->first_name,
                        'service_name' => $cs->service->service_name
                    ];
                }
            }
        }

        return response()->json($data, 200);
    }
}
