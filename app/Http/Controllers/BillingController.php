<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Entities\Patient;
use App\Entities\PatientMailers;
use App\Entities\Invoice;
use App\Entities\ServiceInvoice;
use App\Entities\Receipt;
use App\Entities\Profile;
use App\Entities\Schedule;
use App\Entities\CreditMemo;
use App\Entities\CasePayment;
use App\Entities\InvoiceItems;
use App\Entities\ServiceInvoiceItems;
use App\Entities\CaseServices;
use App\Entities\CaseBillables;
use App\Entities\Masters\Taxrate;
use App\Entities\Masters\Branch;
use App\Mail\Provider\SendInvoice;
use App\Mail\Provider\SendReceipt;

use Carbon\Carbon;

class BillingController extends Controller{

    public function index()
    { 
        $inovices = Invoice::get();
        if(count($inovices)){
            foreach ($inovices as $r) {
                $invoice = Patient::whereId($r->id)->first();
                if($invoice){

                    $invoice->update(['branch_id' => 1]);
                }   
            }
        }
        
        ini_set('memory_limit', '1024M'); // or you could use 1G
        ini_set('max_execution_time', '3000'); // or you could use 1G
        $patients = Patient::select('id','first_name','last_name','patient_id')->whereNotNull('patient_id')->where('patient_id','!=','')->orderBy('first_name','asc')->get();

        $proformaInvoices = Invoice::with('patient')->orderBy('created_at','desc')->paginate(30);
        $serviceInvoices = ServiceInvoice::with('patient')->orderBy('created_at','desc')->paginate(30);
        $receipts = Receipt::with('patient')->orderBy('created_at','desc')->paginate(30);

        $stats = (object) [
            'pending_payments' => Invoice::whereMonth('created_at','=',date('m'))->whereYear('created_at','=',date('Y'))->whereNull('deleted_at')->whereNotIn('status',['Cancelled','Adjusted'])->sum('total_amount') -
                                  Invoice::whereMonth('created_at','=',date('m'))->whereYear('created_at','=',date('Y'))->whereNull('deleted_at')->whereNotIn('status',['Cancelled','Adjusted'])->sum('amount_paid'),
            'payments_received' => Invoice::whereMonth('created_at','=',date('m'))->whereYear('created_at','=',date('Y'))->whereNull('deleted_at')->whereNotIn('status',['Cancelled'])->sum('amount_paid') + InvoiceItems::whereMonth('created_at','=',date('m'))->whereYear('created_at','=',date('Y'))->whereNull('deleted_at')->whereCategory('Credit Memo')->sum('rate')
        ];

        return view('billing.index', compact('patients', 'proformaInvoices', 'serviceInvoices', 'stats', 'receipts'));
    }

    public function getProformaPaginator(Request $request){
        $proformaInvoices = Invoice::with('patient')->orderBy('created_at','desc');
        if(!empty($request->patientId)){
            $proformaInvoices = $proformaInvoices->wherePatientId($request->patientId);
        }
        if(!empty($request->invoiceNo)){
            $proformaInvoices = $proformaInvoices->where('invoice_no','like','%'.$request->invoiceNo.'%');
        }
        if(!empty($request->status)){
            $proformaInvoices = $proformaInvoices->whereStatus($request->status);
        }
        $proformaInvoices  = $proformaInvoices->paginate(30);
        return view('partials.billing.proforma-paginator', compact('proformaInvoices'));
    }

    public function getServicePaginator(Request $request){
        $serviceInvoices = ServiceInvoice::with('patient')->orderBy('created_at','desc');
        if(!empty($request->patientId)){
            $serviceInvoices = $serviceInvoices->wherePatientId($request->patientId);
        }
        if(!empty($request->invoiceNo)){
            $serviceInvoices = $serviceInvoices->where('invoice_no','like','%'.$request->invoiceNo.'%');
        }
        $serviceInvoices  = $serviceInvoices->paginate(30);
        return view('partials.billing.service-paginator', compact('serviceInvoices'));
    }

    public function getReceiptPaginator(Request $request){
        $receipts = Receipt::with('patient')->orderBy('created_at','desc');
        if(!empty($request->patientId)){
            $receipts = $receipts->wherePatientId($request->patientId);
        }
        if(!empty($request->receiptNo)){
            $receipts = $receipts->where('receipt_no','like','%'.$request->receiptNo.'%');
        }
        if(!empty($request->status)){
            $receipts = $receipts->whereReceiptType($request->status);
        }
        $receipts  = $receipts->paginate(30);
        return view('partials.billing.receipt-paginator', compact('receipts'));
    }

    public function createProforma(Request $request, $pid)
    {
        $patient = null;
        if($pid){
            $patient = Patient::find(\Helper::encryptor('decrypt',$pid));
        }

        if(!$patient)
            return back()->with('alert_error','Could not find patient record!');

        //$settings['invoicePrefix'] = \Helper::getSetting('proforma_invoice_inits');
        //$settings['invoiceStart'] = \Helper::getSetting('proforma_invoice_start_no');
        //$settings['paymentDueDate'] = \Helper::getSetting('payment_due_date');

        $settings['invoicePrefix'] = Branch::whereId($patient->branch_id)->pluck('proforma_invoice_inits')->first();
        $settings['invoiceStart'] = Branch::whereId($patient->branch_id)->pluck('proforma_invoice_start_no')->first();
        $settings['paymentDueDate'] = Branch::whereId($patient->branch_id)->pluck('payment_due_date')->first();
        $settings['invoiceCounts'] = Invoice::whereBranchId($patient->branch_id)->count() + 1;
        if(\Helper::encryptor('decrypt',session('dbName')) == 'shc_branch266' && $patient->branch_id == 2){
            if($settings['invoiceCounts'] <= 9){
                $settings['invoiceCounts'] = '0000'.$settings['invoiceCounts'];
            }elseif($settings['invoiceCounts'] <= 99){
                $settings['invoiceCounts'] = '000'.$settings['invoiceCounts'];
            }elseif($settings['invoiceCounts'] <= 999){
                $settings['invoiceCounts'] = '00'.$settings['invoiceCounts'];
            }elseif($settings['invoiceCounts'] <= 9999){
                $settings['invoiceCounts'] = '0'.$settings['invoiceCounts'];
            }else{
                $settings['invoiceCounts'] = $settings['invoiceCounts'];
            }
        }else{
            $settings['invoiceCounts'] = $settings['invoiceCounts'] + $settings['invoiceStart'];
        }

        $taxes = Taxrate::all();

        $pendingInvoices = Invoice::whereNotIn('status',['Cancelled','Adjusted','Paid'])->wherePatientId($patient->id)->whereColumn('total_amount','>','amount_paid')->get();

        return view('billing.create-proforma-invoice', compact('patient','settings','taxes','pendingInvoices'));
    }

    public function createService(Request $request, $pid)
    {
        $patient = null;
        if($pid){
            $patient = Patient::find(\Helper::encryptor('decrypt',$pid));
        }

        if(!$patient)
            return back()->with('alert_error','Could not find patient record!');

        //$settings['invoicePrefix'] = \Helper::getSetting('invoice_inits');
        //$settings['invoiceStart'] = \Helper::getSetting('invoice_start_no');
        //$settings['paymentDueDate'] = \Helper::getSetting('payment_due_date');

        $settings['invoicePrefix'] = Branch::whereId($patient->branch_id)->pluck('proforma_invoice_inits')->first();
        $settings['invoiceStart'] = Branch::whereId($patient->branch_id)->pluck('proforma_invoice_start_no')->first();
        $settings['paymentDueDate'] = Branch::whereId($patient->branch_id)->pluck('payment_due_date')->first();


        $settings['invoiceCounts'] = ServiceInvoice::count();
        $taxes = Taxrate::all();

        return view('billing.create-service-invoice', compact('patient','settings','taxes'));
    }

    public function saveProformaInvoice(Request $request)
    {
        $invoiceData = $request->caseInvoice;
        $serviceData = $request->serviceList;
        $creditMemoData = $request->creditMemoList;
        $pendingInvoiceData = $request->pendingInvoiceList;
        $billableData = $request->billableList;
        if($invoiceData['total_amount'] < 0){

            $creditMemo = CreditMemo::find($creditMemoData[count($creditMemoData) - 1]);
            $receipt = Receipt::whereItemId($creditMemo->id)->whereReceiptType('Payment Received')->first();

            $creditMemo->update(['amount'=>floatVal($creditMemo->amount + $invoiceData['total_amount'])]);
            $receipt->update((['receipt_amount'=>floatVal($creditMemo->amount)]));

            $memo['patient_id'] = $creditMemo['patient_id'];
            $memo['date'] = date_format(new \DateTime($creditMemo['date']),'Y-m-d');
            $memo['user_id'] = \Helper::getUserID();
            $memo['type'] = 'Others';
            $memo['amount'] = -($invoiceData['total_amount']);
            $memo['created_at'] = date('Y-m-d H:i:s');
            $memo['tenant_id'] = \Helper::encryptor('decrypt',session('tenant_id'));
            $memo['comment'] = '-';

            //$creditInit = \Helper::getSetting('credit_inits');
            $creditInit = Branch::whereId(Patient::whereId($invoiceData['patient_id'])->pluck('branch_id')->first())->pluck('credit_inits')->first();
            //$creditStart = \Helper::getSetting('credit_start_no');
            $creditStart = Branch::whereId(Patient::whereId($invoiceData['patient_id'])->pluck('branch_id')->first())->pluck('credit_start_no')->first();
            $lastCreditId = CreditMemo::orderBy(\DB::raw('length(credit_memo_no),credit_memo_no'))->get()->last();
            if($lastCreditId)
                $lastID = substr($lastCreditId->credit_memo_no, strlen($creditInit));
            else
                $lastID = 0;
            
            $memo['credit_memo_no'] = $creditInit.str_pad(intval($lastID + 1),strlen($lastID),0,STR_PAD_LEFT);
            $memoId = CreditMemo::insertGetId($memo);
           // $receiptPrefix = \Helper::getSetting('receipt_inits');
            $receiptPrefix = Branch::whereId(Patient::whereId($invoiceData['patient_id'])->pluck('branch_id')->first())->pluck('receipt_inits')->first();
            //$receiptStart = \Helper::getSetting('receipt_start_no');
            $receiptStart = Branch::whereId(Patient::whereId($invoiceData['patient_id'])->pluck('branch_id')->first())->pluck('receipt_start_no')->first();
            $receiptCount = Receipt::count();

            if($receiptCount > 0){
                $receiptCount += 1;
            }

            $receipt = new Receipt;
            $receipt->patient_id     = $memo['patient_id'];
            $receipt->receipt_type   = 'Payment Received';
            $receipt->receipt_no     = $receiptPrefix . ($receiptStart + $receiptCount);
            $receipt->receipt_amount = $memo['amount'];
            $receipt->item_id        = $memoId;
            $receipt->receipt_date   = date('Y-m-d');
            $receipt->save();
        }   
        $result = false;

        $invoiceData['created_at'] = date('Y-m-d H:i:s');
        $invoiceData['updated_at'] = date('Y-m-d H:i:s');
        $invoiceData['branch_id'] = Patient::whereId($invoiceData['patient_id'])->pluck('branch_id')->first();
        if($invoiceData['total_amount'] == 0.00 || $invoiceData['total_amount'] <= 0){
            $invoiceData['status'] = 'Paid';
            $invoiceData['total_amount'] = 0.00;
        }
        // Insert CaseInvoice and Get ID
        $invoiceId = Invoice::insertGetId($invoiceData);
        $result = true;

        // Loop Through the Services
        if(isset($serviceData)){
            foreach ($serviceData as $service) {
                $type = "Unbilled";
                $dateRange = [];

                // Get the Ranges of Date
                $dates = explode(' to ',$service['services_period']);
                // For Unbilled and Completed Schedules
                if(count($dates) == 2){
                    foreach ($dates as $dr) {
                        $dateRange[] = Carbon::parse($dr)->format('Y-m-d');
                    }

                    $ids = \DB::table('case_invoice_items')->wherePatientId($invoiceData['patient_id'])->select('item_id')->whereCategory('Schedule')
                                ->distinct('item_id')->whereNotNull('item_id')->whereNull('deleted_at')
                                ->pluck('item_id')->toArray();
                    // Get the ids of schedules falling under the date range
                    $schedulesIds = Schedule::whereIn('service_order_id',explode(',',$service['service_id']))
                                        ->whereBetween('schedule_date',$dateRange)->whereChargeable(1)
                                        ->where('status', '!=', 'Cancelled')->whereNotIn('id',$ids)->pluck('id')->toArray();

                    // Loop through the schedule ids and make entries into the case invoice items
                    foreach ($schedulesIds as $sid) {
                        $actualRate = $service['service_rate'];
                        $scheduleAmount = Schedule::whereId($sid)->value('amount');
                        $discountAmount = $actualRate - $scheduleAmount;
                        $taxSlab = $service['service_tax'];

                        $taxAmount = ($scheduleAmount)*($taxSlab/100);
                        $totalAmount = $scheduleAmount + $taxAmount;
                        $invoiceItem = [
                            'tenant_id'       => \Helper::encryptor('decrypt',session('tenant_id')),
                            'patient_id'      => $invoiceData['patient_id'],
                            'invoice_id'      => $invoiceId,
                            'category'        => 'Schedule',
                            'type'            => $type,
                            'item_id'         => $sid,
                            'tax_slab'        => $taxSlab,
                            'rate'            => $actualRate,
                            'discount_amount' => $discountAmount,
                            'tax_amount'      => $taxAmount,
                            'total_amount'    => $totalAmount,
                            'user_id'         => $invoiceData['user_id'],
                            'created_at'      => date('Y-m-d H:i:s'),
                            'updated_at'      => date('Y-m-d H:i:s')
                        ];
                        InvoiceItems::insert($invoiceItem);
                    }
                }
            }
        }

        // Loop Through the CreditMemo List
        if(isset($creditMemoData)){
            foreach ($creditMemoData as $creditMemoID) {
                $creditMemo = CreditMemo::whereId($creditMemoID)->first();
                $creditMemoItem = [
                    'tenant_id'       => \Helper::getTenantID(),
                    'patient_id'      => $invoiceData['patient_id'],
                    'invoice_id'      => $invoiceId,
                    'category'        => 'Credit Memo',
                    'type'            => 'Completed',
                    'item_id'         => $creditMemoID,
                    'tax_slab'        => null,
                    'rate'            => $creditMemo->amount,
                    'tax_amount'      => null,
                    'total_amount'    => $creditMemo->amount,
                    'user_id'         => \Helper::getUserID(),
                    'created_at'      => date('Y-m-d H:i:s'),
                    'updated_at'      => date('Y-m-d H:i:s')
                ];
                InvoiceItems::insert($creditMemoItem);
                CreditMemo::whereId($creditMemoID)->update(['status'=>'Billed']);
            }
        }

        // Loop Through the Pending Invoices List
        if(isset($pendingInvoiceData)){
            foreach ($pendingInvoiceData as $pendingInvoiceID) {
                $invoice = Invoice::whereId($pendingInvoiceID)->first();
                $invoice->status = 'Adjusted';
                $invoice->save();

                $pendingInvoiceItem = [
                    'tenant_id'       => \Helper::getTenantID(),
                    'patient_id'      => $invoiceData['patient_id'],
                    'invoice_id'      => $invoiceId,
                    'category'        => 'Invoice',
                    'type'            => 'Adjusted',
                    'item_id'         => $pendingInvoiceID,
                    'tax_slab'        => null,
                    'rate'            => $invoice->total_amount - $invoice->amount_paid,
                    'tax_amount'      => null,
                    'total_amount'    => $invoice->total_amount - $invoice->amount_paid,
                    'user_id'         => \Helper::getUserID(),
                    'created_at'      => date('Y-m-d H:i:s'),
                    'updated_at'      => date('Y-m-d H:i:s')
                ];
                InvoiceItems::insert($pendingInvoiceItem);
            }
        }

        // Loop Through the Billables List
        if(isset($billableData)){
            foreach ($billableData as $billable) {
                $actualRate = $billable['billable_rate'];
                $billableQuantity = $billable['billable_quantity'];
                $billableAmount = $billable['billable_amount'];
                $taxSlab = !empty($billable['billable_tax'])?$billable['billable_tax']:0;

                $taxAmount = ($billableAmount)*($taxSlab/100);
                $totalAmount = $billableAmount + $taxAmount;

                $billableItem = [
                    'tenant_id'       => \Helper::getTenantID(),
                    'patient_id'      => $invoiceData['patient_id'],
                    'invoice_id'      => $invoiceId,
                    'category'        => 'Billable',
                    'type'            => 'Completed',
                    'item_id'         => $billable['billable_id'],
                    'tax_slab'        => $taxSlab,
                    'rate'            => $actualRate,
                    'duration'        => $billableQuantity,
                    'tax_amount'      => $taxAmount,
                    'total_amount'    => $totalAmount,
                    'user_id'         => \Helper::getUserID(),
                    'created_at'      => date('Y-m-d H:i:s'),
                    'updated_at'      => date('Y-m-d H:i:s')
                ];
                InvoiceItems::insert($billableItem);
                CaseBillables::whereId($billable['billable_id'])->update(['proforma_status'=>'Billed']);
            }
        }

        return response()->json($result, 200);
    }

    public function saveServiceInvoice(Request $request)
    {
        $invoiceData = $request->caseInvoice;
        $serviceData = $request->serviceList;
        $creditMemoData = $request->creditMemoList;
        $billableData = $request->billableList;
        $result = false;

        $invoiceData['created_at'] = date('Y-m-d H:i:s');
        $invoiceData['updated_at'] = date('Y-m-d H:i:s');
        // Insert CaseInvoice and Get ID
        $invoiceId = ServiceInvoice::insertGetId($invoiceData);
        $result = true;

        // Loop Through the Services
        if(isset($serviceData)){
            foreach ($serviceData as $service) {
                $type = "Completed";
                $dateRange = [];

            // Get the Ranges of Date
                $dates = explode(' to ',$service['services_period']);
                // For Unbilled and Completed Schedules
                if(count($dates) == 2){
                    foreach ($dates as $dr) {
                        $dateRange[] = Carbon::parse($dr)->format('Y-m-d');
                    }
                    $ids = \DB::table('case_service_invoice_items')->wherePatientId($invoiceData['patient_id'])->select('item_id')
                                ->whereCategory('Schedule')
                                ->whereType('Completed')
                                ->distinct('item_id')->whereNotNull('item_id')->whereNull('deleted_at')
                                ->pluck('item_id')->toArray();
                    // Get the ids of schedules falling under the date range
                    $schedulesIds = Schedule::whereIn('service_order_id',explode(',',$service['service_id']))
                                        ->whereBetween('schedule_date',$dateRange)->whereChargeable(1)
                                        ->where('status', '=', 'Completed')->whereNotIn('id',$ids)->pluck('id')->toArray();

                    // Loop through the schedule ids and make entries into the case invoice items
                    foreach ($schedulesIds as $sid) {
                        $actualRate = $service['service_rate'];
                        $scheduleAmount = Schedule::whereId($sid)->value('amount');
                        $discountAmount = $actualRate - $scheduleAmount;
                        $taxSlab = $service['service_tax'];

                        $taxAmount = ($scheduleAmount)*($taxSlab/100);
                        $totalAmount = $scheduleAmount + $taxAmount;
                        $invoiceItem = [
                            'tenant_id'       => \Helper::encryptor('decrypt',session('tenant_id')),
                            'patient_id'      => $invoiceData['patient_id'],
                            'invoice_id'      => $invoiceId,
                            'category'        => 'Schedule',
                            'type'            => $type,
                            'item_id'         => $sid,
                            'tax_slab'        => $taxSlab,
                            'rate'            => $actualRate,
                            'discount_amount' => $discountAmount,
                            'tax_amount'      => $taxAmount,
                            'total_amount'    => $totalAmount,
                            'user_id'         => $invoiceData['user_id'],
                            'created_at'      => date('Y-m-d H:i:s'),
                            'updated_at'      => date('Y-m-d H:i:s')
                        ];
                        ServiceInvoiceItems::insert($invoiceItem);
                    }
                }
            }
        }

        // Loop Through the CreditMemo List
        if(isset($creditMemoData)){
            foreach ($creditMemoData as $creditMemoID) {
                $creditMemo = CreditMemo::whereId($creditMemoID)->first();
                $creditMemoItem = [
                    'tenant_id'       => \Helper::getTenantID(),
                    'patient_id'      => $invoiceData['patient_id'],
                    'invoice_id'      => $invoiceId,
                    'category'        => 'Credit Memo',
                    'type'            => 'Completed',
                    'item_id'         => $creditMemoID,
                    'tax_slab'        => null,
                    'rate'            => $creditMemo->amount,
                    'tax_amount'      => null,
                    'total_amount'    => $creditMemo->amount,
                    'user_id'         => \Helper::getUserID(),
                    'created_at'      => date('Y-m-d H:i:s'),
                    'updated_at'      => date('Y-m-d H:i:s')
                ];
                ServiceInvoiceItems::insert($creditMemoItem);
                CreditMemo::whereId($creditMemoID)->update(['service_status'=>'Billed']);
            }
        }

        // Loop Through the Billables List
        if(isset($billableData)){
            foreach ($billableData as $billable) {
                $actualRate = $billable['billable_rate'];
                $billableQuantity = $billable['billable_quantity'];
                $billableAmount = $billableQuantity * $actualRate;
                $taxSlab = !empty($billable['billable_tax'])?$billable['billable_tax']:0;

                $taxAmount = ($billableAmount)*($taxSlab/100);
                $totalAmount = $billableAmount + $taxAmount;

                $billableItem = [
                    'tenant_id'       => \Helper::getTenantID(),
                    'patient_id'      => $invoiceData['patient_id'],
                    'invoice_id'      => $invoiceId,
                    'category'        => 'Billable',
                    'type'            => 'Completed',
                    'item_id'         => $billable['billable_id'],
                    'tax_slab'        => $taxSlab,
                    'rate'            => $actualRate,
                    'duration'        => $billableQuantity,
                    'tax_amount'      => $taxAmount,
                    'total_amount'    => $totalAmount,
                    'user_id'         => \Helper::getUserID(),
                    'created_at'      => date('Y-m-d H:i:s'),
                    'updated_at'      => date('Y-m-d H:i:s')
                ];
                ServiceInvoiceItems::insert($billableItem);
                CaseBillables::whereId($billable['billable_id'])->update(['service_status'=>'Billed']);
            }
        }

        return response()->json($result, 200);
    }

    public function viewInvoice(Request $request, $invId, $type)
    {
        if($invId){
            if($type == 'Proforma'){
                $invoice = Invoice::find(\Helper::encryptor('decrypt',$invId));
                //$data['inv_prefix'] = \Helper::getSetting('proforma_invoice_inits');
                $data['inv_prefix'] = Branch::whereId(Patient::whereId($invoice->patient_id)->pluck('branch_id')->first())->pluck('proforma_invoice_inits')->first();
            }
            else{
                $invoice = ServiceInvoice::find(\Helper::encryptor('decrypt',$invId));
                //$data['inv_prefix'] = \Helper::getSetting('invoice_inits');
                $data['inv_prefix'] = Branch::whereId(Patient::whereId($invoice->patient_id)->pluck('branch_id')->first())->pluck('invoice_inits')->first();
            }
        }

        if(!$invoice)
            return back()->with('alert_error','Could not find invoice!');
     
        $mailers = PatientMailers::wherePatientId($invoice->patient_id)->whereStatus('active')->get();

        return view('billing.view-invoice', compact('invoice','data','type','mailers'));
    }

    public function printInvoice(Request $request, $invId, $type)
    {
        if($invId){
            if($type == 'Proforma'){
                $invoice = Invoice::find(\Helper::encryptor('decrypt',$invId));
                //$data['inv_prefix'] = \Helper::getSetting('proforma_invoice_inits');
                $data['inv_prefix'] = Branch::whereId(Patient::whereId($invoice->patient_id)->pluck('branch_id')->first())->pluck('proforma_invoice_inits')->first();
            }
            else{
                $invoice = ServiceInvoice::find(\Helper::encryptor('decrypt',$invId));
                //$data['inv_prefix'] = \Helper::getSetting('invoice_inits');
                $data['inv_prefix'] = Branch::whereId(Patient::whereId($invoice->patient_id)->pluck('branch_id')->first())->pluck('invoice_inits')->first();
            }
        }

        $profile = Profile::first();
        $branch = Branch::find(Patient::whereId($invoice->patient_id)->pluck('branch_id')->first());

        if(!$invoice)
            return back()->with('alert_error','Could not find invoice!');

        return view('billing.print-preview-invoice', compact('invoice','profile','type','data','branch'));
    }

    public function emailInvoice(Request $request, $invId, $type)
    {
        if($invId){
            if($type == 'Proforma'){
                $data['invoice'] = Invoice::find(\Helper::encryptor('decrypt',$invId));
                //$data['inv_prefix'] = \Helper::getSetting('proforma_invoice_inits');
                $data['inv_prefix'] = Branch::whereId(Patient::whereId($data['invoice']->patient_id)->pluck('branch_id')->first())->pluck('proforma_invoice_inits')->first();
                $data['invoiceRange'] = \Helper::getInvoiceRange($data['invoice']['id'],'Proforma','Unbilled');
            }else{
                $data['invoice'] = ServiceInvoice::find(\Helper::encryptor('decrypt',$invId));
                //$data['inv_prefix'] = \Helper::getSetting('invoice_inits');
                $data['inv_prefix'] = Branch::whereId(Patient::whereId($data['invoice']->patient_id)->pluck('branch_id')->first())->pluck('invoice_inits')->first();
                $data['invoiceRange'] = \Helper::getInvoiceRange($data['invoice']['id'],'Service','Completed');
            }
        }

        $data['profile'] = Profile::first();
        $data['branch'] = Branch::find(Patient::whereId($data['invoice']->patient_id)->pluck('branch_id')->first());
        $data['type'] = $type;
        if(!$data['invoice'])
            return back()->with('alert_error','Could not find invoice!');

        $data['pluginEnabled'] = false;

        // Check for payment gateway settings
        $pg_plugin = \App\Entities\Masters\PluginSettings::whereTenantId(\Helper::getTenantID())
                        ->whereCategory('Payment Gateway')
                        ->whereIsDefault(1)->first();
        if($pg_plugin){
            $settings = json_decode($pg_plugin->settings);
            if(!empty($settings->merchant_id) && !empty($settings->access_code) && !empty($settings->working_key))
                $data['pluginEnabled'] = true;
        }

        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 3600);

        $pdf = \PDF::loadView('billing.print-invoice', $data);
        
        if(filter_var($data['invoice']->patient->email, FILTER_VALIDATE_EMAIL)){
            \Mail::send(new SendInvoice($data, $pdf->output()));
            Invoice::where('invoice_no',$data['invoice']['invoice_no'])->update(['email_invoice'=>date('Y-m-d')]);
            return back()->with('alert_success','Invoice Sent Successfully !');
        }else{
            return back()->with('alert_error','Aborted Sending Email, Improper Email Id !');
        }

    }

    public function deleteInvoice(Request $request, $invId, $type)
    {
        $invoiceID = \Helper::encryptor('decrypt',$invId);
        if ($type == 'Proforma') {
            $creditItems = InvoiceItems::whereInvoiceId($invoiceID)->whereCategory('Credit Memo')->pluck('item_id');
            $billableItems = InvoiceItems::whereInvoiceId($invoiceID)->whereCategory('Billable')->pluck('item_id');
            $scheduleItems = InvoiceItems::whereInvoiceId($invoiceID)->whereCategory('Schedule')->pluck('item_id');
            $adjustedItems = InvoiceItems::whereInvoiceId($invoiceID)->whereCategory('Invoice')->pluck('item_id');

            CreditMemo::whereIn('id',$creditItems)->update(['status' => 'Unbilled']);

            foreach ($adjustedItems as $adInv) {
                $inv = Invoice::whereId($adInv)->first();
                if($inv->amount_paid == 0.00){
                    $inv->update(['status' => 'Pending']);
                }else{
                    $inv->update(['status' => 'Partial']);
                }
                
            }

            foreach ($scheduleItems as $sItem) {
                $cm = CreditMemo::whereScheduleId($sItem)->whereStatus('Unbilled')->whereServiceStatus('Unbilled')->first();
                if(isset($cm)){
                    $cm->delete();
                }
            }
            CaseBillables::whereIn('id',$billableItems)->update(['proforma_status' => 'Unbilled']);

            InvoiceItems::whereInvoiceId($invoiceID)->delete();
            Invoice::whereId($invoiceID)->update(['status' => 'Cancelled']);

            $cp = CasePayment::whereInvoiceId($invoiceID)->first();
            if($cp){
                $patientId = $cp->patient_id;
                $amount = $cp->total_amount;

                CasePayment::whereInvoiceId($invoiceID)->delete();
            }
        }else{
            $creditItems = ServiceInvoiceItems::whereInvoiceId($invoiceID)->whereCategory('Credit Memo')->pluck('item_id');
            $billableItems = ServiceInvoiceItems::whereInvoiceId($invoiceID)->whereCategory('Billable')->pluck('item_id');

            CreditMemo::whereIn('id',$creditItems)->update(['service_status' => 'Unbilled']);
            CaseBillables::whereIn('id',$billableItems)->update(['service_status' => 'Unbilled']);

            ServiceInvoiceItems::whereInvoiceId($invoiceID)->delete();
            ServiceInvoice::whereId($invoiceID)->update(['status'=> 'Cancelled']);
        }

        return back()->with('alert_success','Invoice Successfully Deleted !');
    }
    
    public function addCreditMemo(Request $request)
    {
        if($data['credit_memo_patient_id'] != 0){
            $memo['patient_id'] = $data['credit_memo_patient_id'];
            unset($memo['credit_memo_patient_id']);
            $memo['date'] = date_format(new \DateTime($data['date']),'Y-m-d');
            $memo['user_id'] = \Helper::getUserID();
            $memo['type'] = $data['credit_memo_type'];
            $memo['amount'] = $data['amount'];
            $memo['created_at'] = date('Y-m-d H:i:s');
            $memo['tenant_id'] = \Helper::encryptor('decrypt',session('tenant_id'));
            $memo['comment'] = $data['comment'];
            $creditInit = Branch::whereId(Patient::whereId($memo['patient_id'])->pluck('branch_id')->first())->pluck('credit_inits')->first();
            //$creditInit = \Helper::getSetting('credit_inits');
            //$creditStart = \Helper::getSetting('credit_start_no');
            $creditStart = Branch::whereId(Patient::whereId($memo['patient_id'])->pluck('branch_id')->first())->pluck('credit_start_no')->first();
            $lastCreditId = CreditMemo::orderBy(\DB::raw('length(credit_memo_no),credit_memo_no'))->get()->last();
            if($lastCreditId)
                $lastID = substr($lastCreditId->credit_memo_no, strlen($creditInit));
            else
                $lastID = 0;
            
            $memo['credit_memo_no'] = $creditInit.str_pad(intval($lastID + 1),strlen($lastID),0,STR_PAD_LEFT);
            $memoId = CreditMemo::insertGetId($memo);
   
            //$receiptPrefix = \Helper::getSetting('receipt_inits');
            $receiptPrefix = Branch::whereId(Patient::whereId($memo['patient_id'])->pluck('branch_id')->first())->pluck('receipt_inits')->first();
            //$receiptStart = \Helper::getSetting('receipt_start_no');
            $receiptStart = Branch::whereId(Patient::whereId($memo['patient_id'])->pluck('branch_id')->first())->pluck('receipt_start_no')->first();
            $receiptCount = Receipt::count();

            if($receiptCount > 0){
                $receiptCount += 1;
            }

            $receipt = new Receipt;

            $receipt->patient_id     = $memo['patient_id'];
            $receipt->receipt_type   = 'Payment Received';
            $receipt->receipt_no     = $receiptPrefix . ($receiptStart + $receiptCount);
            $receipt->receipt_amount = $memo['amount'];
            $receipt->item_id        = $memoId;
            $receipt->receipt_date   = date('Y-m-d');

            $receipt->save();

            return back()->with('alert_success','Credit memo created!');
        }        
        return back()->with('alert_warning','Please select a patient to create credit memo!');
    }

    public function updateInvoicePayment(Request $request)
    {
        $data = $request->only('invoice_id','patient_id','invoice_amount','payment_mode','cheque_dd_no','cheque_dd_date','reference_no','bank_name','status');
        $amountPaid = $request->amount_paid;
        if($data){
            $data['branch_id'] = \Helper::getBranchID();
            $data['tenant_id'] = \Helper::getTenantID();
            $data['invoice_id'] = \Helper::encryptor('decrypt',$data['invoice_id']);
            $data['patient_id'] = \Helper::encryptor('decrypt',$data['patient_id']);
            $data['total_amount'] = $amountPaid;
            $data['status'] = 'Success';

            $res = CasePayment::create($data);

            if($res){
                $invoice = Invoice::find($data['invoice_id']);
                if($invoice){
                    $paidAmount = floatVal($invoice->amount_paid + $amountPaid);
                    if($invoice->total_amount == $paidAmount){
                        $invoice->update(['amount_paid' => $paidAmount,'status' => 'Paid']);

                        // Update Invoice Items payment status
                        $items = InvoiceItems::whereInvoiceId($data['invoice_id'])
                        ->whereNotIn('status',['Paid','Cancelled'])->get();
                        if(count($items)){
                            foreach ($items as $item) {
                                $item->update(['status' => 'Paid']);
                            }
                        }
                    }
                    else{
                        $invoice->update(['amount_paid' => $paidAmount, 'status' => 'Partial']);
                    }

                    //$receiptPrefix = \Helper::getSetting('receipt_inits');
                    //$receiptStart = \Helper::getSetting('receipt_start_no');
                    $receiptPrefix = Branch::whereId(Patient::whereId($data['patient_id'])->pluck('branch_id')->first())->pluck('receipt_inits')->first();
                    $receiptStart = Branch::whereId(Patient::whereId($data['patient_id'])->pluck('branch_id')->first())->pluck('receipt_start_no')->first();
                    $receiptCount = Receipt::count();

                    if($receiptCount > 0){
                        $receiptCount += 1;
                    }

                    $receipt = new Receipt;

                    $receipt->patient_id     = $data['patient_id'];
                    $receipt->receipt_type   = 'Payment';
                    $receipt->payment_mode   = $data['payment_mode'];
                    $receipt->reference_no   = $data['cheque_dd_no']?$data['cheque_dd_no']:$data['reference_no'];
                    $receipt->receipt_no     = $receiptPrefix . ($receiptStart + $receiptCount);
                    $receipt->receipt_amount = $amountPaid;
                    $receipt->item_id        = $data['invoice_id'];
                    $receipt->receipt_date   = date('Y-m-d');

                    $receipt->save();
                }
            }

            return back()->with('alert_success','Payment updated successfully');
        }

        return back()->with('alert_error','Failed to udpdate payment!');
    }

    public function settleOffInvoice(Request $request)
    {
        $patientId = \Helper::encryptor('decrypt',$request->pid);
        $amount = $request->amount;
        $invoiceId = \Helper::encryptor('decrypt',$request->invoice);

        $cm = new CreditMemo;
        $cm->patient_id = $patientId;
        $cm->type = 'Settled';
        $cm->date = date('Y-m-d');
        $cm->amount = $amount;
        $cm->comment = 'Settled off Invoice';
        $cm->user_id = \Helper::getUserID();

        $cm->save();

        Invoice::whereId($invoiceId)->update(['status'=>'Settled']);

        return response()->json(200);
    }

    public function invoicePaymentHistory(Request $request)
    {
        $invID = \Helper::encryptor('decrypt',$request->invoice_id);
        $result = [];

        if($invID > 0){
            $payments = CasePayment::whereInvoiceId($invID)->get();
            if(count($payments)){
                foreach ($payments as $payment) {
                    $result[] = [
                        'date' => $payment->created_at->format('d-m-Y H:i:s'),
                        'amount' => $payment->total_amount,
                        'payment_mode' => $payment->payment_mode,
                        'transaction_id' => $payment->transaction_id,
                        'reference_no' => $payment->reference_no,
                        'bank_name' => $payment->bank_name,
                        'cheque_dd_date' => $payment->cheque_dd_date,
                        'cheque_dd_no' => $payment->cheque_dd_no,
                        'status' => $payment->status,
                    ];
                }
            }
        }

        return response()->json($result, 200);
    }

    public function getExpiringServicesAjax(Request $request)
    {
        // Get Services expiring list
        $expDate = Carbon::now()->addDays(4)->format('Y-m-d');
        $data = [];
        $caseServices = CaseServices::whereStatus('Pending')
                            ->whereDate('to_date', '<=',$expDate)
                            ->whereDate('to_date','>=',Carbon::now()->format('Y-m-d'))
                            ->whereHas('schedules' , function ($q){
                                $q->whereStatus('Pending');
                            })
                            ->get();
        if(!empty($caseServices) && count($caseServices)){
            foreach ($caseServices as $cs) {
                if(isset($cs->service)){
                    $data[] = [
                        'episode_id' => $cs->lead->episode_id,
                        'service_name' => $cs->service->service_name,
                        'patient' => $cs->patient->full_name,
                        'enquirer' => isset($cs->patient->enquirer_name)?$cs->patient->enquirer_name:'-',
                        'phone' => $cs->patient->contact_number,
                        'date' => $cs->to_date->format('d-m-Y'),
                        'id' => \Helper::encryptor('encrypt',$cs->lead->id),
                    ];
                }
            }
        }

        return response()->json($data, 200);
    }

    public function getCreditMemoLog(Request $request)
    {
        // Get Credit Memo Logs
        $data = [];
        $creditMemoLog = CreditMemo::wherePatientId($request->patient_id)->where('type', '!=', 'Schedule')->get();

        if(!empty($creditMemoLog) && count($creditMemoLog)){
            foreach ($creditMemoLog as $cml) {
                $data[] = [
                    'number' => $cml->credit_memo_no,
                    'date' => $cml->date->format('d-m-Y'),
                    'type' => $cml->type,
                    'amount' => $cml->amount,
                    'comment' => $cml->comment
                ];
            }
        }

        return response()->json($data, 200);
    }

    public function getCreditMemoRefundLog(Request $request)
    {
        // Get Credit Memo Logs
        $data = [];
        $creditMemoLog = CreditMemo::wherePatientId($request->patient_id)->where('type', '!=', 'Schedule')->whereStatus('Unbilled')->get();

        if(!empty($creditMemoLog) && count($creditMemoLog)){
            foreach ($creditMemoLog as $cml) {
                $data[] = [
                    'id' => $cml->id,
                    'number' => $cml->credit_memo_no,
                    'date' => $cml->date->format('d-m-Y'),
                    'type' => $cml->type,
                    'amount' => $cml->amount,
                    'comment' => $cml->comment
                ];
            }
        }

        return response()->json($data, 200);
    }

    public function raiseCreditMemoReceipt(Request $request)
    {
        $patientID = $request->patient_id;
        $items = $request->item;

        //$receiptPrefix = \Helper::getSetting('receipt_inits');
        //$receiptStart = \Helper::getSetting('receipt_start_no');
        $receiptPrefix = Branch::whereId(Patient::whereId($patientID)->pluck('branch_id')->first())->pluck('receipt_inits')->first();
        $receiptStart = Branch::whereId(Patient::whereId($patientID)->pluck('branch_id')->first())->pluck('receipt_start_no')->first();

        if($patientID){
            foreach ($items as $item) {
                $receiptCount = Receipt::count();

                if($receiptCount){
                    $receiptCount += 1;
                }

                $receiptAmount = CreditMemo::whereId($item)->value('amount');
                $creditMemo    = CreditMemo::whereId($item)->update(['status'=>'Billed']);

                if($creditMemo){
                    $receipt = new Receipt;

                    $receipt->patient_id     = $patientID;
                    $receipt->receipt_type   = 'Memo';
                    $receipt->receipt_no     = $receiptPrefix . ($receiptStart + $receiptCount);
                    $receipt->receipt_amount = $receiptAmount;
                    $receipt->item_id        = $item;
                    $receipt->receipt_date   = date('Y-m-d');

                    $receipt->save();
                }
            }
        }

        return response()->json(true, 200);
    }

    public function getUnbilledServicesAjax(Request $request)
    {   
        // ini_set('memory_limit', '150M'); // or you could use 1G
        ini_set('max_execution_time', '3000'); // or you could use 1G
        // Get Unbilled service orders list 
        $data = [];
        $patients = Patient::select('id')->whereNotNull('patient_id')->get();
        foreach ($patients as $patient) {
            $ids = \DB::table('case_invoice_items')->select('item_id')->wherePatientId($patient->id)->whereCategory('Schedule')
                        ->distinct('item_id')->whereNotNull('item_id')->whereNull('deleted_at')
                        ->pluck('item_id')->toArray();
            $unbilledSchedules = Schedule::wherePatientId($patient->id)->whereIn('status',['Pending','Completed'])->whereChargeable(1)->whereNotIn('id', $ids);
            $unbilledServiceRequests = $unbilledSchedules->distinct()->pluck('service_request_id');

            if(count($unbilledServiceRequests)){
                foreach ($unbilledServiceRequests as $uso) {
                    $uso = CaseServices::find($uso);
                    if(isset($uso->service)){
                        if($uso->custom_dates != null){
                            $dates = explode(",",trim($uso->custom_dates));
                            $period = \Carbon\Carbon::parse(trim($dates[0]))->format('d-m-Y').' to '.\Carbon\Carbon::parse(trim(end($dates)))->format('d-m-Y');
                        }
                        else
                            $period = $uso->from_date->format('d-m-Y').' to '.$uso->to_date->format('d-m-Y');

                        $data[] = [
                          'patient_id'         => isset($uso->patient)?\Helper::encryptor('encrypt',$uso->patient_id):0,
                          'episode_id'         => isset($uso->lead)?$uso->lead->episode_id:'NA',
                          'service_name'       => isset($uso->service_id)?$uso->service->service_name:'NA',
                          'patient'            => isset($uso->lead->patient)?$uso->lead->patient->full_name:'-',
                          'enquirer'           => isset($uso->lead->patient->enquirer_name)?$uso->lead->patient->enquirer_name:'NA',
                          'phone'              => isset($uso->lead->patient->contact_number)?$uso->lead->patient->contact_number:'NA',
                          'period'             => $period,
                          'status'             => isset($uso->lead)?$uso->lead->status:'-',
                          'lead_id'            => isset($uso->lead)?$uso->lead->id:0,
                          'id'                 => isset($uso->lead)?\Helper::encryptor('encrypt',$uso->lead->id):0,
                        ]; 
                    }
                }
            }
        }

        return response()->json($data, 200);
    }

    public function statementOfAccount(Request $request)
    {
        if($request->isMethod('post') || $request->isMethod('get')){
            $data['to_period'] = '';
            $data['soa_annexure'] = $request->has('soa_option_annexure')?true:false;
            $data['soa_payments'] = $request->has('soa_option_payments')?true:false;

            $patientID = $request->soa_patient_id;
            $data['pluginEnabled'] = false;

            // Check for payment gateway settings
            $pg_plugin = \App\Entities\Masters\PluginSettings::whereTenantId(\Helper::getTenantID())
                            ->whereCategory('Payment Gateway')
                            ->whereIsDefault(1)->first();
            if($pg_plugin)
                $data['pluginEnabled'] = true;

            $schedules = Schedule::select('id','lead_id','schedule_date','service_required','service_request_id','chargeable','amount','chargeable','ratecard_id')
                                    ->with(['serviceRequest' => function($q){
                                        $q->addSelect('id','from_date','to_date','net_rate','gross_rate','discount_type','discount_value');
                                    },'service' => function($q){
                                        $q->addSelect('id','service_name');
                                    },'ratecard'])
                                    ->wherePatientId($patientID)->whereChargeable(1)->whereIn('status',['Completed']);

            $payments = CasePayment::select('total_amount')->wherePatientId($patientID)->whereStatus('Success');

            if(!empty($request->soa_period_to)){
                $eDate = date_format(new \DateTime($request->soa_period_to),'Y-m-d');
                $data['to_period'] = $eDate;
                $schedules->whereDate('schedule_date','<=',$eDate);
                $payments->whereDate('created_at','<=',$eDate);
            }
            
            $temp = clone $schedules;
            $data['summary'] = \Helper::getSchedulesByMonth($temp->get());

            $data['schedules'] = $schedules->orderBy('schedule_date','Asc')->get();
            $data['payments'] = $payments->orderBy('created_at','Asc')->get();

            $data['patient'] = Patient::select('id','patient_id','first_name','last_name','enquirer_name','contact_number','alternate_number','email','city','area','street_address')
                                        ->whereId($patientID)->with(['creditMemo' => function($q) {
                                            $q->addSelect('date','amount');
                                        }])->first();
            if(!empty($request->soa_period_to)){
                $data['credit_memos'] = CreditMemo::wherePatientId($patientID)->where('date','<=',$eDate)->get();
                $data['receipts'] = Receipt::wherePatientId($patientID)->where('created_at','<=',$eDate)->whereReceiptType('Memo')->get();
            }else{                
                $data['credit_memos'] = CreditMemo::wherePatientId($patientID)->get();
                $data['receipts'] = Receipt::wherePatientId($patientID)->whereReceiptType('Memo')->get();
            }

            $data['profile'] = Profile::select('tenant_id','organization_name','organization_logo','organization_address','organization_area','organization_city','phone_number','website')->first();
            $data['patient_id'] = $patientID;
            $data['branch'] = Branch::find(Patient::whereId($patientID)->pluck('branch_id')->first());
        }

        if($request->has('download')){
            $pdf = \PDF::loadView('billing.statement-of-account', $data);
            return $pdf->download('Statement-Of-Account_'.preg_replace('/\s+/', '', $data['patient']->full_name).'.pdf');
        }

        return view('billing.statement-of-account', $data);
    }

    public function getSOASchedulesData(Request $request)
    {
        $schedules = Schedule::query()->wherePatientId(\Helper::encryptor('decrypt',$request->pid))
                        ->whereStatus('Completed')->orderBy('schedule_date','Asc');
        if($request->has('tPeriod') && !empty($request->input('tPeriod')))
            $schedules = $schedules->whereDate('schedule_date','<=',$request->input('tPeriod'));

        $schedules = $schedules->with('patient')->with('caregiver')->with('service');

        return \DataTables::of($schedules)
            ->editColumn('service_required', function ($schedule) {
                return isset($schedule->service)?ucwords($schedule->service->service_name):'';
            })
            ->editColumn('schedule_date', function ($schedule) {
                return isset($schedule->schedule_date)?$schedule->schedule_date->format('d-m-Y'):'-';
            })
            ->editColumn('caregiver.employee_id', function ($schedule) {
                return isset($schedule->caregiver)?$schedule->caregiver->employee_id:'-';
            })
            ->editColumn('caregiver.first_name', function ($schedule) {
                return isset($schedule->caregiver)?$schedule->caregiver->full_name:'-';
            })
            ->addColumn('rate', function ($schedule) {
                if(isset($schedule->amount))
                    return $schedule->amount;

                return \Helper::getServiceRate($schedule->service_required);
            })
            ->make(true);
    }

    public function getSOAPaymentsData(Request $request)
    {
        $payments = CasePayment::query()->wherePatientId(\Helper::encryptor('decrypt',$request->pid));
        if($request->has('tPeriod') && !empty($request->input('tPeriod')))
            $payments = $payments->whereDate('created_at','<=',$request->input('tPeriod'));

        $payments = $payments->with('patient')->with('invoice');

        return \DataTables::of($payments)
            ->editColumn('created_at', function ($payment) {
                return $payment->created_at?$payment->created_at->format('d-m-Y'):'-';
            })
            ->editColumn('payment_mode', function ($payment) {
                $str = $payment->payment_mode;
                if($payment->payment_mode == 'Cheque/DD')
                    $str .= '<br><small><b>No.:</b> '.$payment->cheque_dd_no.'<br><b>Date:</b> '.$payment->cheque_dd_date.'</small>';

                if($payment->payment_mode != 'Cash' && $payment->payment_mode != 'Cheque/DD'){
                    $str .= '<br><small>';
                    if(!empty($payment->card_number) && $payment->card_number!='null') $str .= '<b>Card Number: </b>'.$payment->card_number.'<br>';
                    if(!empty($payment->bank_txn) && $payment->bank_txn!='null') $str .= '<b>Txn No.: </b>'.$payment->bank_txn.'<br>';
                    $str .= $payment->bank_name?'<b>Bank: </b> '.$payment->bank_name.'</small>':'';
                }
                return $str;
            })
            ->editColumn('invoice.invoice_no', function ($payment) {
                return isset($payment->invoice)?$payment->invoice->invoice_no:'-';
            })
            ->rawColumns(['payment_mode'])
            ->make(true);
    }

    public function viewReceipt(Request $request, $id)
    {
        $id = \Helper::encryptor('decrypt',$id);

        $receipt = Receipt::find($id);
        if(!$receipt){
            return back()->with('alert_error','Could not find the receipt details!');
        }

        $profile = Profile::whereTenantId(\Helper::getTenantID())->first();
        if($request->has('download')){
            $pdf = \PDF::loadView('billing.receiptpdf', compact(['receipt','profile']));
            return $pdf->download('receipt_'.$receipt->receipt_no.'.pdf');
        }
        if($request->has('email')){
            if(filter_var($receipt->patient->email, FILTER_VALIDATE_EMAIL)){
                $pdf = \PDF::loadView('billing.receiptpdf', compact(['receipt','profile']));

                \Mail::send(new SendReceipt($receipt, $profile, $pdf->output()));
                return back()->with('alert_success','Receipt Sent Successfully !');
            }else{
                return back()->with('alert_error','Aborted Sending Email, Improper Email Id !');
            }
        }
        return view('billing.receipt',compact('receipt','profile'));
    }
}
