<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Helpers\Helper;

use App\Entities\FCMToken;
use App\Entities\Console\Freelancer;
use App\Entities\Physio\FreelancerProfile;
use App\Entities\Physio\Patient;
use App\Entities\Physio\Appointment;
use App\Entities\Physio\Assessment;
use App\Entities\Physio\Treatment;
use App\Entities\Physio\PainPoint;
use App\Entities\Physio\DoctorReport;
use App\Entities\Physio\ReferralCode;
use App\Entities\Physio\CreditBalance;
use App\Entities\Physio\SubscriptionPayment;
use App\Entities\Physio\PromoCode;
use App\Entities\Physio\PromoCodeLog;
use App\Entities\Physio\PatientBill;

class PhysioController extends Controller
{
    public function index()
    {
        return 'Physio API v1.0';
    }


    public static function setDBConnection()
    {
        // Set database name to tenant connection
        \Config::set('database.connections.tenant.database','apnacare_physiotherapy');
        // Set tenant as default connection
        \DB::setDefaultConnection('tenant');
    }

    public function convertNulltoEmpty($array, $single=true)
    {
        if($single){
            return array_map(function($v){ return (is_null($v)) ? "" : $v;},$array);
        }else{
            $data = [];
            foreach ($array as $arr) {
                $data[] = array_map(function($v){ return (is_null($v)) ? "" : $v;},$arr);
            }
            return $data;
        }
    }

    public function generateReferralCode($fname,$lname){
        $check_ref_code = substr($fname, 0, 3).mt_rand(100,999).substr(strrev($lname), 0, 3);
        $crc = ReferralCode::whereReferralCode($check_ref_code)->first();
        if($crc){
            $this->generateReferralCode($fname,$lname);
        }
        return $check_ref_code;
    }

    public function updateRegistrationToken(Request $request)
    {
        $userID = $request->user_id;
        $userType = $request->user_type;
        $token = $request->token;

        $res = false;

        if($userID && $userType && $token){
            $this->setDBConnection();

            // Check if entry exists
            $fcmToken = FCMToken::whereUserId($userID)->whereUserType($userType)->first();

            if($fcmToken){
                $fcmToken->token = $token;
                $res = $fcmToken->save();
            }else{
                $res = FCMToken::create(['user_id' => $userID, 'user_type' => $userType, 'token' => $token]);
            }
        }

        $data['status_code'] = 200;
        if($res)
            $data['result'] = (bool) $res;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function signup(Request $request)
    {
        $data = ['status_code' => 200, 'result' => false];

        $profile = $request->profile;
        $userType = $request->user_type;
        $userPassword = "";

        if($profile){
            if($userType == 'Freelancer'){
                // Create entry in centralconsole freelancer
                $freelancer = [
                    'status' => 'Active',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
                $tenant_id = Freelancer::insertGetId($freelancer);
                // Generate unique physio ID
                $physio_id = 'AP'.(10000 + intval($tenant_id));
                Freelancer::find($tenant_id)->update(['physio_id' => $physio_id]);

                if($tenant_id){
                    $userPassword = $profile['password'];
                    $profile['tenant_id'] = $tenant_id;
                    $profile['password'] = bcrypt($profile['password']);

                    self::setDBConnection();

                    // Create Profile
                    FreelancerProfile::create($profile);

                    //Check for referrer if exists and provided during signup
                    if(!empty($profile['referrer_code'])){
                        $referrer_check = ReferralCode::whereReferralCode($profile['referrer_code'])->first();
                    }else{
                        $referrer_check = null;
                    }
                    
                    if($referrer_check){
                        $referrer_id = $referrer_check->id;
                        $referrer_id_type = $referrer_check->referrer_type;
                    }else{
                        $referrer_id = null;
                        $referrer_id_type = null;
                    }

                    // Generate Referral Code

                    $ref_code = $this->generateReferralCode($profile['first_name'],$profile['last_name']);

                    $rcode = new ReferralCode;
                    $rcode->tenant_id = $tenant_id;
                    $rcode->referral_type = 'P';
                    $rcode->referral_code = $ref_code!=null?$ref_code:'';
                    $rcode->referrer_id = $referrer_id;
                    $rcode->referrer_id_type = $referrer_id_type;
                    $rcode->subscription_credits_used = 'N';
                    $rcode->status = 1;
                    $rcode->save();


                    if($referrer_id != null){
                        $credit_balance = CreditBalance::firstOrNew(['tenant_id' => $tenant_id]);
                        $credit_balance->credits = $credit_balance->credits + 100;
                        $credit_balance->save();
                        $cbal = $credit_balance->credits;
                    }
                    $data['result'] = $this->performLogin($profile['email'], $userPassword);
                    $data['result']['referral_code'] = $ref_code!=null?$ref_code:'';
                    $data['result']['registered_on'] = date('Y-m-d H:i:s');
                    $data['result']['credit_balance'] = isset($cbal)?$cbal:0;
                }
            }
        }

        return response()->json($data);
    }

    public function login(Request $request)
    {
        $email = $request->email;
        $password = $request->password;

        $data = ['status_code' => 200, 'result' => false];

        if($email && $password){
            $data['result'] = $this->performLogin($email, $password);
            if($data['result']){
                $ref_code = ReferralCode::whereTenantId($data['result']['tenant_id'])->whereReferralType('P')->value('referral_code');
                $cbal = CreditBalance::whereTenantId($data['result']['tenant_id'])->whereTenantType('P')->value('credits');
                $data['result']['referral_code'] = $ref_code!=null?$ref_code:'';
                $data['result']['credit_balance'] = isset($cbal)?$cbal:0;
            }
        }
        return response()->json($data);
    }

    public function subscriptionPayment(Request $request)
    {
        self::setDBConnection();
        
        $data = ['status_code' => 200, 'result' => false];

        $response = $request->payment;
        if($response){
            $type = strtoupper($response['plan_id']);

            $sub_payment = new SubscriptionPayment;
            $sub_payment->tenant_id        = $response['tenant_id'];
            $sub_payment->plan_id          = \App\Entities\Physio\SubscriptionPlan::wherePlanName($response['plan_id'])->value('id');
            $sub_payment->transaction_date = date_format(new \DateTime($response['transaction_date']), 'Y-m-d H:i:s');
            $sub_payment->transaction_id   = $response['transaction_id'];
            $sub_payment->amount_paid      = $response['amount_paid'];
            $sub_payment->credits_used     = $response['credits_used'];
            $sub_payment->total_amount     = $response['total_amount'];
            $sub_payment->status           = $response['status'];
            $sub_payment->payment_response = $response['payment_response'];
            $sub_payment->save();
            $data['result'] = $sub_payment->id;
            
            if($response['status'] == 'Success'){
                if($response['credits_used']){
                    $cb = CreditBalance::whereTenantId($response['tenant_id'])->first();
                    if($cb){
                        $cb->credits = $cb->credits - $response['credits_used'];
    	                $cb->save();
    	            }
    	        }

            	$referrer_check = ReferralCode::whereTenantId($response['tenant_id'])->whereReferrerIdType('P')->first();
            	if($referrer_check){
    	            $referrer_id = $referrer_check->referrer_id; //Referred Person's ID
    	            $referrer_id_type = $referrer_check->referrer_id_type; //Referred Person's Type
            	}else{
                	$referrer_id = $referrer_id_type = null;
            	}
            
            	switch ($type) 
            	{
                	case 'AP12':
                        //If Block - Credit to the CP's account on every transaction
                        //Else Block - Credit to the referred physio's account for the first time
                    	if($referrer_id != null && $referrer_id_type != null && $referrer_id_type == 'CP'){
                    		$credit_balance = CreditBalance::firstOrNew(['tenant_id' => $referrer_id]);
                    		$credit_balance->credits = $credit_balance->credits + 1000;
                    		$credit_balance->save();
                    	}elseif($referrer_id != null && $referrer_id_type != null && $referrer_type == 'P' && $referrer_check->subscription_credits_used == 'N'){
                    		$credit_balance = CreditBalance::firstOrNew(['tenant_id' => $referrer_id]);
                    		$credit_balance->credits = $credit_balance->credits + 100;
                    		$credit_balance->save();
                            //Disable the physio's ability to credit to the the referred physio's for the next all transactions
                    		ReferralCode::whereTenantId($response['tenant_id'])->whereReferralIdType('P')->update(['subscription_credits_used' => 'Y']);
                    	}
                        //Extend Plan Expiry Date from the date of transaction
                    	$fp = FreelancerProfile::whereTenantId($response['tenant_id'])->first();
                    	if($fp->expiry_date != null){
                    		$fp->expiry_date = date('Y-m-d', strtotime("+12 months"));
                    	}else{
                    		$fp->expiry_date = date('Y-m-d', strtotime("+12 months", strtotime($fp->expiry_date)));
                    	}
                    	$fp->save();
                	break;

                	case 'AP6':
                        //If Block - Credit to the CP's account on every transaction
                        //Else Block - Credit to the referred physio's account for the first time
                    	if($referrer_id != null && $referrer_id_type != null && $referrer_id_type == 'CP'){
                    		$credit_balance = CreditBalance::firstOrNew(['tenant_id' => $referrer_id]);
                    		$credit_balance->credits = $credit_balance->credits + 375;
                    		$credit_balance->save();
                    	}elseif($referrer_id != null && $referrer_id_type != null && $referrer_type == 'P' && $referrer_check->subscription_credits_used == 'N'){
                    		$credit_balance = CreditBalance::firstOrNew(['tenant_id' => $referrer_id]);
                    		$credit_balance->credits = $credit_balance->credits + 100;
                    		$credit_balance->save();
                            //Disable the physio's ability to credit to the the referred physio's for the next all transactions
                    		ReferralCode::whereTenantId($response['tenant_id'])->whereReferralIdType('P')->update(['subscription_credits_used' => 'Y']);
                    	}
                        //Extend Plan Expiry Date from the date of transaction
                    	$fp = FreelancerProfile::whereTenantId($response['tenant_id'])->first();
                    	if($fp->expiry_date != null){
                    		$fp->expiry_date = date('Y-m-d', strtotime("+6 months"));
                    	}else{
                    		$fp->expiry_date = date('Y-m-d', strtotime("+6 months", strtotime($fp->expiry_date)));
                    	}
                    	$fp->save();
                	break;

                	case 'AP3':
                        //If Block - Credit to the CP's account on every transaction
                        //Else Block - Credit to the referred physio's account for the first time
                    	if($referrer_id != null && $referrer_id_type != null && $referrer_id_type == 'CP'){
                    		$credit_balance = CreditBalance::firstOrNew(['tenant_id' => $referrer_id]);
                    		$credit_balance->credits = $credit_balance->credits + 180;
                    		$credit_balance->save();
                    	}elseif($referrer_id != null && $referrer_id_type != null && $referrer_type == 'P' && $referrer_check->subscription_credits_used == 'N'){
                    		$credit_balance = CreditBalance::firstOrNew(['tenant_id' => $referrer_id]);
                    		$credit_balance->credits = $credit_balance->credits + 100;
                    		$credit_balance->save();
                            //Disable the physio's ability to credit to the the referred physio's for the next all transactions
                    		ReferralCode::whereTenantId($response['tenant_id'])->whereReferralIdType('P')->update(['subscription_credits_used' => 'Y']);
                    	}
                        //Extend Plan Expiry Date from the date of transaction
                    	$fp = FreelancerProfile::whereTenantId($response['tenant_id'])->first();
                    	if($fp->expiry_date != null){
                    		$fp->expiry_date = date('Y-m-d', strtotime("+3 months"));
                    	}else{
                    		$fp->expiry_date = date('Y-m-d', strtotime("+3 months", strtotime($fp->expiry_date)));
                    	}
                    	$fp->save();
                	break;

                	default:
                        //If Block - Credit to the CP's account on every transaction
                        //Else Block - Credit to the referred physio's account for the first time
                    	if($referrer_id != null && $referrer_id_type != null && $referrer_id_type == 'CP'){
                    		$credit_balance = CreditBalance::firstOrNew(['tenant_id' => $referrer_id]);
                    		$credit_balance->credits = $credit_balance->credits + 150;
                    		$credit_balance->save();
                    	}elseif($referrer_id != null && $referrer_id_type != null && $referrer_type == 'P' && $referrer_check->subscription_credits_used == 'N'){
                    		$credit_balance = CreditBalance::firstOrNew(['tenant_id' => $referrer_id]);
                    		$credit_balance->credits = $credit_balance->credits + 100;
                    		$credit_balance->save();
                            //Disable the physio's ability to credit to the the referred physio's for the next all transactions
                    		ReferralCode::whereTenantId($response['tenant_id'])->whereReferralIdType('P')->update(['subscription_credits_used' => 'Y']);
                    	}
                        //Extend Plan Expiry Date from the date of transaction
                        $fp = FreelancerProfile::whereTenantId($response['tenant_id'])->first();
                        if($fp->expiry_date != null){
                            $fp->expiry_date = date('Y-m-d', strtotime("+1 month"));
                        }else{
                            $fp->expiry_date = date('Y-m-d', strtotime("+1 month", strtotime($fp->expiry_date)));
                        }
                        $fp->save();
                    break;
                }
            }
        }
        
        return response()->json($data, 200);
    }

    public function usePromo(Request $request)
    {
        $promo_check = PromoCode::whereCode($request->promo_code)->whereDate('expiry','>=', date('Y-m-d H:i:s'))->first();
        $physio_check = FreelancerProfile::whereTenantId($request->tenant_id)->first();
        $check_promo_log = PromoCodeLog::whereTenantId($request->tenant_id)->wherePromoCodeId($promo_check->id)->first();

        if($promo_check && $physio_check && empty($check_promo_log)){
            $cb = CreditBalance::whereTenantId($request->tenant_id)->whereTenantType('P')->first();
            $cb->credits = $cb->credits + $promo_check->giveaway_credits;
            
            $cb->save();

            $pcl = new PromoCodeLog;
            $pcl->tenant_id = $request->tenant_id;
            $pcl->promo_code_id = $promo_check->id;
            $pcl->date_of_use = date('Y-m-d H:i:s');

            $pcl->save();
            return response()->json(200);
        }
        return response()->json(500);
    }

    public function performLogin($email, $password)
    {
        self::setDBConnection();
        $data = [];
        // Check for login credentials
        $profile = FreelancerProfile::whereEmail($email)->first();
        if($profile && \Hash::check($password,$profile->password)){
            $userProfile = collect($profile)->except('created_at','updated_at','deleted_at','password','remember_token')->toArray();
            $userProfile['physio_id'] = $profile->freelancer->physio_id;
            $userProfile['password'] = $password;
            $userProfile['org_hash'] = \Helper::encryptor('encrypt',$profile->freelancer->database_name);
            $userProfile['profile_image'] = isset($profile->profile_image)?'https://apnafocus.com/uploads/freelancers/'.$profile->profile_image:'';
            $userProfile['tenant_id'] = $profile->tenant_id;
            $userProfile['registered_on'] = $profile->created_at->format('Y-m-d H:i:s');

            $data =  $this->convertNulltoEmpty($userProfile);
        }

        return $data;
    }
    
    public function forgotPassword(Request $request)
    {
        self::setDBConnection();
        
        $data = ['status_code' => 200, 'result' => false];

        $email = $request->email;
        $password = $request->password;        

        if(isset($email) && isset($password)){
            $profile = FreelancerProfile::whereEmail($email)->first();
            if($profile){
                $profile->password = bcrypt($password);
                $profile->save();
                
                $data['result'] = true;
            }            
        }

        return response()->json($data);
    }
    
    public function updateProfile(Request $request)
    {
        self::setDBConnection();
        
        $id = $request->user_id;
        $profileData = $request->profile;
        $res = false;

        $data = ['status_code' => 200, 'result' => false];

        if($id && $profileData){
            $res = FreelancerProfile::withoutGlobalScopes()->whereTenantId($id)->update($profileData);
        }

        if($id && $res)
            $data['result'] = $res;
        else
            $data['error'] = true;

        return response()->json($data);
    }

    public function getCaseData($user_type, $tenant_id)
    {
        self::setDBConnection();
        
        $data = ['status_code' => 200, 'result' => false];

        if($user_type == 'Freelancer'){
            $data['result']['patients']     = $this->convertNulltoEmpty(Patient::whereTenantId($tenant_id)->get()->toArray(), false);
            $data['result']['appointments'] = $this->convertNulltoEmpty(Appointment::whereTenantId($tenant_id)->get()->toArray(), false);
            $data['result']['assessments']  = $this->convertNulltoEmpty(Assessment::whereTenantId($tenant_id)->get()->toArray(), false);
            $data['result']['treatments']   = $this->convertNulltoEmpty(Treatment::whereTenantId($tenant_id)->get()->toArray(), false);
            $data['result']['painpoints']   = $this->convertNulltoEmpty(PainPoint::whereTenantId($tenant_id)->get()->toArray(), false);
        }

        return response()->json($data);
    }

    public function createAssessment(Request $request)
    {
        self::setDBConnection();
        
        $data = ['status_code' => 200, 'result' => false];

        $assessmentID = Assessment::insertGetId($request->assessment);
        if($assessmentID){
            $data['result'] = $assessmentID;
        }

        return response()->json($data);
    }

    public function createAppointment(Request $request)
    {
        self::setDBConnection();
        
        $data = ['status_code' => 200, 'result' => false];

        $appointmentID = Appointment::insertGetId($request->appointment);
        if($appointmentID){
            $data['result'] = $appointmentID;
        }

        return response()->json($data);
    }

    public function createPatient(Request $request)
    {
        self::setDBConnection();
        
        $data = ['status_code' => 200, 'result' => false];

        $patientID = Patient::insertGetId($request->patient);
        if($patientID){
            $data['result'] = $patientID;
        }

        return response()->json($data);
    }
    
    public function uploadPatientPhoto(Request $request)
    {
        self::setDBConnection();
        
        $data = ['status_code' => 200, 'result' => false];
        
        $tenantID = $request->tenant_id;
        $patientID = $request->patient_id;
        $file = $request->file('patient_photo');
        
        if($tenantID && $patientID && $file->isValid()){
            $patient = Patient::whereTenantId($tenantID)->whereId($patientID)->first();
            // Upload image and send updated filename to response
            if($patient){
                $path = 'uploads/apnaphysio/'.\Helper::encryptor('encrypt',$tenantID).'/'.\Helper::encryptor('encrypt',$patientID);
                $filePath = public_path().'/'.$path;
                if(!is_dir($filePath)){
                    \File::makeDirectory($filePath, 0777, true);
                }

                $fileName = \Helper::encryptor('encrypt',$patientID).'.'.$file->extension();
                $file->move($filePath, $fileName);

                $patient->update(['photo_path' => asset($path.'/'.$fileName)]);                

                $data['result'] = asset($path.'/'.$fileName);
            }
        }
        
        return response()->json($data);
    }

    public function createTreatment(Request $request)
    {
        self::setDBConnection();
        
        $treatment = $request->treatment;
        $data = ['status_code' => 200, 'result' => false];
        $treatment['treatment_date'] = \Carbon\Carbon::parse($request->treatment['treatment_date'])->format('Y-m-d H:i:s');

        $treatmentId = Treatment::insertGetId($treatment);
        if($treatmentId){
            $data['result'] = $treatmentId;
        }

        return response()->json($data);
    }

    public function createPainPoints(Request $request)
    {
        self::setDBConnection();
        
        $data = ['status_code' => 200, 'result' => false];

        $painPointID = PainPoint::insertGetId($request->painpoints);
        if($painPointID){
    		$data['result'] = $painPointID;
        }

        return response()->json($data);
    }
    
    public function addDoctorReport(Request $request)
    {
        self::setDBConnection();
        
        $data = ['status_code' => 200, 'result' => false];
        
        $tenantID = $request->tenant_id;
        $patientID = $request->patient_id;
        
        if($tenantID && $patientID){
            $pdata = [
                'tenant_id' => $tenantID,
                'patient_id' => $patientID,
                'report_name' => $request->report_name,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
            $id = DoctorReport::insertGetId($pdata);
            
            // Upload image and send updated filename to response
            $file = $request->file('report_image');
            if($id && isset($file) && $file->isValid()){
                $path = 'uploads/apnaphysio/'.\Helper::encryptor('encrypt',$tenantID).'/'.\Helper::encryptor('encrypt',$patientID).'/'.'documents';
                $filePath = public_path().'/'.$path;
                if(!is_dir($filePath)){
                    \File::makeDirectory($filePath, 0777, true);
                }

                $fileName = 'report_'.\Helper::encryptor('encrypt',$id).'.'.$file->extension();
                $file->move($filePath, $fileName);

                DoctorReport::find($id)
                                ->update(['file_path' => asset($path.'/'.$fileName)]);

                $data['result'] = asset($path.'/'.$fileName);
            }            
        }
        
        return response()->json($data);
    }

    public function billCreate(Request $request)
    {   
        self::setDBConnection();
        $data = ['status_code' => 200, 'bill_id' => 0, 'result' => false];
        $bill = $request->bill;

        $patientBillId = PatientBill::insertGetId([
            "tenant_id"      => $bill['tenant_id'],
            "patient_id"     => $bill['patient_id'],
            "appointment_id" => $bill['appointment_id'],
            "amount"         => $bill['amount'],
            "bill_date"      => date('Y-m-d'),
            "status"         => $bill['status']
        ]);

        if($bill['appointment_id']){
            Appointment::whereIn('id',explode(',',$bill['appointment_id']))->update(['billing_status' => 'Billed']);
        }

        if($bill['status'] == "Paid" && $patientBillId != 0){
            PatientBill::whereId($patientBillId)->update(['invoice_number' => 'APY'.$bill['tenant_id'].$patientBillId]);
        }
        
        if($patientBillId != 0)
            $data = ['status_code' => 200, 'bill_id' => $patientBillId,'result' => true];
        return response()->json($data);
    }

    public function updateBillStatus(Request $request)
    {
        self::setDBConnection();
        $data = ['status_code' => 200, 'result' => false];
        if($request->bill_id && $request->tenant_id){
            if($request->status == 'Paid')
                PatientBill::whereId($request->bill_id)->update(['invoice_number' => 'APY'.$request->tenant_id.$request->bill_id,'status' => $request->status]);
            else
                PatientBill::whereId($request->bill_id)->update(['status' => $request->status]);
            $data = ['status_code' => 200, 'result' => true];    
        }
        return response()->json($data);
    }
}
