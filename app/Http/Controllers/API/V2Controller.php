<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Intervention\Image\Exception\ImageException;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Helpers\Helper;

use App\Entities\User;
use App\Entities\Profile;
use App\Entities\Caregiver\Caregiver;
use App\Entities\Leaves;
use App\Entities\LeadRequests;
use App\Entities\LeaveApproval;
use App\Entities\Patient;
use App\Entities\PatientIncident;
use App\Entities\FCMToken;
use App\Entities\Console\FCMTokenFamily;
use App\Entities\Lead;
use App\Entities\WorkLog;
use App\Entities\Schedule;
use App\Entities\DailyRevenueStorage;
use App\Entities\Console\Subscriber;
use App\Entities\Console\CustomerRegistrations;
use App\Entities\Console\CustomerLinks;
use App\Entities\CaseForm;
use App\Entities\CaseFollowUp;
use App\Entities\CaseServiceOrder;
use App\Entities\WorkLogFeedback;
use App\Entities\PatientWellnessFeedback;
use App\Entities\Caregiver\Verification;
use App\Entities\Caregiver\ProfessionalDetails;
use App\Entities\Caregiver\AccountDetails;
use App\Entities\Attendance;
use App\Entities\Invoice;
use App\Entities\InvoiceItems;
use App\Entities\Settings;
use App\Entities\Masters\Service;
use App\Entities\Masters\ReferralSource;
use App\Entities\Masters\Task;
use App\Entities\Masters\Specialization;
use App\Entities\CaseDisposition;

use Carbon\Carbon;
use DateInterval;
use DateTime;
use DatePeriod;

class V2Controller extends Controller
{
    public function connectToDB($dbName='apnacare_providerdb')
    {
        \Config::set('database.connections.tenant.database',$dbName);
        \DB::setDefaultConnection('tenant');
    }

    public function checkVersion(Request $request)
    {
        $updateRequired = false;
        if($request->has('version') && $request->has('app')){
            if(intval($request->version) < intval(\Config::get('mobileapp.'.$request->input('app').'.version'))){
                $updateRequired = true;
                $updateFilePath = asset('uploads/apk/'.$request->input('app').'/app-release-'.\Config::get('mobileapp.version').'.apk');
            }
        }

        $data['status_code'] = 200;
        $data['result']['version'] = \Config::get('mobileapp.'.$request->input('app').'.version');
        $data['result']['update_required'] = $updateRequired;
        if($updateRequired){
            $data['result']['update_file'] = $updateFilePath;
        }

        return response()->json($data, 200);
    }

    public function updateRegistrationToken(Request $request)
    {
        $userID = $request->user_id;
        $userType = $request->user_type;
        $token = $request->token;

        $res = false;

        if($userID && $userType && $token){
            // Check if entry exists
            $fcmToken = FCMToken::whereUserId($userID)->whereUserType($userType)->first();

            if($fcmToken){
                $fcmToken->token = $token;
                $res = $fcmToken->save();
            }else{
                $res = FCMToken::create(['user_id' => $userID, 'user_type' => $userType, 'token' => $token]);
            }
        }

        $data['status_code'] = 200;
        if($res)
            $data['result'] = (bool) $res;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function sendEmergencyAlert(Request $request)
    {
        $caregiverID = $request->caregiver_id;
        $res = false;

        if($caregiverID){
            $caregiver = Caregiver::withoutGlobalScopes()->whereId($caregiverID)->first();
            if($caregiver){
                $tenantID = $caregiver->tenant_id;
                $userIDs = User::withoutGlobalScopes()->whereTenantId($tenantID)
                            ->whereUserId($caregiver->professional->manager)
                            ->pluck('id')->toArray();

                $tokensRes = FCMToken::whereIn('user_id',$userIDs)->whereUserType('Manager')->get();
                if($tokensRes){
                    $res = true;
                    $tokens = $tokensRes->pluck('token')->toArray();

                    $profilePic = '';
                    $path = public_path('uploads/provider/'.\Helper::encryptor('encrypt',$caregiver->tenant_id).'/caregivers/'.\Helper::encryptor('encrypt',$caregiver->id));
                    if($caregiver->profile_image && file_exists(public_path('uploads/provider/'.\Helper::encryptor('encrypt',$caregiver->tenant_id).'/caregivers/'.\Helper::encryptor('encrypt',$caregiver->id).'/'.$caregiver->profile_image))){
                        //$profilePic = 'http://192.168.1.109/apnacare_saas_nitish/public/uploads/caregivers/'.$c->profile_image;
                        $profilePic = url('uploads/provider/'.\Helper::encryptor('encrypt',$caregiver->tenant_id).'/caregivers/'.\Helper::encryptor('encrypt',$caregiver->id).'/'.$caregiver->profile_image);
                    }

                    $pdata = [
            			'type' => 'emergency',
            			'message' => 'One of your caregiver is in emergency',
                        'name' => $caregiver->full_name,
                        'phone' => $caregiver->mobile_number,
                        'profile_url' => $profilePic,
                        'location' => '',
                    ];

                    $response = \App\Helpers\PushMessaging::send($tokens, 'Emergency Alert - CareManager','One of your caregiver is in emergency',$pdata);
                }
            }
        }

        $data['status_code'] = 200;
        if($res){
            $data['result'] = (bool) $res;
            $data['response'] = $response;
        }
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function getProfile(Request $request)
    {
        $id = $request->user_id;
        $profile = [];
        $basicDetails = [];
        $professionalDetails = [];
        $bankDetails = [];

        if($id){
            $user = User::withoutGlobalScopes()->whereUserId($id)->first();
            if($user){
                $caregiver = Caregiver::withoutGlobalScopes()->whereId($user->user_id)->first();
                if($caregiver){
                    $profilePic = '';
                    $path = public_path('uploads/provider/'.\Helper::encryptor('encrypt',$user->tenant_id).'/caregivers/'.\Helper::encryptor('encrypt',$caregiver->id));
                    if($caregiver->profile_image && file_exists(public_path('uploads/provider/'.\Helper::encryptor('encrypt',$user->tenant_id).'/caregivers/'.\Helper::encryptor('encrypt',$caregiver->id).'/'.$caregiver->profile_image))){
                        $profilePic = url('uploads/provider/'.\Helper::encryptor('encrypt',$user->tenant_id).'/caregivers/'.\Helper::encryptor('encrypt',$caregiver->id).'/'.$caregiver->profile_image);
                    }
                    $profile = [
                        'id' => Helper::str($caregiver->id),
                        'tenant_id' => Helper::str($caregiver->tenant_id),
                        'first_name' => Helper::str($caregiver->first_name),
                        'middle_name' => Helper::str($caregiver->middle_name),
                        'last_name' => Helper::str($caregiver->last_name),
                        'gender' => Helper::str($caregiver->gender),
                        'blood_group' => Helper::str($caregiver->blood_group),
                        'marital_status' => Helper::str($caregiver->marital_status),
                        'mobile_number' => Helper::str($caregiver->mobile_number),
                        'email' => Helper::str($caregiver->email),
                        'street_address' => Helper::str($caregiver->current_address),
                        'area' => Helper::str($caregiver->current_area),
                        'city' => Helper::str($caregiver->current_city),
                        'state' => Helper::str($caregiver->current_state),
                        'country' => Helper::str($caregiver->current_country),
                        'specialization' => isset($caregiver->professional)?Helper::str($caregiver->professional->specialization):'-',
                        'specialization_name' => isset($caregiver->professional->specializations)?Helper::str($caregiver->professional->specializations->specialization_name):'-',
                        'qualification' => isset($caregiver->professional)?Helper::str($caregiver->professional->qualification):'-',
                        'college_name' => isset($caregiver->professional)?Helper::str($caregiver->professional->college_name):'-',
                        'experience' => isset($caregiver->professional)?Helper::str($caregiver->professional->experience):'-',
                        'pan_number' => isset($caregiver->account)?Helper::str($caregiver->account->pan_number):'-',
                        'account_name' => isset($caregiver->account)?Helper::str($caregiver->account->account_name):'-',
                        'account_number' => isset($caregiver->account)?Helper::str($caregiver->account->account_number):'-',
                        'bank_name' => isset($caregiver->account)?Helper::str($caregiver->account->bank_name):'-',
                        'bank_branch' => isset($caregiver->account)?Helper::str($caregiver->account->bank_branch):'-',
                        'ifsc_code' => isset($caregiver->account)?Helper::str($caregiver->account->ifsc_code):'-',
                        'profile_url' => $profilePic,
                        'languages_known' => $caregiver->professional->languages_known,
                    ];
                }
            }
        }

        $data['status_code'] = 200;
        if($profile)
            $data['result'] = $profile;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function markAttendance(Request $request)
    {
        $manager_user_id = User::whereUserId($request->caregiver_id)->pluck('id')->first();
        $date = Carbon::parse($request->date)->format('Y-m-d');
        $att_data = [
            'tenant_id' => $request->tenant_id,
            'caregiver_id' => (int) $request->caregiver_id,
            'schedule_id' => (int) $request->schedule_id,
            'date' => $date,
            'source' => 'App',
            'marked_by' => (int) $manager_user_id,
            'status' => 'P',
            'on_training' => 'No'
        ];

        if($request->intime){
            $att_data['punch_in'] = Carbon::parse($request->intime)->format('h:i A');
        }else if($request->outtime){
            $att_data['punch_out'] = Carbon::parse($request->outtime)->format('h:i A');
        }

        $att = Attendance::whereDate('date',$date)->whereCaregiverId($request->caregiver_id)->whereScheduleId($request->schedule_id)->first();
        if($att){
            $att->update($att_data);
        }else{
            $att = Attendance::create($att_data);
        }

        $attBench = Attendance::whereDate('date',$date)->whereCaregiverId($request->caregiver_id)->whereNull('schedule_id')->get();
        if(count($attBench)){
            foreach ($attBench as $ab) {
                $ab->delete();
            }
        }

        if(isset($request->schedule_id)){
            $schedule = Schedule::whereId($request->schedule_id)->first();
            if($schedule->status != 'Completed'){
                if($request->outtime){
                    $schedule->update(['status' => 'Completed']);

                    $drs = DailyRevenueStorage::whereDate('date',$schedule->schedule_date)->first();

                    if($drs){
                       $drs->flag = 'Unsorted';
                       $drs->updated_at = date('Y-m-d H:i:s');

                       $drs->save();
                    }
                }
            }
        }

        $data['status_code'] = 200;

        if($att)
            $data['result'] = true;
        else
            $data['error'] = true;

        return response()->json($data,200);
    }

    public function addPatientWellnessFeedback(Request $request)
    {
        $orgHash            = $request->org_hash;
        $tenant_id          = $request->tenant_id;
        $lead_id            = $request->lead_id;
        $schedule_id        = $request->schedule_id;
        $patient_id         = $request->patient_id;
        $caregiver_id       = $request->caregiver_id;

        $happiness          = $request->happiness;
        $appetite           = $request->appetite;
        $physical_activity  = $request->physical_activity;
        $communication      = $request->communication;
        $anger              = $request->anger;

        $dbName = \Helper::encryptor('decrypt',$orgHash);
        \Config::set('database.connections.tenant.database',$dbName);
        \DB::setDefaultConnection('tenant');

        if($tenant_id && $lead_id && $schedule_id && $patient_id && $caregiver_id){
            if($happiness && $appetite && $physical_activity && $communication && $anger){

                $cl = new PatientWellnessFeedback;
                $cl->tenant_id = $tenant_id;
                $cl->lead_id =  $lead_id;
                $cl->schedule_id = $schedule_id;
                $cl->patient_id =  $patient_id;
                $cl->caregiver_id = $caregiver_id;
                $cl->happiness =  $happiness;
                $cl->appetite = $appetite;
                $cl->physical_activity =  $physical_activity;
                $cl->communication = $communication;
                $cl->anger =  $anger;
                $cl->save();

                return response()->json(['error' => false, 'result' => 'Wellness feedback sent successfully'], 200);
            } else {
                return response()->json(['error' => true, 'result' => 'Please complete the feedback form'], 401);  
            }
        } else {
            return response()->json(['error' => true, 'result' => 'Unable to send the feedback'], 401);
        }
    }

    public function getPatientWellnessFeedback(Request $request)
    {
        $orgHash    = $request->org_hash;
        $patient_id = $request->patient_id;

        $dbName = \Helper::encryptor('decrypt',$orgHash);
        \Config::set('database.connections.tenant.database',$dbName);
        \DB::setDefaultConnection('tenant');

        $patient_wellness_feedback = [];

        if($patient_id){
            $feedback = PatientWellnessFeedback::wherePatient_id($patient_id)->get();
            if($feedback){
                foreach ($feedback as $f) {
                    $patient_wellness_feedback[] = [
                        'schedule_date' => Helper::str(Carbon::parse($f->created_at)->format('d-m-Y')),
                        'happiness' => Helper::str($f->happiness),
                        'appetite' => Helper::str($f->appetite),
                        'physical_activity' => Helper::str($f->physical_activity),
                        'communication' => Helper::str($f->communication),
                        'anger' => Helper::str($f->anger),
                    ];
                }
            }
        }

        $data['status_code'] = 200;

        if($patient_wellness_feedback){
            $data['result'] = $patient_wellness_feedback;
            $data['error'] = false;
        }else{
            $data['error'] = true;
        }

        return response()->json($data, 200);
    }

    public function locationUpdate(Request $request)
    {
        $caregiverID = $request->caregiver_id;
        $lat = $request->lat;
        $lng = $request->lng;
        $timestamp = $request->timestamp;
        $res = null;

        if($caregiverID && $lat && $lng){
            $tracking = [
                'caregiver_id' => $caregiverID,
                'lat' => $lat,
                'lng' => $lng,
                'server_timestamp' => date('Y-m-d H:i:s'),
                'app_timestamp' => $timestamp,
            ];

            $res = Tracking::create($attendance);
        }

        $data['status_code'] = 200;
        if($res)
            $data['result'] = true;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function getCaregivers(Request $request)
    {
        $id = $request->tenant_id;
        $caregiverID = $request->user_id;
        $managerID = $request->manager_id;
        $caregivers = [];

        if($id){
            $caregiverResult = Caregiver::withoutGlobalScopes()->whereTenantId($id);

            if($caregiverID){
                $caregiverResult->whereId($caregiverID);
            }

            if($managerID){
                $caregiverResult->whereHas('professional', function($q) use($managerID){
                    $q->whereManager($managerID);
                });
            }

            $caregiverResult = $caregiverResult->get();

            if(count($caregiverResult)){
                foreach ($caregiverResult as $c) {
                    $profilePic = '';
                    $path = public_path('uploads/provider/'.\Helper::encryptor('encrypt',$c->tenant_id).'/caregivers/'.\Helper::encryptor('encrypt',$c->id));
                    if($c->profile_image && file_exists(public_path('uploads/provider/'.\Helper::encryptor('encrypt',$c->tenant_id).'/caregivers/'.\Helper::encryptor('encrypt',$c->id).'/'.$c->profile_image))){
                        //$profilePic = 'http://192.168.1.109/apnacare_saas_nitish/public/uploads/caregivers/'.$c->profile_image;
                        $profilePic = url('uploads/provider/'.\Helper::encryptor('encrypt',$c->tenant_id).'/caregivers/'.\Helper::encryptor('encrypt',$c->id).'/'.$c->profile_image);
                    }
                    $caregivers[] = [
                        'id'                  => Helper::str($c->id),
                        'tenant_id'           => Helper::str($c->tenant_id),
                        'employee_id'         => Helper::str($c->employee_id),
                        'first_name'          => Helper::str($c->first_name),
                        'middle_name'           => Helper::str($c->middle_name),
                        'last_name'           => Helper::str($c->last_name),
                        'gender'              => Helper::str($c->gender),
                        'date_of_birth'       => Helper::getDate($c->date_of_birth, true),
                        'blood_group'         => Helper::str($c->blood_group),
                        'marital_status'      => Helper::str($c->marital_status),
                        'mobile_number'       => Helper::str($c->mobile_number),
                        'email'               => Helper::str($c->email),
                        'current_address'     => Helper::str($c->current_address),
                        'current_city'        => Helper::str($c->current_city),
                        'current_state'       => Helper::str($c->current_state),
                        'work_status'         => Helper::str($c->work_status),
                        'specialization_id'   => isset($c->professional)?Helper::str($c->professional->specialization):0,
                        'specialization_name' => (isset($c->professional) && isset($c->professional->specializations))?Helper::str($c->professional->specializations->specialization_name):'',
                        'qualification'       => isset($c->professional)?Helper::str($c->professional->qualification):'',
                        'college_name'        => isset($c->professional)?Helper::str($c->professional->college_name):'-',
                        'experience'          => isset($c->professional)?intval(Helper::str($c->professional->experience)):'',
                        'languages_known'     => isset($c->professional)?Helper::str($c->professional->languages_known):'',
                        'profile_image'       => $profilePic
                    ];
                }
            }
        }

        $data['status_code'] = 200;
        if($caregivers)
            $data['result']['caregivers'] = $caregivers;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function updateCaregiverStatus(Request $request)
    {
        $caregiverID = $request->caregiver_id;
        $status = $request->status;

        $data['status_code'] = 200;

        if($caregiverID && $status){
            $caregiver = Caregiver::whereId($caregiverID)->first();
            if($caregiver){
                $caregiver->work_status = $status;
                $caregiver->save();

                $data['result'] = true;
            }
        }else{
            $data['error'] = true;
        }

        return response()->json($data, 200);
    }

    public function createLead(Request $request)
    {
        $leadData = $request->input('lead');
        $res = false;
        $patientDetails = collect($leadData)
                    ->only('tenant_id','first_name','patient_age','patient_weight','gender','street_address','area','city','contact_number','alternate_number','created_at')
                    ->toArray();
        $caseDetails = collect($leadData)
                    ->only('tenant_id','case_description','medical_conditions','service_required','estimated_duration','gender_preference','language_preference','rate_agreed','registration_amount','hospital_name','primary_doctor_name','referral_category','referral_source','referrer_name','manager_id','status','remarks','created_at')
                    ->toArray();
        //dd([$patientDetails, $caseDetails]);

        if($patientDetails && $caseDetails){
            // Create Patient
            $patientDetails['updated_at'] = $patientDetails['created_at'];
            $patientID = Patient::insertGetId($patientDetails);

            if($patientID > 0){
                $caseDetails['patient_id'] = $patientID;
                $caseDetails['updated_at'] = $caseDetails['created_at'];

                $res = Lead::create($caseDetails);
            }
        }

        $data['status_code'] = 200;
        if($res)
            $data['result'] = true;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function getAssessments(Request $request)
    {
        $results['assessments'] = [];

        $userID   = $request->user_id;
        $userType = $request->user_type;
        
        $leads = Lead::withoutGlobalScopes()
                    ->with(['patient','assessment','worklog','comments','schedules'])
                    ->whereAssessmentDate(date('d-m-Y'))
                    ->whereNotIn('status',['Closed','Dropped']);

        if($userType == 'Manager'){
            if(isset($userID) && $userID > 0){
                $leads->whereRaw(\DB::raw('FIND_IN_SET('.$userID.', manager_id) > 0'));
            }
        }

        $leads = $leads->get();        

        if($leads){
            $assessments = [];

            foreach ($leads as $lead) {
                // Lead details
                $assessments[] = [
                    'lead_id'                 => $lead->id,
                    'episode_id'         => Helper::str($lead->episode_id),
                    'patient_primary_id' => Helper::str($lead->patient->id),
                    'patient_id'         => Helper::str($lead->patient->patient_id),
                    'episode_created_at' => Helper::getDate($lead->created_at),
                    'enquirer_name'      =>Helper::str($lead->patient->enquirer_name),
                    'contact_number'     => Helper::str($lead->patient->contact_number),
                    'street_address'     => Helper::str($lead->patient->street_address),
                    'area'               => Helper::str($lead->patient->area),
                    'city'               => Helper::str($lead->patient->city),
                    'zipcode'            => Helper::str($lead->patient->zipcode),
                    'state'              => Helper::str($lead->patient->state),
                    'latitude'           => Helper::str($lead->patient->latitude),
                    'longitude'          => Helper::str($lead->patient->longitude),
                    'assessment_time'    =>Helper::str($lead->assessment_time)
                ];
            }

            $results['assessments'] = $assessments;
        }

        $data['status_code'] = 200;
        $data['result'] = $results;

        return response()->json($data, 200);
    }

    public function getLeads(Request $request)
    {
        $results['leads'] = [];
        $results['patients'] = [];
        $results['assessments'] = [];
        $results['followups'] = [];
        $results['worklogs'] = [];
        $results['schedules'] = [];
        $results['comments'] = [];
        $results['masters'] = [];

        $leadID = $request->lead_id;
        $userID = $request->user_id;
        $userType = $request->user_type;
        
        if($userType == 'Customer'){
            $leads = Lead::select('leads.*')
                ->with(['patient','schedules','assessment','followUp','worklog','comments']);
        }else{
            $leads = Lead::select('leads.*')
                ->with(['patient','schedules','assessment','followUp','worklog','comments'])
                ->whereNotIn('leads.status',['Closed','Dropped']);
        }

        if($leadID > 0){
            $leads->where('leads.id',$leadID);
        }

        if($userType == 'Manager'){
            $leadIDs = Schedule::select('lead_id')
                ->whereHas('caregiver.professional', function($q) use($userID){
                    $q->whereRole('Caregiver')->whereManager($userID);
                })->get()->pluck('lead_id')->unique()->values()->toArray();            
            if(count($leadIDs) && $leadIDs > 0){
                $leads->whereRaw(\DB::raw('FIND_IN_SET('.$userID.', leads.manager_id) > 0'));
                // $leads->whereRaw(\DB::raw('( FIND_IN_SET('.$userID.', manager_id) > 0 OR id IN ('.implode($leadIDs,",").'))'));
            }else{
                $leads->whereRaw(\DB::raw('FIND_IN_SET('.$userID.', leads.manager_id) > 0'));
            }
        }        


        if($userType == 'Caregiver'){
            $leads->join('case_schedules', 'case_schedules.lead_id', '=', 'leads.id')
            ->where('case_schedules.caregiver_id',$userID)
            ->whereIn('case_schedules.status',['Pending','Completed'])
            ->where('case_schedules.schedule_date',date('Y-m-d'));
        }

        if($userType == 'Customer'){
            $leads->where('leads.patient_id',$userID);
        }

        $leads = $leads->get();        

        if($leads){
            $cases = [];
            $patients = [];
            $assessments = [];
            $followups = [];
            $worklogsMerged = [];
            $feedbacks = [];
            $schedules = [];
            $comments = [];
            $masters = [];

            if($userType == 'Customer')
                $caregiversList = [];

            $patientID = null;
            $caregiverIDs = [];

            foreach ($leads as $lead) {
                // Lead details
                $cases[] = [
                    'id' => $lead->id,
                    'episode_id' => Helper::str($lead->episode_id),
                    'patient_id' => Helper::str($lead->patient_id),
                    'case_description' => Helper::str($lead->case_description),
                    'medical_conditions' => Helper::str($lead->medical_conditions),
                    'medications' => Helper::str($lead->medications),
                    'procedures' => Helper::str($lead->procedures),
                    'special_instructions' => Helper::str($lead->special_instructions),
                    'gender_preference' => Helper::str($lead->gender_preference),
                    'language_preference' => Helper::str($lead->language_preference),
                    'assessment_date' => Helper::str($lead->assessment_date),
                    'assessment_time' => Helper::str($lead->assessment_time),
                    'assessment_notes' => Helper::str($lead->assessment_notes),
                    'payment_mode' => Helper::str($lead->payment_mode),
                    'payment_notes' => Helper::str($lead->payment_notes),
                    'status' => Helper::str($lead->status),
                    'created_at' => Helper::getDate($lead->created_at)
                ];

                // Patients
                if($patientID != $lead->patient_id && isset($lead->patient)){
                    $patientProfilePic = '';
                    $patientProfilePath = 'uploads/provider/'.\Helper::encryptor('encrypt',$lead->tenant_id).'/case_documents/'.\Helper::encryptor('encrypt',$lead->patient->id).'/'.\Helper::encryptor('encrypt',$lead->patient->id).'.jpg';
                    if(file_exists(public_path($patientProfilePath))){
                        $patientProfilePic = url($patientProfilePath);
                    }
                    $patients[] = [
                        'id' => Helper::str($lead->patient->id),
                        'patient_id' => Helper::str($lead->patient->patient_id),
                        'first_name' => Helper::str($lead->patient->first_name),
                        'last_name' => Helper::str($lead->patient->last_name),
                        'gender' => Helper::str($lead->patient->gender),
                        'patient_age' => Helper::str($lead->patient->patient_age),
                        'patient_weight' => Helper::str($lead->patient->patient_weight),
                        'street_address' => Helper::str($lead->patient->street_address),
                        'area' => Helper::str($lead->patient->area),
                        'city' => Helper::str($lead->patient->city),
                        'zipcode' => Helper::str($lead->patient->zipcode),
                        'state' => Helper::str($lead->patient->state),
                        'latitude' => Helper::str($lead->patient->latitude),
                        'longitude' => Helper::str($lead->patient->longitude),
                        'enquirer_name' => Helper::str($lead->patient->enquirer_name),
                        'contact_number' => Helper::str($lead->patient->contact_number),
                        'email' => Helper::str($lead->patient->email),
                        'alternate_number' => Helper::str($lead->patient->alternate_number),
                        'profile_image' => $patientProfilePic
                    ];
                }

                $patientID = $lead->patient_id;

                // Schedules
                if(isset($lead->schedules)){
                    $scheduleList = $lead->schedules;
                    if($userType == 'Caregiver'){
                        $scheduleList = $lead->schedules()->whereScheduleDate(date('Y-m-d'))->whereLeadId($lead->id)->whereCaregiverId($userID)->get();
                    }
                    if($userType == 'Customer'){
                        $scheduleList = $lead->schedules()->whereDate('schedule_date','>=', \Carbon\Carbon::now()->subDays(30))->get();
                    }
                    foreach ($scheduleList as $schedule) {
                        $schedules[] = [
                            'id'               => $schedule->id,
                            'lead_id'          => Helper::str($schedule->lead_id),
                            'patient_id'       => Helper::str($schedule->patient_id),
                            'caregiver_id'     => Helper::str($schedule->caregiver_id),
                            'caregiver_name'   => isset($schedule->caregiver)?Helper::str(trim($schedule->caregiver->full_name)):'',
                            'schedule_date'    => Helper::getDate($schedule->schedule_date, true),
                            'start_time'       => Helper::str($schedule->start_time),
                            'end_time'         => Helper::str($schedule->end_time),
                            'service_required' => Helper::str($schedule->service_required),
                            'service_name'     => $schedule->service->service_name,
                            'notes'            => Helper::str($schedule->notes),
                            'chargeable'       => $schedule->chargeable,
                            'status'           => Helper::str($schedule->status),
                            'assigned_tasks'   => Helper::str($schedule->assigned_tasks),
                            'created_at'       => Helper::getDate($schedule->created_at),
                        ];

                        if($userType == 'Customer'){
                            $cr = $schedule->caregiver;
                            if(!in_array($schedule->caregiver_id,$caregiverIDs) && $cr){
                                array_push($caregiverIDs,$schedule->caregiver_id);
                            }
                        }
                    }
                }

                // Assessments
                if(isset($lead->assessment)){
                    foreach ($lead->assessment as $assessment) {
                        $assessments[] = [
                            'id' => $assessment->id,
                            'lead_id' => Helper::str($assessment->lead_id),
                            'user_id' => Helper::str($assessment->user_id),
                            'type' => Helper::str($assessment->type),
                            'form_type' => Helper::str($assessment->form_type),
                            'form_data' => Helper::str($assessment->form_data),
                            'signature_file' => Helper::str($assessment->signature_file),
                            'created_at' => Helper::getDate($assessment->created_at),
                        ];
                    }
                }

                // FollowUps
                if(isset($lead->followUp)){
                    foreach ($lead->followUp as $followup) {
                        $followups[] = [
                            'id' => $followup->id,
                            'lead_id' => Helper::str($followup->lead_id),
                            'user_id' => Helper::str($followup->user_id),
                            'followup_date' => Helper::getDate($followup->followup_date),
                            'form_data' => Helper::str($followup->form_data),
                            'created_at' => Helper::getDate($followup->created_at),
                        ];
                    }
                }

                // Worklogs
                if(isset($lead->worklog)){
                    foreach ($lead->worklog as $worklog) {
                        $routines = json_decode($worklog->routines,true)[0];
                        $routinesArr = [];
                        $indexRoutine = 0;
                        if(!empty($routines)){
                            foreach ($routines as $key => $routine) {
                                $routinesArr[$indexRoutine]['task'] = $key;
                                $routinesArr[$indexRoutine]['time'] = empty($routine)?array():explode(',',$routine);
                                $indexRoutine++;
                            }
                        }else{
                            $routinesArr = null;
                        }

                        $worklogsMerged[] = [
                            'id' => $worklog->id,
                            'tenant_id' => Helper::str($worklog->tenant_id),
                            'lead_id' => Helper::str($worklog->lead_id),
                            'schedule_id' => Helper::str($worklog->schedule_id),
                            'caregiver_id' => Helper::str($worklog->caregiver_id),
                            'caregiver_name' => isset($worklog->caregiver)?Helper::str($worklog->caregiver->full_name):'',
                            'worklog_date' => Helper::str($worklog->worklog_date),
                            'vitals' => Helper::str($worklog->vitals),
                            'routines' => Helper::str(json_encode($routinesArr)),
                            'created_at' => Helper::getDate($worklog->created_at),
                        ];
                        // Load worklog feedback if any
                        $feedback = WorkLogFeedback::whereWorklogId($worklog->id)->get();
                        if(isset($feedback)){
                            foreach ($feedback as $f) {
                                $feedbacks[] = [
                                    'id' => Helper::str($f->id),
                                    'tenant_id' => $f->tenant_id,
                                    'lead_id' => Helper::str($f->lead_id),
                                    'worklog_id' => Helper::str($f->worklog_id),
                                    'customer_name' => Helper::str($f->customer_name),
                                    'rating' => floatval($f->rating),
                                    'comment' => Helper::str($f->comment),
                                    'created_at' => Helper::getDate($f->created_at),
                                ];
                            }
                        }
                    }
                }

                // Comments
                if(isset($lead->comments)){
                    foreach ($lead->comments as $comment) {
                        $comments[] = [
                            'id' => $comment->id,
                            'lead_id' => Helper::str($comment->lead_id),
                            'user_id' => Helper::str($comment->user_id),
                            'user_type' => Helper::str($comment->user_type),
                            'user_name' => Helper::str($comment->user_name),
                            'comment' => Helper::str($comment->comment),
                            'created_at' => Helper::getDate($comment->created_at),
                        ];
                    }
                }
            }

            if($userType == 'Customer' && count($caregiverIDs)){
                $cres = collect($caregiverIDs)->unique()->sort()->values();
                foreach ($cres as $res) {
                    $cr = Caregiver::find($res);

                    $caregiversList[] = [
                        'id' => $cr->id,
                        'employee_id' => Helper::str($cr->employee_id),
                        'first_name' => Helper::str($cr->first_name),
                        'middle_name' => Helper::str($cr->middle_name),
                        'last_name' => Helper::str($cr->last_name),
                        'date_of_birth' => isset($cr->date_of_birth)?Helper::getDate($cr->date_of_birth):'',
                        'gender' => Helper::str($cr->gender),
                        'blood_group' => Helper::str($cr->blood_group),
                        'marital_status' => Helper::str($cr->marital_status),
                        'mobile_number' => Helper::str($cr->mobile_number),
                        'email' => Helper::str($cr->email),
                        'current_address' => Helper::str($cr->current_address),
                        'current_city' => Helper::str($cr->current_city),
                        'current_state' => Helper::str($cr->current_state),
                        'current_country' => Helper::str($cr->current_country),
                        'profile_image' => isset($cr->profile_image)?asset('uploads/caregivers/'.Helper::str($cr->profile_image)):"",
                        'work_status' => Helper::str($cr->work_status),
                        'specialization' => isset($cr->professional->specialization) && ($cr->professional->specialization !=0)?Helper::str($cr->professional->specializations->specialization_name):'-',
                        'specialization_id' => isset($c->professional)?Helper::str($c->professional->specialization):0,
                        'qualification' => Helper::str($cr->professional->qualification),
                        'experience' => Helper::str($cr->professional->experience),
                        'languages_known' => Helper::str($cr->professional->languages_known),
                        'achievements' => Helper::str($cr->professional->achievements),
                        'college_name' => Helper::str($cr->professional->college_name),
                    ];
                }
            }

            // Masters
            $services = Service::get();
            if(count($services)){
                $servicesMaster = [];
                foreach ($services as $service) {
                    $servicesMaster[] = [
                        'id' => $service->id,
                        'service_name' => $service->service_name,
                    ];
                }
                $results['masters']['services'] = $servicesMaster;
            }

            $referrals = ReferralSource::get();
            if(count($referrals)){
                $referralsMaster = [];
                foreach ($referrals as $referral) {
                    $referralsMaster[] = [
                        'id' => $referral->id,
                        'source_name' => $referral->source_name,
                        'category_id' => $referral->category_id,
                        'category' => isset($referral->category)?$referral->category->category_name:'',
                    ];
                }
                $results['masters']['referrals'] = $referralsMaster;
            }
            
            $tasks = Task::get();
            if(count($tasks)){
                $tasksMaster = [];
                foreach ($tasks as $task) {
                    $tasksMaster[] = [
                        'id' => $task->id,
                        'category' => $task->category,
                        'task_name' => $task->task_name,
                        'description' => $task->description
                    ];
                }
                $results['masters']['tasks'] = $tasksMaster;
            }
            
            $managers = Caregiver::select(\DB::raw('id, CONCAT(COALESCE(first_name,"")," ",COALESCE(last_name,"")," - ",COALESCE(employee_id,"")) as manager_name'))
                            ->whereHas('professional', function($q){
                                $q->whereRole('Manager');
                            })->get()->toArray();
            if(count($managers)){                
                $results['masters']['managers'] = $managers;
            }

            $results['leads'] = $cases;
            $results['patients'] = $patients;
            $results['schedules'] = array_reverse($schedules);
            $results['assessments'] = $assessments;
            $results['followups'] = $followups;
            $results['worklogs'] = $worklogsMerged;
            $results['feedbacks'] = $feedbacks;
            $results['comments'] = $comments;
            if($userType == 'Customer')
                $results['caregivers'] = $caregiversList;
        }

        $data['status_code'] = 200;
        $data['result'] = $results;

        return response()->json($data, 200);
    }

    public function getLeadInvoices(Request $request)
    {
        //$leadID = $request->lead_id;

        //$lead = Lead::find($leadID);
        //$patient_id = $lead->patient_id;

        $invoices = Invoice::wherePatientId($request->patient_id)->get();
        foreach($invoices as $i){  
            $invoice[] = [
                'id'=> $i->id,
                'branch_id'=> $i->branch_id,
                'tenant_id'=> $i->tenant_id,
                'user_id'=> $i->user_id,
                'lead_id'=> $i->lead_id,
                'patient_id'=> $i->patient_id,
                'invoice_no'=> $i->invoice_no,
                'invoice_date'=> $i->invoice_date->format('d-m-Y'),
                'invoice_due_date'=> $i->invoice_due_date->format('d-m-Y'),
                'taxable_amount'=> $i->taxable_amount,
                'less_amount'=> $i->less_amount,
                'taxed_amount'=> $i->taxed_amount,
                'net_amount'=> number_format($i->taxable_amount + $i->taxed_amount,2),
                'total_amount'=> $i->total_amount,
                'amount_paid'=> $i->amount_paid,
                'status'=> $i->status,
            ];
        }
       
        $invoicesItems = InvoiceItems::whereInvoiceId($request->invoice_id)->groupBy("rate")->orderBy('category','asc')->get();
        
        if($request->type == "invoice"){
            return response()->json($invoice, 200);
        }else{
            $items=[];
            foreach($invoicesItems as $i){
                $category  = $i->category;
                $item_id = $i->item_id;
                $invoice_id = $i->invoice_id;
                $items[] = [
                    'invoice_id'=> Helper::str($i->invoice_id),
                    'category'=> Helper::str($i->category),
                    'description'=> Helper::Itemname($item_id,$category,$invoice_id,$type="dec"),
                    'service'=> Helper::Itemname($item_id,$category,$invoice_id,$type="service"),
                    'quantity'=> Helper::Itemname($item_id,$category,$invoice_id,$type="qun"),
                    'type'=> Helper::Itemname($item_id,$category,$invoice_id,$type="type"),
                    'rate'=>$i->rate,
                    'discount_amount'=>$i->discount_amount,
                    'net_amount'=> Helper::Itemname($item_id,$category,$invoice_id,$type="net"),
                    'CGST' => number_format($i->tax_amount/2,2),
                    'SGST' => number_format($i->tax_amount/2,2),
                    'total_amount'=> Helper::Itemname($item_id,$category,$invoice_id,$type="amount"),
                    'date'=>$i->invoice->invoice_date->format('d-m-Y'),
                    'due_date'=>$i->invoice->invoice_due_date->format('d-m-Y')
                ];
            }
        }
       // $items = array_map('unserialize', array_unique(array_map('serialize', $items)));
        return response()->json($items, 200);
    }

    public function updateAssessmentTimings(Request $request)
    {
        $leadID = $request->lead_id;
        $date = $request->assessment_date;
        $time = $request->assessment_time;

        $data['status_code'] = 200;
        if($leadID && $date && $time){
            $lead = Lead::whereId($leadID)->first();
            if($lead){
                $lead->assessment_date = \Carbon\Carbon::parse($date)->format('Y-m-d');
                $lead->assessment_time = \Carbon\Carbon::parse($time)->format('h:i A');
                $lead->save();

                $data['result'] = true;
            }
        }else{
            $data['error'] = true;
        }

        return response()->json($data, 200);
    }

    public function addAssessment(Request $request)
    {
        $assessmentData = $request->input('form');
        $result = false;
        $id = 0;

        if($assessmentData){
            $assessmentData['created_at'] = $assessmentData['updated_at'] = date('Y-m-d H:i:s');
            // $id = $assessmentData['id'];
            // unset($assessmentData['id']);
            $result['id'] = CaseForm::withoutGlobalScopes()->insertGetId($assessmentData);
            if($assessmentData['type'] == 'Detailed'){
                $tenantID = \Helper::encryptor('encrypt',$assessmentData['tenant_id']);
                $leadID = \Helper::encryptor('encrypt',$assessmentData['lead_id']);
                $formID = \Helper::encryptor('encrypt',$result['id']);
                $result['path'] = asset('uploads/provider/'.$tenantID.'/'.'case_documents/'.$leadID.'/'.'signature_'.$formID.'.png');
            }
        }

        $data['status_code'] = 200;
        if($id > 0 && $result)
            $data['result'] = $result;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function addAssessmentSignature(Request $request)
    {
        $tenantID = $request->tenant_id;
        $leadID = $request->lead_id;
        $assessmentID = $request->assessment_id;
        $result = false;
        $id = 0;

        if($tenantID && $leadID && $assessmentID){
            $form = CaseForm::find($assessmentID);
            $tenantID = \Helper::encryptor('encrypt',$tenantID);
            $leadID = \Helper::encryptor('encrypt',$leadID);
            $file = $request->file('assessment_signature');
            if($form && $file->isValid()){
                $filePath = public_path().'/uploads/provider/'.$tenantID.'/'.'case_documents/'.$leadID;
                if(!is_dir($filePath)){
                    \File::makeDirectory($filePath, 0777, true);
                }

                $fileName = 'signature_'.\Helper::encryptor('encrypt',$form->id).'.'.$file->extension();
                $path = $file->move($filePath, $fileName);

                CaseForm::find($assessmentID)
                                ->update(['signature_file' => asset('uploads/provider/'.$tenantID.'/'.'case_documents/'.$leadID.'/'.$fileName)]);

                $result = asset('uploads/provider/'.$tenantID.'/'.'case_documents/'.$leadID.'/'.$fileName);
            }
        }

        $data['status_code'] = 200;
        if($result)
            $data['result'] = $result;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function addFollowup(Request $request)
    {
        $followupData = $request->input('form');
        $result = false;

        if($followupData){
            $followupData['created_at'] = $followupData['updated_at'] = date('Y-m-d H:i:s');
            $result = CaseFollowUp::withoutGlobalScopes()->insert($followupData);
        }

        $data['status_code'] = 200;
        if($result)
            $data['result'] = $result;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function deleteAssessment(Request $request)
    {
        $assessmentID = $request->assessment_id;
        $leadID = $request->lead_id;
        $result = false;

        if($assessmentID && $leadID){
            $form = CaseForm::withoutGlobalScopes()
                                ->whereId($assessmentID)->whereLeadId($leadID)->first();
            if($form)
                $result = $form->forceDelete();
        }

        $data['status_code'] = 200;
        if($result)
            $data['result'] = $result;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function deleteFollowUp(Request $request)
    {
        $followupID = $request->followup_id;
        $leadID = $request->lead_id;
        $result = false;

        if($followupID && $leadID){
            $form = CaseFollowUp::withoutGlobalScopes()
                                ->whereId($followupID)->whereLeadId($leadID)->first();
            if($form)
                $result = $form->forceDelete();
        }

        $data['status_code'] = 200;
        if($result)
            $data['result'] = $result;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function saveIncident(Request $request)
    {
        $incidentID = $request->incident_id;
        $patientID = $request->patient_id;
        $result = false;

        if($followupData){
            $followupData['created_at'] = $followupData['updated_at'] = date('Y-m-d H:i:s');
            $result['id'] = PatientIncident::withoutGlobalScopes()->insertGetId($followupData);
        }

        $data['status_code'] = 200;
        if($result)
            $data['result'] = $result;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function deleteIncident(Request $request)
    {
        $incidentID = $request->incident_id;
        $patientID = $request->patient_id;
        $result = false;

        if($incidentID && $patientID){
            $form = PatientIncident::withoutGlobalScopes()
                    ->whereId($incidentID)->wherePatientId($patientID)->first();
            if($form)
                $result = $form->delete();
        }

        $data['status_code'] = 200;
        if($result)
            $data['result'] = $result;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function updateScheduleCaregiver(Request $request)
    {
        $leadID = $request->lead_id;
        $scheduleID = $request->schedule_id;
        $caregiverID = $request->caregiver_id;

        $data['status_code'] = 200;

        if($leadID && $scheduleID && $caregiverID){
            $schedule = Schedule::whereLeadId($leadID)->whereId($scheduleID)->first();
            if($schedule){
                $schedule->caregiver_id = $caregiverID;
                $schedule->save();

                $data['result'] = true;
            }
        }else{
            $data['error'] = true;
        }

        return response()->json($data, 200);
    }

    public function updateScheduleStatus(Request $request)
    {
        $leadID = $request->lead_id;
        $scheduleID = $request->schedule_id;
        $status = $request->status;

        $data['status_code'] = 200;

        if($leadID && $scheduleID && $status){
            $schedule = Schedule::whereLeadId($leadID)->whereId($scheduleID)->first();
            if($schedule){
                $schedule->status = $status;
                $schedule->save();

                $data['result'] = true;
            }
        }else{
            $data['error'] = true;
        }

        return response()->json($data, 200);
    }

    public function updateProfile(Request $request)
    {
        $id = $request->input('id');
        $type = $request->input('type');
        $profileData = $request->input('profile');
        $res = false;

        if($id && $type){
            if($type == 'Provider'){
                $res = Profile::withoutGlobalScopes()->whereId($id)->update($profileData);
            }

            if($type == 'Caregiver'){
                // Update Basic details
                $res = Caregiver::withoutGlobalScopes()->whereId($id)->update($profileData['basic']);

                // Update account details
                $accountDetails = AccountDetails::withoutGlobalScopes()->whereId($id)->first();
                if($accountDetails){
                    // Update account details entry
                    $res = $accountDetails->update($profileData['account']);
                }
                else{
                    // Create account details entry
                    $profileData['account']['caregiver_id'] = $id;
                    AccountDetails::withoutGlobalScopes()->create($profileData['account']);
                }
            }
        }

        $data['status_code'] = 200;
        if($id && $res)
            $data['result'] = $res;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function getLeaves(Request $request)
    {
        $id = $request->user_id;
        $type = $request->type;        
        // $leaveId = $request->leave_id;
        $leaves = [];
        $leavesResult = [];

        $approval = false;

        if($id && $type){
            switch ($type) {
                case 'myleaves':
                    $leavesResult = Leaves::withoutGlobalScopes()->whereCaregiverId($id)->get();
                    if(count($leavesResult)){
                        foreach ($leavesResult as $l) {
                            $leaves[] = [
                                'id' => Helper::str($l->id),
                                'tenant_id' => Helper::str($l->tenant_id),
                                'approver_id' => $approval?$id:0,
                                'caregiver_id' => Helper::str($l->caregiver_id),
                                'name' => Helper::str($l->name),
                                'date_received' => Helper::getDate($l->date_received, true),
                                'date_from' => Helper::getDate($l->date, true),
                                'date_to' => Helper::getDate($l->date, true),
                                'reason' => Helper::str($l->reason),
                                'type' => Helper::str($l->type),
                                'status' => Helper::str($l->status),
                                'created_at' => Helper::getDate($l->created_at),
                            ];
                        }
                    }
                    break;
                case 'approvals':
                    /*$leavesResult = LeaveApproval::select('leave_id','approver_id','status','comment')
                                    ->with(['approver' => function($q){
                                        $q->addSelect('id','first_name','middle_name','last_name','employee_id');
                                    },'approver' => function($q){
                                        $q->addSelect('id','first_name','middle_name','last_name','employee_id');
                                    }])
                                    ->whereApproverId($id);

                    if(isset($leaveId) && $leaveId > 0){
                        $leavesResult->whereLeaveId($leaveId);
                    }
                    $leavesResult = $leavesResult->get();*/

                    $leavesResult = Leaves::withoutGlobalScopes()->whereApproverId($id)->get();
                    if(count($leavesResult)){
                        foreach ($leavesResult as $l) {
                            $leaves[] = [
                                'id' => Helper::str($l->id),
                                'tenant_id' => Helper::str($l->tenant_id),
                                'approver_id' => $approval?$id:0,
                                'caregiver_id' => Helper::str($l->caregiver_id),
                                'name' => Helper::str($l->name),
                                'date_received' => Helper::getDate($l->date_received, true),
                                'date_from' => Helper::getDate($l->date, true),
                                'date_to' => Helper::getDate($l->date, true),
                                'reason' => Helper::str($l->reason),
                                'type' => Helper::str($l->type),
                                'status' => Helper::str($l->status),
                                'created_at' => Helper::getDate($l->created_at),
                            ];
                        }
                    }
                    break;
            }
        }

        $data['status_code'] = 200;
        if($leaves)
            $data['result']['leaves'] = $leaves;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function updateLeaveRequest(Request $request)
    {
        $userID = $request->user_id; //Caremanager ID
        $leaveID = $request->leave_id; //Leave ID
        //Status of Leave -  Approved / Declined
        if($request->status=='Granted') $status='Approved';
        elseif($request->status=='Not Granted') $status='Declined';

        $data['status_code'] = 200;

        if($userID && $leaveID && $status){
            // Update leave approvals table
            $leaveApproval = Leaves::whereId($leaveID)->whereApproverId($userID)->first();
            if($leaveApproval){
                $leaveApproval->update(['status' => $status]);                
                $data['result'] = true;
            }else{
                $data['error'] = true;
            }
        }
        return response()->json($data, 200);
    }

    public function addLeave(Request $request)
    {
        $leaveData = $request->input('leave');
        $result = false;
        $id = 0;

        if($leaveData){
            $leaveData['created_at'] = date('Y-m-d H:i:s');
            $leaveData['user_id'] = $leaveData['caregiver_id'];
            if($leaveData['type'] == 'Comp-Off'){
                $leaveData['type'] = 'CO';
            } else if($leaveData['type'] == 'Casual Leave'){
                $leaveData['type'] = 'CL';
            } else if($leaveData['type'] == 'Sick Leave'){
                $leaveData['type'] = 'SL';
            } else if($leaveData['type'] == 'Maternity Leave'){
                $leaveData['type'] = 'ML';
            } else if($leaveData['type'] == 'Abscond'){
                $leaveData['type'] = 'AC';
            }

            $interval = new DateInterval('P1D'); // of period 1 day
            $realEnd = new DateTime($leaveData['date_to']); 
            $realEnd->add($interval);
            $datePeriod = new DatePeriod(new DateTime($leaveData['date_from']), $interval, $realEnd);
            unset($leaveData['date_from']);
            unset($leaveData['date_to']);
            
            foreach ($datePeriod as $date) {
                $leaveData['date'] = $date->format('Y-m-d');
                $result = Leaves::insert($leaveData);
            }

            // if($result){
            //     $caregiver = Caregiver::find($leaveData['caregiver_id']);
            //     $approval = [
            //         'tenant_id'   => $leaveData['tenant_id'],
            //         // 'branch_id'   => $leaveData['branch_id'],
            //         'leave_id'    => $result,
            //         'approver_id' => $approverID,
            //         'status'      => 'PENDING'
            //     ];
                // LeaveApproval::create($approval);

                // Send Notification to manager
                // $extraData = [
                //     'id' => $result,
                //     'category' => 'Leave',
                //     'title' => 'Leave Request',
                //     'message' => 'has a leave request.',
                // ];
                // $this->sendNotificationToManager($leaveData['caregiver_id'], $approverID, $extraData);
            // }
        }

        $data['status_code'] = 200;
        if($result)
            $data['result'] = $id;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function saveWorklog(Request $request)
    {
        $worklogData = $request->input('worklog');
        $result = false;
        $id = 0;
        if($worklogData){
            if(isset($worklogData['worklog_date'])){
                $worklog = WorkLog::withoutGlobalScopes()
                            ->whereLeadId($worklogData['lead_id'])
                            ->whereCaregiverId($worklogData['caregiver_id'])
                            ->whereScheduleId($worklogData['schedule_id'])
                            ->whereWorklogDate($worklogData['worklog_date'])->first();

                $routines = $worklogData['routines'];
                $routineArr = [];
                foreach ($routines as $routine) {
                    $routineArr[$routine['task']] = implode(',',$routine['time']);
                }
                $worklogData['routines'] = json_encode(array($routineArr));
                $worklogData['created_at'] = date('Y-m-d H:i:s');
                $worklogData['updated_at'] = date('Y-m-d H:i:s');
                
                if($worklog){
                    unset($worklogData['created_at']);
                    $result = $worklog->update($worklogData);
                }
                else
                    $result = WorkLog::withoutGlobalScopes()->insert($worklogData);

                $lead = Lead::withoutGlobalScopes()->whereId($worklogData['lead_id'])->first();
                $extraData = [
                    'id' => $worklogData['lead_id'],
                    'episode_id' => $lead->episode_id,
                    'category' => 'Task',
                    'title' => 'Caregiver Tasks',
                    'message' => 'has updated the daily tasks and routines.',
                ];
                // Send Push Notification to Manager
                // $this->sendNotificationToManager($worklogData['caregiver_id'], null, $extraData);
            }
        }

        $data['status_code'] = 200;

        if($result)
            $data['result'] = true;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function getPatients(Request $request)
    {
        $patientsList = [];

        if($request->email){
            $patients = Patient::withoutGlobalScopes()->whereEmail($request->email)->get();
            if($patients){
                foreach ($patients as $p) {
                    $patientsList[] = [
                        'id' => Helper::str($p->id),
                        'patient_id' => Helper::str($p->patient_id),
                        'first_name' => Helper::str($p->first_name),
                        'last_name' => Helper::str($p->last_name),
                        'contact_number' => Helper::str($p->contact_number),
                        'email' => Helper::str($p->email),
                        'gender' => Helper::str($p->gender),
                        'patient_age' => Helper::str($p->patient_age),
                        'patient_weight' => Helper::str($p->patient_weight),
                        'street_address' => Helper::str($p->street_address),
                        'area' => Helper::str($p->area),
                        'city' => Helper::str($p->city),
                        'zipcode' => Helper::str($p->zipcode),
                        'state' => Helper::str($p->state),
                        'country' => Helper::str($p->country),
                        'latitude' => Helper::str($p->latitude),
                        'longitude' => Helper::str($p->longitude),
                        'relationship_with_patient' => Helper::str($p->relationship_with_patient),
                    ];
                }
            }
        }

        $data['status_code'] = 200;
        if($patientsList)
            $data['result'] = $patientsList;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function getAttendance(Request $request)
    {
        $id = $request->tenant_id;
        $managerID = $request->manager_id;
        $date = date('Y-m-d');

        if($id && $managerID){
            $scheduledCaregivers = collect(
                \DB::table('caregivers')
                ->where('caregivers.tenant_id',$id)
                ->join('caregiver_professional_details',function ($q) use($managerID){
                    $q->on('caregivers.id','=','caregiver_professional_details.caregiver_id')
                    ->where('caregiver_professional_details.manager',$managerID);
                })
                ->join('case_schedules',function ($join) use ($date){
                    $join->on('caregivers.id', '=','case_schedules.caregiver_id')
                    ->where('case_schedules.status','!=','Cancelled')
                    ->whereScheduleDate($date);
                })
                ->join('patients','case_schedules.patient_id', '=', 'patients.id')
                ->leftJoin('employee_attendance',function ($join) use ($date){
                    $join->on('caregivers.id', '=','employee_attendance.caregiver_id')
                    ->on('employee_attendance.schedule_id','=','case_schedules.id')
                    ->where('employee_attendance.date','=',$date)
                    ->whereNull('employee_attendance.deleted_at');
                })
                ->leftJoin('master_services',function ($join){
                    $join->on('case_schedules.service_required', '=','master_services.id');
                })
                ->select(
                    'caregivers.id as caregiverId',
                    'caregivers.employee_id as caregiverCode',
                    'caregivers.first_name as caregiverFname',
                    'caregivers.middle_name as caregiverMname',
                    'caregivers.last_name as caregiverLname',
                    'case_schedules.id as scheduleId',
                    'case_schedules.schedule_date as scheduleDate',
                    'case_schedules.start_time as sft',
                    'case_schedules.end_time as stt',
                    'master_services.service_name as serviceName',
                    'patients.id as patientId',
                    'patients.first_name as patientFname',
                    'patients.last_name as patientLname',
                    'employee_attendance.punch_in as eIn',
                    'employee_attendance.punch_out as eOut',
                    'employee_attendance.status as attStatus'
                )
                ->get()
            )->toArray();

            $excludedIds = array_pluck($scheduledCaregivers,'caregiverId');

            $nonScheduledCaregivers = collect(
                \DB::table('caregivers')
                ->where('caregivers.tenant_id',$id)
                ->join('users',function ($q){
                    $q->on('caregivers.id','=','users.user_id')
                    ->whereStatus(1)->where('caregivers.work_status','!=','Training');
                })
                ->whereNotIn('caregivers.id',$excludedIds)
                ->distinct('caregivers.id')
                ->join('caregiver_professional_details',function($q) use ($date, $managerID){
                    $q->on('caregivers.id', '=','caregiver_professional_details.caregiver_id')
                    ->where('caregiver_professional_details.manager',$managerID)
                    ->where('caregiver_professional_details.role','=','Caregiver')
                    ->where(function($r) use ($date){
                        $r->whereDate('caregiver_professional_details.date_of_joining','<=',$date)
                        ->orWhereNull('caregiver_professional_details.date_of_joining');
                    })
                    ->where(function($r) use ($date){
                        $r->whereDate('caregiver_professional_details.resignation_date','>=',$date)
                        ->orWhereNull('caregiver_professional_details.resignation_date');
                    });
                })
                ->leftJoin('employee_attendance',function ($join) use ($date){
                    $join->on('caregivers.id', '=','employee_attendance.caregiver_id')
                    ->where('employee_attendance.date','=',$date)
                    ->whereNull('employee_attendance.deleted_at');
                })
                ->leftJoin('employee_leaves',function ($join) use ($date){
                    $join->on('caregivers.id', '=','employee_leaves.caregiver_id')
                    ->where('employee_leaves.date','=',$date)
                    ->where('employee_leaves.status','!=','Declined')
                    ->whereNull('employee_leaves.deleted_at');
                })
                ->select(
                    'caregivers.id as caregiverId',
                    'caregivers.employee_id as caregiverCode',
                    'caregivers.first_name as caregiverFname',
                    'caregivers.middle_name as caregiverMname',
                    'caregivers.last_name as caregiverLname',
                    'employee_attendance.status as attStatus',
                    'employee_leaves.type as leaveType',
                    'employee_leaves.status as leaveStatus'
                )
                ->get()
            )->toArray();
            
            $nonScheduledTrainees = collect(
                \DB::table('caregivers')
                ->where('caregivers.tenant_id',$id)
                ->join('users',function ($q){
                    $q->on('caregivers.id','=','users.user_id')
                    ->whereStatus(1);
                })
                ->where('caregivers.work_status','=','Training')
                ->whereNotIn('caregivers.id',$excludedIds)
                ->distinct('caregivers.id')
                ->join('caregiver_professional_details',function ($q) use ($managerID){
                    $q->on('caregivers.id','=','caregiver_professional_details.caregiver_id')
                    ->where('caregiver_professional_details.manager',$managerID);
                })
                ->leftJoin('employee_attendance',function ($join) use ($date){
                    $join->on('caregivers.id', '=','employee_attendance.caregiver_id')
                    ->where('employee_attendance.date','=',$date)
                    ->whereNull('employee_attendance.deleted_at');
                })
                ->leftJoin('employee_leaves',function ($join) use ($date){
                    $join->on('caregivers.id', '=','employee_leaves.caregiver_id')
                    ->where('employee_leaves.date','=',$date)
                    ->where('employee_leaves.status','!=','Declined')
                    ->whereNull('employee_leaves.deleted_at');
                })
                ->select(
                    'caregivers.id as caregiverId',
                    'caregivers.employee_id as caregiverCode',
                    'caregivers.first_name as caregiverFname',
                    'caregivers.middle_name as caregiverMname',
                    'caregivers.last_name as caregiverLname',
                    'employee_attendance.status as attStatus',
                    'employee_leaves.type as leaveType',
                    'employee_leaves.status as leaveStatus'
                )
                ->get()
            )->toArray();
        }

        $data['status_code'] = 200;
        if($scheduledCaregivers || $nonScheduledCaregivers || $nonScheduledTrainees){
            $data['result']['scheduledCaregivers'] = $scheduledCaregivers;
            $data['result']['nonScheduledCaregivers'] = $nonScheduledCaregivers;
            $data['result']['nonScheduledTrainees'] = $nonScheduledTrainees;
            $data['result']['date'] = Carbon::parse($date)->format('d-m-Y');
        }else{
            $data['error'] = true;
        }

        return response()->json($data, 200);
    }

    public function markDailyAttendance(Request $request)
    {
        $date = date('Y-m-d');
        $manager_user_id = User::whereUserId($request->manager_id)->pluck('id')->first();
        if(isset($request->shift)){
            // attendance for schedules staff
            $att_data = [
                'tenant_id' => (int) $request->tenant_id,
                'caregiver_id' => (int) $request->cid,
                'schedule_id' => (int) $request->sid,
                'date' => $date,
                'punch_in' => Carbon::parse($request->intime)->format('h:i A'),
                'punch_out' => Carbon::parse($request->outtime)->format('h:i A'),
                'source' => 'App',
                'marked_by' => (int) $manager_user_id,
                'status' => 'P',
                'on_training' => 'No'
            ];
            $att = Attendance::whereDate('date',$date)->whereCaregiverId($request->cid)->whereScheduleId($request->sid)->first();
            if($att){
                $att->update($att_data);
            }else{
                Attendance::create($att_data);
            }

            $attBench = Attendance::whereDate('date',$date)->whereCaregiverId($request->cid)->whereNull('schedule_id')->get();
            if(count($attBench)){
                foreach ($attBench as $ab) {
                    $ab->delete();
                }
            }

            if(isset($request->sid)){
                $schedule = Schedule::whereId($request->sid)->first();
                if($schedule->status != 'Completed'){
                    $schedule->update(['status' => 'Completed']);

                    $drs = DailyRevenueStorage::whereDate('date',$schedule->schedule_date)->first();

                    if($drs){
                       $drs->flag = 'Unsorted';
                       $drs->updated_at = date('Y-m-d H:i:s');

                       $drs->save();
                    }
                }
            }
            $leave = Leaves::whereDate('date',$date)->whereCaregiverId($request->cid)->first();
            if($leave){
                $leave->update(['status' => 'Declined']);
            }
            return response()->json(['error'=>false, 'result'=>'Attendance marked against the schedule'], 200);
        } else if(isset($request->training)){
            // attendance for trainees
            $att_data = [
                'tenant_id' => (int) $request->tenant_id,
                'caregiver_id' => (int) $request->cid,
                'date' => $date,
                'source' => 'App',
                'marked_by' => (int) $manager_user_id,
                'status' => $request->status,
                'on_training' => 'Yes'
            ];
            $att = Attendance::where('date','=',$date)->whereCaregiverId($request->cid)->first();
            if($att){
                $att->update($att_data);
            }else{
                Attendance::create($att_data);
            }
            return response()->json(['error'=>false, 'result'=>'Attendance marked for the trainee'], 200);
        } else {
            // attendance for Non-scheduled staff & Managers
            $att_data = [
                'tenant_id' => (int) $request->tenant_id,
                'caregiver_id' => (int) $request->cid,
                'date' => $date,
                'source' => 'App',
                'marked_by' => (int) $manager_user_id,
                'status' => $request->status,
                'on_training' => 'No'
            ];
            $att = Attendance::where('date','=',$date)->whereCaregiverId($request->cid)->first();
            if($att){
                $att->update($att_data);
            }else{
                Attendance::create($att_data);
            }
            return response()->json(['error'=>false, 'result'=>'Attendance marked for non schedule staff'], 200);
        }
        return response()->json(['error' => true, 'result' => 'Unable to mark the attendance'], 401);
    }

    /*---------------------------------------*/
    /* Customer API Calls - START */
    /*---------------------------------------*/
    public function updateRegistrationTokenFamily(Request $request)
    {
        $customerID = $request->customer_id;
        $token = $request->token;

        $res = false;

        if($customerID && $token){
            // Check if entry exists
            $fcmToken = FCMTokenFamily::whereCustomerId($customerID)->first();

            if($fcmToken){
                $fcmToken->token = $token;
                $res = $fcmToken->save();
            }else{
                $res = FCMTokenFamily::create(['customer_id' => $customerID, 'token' => $token]);
            }
        }

        $data['status_code'] = 200;
        if($res)
            $data['result'] = (bool) $res;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function newCustomerRequest(Request $request)
    {
        $result = false;
        $id = 0;

        $requestData = [];
        $requestData['name'] = $request->full_name;
        $requestData['email'] = $request->email;
        $requestData['requirement'] = $request->requirement;
        $requestData['phone_number'] = $request->mobile_number;
        $requestData['customer_id'] = $request->customer_id;
        $requestData['source'] = 'Customer App';
        $requestData['status'] = 'Not Checked';

        $requestData['created_at'] = date('Y-m-d H:i:s');
        $requestData['updated_at'] = date('Y-m-d H:i:s');

        $result = LeadRequests::insert($requestData);

        $data['status_code'] = 200;
        if($result)
            $data['result'] = $result;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function getCustomerRequest(Request $request)
    {
        $result = [];
        $id = 0;

        $leadRequests = LeadRequests::whereCustomerId($request->customer_id)->whereSource('Customer App')->get();

        if(count($leadRequests)){
            foreach ($leadRequests as $lr) {
                $result[] = [
                    'requirement' => $lr->requirement,
                    'status' => $lr->status,
                    'created_at' => $lr->created_at->format('d-m-Y')
                ];
            } 
        }

        $data['status_code'] = 200;
        if($result)
            $data['result'] = $result;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function getPatientCaregivers(Request $request)
    {
        $result = [];
        $id = 0;

        $customerLinks = CustomerLinks::whereCustomerId($request->customer_id)->get();

        if(count($customerLinks)){
            foreach ($customerLinks as $cl) {
                $dbName = \Helper::encryptor('decrypt',$cl->org_hash);
                \Config::set('database.connections.tenant.database',$dbName);
                \DB::setDefaultConnection('tenant');

                $caregivers = Schedule::whereDate('schedule_date',date('Y-m-d'))->wherePatientId($cl->patient_id)
                                        ->where('caregiver_id','!=',0)->distinct('caregiver_id')
                                        ->pluck('caregiver_id')->toArray();

                if(isset($caregivers)){
                    foreach ($caregivers as $caregiver) {
                        $c = Caregiver::find($caregiver);
                        if($c->professional->specialization != 0){
                            $specialization = Specialization::find($c->professional->specialization)->value('specialization_name');
                        }else{
                            $specialization = 'NA';
                        }

                        if(isset($c->professional->manager) && $c->professional->manager != 0){
                            $manager = Caregiver::find($c->professional->manager);
                            $managerID = $manager->employee_id ?? 'NA';
                            $managerName = $manager->full_name;
                            $managerNumber = $manager->mobile_number;
                        }else{
                            $managerID     = null;
                            $managerName   = null;
                            $managerNumber = null;
                        }

                        if($c->profile_image && file_exists(public_path('uploads/provider/'.\Helper::encryptor('encrypt',$c->tenant_id).'/caregivers/'.\Helper::encryptor('encrypt',$c->id)).'/'.$c->profile_image) ){
                            $cImg = asset('uploads/provider/'.\Helper::encryptor('encrypt',$c->tenant_id).'/caregivers/'.\Helper::encryptor('encrypt',$c->id).'/'.$c->profile_image);
                        }else{
                            $cImg = asset('img/default-avatar.jpg');
                        }

                        $result[] = [
                            'caregiver_id'      => $c->id,
                            'tenant_id'         => $c->tenant_id,
                            'name'              => $c->full_name,
                            'achievements'      => $c->professional->achievements,
                            'blood_group'       => $c->blood_group,
                            'college_name'      => $c->professional->college_name,
                            'current_address'   => $c->current_address,
                            'current_city'      => $c->current_city,
                            'current_state'     => $c->current_state,
                            'current_country'   => $c->current_country,
                            'date_of_birth'     => isset($c->date_of_birth)?$c->date_of_birth->format('d-m-Y'):null,
                            'age'               => isset($c->date_of_birth)?\Carbon\Carbon::parse($c->date_of_birth)->age:null,
                            'email'             => $c->email,
                            'employee_id'       => $c->employee_id,
                            'experience'        => $c->professional->experience,
                            'gender'            => $c->gender,
                            'languages_known'   => $c->professional->languages_known,
                            'marital_status'    => $c->marital_status,
                            'mobile_number'     => $c->mobile_number,
                            'profile_image'     => $cImg,
                            'qualification'     => $c->professional->qualification,
                            'specialization'    => $specialization,
                            'specialization_id' => $c->professional->specialization,
                            'work_status'       => $c->work_status,
                            'org_hash'          => $cl->org_hash,
                            'manager_id'        => $managerID,
                            'manager_name'      => $managerName,
                            'manager_number'    => $managerNumber
                        ];
                    }
                }

            } 
        }

        $data['status_code'] = 200;
        $data['result'] = $result;

        return response()->json($data, 200);
    }

    public function getCustomerProfile(Request $request)
    {
        $email = $request->email;
        $profile = [];

        if($email){
            $res = Patient::withoutGlobalScopes()->whereEmail($email)->first();
            if($res){
                $profile = [
                    'name' => $res->fullname,
                    'phone' => $res->contact_number,
                    'email' => $res->email,
                    'date_of_birth' => $res->date_of_birth,
                    'gender' => $res->gender,
                    'age' => $res->age,
                    'weight' => $res->weight,
                    'street_address' => $res->street_address,
                    'area' => $res->area,
                    'city' => $res->city,
                    'zipcode' => $res->zipcode,
                    'state' => $res->state,
                    'latitude' => $res->latitude,
                    'longitude' => $res->longitude,
                ];
            }
        }

        $data['status_code'] = 200;
        if($profile)
            $data['result'] = $profile;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function updatePatient(Request $request)
    {
        $patientData = $request->input('patient');
        $result = false;
        $id = 0;

        if($patientData){
            $patientData['updated_at'] = date('Y-m-d H:i:s');

            $id = $patientData['id'];
            unset($patientData['id']);
            $result = Patient::withoutGlobalScopes()->whereId($id)->update($patientData);
        }

        $data['status_code'] = 200;
        if($id > 0 && $result)
            $data['result'] = $id;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function worklogFeedback(Request $request)
    {
        $worklogID     = $request->worklog_id;
        $leadID        = $request->lead_id;
        $customerID    = $request->customer_id;
        $overallRating = $request->over_all_rating;
        $orgHash       = $request->org_hash;
        $id = 0;

        $dbName = \Helper::encryptor('decrypt',$orgHash);
        \Config::set('database.connections.tenant.database',$dbName);
        \DB::setDefaultConnection('tenant');

        if($worklogID && $leadID && $customerID && $overallRating){
            // Get tenant_id from worklog table
            $worklog = WorkLog::withoutGlobalScopes()->whereId($worklogID)->first();
            if($worklog){
                // Update Worklog Feedback
                $data = [
                     'tenant_id'                  => $worklog->tenant_id,
                     'lead_id'                    => $leadID,
                     'worklog_id'                 => $worklogID,
                     'patient_id'                 => $request->patient_id,
                     'customer_id'                => $customerID,
                     'caring_and_patience'        => $request->caring_and_patience,
                     'knowledge'                  => $request->knowledge,
                     'cleanliness'                => $request->cleanliness,
                     'reliabilty_and_punctuality' => $request->reliabilty_and_punctuality,
                     'communication_with_patient' => $request->communication_with_patient,
                     'over_all_rating'            => $overallRating,
                     'additional_comments'        => $request->additional_comments,
                     'created_at'                 => date('Y-m-d H:i:s'),
                     'updated_at'                 => date('Y-m-d H:i:s')
                ];
                $feedback = WorkLogFeedback::whereWorklogId($worklogID)->first();
                if(!$feedback){
                    $id = WorkLogFeedback::insertGetId($data);
                }else{
                    $result['status_code'] = 207;
                    $result['result'] = 'Feedback Already Submitted for the Schedule.';
                    return response()->json($result, 200); 
                }
            }
        }

        $result['status_code'] = 200;
        if($id > 0){
            $result['result'] = 'Feedback Submitted Successfully';
        }else{
            $result['error'] = true;
            $result['data'] = $worklogID.','.$leadID.','.$customerID.','.$overallRating;
        }

        return response()->json($result, 200);
    }

    public function getCustomerBills(Request $request)
    {
        $email = $request->input('email');
        $invoices = [];

        if($email){
            $patients = Patient::whereEnquirerEmail($email)
                                ->whereHas('careplan', function($q){
                                    $q->withoutGlobalScopes();
                                })->get();

            if($patients){
                foreach($patients as $p){
                    if(isset($p->careplan)){
                        foreach ($p->careplan as $c) {
                            if(isset($c->invoices)){
                                foreach ($c->invoices as $i) {
                                    $invoices[] = [
					'id' => $i->id,
                                        'careplan_id' => Helper::str($i->careplan_id),
                                        'invoice_no' => Helper::str($i->invoice_no),
                                        'invoice_period_from' => Helper::getDate($i->invoice_period_from),
                                        'invoice_period_to' => Helper::getDate($i->invoice_period_to),
                                        'invoice_date' => Helper::getDate($i->invoice_date),
                                        'case_charges' => Helper::str($i->case_charges),
                                        'invoice_amount' => Helper::str($i->invoice_amount),
                                        'status' => Helper::str($i->status),
                                    ];
                                }
                            }
                        }
                    }
                }
            }
        }

        $data['status_code'] = 200;
        if($invoices)
            $data['result']['bills'] = $invoices;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    function getAllDocuments(Request $request)
    {
        $results = [];
        $caregiverID = $request->caregiver_id;

        $dbName = \Helper::encryptor('decrypt',$request->org_hash);
        \Config::set('database.connections.tenant.database',$dbName);
        \DB::setDefaultConnection('tenant');

        if($caregiverID){
            $docs = Verification::withoutGlobalScopes()->whereCaregiverId($caregiverID)->get();
            if($docs){
                foreach ($docs as $d) {
                    $path     = asset("uploads/provider/".\Helper::encryptor('encrypt',$d->tenant_id)."/caregivers/".\Helper::encryptor('encrypt',$d->caregiver_id)."/mobile");
                    $fileName = substr($d->doc_path, 0, strrpos($d->doc_path, "."));
                    $results[] = [
                        'id'            => $d->id,
                        'caregiver_id'  => $d->caregiver_id,
                        'document_name' => $d->doc_name,
                        'document_path' => $path."/".$fileName.".jpg",
                    ];
                }
            }
        }

        $data['status_code'] = 200;
        if($results)
            $data['result']['documents'] = $results;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    function getInvoice(Request $request)
    {
        $invoiceID = $request->invoice_id;

        if($invoiceID){
            $invoice = Invoice::withoutGlobalScopes()->whereId($invoiceID)->first();
            if($invoice){
                $data['profile'] = Profile::withoutGlobalScopes()->whereTenantId($invoice->tenant_id)->first();
                $data['billset'] = Settings::withoutGlobalScopes()->whereTenantId($invoice->tenant_id)->first();
                $data['invoice'] = $invoice;

                $pdf = \PDF::loadView('billings.invoice-pdf', $data);

                //return view('billings.invoice-pdf', $data);
                return $pdf->setPaper('a4', 'portrait')->setWarnings(false)->stream('invoice.pdf');
            }
        }
    }

    public function customerLink(Request $request)
    {
        if($request->org_code && $request->patient_id && $request->customer_id && $request->contact_number){
            // Get database name from subscriber ID
            $subscriber = Subscriber::active()->whereOrgCode($request->org_code)->first();
            if($subscriber){
                // Fetch database_name field
                $dbName = $subscriber->database_name;
                \Config::set('database.connections.tenant.database',$dbName);
                // Set the tenant as default connection
                \DB::setDefaultConnection('tenant');
                $patient = Patient::wherePatientId($request->patient_id)->where('contact_number','like','%'.$request->contact_number.'%')->get();

                if(count($patient) > 1){
                    return response()->json(['error' => true, 'result' => 'Multiple Records found'], 207);
                }else if(count($patient) == 0){
                    return response()->json(['error' => true, 'result' => 'No Patient found'], 207);
                }else{
                    $patient = $patient->first();
                    $checkLink = CustomerLinks::whereCustomerId($request->customer_id)->wherePatientId($patient->id)->first();
                    if($checkLink){
                        return response()->json(['error' => true, 'result' => 'Patient Already Linked'], 401);
                    }else{
                        $cl = new CustomerLinks;
                        $cl->tenant_id = Profile::value('tenant_id');
                        $cl->patient_id = $patient->id;
                        $cl->org_hash = \Helper::encryptor('encrypt',$subscriber->database_name);
                        $cl->customer_id =  $request->customer_id;

                        $cl->save();

                        return response()->json(['error' => false, 'result' => 'Patient Linked Successfully'], 200);
                    }
                }
            }
            return response()->json(['error' => true,'result' => 'Organization not Found'], 401);
        }

        return response()->json(['error' => true,'result' => 'Incomplete Inputs'], 401);
    }

    public function customerProfileUpload(Request $request)
    {
        $customerId = $request->customer_id;
        $profileImage = $request->profile_img;

        if($customerId && $profileImage){
            try {
                $profileImage = str_replace('data:image/png;base64,', '', $profileImage);
                $profileImage = str_replace(' ', '+', $profileImage);
                $path = public_path('uploads/customer_registrations/'.\Helper::encryptor('encrypt',$customerId).'/'.$request->lead_id);
                if(!is_dir($path)){ \File::makeDirectory($path, 0777, true); }
                $imageName = \Helper::encryptor('encrypt',$customerId).'.jpg';
                $path = $path.'/'.$imageName;
                \Image::make(base64_decode($profileImage))->resize(300,300)->save($path, 80);
            } catch (ImageException $e) {
                return response()->json(['error' => true, 'result' => 'Unable to process the image'], 401);
            }

            CustomerRegistrations::whereId($customerId)->update(['profile_image'=>$imageName]);
            return response()->json(['error' => false, 'result' => 'Profile image updated successfully'], 200);
        } else {
            return response()->json(['error' => true, 'result' => 'Unable to update profile image'], 401);
        }
    }
    
    /* Customer API Calls - END */


    /*---------------------------------------*/
    /* Push Notification API Calls - START   */
    /*---------------------------------------*/
    public function sendNotificationToCaregiver($caregiverID)
    {
        if($caregiverID){
            $user = User::withoutGlobalScopes()->whereNull('deleted_at')->whereUserId($caregiverID)->first();
            if($user){
                $token = FCMToken::withoutGlobalScopes()->whereUserId($user->id)
                            ->whereUserType('Caregiver')
                            ->orderBy('created_at','Desc')
                            ->orderBy('updated_at','Desc')
                            ->first();

                if($token){
                    $pdata = [
                        'type' => 'notification',
                        'message' => 'You have been assigned a new schedule',
                        'category' => 'Schedule',
                    ];

                    $response = \App\Helpers\PushMessaging::send($token->token, 'New Case Assigned','You have been assigned a new schedule.',$pdata);
                }
            }
        }
    }

    public function sendNotificationToManager($caregiverID, $managerID=null, $extra)
    {
        $managerIDs = [];
        if($caregiverID && $managerID == null){
            $user = User::withoutGlobalScopes()->whereNull('deleted_at')->whereUserId($caregiverID)->first();
            if($user){
                $caregiverName = trim($user->full_name);
                $managerRes = Caregiver::withoutGlobalScopes()->whereId($user->user_id)->first();
                if($managerRes){
                    $managerID = User::withoutGlobalScopes()->whereUserId($managerRes->professional->manager)->value('id');
                    $managerIDs[] = $managerID;

                    // Old Logic
                    // $managerIDs = User::withoutGlobalScopes()->whereNull('deleted_at')
                    //                 ->whereTenantId($user->tenant_id)
                    //                 ->whereUserType('Manager')
                    //                 ->pluck('id')->toArray();
                }                
            }
        }
        
        if($managerID != null){
            $id = User::withoutGlobalScopes()->whereUserId($managerID)->value('id');
            if($id){
                $caregiverName = Caregiver::find($caregiverID)->full_name;
                $managerIDs[] = $id;
            }
        }
        
        if(count($managerIDs)){
            $tokens = FCMToken::withoutGlobalScopes()->whereIn('user_id',$managerIDs)
                            ->whereUserType('Manager')->pluck('token')->toArray();
            if($tokens){
                $pdata = [
                    'type' => 'notification',
                    'message' => $caregiverName.' '.$extra['message'],
                    'category' => $extra['category'],
                    'id' => isset($extra['id'])?$extra['id']:"",
                    'episode_id' => isset($extra['episode_id'])?$extra['episode_id']:""
                ];

                $response = \App\Helpers\PushMessaging::send($tokens, $extra['title'],$caregiverName.' '.$extra['message'],$pdata);
            }
        }
    }
    /* Push Notification API Calls - END */


    /*---------------------------------------*/
    /* Tally API Calls - START   */
    /*---------------------------------------*/
    public function getAllPatients()
    {
        $patientsList = [];
        $patients = Patient::withoutGlobalScopes()->get();
        if($patients){
            foreach ($patients as $p){
                $patientsList[] = [
                    'CUSTOMER_NAME' => Helper::str($p->first_name).' '.Helper::str($p->last_name),
                    'CUSTOMER_CODE' => Helper::str($p->patient_id),
                    'GROUP' => null,
                    'ADDRESS_1' => Helper::str($p->street_address),
                    'ADDRESS_2' => Helper::str($p->area),
                    'ADDRESS_3' => Helper::str($p->city),
                    'ADDRESS_4' => Helper::str($p->zipcode),
                    'STATE' => Helper::str($p->state),
                    'EMAIL_ID' => Helper::str($p->email),
                    'MOBILE_NUMBER' => Helper::str($p->contact_number),
                    'GST_NUMBER' => null
                ];
            }
        }
        if($patientsList){
            $data['status'] = 'success';
            $data['result'] = $patientsList;
        }else{
            $data['status'] = 'failed';
        }
        return response()->json($data, 200);
    }

    public function getPatientByPatientId(Request $request)
    {
        $patientsList = [];
        if($request->patient_id){
            $patients = Patient::wherePatientId($request->patient_id)->get();
            if($patients){
                foreach ($patients as $p){
                    $patientsList[] = [
                        'CUSTOMER_NAME' => Helper::str($p->first_name).' '.Helper::str($p->last_name),
                        'CUSTOMER_CODE' => Helper::str($p->patient_id),
                        'GROUP' => null,
                        'ADDRESS_1' => Helper::str($p->street_address),
                        'ADDRESS_2' => Helper::str($p->area),
                        'ADDRESS_3' => Helper::str($p->city),
                        'ADDRESS_4' => Helper::str($p->zipcode),
                        'STATE' => Helper::str($p->state),
                        'EMAIL_ID' => Helper::str($p->email),
                        'MOBILE_NUMBER' => Helper::str($p->contact_number),
                        'GST_NUMBER' => null
                    ];
                }
            }
        }
        if($patientsList){
            $data['status'] = 'success';
            $data['result'] = $patientsList;
        }else{
            $data['status'] = 'failed';
        }
        return response()->json($data, 200);
    }

    /*---------------------------------------*/
    /* Tally API Calls - END   */
    /*---------------------------------------*/


    /*---------------------------------------*/
    /* Aajicare API Calls - START   */
    /*---------------------------------------*/
    public function saveLeadFromAajicareWebsite(Request $request)
    {
        $tenantId = 269;
        $branchId = '1';
        $userId = 0;
        $patientID = null;
        $lead = null;
        $caseDisposition = null;

        if($request->contact_number != '' || $request->email != ''){
            $patientDetails['branch_id'] = $branchId;
            $patientDetails['tenant_id'] = $tenantId;
            $patientDetails['first_name'] = empty($request->patient_name)?'Name unknown':$request->patient_name;
            $patientDetails['last_name'] = '';
            $patientDetails['contact_number'] = empty($request->contact_number)?'1234567890':$request->contact_number;
            if (isset($patientDetails['contact_number'])) {
                $patientDetails['contact_number'] = preg_replace('/\s+/','',$patientDetails['contact_number']);
            }
            $patientDetails['gender'] = 'Other';
            $patientDetails['date_of_birth'] = null;
            $patientDetails['patient_age'] = $patientDetails['patient_weight'] = $patientDetails['street_address'] = $patientDetails['area'] = $patientDetails['city'] = $patientDetails['zipcode'] = $patientDetails['state'] = $patientDetails['country'] = $patientDetails['latitude'] = $patientDetails['longitude'] = $patientDetails['enquirer_name'] = $patientDetails['alternate_number'] = $patientDetails['relationship_with_patient'] = '';
            $patientDetails['email'] = empty($request->email)?'':$request->email;
            $patientDetails['created_at'] = $patientDetails['updated_at'] = date('Y-m-d H:i:s');
            $patientID = Patient::insertGetId($patientDetails);
        }

        if($patientID){
            $caseDetails['tenant_id'] = $tenantId;
            $caseDetails['branch_id'] = $branchId;
            $caseDetails['patient_id'] = $patientID;
            $caseDetails['case_description'] = $caseDetails['case_type'] = $caseDetails['medical_conditions'] = $caseDetails['medications'] = $caseDetails['procedures'] = $caseDetails['hospital_name'] = $caseDetails['primary_doctor_name'] = $caseDetails['special_instructions'] = $caseDetails['assessment_date'] = $caseDetails['assessment_time'] = $caseDetails['assessment_notes'] = $caseDetails['rate_agreed'] = $caseDetails['payment_mode'] = $caseDetails['payment_notes'] = $caseDetails['referral_category'] = $caseDetails['referrer_name'] = $caseDetails['converted_by'] = $caseDetails['manager_id'] = $caseDetails['referral_type'] = '';
            $caseDetails['service_category'] = $caseDetails['service_required'] = $caseDetails['estimated_duration'] = $caseDetails['registration_amount'] = 0;
            $caseDetails['gender_preference'] = 'Any';
            $caseDetails['referral_value'] = '0';
            $caseDetails['remarks'] = 'This lead is from aajicare.in contact us page';
            $caseDetails['status'] = 'Pending';
            $caseDetails['created_at'] = $caseDetails['updated_at'] = date('Y-m-d H:i:s');
            $lead = Lead::create($caseDetails);
            $leadID = $lead->id;
        }

        if($lead){
            $dispoData['tenant_id'] = $tenantId;
            $dispoData['lead_id'] = $leadID;
            $dispoData['user_id'] = $userId;
            $dispoData['status'] = 'Pending';
            $dispoData['disposition_date'] = date('Y-m-d H:i:s');
            $caseDisposition = CaseDisposition::firstOrCreate($dispoData);
        }

        if($caseDisposition){
            $data['status'] = 'success';
        }else{
            $data['status'] = 'failed';
        }
        return response()->json($data, 200);
    }
    /*---------------------------------------*/
    /* Aajicare API Calls - ENDS   */
    /*---------------------------------------*/
}