<?php
namespace App\Http\Controllers\Corporates;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Entities\Caregiver\Caregiver;
use App\Entities\Caregiver\CaregiverSkill;
use App\Entities\Caregiver\ProfessionalDetails;
use App\Entities\Caregiver\AccountDetails;
use App\Entities\Caregiver\PayrollDetails;
use App\Entities\Caregiver\AccommodationDetails;
use App\Entities\Caregiver\Verification;
use App\Entities\Caregiver\WorkLogFeedback;

use App\Entities\Console\Subscriber;

use App\Entities\Corporates\CorpUsers;
use App\Entities\Corporates\CaregiverLog;

use App\Entities\Masters\Department;
use App\Entities\Masters\Designation;
use App\Entities\Masters\Language;
use App\Entities\Masters\Skill;
use App\Entities\Masters\Branch;
use App\Entities\Masters\Specialization;
use App\Entities\Masters\Agency;

use App\Entities\Profile;
use App\Entities\Leaves;
use App\Entities\Attendance;
use App\Entities\Schedule;
use App\Entities\Settings;
use App\Entities\Patient;
use App\Entities\User;
use App\Entities\Role;

use App\Helpers\Helper;

use App\Http\Requests\StoreCaregiver;

use Validator;
use Carbon\Carbon;
use Hash;

class TeamController extends Controller
{
    public function index()
    {
        $corp_id = \Helper::encryptor('decrypt',session('corporate.corp_id'));
        $subscribers = Subscriber::select('database_name')->whereCorpId($corp_id)->get();
        foreach ($subscribers as $subscriber) {
            \DB::disconnect('tenant');
            \Config::set('database.connections.tenant.database',$subscriber->database_name);
            \DB::setDefaultConnection('tenant');
            $data['stats']['emp'] = Caregiver::branch()->active()->count();
            $data['stats']['leave'] = Leaves::branch()->pendingApproval()->count();
            \DB::disconnect('tenant');
        }
        return view('corporate.team.index', $data);
    }

    public function viewUsers()
    {
        $users = CorpUsers::orderBy('created_at','desc')->paginate(50);
        return view('corporate.team.users', compact('users'));
    }

    public function addUsers(Request $request)
    {
        if(empty($request->name) || empty($request->email) || empty($request->user_password)){
            return back()->with('alert_error','Name or Email was not supplied, please try again !');
        }
        if(!filter_var($request->email, FILTER_VALIDATE_EMAIL)){
            return back()->with('alert_error','Invalid Email, please try again !');
        }
        $corpUser            = new CorpUsers;
        $corpUser->full_name = ucwords($request->name);
        $corpUser->email     = $request->email;
        $corpUser->gender    = $request->gender;
        $corpUser->password  = bcrypt($request->user_password);
        $corpUser->status    = $request->status;
        $corpUser->save();
            
        return back()->with('alert_success','User added successfully.');
    }

    public function updateUsers(Request $request)
    {
        $userUpdateToken = $request->except('_token');
        if($request->status){
            $user_data = [
                'status' => $request->status
            ];
        }else{
            $user_data = [
                'full_name' => $request->full_name,
                'email' => $request->email,
                'gender' => $request->gender
            ];
            if(!empty($request->new_password)){             
                $user_data = [
                'password' => $request->new_password
            ];
            }
        }

        $corpUsers = CorpUsers::whereId(\Helper::encryptor('decrypt',$request->userId))->first();
        if($corpUsers){
            $corpUsers->update($user_data);
            return response()->json(200);
        }
    }

    public function employee(Request $request)
    {
        if(isset($request->branch_filter) && !empty($request->branch_filter)){
            $filterBranch = $request->branch_filter;
        }else{
            $filterBranch = 'All';
        }

        $caregivers = [];
        $corp_id = \Helper::encryptor('decrypt',session('corporate.corp_id'));
        // connecting to centralconsole db
        $subscribers = Subscriber::select('database_name','org_code')->whereCorpId($corp_id)->get();
        foreach ($subscribers as $subscriber) {
            if($filterBranch != 'All' && \Helper::encryptor('decrypt',$filterBranch) != $subscriber->database_name)
                continue;
            
            \DB::disconnect('tenant');
            // connecting to all branch db
            \Config::set('database.connections.tenant.database',$subscriber->database_name);
            \DB::setDefaultConnection('tenant');
            $orgCode = $subscriber->org_code;
            $branchDb= \Helper::encryptor('encrypt',$subscriber->database_name);
            $collection = Caregiver::active()->with('professional')->with('professional.specializations')->get();
            $caregivers[] = $collection->map(function ($item) use ($orgCode,$branchDb){
                $item['branch_db'] = $branchDb;
                $item['org_code'] = $orgCode;
                return $item;
            });
            \DB::disconnect('tenant');
        }
        return view('corporate.team.caregivers.index',compact('subscribers','caregivers','filterBranch'));
    }

    public function checkemail(Request $request)
    {
        $email= $request->user_email;
        $checkcaregiver = CaregiverLog::where('email',$email)->count();

        if($checkcaregiver){
            echo "Email Already Exist. Please Choose a unique email id.";
        }else{
            if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
                echo "OK";
            }else{
                echo "Enter a valid email id.";
            }
        }
        exit();
    }

    public function checkEmpID(Request $request)
    {
        \DB::transaction(function() use ($request){
            $eid= $request->employee_id;
            $cid= $request->currEmpId;

            $checkEmpID = CaregiverLog::where('employee_id',$eid)->count();
            if($checkEmpID && $eid != $cid){
                echo "Employee Id already Taken. Please Choose a unique id.";
            }else{
                \DB::disconnect('tenant');
                // connecting to corporate db to check for duplicate employee_id
                \Config::set('database.connections.tenant.database',\Helper::encryptor('decrypt',session('corporate.dbName')));
                \DB::setDefaultConnection('tenant');
                CaregiverLog::where('employee_id',$cid)->update(['employee_id' => $eid]);
                \DB::disconnect('tenant');

                // connecting to centralconsole db
                // \Config::set('database.default','central');
                $subscribers = Subscriber::select('database_name')->whereCorpId(\Helper::encryptor('decrypt',session('corporate.corp_id')))->get();
                // \DB::setDefaultConnection('tenant');
                // \DB::disconnect('tenant');

                // connecting to all branch db
                foreach ($subscribers as $subscriber) {
                    \Config::set('database.connections.tenant.database',$subscriber->database_name);
                    \DB::setDefaultConnection('tenant');
                    Caregiver::where('employee_id',$cid)->update(['employee_id' => $eid]);
                    \DB::disconnect('tenant');
                }

                echo "Updated Successfully";
            }
        });
        exit();
    }

    public function createEmployee($database_name)
    {
        \DB::disconnect('tenant');
        \Config::set('database.connections.tenant.database',\Helper::encryptor('decrypt',$database_name));
        \DB::setDefaultConnection('tenant');

        $data['departments'] = Department::get();
        $data['designations'] = Designation::get();
        $data['languages'] = Language::sortByAsc()->get();
        $data['specializations'] = Specialization::get();
        $data['managers'] = Caregiver::whereHas('professional', function($q){
            $q->where('role','Manager');
        })->get();
        $data['skills'] = Skill::get();
        $data['branches'] = Branch::get();
        $data['roles'] = Role::where('name','!=','admin')->where('id','!=',1)->get();
        $data['uploaded_document_categories'] = [];

        \DB::disconnect('tenant');
        
        $data['database_name'] = $database_name;
        return view('corporate.team.caregivers.form', $data);
    }

    public function storeEmployee(StoreCaregiver $request)
    {
        \DB::transaction(function() use ($request){
            $database_name = \Helper::encryptor('decrypt',$request->database_name);
            \DB::disconnect('tenant');
            // connecting to centralconsole db
            \Config::set('database.default','central');
            $subscriber = Subscriber::whereDatabaseName($database_name)->first();
            $tenantId = \Helper::encryptor('encrypt',$subscriber->id);
            \DB::setDefaultConnection('tenant');
            \DB::disconnect('tenant');

            // connecting to branch db
            \Config::set('database.connections.tenant.database',$database_name);
            \DB::setDefaultConnection('tenant');

            $users = User::where('status','1')->count();
            $empID = 0;

            $caregiver_details = $request->all('first_name','middle_name','last_name','date_of_birth','gender','blood_group','marital_status','religion','food_habits','mobile_number','alternate_number','email','personal_email','current_address','current_area','current_city','current_zipcode','current_state','current_country','latitude','longitude','permanent_address','permanent_city','permanent_zipcode','permanent_state','permanent_country');

            if($caregiver_details){
                $bd = self::saveBasicDetails($caregiver_details, 0, $database_name, $subscriber->id);
                $empID = $bd['empID'];
                $employeeId = $bd['employeeId'];
            }

            $user = new User;
            $user->tenant_id = $subscriber->id;
            $user->user_type = 'Caregiver';
            $user->created_by = 'Custom';
            $user->user_id = $empID;
            $user->branch_id = $request->branch_id;
            if( $subscriber->users_limit <= $users ){
                $user->status = 0;
            }
            $user->role_id = $request->access_role;
            $user->full_name = $request->first_name.' '.$request->middle_name.' '.$request->last_name;
            $user->email = $request->email;
            $user->password = $request->password;
            if($request->employment_status != '' && $request->employment_status != null){
                $user->status = $request->employment_status;
            }

            $user->save();
            $user->attachRole($request->access_role);

            if($empID > 0)
            {
                if($request->hasFile('profile_picture'))
                {
                    $profile = $request->file('profile_picture');
                    $extension = $profile->extension();

                    $path = public_path('uploads/provider/'.$tenantId.'/caregivers/'.\Helper::encryptor('encrypt',$empID));
                    if(!is_dir($path)){
                       \File::makeDirectory($path, 0777, true);
                    }

                   $fileName = \Helper::encryptor('encrypt',$empID).'.'.$extension;

                   $res = $profile->move($path, $fileName);

                   Caregiver::find($empID)->update([
                    'profile_image' => $fileName
                    ]);
                }

                $professional_details = $request->all('specialization','qualification','experience_level','college_name','experience','languages_known','workdays','work_from_time','work_to_time','achievements','specialization','branch_id','role','deployable','designation','department','manager','source','source_name','employment_type','date_of_joining','resignation_date','resignation_type','resignation_reason');
                if($professional_details)
                {
                   $id = $request->professional_details_id;
                   $professional_details['caregiver_id'] = $empID;

                   self::saveProfessionalDetails($professional_details, $id);
                }

                $skill_details = $request->all('skills');
                if($skill_details && count($skill_details))
                {
                    $skill_data = [];
                    foreach($skill_details as $s){
                        if(!empty($s)){
                            foreach(json_decode($s,true) as $skill){
                                if($skill){
                                    $skill_data[$skill['skill_id']] = ['tenant_id' => $subscriber->id,'caregiver_id' => $empID, 'proficiency' => $skill['proficiency']];
                                }
                            }
                        }
                    }
                    $emp = Caregiver::find($empID);
                    $emp->caregiver_skills()->sync($skill_data);
                }

                $employment_details = $request->all('employments');
                if($employment_details && count($employment_details)){
                    $employments_data = [];
                    foreach($employment_details as $s){
                        if(!empty($s)){
                            foreach(json_decode($s,true) as $employment){
                                if($employment){
                                    \App\Entities\Caregiver\EmploymentHistory::updateOrCreate(['tenant_id' => $subscriber->id,'caregiver_id' => $empID, 'company_name' => $employment['company_name'], 'period' => $employment['period'], 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
                                }
                            }
                        }
                    }
                }

                $account_details = $request->all('pan_number','aadhar_number','account_name','account_number','bank_name','bank_branch','ifsc_code');
                if($account_details)
                {
                   $id = $request->account_details_id;
                   $account_details['caregiver_id'] = $empID;

                   self::saveAccountDetails($account_details, $id);
                }

                $payroll_details = $request->all('basic_salary','hra','conveyance','food_and_accommodation','medical_allowance','lta','other_allowance','stipend','pf_deduction','esi_deduction','other_deduction','effective_date_of_salary');
                if($payroll_details)
                {
                    $id = $request->payroll_details_id;
                    $payroll_details['caregiver_id'] = $empID;

                    self::savePayrollDetails($payroll_details, $id);
                }

                $accommodation_details = $request->all('arranged_by','accommodation_address','accommodation_city','accommodation_state','accommodation_from_date','accommodation_to_date');
                if($accommodation_details)
                {
                     $id = $request->accommodation_details_id;
                     $accommodation_details['caregiver_id'] = $empID;

                     self::saveAccommodationDetails($accommodation_details, $id);
                }
            }
            \DB::disconnect('tenant');

            // connecting to corporate db
            \Config::set('database.connections.tenant.database',\Helper::encryptor('decrypt',session('corporate.dbName')));
            \DB::setDefaultConnection('tenant');
            $caregiverLog = new CaregiverLog;
            $caregiverLog->employee_id = $employeeId;
            $caregiverLog->email = $request->email;
            $caregiverLog->save();
        });
        return redirect()->route('corporate.employee')->with('alert_success','Employee added Successfully');
    }

    public function editEmployee($id, $database_name)
    {
        \DB::disconnect('tenant');
        \Config::set('database.connections.tenant.database',\Helper::encryptor('decrypt',$database_name));
        \DB::setDefaultConnection('tenant');

        $data['departments'] = Department::get();
        $data['designations'] = Designation::get();
        $data['languages'] = Language::sortByAsc()->get();
        $data['specializations'] = Specialization::get();
        $data['managers'] = Caregiver::whereHas('professional', function($q){
            $q->where('role','Manager');
        })->whereNotIn('id',[Helper::encryptor('decrypt',$id)])->get();
        $data['skills'] = Skill::get();
        $data['branches'] = Branch::get();
        $data['agencies'] = Agency::get();
        $data['e'] = Caregiver::find(Helper::encryptor('decrypt',$id));
        $data['caregiver_skills'] = [];
        if($data['e']){
            $data['caregiver_skills'] = CaregiverSkill::whereCaregiverId($data['e']->id)->get();
            $data['uploaded_document_categories'] = $data['e']->verifications->pluck('doc_type')->unique()->toArray();
        }

        \DB::disconnect('tenant');        
        $data['database_name'] = $database_name;
        return view('corporate.team.caregivers.form', $data);
    }

    public function updateEmployee(StoreCaregiver $request, $id)
    {
        \DB::transaction(function() use ($request, $id){
            $database_name = \Helper::encryptor('decrypt',$request->database_name);
            \DB::disconnect('tenant');
            // connecting to centralconsole db
            \Config::set('database.default','central');
            $subscriber = Subscriber::whereDatabaseName($database_name)->first();
            $tenantId = \Helper::encryptor('encrypt',$subscriber->id);
            \DB::setDefaultConnection('tenant');
            \DB::disconnect('tenant');

            // connecting to branch db
            \Config::set('database.connections.tenant.database',$database_name);
            \DB::setDefaultConnection('tenant');

            $empID = Helper::encryptor('decrypt',$id);

            // Basic Details
            $caregiver_details = $request->all('first_name','middle_name','last_name','date_of_birth','gender','blood_group','marital_status','religion','food_habits','mobile_number','alternate_number','email','personal_email','current_address','current_area','current_city','current_zipcode','current_state','current_country','latitude','longitude','permanent_address','permanent_city','permanent_zipcode','permanent_state','permanent_country');

            if($caregiver_details){
                $bd = self::saveBasicDetails($caregiver_details, Helper::encryptor('decrypt',$id), $database_name, $subscriber->id);
                $empID = $bd['empID'];
                $employeeId = $bd['employeeId'];
            }

            $user = User::where('user_id',Helper::encryptor('decrypt',$id))
                        ->where('tenant_id',$subscriber->id)
                        ->first();
            
                $user->tenant_id = $subscriber->id;
                $user->user_type = 'Caregiver';
                $user->created_by = 'Custom';
                $user->user_id = Helper::encryptor('decrypt',$id);
                $user->full_name = $request->first_name.' '.$request->middle_name.' '.$request->last_name;
                $user->email = $request->email;
                if($request->password != '' || $request->password != null){
                    $user->password = $request->password;
                }
                if($request->employment_status != '' && $request->employment_status != null){
                    $user->status = $request->employment_status;
                }
                $user->save();

            if($empID > 0){
                    // Upload Profile Picture
                if($request->hasFile('profile_picture')){
                    $profile = $request->file('profile_picture');
                    $extension = $profile->extension();

                    $path = public_path('uploads/provider/'.$tenantId.'/caregivers/'.$id);
                    if(!is_dir($path)){
                        \File::makeDirectory($path, 0777, true);
                    }

                    $fileName = Helper::encryptor('encrypt',$empID).'.'.$extension;

                    $res = $profile->move($path, $fileName);

                    // Update File Name to DB
                    Caregiver::find($empID)->update([
                        'profile_image' => $fileName
                        ]);
                }

                // Professional Details
                $professional_details =
                $request->all('specialization','qualification','experience_level','college_name','experience','languages_known','workdays','work_from_time','work_to_time','achievements','specialization','branch_id','role','deployable','designation','department','manager','source','source_name','employment_type','date_of_joining','resignation_date','resignation_type','resignation_reason');

                if($professional_details){
                    $id = Helper::encryptor('decrypt',$request->professional_details_id);
                    $professional_details['caregiver_id'] = $empID;

                    self::saveProfessionalDetails($professional_details, $id);
                }

                // Caregiver Skill Details
                $skill_details = $request->all('skills');
                if($skill_details && count($skill_details)){
                    $skill_data = [];
                    foreach($skill_details as $s){
                        if(!empty($s)){
                            foreach(json_decode($s,true) as $skill){
                                if($skill){
                                    $skill_data[$skill['skill_id']] = ['tenant_id' => $subscriber->id,'caregiver_id' => $empID, 'proficiency' => $skill['proficiency']];
                                }
                            }
                        }
                    }
                    $emp = Caregiver::find($empID);
                    $emp->caregiver_skills()->sync($skill_data);
                }

                // Caregiver Previuos Employment Details
                $employment_details = $request->all('employments');
                if($employment_details && count($employment_details)){
                    $employments_data = [];
                    foreach($employment_details as $s){
                        if(!empty($s)){
                            foreach(json_decode($s,true) as $employment){
                                if($employment){
                                    \App\Entities\Caregiver\EmploymentHistory::updateOrCreate(['tenant_id' => $subscriber->id,'caregiver_id' => $empID, 'company_name' => $employment['company_name'], 'period' => $employment['period']]);
                                }
                            }
                        }
                    }
                }

                // Bank Account Details
                $account_details = $request->all('pan_number','aadhar_number','account_name','account_number','bank_name','bank_branch','ifsc_code');
                if($account_details){
                    $id = Helper::encryptor('decrypt',$request->account_details_id);
                    $account_details['caregiver_id'] = $empID;

                    self::saveAccountDetails($account_details, $id);
                }

                // Salary Details
                $payroll_details = $request->all('basic_salary','hra','conveyance','food_and_accommodation','medical_allowance','lta','other_allowance','stipend','pf_deduction','esi_deduction','other_deduction','effective_date_of_salary');
                if($payroll_details){
                    $id = Helper::encryptor('decrypt',$request->payroll_details_id);
                    $payroll_details['caregiver_id'] = $empID;

                    self::savePayrollDetails($payroll_details, $id);
                }

                // Accommodation Details
                $accommodation_details = $request->all('arranged_by','accommodation_address','accommodation_city','accommodation_state','accommodation_from_date','accommodation_to_date');
                if($accommodation_details){
                    $id = Helper::encryptor('decrypt',$request->accommodation_details_id);
                    $accommodation_details['caregiver_id'] = $empID;

                    self::saveAccommodationDetails($accommodation_details, $id);
                }
            }
            \DB::disconnect('tenant');
        });
        return back()->with('alert_success','Employee updated Successfully');
    }

    public function updateStatus(Request $request)
    {
        $id = Helper::encryptor('decrypt',$request->id);
        if($id){
            \DB::disconnect('tenant');
            // connecting to branch db
            $database_name = \Helper::encryptor('decrypt',$request->branchDb);
            \Config::set('database.connections.tenant.database',$database_name);
            \DB::setDefaultConnection('tenant');
            $res = Caregiver::find($id)->update(['work_status' => $request->status]);
            \DB::disconnect('tenant');            
        }

        return response()->json($res,200);
    }

    public function updateAvailability(Request $request)
    {
        $id = Helper::encryptor('decrypt',$request->id);
        if($id){
            $res = Caregiver::find($id)->update(['availability_status' => $request->status]);
        }

        return response()->json($res,200);
    }

    public function destroy($id)
    {
        $e = Caregiver::find(Helper::encryptor('decrypt',$id));
        if($e){
                    // Delete Professional Details
            $e->professional->delete();
                    // Delete Account Details
            $e->account->delete();
                    // Delete Payroll Details
            $e->payroll->delete();
                    // Delete Accommodation Details
            $e->accommodation->delete();

            $e->delete();
        }

        return back()->with('alert_info','Caregiver deleted successfully');
    }

    static function saveBasicDetails($data, $id, $database_name, $tenant_id)
    {
        $empID = $id;

        if($data){
            if(isset($id) && $id > 0){
                $caregiver = Caregiver::find($id);
                $empID = $id;
            }else{
                $caregiver = new Caregiver;
            }

            if(isset($data['date_of_birth']) && !empty($data['date_of_birth']))
                $dob = date_format(new \DateTime($data['date_of_birth']),'Y-m-d');
            else
                $dob = null;

            $caregiver->tenant_id = $tenant_id;

            if($id == 0){
                \DB::disconnect('tenant');
                // connecting to corporate db to count total caregivers
                \Config::set('database.connections.tenant.database',\Helper::encryptor('decrypt',session('corporate.dbName')));
                \DB::setDefaultConnection('tenant');
                $caregiver_count = CaregiverLog::count();
                \DB::disconnect('tenant');

                // connecting to branch db
                \Config::set('database.connections.tenant.database',$database_name);
                \DB::setDefaultConnection('tenant');
                
                do{
                    $caregiver->employee_id = Settings::pluck('employee_id_prefix')->first().($caregiver_count+1);
                    \DB::disconnect('tenant');

                    // connecting to corporate db to check for duplicate employee_id
                    \Config::set('database.connections.tenant.database',\Helper::encryptor('decrypt',session('corporate.dbName')));
                    \DB::setDefaultConnection('tenant');
                    $checkEmpID = CaregiverLog::where('employee_id',$caregiver->employee_id)->count();
                    \DB::disconnect('tenant');

                    // connecting to branch db
                    \Config::set('database.connections.tenant.database',$database_name);
                    \DB::setDefaultConnection('tenant');

                    $caregiver_count++;
                }while($checkEmpID);

                $caregiver->work_status = 'Available';
            }
            $caregiver->first_name = $data['first_name'];
            $caregiver->middle_name = $data['middle_name'];
            $caregiver->last_name = $data['last_name'];
            $caregiver->date_of_birth = $dob;
            $caregiver->gender = $data['gender'];
            $caregiver->blood_group = $data['blood_group'];
            $caregiver->marital_status = $data['marital_status'];
            $caregiver->religion = $data['religion'];
            $caregiver->food_habits = $data['food_habits'];
            $caregiver->mobile_number = $data['mobile_number'];
            $caregiver->alternate_number = empty($data['alternate_number'])?null:$data['alternate_number'];
            $caregiver->email = $data['email'];
            $caregiver->personal_email = $data['personal_email'];
            $caregiver->current_address = $data['current_address'];
            $caregiver->current_area = $data['current_area'];
            $caregiver->current_city = $data['current_city'];
            $caregiver->current_zipcode = $data['current_zipcode'];
            $caregiver->current_state = $data['current_state'];
            $caregiver->current_country = $data['current_country'];
            $caregiver->latitude = $data['latitude'];
            $caregiver->longitude = $data['longitude'];
            $caregiver->permanent_address = $data['permanent_address'];
            $caregiver->permanent_city = $data['permanent_city'];
            $caregiver->permanent_zipcode = $data['permanent_zipcode'];
            $caregiver->permanent_state = $data['permanent_state'];
            $caregiver->permanent_country = $data['permanent_country'];

            $caregiver->save();

            $empID = $caregiver->id;
            $employeeId = $caregiver->employee_id;

        }

        return array('empID' => $empID, 'employeeId' => $employeeId);
    }

    static function saveProfessionalDetails($data, $id)
    {
        if($data){
            if(isset($id) && $id > 0){
                $details = ProfessionalDetails::find($id);
            }else{
                $details = new ProfessionalDetails;
            }

            if(isset($data['date_of_joining']) && !empty($data['date_of_joining']))
                $doj = date_format(new \DateTime($data['date_of_joining']),'Y-m-d');
            else
                $doj = null;
                
            if(isset($data['resignation_date']) && !empty($data['resignation_date']))
                $dor = date_format(new \DateTime($data['resignation_date']),'Y-m-d');
            else
                $dor = null;

            $details->caregiver_id = $data['caregiver_id'];
            $details->branch_id = $data['branch_id'];
            $details->specialization = $data['specialization'];
            $details->qualification = $data['qualification'];
            $details->experience_level = $data['experience_level'];
            $details->college_name = $data['college_name'];
            $details->experience = $data['experience'];
            $details->languages_known = $data['languages_known']?implode(",",$data['languages_known']):'';
            $details->working_days = $data['workdays']?implode(",",$data['workdays']):'';
            $details->achievements = $data['achievements'];
            $details->role = $data['role'];
            $details->deployable = isset($data['deployable'])?$data['deployable']:'No';
            $details->designation = $data['designation'];
            $details->department = $data['department'];
            $details->manager = $data['manager'];
            $details->source = $data['source'];
            $details->source_name = $data['source_name'];
            $details->date_of_joining = $doj;
            $details->employment_type = $data['employment_type'];            
            $details->resignation_date = $dor;
            $details->resignation_type = $data['resignation_type'];
            $details->resignation_reason = $data['resignation_reason'];

            if(isset($data['work_from_time']) && !empty($data['work_from_time']))
                $details->working_hours_from = Carbon::parse($data['work_from_time'])->format('H:i');
            else
                $details->working_hours_from = null;

            if(isset($data['work_to_time']) && !empty($data['work_to_time']))
                $details->working_hours_to = Carbon::parse($data['work_to_time'])->format('H:i');
            else
                $details->working_hours_to = null;                      

            $details->save();

            Caregiver::find($data['caregiver_id'])->update(['branch_id' => $data['branch_id']]);
            User::whereUserId($data['caregiver_id'])->update(['branch_id' => $data['branch_id']]);
        }
    }

    static function saveAccountDetails($data, $id)
    {
        if($data){
            if(isset($id) && $id > 0){
                $details = AccountDetails::find($id);
            }else{
                $details = new AccountDetails;
            }

            $details->caregiver_id = $data['caregiver_id'];
            $details->pan_number = $data['pan_number'];
            $details->aadhar_number = $data['aadhar_number'];
            $details->account_name = $data['account_name'];
            $details->account_number = $data['account_number'];
            $details->bank_name = $data['bank_name'];
            $details->bank_branch = $data['bank_branch'];
            $details->ifsc_code = $data['ifsc_code'];

            $details->save();
        }
    }

    static function savePayrollDetails($data, $id)
    {
        if($data){
            if(isset($id) && $id > 0){
                $details = PayrollDetails::find($id);
            }else{
                $details = new PayrollDetails;
            }

            if(isset($data['effective_date_of_salary']) && !empty($data['effective_date_of_salary']))
                $edos = date_format(new \DateTime($data['effective_date_of_salary']),'Y-m-d');
            else
                $edos = null;

            $details->caregiver_id = $data['caregiver_id'];
            $details->basic_salary = floatval($data['basic_salary']);
            $details->hra = floatval($data['hra']);
            $details->conveyance = floatval($data['conveyance']);
            $details->food_and_accommodation = floatval($data['food_and_accommodation']);
            $details->medical_allowance = floatval($data['medical_allowance']);
            $details->lta = floatval($data['lta']);
            $details->other_allowance = floatval($data['other_allowance']);
            $details->stipend = floatval($data['stipend']);
            $details->pf_deduction = floatval($data['pf_deduction']);
            $details->esi_deduction = floatval($data['esi_deduction']);
            $details->other_deduction = floatval($data['other_deduction']);
            $details->effective_date_of_salary = $edos;

            $details->save();
        }
    }

    static function saveAccommodationDetails($data, $id)
    {
        if($data){
            if(isset($id) && $id > 0){
                $details = AccommodationDetails::find($id);
            }else{
                $details = new AccommodationDetails;
            }

            if(isset($data['accommodation_from_date']) && !empty($data['accommodation_from_date']))
                $fdate = date_format(new \DateTime($data['accommodation_from_date']),'Y-m-d');
            else
                $fdate = null;

            if(isset($data['accommodation_to_date']) && !empty($data['accommodation_to_date']))
                $tdate = date_format(new \DateTime($data['accommodation_to_date']),'Y-m-d');
            else
                $tdate = null;

            $details->caregiver_id = $data['caregiver_id'];
            $details->arranged_by = $data['arranged_by'];
            $details->accommodation_address = $data['accommodation_address'];
            $details->accommodation_city = $data['accommodation_city'];
            $details->accommodation_state = $data['accommodation_state'];
            $details->accommodation_from_date = $fdate;
            $details->accommodation_to_date = $tdate;

            $details->save();
        }
    }

    public function apply_for_verification(Request $request)
    {
        $database_name = \Helper::encryptor('decrypt',$request->database_name);
        \DB::disconnect('tenant');
        // connecting to branch db
        \Config::set('database.connections.tenant.database',$database_name);
        \DB::setDefaultConnection('tenant');

        $govt_verification = Verification::whereCaregiverId($request->caregiver_Id)->where('doc_type','Government Issued Identification')->count();
        $perm_add_verification = Verification::whereCaregiverId($request->caregiver_Id)->where('doc_type','Permanent Address')->count();
        $curr_add_verification = Verification::whereCaregiverId($request->caregiver_Id)->where('doc_type','Current Address')->count();
        $edu_verification = Verification::whereCaregiverId($request->caregiver_Id)->where('doc_type','Educational Document')->count();
        $imc_verification = Verification::whereCaregiverId($request->caregiver_Id)->where('doc_type','IMC Document')->count();

        if($govt_verification > 0 && $perm_add_verification > 0 && $curr_add_verification > 0 && $edu_verification > 0 && $imc_verification > 0){
            $caregiver = Caregiver::whereId($request->caregiver_Id)->first();
            if($caregiver){
                $agency = Agency::whereId($request->agency_Id)->first();

                if($agency){
                    $mail = Mail::to($agency->agency_email);
                    $mail->send(new DocumentVerifyEmail($caregiver));
                }

                $caregiver->prof_verification = 'Pending';
                $caregiver->save();
                $state = "Applied";
            }else{
                $state = "OOPS";
            }
        }else{
            if($imc_verification == 0)
                $state = "ImcMissing";
            if($edu_verification == 0)
                $state = "EduMissing";
            if($curr_add_verification == 0)
                $state = "CurrAddMissing";
            if($perm_add_verification == 0)
                $state = "PermAddMissing";
            if($govt_verification == 0)
                $state = "GovtMissing";
        }
        \DB::disconnect('tenant');

        return response()->json($state,200);
    }

    public function updateDocuments(Request $request)
    {
        \DB::transaction(function() use ($request){
            $database_name = \Helper::encryptor('decrypt',$request->database_name);
            \DB::disconnect('tenant');
            // connecting to centralconsole db
            \Config::set('database.default','central');
            $subscriber = Subscriber::whereDatabaseName($database_name)->first();
            $tenantId = \Helper::encryptor('encrypt',$subscriber->id);
            \DB::setDefaultConnection('tenant');
            \DB::disconnect('tenant');

            // connecting to branch db
            \Config::set('database.connections.tenant.database',$database_name);
            \DB::setDefaultConnection('tenant');

            $document = 'verification_documents';
            $caregiverID = $request->caregiver_id;
            $document_upload = false;
            $type = $request->type;
            $data = [];

            $document_name = $document.'_document';

            if(!empty($request->$document) || !empty($request->verification_documents_comment))
            {
               if($request->file('documents')->getSize()/1024 < 2048){
                    $document_upload = true;
               }else{
                    $data['message'] = 'Documents Size should be less than 2MB';
               }
            }

            if($document_upload){
                if($type == 'e'){
                    $caregiver = Caregiver::whereId($caregiverID)->first();
                    $upload_path = public_path("uploads/provider/".\Helper::encryptor('encrypt',$subscriber->id)."/caregivers/".\Helper::encryptor('encrypt',$caregiver->id)."/"."document");
                }else{
                    $upload_path = public_path("uploads/provider/".\Helper::encryptor('encrypt',$subscriber->id)."/caregivers/"."/"."temp");
                }

                if(!is_dir($upload_path)){
                    \File::makeDirectory($upload_path, 0777, true);
                }

                //explode to check document duplication
                $temp = explode("_",$request->verification_documents);
                if($type == 'a'){
                    $verification = Verification::whereNull('caregiver_id')->where('doc_name', $temp[1])->get();
                }else{
                    $verification = Verification::whereCaregiverId($caregiver->id)->where('doc_name', $temp[1])->get();
                }

                //upload document
                if($verification->isEmpty()){
                    if(!empty($request->$document) && !empty($request->$document.'_document'))
                    {
                        $document_extension = $request->file('documents')->getClientOriginalExtension();
                        $temp = explode("_",$request->verification_documents);

                        if(count($temp)){
                            $documents = $temp[0];
                            $document_uploaded=$temp[1];
                        }

                        $verification                          = new Verification;
                        if($type == 'e'){
                            $verification->caregiver_id        = $caregiver->id;
                        }else{
                            $verification->caregiver_id        = null;
                        }

                        $verification->tenant_id               = $tenantId;
                        $verification->doc_type                = $documents;
                        $verification->doc_name                = $document_uploaded;
                        $verification->description             = $request->verification_documents_comment;
                        $verification->doc_verification_status = 'Pending';

                        $verification->push();

                        // Update Document Path

                        $document_without_ext   = md5($verification->id)."_".str_replace(' ','_',strtolower($temp[1]));
                        $document_name          = $document_without_ext.".".$document_extension;
                        $request->file('documents')->move($upload_path, $document_name);
                        $verification->doc_path = $document_name;
                        $verification->save();

                        $image = $upload_path.DIRECTORY_SEPARATOR.$document_name;
                        // a new imagick object
                        $im = new \Imagick();
                        // set rsolution
                        $im->setResolution(300,300);
                        // ping the image
                        $im->pingImage($image);
                        // read the image into the object
                        $im->readImage($image);
                        // convert to png
                        $im->setImageFormat("jpg");
                        // other settings
                        $im->setImageCompression(\Imagick::COMPRESSION_JPEG);
                        $im->setImageCompressionQuality(90);
                        $im->setImageUnits(\Imagick::RESOLUTION_PIXELSPERINCH);


                        if($type == 'e'){
                            $mobile_path = public_path("uploads/provider/".\Helper::encryptor('encrypt',$subscriber->id)."/caregivers/".\Helper::encryptor('encrypt',$caregiver->id)."/"."mobile");
                        }else{
                            $mobile_path = public_path("uploads/provider/".\Helper::encryptor('encrypt',$subscriber->id)."/caregivers/"."/"."tempmobile");
                        }
                        if(!is_dir($mobile_path)){
                            \File::makeDirectory($mobile_path, 0777, true);
                        }

                        // write image to disk
                        $im->writeImage($mobile_path."/".$document_without_ext.".jpg");

                        $data['doc_id'] = $verification->id;
                        $data['message'] = 'Documents Updated';
                    }
                }else{
                    $data['message'] = 'The document that you are trying to upload has already been uploaded,please remove it first before re-uploading';
                    $data['duplicate'] = true;
                }
            }            
            \DB::disconnect('tenant');
        });
        return response()->json($data,200);
    }

    public function documentRemove(Request $request)
    {
        $database_name = \Helper::encryptor('decrypt',$request->database_name);
        \DB::disconnect('tenant');
        // connecting to centralconsole db
        \Config::set('database.default','central');
        $subscriber = Subscriber::whereDatabaseName($database_name)->first();
        $tenantId = \Helper::encryptor('encrypt',$subscriber->id);
        \DB::setDefaultConnection('tenant');
        \DB::disconnect('tenant');

        // connecting to branch db
        \Config::set('database.connections.tenant.database',$database_name);
        \DB::setDefaultConnection('tenant');

        $caregiver_doc_id   = $request->doc_Id;
        $caregiver_id       = $request->caregiver_Id;
        $caregiver_folder   = $request->doc_Folder;
        $caregiver_doc_name = $request->doc_Name;
        $data               = $request->except('_token');

        $upload_path = public_path("uploads/provider/".\Helper::encryptor('encrypt',$subscriber->id)."/caregivers/".$caregiver_folder."/document/".$caregiver_doc_name);
        $caregiver_doc_name_without_extension = substr($caregiver_doc_name, 0, strrpos($caregiver_doc_name, "."));
        $mobile_path = public_path("uploads/provider/".\Helper::encryptor('encrypt',$subscriber->id)."/caregivers/".$caregiver_folder."/mobile/".$caregiver_doc_name_without_extension.".jpg");
        $res = [];
        if($data){
            $res = Verification::where('id',$caregiver_doc_id)->where('doc_path',$caregiver_doc_name)->delete();
            if (file_exists($upload_path)){
                unlink($upload_path);
            }
            if (file_exists($mobile_path)){
                unlink($mobile_path);
            }
        }
        \DB::disconnect('tenant');

        return response()->json($res,200);
    }
}
