<?php
namespace App\Http\Controllers\Corporates;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Corporates\CorpProfile;

class ProfileController extends Controller
{
    public function index()
    {
        $profile = CorpProfile::first();
        return view('corporate.administration.profile', compact('profile'));
    }

    public function update(Request $request)
    {
        $data = $request->only('email','name','address','state','landline_number');

        $profile = CorpProfile::first();

        if($data && $profile){
            $res = $profile->update($data);

        if($request->hasFile('logo')){
                $profile = CorpProfile::first();

                $logo = $request->file('logo');

                $path = public_path('uploads/corporate');
                if(!is_dir($path)){
                    \File::makeDirectory($path,0777,true);
                }
                $fileNameWithoutExt = \Helper::encryptor('encrypt',$profile->id);
                $fileName = session('corporate.dbName').'.'.$logo->extension();
                $mask = $fileNameWithoutExt."."."*";
                array_map('unlink', glob($path.'/'.$mask));
                $res = $logo->move($path,$fileName);

                // Update organization logo file path
                $profile->logo = $fileName;
                $profile->save();

                session()->forget('corporate.logo');
                session()->put('corporate.logo',$fileName);
                $CookieLogo = \Cookie::forever('corporateCookieLogo', $fileName);
            }
            session()->forget('corporate.name');
            session()->put('corporate.name',$request->name);

            $redirecting = redirect()->back()->with('alert_success','Profile updated successfully');
            if(isset($CookieLogo)){
                $redirecting = $redirecting->withCookie($CookieLogo);
            }          

            return $redirecting;
        }
    }
}
