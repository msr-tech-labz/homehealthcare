<?php
namespace App\Http\Controllers\Corporates;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Console\Subscriber;
use App\Entities\Profile;

class DashboardController extends Controller
{
    public function index()
    {
    	$profiles = [];
    	$corp_id = \Helper::encryptor('decrypt',session('corporate.corp_id'));
    	$subscribers = Subscriber::select('database_name')->whereCorpId($corp_id)->get();
    	foreach ($subscribers as $subscriber) {
    		\Config::set('database.connections.tenant.database',$subscriber->database_name);
    		\DB::setDefaultConnection('tenant');
    		$profiles[] = Profile::first();
    		\DB::disconnect('tenant');
    	}
        return view('corporate.dashboard', compact('profiles'));
    }

    public function administration()
    {
        return view('corporate.administration.index');
    }
}