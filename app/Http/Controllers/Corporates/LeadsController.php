<?php
namespace App\Http\Controllers\Corporates;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Helpers\Helper;

use App\Entities\Console\Subscriber;

use App\Entities\Masters\Service;
use App\Entities\Masters\ServiceCategory;
use App\Entities\Masters\Language;
use App\Entities\Masters\Skill;
use App\Entities\Masters\Specialization;
use App\Entities\Masters\ReferralCategory;
use App\Entities\Masters\ReferralSource;
use App\Entities\Masters\Holidays;
use App\Entities\Masters\Task;
use App\Entities\Masters\RateCard;
use App\Entities\Masters\Taxrate;

use App\Entities\Masters\Consumables;
use App\Entities\Masters\Surgicals;
use App\Entities\Masters\BillingProfile;

use App\Entities\CaseBillables;
use App\Entities\CaseServiceRequest;
use App\Entities\CaseServices;
use App\Entities\CaseServiceOrder;
use App\Entities\Patient;
use App\Entities\CreditMemo;
use App\Entities\Lead;
use App\Entities\DiscountsLog;
use App\Entities\CaseForm;
use App\Entities\LeadRequests;
use App\Entities\Schedule;
use App\Entities\Comment;
use App\Entities\FCMToken;
use App\Entities\User;
use App\Entities\CaseDisposition;
use App\Entities\Caregiver\Caregiver;
use App\Entities\CaseRatecard;
use App\Entities\ServiceInterruption;
use App\Entities\CaseDocument;
use App\Entities\Settings;
use App\Entities\Attendance;
use App\Entities\AggregatorLeads;
use App\Entities\Masters\ManagementComposition;
use App\Entities\ReferralPayout;
use App\Entities\PatientAddress;
use App\Entities\DailyRevenueStorage;
use App\Entities\PatientMailers;

use Mail;
use Carbon\Carbon;

use App\Mail\Provider\LeadConvertion;
use App\Mail\Provider\LeadCompletion;
use App\Mail\Provider\LeadLoss;
use App\Mail\Provider\BillableToVendor;
use App\Mail\Provider\StaffAssign;

class LeadsController extends Controller
{    
    public function index(Request $request)
    {
        if(!empty($request->from_date) && !empty($request->to_date)){
            $filterFromDate = date_format(new \DateTime($request->from_date),"Y-m-d");
            $filterToDate = date_format(new \DateTime($request->to_date),"Y-m-d");
            $filterStatus = $request->status_filter;
            $filterBranch = $request->branch_filter;
        }else if(!empty(session('corp.from_date_leads')) && !empty(session('corp.to_date_leads'))){
            $filterFromDate = session('corp.from_date_leads');
            $filterToDate = session('corp.to_date_leads');
            $filterStatus = session('corp.status_leads');
            $filterBranch = session('corp.branch_leads');
        }else{
            $filterFromDate = date('Y-m-01');
            $filterToDate = date('Y-m-t');
            $filterStatus = 'All';
            $filterBranch = 'All';
        }
        session()->put('corp.from_date_leads',$filterFromDate);
        session()->put('corp.to_date_leads',$filterToDate);
        session()->put('corp.status_leads',$filterStatus);
        session()->put('corp.branch_leads',$filterBranch);

        $tableLeads = [];
        $leads = [];
        $followupArray = array();
        $pendingLeads = 0;
        $followupLeads = 0;
        $abc = 0;
        $corp_id = Helper::encryptor('decrypt',session('corporate.corp_id'));
        // connecting to centralconsole db
        $subscribers = Subscriber::select('database_name','org_code')->whereCorpId($corp_id)->get();
        foreach ($subscribers as $key => $subscriber) {
            if($filterBranch != 'All' && Helper::encryptor('decrypt',$filterBranch) != $subscriber->database_name)
                continue;

            \DB::disconnect('tenant');
            // connecting to all branch db
            \Config::set('database.connections.tenant.database',$subscriber->database_name);
            \DB::setDefaultConnection('tenant');
            $orgCode = $subscriber->org_code;
            $branchDb= Helper::encryptor('encrypt',$subscriber->database_name);

            $leads_query = Lead::with(['patient' => function($q){
                            $q->addSelect('id','first_name','last_name','enquirer_name','contact_number','alternate_number','city');
                        },'referralSource','serviceRequired']);
           
            $leads_query1 = clone $leads_query;
            $leads_query2 = clone $leads_query;
            $leads_query3 = clone $leads_query;
            $leads_query4 = $leads_query->get();

            // total pending
            $pendingLeads += $leads_query2->whereStatus('Pending')->count();

            // total pending
            $followupLeads += $leads_query3->whereStatus('Follow Up')->count();

            // Follow-up Leads list
            foreach($leads_query4->whereIn('status',['Follow Up']) as $lead){
                $followupArray[] = [
                    'patient'  => $lead->patient->full_name,
                    'enquirer' => $lead->patient->enquirer_name,
                    'phone'    => $lead->patient->contact_number,
                    'date'     => count($lead->dispositions)?date_format(new \DateTime($lead->dispositions->last()->follow_up_datetime),'d-m-Y'):'',
                    'time'     => count($lead->dispositions)?date_format(new \DateTime($lead->dispositions->last()->follow_up_datetime),'h:i:s A'):'',
                    'comment'  => count($lead->dispositions)?$lead->dispositions->last()->follow_up_comment:'',
                    'id'       => Helper::encryptor('encrypt',$lead->id),
                ];
            }

            \DB::disconnect('tenant');

            if($filterStatus != 'All'){
                $leads_query = $leads_query1->whereStatus($filterStatus);
            }
            $leads_query1 = $leads_query1
                        ->whereRaw(\DB::raw('DATE(leads.created_at) BETWEEN "'.$filterFromDate.'" AND "'.$filterToDate.'"'))
                        ->whereIn('status',['Pending','Follow Up'])
                        ->orderBy('leads.created_at','Desc')
                        ->get();
            $tableLeads[] = $leads_query1->map(function ($item) use ($orgCode,$branchDb){
                $item['branch_db'] = $branchDb;
                $item['org_code'] = $orgCode;
                return $item;
            });
        }
        return view('corporate.leads.datatable-leads-index', compact('subscribers','tableLeads','pendingLeads','followupLeads','followupArray','filterFromDate','filterToDate','filterStatus','filterBranch'));
    }

    public function createLead($database_name)
    {
        \DB::disconnect('tenant');
        \Config::set('database.connections.tenant.database',Helper::encryptor('decrypt',$database_name));
        \DB::setDefaultConnection('tenant');

        $cadre = explode(',',ManagementComposition::whereType('leads_converted_by')->value('composition_cadre'));
        $data['languages'] = Language::get();
        $data['services'] = Service::get();
        $data['leadRequestId'] = 0;

        $data['servicecategories'] = ServiceCategory::get();
        if(session('orgType') == 'Physiotherapy'){
            $data['services'] = Service::where('service_type','PHYSIOTHERAPY')->orderBy('category_id','Asc')->get();
        }else{
            $data['services'] = Service::orderBy('category_id','Asc')->get();
        }
        $data['specializations'] = Specialization::get();
        $data['managers'] = Caregiver::active()->whereHas('professional', function($q){
                                        $q->whereRole('Manager');
                                    })->get();

        $data['categories'] = ReferralCategory::get();

        $data['staffs'] = Caregiver::active()->select('id','employee_id','first_name','middle_name','last_name')
                                     ->whereHas('professional', function($q) use ($cadre){
                                        $q->whereIn('department',$cadre);
                                     })->get();

        $data['sources'] = ReferralSource::orderBy('category_id','Asc')->get();

        $holidays = Holidays::get()->pluck('holiday_date');
        if(count($holidays) != 0){

            $data['holidays'] = [];
            $data['holidaysr'] = [];

            foreach ($holidays as $h) {
                array_push($data['holidays'],$h->format('d-m-Y'));
                array_push($data['holidaysr'],$h->format('Y-m-d'));
            }
            $data['holidays'] = implode('","',$data['holidays']);
            $data['holidaysr'] = implode('","',$data['holidaysr']);
        }else{
            $data['holidays'] = '20-03-1992';
            $data['holidaysr'] = '1992-03-20';
        }

        \DB::disconnect('tenant');

        $data['database_name'] = $database_name;

        return view('corporate.leads.create', $data);
    }

    public function storeLead(Request $request)
    {
        $saveLeadOnly = null;
        \DB::transaction(function() use ($request, &$saveLeadOnly){
            $database_name = Helper::encryptor('decrypt',$request->database_name);
            \DB::disconnect('tenant');
            // connecting to centralconsole db
            \Config::set('database.default','central');
            $subscriber = Subscriber::whereDatabaseName($database_name)->first();
            $tenantId = Helper::encryptor('encrypt',$subscriber->id);
            \DB::setDefaultConnection('tenant');
            \DB::disconnect('tenant');

            // connecting to branch db
            \Config::set('database.connections.tenant.database',$database_name);
            \DB::setDefaultConnection('tenant');

            if(isset($request->email) && $request->email != null){
                if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
                  $emailErr = "Invalid email format"; 
                  return redirect()->back()->with('alert_error', $emailErr);
                }
            }

            $id = ($request->id!='0')?Helper::encryptor('decrypt',$request->id):0;
            $patientID = ($request->patient_id!='0')?(!is_numeric($request->patient_id))?Helper::encryptor('decrypt',$request->patient_id):$request->patient_id:0;
            $saveLeadOnly = Helper::encryptor('decrypt',$request->mode);
            if($saveLeadOnly != 'L'){
                $patientDetails = $request->only('first_name','last_name','date_of_birth','patient_age','patient_weight','gender','contact_number','email','alternate_number','enquirer_name','relationship_with_patient','street_address','area','city','zipcode','state','country','latitude','longitude');

                if($patientID > 0){
                    $patient = Patient::find($patientID);
                }else{
                    $patient = null;
                }

                if(!empty($patientDetails['date_of_birth'])){
                    $date = new \DateTime($patientDetails['date_of_birth']);
                    $patientDetails['date_of_birth'] = date_format($date,'Y-m-d');
                }else{
                    $patientDetails['date_of_birth'] = null;
                }

                $patientDetails['tenant_id'] = $subscriber->id;

                // Create or Update Patient data
                if($patient){
                    if(empty($patient->patient_id) && $request->status == 'Converted'){
                        $prefix = Settings::value('patient_id_prefix');
                        if($prefix){
                            $lastPatientId = Patient::whereNotNull('patient_id')->orderBy(\DB::raw('length(patient_id),patient_id'))->get()->last();
                            if($lastPatientId)
                                $lastID = substr($lastPatientId->patient_id, strlen($prefix));
                            else
                                $lastID = 0;

                            $patientDetails['patient_id'] = $prefix.str_pad(intval($lastID + 1),strlen($lastID),0,STR_PAD_LEFT);
                        }
                    }
                    $patient->update($patientDetails);
                    $patientID = $patient->id;
                }else{
                    $patientDetails['created_at'] = date('Y-m-d H:i:s');
                    // Generate Unique Patient ID
                    if(empty($patient->patient_id) && $request->status == 'Converted'){
                        $prefix = Settings::value('patient_id_prefix');
                        if($prefix){
                            $lastPatientId = Patient::whereNotNull('patient_id')->orderBy(\DB::raw('length(patient_id),patient_id'))->get()->last();
                            if(!$lastPatientId){
                                $lastID = 0;
                            }else{
                                $lastID = substr($lastPatientId->patient_id, strlen($prefix));
                            }
                            $patientDetails['patient_id'] = $prefix.str_pad(intval($lastID + 1),strlen($lastID),0,STR_PAD_LEFT);
                        }
                    }
                    if (isset($patientDetails['contact_number'])) {
                        $patientDetails['contact_number'] = preg_replace('/\s+/','',$patientDetails['contact_number']);
                    }
                    if (isset($patientDetails['alternate_number'])) {
                        $patientDetails['alternate_number'] = preg_replace('/\s+/','',$patientDetails['alternate_number']);
                    }

                    $patientID = Patient::insertGetId($patientDetails);
                }
            }

            // Create lead details
            $caseDetails = $request->only('case_description','case_type','medical_conditions','medications','procedures','hospital_name','primary_doctor_name','special_instructions','service_category','service_required','service_offered','manager_id','current_rack_rate','estimated_duration','gender_preference','language_preference','assessment_date','assessment_time','assessment_notes','rate_agreed','registration_amount','payment_mode','payment_notes','referral_category','referral_source','referrer_name','converted_by','referral_value','referral_type','branch_id','status','dropped_reason','dropped_reason_other','remarks');

            if($patientID > 0){
                $caseDetails['patient_id'] = $patientID;
                $caseDetails['branch_id'] = $request->branch_id;

                $caseDetails['tenant_id'] = Helper::getTenantID();

                if(!empty($caseDetails['dropped_reason']))
                $caseDetails['dropped_reason'] = json_encode($caseDetails['dropped_reason']);

                if(!empty($caseDetails['dropped_reason_other']))
                $caseDetails['dropped_reason_other'] = $request->dropped_reason_other;

                if(!empty($caseDetails['language_preference']))
                    $caseDetails['language_preference'] = implode(",",$caseDetails['language_preference']);

                if($id == 0){
                    $caseDetails['created_at'] = $caseDetails['updated_at'] = date('Y-m-d H:i:s');

                    // Generate Unique Episode ID
                    if($request->status == 'Converted'){
                        $caseDetails['episode_id'] = Patient::find($patientID)->patient_id.str_pad(Lead::wherePatientId($patientID)->count() + 1,2,0,STR_PAD_LEFT);
                    }

                    $lead = Lead::create($caseDetails);
                    $leadID = $lead->id;
                }else{
                    $lead = Lead::find($id);
                    if(empty($lead->episode_id) && $request->status == 'Converted'){
                        // Generate Unique Episode ID
                        $caseDetails['episode_id'] = Patient::find($patientID)->patient_id.str_pad(Lead::wherePatientId($patientID)->count() + 1,2,0,STR_PAD_LEFT);
                    }

                    $caseDetails['updated_at'] = date('Y-m-d H:i:s');
                    $leadID = $id;
                    $lead->update($caseDetails);
                }

                if($caseDetails['status'] == 'Follow Up'){
                    $followupData = $request->only('status','follow_up_datetime','follow_up_comment');
                    $followupData['lead_id'] = $leadID;
                    $followupData['user_id'] = Helper::getUserID();
                    $followupData['disposition_date'] = date('Y-m-d H:i:s');
                    CaseDisposition::firstOrCreate($followupData);
                }else if($caseDetails['status'] == 'Dropped'){
                    $droppedData['lead_id'] = $leadID;
                    $droppedData['status'] = $request->status;
                    $droppedData['caselosscomment'] = isset($request->dropped_reason)?implode(',',$request->dropped_reason):null;
                    $droppedData['user_id'] = Helper::getUserID();
                    $droppedData['disposition_date'] = date('Y-m-d H:i:s');
                    CaseDisposition::firstOrCreate($droppedData);
                }else{
                    $dispoData['lead_id'] = $leadID;
                    $dispoData['user_id'] = Helper::getUserID();
                    $dispoData['status'] = $request->status;
                    $dispoData['disposition_date'] = date('Y-m-d H:i:s');
                    CaseDisposition::firstOrCreate($dispoData);
                }

                $commission = ReferralPayout::whereLeadId($leadID)->first();
                 // && $caseDetails['status'] == 'Converted'
                if(!$commission && $request->referral_source > 0){
                    $newCommission = new ReferralPayout;

                    $newCommission->lead_id = $leadID;
                    $newCommission->tenant_id = Helper::getTenantID();
                    $newCommission->source_id = $request->referral_source;
                    $newCommission->value = ReferralSource::whereId($request->referral_source)->value('referral_value');
                    $newCommission->type = $request->referral_type;
                    $newCommission->patient_id = $patientID;

                    $newCommission->save();
                }
            }

            
            if($lead->patient->email != null && $id == 0 && $request->status == 'Converted' && filter_var($lead->patient->email, FILTER_VALIDATE_EMAIL)){
                $mail = Mail::to($lead->patient->email);
                $mail->send(new LeadConvertion($lead,$lead->patient));
            }
            \DB::disconnect('tenant');
        });

        if($saveLeadOnly == 'L')
            return back()->with('alert_success','Details saved successfully');
        return redirect()->route('corporate.lead.index')->with('alert_success','Lead saved successfully');
    }

    public function viewLead($id, $database_name)
    {
        // connecting to centralconsole db
        $tenant_id = Subscriber::whereDatabaseName(Helper::encryptor('decrypt',$database_name))->pluck('id')->first();
        \DB::disconnect('tenant');
        \Config::set('database.connections.tenant.database',Helper::encryptor('decrypt',$database_name));
        \DB::setDefaultConnection('tenant');

        $id = Helper::encryptor('decrypt',$id);
        $data['l'] = Lead::whereId($id)
                        ->with(['patient', 'serviceRequests', 'schedules',
                        'assessment' => function($q){
                            $q->addSelect('id','lead_id','form_type','form_data','created_at');
                        },
                        'billables' => function($q){
                            $q->addSelect('id','lead_id','created_at','category','item_id','item','rate','quantity','amount');
                        },
                        'billables.consumable' => function($q){
                            $q->addSelect('id','consumable_name','consumable_price','tax_id');
                        },
                        'billables.surgical' => function($q){
                            $q->addSelect('id','surgical_name','surgical_price','tax_id');
                        },
                        'comments' => function($q){
                            $q->addSelect('id','lead_id','user_name','comment','created_at');
                        },
                        'serviceRequests.service' => function($q){
                            $q->addSelect('id','service_name');
                        },'serviceRequired' => function($q){
                            $q->addSelect('id','service_name');
                        }])
                        ->first();

        if(!$data['l']){
            return back()->with('alert_error','Unable to find lead!');
        }

        if(session('orgType') == 'Physiotherapy'){
            $data['services'] = Service::select('id','service_type','service_name')->where('service_type','PHYSIOTHERAPY')->with('ratecard')->get();
        }else{
            $data['services'] = Service::select('id','service_type','service_name')->with('ratecard')->get();
        }

        if(Helper::getSetting('schedule_creation_for_outstanding_customer') ==  1 && !\Entrust::ability('admin','leads-schedules-authorize'))
            $data['outstanding'] = ['total_charges' => 0,'total_payments' => 0,'outstanding_balance' => 0];
        else
            $data['outstanding'] = ['total_charges' => 0,'total_payments' => 0,'outstanding_balance' => 0];

        $data['servicecategories'] = ServiceCategory::select('id','category_name')->get();
        $data['consumables'] = Consumables::select('id','consumable_name','consumable_price','tax_id')->get();
        $data['surgicals'] = Surgicals::select('id','surgical_name','surgical_price','tax_id')->get();

        $data['categories'] = ReferralCategory::select('id','category_name')->get();
        $data['languages'] = Language::select('id','language_name')->get();
        $data['skills'] = Skill::select('id','skill_name')->get();
        $data['sources'] = ReferralSource::select('id','source_name')->get();
        $data['managers'] = Caregiver::whereHas('professional', function($q){
                                        $q->whereRole('Manager');
                                    })->get();
        $data['specializations'] = Specialization::select('id','specialization_name')->get();
        $data['feedbackcomments'] = [];
        $data['losscomment'] = [];
        $data['persons'] = Caregiver::select('id','first_name','middle_name','last_name','employee_id')
                            ->whereHas('professional',function($q){
                                $q->whereRole('Manager');
                            })->get();

        $data['vendors'] = BillingProfile::get();
        $data['tasks'] = Task::select('id','category','task_name','description')->get();

        $holidays = Holidays::select('holiday_name','holiday_date')->get()->pluck('holiday_date');
        if(count($holidays) != 0){
            $data['holidays'] = [];
            $data['holidaysr'] = [];

            foreach ($holidays as $h) {
                if(!empty($h)){
                    array_push($data['holidays'],$h->format('d-m-Y'));
                    array_push($data['holidaysr'],$h->format('Y-m-d'));
                }
            }
            $data['holidays'] = implode('","',$data['holidays']);
            $data['holidaysr'] = implode('","',$data['holidaysr']);
        }else{
            $data['holidays'] = '20-03-1992';
            $data['holidaysr'] = '1992-03-20';
        }
        $data['sources'] = ReferralSource::orderBy('category_id','Asc')->get();
        $data['extraAddress'] = PatientAddress::wherePatientId($data['l']->patient->id)->whereStatus('active')->get();

        \DB::disconnect('tenant');
        $data['database_name'] = $database_name;
        $data['tenant_id'] = Helper::encryptor('encrypt',$tenant_id);
        return view('corporate.leads.view-lead', $data);
    }

    public function searchPatient(Request $request)
    {
        $database_name = Helper::encryptor('decrypt',$request->database_name);
        \DB::disconnect('tenant');
        // connecting to branch db
        \Config::set('database.connections.tenant.database',$database_name);
        \DB::setDefaultConnection('tenant');

        $res = [];
        $results = [];

        $patient = Patient::select('id','patient_id','first_name','last_name','date_of_birth','patient_age','patient_weight','gender','contact_number','email','alternate_number','enquirer_name','relationship_with_patient','street_address','area','city','zipcode','state','country','latitude','longitude');
        $number = preg_replace('~^[0\D]++|\D++~','', $request->t);

        if(!empty($request->c)){
            switch ($request->c) {
                case 'ID':
                    $patient->wherePatientId($request->t);
                    break;

                case 'Mobile':
                    $patient->whereRaw(\DB::raw("replace(`contact_number`,' ','') like '%$number%'"))
                            ->orWhereRaw(\DB::raw("replace(`alternate_number`,' ','') like '%$number%'"));
                    break;

                case 'Email':
                    $patient->where('email','like','%'.$request->t.'%');
                    break;
            }
        }
        if(!empty($request->e)){
            $patient->whereEmail($request->e);
        }
        if(!empty($request->f)){
            $patient->where('first_name','like','%'.$request->f.'%')
                    ->orWhere('last_name','like','%'.$request->f.'%');
        }
        if(!empty($request->l)){
            $patient->where('first_name','like','%'.$request->l.'%')
                    ->orWhere('last_name','like','%'.$request->l.'%');
        }

        $results = $patient->get();

        \DB::disconnect('tenant');

        return response()->json($results, 200);
    }

    public function checkemail(Request $request)
    {
        $database_name = Helper::encryptor('decrypt',$request->database_name);
        \DB::disconnect('tenant');
        // connecting to branch db
        \Config::set('database.connections.tenant.database',$database_name);
        \DB::setDefaultConnection('tenant');

        $email= $request->user_email;
        $checkpatient = Patient::where('email',$email)->count();
        if($checkpatient){
            echo "Email Already Exist.";
        }
        else
        {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
                echo "OK";
            }else{
                echo "Enter a valid email id.";
            }
        }
        \DB::disconnect('tenant');

        exit();
    }

    public function savePatient(Request $request)
    {
        if(isset($request->email) && $request->email != null){
            if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
              $emailErr = "Invalid email format"; 
              return redirect()->back()->with('alert_error', $emailErr);
            }
        }

        // connecting to centralconsole db
        \DB::disconnect('tenant');
        // connecting to branch db
        \Config::set('database.connections.tenant.database',Helper::encryptor('decrypt',$request->database_name));
        \DB::setDefaultConnection('tenant');

        $patientData = [
            'first_name'                => $request->first_name,
            'last_name'                 => $request->last_name,
            'patient_age'               => $request->patient_age,
            'patient_weight'            => $request->patient_weight,
            'gender'                    => $request->gender,
            'contact_number'            => $request->contact_number,
            'email'                     => $request->email,
            'alternate_number'          => $request->alternate_number,
            'enquirer_name'             => $request->enquirer_name,
            'street_address'            => $request->street_address,
            'area'                      => $request->area,
            'city'                      => $request->city,
            'zipcode'                   => $request->zipcode,
            'state'                     => $request->state,
            'country'                   => $request->country,
            'latitude'                  => $request->latitude,
            'longitude'                 => $request->longitude,
            'relationship_with_patient' => $request->relationship_with_patient,
        ];

        if($request->dob){
            $patientData['dob'] = date_format(new \DateTime($request->dob),'Y-m-d');
        }else{
            $patientData['dob'] = null;
        }

        // Store Patient Info
        if($patientData){
            $patient = Patient::find(\Helper::encryptor('decrypt',$request->patient_id));

            $patient->first_name = $patientData['first_name'];
            $patient->last_name = $patientData['last_name'];
            $patient->date_of_birth = $patientData['dob'];
            $patient->patient_age = $patientData['patient_age'];
            $patient->patient_weight = $patientData['patient_weight'];
            $patient->gender = $patientData['gender'];
            $patient->contact_number = $patientData['contact_number'];
            $patient->email = $patientData['email'];
            $patient->alternate_number = $patientData['alternate_number'];
            $patient->enquirer_name = $patientData['enquirer_name'];
            $patient->street_address = $patientData['street_address'];
            $patient->area = $patientData['area'];
            $patient->city = $patientData['city'];
            $patient->zipcode = $patientData['zipcode'];
            $patient->state = $patientData['state'];
            $patient->country = $patientData['country'];
            $patient->latitude = $patientData['latitude'];
            $patient->longitude = $patientData['longitude'];
            $patient->relationship_with_patient = $patientData['relationship_with_patient'];
            $patient->save();

            \DB::disconnect('tenant');

            return back()->with('alert_success','Patient Details Saved Successfully');
        }

        return back()->with('alert_danger','There was an error, please try re-updating');
    }

    public function lookUpPatientNumber(Request $request)
    {
        $database_name = Helper::encryptor('decrypt',$request->database_name);
        \DB::disconnect('tenant');
        // connecting to branch db
        \Config::set('database.connections.tenant.database',$database_name);
        \DB::setDefaultConnection('tenant');

        $number= $request->patient_number;
        $checknumber = Patient::whereRaw(\DB::raw("replace(`contact_number`,' ','') like '%$number%'"))->orWhereRaw(\DB::raw("replace(`alternate_number`,' ','') like '%$number%'"))->count();
        if($checknumber){
            echo "Patient Already Exists";
        }
        else{
            echo "New Patient Entry. Please Continue.";
        }
        \DB::disconnect('tenant');

        exit();
    }

    public function uploadPicture(Request $request)
    {
        // connecting to centralconsole db
        $tenant_id = Subscriber::whereDatabaseName(Helper::encryptor('decrypt',$request->database_name))->pluck('id')->first();
        \DB::disconnect('tenant');
        // connecting to branch db
        \Config::set('database.connections.tenant.database',Helper::encryptor('decrypt',$request->database_name));
        \DB::setDefaultConnection('tenant');

        // Upload Picture
        $profile = $request->file('pic_upload');
        if($profile){
            ini_set('display_error',1);
            
            $extension = $profile->extension();            

            $path = public_path('uploads/provider/'.Helper::encryptor('encrypt',$tenant_id).'/case_documents/'.$request->lead_id);
            if(!is_dir($path)){
                \File::makeDirectory($path, 0777, true);
            }

            $tempFileName = $request->patient_id.'_temp.jpg';
            $fileName = $request->patient_id.'.jpg';
            
            $path = $path.'/'.$fileName;            
            \Image::make($profile->getRealPath())->resize(300,300)->save($path, 80);

            \DB::disconnect('tenant');

            return back()->with('alert_success','Image updated successfully');            
        }
        \DB::disconnect('tenant');
        
        return back()->with('alert_error','Error uploading patient image!');
    }

    public function addemailaddress($id, $database_name)
    {
        \DB::disconnect('tenant');
        \Config::set('database.connections.tenant.database',Helper::encryptor('decrypt',$database_name));
        \DB::setDefaultConnection('tenant');

        $patient = Patient::whereId(\Helper::encryptor('decrypt',$id))->first();
        $emails = PatientMailers::wherePatientId(\Helper::encryptor('decrypt',$id))->orderBy('status','asc')->get();
        $addresses = PatientAddress::wherePatientId(\Helper::encryptor('decrypt',$id))->orderBy('status','asc')->get();

        \DB::disconnect('tenant');

        return view('corporate.leads.addEmailAddress',compact('patient','emails','addresses','database_name'));
    }

    public function extraAddressStore(Request $request)
    {
        if(empty($request->type) || empty($request->address || empty($request->database_name))){
            return back()->with('alert_error','Type or Address was not supplied, please try again !');
        }
        \DB::disconnect('tenant');
        \Config::set('database.connections.tenant.database',Helper::encryptor('decrypt',$request->database_name));
        \DB::setDefaultConnection('tenant');

        $extraAddress = new PatientAddress;
        $extraAddress->type = ucwords($request->type);
        $extraAddress->address = ucwords($request->address);
        $extraAddress->patient_id = \Helper::encryptor('decrypt',$request->patient_id);
        $extraAddress->latitude = $request->latitude;
        $extraAddress->longitude = $request->longitude;
        $extraAddress->save();

        \DB::disconnect('tenant');            
        return back()->with('alert_success','Address added for the patient.');
    }

    public function extraAddressUpdate(Request $request)
    {
        if(empty($request->type) || empty($request->address) || empty($request->database_name)){
            return back()->with('alert_error','Type or Address was not supplied, please try again !');
        }
        \DB::disconnect('tenant');
        \Config::set('database.connections.tenant.database',Helper::encryptor('decrypt',$request->database_name));
        \DB::setDefaultConnection('tenant');

        $extraAddress = PatientAddress::whereId(\Helper::encryptor('decrypt',$request->id))->first();
        $extraAddress->type = ucwords($request->type);
        $extraAddress->address = ucwords($request->address);
        $extraAddress->patient_id = \Helper::encryptor('decrypt',$request->patient_id);
        $extraAddress->latitude = $request->latitude;
        $extraAddress->longitude = $request->longitude;
        $extraAddress->save();
            
        \DB::disconnect('tenant');
        return back()->with('alert_success','Address updated for the patient.');
    }

    public function extraAddressUpdateStatus(Request $request)
    {
        if(empty($request->id) || empty($request->database_name)){
            return back()->with('alert_error','Address not found, please try again !');
        }
        \DB::disconnect('tenant');
        \Config::set('database.connections.tenant.database',Helper::encryptor('decrypt',$request->database_name));
        \DB::setDefaultConnection('tenant');

        $extraAddress = PatientAddress::whereId(\Helper::encryptor('decrypt',$request->id))->first();

        if($request->status == 1){
            $extraAddress->status = 'active';
        }else{
            $extraAddress->status = 'inactive';
        }

        $extraAddress->save();
        $state = 'statusChanged';

        \DB::disconnect('tenant');
        return $state;
    }

    public function extraEmailStore(Request $request)
    {   
        if(empty($request->name) || empty($request->email) || empty($request->database_name)){
            return back()->with('alert_error','Name or Email was not supplied, please try again !');
        }
        if(!filter_var($request->email, FILTER_VALIDATE_EMAIL)){
            return back()->with('alert_error','Invalid Email, please try again !');
        }
        \DB::disconnect('tenant');
        \Config::set('database.connections.tenant.database',Helper::encryptor('decrypt',$request->database_name));
        \DB::setDefaultConnection('tenant');

        $extraEmail = new PatientMailers;
        $extraEmail->name = ucwords($request->name);
        $extraEmail->email = $request->email;
        $extraEmail->patient_id = \Helper::encryptor('decrypt',$request->patient_id);
        $extraEmail->save();
            
        \DB::disconnect('tenant');
        return back()->with('alert_success','Email added for the patient.');
    }
    
    public function extraEmailUpdate(Request $request)
    {
        if(empty($request->name) || empty($request->email) || empty($request->database_name)){
            return back()->with('alert_error','Type or Address was not supplied, please try again !');
        }
        if(!filter_var($request->email, FILTER_VALIDATE_EMAIL)){
            return back()->with('alert_error','Invalid Email, please try again !');
        }
        \DB::disconnect('tenant');
        \Config::set('database.connections.tenant.database',Helper::encryptor('decrypt',$request->database_name));
        \DB::setDefaultConnection('tenant');

        $extraEmail = PatientMailers::whereId(\Helper::encryptor('decrypt',$request->id))->first();
        $extraEmail->name = ucwords($request->name);
        $extraEmail->email = $request->email;
        $extraEmail->patient_id = \Helper::encryptor('decrypt',$request->patient_id);
        $extraEmail->save();
            
        \DB::disconnect('tenant');
        return back()->with('alert_success','Email updated for the patient.');
    }

    public function extraEmailUpdateStatus(Request $request)
    {
        if(empty($request->id) || empty($request->database_name)){
            return back()->with('alert_error','Email not found, please try again !');
        }
        \DB::disconnect('tenant');
        \Config::set('database.connections.tenant.database',Helper::encryptor('decrypt',$request->database_name));
        \DB::setDefaultConnection('tenant');

        $extraEmail = PatientMailers::whereId(\Helper::encryptor('decrypt',$request->id))->first();
        if($request->status == 1){
            $extraEmail->status = 'active';
        }else{
            $extraEmail->status = 'inactive';
        }

        $extraEmail->save();
        $state = 'statusChanged';

        \DB::disconnect('tenant');
        return $state;
    }

    public function updateDisposition(Request $request)
    {
        $res = false;

        if($request->ajax() && $request->id && $request->database_name){
            // connecting to centralconsole db
            $tenant_id = Subscriber::whereDatabaseName(Helper::encryptor('decrypt',$request->database_name))->pluck('id')->first();
            \DB::disconnect('tenant');
            // connecting to branch db
            \Config::set('database.connections.tenant.database',Helper::encryptor('decrypt',$request->database_name));
            \DB::setDefaultConnection('tenant');

            $id = Helper::encryptor('decrypt',$request->id);

            $lead = Lead::find($id);
            $patient = Patient::whereId($lead->patient_id)->get();

            if($lead){
                // Update leads table
                $leadData['status'] = $request->status;

                $date = null;
                if($request->date){
                    $date = date_format(new \DateTime($request->date),'Y-m-d');
                }

                if($request->status == 'Dropped'){
                    $leadData['dropped_reason'] = $caselosscomment = $request->caselosscomment;
                }else{
                    $caselosscomment = '-';
                }

                $lead->update($leadData);

                // Add to case disposition table
                $dispositionData = [
                    'tenant_id'        => $tenant_id,
                    'lead_id'          => $id,
                    'user_id'          => 1,
                    'status'           => $request->status,
                    'disposition_date' => $date,
                    'comment'          => $request->comment,
                    'caselosscomment'  => $caselosscomment
                ];

                if(!empty($request->followupdate)){
                    $dispositionData += [
                        'follow_up_datetime' => date_format(new \DateTime($request->followupdate),'Y-m-d H:i:s'),
                        'follow_up_comment'  => $request->followupcomment,
                    ];
                }

                $res = CaseDisposition::create($dispositionData);

                if($patient){
                    $lead = Lead::whereId($id)->first();
                    $patient = Patient::whereId($lead->patient_id)->first();

                    if(empty($patient->patient_id) && $request->status == 'Converted'){
                        $patientDetails = [];

                        $prefix = Settings::value('patient_id_prefix');
                        if($prefix){
                            $lastPatient = Patient::whereNotNull('patient_id')->orderBy(\DB::raw('length(patient_id),patient_id'))->get()->last();
                            if($lastPatient){
                                $lastID = substr($lastPatient->patient_id, strlen($prefix));
                            }else{
                                $lastID = 0;
                            }

                            $patientDetails['patient_id'] = $prefix.str_pad(intval($lastID + 1),strlen($lastID),0,STR_PAD_LEFT);
                        }
                        $patient->update($patientDetails);
                        $patientID = $patient->id;
                    }
                    if(empty($lead->episode_id) && $request->status == 'Converted'){
                        $caseDetails = [];
                        $caseDetails['episode_id'] = $patient->patient_id.str_pad(Lead::wherePatientId($patient->id)->count(),2,0,STR_PAD_LEFT);
                        $lead->update($caseDetails);
                    }
                    if($lead->aggregator_lead && $request->status == 'Dropped'){
                        AggregatorLeads::whereLeadId($lead->id)->update(['status' => 'Dropped']);
                    }
                    if($patient->email != null && $request->status == 'Converted' && filter_var($patient->email, FILTER_VALIDATE_EMAIL)){
                        $mail = Mail::to($patient->email);
                        $mail->send(new LeadConvertion($lead,$patient));
                    }
                    if($patient->email != null && $request->status == 'Closed' && filter_var($patient->email, FILTER_VALIDATE_EMAIL)){
                        $mail = Mail::to($patient->email);
                        $mail->send(new LeadCompletion($lead,$patient));
                    }
                    if($patient->email != null && $request->status == 'Dropped' && filter_var($patient->email, FILTER_VALIDATE_EMAIL)){
                        $mail = Mail::to($patient->email);
                        $mail->send(new LeadLoss($lead,$patient));
                    }
                }
            }
            \DB::disconnect('tenant');
        }

        return response()->json((bool) $res,200);
    }

    public function prevServiceRecords(Request $request, $id, $database_name)
    {
        \DB::disconnect('tenant');
        \Config::set('database.connections.tenant.database',Helper::encryptor('decrypt',$database_name));
        \DB::setDefaultConnection('tenant');

        $services = CaseServices::wherePatientId(\Helper::encryptor('decrypt',$id))
                    ->latest()->get()
                    ->groupBy(function($date) {
                        return \Carbon\Carbon::parse($date->created_at)->format('m-Y');
                    });
        $patient = Patient::whereId(\Helper::encryptor('decrypt',$id))->first();

        \DB::disconnect('tenant');
        return view('leads.prev-service-records', compact('services', 'patient'));
    }
}