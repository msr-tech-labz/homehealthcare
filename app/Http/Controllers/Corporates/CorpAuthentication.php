<?php
namespace App\Http\Controllers\Corporates;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Corporates\CorpUsers;
use App\Entities\Corporates\CorpProfile;

class CorpAuthentication
{
    public function login()
    {
        if(session('corporate.isLoggedIn')){
            return redirect('/corporate/dashboard');
        }
        return view('auth.corporate-login');
    }

    public function authenticate(Request $request)
    {
        $user = CorpUsers::validateCredentials($request->corporate_id, $request->email, $request->password);
        
        if($user === 'DisabledCorp'){
            return back()->with('alert_error','The Corporate Office is disabled. Please contact SHG.');
        }
        if($user === 'Duplicate'){
            return back()->with('alert_error','Multiple Records exists with same Email. Login as Admin to reset the accounts.');
        }
        if($user === 'NoSubscriber'){
            return back()->with('alert_error','No Corporate Head Office with the provided Corporate ID . Please contact SHG.');
        }
        if($user === 'DisabledUser'){
            return back()->with('alert_error','User has been disabled.');
        }
        if($user === 'InvalidPassword'){
            return back()->with('alert_error','Invalid Password. Please try again');
        }
        if($user === 'NoUserFound'){
            return back()->with('alert_error','No User found with the given Email.');
        }

        if($user){
            // Set User details
            session()->put('corporate.isLoggedIn',true);
            session()->put('corporate.user_id',\Helper::encryptor('encrypt',$user->id));
            session()->put('corporate.user_name',$user->full_name);
            session()->put('corporate.gender',$user->gender);

            $profile = CorpProfile::first();
            if($profile){
                session()->put('corporate.email',$profile->email);
                session()->put('corporate.name',$profile->name);
                session()->put('corporate.logo',$profile->logo);
                $CookieLogo = \Cookie::forever('corporateCookieLogo', $profile->logo);
                $CookieCorporateId = \Cookie::forever('corporateCookieCorporateId', session('corporate.corporate_id'));

                session()->put('corporate.address',$profile->address);
                session()->put('corporate.state',$profile->state);
                session()->put('corporate.landline_number',$profile->landline_number);
            }

            return redirect('/corporate/dashboard')->withCookie($CookieLogo)->withCookie($CookieCorporateId);
        } else {

            return back()->with('alert_error','Unable to Login at the moment. Please Try again');
        }
        
        return view('auth.corporate.login');
    }

    public function logout()
    {
        session()->forget('corporate.isLoggedIn');
        session()->flush();

        return redirect()->route('corporate.login');
    }
}
