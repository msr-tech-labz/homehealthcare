<?php
namespace App\Http\Controllers\Corporates;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Corporates\CorpUsers;

class ReportsController
{
    public function index()
    {
        return view('corporate.reports');
    }
}
