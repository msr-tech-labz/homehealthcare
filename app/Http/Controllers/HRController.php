<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\Helper;
use App\Entities\Leaves;
use App\Entities\LeaveApproval;

use App\Entities\User;
use App\Entities\Caregiver\Caregiver;
use App\Entities\Caregiver\ProfessionalDetails;
use App\Entities\Attendance;
use App\Entities\Schedule;
use App\Entities\Appraisal;
use App\Entities\DailyRevenueStorage; 
use App\Entities\WorkLogFeedback; 
use Validator;
use Hash;

use Carbon\Carbon;

class HRController extends Controller
{
    public function index()
    {
        $data['stats']['emp'] = Caregiver::branch()->active()->count();
        $data['stats']['leave'] = Leaves::branch()->pendingApproval()->count();
        $data['stats']['active'] = count(Schedule::select('caregiver_id')->where('schedule_date','=', date('Y-m-d'))->whereStatus('Pending')->distinct('caregiver_id')->get());
        $data['stats']['bench'] =  $data['stats']['emp'] - $data['stats']['active'] - (Leaves::where('date','=', date('Y-m-d'))->whereStatus('Approved')->count()) - (Attendance::where('date','=', date('Y-m-d'))->whereStatus('A')->count());

        return view('hr.index', $data);
    }

    public function leaves(Request $request){
        ini_set('memory_limit', '150M');
        $isCaregiver = ProfessionalDetails::whereCaregiverId(\Helper::encryptor('decrypt',session('admin_id')))->value('role') == 'Caregiver';

        $caregivers = Caregiver::select('id','first_name','middle_name','last_name','employee_id','profile_image')->active()
        ->with(['professional' => function($q){
            $q->addSelect('caregiver_id','role','specialization');
        },'professional.specializations' => function($q){
            $q->addSelect('id','specialization_name');
        }])->get();

        $managers = Caregiver::select('id','first_name','middle_name','last_name','employee_id','profile_image')->active()
        ->with(['professional' => function($q){
            $q->addSelect('caregiver_id','role','specialization');
        },'professional.specializations' => function($q){
            $q->addSelect('id','specialization_name');
        }])->whereHas('professional', function($q){
            $q->whereRole('Manager');
        })->get();
        
        $pendingLeaves = Leaves::whereStatus('Pending')->orderBy('created_at','desc')->paginate(50);
        $approvedLeaves = Leaves::whereStatus('Approved')->orderBy('created_at','desc')->paginate(50);
        $declinedLeaves = Leaves::whereStatus('Declined')->orderBy('updated_at','desc')->paginate(50);

        return view('hr.leaves.index', compact('pendingLeaves','approvedLeaves','declinedLeaves','caregivers','managers'));
    }

    public function getPendingLeavePaginator(Request $request){
        $pendingLeaves = Leaves::whereStatus('Pending');
        if(!empty($request->searchString)){
            $pendingLeaves = $pendingLeaves->whereHas('caregiver', function($q) use ($request){
                $q->where('first_name','like','%'.$request->searchString.'%');
            });
        }
        $pendingLeaves = $pendingLeaves->orderBy('created_at','desc')->paginate(50);        
        return view('partials.leaves.pending-leaves-paginator', compact('pendingLeaves'));
    }

    public function getApprovedLeavePaginator(Request $request){
        $approvedLeaves = Leaves::whereStatus('Approved');
        if(!empty($request->searchString)){
            $approvedLeaves = $approvedLeaves->whereHas('caregiver', function($q) use ($request){
                $q->where('first_name','like','%'.$request->searchString.'%');
            });
        }
        $approvedLeaves = $approvedLeaves->orderBy('created_at','desc')->paginate(50);
        return view('partials.leaves.approved-leaves-paginator', compact('approvedLeaves'));
    }

    public function getDeclinedLeavePaginator(Request $request){
        $declinedLeaves = Leaves::whereStatus('Declined');
        if(!empty($request->searchString)){
            $declinedLeaves = $declinedLeaves->whereHas('caregiver', function($q) use ($request){
                $q->where('first_name','like','%'.$request->searchString.'%');
            });
        }
        $declinedLeaves = $declinedLeaves->orderBy('updated_at','desc')->paginate(50);
        return view('partials.leaves.declined-leaves-paginator', compact('declinedLeaves'));
    }

    public function submitLeaveRequest(Request $request)
    {
        $leaveData = $request->all('type','reason');
        $leaveData['caregiver_id'] = \Helper::encryptor('decrypt',$request->caregiver_id);
        $leaveData['approver_id'] = \Helper::encryptor('decrypt',$request->approver_id);
        $leaveData['tenant_id'] = \Helper::getTenantID();
        $leaveData['user_id'] = \Helper::getUserID();
        $leaveData['status'] = 'Pending';
        $leaveData['created_at'] = $leaveData['created_at'] = date('Y-m-d H:i:s');
        $leaveData['branch_id'] = Caregiver::whereId(\Helper::encryptor('decrypt',$request->caregiver_id))->value('branch_id');
        $leavedates = array_map('trim', explode(',', $request->leavedates));

        foreach ($leavedates as $leavedate) {
            $leaveData['date'] = $leavedate;
            Leaves::insert($leaveData);
        }

        return back()->with('alert_success','Leave submitted successfully');
    }

    public function getBlockedDates(Request $request){
        $leaves = Leaves::whereCaregiverId(\Helper::encryptor('decrypt',$request->id))->where('status','!=','Declined')->get();
        $schedules = Schedule::whereCaregiverId(\Helper::encryptor('decrypt',$request->id))->where('status','Completed')->whereBetween('schedule_date',[date('Y-m-01' , strtotime('-1 months')),date('Y-m-t')])->get();

        $disabledDates = [];
        foreach ($leaves as $l) {
            array_push($disabledDates, $l->date->format('Y-m-d'));
        }

        foreach ($schedules as $s) {
                array_push($disabledDates, $s->schedule_date->format('Y-m-d'));
        }
        return response()->json($disabledDates,200);
    }

    public function processLeaveRequest(Request $request)
    {
        $leaveID = \Helper::encryptor('decrypt',$request->id);
        $leavesData = $request->all('type','reason','status');
        $leavesData['approver_id'] = Helper::encryptor('decrypt',$request->approver_id);
        $leavesData['approved_by'] = \Helper::getUserID();
        Leaves::find($leaveID)->update($leavesData);

        if((\Helper::getTenantId() == 266) && isset($request->ivrs_type)){
            //WARNING - Tenant ID in production mode to be set to 266 Only
            $type = $request->ivrs_type;
            \App\Helpers\IVRS::leaveResponse($type,$request);
        }

        if($request->status == 'Approved'){
            $leave = Leaves::find($leaveID);
            Schedule::whereScheduleDate($leave->date)
                    ->whereCaregiverId($leave->caregiver_id)
                    ->whereIn('status',['Pending','Cancelled'])
                    ->update(['caregiver_id' => 0]);
            Attendance::whereDate('date',$leave->date)
                    ->whereCaregiverId($leave->caregiver_id)
                    ->delete();
        }

        return back()->with('alert_info','Leave request process successfull');
    }

    public function leavesrevoke(Request $request)
    {
        $leave = Leaves::whereId(\Helper::encryptor('decrypt',$request->id))->first();
        $leave->update(['status' => 'Declined']);

        if((\Helper::getTenantId() == 266) && isset($request->ivrs_type)){
            //WARNING - Tenant ID in production mode to be set to 266 Only
            $type = 'ivrs_leave_event_status';
            
            $data['id'] = \Helper::encryptor('encrypt',$leave->id);
            $data['reason'] = $leave->reason;

            \App\Helpers\IVRS::leaveResponse($type,(object) $data);
        }
        return back()->with('message','The leave has been successfully Revoked with Rejection.');
    }

    public function leavesedit(Request $request)
    {
        $leavesData = $request->all('type','reason');
        $leavesData['approver_id'] = Helper::encryptor('decrypt',$request->approver_id);
        $leavesData['approved_by'] = \Helper::getUserID();
        Leaves::find(Helper::encryptor('decrypt',$request->leave_id))->update($leavesData);

        return back()->with('alert_info','The leave has been successfully edited.');
    }

    public function dailyAttendanceShifts(Request $request)
    {
        if($request->attendanceDate)
            $date = $request->attendanceDate;
        else
            $date = date('Y-m-d');

        $scheduledCaregivers = collect(
            \DB::table('caregivers')
            // ->join('users',function ($q){
            //     $q->on('caregivers.id','=','users.user_id')
            //     ->whereStatus(1);
            // })
            ->join('case_schedules',function ($join) use ($date){
                $join->on('caregivers.id', '=','case_schedules.caregiver_id')
                ->where('case_schedules.status','!=','Cancelled')
                ->whereScheduleDate($date);
            })
            ->join('patients','case_schedules.patient_id', '=', 'patients.id')
            ->leftJoin('employee_attendance',function ($join) use ($date){
                $join->on('caregivers.id', '=','employee_attendance.caregiver_id')
                ->on('employee_attendance.schedule_id','=','case_schedules.id')
                ->where('employee_attendance.date','=',$date)
                ->whereNull('employee_attendance.deleted_at');
            })
            ->leftJoin('master_services',function ($join){
                $join->on('case_schedules.service_required', '=','master_services.id');
            })
            ->select(
                'caregivers.id as caregiverId',
                'caregivers.employee_id as caregiverCode',
                'caregivers.first_name as caregiverFname',
                'caregivers.middle_name as caregiverMname',
                'caregivers.last_name as caregiverLname',
                'case_schedules.id as scheduleId',
                'case_schedules.schedule_date as scheduleDate',
                'case_schedules.start_time as sft',
                'case_schedules.end_time as stt',
                'master_services.service_name as serviceName',
                'patients.id as patientId',
                'patients.first_name as patientFname',
                'patients.last_name as patientLname',
                'employee_attendance.punch_in as eIn',
                'employee_attendance.punch_out as eOut',
                'employee_attendance.status as attStatus'
            )
            ->get()
        )->toArray();

        $excludedIds = array_pluck($scheduledCaregivers,'caregiverId');

        $nonScheduledCaregivers = collect(
            \DB::table('caregivers')
            ->join('users',function ($q){
                $q->on('caregivers.id','=','users.user_id')
                ->whereStatus(1)->where('caregivers.work_status','!=','Training');
            })
            ->whereNotIn('caregivers.id',$excludedIds)
            ->distinct('caregivers.id')
            ->join('caregiver_professional_details',function($q) use ($date){
                $q->on('caregivers.id', '=','caregiver_professional_details.caregiver_id')
                ->where('caregiver_professional_details.role','=','Caregiver')
                ->where(function($r) use ($date){
                    $r->whereDate('caregiver_professional_details.date_of_joining','<=',$date)
                    ->orWhereNull('caregiver_professional_details.date_of_joining');
                })
                ->where(function($r) use ($date){
                    $r->whereDate('caregiver_professional_details.resignation_date','>=',$date)
                    ->orWhereNull('caregiver_professional_details.resignation_date');
                });
            })
            ->leftJoin('employee_attendance',function ($join) use ($date){
                $join->on('caregivers.id', '=','employee_attendance.caregiver_id')
                ->where('employee_attendance.date','=',$date)
                ->whereNull('employee_attendance.deleted_at');
            })
            ->leftJoin('employee_leaves',function ($join) use ($date){
                $join->on('caregivers.id', '=','employee_leaves.caregiver_id')
                ->where('employee_leaves.date','=',$date)
                ->where('employee_leaves.status','!=','Declined')
                ->whereNull('employee_leaves.deleted_at');
            })
            ->select(
                'caregivers.id as caregiverId',
                'caregivers.employee_id as caregiverCode',
                'caregivers.first_name as caregiverFname',
                'caregivers.middle_name as caregiverMname',
                'caregivers.last_name as caregiverLname',
                'employee_attendance.status as attStatus',
                'employee_leaves.type as leaveType',
                'employee_leaves.status as leaveStatus'
            )
            ->get()
        )->toArray();

        $nonScheduledManagers = collect(
            \DB::table('caregivers')
            ->join('users',function ($q){
                $q->on('caregivers.id','=','users.user_id')
                ->whereStatus(1)->where('caregivers.work_status','!=','Training');
            })
            ->whereNotIn('caregivers.id',$excludedIds)
            ->distinct('caregivers.id')
            ->join('caregiver_professional_details',function($q){
                $q->on('caregivers.id', '=','caregiver_professional_details.caregiver_id')
                ->where('caregiver_professional_details.role','=','Manager');
            })
            ->leftJoin('employee_attendance',function ($join) use ($date){
                $join->on('caregivers.id', '=','employee_attendance.caregiver_id')
                ->where('employee_attendance.date','=',$date)
                ->whereNull('employee_attendance.deleted_at');
            })
            ->leftJoin('employee_leaves',function ($join) use ($date){
                $join->on('caregivers.id', '=','employee_leaves.caregiver_id')
                ->where('employee_leaves.date','=',$date)
                ->where('employee_leaves.status','!=','Declined')
                ->whereNull('employee_leaves.deleted_at');
            })
            ->select(
                'caregivers.id as caregiverId',
                'caregivers.employee_id as caregiverCode',
                'caregivers.first_name as caregiverFname',
                'caregivers.middle_name as caregiverMname',
                'caregivers.last_name as caregiverLname',
                'employee_attendance.status as attStatus',
                'employee_leaves.type as leaveType',
                'employee_leaves.status as leaveStatus'
            )
            ->get()
        )->toArray();
        
        $nonScheduledTrainees = collect(
            \DB::table('caregivers')
            ->join('users',function ($q){
                $q->on('caregivers.id','=','users.user_id')
                ->whereStatus(1);
            })
            ->where('caregivers.work_status','=','Training')
            ->whereNotIn('caregivers.id',$excludedIds)
            ->distinct('caregivers.id')
            ->join('caregiver_professional_details',function($q){
                $q->on('caregivers.id', '=','caregiver_professional_details.caregiver_id');
            })
            ->leftJoin('employee_attendance',function ($join) use ($date){
                $join->on('caregivers.id', '=','employee_attendance.caregiver_id')
                ->where('employee_attendance.date','=',$date)
                ->whereNull('employee_attendance.deleted_at');
            })
            ->leftJoin('employee_leaves',function ($join) use ($date){
                $join->on('caregivers.id', '=','employee_leaves.caregiver_id')
                ->where('employee_leaves.date','=',$date)
                ->where('employee_leaves.status','!=','Declined')
                ->whereNull('employee_leaves.deleted_at');
            })
            ->select(
                'caregivers.id as caregiverId',
                'caregivers.employee_id as caregiverCode',
                'caregivers.first_name as caregiverFname',
                'caregivers.middle_name as caregiverMname',
                'caregivers.last_name as caregiverLname',
                'employee_attendance.status as attStatus',
                'employee_leaves.type as leaveType',
                'employee_leaves.status as leaveStatus'
            )
            ->get()
        )->toArray();
        
        return view('hr.attendanceshifts', compact('scheduledCaregivers','nonScheduledCaregivers','nonScheduledManagers','nonScheduledTrainees','date'));
    }

    public function markAttendance(Request $request)
    {
        if(isset($request->shift)){
            // attendance for schedules staff
            $attendance = $request->except('_token');
            $att_data = [
                'caregiver_id' => (int) $request->cid,
                'schedule_id' => (int) $request->sid,
                'date' => $request->date,
                'punch_in' => Carbon::parse($request->intime)->format('h:i A'),
                'punch_out' => Carbon::parse($request->outtime)->format('h:i A'),
                'source' => 'Web',
                'marked_by' => (int) Helper::encryptor('decrypt',session('user_id')),
                'status' => 'P',
                'on_training' => 'No'
            ];
            $att = Attendance::whereDate('date',$request->date)->whereCaregiverId($request->cid)->whereScheduleId($request->sid)->first();
            if($att){
                $att->update($att_data);
            }else{
                Attendance::create($att_data);
            }

            $attBench = Attendance::whereDate('date',$request->date)->whereCaregiverId($request->cid)->whereNull('schedule_id')->get();
            if(count($attBench)){
                foreach ($attBench as $ab) {
                    $ab->delete();
                }
            }

            if(isset($request->sid)){
                $schedule = Schedule::whereId($request->sid)->first();
                if($schedule->status != 'Completed'){
                    $schedule->update(['status' => 'Completed']);

                    $drs = DailyRevenueStorage::whereDate('date',$schedule->schedule_date)->first();

                    if($drs){
                       $drs->flag = 'Unsorted';
                       $drs->updated_at = date('Y-m-d H:i:s');

                       $drs->save();
                    }
                }
            }
            $leave = Leaves::whereDate('date',$request->date)->whereCaregiverId($request->cid)->first();
            if($leave){
                $leave->update(['status' => 'Declined']);
            }
            return response()->json(200);
        } else if(isset($request->training)){
            // attendance for trainees
            $attendance = $request->except('_token');
            $att_data = [
                'caregiver_id' => (int) $request->cid,
                'date' => $request->date,
                'source' => 'Web',
                'marked_by' => (int) Helper::encryptor('decrypt',session('user_id')),
                'status' => $request->status,
                'on_training' => 'Yes'
            ];
            $att = Attendance::where('date','=',$request->date)->whereCaregiverId($request->cid)->first();
            if($att){
                $att->update($att_data);
            }else{
                Attendance::create($att_data);
            }
            return response()->json(200);
        } else {
            // attendance for Non-scheduled staff & Managers
            $attendance = $request->except('_token','attendance_date','DataTables_Table_0_length','DataTables_Table_1_length','DataTables_Table_2_length');
            $date = $request->attendance_date;
            foreach($attendance as $key => $a){
                $empID = explode("_",$key)[1];
                $att_data = [
                    'caregiver_id' => (int) $empID,
                    'date' => $date,
                    'source' => 'Web',
                    'marked_by' => (int) Helper::encryptor('decrypt',session('user_id')),
                    'status' => $a,
                    'on_training' => 'No'
                ];
                $att = Attendance::where('date','=',$date)->whereCaregiverId($empID)->first();
                if($att){
                    $att->update($att_data);
                }else{
                    Attendance::create($att_data);
                }
            }
            return redirect()->route('attendanceShifts.index',['attendanceDate' => $date]);
        }
    }

    public function appraisalsView(Request $request)
    {
        if($request->filter_month && $request->filter_year){
            $month = $request->filter_month;
            $year = $request->filter_year;
        }else{
            $month = date('m');
            $year = date('Y');
        }
        $caregivers = collect();
        $employees = Caregiver::select('id','employee_id','first_name','middle_name','last_name')->get();
        foreach ($employees as $srno => $employee){
            $star = Appraisal::whereCaregiverId($employee->id)->whereDate('date',$year.'-'.$month.'-01')
            ->whereNull('deleted_at')
            ->pluck('star')->first();

            $caregivers->push([
                'srno' => ++$srno,
                'id' => $employee->id,
                'employee_id' => $employee->employee_id,
                'first_name' => $employee->first_name,
                'middle_name' => $employee->middle_name,
                'last_name' => $employee->last_name,
                'star' => isset($star)?$star:0,
                'warning' => $employee->professional->warning,
                'warning_1' => $employee->professional->warning_date_one,
                'warning_2' => $employee->professional->warning_date_two,
                'warning_3' => $employee->professional->warning_date_three
            ]);
        }
        return view('hr.appraisals', compact('caregivers','month','year'));
    }

    public function appraisalsMarking(Request $request)
    {
        $cid = Helper::encryptor('decrypt',$request->cid);
        if(isset($request->warning)){
            $professionalDetails = ProfessionalDetails::whereCaregiverId($cid)->first();
            $warning = $professionalDetails->warning+1;
            $professionalDetails->update(['warning' => (int) $warning]);

            if($warning == 1){
                $professionalDetails->update(['warning_date_one' => date('Y-m-d H:i:s')]);
            }elseif($warning == 2){
                $professionalDetails->update(['warning_date_two' => date('Y-m-d H:i:s')]);
            }else{
                $professionalDetails->update(['warning_date_three' => date('Y-m-d H:i:s')]);
            }
        }
        elseif(isset($request->star)){
            if($request->status == '1'){
                $appraisal_data = [
                    'tenant_id' => Helper::getTenantID(),
                    'caregiver_id' => (int) $cid,
                    'star' => $request->status,
                    'date' => $request->date,
                    'marked_by' => (int) Helper::encryptor('decrypt',session('user_id'))
                ];
                Appraisal::create($appraisal_data);
            }else{
                $appraisal = Appraisal::whereDate('date',$request->date)->whereCaregiverId($cid)->first();
                if($appraisal){
                    $appraisal->delete();
                }
            }
        }
        return response()->json(200);
    }

    public function customerFeedback(Request $request)
    {
        if($request->filter_month){
           $feedbacks = WorkLogFeedback::whereMonth('created_at','=',$request->filter_month)->whereYear('created_at', '=', $request->filter_year)->get();
        }else{
            $feedbacks = WorkLogFeedback::get();
        }
        
        return view('hr.customerfeedback.index',compact('feedbacks'));
    }

    public function customerFeedbackActionSave(Request $request)
    {
        if($request->feedback_id){
            $data = [
                'status' => $request->feedback_status,
                'action' => $request->fedback_action,
                'action_by' => (int) Helper::encryptor('decrypt',session('user_id'))
            ];
            WorkLogFeedback::find($request->feedback_id)->update($data);
        }

        return back()->with('alert_success','Action submitted successfully');
    }
}
