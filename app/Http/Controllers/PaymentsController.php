<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\Helper;
use PaymentGateway\Atom\Facades\Atompay;
use Appnings\Payment\Facades\Payment;

use App\Entities\Invoice;
use App\Entities\Patient;
use App\Entities\CasePayment;
use App\Entities\Profile;
use App\Entities\Settings;
use App\Entities\Receipt;
use App\Entities\Console\CCAvenueTransactionLog;

class PaymentsController extends Controller
{
    public function __construct()
    {
        \DB::setDefaultConnection('tenant');
    }

    public function subscriptionPayment(Request $request)
    {
        return view('subscription-payment');
    }

    public function getCompanyInfo(Request $request)
    {
        $result = ['result' => false];
        if($request->isMethod('post') && $request->has('orgCode')){
            $subscriber = \App\Entities\Console\Subscriber::whereSubscriberId($request->orgCode)->first();
            if($subscriber){
                self::setTenantConnection($subscriber->id);

                $result['result'] = true;
                $result['profile'] = collect(\App\Entities\Profile::first())
                                        ->except('id','tenant_id','created_at','updated_at','deleted_at');
            }
        }
        return response()->json($result, 200);
    }

    public function responseview(Request $request)
    {
        $invoice = Invoice::find($request->id);

        return view('plugin.paymentgateway.response',compact('invoice'));
    }

    /*
    | Payment Gateway request
    */
    public function invoice(Request $request)
    {
        $tenantID  = isset($request->t_id)?\Helper::encryptor('decrypt',$request->t_id):0;
        $branchID  = isset($request->b_id)?\Helper::encryptor('decrypt',$request->b_id):0;
        $invoiceID = isset($request->inv_id)?\Helper::encryptor('decrypt',$request->inv_id):0;
        $invoiceNo = isset($request->inv_no)?\Helper::encryptor('decrypt',$request->inv_no):0;
        $patientID = isset($request->pid)?\Helper::encryptor('decrypt',$request->pid):0;
        $type      = isset($request->type)?\Helper::encryptor('decrypt',$request->type):0;

        if($tenantID){
            self::setTenantConnection($tenantID);
        }else{
            return false;
        }

        if($type != 0 && $type == 'SOA'){
            $amount = \Helper::encryptor('decrypt',$request->amt);

            // Generate Invoice Number, Date and Status
            $billingSettings = Settings::first();
            $invoiceStartNo = $billingSettings->invoice_start_no;
            $invoicePrefix = $billingSettings->invoice_inits;
            $billsCount = Invoice::whereInvoiceFor('SOA')->count();
            if($billsCount == 0)
                $invoiceNo = $invoicePrefix."S-".$invoiceStartNo;
            else
                $invoiceNo = $invoicePrefix."S-".($invoiceStartNo + $billsCount);

            $data = [
                'tenant_id' => $tenantID,
                'patient_id' => $patientID,
                'branch_id' => 1,
                'invoice_type' => 'Service',
                'invoice_for' => 'SOA',
                'invoice_no' => $invoiceNo,
                'invoice_date' => date('Y-m-d'),
                'invoice_amount' => $amount,
                'status' => 'Pending',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];

            $invoiceID = Invoice::insertGetId($data);
        }

        // Check if its already paid
        $invoice = Invoice::withoutGlobalScopes()->whereId($invoiceID)->first();
        if($invoice){
            if($invoice->status == 'Paid'){
                $profile = Profile::withoutGlobalScopes()->whereTenantId($tenantID)->first();

                return view('plugin.paymentgateway.paid',compact('invoice','profile'));
            }

            if($invoice->status == 'Partial'){
                $profile = Profile::withoutGlobalScopes()->whereTenantId($tenantID)->first();

                return view('plugin.paymentgateway.partial',compact('invoice','profile'));
            }

            if($invoice->status == 'Adjusted'){
                $profile = Profile::withoutGlobalScopes()->whereTenantId($tenantID)->first();

                return view('plugin.paymentgateway.adjusted',compact('invoice','profile'));
            }

            if($invoice->total_amount == $invoice->amount_paid){
                $profile = Profile::withoutGlobalScopes()->whereTenantId($tenantID)->first();

                return view('plugin.paymentgateway.paid',compact('invoice','profile'));
            }
        }

        $parameters = [];

        if($invoiceID && $invoiceNo && $patientID){
            $invoice = Invoice::withoutGlobalScopes()->whereId($invoiceID)
                                ->whereInvoiceNo($invoiceNo)
                                ->wherePatientId($patientID)
                                ->first();
            $totalInvoiceAmount = $invoice->total_amount;
            // Get Patient and Amount Details
            $patient = Patient::withoutGlobalScopes()->whereId($patientID)->first();
            if(!filter_var($patient->email, FILTER_VALIDATE_EMAIL))
                return redirect()->route('billing.index')->with('alert_error','Could not process payment gateway request as email address is not valid!');

            // Get Payment gateway settings
            $plugin = \App\Entities\Masters\PluginSettings::whereTenantId($tenantID)
                            ->whereCategory('Payment Gateway')->whereIsDefault(1)->first();
            if(!$plugin){
                return back()->with('alert_error','Oops! Something went wrong. Unable to process request.');
            }

            if($plugin->plugin_name == 'pg_ccavenue'){
                $return = $this->prepareCcavenuePlugin($request, $patient, $patientID, $invoiceID, $tenantID, $totalInvoiceAmount);

                return $return;
            }

            if($plugin->plugin_name == 'pg_atom'){
                $return = $this->prepareAtomPayPlugin($settings, $patient, $invoice, $patientID, $invoiceID, $tenantID);

                return redirect($return);
            }
        }
    }

    /*
    | Payment Gateway response
    */
    public function response(Request $request, $tenantID)
    {
        if($tenantID > 0){
            self::setTenantConnection($tenantID);
        }else{
            return false;
        }

        // Get Payment gateway settings
        $this->setPaymentGatewaySettings($tenantID);

        // For default Gateway
        $response = Payment::response($request);

        if(empty($response))
            return back();

        $transactionLog = CCAvenueTransactionLog::whereOrderId($response['order_id'])->first();
        if(!$transactionLog){
            return false;
        }
        $transactionLog->update([
            'tracking_id' => isset($response['tracking_id'])?$response['tracking_id']:null,
            'response_params' => json_encode($response),
            'response_timestamp' => date('Y-m-d H:i:s'),
            'payment_mode' => isset($response['payment_mode'])?$response['payment_mode']:null,
            'amount' => isset($response['amount'])?$response['amount']:null,
            'status' => isset($response['order_status'])?$response['order_status']:'Failed'
        ]);

        $invoice = Invoice::withoutGlobalScopes()->whereId($transactionLog->invoice_id)->first();

        if($response['order_status'] == 'Success')
            $invoice->update(['amount_paid' => $response['amount'], 'status' => 'Paid']);

        // Update case_payments table
        $checkPayment = CasePayment::whereMerchantTxnId($response['tracking_id'])->count();
        if($checkPayment == 0 && $response['order_status'] == 'Success'){
            CasePayment::create([
                'tenant_id' => $tenantID,
                'branch_id' => 1,
                'invoice_id' => $invoice->id,
                'patient_id' => $invoice->patient_id,
                'merchant_txn_id' => $response['tracking_id'],
                'payment_mode' => $response['payment_mode'],
                'invoice_amount' => $invoice->total_amount,
                'total_amount' => $response['amount'],
                'pg_response' => json_encode($response),
                'status' => 'Success'
            ]);
        }

        // Update case_receipts table
        $receipt = Receipt::whereReferenceNo($response['tracking_id'])->count();
        if($receipt == 0 && $response['order_status'] == 'Success'){
            
            $receiptPrefix = Settings::first()->value('receipt_inits');
            $receiptStart  = Settings::first()->value('receipt_start_no');
            $receiptCount  = Receipt::count() + 1;

            Receipt::create([
                'tenant_id' => $tenantID,
                'patient_id' => $invoice->patient_id,
                'item_id' => $invoice->id,
                'receipt_type' => 'Payment',
                'payment_mode' => $response['payment_mode'],
                'reference_no' => $response['tracking_id'],
                'receipt_no' => $receiptPrefix . ($receiptStart + $receiptCount),
                'receipt_date' => date('Y-m-d H:i:s'),
                'receipt_amount' => $response['amount'],
            ]);
        }

        // TODO: Update statement of account table and other (if required)

        // Send Receipt to customer's email
        $profile = Profile::first();
        if($profile){
            $data['profile'] = $profile;
            $data['invoice'] = $invoice;
            $data['response'] = $response;

            // TODO: Handle the receipt PDF generation

            // $pdf = \PDF::loadView('billings.receiptpdf', compact(['payment','invoice','profile']))
            //             ->setPaper('a4', 'landscape')->setWarnings(false);
            $pdf = '';
            $data['patientEmail'] = $invoice->patient->email;
            //$data['patientEmail'] = 'suhas@apnacare.in';

            if(filter_var($data['patientEmail'], FILTER_VALIDATE_EMAIL)){
                \Mail::send('mails.receipt', $data, function($message) use($profile, $data, $pdf){
                    $message->from('no-reply@smarthealthconnect.com', $profile->organization_name);
                    if(!empty($profile->email) && !is_null($profile->email)){
                        if(filter_var($profile->email, FILTER_VALIDATE_EMAIL)){
                            $message->replyTo($profile->email)
                            ->bcc($profile->email);
                        }
                    }

                    $message->to($data['patientEmail'])
                            ->subject('Payment Receipt - '.$data['invoice']->invoice_no);

                    // TODO: Attach the PDF to email
                    //$message->attachData($pdf->output(), "Payment-Receipt-".$data['invoice']->invoice_no.".pdf");
                });
            }
        }

        return view('plugin.paymentgateway.response', compact('profile','payment','invoice','response'));
    }

    public function prepareCcavenuePlugin($request, $patient, $patientID, $invoiceID, $tenantID, $totalInvoiceAmount)
    {
        $timestamp = date('Ymdhis');
        $orderID = $tenantID.'_'.$patientID.'_'.$invoiceID.'_'.$timestamp;
        $parameters = [
            'tid' => $timestamp,
            'order_id' => $orderID,
            'billing_name' => $patient->full_name,
            'billing_email' => $patient->email,
            'billing_address' => $patient->street_address.' '.$patient->area,
            'billing_city' => $patient->city,
            'billing_state' => $patient->state,
            'billing_zip' => $patient->zipcode,
            'billing_country' =>'India',
            'billing_tel' => $patient->contact_number,
            'amount' => $totalInvoiceAmount,
            'merchant_param1' => $tenantID,
            'merchant_param2' => $patientID,
            'merchant_param3' => $invoiceID,
        ];

        // Save the transaction log to db
        CCAvenueTransactionLog::create([
            'tenant_id' => $tenantID,
            'invoice_id' => $invoiceID,
            'transaction_timestamp' => date('Y-m-d H:i:s'),
            'ip_address' => $request->ip(),
            'user_agent' => $request->userAgent(),
            'order_id' => $orderID,
            'request_params' => json_encode($parameters),
            'status' => 'Pending'
        ]);

        // Set payment gatew
        $this->setPaymentGatewaySettings($tenantID);

        $order = Payment::prepare($parameters);

        return Payment::process($order);
    }

    public function setPaymentGatewaySettings($tenantID)
    {
        $plugin = \App\Entities\Masters\PluginSettings::whereTenantId($tenantID)
                        ->whereCategory('Payment Gateway')->whereIsDefault(1)->first();
        if(!$plugin){
            return back()->with('alert_error','Oops! Something went wrong. Unable to process request.');
        }

        $settings = json_decode($plugin->settings);

        // Set the configuration
        \Config::set('payment.testMode',false);
        \Config::set('payment.ccavenue.merchantId',$settings->merchant_id);
        \Config::set('payment.ccavenue.accessCode',$settings->access_code);
        \Config::set('payment.ccavenue.workingKey',$settings->working_key);
        \Config::set('payment.ccavenue.redirectUrl',\Config::get('payment.ccavenue.redirectUrl').'/'.$tenantID);
        \Config::set('payment.ccavenue.cancelUrl',\Config::get('payment.ccavenue.cancelUrl').'/'.$tenantID);
    }

    public function prepareAtomPayPlugin($settings, $patient, $invoice, $patientID, $invoiceID, $tenantID)
    {
        $parameters = [
            'udf1' => $patient->full_name,
            'udf2' => $patient->email,
            'udf3' => $patient->contact_number,
            'email' => $patient->email,
            'phone' => $patient->contact_number,
            'Amount' => $invoice->invoice_amount,
            'patient_id' => $patientID,
            'pid' => base64_encode($patientID),
            'inv_id' => $invoiceID,
            'tenant_id' => $tenantID,
            'branch_id' => 1,
        ];

        // Set the configuration
        \Config::set('Atompay.testMode',false);
        \Config::set('Atompay.Atom.ATOM_LOGIN',$settings->login_id);
        \Config::set('Atompay.Atom.ATOM_PASSWORD',$settings->password);
        \Config::set('Atompay.Atom.ATOM_PRO_ID',$settings->product_id);
        \Config::set('Atompay.Atom.ATOM_CLIENT_CODE',base64_encode(env('ATOM_CLIENT_CODE', '007')));

        return Atompay::prepare($parameters);
    }
}
