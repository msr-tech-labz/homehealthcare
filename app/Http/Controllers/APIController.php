<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Helpers\Helper;

use App\Entities\User;
use App\Entities\Profile;
use App\Entities\Caregiver\Caregiver;
use App\Entities\Leaves;
use App\Entities\Patient;
use App\Entities\FCMToken;
use App\Entities\Lead;
use App\Entities\Worklog;
use App\Entities\WorkLogFeedback;
use App\Entities\Caregiver\Verification;
use App\Entities\Caregiver\ProfessionalDetails;
use App\Entities\Caregiver\AccountDetails;

use App\Entities\Invoice;
use App\Entities\Settings;

class APIController extends Controller
{
    public function index()
    {
        return 'API v2.0';
    }

    public function checkVersion(Request $request)
    {
        $updateRequired = false;
        if($request->has('version')){
            if(intval($request->version) < intval(\Config::get('mobileapp.version'))){
                $updateRequired = true;
                $updateFilePath = asset('uploads/apk/app-release-'.\Config::get('mobileapp.version').'.apk');
            }
        }

        $data['status_code'] = 200;
        $data['result']['version'] = \Config::get('mobileapp.version');
        $data['result']['update_required'] = $updateRequired;
        if($updateRequired){
            $data['result']['update_file'] = $updateFilePath;
        }

        return response()->json($data, 200);
    }

    public function updateRegistrationToken(Request $request)
    {
        $userID = $request->user_id;
        $userType = $request->user_type;
        $token = $request->token;

        $res = false;

        if($userID && $userType && $token){
            // Check if entry exists
            $fcmToken = FCMToken::whereUserId($userID)->whereUserType($userType)->first();

            if($fcmToken){
                $fcmToken->token = $token;
                $res = $fcmToken->save();
            }else{
                $res = FCMToken::create(['user_id' => $userID, 'user_type' => $userType, 'token' => $token]);
            }
        }

        $data['status_code'] = 200;
        if($res)
            $data['result'] = (bool) $res;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function sendEmergencyAlert(Request $request)
    {
        $caregiverID = $request->caregiver_id;
        $res = false;

        if($caregiverID){
            $caregiver = Caregiver::withoutGlobalScopes()->whereId($caregiverID)->first();
            if($caregiver){
                $tenantID = $caregiver->tenant_id;
                $userIDs = User::withoutGlobalScopes()->whereTenantId($tenantID)->whereUserType('Provider')->get()->pluck('id')->toArray();
                $tokensRes = FCMToken::whereIn('user_id',$userIDs)->whereUserType('Provider')->get();
                if($tokensRes){
                    $res = true;
                    $tokens = $tokensRes->pluck('token')->toArray();

                    $profilePic = '';
                    if($caregiver->profile_image && file_exists(public_path('uploads/caregivers/'.$caregiver->profile_image))){
                        //$profilePic = 'http://192.168.1.109/apnacare_saas_nitish/public/uploads/caregivers/'.$caregiver->profile_image;
                        $profilePic = url('uploads/caregivers/'.$caregiver->profile_image);
                    }

                    $pdata = [
			'type' => 'emergency',
			'message' => 'One of your caregiver is in emergency',
                        'name' => $caregiver->first_name.' '.$caregiver->middle_name.' '.$caregiver->last_name,
                        'phone' => $caregiver->mobile_number,
                        'profile_url' => $profilePic,
                        'location' => '',
                    ];

                    $response = \App\Helpers\PushMessaging::send($tokens, 'Emergency Alert - Practitioner','One of your caregiver is in emergency',$pdata);
                }
            }
        }

        $data['status_code'] = 200;
        if($res){
            $data['result'] = (bool) $res;
            $data['response'] = $response;
        }
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function getProfile(Request $request)
    {
        $id = $request->user_id;
        $type = $request->type;
        $profile = [];
        $basicDetails = [];
        $professionalDetails = [];
        $bankDetails = [];

        if($id && $type){
            switch ($type) {
                case 'Aggregator':
                case 'Provider':
                    $user = User::withoutGlobalScopes()->whereId($id)->first();
                    if($user){
                        $provider = Profile::withoutGlobalScopes()->whereTenantId($user->tenant_id)->first();
                        if(count($provider)){
                            $profile = [
                                'id' => Helper::str($provider->id),
                                'tenant_id' => Helper::str($provider->tenant_id),
                                'organization_name' => Helper::str($provider->organization_name),
                                'short_name' => Helper::str($provider->organization_short_name),
                                'contact_person' => Helper::str($provider->contact_person),
                                'mobile_number' => Helper::str($provider->phone_number),
                                'email_address' => Helper::str($provider->email),
                                'street_address' => Helper::str($provider->organization_address),
                                'area' => Helper::str($provider->organization_area),
                                'city' => Helper::str($provider->organization_city),
                                'state' => Helper::str($provider->organization_state),
                                'country' => Helper::str($provider->organization_country),
                            ];
                        }
                    }

                    break;

                case 'Caregiver':
                    $user = User::withoutGlobalScopes()->whereId($id)->first();
                    if($user){
                        $caregiver = Caregiver::withoutGlobalScopes()->whereId($user->user_id)->first();
                        if(count($caregiver)){
                            $profilePic = '';
                            if($caregiver->profile_image && file_exists(public_path('uploads/caregivers/'.$caregiver->profile_image))){
                                //$profilePic = 'http://192.168.1.109/apnacare_saas_nitish/public/uploads/caregivers/'.$caregiver->profile_image;
                                $profilePic = url('uploads/caregivers/'.$caregiver->profile_image);
                            }
                            $profile = [
                                'id' => Helper::str($caregiver->id),
                                'tenant_id' => Helper::str($caregiver->tenant_id),
                                'first_name' => Helper::str($caregiver->first_name),
                                'middle_name' => Helper::str($caregiver->middle_name),
                                'last_name' => Helper::str($caregiver->last_name),
                                'gender' => Helper::str($caregiver->gender),
                                'blood_group' => Helper::str($caregiver->blood_group),
                                'marital_status' => Helper::str($caregiver->marital_status),
                                'mobile_number' => Helper::str($caregiver->mobile_number),
                                'email' => Helper::str($caregiver->email),
                                'street_address' => Helper::str($caregiver->current_address),
                                'area' => Helper::str($caregiver->current_area),
                                'city' => Helper::str($caregiver->current_city),
                                'state' => Helper::str($caregiver->current_state),
                                'country' => Helper::str($caregiver->current_country),
                                'specialization' => isset($caregiver->professional)?Helper::str($caregiver->professional->specialization):'-',
                                'specialization_name' => isset($caregiver->professional)?Helper::str($caregiver->professional->specializations->specialization_name):'-',
                                'qualification' => isset($caregiver->professional)?Helper::str($caregiver->professional->qualification):'-',
                                'college_name' => isset($caregiver->professional)?Helper::str($caregiver->professional->college_name):'-',
                                'experience' => isset($caregiver->professional)?Helper::str($caregiver->professional->experience):'-',
                                'pan_number' => isset($caregiver->account)?Helper::str($caregiver->account->pan_number):'-',
                                'account_name' => isset($caregiver->account)?Helper::str($caregiver->account->account_name):'-',
                                'account_number' => isset($caregiver->account)?Helper::str($caregiver->account->account_number):'-',
                                'bank_name' => isset($caregiver->account)?Helper::str($caregiver->account->bank_name):'-',
                                'bank_branch' => isset($caregiver->account)?Helper::str($caregiver->account->bank_branch):'-',
                                'ifsc_code' => isset($caregiver->account)?Helper::str($caregiver->account->ifsc_code):'-',
                                'profile_url' => $profilePic,
                            ];
                        }
                    }

                    break;
            }
        }

        $data['status_code'] = 200;
        if($profile)
            $data['result'] = $profile;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function getCaregivers(Request $request)
    {
        $id = $request->user_id;
        $caregivers = [];

        if($id){
            $caregiverResult = Caregiver::withoutGlobalScopes()->whereTenantId($id)->get();
            if(count($caregiverResult)){
                foreach ($caregiverResult as $c) {
                    $profilePic = '';
                    if($c->profile_image && file_exists(public_path('uploads/caregivers/'.$c->profile_image))){
                        //$profilePic = 'http://192.168.1.109/apnacare_saas_nitish/public/uploads/caregivers/'.$c->profile_image;
                        $profilePic = url('uploads/caregivers/'.$c->profile_image);
                    }
                    $caregivers[] = [
                        'id' => Helper::str($c->id),
                        'tenant_id' => Helper::str($c->tenant_id),
                        'employee_id' => Helper::str($c->employee_id),
                        'first_name' => Helper::str($c->first_name),
                        'middle_name' => Helper::str($c->middle_name),
                        'last_name' => Helper::str($c->last_name),
                        'gender' => Helper::str($c->gender),
                        'date_of_birth' => Helper::getDate($c->date_of_birth, true),
                        'blood_group' => Helper::str($c->blood_group),
                        'marital_status' => Helper::str($c->marital_status),
                        'mobile_number' => Helper::str($c->mobile_number),
                        'email' => Helper::str($c->email),
                        'current_address' => Helper::str($c->current_address),
                        'current_city' => Helper::str($c->current_city),
                        'current_state' => Helper::str($c->current_state),
                        'current_country' => Helper::str($c->current_country),
                        'work_status' => Helper::str($c->work_status),
                        'profile_image' => Helper::str($c->profile_image),
                        'specialization_id' => isset($c->professional)?Helper::str($c->professional->specialization):0,
                        'specialization_name' => (isset($c->professional) && isset($c->professional->specializations))?Helper::str($c->professional->specializations->specialization_name):'',
                        'qualification' => isset($c->professional)?Helper::str($c->professional->qualification):'',
                        'college_name' => isset($c->professional)?Helper::str($c->professional->college_name):'-',
                        'experience' => isset($c->professional)?intval(Helper::str($c->professional->experience)):'',
                        'languages_known' => isset($c->professional)?Helper::str($c->professional->languages_known):'',
                        'profile_url' => $profilePic
                    ];
                }
            }
        }

        $data['status_code'] = 200;
        if($caregivers)
            $data['result']['caregivers'] = $caregivers;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function updateProfile(Request $request)
    {
        $id = $request->input('id');
        $type = $request->input('type');
        $profileData = $request->input('profile');
        $res = false;

        if($id && $type){
            if($type == 'Provider'){
                $res = Profile::withoutGlobalScopes()->whereId($id)->update($profileData);
            }

            if($type == 'Caregiver'){
                // Update Basic details
                $res = Caregiver::withoutGlobalScopes()->whereId($id)->update($profileData['basic']);

                // Update Professional details
                $res = ProfessionalDetails::withoutGlobalScopes()->whereCaregiverId($id)->update($profileData['professional']);

                // Update account details
                $res = AccountDetails::withoutGlobalScopes()->whereId($id)->update($profileData['account']);
            }
        }

        $data['status_code'] = 200;
        if($id && $res)
            $data['result'] = $res;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function getLeaves(Request $request)
    {
        $id = $request->user_id;
        $type = $request->type;
        $leaves = [];
        $leavesResult = [];

        $approval = false;

        if($id && $type){
            switch ($type) {
                case 'myleaves':
                    $leavesResult = Leaves::withoutGlobalScopes()->whereCaregiverId($id)->get();
                    break;

                case 'approvals':
                    $empIDs = [];
                    $employees = Caregiver::withoutGlobalScopes()
                                            ->whereHas('professional', function($q) use ($id){
                                                $q->whereManager($id);
                                            })
                                            ->get();
                    if($employees && count($employees)){
                        foreach ($employees as $e) {
                            $empIDs[] = $e->id;
                        }

                        if($empIDs){
                            $approval = true;
                            $leavesResult = Leaves::withoutGlobalScopes()->whereIn('caregiver_id',$empIDs)->get();
                        }
                    }
                    break;
            }

            if(count($leavesResult)){
                foreach ($leavesResult as $l) {
                    $leaves[] = [
                        'id' => Helper::str($l->id),
                        'tenant_id' => Helper::str($l->tenant_id),
                        'approver_id' => $approval?$id:0,
                        'caregiver_id' => Helper::str($l->caregiver_id),
                        'name' => Helper::str($l->name),
                        'date_received' => Helper::str($l->date_received),
                        'date_from' => Helper::str($l->date_from),
                        'date_to' => Helper::str($l->date_to),
                        'reason' => Helper::str($l->reason),
                        'type' => Helper::str($l->type),
                        'status' => Helper::str($l->status),
                        'created_at' => Helper::getDate($l->created_at),
                    ];
                }
            }
        }

        $data['status_code'] = 200;
        if($leaves)
            $data['result']['leaves'] = $leaves;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function addLeave(Request $request)
    {
        $leaveData = $request->input('leave');
        $result = false;
        $id = 0;

        if($leaveData){
            $leaveData['created_at'] = date('Y-m-d H:i:s');
            $id = $leaveData['id'];
            unset($leaveData['id']);
            $result = Leaves::withoutGlobalScopes()->insert($leaveData);
        }

        $data['status_code'] = 200;
        if($id > 0 && $result)
            $data['result'] = $id;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function getPatients(Request $request)
    {
        $patientsList = [];

        if($request->email){
            $patients = Patient::withoutGlobalScopes()->whereEnquirerEmail($request->email)->get();
            if($patients){
                foreach ($patients as $p) {
                    $patientsList[] = [
                        'id' => Helper::str($p->id),
                        'first_name' => Helper::str($p->first_name),
                        'last_name' => Helper::str($p->last_name),
                        'gender' => Helper::str($p->id),
                        'patient_age' => Helper::str($p->patient_age),
                        'patient_weight' => Helper::str($p->patient_weight),
                        'street_address' => Helper::str($p->street_address),
                        'area' => Helper::str($p->area),
                        'city' => Helper::str($p->city),
                        'zipcode' => Helper::str($p->zipcode),
                        'state' => Helper::str($p->state),
                        'country' => Helper::str($p->country),
                        'street_address' => Helper::str($p->street_address)
                    ];
                }
            }
        }

        $data['status_code'] = 200;
        if($patientsList)
            $data['result'] = $patientsList;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function newCustomerRequest(Request $request)
    {
        $requestData = $request->input('request');
        $result = false;
        $id = 0;

        if($requestData){
            $requestData['created_at'] = date('Y-m-d H:i:s');
            $requestData['source'] = 'Customer App';
            $requestData['status'] = 'Not Checked';

            $id = $requestData['id'];
            unset($requestData['id']);
            $result = Lead::withoutGlobalScopes()->insert($requestData);
        }

        $data['status_code'] = 200;
        if($id > 0 && $result)
            $data['result'] = $id;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function getCustomerProfile(Request $request)
    {
        $email = $request->input('email');
        $profile = [];

        if($email){
            $res = Patient::withoutGlobalScopes()->whereEnquirerEmail($email)->first();
            if($res){
                $profile = [
                    'name' => $res->enquirer_name,
                    'phone' => $res->enquirer_phone,
                    'email' => $res->enquirer_email,
                ];
            }
        }

        $data['status_code'] = 200;
        if($profile)
            $data['result'] = $profile;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function updatePatient(Request $request)
    {
        $patientData = $request->input('patient');
        $result = false;
        $id = 0;

        if($patientData){
            $patientData['updated_at'] = date('Y-m-d H:i:s');

            $id = $patientData['id'];
            unset($patientData['id']);
            $result = Patient::withoutGlobalScopes()->whereId($id)->update($patientData);
        }

        $data['status_code'] = 200;
        if($id > 0 && $result)
            $data['result'] = $id;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function worklogFeedback(Request $request)
    {
        $worklogID = $request->worklog_id;
        $careplanID = $request->careplan_id;
        $customerName = $request->customer_name;
        $rating = $request->rating;
        $comment = $request->comment;
        $id = 0;

        if($worklogID && $careplanID && $customerName && $rating && $comment){
            // Get tenant_id from worklog table
            $worklog = WorkLog::withoutGlobalScopes()->whereId($worklogID)->first();
            if($worklog){
                $tenantID = $worklog->tenant_id;
            }

            // Update Worklog Feedback
            $data = [
                'tenant_id' => $tenantID,
                'careplan_id' => $careplanID,
                'worklog_id' => $worklogID,
                'customer_name' => $customerName,
                'rating' => $rating,
                'comment' => $comment,
                'created_at' => date('Y-m-d H:i:s')
            ];

            $id = WorkLogFeedback::insertGetId($data);
        }

        $data['status_code'] = 200;
        if($id > 0)
            $data['result'] = $id;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function getCustomerBills(Request $request)
    {
        $email = $request->input('email');
        $invoices = [];

        if($email){
            $patients = Patient::whereEnquirerEmail($email)
                                ->whereHas('careplan', function($q){
                                    $q->withoutGlobalScopes();
                                })->get();

            if($patients){
                foreach($patients as $p){
                    if(isset($p->careplan)){
                        foreach ($p->careplan as $c) {
                            if(isset($c->invoices)){
                                foreach ($c->invoices as $i) {
                                    $invoices[] = [
					'id' => $i->id,
                                        'careplan_id' => Helper::str($i->careplan_id),
                                        'invoice_no' => Helper::str($i->invoice_no),
                                        'invoice_period_from' => Helper::getDate($i->invoice_period_from),
                                        'invoice_period_to' => Helper::getDate($i->invoice_period_to),
                                        'invoice_date' => Helper::getDate($i->invoice_date),
                                        'case_charges' => Helper::str($i->case_charges),
                                        'invoice_amount' => Helper::str($i->invoice_amount),
                                        'status' => Helper::str($i->status),
                                    ];
                                }
                            }
                        }
                    }
                }
            }
        }

        $data['status_code'] = 200;
        if($invoices)
            $data['result']['bills'] = $invoices;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    function getAllDocuments(Request $request)
    {
        $caregiverID = $request->id;
        $results = [];

        if($caregiverID){
            $docs = Verification::withoutGlobalScopes()->whereCaregiverId($caregiverID)->get();
            if($docs){
                foreach ($docs as $d) {
                    $path = asset("uploads/caregivers/".\Helper::getFolderName($d->caregiver_id)."/mobile");
                    $fileName = substr($d->doc_path, 0, strrpos($d->doc_path, "."));
                    $results[] = [
			'id' => $d->id,
                        'caregiver_id' => $d->caregiver_id,
                        'document_name' => $d->doc_name,
                        'document_path' => $path."/".$fileName.".jpg",
                    ];
                }
            }
        }

        $data['status_code'] = 200;
        if($results)
            $data['result']['documents'] = $results;
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    function getInvoice(Request $request)
    {
        $invoiceID = $request->invoice_id;

        if($invoiceID){
            $invoice = Invoice::withoutGlobalScopes()->whereId($invoiceID)->first();
            if($invoice){
                $data['profile'] = Profile::withoutGlobalScopes()->whereTenantId($invoice->tenant_id)->first();
                $data['billset'] = Settings::withoutGlobalScopes()->whereTenantId($invoice->tenant_id)->first();
                $data['invoice'] = $invoice;

                $pdf = \PDF::loadView('billings.invoice-pdf', $data);

                //return view('billings.invoice-pdf', $data);
                return $pdf->setPaper('a4', 'portrait')->setWarnings(false)->stream('invoice.pdf');
            }
        }
    }
}
