<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\StorePatient;
use App\Entities\Patient;
use App\Entities\Console\Subscriber;
use App\Entities\PatientMailers;
use App\Entities\PatientAddress;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patients = Patient::get();

        return view('patients.index', compact('patients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('patients.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePatient $request)
    {
        $empID = 0;
        $patient_details = $request->only('first_name','last_name','date_of_birth','patient_age','patient_weight','gender','contact_number','email','alternate_number','enquirer_name','relationship_with_patient','street_address','area','city','zipcode','state','country','latitude','longitude');

        if($patient_details){
            $empID = self::saveBasicDetails($patient_details, 0);
        }

        return redirect()->route('patient.index')->with('alert_success','Patient addedd Successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $e = Patient::find(\Helper::encryptor('decrypt',$id));

        return view('patients.form', compact('e'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePatient $request, $id)
    {
        $empID = \Helper::encryptor('decrypt',$id);

        // Patient Details
        $patient_details = $request->only('first_name','last_name','date_of_birth','patient_age','patient_weight','gender','contact_number','email','alternate_number','enquirer_name','relationship_with_patient','street_address','area','city','zipcode','state','country','latitude','longitude');

        if($patient_details){
            if(isset($patient_details['date_of_birth']) && !empty($patient_details['date_of_birth']))
                $patient_details['date_of_birth'] = date_format(new \DateTime($patient_details['date_of_birth']),'Y-m-d');
            else
                $patient_details['date_of_birth'] = null;

            Patient::find(\Helper::encryptor('decrypt',$id))->update($patient_details);
        }

        return redirect()->route('patient.index')->with('alert_success','Patient updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $e = Patient::find(\Helper::encryptor('decrypt',$id));
        if($e){
            $e->delete();
        }

        return back()->with('alert_info','Patient deleted successfully');
    }

    public function savePatient(Request $request)
    {
        if(isset($request->email) && $request->email != null){
            if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
              $emailErr = "Invalid email format"; 
              return redirect()->back()->with('alert_error', $emailErr);
            }
        }

        $patientData = [
            'first_name'                => $request->first_name,
            'last_name'                 => $request->last_name,
            'patient_age'               => $request->patient_age,
            'patient_weight'            => $request->patient_weight,
            'gender'                    => $request->gender,
            'contact_number'            => $request->contact_number,
            'email'                     => $request->email,
            'alternate_number'          => $request->alternate_number,
            'enquirer_name'             => $request->enquirer_name,
            'street_address'            => $request->street_address,
            'area'                      => $request->area,
            'city'                      => $request->city,
            'zipcode'                   => $request->zipcode,
            'state'                     => $request->state,
            'country'                   => $request->country,
            'latitude'                  => $request->latitude,
            'longitude'                 => $request->longitude,
            'relationship_with_patient' => $request->relationship_with_patient,
        ];

        if($request->dob){
            $patientData['dob'] = date_format(new \DateTime($request->dob),'Y-m-d');
        }else{
            $patientData['dob'] = null;
        }

        // Store Patient Info
        if($patientData){
            $patient = Patient::find(\Helper::encryptor('decrypt',$request->patient_id));

            $patient->first_name = $patientData['first_name'];
            $patient->last_name = $patientData['last_name'];
            $patient->date_of_birth = $patientData['dob'];
            $patient->patient_age = $patientData['patient_age'];
            $patient->patient_weight = $patientData['patient_weight'];
            $patient->gender = $patientData['gender'];
            $patient->contact_number = $patientData['contact_number'];
            $patient->email = $patientData['email'];
            $patient->alternate_number = $patientData['alternate_number'];
            $patient->enquirer_name = $patientData['enquirer_name'];
            $patient->street_address = $patientData['street_address'];
            $patient->area = $patientData['area'];
            $patient->city = $patientData['city'];
            $patient->zipcode = $patientData['zipcode'];
            $patient->state = $patientData['state'];
            $patient->country = $patientData['country'];
            $patient->latitude = $patientData['latitude'];
            $patient->longitude = $patientData['longitude'];
            $patient->relationship_with_patient = $patientData['relationship_with_patient'];
            $patient->save();

            return back()->with('alert_success','Patient Details Saved Successfully');
        }

        return back()->with('alert_danger','There was an error, please try re-updating');
    }

    static function saveBasicDetails($data, $id)
    {
        $empID = $id;

        if($data){
            if(isset($id) && $id > 0){
                $patient = Patient::find($id);
                $empID = $id;
            }else{
                $patient = new Patient;
                $patient->tenant_id = \Helper::getTenantID();
            }

            if(isset($data['date_of_birth']) && !empty($data['date_of_birth']))
                $dob = date_format(new \DateTime($data['date_of_birth']),'Y-m-d');
            else
                $dob = null;

            $patient->patient_id = $data['patient_id'];
            $patient->first_name = $data['first_name'];
            $patient->last_name = $data['last_name'];
            $patient->patient_age = $data['patient_age'];
            $patient->patient_weight = $data['patient_weight'];
            $patient->gender = $data['gender'];
            $patient->contact_number = $data['contact_number'];
            $patient->email = $data['email'];
            $patient->alternate_number = $data['alternate_number'];
            $patient->enquirer_name = $data['enquirer_name'];
            $patient->street_address = $data['street_address'];
            $patient->area = $data['area'];
            $patient->city = $data['city'];
            $patient->zipcode = $data['zipcode'];
            $patient->state = $data['state'];
            $patient->country = $data['country'];
            $patient->latitude = $data['latitude'];
            $patient->longitude = $data['longitude'];
            $patient->relationship_with_patient = $data['relationship_with_patient'];
            $patient->save();

            $empID = $patient->id;
        }

        return $empID;
    }

    public function lookUpPatientNumber(Request $request)
    {
        $number= $request->patient_number;
        $checknumber = Patient::whereRaw(\DB::raw("replace(`contact_number`,' ','') like '%$number%'"))->orWhereRaw(\DB::raw("replace(`alternate_number`,' ','') like '%$number%'"))->count();
        if($checknumber){
            echo "Patient Already Exists";
        }
        else{
            echo "New Patient Entry. Please Continue.";
        }
        exit();
    }

    public function uploadPicture(Request $request)
    {
        // Upload Picture
        $profile = $request->file('pic_upload');
        if($profile){
            ini_set('display_error',1);
            
            $extension = $profile->extension();            

            $path = public_path('uploads/provider/'.session('tenant_id').'/case_documents/'.$request->lead_id);
            if(!is_dir($path)){
                \File::makeDirectory($path, 0777, true);
            }

            $tempFileName = $request->patient_id.'_temp.jpg';
            $fileName = $request->patient_id.'.jpg';
            
            $path = $path.'/'.$fileName;            
            \Image::make($profile->getRealPath())->resize(300,300)->save($path, 80);            
            // $res = $profile->move($path, $tempFileName);

            // Crop Picture
            // $targ_w = $targ_h = 300;
            // $jpeg_quality = 90;
            // $data = json_decode($request->crop_values);
            // 
            // if($extension == 'jpg' || $extension == 'jpeg')
            //     $img_r = imagecreatefromjpeg($path.'/'.$tempFileName);
            // 
            // if($extension == 'png')
            //     $img_r = imagecreatefrompng($path.'/'.$tempFileName);
            // 
            // $dst_r = ImageCreateTrueColor($targ_w, $targ_h);
            // 
            // imagecopyresampled($dst_r,$img_r,0,0,$data->x,$data->y,$targ_w,$targ_h,$data->w,$data->h);
            // 
            // // Save the image
            // imagejpeg($dst_r, $path.'/'.$fileName, $jpeg_quality);
            // 
            // // Free up memory
            // imagedestroy($dst_r);
            // 
            // // Delete temp image
            // if(file_exists($path."/".$tempFileName))
            //     unlink($path."/".$tempFileName);

            return back()->with('alert_success','Image updated successfully');            
        }
        
        return back()->with('alert_error','Error uploading patient image!');
    }

    public function addemailaddress($id)
    {
        $patient = Patient::whereId(\Helper::encryptor('decrypt',$id))->first();
        $emails = PatientMailers::wherePatientId(\Helper::encryptor('decrypt',$id))->orderBy('status','asc')->get();
        $addresses = PatientAddress::wherePatientId(\Helper::encryptor('decrypt',$id))->orderBy('status','asc')->get();

        return view('patients.addEmailAddress',compact('patient','emails','addresses'));
    }

    public function extraAddressStore(Request $request)
    {
        if(empty($request->type) || empty($request->address)){
            return back()->with('alert_error','Type or Address was not supplied, please try again !');
        }
        $extraAddress = new PatientAddress;
        $extraAddress->type = ucwords($request->type);
        $extraAddress->address = ucwords($request->address);
        $extraAddress->patient_id = \Helper::encryptor('decrypt',$request->patient_id);
        $extraAddress->latitude = $request->latitude;
        $extraAddress->longitude = $request->longitude;
        $extraAddress->save();
            
        return back()->with('alert_success','Address added for the patient.');
    }

    public function extraAddressUpdate(Request $request)
    {
        if(empty($request->type) || empty($request->address)){
            return back()->with('alert_error','Type or Address was not supplied, please try again !');
        }
        $extraAddress = PatientAddress::whereId(\Helper::encryptor('decrypt',$request->id))->first();
        $extraAddress->type = ucwords($request->type);
        $extraAddress->address = ucwords($request->address);
        $extraAddress->patient_id = \Helper::encryptor('decrypt',$request->patient_id);
        $extraAddress->latitude = $request->latitude;
        $extraAddress->longitude = $request->longitude;
        $extraAddress->save();
            
        return back()->with('alert_success','Address updated for the patient.');
    }

    public function extraAddressUpdateStatus(Request $request)
    {
        if(empty($request->id)){
            return back()->with('alert_error','Address not found, please try again !');
        }
        $extraAddress = PatientAddress::whereId(\Helper::encryptor('decrypt',$request->id))->first();

        if($request->status == 1){
            $extraAddress->status = 'active';
        }else{
            $extraAddress->status = 'inactive';
        }

        $extraAddress->save();
        $state = 'statusChanged';

        return $state;
    }

    public function extraEmailStore(Request $request)
    {   
        if(empty($request->name) || empty($request->email)){
            return back()->with('alert_error','Name or Email was not supplied, please try again !');
        }
        if(!filter_var($request->email, FILTER_VALIDATE_EMAIL)){
            return back()->with('alert_error','Invalid Email, please try again !');
        }
        $extraEmail = new PatientMailers;
        $extraEmail->name = ucwords($request->name);
        $extraEmail->email = $request->email;
        $extraEmail->patient_id = \Helper::encryptor('decrypt',$request->patient_id);
        $extraEmail->save();
            
        return back()->with('alert_success','Email added for the patient.');
    }
    
    public function extraEmailUpdate(Request $request)
    {
        if(empty($request->name) || empty($request->email)){
            return back()->with('alert_error','Type or Address was not supplied, please try again !');
        }
        if(!filter_var($request->email, FILTER_VALIDATE_EMAIL)){
            return back()->with('alert_error','Invalid Email, please try again !');
        }
        $extraEmail = PatientMailers::whereId(\Helper::encryptor('decrypt',$request->id))->first();
        $extraEmail->name = ucwords($request->name);
        $extraEmail->email = $request->email;
        $extraEmail->patient_id = \Helper::encryptor('decrypt',$request->patient_id);
        $extraEmail->save();
            
        return back()->with('alert_success','Email updated for the patient.');
    }

    public function extraEmailUpdateStatus(Request $request)
    {
        if(empty($request->id)){
            return back()->with('alert_error','Email not found, please try again !');
        }
        $extraEmail = PatientMailers::whereId(\Helper::encryptor('decrypt',$request->id))->first();
        if($request->status == 1){
            $extraEmail->status = 'active';
        }else{
            $extraEmail->status = 'inactive';
        }

        $extraEmail->save();
        $state = 'statusChanged';

        return $state;
    }
    
    /*****************************************************************
    |
    |  API Methods
    |
    |****************************************************************/

    public function getAllPatients(Request $request)
    {
        $tenantID = $request->tenant_id;
        $patients = [];

        if($tenantID){
            $res = Patient::withoutGlobalScopes()->whereTenantId($tenantID)->whereNull('deleted_at')->get();
            if($res){
                foreach ($res as $r) {
                    $patients[] = [
                        'id' => $r->id,
                        'first_name' => \Helper::str($r->first_name),
                        'last_name' => \Helper::str($r->last_name),
                        'gender' => \Helper::str($r->gender),
                        'patient_age' => \Helper::str($r->patient_age),
                        'patient_weight' => \Helper::str($r->patient_weight),
                        'street_address' => \Helper::str($r->street_address),
                        'area' => \Helper::str($r->area),
                        'city' => \Helper::str($r->city),
                        'zipcode' => \Helper::str($r->zipcode),
                        'state' => \Helper::str($r->state),
                        'country' => \Helper::str($r->country),
                        'enquirer_name' => \Helper::str($r->enquirer_name),
                        'enquirer_phone' => \Helper::str($r->enquirer_phone),
                        'enquirer_email' => \Helper::str($r->enquirer_email),
                        'alternate_number' => \Helper::str($r->alternate_number),
                    ];
                }
            }
        }

        $data['status_code'] = 200;
        if(count($patients) > 0){
            $data['result']['patients'] = $patients;
        }
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }

    public function savePatientFromApp(Request $request)
    {
        $res = false;
        $patient = $request->patient;
        $id = $patient['id'];
        $patientID = 0;

        if($patient){
            $patientID = $patient['patient_id'];
            unset($patient['id']);
            unset($patient['patient_id']);

            if($patientID == 0){
                $patientID = Patient::withoutGlobalScopes()->insertGetId($patient);
            }else{
                $res = Patient::withoutGlobalScopes()->whereId($patientID)->update($patient);
            }
        }

        $data['status_code'] = 200;
        if($id > 0){
            $data['result']['id'] = $id;
            $data['result']['patient_id'] = $patientID;
        }
        else
            $data['error'] = true;

        return response()->json($data, 200);
    }


    public function bulkImport(Request $request){
        if($request->isMethod('post'))
        {
            $count = 0;
            $file = $request->file('excel_file');
            $path = $file->getRealPath();

            $reader = \Excel::selectSheetsByIndex(0)->load($path, function($reader) {});


            $data = $reader->get();

            if(!empty($data) && $data->count())
            {
                $data = $data->toArray();
                $count = count($data);
                foreach ($data as $key => $value)
                {
                    if(count($value))
                    {
                        \Config::set('database.default','central');
                        $subscriber = Subscriber::whereId(\Helper::getTenantID())->first();
                        \DB::setDefaultConnection('tenant');

                        $patient = new Patient;

                        $patient->tenant_id = \Helper::getTenantID();
                        $patient->first_name = $value['first_name'];
                        $patient->last_name = $value['last_name'];
                        $patient->gender = $value['gender'];
                        $patient->patient_age = sprintf("%.0f", $value['patient_age']);
                        $patient->patient_weight = sprintf("%.0f", $value['patient_weight']);
                        $patient->street_address = $value['street_address'];
                        $patient->area = $value['area'];
                        $patient->city = $value['city'];
                        $patient->state = $value['state'];
                        $patient->country = $value['country'];
                        $patient->enquirer_name = $value['enquirer_name'];

                        $patient->save();
                    }
                }
            }
            return back()->with('alert_success',$count.' patients(s) has been successfully imported.');
        }

        return back();
    }
}
