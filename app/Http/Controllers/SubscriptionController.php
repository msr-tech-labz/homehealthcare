<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Entities\SubscriptionPayment;
use App\Entities\SubscriptionCreditBalance;

use App\Entities\Profile;
use App\Entities\User;
use App\Entities\Lead;
use App\Entities\Settings;

use App\Entities\Console\Subscriber;
use App\Entities\Console\SubscriptionInvoice;
use App\Entities\Console\SubscriptionInvoicePayments;

use App\Mail\Provider\SubscriptionInvoicePayment;
use Softon\Indipay\Facades\Indipay;

use Mail;

class SubscriptionController extends Controller
{
    public function index()
    {
        $data['view'] = 'buy-licence';
        $data['title'] = 'Make Payment';
        $data['balance'] = $this->getBalance();

        $data['used'] = User::count();

        \Config::set('database.default','central');
        $subscriber = Subscriber::whereId(\Helper::getTenantID())->first();
        $data['total_limit'] = $subscriber->users_limit;
        $data['charge_per_user'] = $subscriber->charge_per_user;
        \DB::setDefaultConnection('tenant');

        return view('subscription.index', $data);
    }

    public function renewPaymentPage(Request $request)
    {   
        $renew = true;
        $data['user'] = User::validateCredentials($request->org_code, $request->admin_email, $request->admin_password, $renew);

        if($data['user']){
            session()->put('utype',\Helper::encryptor('encrypt',$data['user']->user_type));
            session()->put('tenant_id',\Helper::encryptor('encrypt',$data['user']->tenant_id));
            session()->put('uid',\Helper::encryptor('encrypt',$data['user']->tenant_id));
            session()->put('utype',\Helper::encryptor('encrypt',$data['user']->user_type));
            session()->put('paymentType','Renew');

            $data['profile'] = Profile::first();
            $data['active_users'] = User::withoutGlobalScopes()->count();
            $data['subscriber'] = Subscriber::whereSubscriberId($request->org_code)->first();
        }

        return response()->json($data,200);
    }

    public function paymentHistory()
    {
        $data['view'] = 'payment-history';
        $data['title'] = 'Payment History';
        $data['balance'] = $this->getBalance();

        $data['payments'] = SubscriptionPayment::whereTenantId(\Helper::getTenantID())->orderBy('id','Desc')->get();

        $data['used'] = User::whereUserDeleted('0')->count();
        \Config::set('database.default','central');
        $subscriber = Subscriber::whereId(\Helper::getTenantID())->first();
        $data['total_limit'] = $subscriber->users_limit;
        \DB::setDefaultConnection('tenant');

        return view('subscription.index', $data);
    }

    public function getBalance()
    {
        $balance = 0;
        $balance = SubscriptionInvoice::whereTenantId(\Helper::getTenantID())->whereStatus('PENDING')->sum('total_amount');

        return $balance;
    }

    public function paymentInvoices()
    {
        $data['view'] = 'payment-invoices';
        $data['title'] = 'Payment Invoices';
        $data['balance'] = $this->getBalance();

        $data['payments'] = SubscriptionInvoice::whereTenantId(\Helper::getTenantID())->orderBy('created_at','asc')->get();
        $data['used'] = User::whereUserDeleted('0')->count();
        \Config::set('database.default','central');
        $subscriber = Subscriber::whereId(\Helper::getTenantID())->first();
        $data['total_limit'] = $subscriber->users_limit;
        \DB::setDefaultConnection('tenant');

        return view('subscription.index', $data);
    }

    public function paymentInvoicesPay(Request $request)
    {
        $paymentID = \Helper::encryptor('decrypt',$request->pid);
        $tenantID = \Helper::encryptor('decrypt',$request->tid);

        $sip = new SubscriptionInvoicePayments;

        $sip->tenant_id = $tenantID;
        $sip->subscription_invoice_id = $paymentID;
        $sip->transaction_id = 1;
        $sip->merchant_txn_id = 1;
        $sip->transaction_date = date("Y-m-d");
        $sip->total_amount = 45000;
        $sip->pg_response = 'Payment successsful';
        $sip->status = 'SUCCESS';

        $sip->save();
        if($sip->status == 'SUCCESS'){
            SubscriptionInvoice::whereId($paymentID)->whereTenantId($tenantID)->update(['status' => 'PAID']);
            $profile = Profile::first();
            $sip = SubscriptionInvoicePayments::with('invoiceGenerated')->select('subscription_invoice_id','transaction_date','transaction_id','total_amount')
                                                ->whereSubscriptionInvoiceId($paymentID)
                                                ->first();

            if($profile->email != null){
                $mail = Mail::to($profile->email)->send(new SubscriptionInvoicePayment($profile,$sip));
                return back()->with('success','Payment Successfull.Invoice has been sent to the registered email id.');
            }
                return back()->with('success','Payment Successfull');
            }else{
                return back()->with('failure','Payment Unsuccessful');
            }
    }

    public function paymentInvoiceDownload(Request $request)
    {
        $paymentID = \Helper::encryptor('decrypt',$request->pid);
        $tenantID = \Helper::encryptor('decrypt',$request->tid);
        $profile = Profile::select('organization_name','contact_person')->first();
        $sip = SubscriptionInvoicePayments::with('invoiceGenerated')->select('subscription_invoice_id','transaction_date','transaction_id','total_amount')
                                            ->whereSubscriptionInvoiceId($paymentID)
                                            ->first();

        $pdf = \PDF::loadView('subscription.apnafocusInvoicePDF', compact(['sip','profile']));
        return $pdf->download('apnafocus_invoice_'.$sip->transaction_id.'.pdf');
    }

    function processPayment(Request $request)
    {
        $data = json_decode($request->payment_data, true);
        if($this->validatePaymentData($data)){
            $count = SubscriptionPayment::count();
            $transactionID = 'APS-'.date('Ymd').str_pad(($count+1),4,'0',STR_PAD_LEFT);

            $paymentData = [
                'tenant_id' => \Helper::getTenantID(),
                'user_type' => \Helper::encryptor('decrypt',session('utype')),
                'transaction_id' => $transactionID,
                'transaction_date' => date('Y-m-d H:i:s'),
                'credit_count' => $data['credit'],
                'actual_amount' => $data['actual_amount'],
                'gst' => $data['gst'],
                'tds' => $data['tds'],
                'total_amount' => $data['total_amount'],
                'status' => 'Pending',
                'created_at' => date('Y-m-d H:i:s')
            ];
            $paymentID = SubscriptionPayment::insertGetId($paymentData);
            \DB::purge('api');
            self::setTenantConnection(\Helper::getTenantID());

            // Initiate Payment
            $profile = Profile::first();

            if($profile){
                /* All Required Parameters by your Gateway */
                $parameters = [
                    'tid' => $transactionID,
                    'transactionRefID' => $transactionID,
                    'orderAmount' =>  $data['total_amount'],
                    'firstName' => preg_replace('/[^A-Za-z0-9 ]/', '', $profile->organization_name),
                    'lastName' => preg_replace('/[^A-Za-z0-9 ]/', '', $profile->organization_short_name),
                    'email' => $profile->email,
                    'phoneNumber' => $profile->phone_number,
                    'OrganizationCode' => isset($profile->subscriber)?$profile->subscriber->subscriber_id:'',
                    'addressStreet1' => preg_replace('/[^A-Za-z0-9 ]/', '', ($profile->organization_address.' '.$profile->organization_area)),
                    'addressCity' => $profile->organization_city,
                    'addressState' => $profile->organization_state,
                    'addressCountry' => $profile->organization_country,
                    'addressZip' => $profile->organization_zipcode,
                ];
                //$parameters = array_merge($parameters, $profileDetails);
                $order = Indipay::gateway('Citrus')->prepare($parameters);

                //Update MerchantTxnId to table
                $transaction = SubscriptionPayment::whereTransactionId($transactionID)->first();
                $transaction->merchant_txn_id = $order->getMerchantTxnId();
                $transaction->save();

                return Indipay::process($order);
            }
        }

        return back()->with('alert_error','Could not process your request. Try again later');
    }

    function validatePaymentData($data)
    {
        $valid = false;
        if($data){
            $keys = ['credit','actual_amount','gst','tds','total_amount'];
            foreach($data as $key => $value){
                if(!isset($value)){
                    $valid = false;
                }else{
                    $valid = true;
                }
            }
        }

        return $valid;
    }

    function paymentResponse(Request $request)
    {
       $response = Indipay::response($request);
       $success = false;

       if(isset($response['TxId'])){
           $status = $response['TxStatus'];
           $pg_response = json_encode($response);

           $transaction = SubscriptionPayment::whereMerchantTxnId($response['TxId'])->first();
           if($transaction){
               $transaction->status = $status;
               $transaction->pg_response = $pg_response;
               $transaction->save();

               // If successfull, Update the Balance
               if($status == 'SUCCESS'){
                   $credits = $transaction->credit_count;
                   \Config::set('database.default','central');
                    $subscriber = Subscriber::where('id',$transaction->tenant_id)->first();
                    $subscriber->users_limit = $subscriber->users_limit + $credits;
                    $subscriber->amount_userbase = $subscriber->charge_per_user * $subscriber->users_limit;
                    $subscriber->save();
                   \DB::setDefaultConnection('tenant');

                   // $balanceData = [
                   //     'tenant_id' => \Helper::getTenantID(),
                   //     'user_type' => \Helper::encryptor('decrypt',session('utype')),
                   //     'transaction_id' => $transaction->transaction_id,
                   //     'transaction_date' => date('Y-m-d H:i:s'),
                   //     'previous_balance' => $prevBalance,
                   //     'current_balance' => intval($prevBalance) + intval($transaction->actual_amount)
                   // ];

                   //SubscriptionCreditBalance::create($balanceData);
               }
           }

           if($status == 'SUCCESS')
                $success = true;
       }
       if(isset($response['paymentType'])){
            if($success){
                 session()->forget('subscription_expired');
                 session()->forget('trial_expired');
                 return redirect('/login')->with('payment_status','Payment Successfull, your subscription has been updated, please re-login');
             }
            else
                 return redirect('/login')->with('payment_status','Payment Failed! Please try again later');
       }else{
           if($success){
               session()->forget('subscription_expired');
               session()->forget('trial_expired');
               return redirect('/subscription')->with('success','Payment Successfull, your credits have been updated');
            }
           else
                return redirect('/subscription')->with('failure','Payment Failed! Please try again later');
       }
    }

    function paymentReceipt(Request $request, $id = null)
    {
        if($id){
            $payment = SubscriptionPayment::find(\Helper::encryptor('decrypt',$id));
            if($payment)
                return view('subscription.payment-receipt', compact('payment'));
        }

        return redirect('/subscription/paymentHistory')->with('failure','Couldnot find receipt!');
    }
}
