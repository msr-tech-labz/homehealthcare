<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Entities\Console\Subscriber;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function setTenantConnection($tenantID)
    {
        // Get Database from tid and set default connection
        $subscriber = Subscriber::find($tenantID);
        if($subscriber){
            \Config::set('database.connections.api.database',$subscriber->database_name);
            \DB::setDefaultConnection('api');
        }
    }
}
