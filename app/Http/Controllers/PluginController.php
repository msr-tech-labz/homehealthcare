<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Helpers\Helper;
use App\Entities\Console\Subscriber;
use App\Entities\Patient;
use App\Entities\Settings;
use App\Entities\Lead;
use App\Entities\Masters\PluginSettings;

class PluginController extends Controller
{

    /**
     * Plugin Index
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $ch = PluginSettings::whereTenantId(\Helper::getTenantID())
                                ->whereCategory('Call Handling')->get();
        $data['ch'] = $ch->groupBy('plugin_name')->toArray();

        $pg = PluginSettings::whereTenantId(\Helper::getTenantID())
                                ->whereCategory('Payment Gateway')->get();
        $data['pg'] = $pg->groupBy('plugin_name')->toArray();

        return view('administration.plugin', $data);
    }

    /**
     * Exotel Plugin Function
     *
     * @return \Illuminate\Http\Response
     */
    public function exotelGetCallLog(Request $request)
    {
        $data = $request->only('CallSid','From','To');
        $res = false;

        if(!empty($data['CallSid']) && !empty($data['From'])){
            $res = \DB::connection('central')->table('exotel_logs')
                ->insert(['sid' => $data['CallSid'],
                    'date_created' => date('Y-m-d'),
                    'from_no' => $data['From'],
                    'to_no' => $data['To'],
                    'status' => 'Not Checked']);

            \DB::connection('central')->table('lead_requests')
                ->insert(['exotel_sid' => $data['CallSid'],
                    'created_at' => date('Y-m-d H:i:s'),
                    'phone_number' => $data['From'],
                    'source' => 'Exotel',
                    'status' => 'Not Checked']);
        }

        return response()->json(['status' => (bool) $res], 200);
    }

    public function setDefault(Request $request)
    {
        $data = $request->only('id','category','plugin_name');
        $id = \Helper::encryptor('decrypt',$data['id']);

        $plugin = PluginSettings::find($id);
        if($plugin){
            // Clear default for other plugins
            $plugins = PluginSettings::whereCategory($data['category'])->get();
            if(count($plugins)){
                foreach ($plugins as $p) {
                    $p->is_default = 0;
                    $p->save();
                }
            }

            // Make the current as default
            $plugin->is_default = 1;
            $plugin->save();

            return back()->with('alert_success','Set default successful');
        }

        return back();
    }

    /**
     * Knowlarity Plugin Function
     *
     * @return \Illuminate\Http\Response
     */
    public function knowlarityGetCallLog(Request $request)
    {
        $input = $request->only('tid','call_type','customer_number');
        $res = false;

        if(!empty($input['tid']) && isset($input['call_type']) && !empty($input['customer_number'])){
            if($input['call_type'] == '0'){
                $tenantID = Helper::encryptor('decrypt',$input['tid']);

                // Set Tenant Connection
                self::setTenantConnection($tenantID);

                // Create Patient Record (If not exists)
                $patientID = 0;
                $patient = Patient::whereContactNumber($input['customer_number'])
                                    ->orWhere('alternate_number',$input['customer_number'])
                                    ->first();

                if($patient){
                    $patientID = $patient->id;
                }else{
                    // Generate Unique Patient ID
                    $prefix = Settings::value('patient_id_prefix');
                    $startNo = intval(Settings::value('patient_id_start_no'));
                    if($prefix){
                        $pID = $prefix.intval($startNo + (Patient::count() + 1));
                    }
                    $patientID = Patient::insertGetId(['tenant_id' => $tenantID,'patient_id' => $pID, 'contact_number' => $input['customer_number'], 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
                }


                // Generate Unique Episode ID
                $prefix = Settings::value('episode_id_prefix');
                $startNo = intval(Settings::value('episode_id_start_no'));
                if($prefix){
                    $episodeID = $prefix.intval($startNo + (Lead::count() + 1));
                }
                // Create Lead Entry
                $res = Lead::create(['tenant_id' => $tenantID, 'episode_id' => $episodeID,'patient_id' => $patientID, 'referral_source' => 'Knowlarity']);
            }
        }

        return response()->json(['status' => (bool) $res], 200);
    }

    public function saveCcavenueCredentials(Request $request)
    {
        $data = $request->only('category','plugin_name');
        $input = $request->except('_token','category','plugin_name');
        $data['settings'] = json_encode($input);

        $data['tenant_id'] = \Helper::getTenantID();

        $settings = PluginSettings::whereTenantId($data['tenant_id'])
                            ->whereCategory('Payment Gateway')
                            ->wherePluginName('pg_ccavenue')
                            ->first();
        if($settings){
            $settings->update($data);
        }else{
            PluginSettings::create($data);
        }

        return back()->with('alert_success','Plugin settings saved.');
    }

    public function saveAtomCredentials(Request $request)
    {
        $data = $request->only('category','plugin_name');
        $input = $request->except('_token','category','plugin_name');
        $data['settings'] = json_encode($input);

        $data['tenant_id'] = \Helper::getTenantID();

        $settings = PluginSettings::whereTenantId($data['tenant_id'])
                            ->whereCategory('Payment Gateway')
                            ->wherePluginName('pg_atom')
                            ->first();
        if($settings){
            $settings->update($data);
        }else{
            PluginSettings::create($data);
        }

        return back()->with('alert_success','Plugin settings saved.');
    }

    /**
     * Quick Service Request Form - Aggregator
     *
     * @return \Illuminate\Http\Response
     */
    public function quickCaseFormSubmit(Request $request)
    {
        $data = $request->except('_token');
        $res = [];
        $service_name = "";

        if($data){
            $res = Lead::on('aggregator')->create($data);
            if(isset($data['service_name'])){
                $service_name = $data['service_name'];
                unset($data['service_name']);
            }

            // Duplicate the same request to Old Call Handling Software
            \DB::connection('portal')->table('website_leads')
                ->insert(['name' => $data['name'],
                'phone' => $data['phone'],
                'email' => $data['email'],
                'city' => $data['city'],
                'area' => '',
                'status' => 'Not Checked',
                'created_at' => date('Y-m-d H:i:s')]);

            \DB::connection('hhms')->table('leads')
                ->insert(['name' => $data['name'],
                'phone' => $data['phone'],
                'email' => $data['email'],
                'city' => $data['city'],
                'area' => isset($data['locality'])?$data['locality']:'',
                'requirement' => $data['requirement'],
                'source' => 'Website',
                'status' => 'Not Checked',
                'created_at' => date('Y-m-d H:i:s')]);

            // Notify to Apnacare Team
            try{
                $data['recipient_name'] = 'Team';
                $data['service_name'] = $service_name;
                $data['created_at'] = $res->created_at->format('d-m-Y h:i A');
                Email::newServiceRequestFromWebsite($data);
            }catch(Exception $e){
            }

            // Send Acknowledgment to Customer
            try{
                $data['recipient_name'] = $data['name'];
                $data['service_name'] = $service_name;
                Email::quickCaseCustomerAcknowledgment($data);
            }catch(Exception $e){
            }
        }

        return response()->json($res,200);
    }
}
