<?php

namespace App\Http\Middleware;

use Closure;

class CheckAPISession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->header('Accept') == 'application/x.apnacare.v1+json' && !empty($request->header('ORGHASH'))){
            $dbName = \Helper::encryptor('decrypt',$request->header('ORGHASH'));        
            \Config::set('database.connections.tenant.database',$dbName);
            \DB::setDefaultConnection('tenant');
        }

        return $next($request);
    }
}
