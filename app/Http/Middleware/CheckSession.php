<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // if mobile screen then redirect to mobile page
        /* $user_ag = isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:'';
        if(preg_match('/(android|Kindle|Silk.*Accelerated|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i',$user_ag)){
           session()->flush();
           return redirect('mobile');
        } */

        if(!$request->ajax() && !$request->wantsJson()) {      
            if(!session('uid') || !session('utype')){
                return redirect('/login');
            }
        }
        $dbName = \Helper::encryptor('decrypt',session('dbName'));
        \Config::set('database.connections.tenant.database',$dbName);
        \DB::setDefaultConnection('tenant');

        // if(session('last_session') != Auth::user()->last_session){
        //     session()->flush();
        //     return redirect('login')->with('alert_error','User is currently logged in from another system !!');
        // }else{
        //     return $next($request);
        // }
            return $next($request);
        
    }
}
