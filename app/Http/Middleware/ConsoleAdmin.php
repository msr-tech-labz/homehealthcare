<?php

namespace App\Http\Middleware;

use Closure;

class ConsoleAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!session()->has('console.isLoggedIn') && !session('console.isLoggedIn')){
            return redirect()->route('console.login');
        }
        return $next($request);
    }
}
