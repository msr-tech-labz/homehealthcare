<?php

namespace App\Http\Middleware;

use Closure;

class CorpUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \Config::set('database.connections.tenant.database',\Helper::encryptor('decrypt',session('corporate.dbName')));
        \DB::setDefaultConnection('tenant');

        if(!session()->has('corporate.isLoggedIn') && !session('corporate.isLoggedIn')){
            return redirect()->route('corporate.login');
        }
        return $next($request);
    }
}
