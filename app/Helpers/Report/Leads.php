<?php
namespace App\Helpers\Report;

use App\Entities\Lead;
use App\Entities\Console\Subscriber;
use App\Entities\LeadRequests;
use App\Entities\AggregatorLeads;
use App\Entities\LeadDistributionHistory;

class Leads {

    static function masterData($request)
    {
        $data = collect();
        $leadsArr = array();

        if($request){
            if(\Helper::isAggregator()){
                $subscribers = Subscriber::whereStatus('Active')->pluck('database_name')->toArray();
                foreach ($subscribers as $i => $s) {
                    \Config::set('database.connections.api.database',$s);
                    \Config::set('database.default','api');
                    \DB::setDefaultConnection('api');

                    $leads = Lead::withoutGlobalScopes()->where('aggregator_manager_id','>',0)->with('profile')->with('patient')->with('serviceRequired')->with('referralCategory')->with('referralSource')
                                ->with(['convertedBy' => function($q){
                                    $q->select('employee_id','first_name','middle_name','last_name');
                                }]);
                    if(!empty($request->filter_status)){
                        $leads->whereStatus($request->filter_status);
                    }

                    if(!empty($request->filter_period)){
                        $period = explode("_",$request->filter_period);                
                        $leads->whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$period[0].'" AND "'.$period[1].'"'));
                    }
                        if($leads->count())
                            $leadsArr[] = $leads->get();

                    \DB::purge('api');
                }
            }else{
                    $leads = Lead::with('patient')->with('serviceRequired')->with('referralCategory')->with('referralSource');
                    if(!empty($request->filter_status)){
                        $leads->whereStatus($request->filter_status);
                    }

                    if(!empty($request->filter_period)){
                        $period = explode("_",$request->filter_period);                
                        $leads->whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$period[0].'" AND "'.$period[1].'"'));
                    }
                    if($leads->count())
                        $leadsArr[] = $leads->get();
            }
                return collect($leadsArr);
        }

        return $data;
    }

    static function dailyStats($fromDate,$toDate)
    {
        $data = collect();
        if($fromDate && $toDate){
            //Aggregator
            $data['totalLeads'] = (Lead::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->count()) + (AggregatorLeads::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->count());
            $data['aggPending'] = Lead::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereStatus('Pending')->count();
            $data['aggConverted'] = Lead::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereStatus('Converted')->count();
            $data['aggDropped'] = Lead::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereStatus('Dropped')->count();

            //Provider
            $data['transProvider'] = AggregatorLeads::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->count();
            $data['pendingProvider'] = AggregatorLeads::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereStatus('Pending')->count();
            $data['convertedProvider'] = AggregatorLeads::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereStatus('Accepted')->count();
            $data['lossProvider'] = AggregatorLeads::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereStatus('Dropped')->count();
            $data['acceptanceRatioProvider'] = (LeadDistributionHistory::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereStatus('Accepted')->count()).':'.(LeadDistributionHistory::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereStatus('Declined')->count());

            //Freelancer

            //Exotel
            $data['leadExotelTotal'] = LeadRequests::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereSource('Exotel')->count();
            $data['leadExotelAttended'] = LeadRequests::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereSource('Exotel')->whereStatus('Checked')->count();
            $data['leadExotelUnattended'] = LeadRequests::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereSource('Exotel')->whereStatus('Not Checked')->count();
            $data['leadExotelPending'] = LeadRequests::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereSource('Exotel')
                                                        ->whereHas('sources',function($q){
                                                                $q->whereStatus('Pending');
                                                        })->count();
            $data['leadExotelConverted'] = LeadRequests::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereSource('Exotel')
                                                        ->whereHas('sources',function($q){
                                                                $q->whereStatus('Accepted');
                                                        })->count();

            //Website
            $data['leadWebsiteTotal'] = LeadRequests::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereSource('Website')->count();
            $data['leadWebsiteAttended'] = LeadRequests::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereSource('Website')->whereStatus('Checked')->count();
            $data['leadWebsiteUnattended'] = LeadRequests::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereSource('Website')->whereStatus('Not Checked')->count();
            $data['leadWebsitePending'] = LeadRequests::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereSource('Website')
                                                        ->whereHas('sources',function($q){
                                                                $q->whereStatus('Pending');
                                                        })->count();
            $data['leadWebsiteConverted'] = LeadRequests::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereSource('Website')
                                                        ->whereHas('sources',function($q){
                                                                $q->whereStatus('Accepted');
                                                        })->count();
            //MCM's
            $data['jayaPending'] = (Lead::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereAggregatorManagerId('1')->whereStatus('Pending')->count())+(AggregatorLeads::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereUserId('1')->whereStatus('Pending')->count());
            $data['jayaConverted'] = (Lead::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereAggregatorManagerId('1')->whereStatus('Converted')->count())+(AggregatorLeads::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereUserId('1')->whereStatus('Accepted')->count());
            $data['jayaCaseLoss'] = (Lead::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereAggregatorManagerId('1')->whereStatus('Dropped')->count())+(AggregatorLeads::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereUserId('1')->whereStatus('Dropped')->count());

            $data['sheelaPending'] = (Lead::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereAggregatorManagerId('2')->whereStatus('Pending')->count())+(AggregatorLeads::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereUserId('2')->whereStatus('Pending')->count());
            $data['sheelaConverted'] = (Lead::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereAggregatorManagerId('2')->whereStatus('Converted')->count())+(AggregatorLeads::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereUserId('2')->whereStatus('Converted')->count());
            $data['sheelaCaseLoss'] = (Lead::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereAggregatorManagerId('2')->whereStatus('Dropped')->count())+(AggregatorLeads::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereUserId('2')->whereStatus('Dropped')->count());

            $data['bhavyaPending'] = (Lead::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereAggregatorManagerId('3')->whereStatus('Pending')->count())+(AggregatorLeads::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereUserId('3')->whereStatus('Pending')->count());
            $data['bhavyaConverted'] = (Lead::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereAggregatorManagerId('3')->whereStatus('Converted')->count())+(AggregatorLeads::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereUserId('3')->whereStatus('Converted')->count());
            $data['bhavyaCaseLoss'] = (Lead::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereAggregatorManagerId('3')->whereStatus('Dropped')->count())+(AggregatorLeads::whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereUserId('3')->whereStatus('Dropped')->count());
        }
        return $data;
    }
}
