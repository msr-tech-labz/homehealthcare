<?php
namespace App\Helpers\Report;

use App\Entities\Schedule;
use App\Entities\CaseServiceOrder;
use App\Entities\Masters\RateCard;
use App\Entities\Invoice;
use App\Entities\ServiceInvoice;
use App\Entities\Caregiver\Caregiver;
use App\Entities\Lead;
use App\Entities\Receipt;

class Financial {

    static function revenueWithSchedules($request)
    {
        ini_set('memory_limit', '1024M'); // or you could use 1G
        ini_set('max_execution_time', '3000'); // or you could use 1G
        $data = collect();
        $patient = $request->patient;
        $period = $request->period;
        if($request->ajax()){
            $schedules = Schedule::whereStatus('Completed')->with(['lead','service.ratecard.tax','patient','caregiver','serviceRequest'])
            ->whereHas('lead', function($q){
                $q->whereIn('status',['Converted','Closed']);
            });
            if(!empty($patient) && $patient != "0"){
                $schedules->wherePatientId((int) $patient);
            }

            if(!empty($period) && $period != "0" && $period != ""){
                $period = explode("_",$period);
                $schedules->whereBetween('schedule_date',[$period[0],$period[1]]);
            }

            $schedules = $schedules->orderBy('schedule_date','ASC')->get();

            if(count($schedules)){
                foreach ($schedules as $schedule) 
                {
                    // $rate = $schedule->serviceRequest->gross_rate;
                    $grossRate = $schedule->service->ratecard->amount;
                    $netRate = $schedule->amount;
                    $taxRate = $schedule->service->ratecard->tax->tax_rate;
                    $taxedAmount = ($grossRate)*(($taxRate)/100);
                    $discount = $grossRate - $netRate;
                    $data->push([
                        'date' => $schedule->schedule_date->format('d-m-Y'),
                        'episode_id' => $schedule->caserecord->episode_id,
                        'patient_name' => $schedule->patient->full_name,
                        'patient_phone' => $schedule->patient->contact_number,
                        'service_requested' => $schedule->service->service_name,
                        'employee_id' => isset($schedule->caregiver)?$schedule->caregiver->employee_id:'',
                        'employee_name' => isset($schedule->caregiver)?$schedule->caregiver->full_name:'',
                        'chargeable' => $schedule->chargeable?'Yes':'No',
                        'gross_rate' => number_format($grossRate,2),
                        'net_rate' => number_format($netRate,2),
                        'discount' => number_format($discount,2),
                        'revenue' => $schedule->chargeable?number_format($netRate):0
                    ]);
                }
            }
        }
        unset($schedules);

        return \DataTables::of($data)->make(true);
    }

    static function groupScheduleByDate($dates)
    {
        // process the array
        $lastDate = null;
        $ranges = array();
        $currentRange = array();
        $results = [];

        foreach ($dates as $date) {
            if (null === $lastDate) {
                $currentRange[] = $date;
            } else {
                // get the DateInterval object
                $interval = $date->diff($lastDate);

                // DateInterval has properties for days, weeks. months etc.
                // You should implement some more robust conditions here to
                // make sure all you're not getting false matches for diffs
                // like a month and a day, a year and a day and so on...

                if ($interval->days === 1) {
                    // add this date to the current range
                    $currentRange[] = $date;
                } else {
                    // store the old range and start anew
                    $ranges[] = $currentRange;
                    $currentRange = array($date);
                }
            }

            // end of iteration...
            // this date is now the last date
            $lastDate = $date;
        }

        // messy...
        $ranges[] = $currentRange;
        $from = "";
        $to = "";

        // print dates
        foreach ($ranges as $range) {
            $endDate = null;
            // there'll always be one array element, so
            // shift that off and create a string from the date object
            $startDate = array_shift($range);
            $str = sprintf('%s', $startDate->format('d-m-Y')); // D j M
            $from = sprintf('%s', $startDate->format('d-m-Y'));

            // if there are still elements in $range
            // then this is a range. pop off the last
            // element, do the same as above and concatenate
            if (count($range)) {
                $endDate = array_pop($range);
                if($endDate){
                    $str .= sprintf(' to %s', $endDate->format('d-m-Y'));
                    $to = sprintf('%s', $endDate->format('d-m-Y'));
                }else{
                    $to = "";
                }
            }

            $duration = self::getDurationBetweenDates($from, $to);

            $results[] = ['from' => $from, 'to' => $to,'duration' => $duration, 'fromDate' => $startDate, 'toDate' => $endDate];
            $endDate = "";$to = "";
        }

        return $results;
    }

    static function getDurationBetweenDates($startDate, $endDate)
    {
        $begin = new \DateTime($startDate);
        if(!empty($endDate)){
            $end = new \DateTime($endDate);
        }else{
            $end = new \DateTime($startDate);
        }
        $end = $end->modify( '+1 day' );

        $interval = new \DateInterval('P1D');
        $daterange = new \DatePeriod($begin, $interval ,$end);

        return iterator_count($daterange);
    }

    static function serviceDetailsPatientWise($request)
    {
        $data = collect();
        $results = [];
        if($request->isMethod('get')){
            $patient = $request->patient;
            $period = $request->period;

            $schedules = Schedule::with('caserecord')
                            ->whereHas('caserecord', function($q){
                                //$q->whereStatus('Converted');
                            })
                            ->get();

            if(count($schedules)){
                foreach ($schedules->groupBy('service_required') as $key => $group) {
                    // To get Rate from service order
                    $serviceOrder = null;
                    $orderID = $group->where('service_order_id','!=',0)->pluck('service_order_id')->flatten()->unique()->first();

                    if($orderID){
                        $serviceOrder = CaseServiceOrder::find($orderID);
                        if($serviceOrder){
                            //Invoice::
                        }
                    }

                    $dates = $group->pluck('schedule_date');
                    $items = self::groupScheduleByDate($dates, $group);
                    if(count($items)){
                        foreach ($items as $item) {
                            $period = $item['from'];
                            if($item['to'])
                                $period .= ' to '.$item['to'];

                            $results[] = [
                                'id' => $key,
                                'episode_id' => $group->pluck('caserecord.episode_id')->flatten()->unique()->first(),
                                'patient_name' => $group->pluck('patient.full_name')->flatten()->unique()->first(),
                                'service_name' => \Helper::getServiceName($key),
                                'from_date' => $item['from'],
                                'start_date' => $item['from'],
                                'end_date' => $item['to'],
                                'last_service_order' => ''
                            ];
                        }
                    }
                }
            }
        }
        dd($results);

        return \DataTables::of($data)->make(true);
    }

    static function statementOfAccount($period, $patientID = null)
    {
        $data = collect();

        if($period){
            $fromDate = explode("_",$period)[0];
            $toDate = explode("_",$period)[1];

            $res = \DB::table('case_schedules as cs')
                    ->selectRaw('cs.patient_id as id, p.patient_id, SUM(csr.net_rate * IF(cs.chargeable = 1, cs.chargeable, cs.chargeable + -1)) as serviceCharges')
                    ->join('case_service_requests as csr','csr.id','=','cs.service_request_id')
                    ->join('patients as p','p.id','=','cs.patient_id')
                    ->whereRaw('cs.status = "Completed"')
                    ->whereRaw('p.patient_id IS NOT NULL and p.patient_id != ""')
                    ->whereRaw('DATE(cs.schedule_date) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"')
                    ->groupBy('cs.patient_id')
                    ->orderBy('cs.patient_id');

            if($patientID != null && !empty($patientID))
                $res->whereRaw('cs.patient_id = '.$patientID);

            $res = $res->get();

            $chargesArray = collect($res)->mapWithKeys(function ($item) {
                                return [$item->id => $item->serviceCharges];
                            })->toArray();

            $patients = \App\Entities\Patient::select('id','patient_id','first_name','last_name','contact_number','email')
                            ->whereNotNull('patient_id')->where('patient_id','!=','')
                            ->whereIn('id',array_keys($chargesArray))
                            ->with([ 'creditMemo','lead' => function($q){
                                $q->whereNotIn('status',['Pending']);
                            }])->get();
            if(count($patients)){
                foreach ($patients as $patient) {
                    // Get schedule charges
                    $scheduleCharges = 0;
                    if(isset($chargesArray[$patient->id]))
                        $scheduleCharges = floatVal($chargesArray[$patient->id]);

                    // Get Invoice balance
                    $invoiceBalance = $patient->invoices()->whereRaw(\DB::raw('DATE(invoice_date) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereStatus('Pending')->sum('total_amount');

                    // Get Case Billables / Consumables sum
                    $billablesTotal = \App\Entities\CaseBillables::whereIn('lead_id',$patient->lead()->pluck('id')->toArray())
                                            ->whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))
                                            ->sum('amount');

                    // Deduct Credit balance from Outstanding balance
                    $totalBalance = (floatVal($scheduleCharges) + floatVal($invoiceBalance) + floatVal($billablesTotal));

                    // Get payments sum
                    $payments = $patient->payments()->whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->whereStatus('Success')->sum('total_amount');
                    // Get Credit memo sum
                    $creditMemoSum = $patient->creditMemo()->whereRaw(\DB::raw('DATE(date) BETWEEN "'.$fromDate.'" AND "'.$toDate.'" and type = "Others"'))->sum('amount');

                    $totalPayments = floatVal($payments) + floatVal($creditMemoSum);

                    $data->push([
                        'id' => $patient->id,
                        'patient_id' => $patient->patient_id,
                        'patient_name' => $patient->full_name,
                        'patient_phone' => $patient->contact_number,
                        'email' => $patient->email,
                        'total_charges' => number_format($totalBalance,2),
                        'total_payments' => number_format($totalPayments,2),
                        'outstanding_balance' => number_format(floatVal($totalBalance) - floatVal($totalPayments),2)
                    ]);
                }
            }
        }

        return $data;
    }

    static function manualCreditMemoReport($request)
    {
        ini_set('memory_limit', '1024M'); // or you could use 1G
        ini_set('max_execution_time', '3000'); // or you could use 1G
        $data = collect();
        $patient = $request->patient;

        if($request->ajax()){
            $creditmemos = \App\Entities\CreditMemo::select('*')->where('type','!=', 'Schedule');
            if(!empty($patient) && $patient != "0")
                $creditmemos->wherePatientId($patient);

            if(!empty($request->period) && $request->period != "0" && $request->period != ""){
                $period = explode("_",$request->period);
                $creditmemos->whereRaw(\DB::raw('DATE(date) BETWEEN "'.$period[0].'" AND "'.$period[1].'"'));
            }

            $creditmemos = $creditmemos->get();

            if(count($creditmemos)){
                foreach ($creditmemos as $i => $creditmemo) 
                {
                    $data->push([
                        'date_on_memo' => \Carbon\Carbon::parse($creditmemo->date)->format('d-m-Y'),
                        'date_created' => $creditmemo->created_at->format('d-m-Y'),
                        'patient_id' => $creditmemo->patient->patient_id,
                        'patient_name' => $creditmemo->patient->full_name,
                        'credit_memo_no' => isset($creditmemo->credit_memo_no)?$creditmemo->credit_memo_no:'',
                        'amount' => isset($creditmemo->amount)?$creditmemo->amount:'',
                        'comment' => isset($creditmemo->comment)?$creditmemo->comment:'NA',
                        'status' => $creditmemo->status,
                    ]);
                }
            }
        }
        unset($schedules);

        return \DataTables::of($data)->make(true);
    }

    static function invoiceReport($request)
    { 
        $data = collect();
        $patient = $request->patient;
        $invoice_type = $request->invoice_type;

        if($request->ajax()){
            if($invoice_type=='Proforma'){
                $invoices = Invoice::all();
            } else if($invoice_type=='Service'){
                $invoices = ServiceInvoice::all();
            } else {
                $invoices = collect(Invoice::all())->merge(collect(ServiceInvoice::all()));
            }

            if(!empty($patient) && $patient != "0"){
                $invoices = $invoices->where('patient_id',$patient);
            }


            if(!empty($request->period) && $request->period != "0" && $request->period != ""){
                $period = explode("_",$request->period);
                $invoices = $invoices->where('invoice_date','>=',$period[0])->where('invoice_date','<=',$period[1]);
            }

            if(count($invoices)){
                foreach ($invoices as $i => $invoice)
                {
                    $salesList = '';
                    $itemIds = $invoice->items()->withTrashed()->where('category','Schedule')->pluck('item_id');
                    $episodesIds = Schedule::whereIn('id',$itemIds)->pluck('lead_id')->unique();
                    $salesIdList = Lead::whereIn('id',$episodesIds)->where('converted_by','!=',null)->where('converted_by','!=','')->pluck('converted_by')->unique();
                    if($salesIdList->count()){
                        foreach ($salesIdList as $id) {
                            $c = Caregiver::find($id);
                            if($c){
                                $salesList .= $c->full_name.'-'.$c->mobile_number.',';
                            }
                        }
                    }

                    $prefix = $invoice instanceOf Invoice?\Helper::encryptor('decrypt',session('settings.proforma_invoice_inits')):\Helper::encryptor('decrypt',session('settings.invoice_inits'));
                    if($invoice instanceOf Invoice){
                        $dateInv =  \Carbon\Carbon::parse($invoice->invoice_due_date);
                        $now =  \Carbon\Carbon::now();
                        $diff = $dateInv->diffForHumans($now,false,false,2);

                        if($invoice->status == 'Pending' || $invoice->status == 'Partial'){
                            $ageingDiff = date_diff(new \DateTime(), $invoice->created_at)->format("%a day(s) %h hr(s)");
                        }else{
                            $ageingDiff = '';
                        }

                        if($invoice->status == 'Paid'){
                            $diff = 'Cleared';
                        }else if($invoice->status == 'Cancelled'){
                            $diff = '';
                        }else if($invoice->status == 'Adjusted'){
                            $diff = '';
                        }

                        $receipts_info = '';
                        if(isset($invoice->receipts) && count($invoice->receipts)){
                            $index = 1;
                            foreach ($invoice->receipts as $receipt) {
                                $receipts_info .= nl2br(($index++).') Mode-"'.$receipt->payment_mode.'" | Ref No-"'.$receipt->reference_no.'"'.',');
                            }
                        }

                        $creditMemosCost = $invoice->items()->withTrashed()->where('category','Credit Memo')->pluck('total_amount')->sum();
                        $invoiceDiscount = $invoice->items()->withTrashed()->where('category','Schedule')->pluck('discount_amount')->sum();
                        $data->push([
                            'patient_id'       => $invoice->patient->patient_id,
                            'patient_name'     => $invoice->patient->full_name,
                            'sales_person'     => $salesList,
                            'type'             => 'Proforma',
                            'invoice_no'       => $prefix.$invoice->invoice_no,
                            'invoice_date'     => \Carbon\Carbon::parse($invoice->invoice_date)->format('d-m-Y'),
                            'invoice_due_date' => $invoice->invoice_due_date->format('d-m-Y'),
                            'days_left'        => $diff,
                            'ageing'           => $ageingDiff,
                            'receipts_info'    => $receipts_info,
                            'total_amount'     => isset($invoice->total_amount)?$invoice->total_amount:'',
                            'amount_paid'      => isset($invoice->amount_paid)?$invoice->amount_paid:'',
                            'outstanding'      => $invoice->total_amount - $invoice->amount_paid,
                            'advance_amount'   => $creditMemosCost,
                            'discount'         => $invoiceDiscount,
                            'status'           => $invoice->status,
                        ]);
                        unset($receipts_info,$min,$max);
                    }else if($invoice instanceOf ServiceInvoice){
                        $data->push([
                            'patient_id'       => $invoice->patient->patient_id,
                            'patient_name'     => $invoice->patient->full_name,
                            'sales_person'     => $salesList,
                            'type'             => 'Service',
                            'invoice_no'       => $prefix.$invoice->invoice_no,
                            'invoice_date'     => \Carbon\Carbon::parse($invoice->invoice_date)->format('d-m-Y'),
                            'invoice_due_date' => '',
                            'days_left'        => '',
                            'ageing'           => '',
                            'receipts_info'    => '',
                            'total_amount'     => isset($invoice->total_amount)?$invoice->total_amount:'',
                            'amount_paid'      => '',
                            'outstanding'      => '',
                            'advance_amount'   => '',
                            'discount'         => '',
                            'status'           => '',
                        ]);
                    }
                    
                }
            }
        }
        unset($schedules);

        return \DataTables::of($data)->make(true);
    }

    static function invoiceReportWithService($request)
    { 
        $data = collect();
        $patient = $request->patient;

        if($request->ajax()){
            $invoices = Invoice::whereIn('status',['Pending','Partial']);
            if(!empty($patient) && $patient != "0"){
                $invoices = $invoices->where('patient_id',$patient);
            }


            if(!empty($request->period) && $request->period != "0" && $request->period != ""){
                $period = explode("_",$request->period);
                $invoices = $invoices->where('invoice_date','>=',$period[0])->where('invoice_date','<=',$period[1]);
            }
            $invoices = $invoices->get();
            if(count($invoices)){
                foreach ($invoices as $i => $invoice)
                {
                    $itemIds = $invoice->items()->withTrashed()->where('category','Schedule')->pluck('item_id');
                    $episodesIds = Schedule::whereIn('id',$itemIds)->pluck('lead_id')->unique();

                    $prefix = \Helper::encryptor('decrypt',session('settings.proforma_invoice_inits'));
                    $dateInv =  \Carbon\Carbon::parse($invoice->invoice_due_date);
                    $now =  \Carbon\Carbon::now();
                    $diff = $dateInv->diffForHumans($now,false,false,2);

                    if($invoice->status == 'Pending' || $invoice->status == 'Partial'){
                        $ageingDiff = date_diff(new \DateTime(), $invoice->created_at)->format("%a day(s) %h hr(s)");
                    }else{
                        $ageingDiff = '';
                    }

                    $receipts_info = '';
                    if(isset($invoice->receipts) && count($invoice->receipts)){
                        $index = 1;
                        foreach ($invoice->receipts as $receipt) {
                            $receipts_info .= nl2br(($index++).') Mode-"'.$receipt->payment_mode.'" | Ref No-"'.$receipt->reference_no.'"'.',');
                        }
                    }
                    
                    $servicedItems = $invoice->items->where('category','=','Schedule');
                    $daysArr = [];
                    if(count($servicedItems)){
                        foreach ($servicedItems as $item) {
                            $daysArr[] = $item->schedule->schedule_date;
                        }
                        $min = date('d-m-Y', min(array_map('strtotime', $daysArr)));
                        $max = date('d-m-Y', max(array_map('strtotime', $daysArr)));
                    }
                    
                    $data->push([
                        'patient_id'       => $invoice->patient->patient_id,
                        'patient_name'     => $invoice->patient->full_name,
                        'invoice_no'       => $prefix.$invoice->invoice_no,
                        'serviced_from'    => isset($min)?$min:'',
                        'serviced_to'      => isset($max)?$max:'',
                        'invoice_date'     => \Carbon\Carbon::parse($invoice->invoice_date)->format('d-m-Y'),
                        'invoice_due_date' => $invoice->invoice_due_date->format('d-m-Y'),
                        'days_left'        => $diff,
                        'ageing'           => $ageingDiff,
                        'receipts_info'    => $receipts_info,
                        'total_amount'     => isset($invoice->total_amount)?$invoice->total_amount:'',
                        'amount_paid'      => isset($invoice->amount_paid)?$invoice->amount_paid:'',
                        'outstanding'      => $invoice->total_amount - $invoice->amount_paid,
                        'status'           => $invoice->status,
                    ]);
                    unset($receipts_info,$min,$max);
                }
            }
        }
        unset($schedules);

        return \DataTables::of($data)->make(true);
    }

    static function receiptReport($request)
    { 
        $data = collect();
        $patient = $request->patient;

        if($request->ajax()){
            $receipts = Receipt::get();

            if(!empty($patient) && $patient != "0"){
                $receipts = $receipts->where('patient_id',$patient);
            }

            if(!empty($request->period) && $request->period != "0" && $request->period != ""){
                $period = explode("_",$request->period);
                // $receipts = $receipts->where('receipt_date','>=',$period[0])->whereDate('receipt_date','<=',$period[1]);
                $receipts = $receipts->filter(function ($item) use ($period) {
                    return (data_get($item, 'receipt_date') > $period[0]) && (data_get($item, 'receipt_date') < \Carbon\Carbon::parse($period[1])->endOfDay());
                });

            }

            if(count($receipts)){
                foreach ($receipts as $i => $receipt)
                {
                    if($receipt->receipt_type == 'Payment'){
                        $prefix = \Helper::encryptor('decrypt',session('settings.proforma_invoice_inits'));
                        $againstNo = $prefix.$receipt->invoice->invoice_no;
                    }else if($receipt->receipt_type == 'Memo'){
                        $againstNo = $receipt->creditMemo->credit_memo_no;
                    }else{
                        $againstNo = 'NA';
                    }
                    $data->push([
                        'patient_id'      => $receipt->patient->patient_id,
                        'patient_name'    => $receipt->patient->full_name,
                        'receipt_no'      => $receipt->receipt_no,
                        'type'            => $receipt->receipt_type,
                        'receipt_date'    => \Carbon\Carbon::parse($receipt->receipt_date)->format('d-m-Y'),
                        'receipt_against' => $againstNo,
                        'payment_mode'    => $receipt->payment_mode,
                        'reference_no'    => $receipt->reference_no,
                        'total_amount'    => isset($receipt->receipt_amount)?$receipt->receipt_amount:'',
                    ]);
                }
            }
        }

        return \DataTables::of($data)->make(true);
    }
}
