<?php
namespace App\Helpers\Report;

use App\Entities\Caregiver\Caregiver;
use \App\Entities\Attendance;
use App\Entities\WorkLogFeedback;
use Carbon\Carbon;
class HR {

    static function masterData($request)
    {
        $data = collect();

        if($request){
            $data = collect(Caregiver::with('professional')->with('account')->with('accommodation')->with('payroll')->get());
        }

        return $data;
    }

    static function attendanceReport($request)
    {
        ini_set('max_execution_time', 3000);
        $filters = $request->all('filter_user','filter_date');

        $filter = explode("_",$filters['filter_user']);

        $date = !empty($filters['filter_date'])?\Carbon\Carbon::parse($filters['filter_date'])->format('Y-m-d'):date('Y-m-d');

        $caregiver = Caregiver::active()->select('id','employee_id','first_name','middle_name','last_name','mobile_number')->whereHas('attendance',function($q)use($date){
            $q->where('date',$date)->whereIn('status',['P','A']);
        });
        $employees = $caregiver->get();
        $departments = \App\Entities\Masters\Department::all();

        if(!empty($filters['filter_user'])){
            if($filter[0] == 'dept'){
                $caregiver->whereHas('professional', function($q) use($filter){
                    $q->whereDepartment($filter[1]);
                });
            }
            if($filter[0] == 'emp'){
                $caregiver->whereId($filter[1]);
            }
        }
        $caregivers = $caregiver->get();
        return compact('employees','departments','caregivers','filters','date');
    }

    static function leavesStatsReport($request)
    {
        ini_set('memory_limit', '1024M'); // or you could use 1G
        ini_set('max_execution_time', '3000'); // or you could use 1G

        $filters = $request->all('from_date','to_date');

        $from = !empty($filters['from_date'])?\Carbon\Carbon::parse($filters['from_date'])->format('Y-m-d'):date('Y-m-d');
        $to = !empty($filters['to_date'])?\Carbon\Carbon::parse($filters['to_date'])->format('Y-m-d'):date('Y-m-d');
        $totalDays = \Carbon\Carbon::parse($filters['from_date'])->diffInDays(\Carbon\Carbon::parse($filters['to_date'])->addDay(1));

        $caregiver = Caregiver::select('caregivers.id','caregivers.employee_id','caregivers.first_name','caregivers.middle_name','caregivers.last_name')
        ->where('caregivers.work_status','!=','Training')
        ->join('caregiver_professional_details', 'caregivers.id', '=', 'caregiver_professional_details.caregiver_id')
        ->where(function($t) use ($to){
            $t->whereDate('caregiver_professional_details.date_of_joining','<=',$to)
            ->orWhereNull('caregiver_professional_details.date_of_joining');
        })
        ->where(function($f) use ($from){
            $f->whereDate('caregiver_professional_details.resignation_date','>=',$from)
            ->orWhereNull('caregiver_professional_details.resignation_date');
        });

        $employees = $caregiver->get();

        $caregivers = $caregiver->get();

        $leaves = \DB::table('employee_leaves')
                    ->select('id','caregiver_id','type')
                    ->whereStatus('Approved')
                    ->selectRaw(\DB::raw('SUM(DATEDIFF(least(date, "'.$to.'"),greatest(date, "'.$from.'")) + 1) as days'))
                    ->where('date','<=',$to)
                    ->where('date','>=',$from)
                    ->where('deleted_at','=',null)
                    ->groupBy('id','type');
        

        $leaves = collect($leaves->get());

        $leaves = $leaves->groupBy(function($key){
            return $key->caregiver_id;
        });
        
        $leaves = $leaves->map(function ($item){
            return $item->groupBy('type')->map(function($item){
                     return $item->sum('days');
                    });
        })->toArray();

        $excludedIds = array_keys($leaves);

        $excludedEmployees = $employees->whereNotIn('id',$excludedIds);

        return compact('employees','departments','caregivers','filters','excludedEmployees','leaves','totalDays');
    }

    static function attendanceSummary($request)
    {
        $data = [];
        $heads = ['total', 'effective_utilization', 'utilization_nc', 'bench', 'off_compoff', 'leave', 'absence', 'training'];
        $params = ['pca_male' => 0, 'pca_female' => 0, 'srpca_male' => 0, 'srpca_female' => 0, 'rn_male' => 0, 'rn_female' => 0];
        foreach ($heads as $val) {
            $data[$val] = $params;
        }

        // Specialization Ids
        $specialization  = \App\Entities\Masters\Specialization::select('id','specialization_name')
                                ->whereIn('specialization_name',['PCA','Sr.PCA','RN']);
        $specializations = $specialization->pluck('id','specialization_name')->toArray();
        $specializationIDs = $specialization->pluck('id')->toArray();

        // 1. Effective Utilization
        $attendance = \App\Entities\Attendance::whereIn('status',['P','A','L','O','CO'])->with('caregiver')->with('caregiver.professional');
        $attendance = $attendance->whereHas('caregiver.professional', function($q) use($specializationIDs){
                        $q->whereIn('specialization',$specializationIDs);
                     })->get();

        $data['effective_utilization']['pca_male'] = $attendance->where('caregiver.professional.specialization',$specializations["PCA"])
                                                               ->where('caregiver.gender','Male')->count();
        $data['effective_utilization']['pca_female'] = $attendance->where('caregiver.professional.specialization',$specializations["PCA"])
                                                              ->where('caregiver.gender','Female')->count();

        $data['effective_utilization']['srpca_male'] = $attendance->where('caregiver.professional.specialization',$specializations["Sr.PCA"])
                                                             ->where('caregiver.gender','Male')->count();
        $data['effective_utilization']['srpca_female'] = $attendance->where('caregiver.professional.specialization',$specializations["Sr.PCA"])
                                                            ->where('caregiver.gender','Female')->count();

        $data['effective_utilization']['rn_male'] = $attendance->where('caregiver.professional.specialization',$specializations["RN"])
                                                               ->where('caregiver.gender','Male')->count();
        $data['effective_utilization']['rn_female'] = $attendance->where('caregiver.professional.specialization',$specializations["RN"])
                                                              ->where('caregiver.gender','Female')->count();


        // 3. Bench
        $caregivers = \App\Entities\Caregiver\Caregiver::whereWorkStatus('Available')->with('professional');
        $caregivers = $caregivers->whereHas('professional', function($q) use($specializationIDs){
                        $q->whereIn('specialization',$specializationIDs);
                     })->get();

        $data['bench']['pca_male'] = $caregivers->where('professional.specialization',$specializations["PCA"])
                                                            ->where('gender','Male')->count();
        $data['bench']['pca_female'] = $caregivers->where('professional.specialization',$specializations["PCA"])
                                                           ->where('gender','Female')->count();

        $data['bench']['srpca_male'] = $caregivers->where('professional.specialization',$specializations["Sr.PCA"])
                                                          ->where('gender','Male')->count();
        $data['bench']['srpca_female'] = $caregivers->where('professional.specialization',$specializations["Sr.PCA"])
                                                         ->where('gender','Female')->count();

        $data['bench']['rn_male'] = $caregivers->where('professional.specialization',$specializations["RN"])
                                                            ->where('gender','Male')->count();
        $data['bench']['rn_female'] = $caregivers->where('professional.specialization',$specializations["RN"])
                                                           ->where('gender','Female')->count();

        // 4. Off & Comp-Off
        $data['off_compoff']['pca_male'] = $attendance->where('caregiver.professional.specialization',$specializations["PCA"])
                                                            ->where('caregiver.gender','Male')->whereIn('status',['O','CO'])->count();
        $data['off_compoff']['pca_female'] = $attendance->where('caregiver.professional.specialization',$specializations["PCA"])
                                                           ->where('caregiver.gender','Female')->whereIn('status',['O','CO'])->count();

        $data['off_compoff']['srpca_male'] = $attendance->where('caregiver.professional.specialization',$specializations["Sr.PCA"])
                                                          ->where('caregiver.gender','Male')->whereIn('status',['O','CO'])->count();
        $data['off_compoff']['srpca_female'] = $attendance->where('caregiver.professional.specialization',$specializations["Sr.PCA"])
                                                         ->where('caregiver.gender','Female')->whereIn('status',['O','CO'])->count();

        $data['off_compoff']['rn_male'] = $attendance->where('caregiver.professional.specialization',$specializations["RN"])
                                                            ->where('caregiver.gender','Male')->whereIn('status',['O','CO'])->count();
        $data['off_compoff']['rn_female'] = $attendance->where('caregiver.professional.specialization',$specializations["RN"])
                                                           ->where('caregiver.gender','Female')->whereIn('status',['O','CO'])->count();

        // 5. Leave
        $data['leave']['pca_male'] = $attendance->where('caregiver.professional.specialization',$specializations["PCA"])
                                                            ->where('caregiver.gender','Male')->where('status','L')->count();
        $data['leave']['pca_female'] = $attendance->where('caregiver.professional.specialization',$specializations["PCA"])
                                                           ->where('caregiver.gender','Female')->where('status','L')->count();

        $data['leave']['srpca_male'] = $attendance->where('caregiver.professional.specialization',$specializations["Sr.PCA"])
                                                          ->where('caregiver.gender','Male')->where('status','L')->count();
        $data['leave']['srpca_female'] = $attendance->where('caregiver.professional.specialization',$specializations["Sr.PCA"])
                                                         ->where('caregiver.gender','Female')->where('status','L')->count();

        $data['leave']['rn_male'] = $attendance->where('caregiver.professional.specialization',$specializations["RN"])
                                                            ->where('caregiver.gender','Male')->where('status','L')->count();
        $data['leave']['rn_female'] = $attendance->where('caregiver.professional.specialization',$specializations["RN"])
                                                           ->where('caregiver.gender','Female')->where('status','L')->count();

        // 6. Absence
        $data['absence']['pca_male'] = $attendance->where('caregiver.professional.specialization',$specializations["PCA"])
                                                           ->where('caregiver.gender','Male')->where('status','A')->count();
        $data['absence']['pca_female'] = $attendance->where('caregiver.professional.specialization',$specializations["PCA"])
                                                          ->where('caregiver.gender','Female')->where('status','A')->count();

        $data['absence']['srpca_male'] = $attendance->where('caregiver.professional.specialization',$specializations["Sr.PCA"])
                                                         ->where('caregiver.gender','Male')->where('status','A')->count();
        $data['absence']['srpca_female'] = $attendance->where('caregiver.professional.specialization',$specializations["Sr.PCA"])
                                                        ->where('caregiver.gender','Female')->where('status','A')->count();

        $data['absence']['rn_male'] = $attendance->where('caregiver.professional.specialization',$specializations["RN"])
                                                           ->where('caregiver.gender','Male')->where('status','A')->count();
        $data['absence']['rn_female'] = $attendance->where('caregiver.professional.specialization',$specializations["RN"])
                                                          ->where('caregiver.gender','Female')->where('status','A')->count();

        return $data;
    }

    static function attendanceWorkTime($request)
    {
        $month = !empty($request['filter_month'])?$request['filter_month']:date('m');
        $year = !empty($request['filter_year'])?$request['filter_year']:date('Y');

        $userAttendance = \App\Entities\Attendance::whereNotNull('schedule_id')->whereMonth('date',$month)->whereYear('date',$year)->whereNotNull('punch_in')->whereNotNull('punch_out');

        $caregiverIDs = $userAttendance->pluck('caregiver_id')->unique();

        $caregivers = Caregiver::select('id','employee_id','first_name','middle_name','last_name')->whereIn('id',$caregiverIDs)->get();

        $userAttendance = $userAttendance->get()->toArray();
        session()->put('attendance_res',$userAttendance);

        return $caregivers;
    }

    static function CustomerFeedback($request)
    {
        $data = collect();
        if($request->ajax()){
            $worklogs = WorkLogFeedback::select('*');
        }

        if(!empty($request->status) && $request->status != "0"){
            $worklogs->whereStatus($request->status);
        }

        if(!empty($request->period) && $request->period != "0" && $request->period != ""){
            $period = explode("_", $request->period);
            $worklogs->where('created_at','>=',$period[0])->where('created_at','<=',$period[1]);
        }
        $worklogs = $worklogs->get();

        if(count($worklogs)){
            $i = 1;
            foreach ($worklogs as $worklog) 
            {
                $data->push([
                    'count' => $i++,
                    'name' => $worklog->worklog->caregiver->full_name.'-'.$worklog->worklog->caregiver->employee_id,
                    'patient_name' => $worklog->lead->patient->full_name.'-'.$worklog->lead->patient->patient_id,
                    'schedule_id' => $worklog->worklog->schedule->schedule_id,
                    'schedule_date' => \Carbon\Carbon::parse($worklog->worklog->schedule->schedule_date )->format('d-m-Y'),
                    'caring_and_patience'   => $worklog->caring_and_patience,
                    'knowledge' => $worklog->knowledge,
                    'cleanliness' => $worklog->cleanliness,
                    'reliabilty_and_punctuality' => $worklog->reliabilty_and_punctuality,
                    'communication_with_patient' => $worklog->communication_with_patient,
                    'over_all_rating' => $worklog->over_all_rating,
                    'comment' => $worklog->comment,
                    'action' => $worklog->action,
                    'status' => $worklog->status,
                ]);
            }
        }
        unset($worklogs);
        return \DataTables::of($data)->make(true);
    }
}
