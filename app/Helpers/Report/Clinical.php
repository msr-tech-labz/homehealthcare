<?php
namespace App\Helpers\Report;

use App\Entities\Schedule;
use App\Entities\CaseServices;
use App\Entities\Masters\Service;
use App\Entities\CaseBillables;
use App\Entities\CaseDisposition;
use App\Entities\Lead;
use App\Entities\Patient;


class Clinical {

    static function serviceSummary($request)
    {
        $data = [];
        $servicesList = [];

        $services = Service::get();
        if(count($services)){
            foreach ($services as $service) {
                $servicesList[$service->id] = [
                    'service_name' => $service->service_name,
                    'count' => 0,
                    'mtd' => 0,
                    'ytd' => 0,
                ];
            }
        }

        $filters = $request->only('filter_period');

        $currentDate = empty($filters['filter_period'])?date('Y-m-d'):$filters['filter_period'];

        $schedules = Schedule::whereHas('service')->select('schedule_date','service_required')->whereStatus('Completed')->get();

        if(count($schedules)){
            $s = collect($schedules);
            foreach ($s->groupBy('service_required') as $key => $value) {
                if(count($value)){
                    $today = $value->filter(function($v, $key) use ($currentDate){
                        return $v->schedule_date->format('Y-m-d') == $currentDate;
                    })->count();
                    $mtd = $value->where('schedule_date','>=',date('Y-m-d',strtotime('first day of '.date('M').' '.date('Y'))))->where('schedule_date','<=',$currentDate)->count();
                    $ytd = $value->where('schedule_date','>=',date('Y-m-d',strtotime('first day of January '.date('Y'))))->where('schedule_date','<=',$currentDate)->count();

                    $data[$key] = [
                        'service_name' => \Helper::getServiceName($key),
                        'count' => $today,
                        'mtd' => $mtd,
                        'ytd' => $ytd,
                    ];
                }
            }  
            $data = array_replace_recursive($servicesList, $data);
        }
        return collect($data)->sortBy('service_name');
    }

    static function renewableServiceOrders($request, $onlySchedules=false)
    {
        ini_set('memory_limit', '1024M'); // or you could use 1G
        ini_set('max_execution_time', '3000'); // or you could use 1G
        $data = collect();

        $period = $request->period;
        if($request->ajax() && $period){
            $serviceOrders = CaseServices::with('patient')->with('lead')->with('service')
                                ->whereStatus('Pending')
                                ->whereDate('to_date', '<', date('Y-m-d') );
            if(!empty($period) && $period != "0" && $period != ""){
                $period = explode("_",$period);
                $serviceOrders->whereRaw(\DB::raw('DATE(to_date) BETWEEN "'.$period[0].'" AND "'.$period[1].'"'));
            }

            $serviceOrders = $serviceOrders->orderBy('to_date','ASC')->get();
            foreach ($serviceOrders as $so) {
                $data->push([
                    'date' => $so->created_at->format('d-m-Y'),
                    'episode_id' => $so->lead->episode_id,
                    'patient_name' => $so->patient->full_name,
                    'patient_phone' => $so->patient->contact_number,
                    'service_requested' => $so->service->service_name,
                    'from_date' => $so->from_date->format('d-m-Y'),
                    'to_date' => $so->to_date->format('d-m-Y'),
                    'status' => $so->status                            
                ]);
            }
        }
        return \DataTables::of($data)->make(true);
    }

    static function unconfirmedSchedules($request, $onlySchedules=false)
    {
        ini_set('memory_limit', '1024M'); // or you could use 1G
        ini_set('max_execution_time', '3000'); // or you could use 1G
        $data = collect();

        $period = $request->period;
        if($request->ajax() && $period){
            $schedules = Schedule::with('patient')->with('lead')->with('service')
                                ->whereStatus('Pending')
                                ->whereDate('schedule_date', '<', date('Y-m-d') );
            if(!empty($period) && $period != "0" && $period != ""){
                $period = explode("_",$period);
                $schedules->whereRaw(\DB::raw('DATE(schedule_date) BETWEEN "'.$period[0].'" AND "'.$period[1].'"'));
            }

            $schedules = $schedules->orderBy('schedule_date','ASC')->get();
            foreach ($schedules as $schedule) {
                $data->push([
                    'date' => $schedule->schedule_date->format('d-m-Y'),
                    'episode_id' => $schedule->lead->episode_id,
                    'patient_name' => $schedule->patient->full_name,
                    'patient_phone' => $schedule->patient->contact_number,
                    'service_requested' => $schedule->service->service_name,
                    'caregiver' => isset($schedule->caregiver)?$schedule->caregiver->full_name:'Unallocated',
                    'status' => $schedule->status
                ]);
            }
        }
        return \DataTables::of($data)->make(true);
    }

    static function allSchedulesData($request)
    {
        ini_set('memory_limit', '1024M'); // or you could use 1G
        ini_set('max_execution_time', '3000'); // or you could use 1G
        $data = collect();
        $patient = $request->patient;
        $period = $request->period;
        if($request->ajax() && $period){
            $schedules = Schedule::with(['lead','service.ratecard.tax','patient','caregiver.professional.designations','serviceRequest'])
            ->whereHas('lead', function($q){
                $q->whereIn('status',['Converted','Closed']);
            });

            if(!empty($patient) && $patient != "0"){
                $schedules->wherePatientId((int) $patient);
            }

            if(!empty($period) && $period != "0" && $period != ""){
                $period = explode("_",$period);
                $schedules->whereBetween('schedule_date',[$period[0],$period[1]]);
            }

            $schedules = $schedules->orderBy('schedule_date','ASC')->get();

            if(count($schedules)){
                foreach ($schedules as $schedule) 
                {
                    // $rate = $schedule->serviceRequest->gross_rate;
                    $grossRate = $schedule->service->ratecard->amount;
                    $netRate = $schedule->amount;
                    $taxRate = $schedule->service->ratecard->tax->tax_rate;
                    $taxedAmount = ($grossRate)*(($taxRate)/100);
                    $discount = $grossRate - $netRate;
                    $data->push([
                        'date'              => $schedule->schedule_date->format('d-m-Y'),
                        'episode_id'        => $schedule->caserecord->episode_id,
                        'patient_name'      => $schedule->patient->full_name,
                        'patient_phone'     => $schedule->patient->contact_number,
                        'service_requested' => $schedule->service->service_name,
                        'employee_id'       => isset($schedule->caregiver)?$schedule->caregiver->employee_id:'',
                        'employee_name'     => isset($schedule->caregiver)?$schedule->caregiver->full_name:'',
                        'number'            => isset($schedule->caregiver)?$schedule->caregiver->mobile_number:'',
                        'designation'       => isset($schedule->caregiver->professional->designations)?$schedule->caregiver->professional->designations->designation_name:'',
                        'chargeable'        => $schedule->chargeable?'Yes':'No',
                        'gross_rate'        => number_format($grossRate,2),
                        'net_rate'          => number_format($netRate,2),
                        'discount'          => number_format($discount,2),
                        'status'            => $schedule->status
                    ]);
                }
            }
        }
        unset($schedules);

        return \DataTables::of($data)->make(true);
    }

    static function getBillablesReport($request)
    {
        $data = collect();
        $patient = $request->patient;
        $period = $request->period;
        if($request->ajax()){
            $billables = CaseBillables::select('*');
            if(!empty($patient) && $patient != "0"){
                $billables->wherePatientId((int) $patient);
            }
            if(!empty($period) && $period != "0" && $period != ""){
                $period = explode("_",$period);
                $billables->whereDate('created_at','>=',$period[0])->whereDate('created_at','<=',$period[1]);
            }
            $billables = $billables->orderBy('created_at','ASC')->get();
            if(count($billables)){
                foreach ($billables as $billable) 
                {
                    switch ($billable->category) {
                        case 'Consumables':
                        $item_name = $billable->consumable->consumable_name;
                        $item_tax = $billable->consumable->tax->tax_rate.' %';
                        break;
                        case 'Equipments':
                        $item_name = $billable->surgical->surgical_name;
                        $item_tax = $billable->surgical->tax->tax_rate.' %';
                        break;
                        default:
                        $item_name = $billable->item;
                        $item_tax = '-';
                        break;
                    }
                    $data->push([
                        'date_created' => $billable->created_at->format('d-m-Y'),
                        'patient_id' => $billable->patient->patient_id,
                        'patient_name' => $billable->patient->full_name,
                        'item_type' => $billable->category,
                        'item' => $item_name,
                        'tax_per_item' => $item_tax,
                        'item_price' => $billable->rate,
                        'quantity' => $billable->quantity,
                        'amount' => $billable->amount,
                        'proforma' => $billable->proforma_status,
                        'service' => $billable->service_status,
                    ]);
                }
            }
        }
        unset($billables);
        return \DataTables::of($data)->make(true);
    }
    
    static function getClientNoServiceDays($request){
        $data = collect();
        $patient = $request->patient;
        $period = $request->period;
        if($request->ajax()){
            $schedules = Schedule::select('*')->whereStatus('Pending');
        
            if(!empty($patient) && $patient != "0"){
                $schedules->wherePatientId((int) $patient);
            }
            if(!empty($period) && $period != "0" && $period != ""){
                $period = explode("_",$period);
                $schedules->whereDate('schedule_date','>=',$period[0])->whereDate('schedule_date','<=',$period[1]);
            }
            $schedules = $schedules->orderBy('created_at','ASC')->get();

            if(count($schedules)){
                foreach ($schedules as $schedule) 
                {
                    $str= $schedule->lead->manager_id;
                    $arr = explode(",", $str);
                    if($str){
                        foreach($arr as $a){
                            $name[] =   \Helper::checkManagers($a);
                        }
                    }else{
                        $name = "-";
                    }
                    $data->push([
                        'schedule_date' => $schedule->schedule_date->format('d-m-Y'),
                        'schedule_id' => $schedule->schedule_id,
                        'patient_id' => $schedule->patient->patient_id,
                        'patient_name' => $schedule->patient->full_name,
                        'caregiver'   =>isset($schedule->caregiver_id)?$schedule->caregiver->first_name.' '.$schedule->caregiver->last_name:'NA',
                        'start_time' => \Carbon\Carbon::parse($schedule->start_time)->format('h:i A'),
                        'end_time' => \Carbon\Carbon::parse($schedule->end_time)->format('h:i A'),
                        'manager' => $name,
                    ]);
                    unset($name);
                }
            }
        }
        unset($billables);
        return \DataTables::of($data)->make(true);
    }

    static function convertedLeadsReport($request)
    {
        $data = collect();
        $period = $request->period;
        $status = $request->status;
        if($request->ajax()){
            $dispositions = CaseDisposition::select('*')->whereStatus($status);
            if($status == "Converted"){
                $dispositions->whereConverted(1);
            }
            
            if(!empty($period) && $period != "0" && $period != ""){
                $period = explode("_",$period);
                $dispositions->where('disposition_date','>=',$period[0])->where('disposition_date','<=',$period[1]);
            }
            $dispositions = $dispositions->orderBy('disposition_date','ASC')->groupby('lead_id')->distinct()->get();
            if(count($dispositions)){
                $i = 1;
                foreach ($dispositions as $disposition) 
                {
                    if($status == "Converted"){
                        $comment = CaseDisposition::whereLeadId($disposition->lead_id)->whereStatus('Closed')->first();
                        if($comment){
                            $comment = ' Closed on '.$comment->disposition_date->format('d-m-Y');
                        }
                    }elseif($status == "Closed"){
                        $comment = CaseDisposition::whereLeadId($disposition->lead_id)->whereStatus('Converted')->whereConverted(1)->first();
                        if($comment){
                            $comment = ' Converted on '.$comment->disposition_date->format('d-m-Y');
                        }   
                    }else{
                        $comment = CaseDisposition::whereLeadId($disposition->lead_id)->whereStatus($status)->first();
                        if($comment){
                            $comment = $comment->comment;
                        } 
                    }

                    if(!$comment){
                        $comment = '';
                    }
                    $patient = Patient::find(Lead::whereId($disposition->lead_id)->pluck('patient_id')->first());
                    $data->push([
                        'count' => $i++,
                        'patient_id' => $patient->patient_id,
                        'patient_name' => $patient->first_name.' '.$patient->last_name,
                        'date' => $disposition->disposition_date->format('d-m-Y'),
                        'comment' => $comment,
                    ]);
                }
            }
        }
        unset($dispositions);
        return \DataTables::of($data)->make(true);
    }
}
