<?php
namespace App\Helpers\Report;

use App\Entities\Masters\ManagementComposition;
use App\Entities\Masters\Designation;

class MIS {

    static function leadStats($request)
    {
    	$date = $request['date'];
    	$requestDate = "";
    	ini_set('max_execution_time', 3000); //300 seconds = 5 minutes
    	$data = [];
    	$data['graph'] = [];
    	$data['graph']['labels'] = ["Pending", "Converted", "Closed", "Dropped"];
    	$todayArrLead = $mtdArrLead = $ytdArrLead = [];
    	if(!empty($date)){
    	    $requestDate = $date;
    	}

    	$todayArrLead = self::getLeadsByPeriod('Today', $requestDate);
    	$mtdArrLead = self::getLeadsByPeriod('MTD', $requestDate);
    	$ytdArrLead = self::getLeadsByPeriod('YTD', $requestDate);

    	$data['graph']['datasets'][] = [
    	        'label' => 'TODATE',
    	        'data' => $todayArrLead,
    	        'backgroundColor' => 'rgb(255, 153, 51)'
    	    ];
    	$data['graph']['datasets'][] = 
    	    [
    	        'label' => 'MTD',
    	        'data' => $mtdArrLead,
    	        'backgroundColor' => 'rgb(56, 133, 157)'
    	    ];
    	$data['graph']['datasets'][] = [
    	        'label' => 'YTD',
    	        'data' => $ytdArrLead,
    	        'backgroundColor' => 'rgb(60, 179, 113)'
    	    ];       
    	return $data;
    }

    static function getLeadsByPeriod($period, $date)
    {
        if(empty($date)){
            $date = null;
        }
        $data = ['pending' => 0, 'converted' => 0, 'closed' => 0, 'dropped' => 0];

        $leadsAll = \App\Entities\Lead::withoutGlobalScopes()->select('id','status','created_at');

        if(empty($date)){
            //Setting Financial Year On Page Load
            if(date('m') <= 3){
                $fyStart = date((date('Y') - 1).'-04-01');
                $fyEnd = date('Y-m-d',strtotime("+1 days"));
            }else{
                $fyStart = date('Y-04-01');
                $fyEnd = date('Y-m-d',strtotime("+1 days"));
            }

            if($period == 'Today'){
                $leads = $leadsAll->whereDate('created_at',date('Y-m-d',strtotime("-1 days")));
            }
            if($period == 'MTD'){
                $leads = $leadsAll->whereYear('created_at',date('Y'))->whereMonth('created_at',date('m'))->whereDate('created_at','<=',date('Y-m-d',strtotime("-1 days")));
            }
            if($period == 'YTD'){
                $leads = $leadsAll->whereBetween('created_at',[$fyStart,$fyEnd]);
            }
        }else{
            $dateParsed = \Carbon\Carbon::parse($date);
            $dateParsedMonth = $dateParsed->format('m');
            $dateParsedYear = $dateParsed->format('Y');
            //Setting Financial Year On Filter
            if($dateParsedMonth <= 3){
                $fyStart = date(($dateParsedYear - 1).'-04-01');
                $fyEnd = \Carbon\Carbon::parse($date)->addDay()->format('Y-m-d');
            }else{
                $fyStart = date($dateParsedYear.'-04-01');
                $fyEnd = \Carbon\Carbon::parse($date)->addDay()->format('Y-m-d');
            }

            if($period == 'Today'){
                $leads = $leadsAll->whereDate('created_at',$dateParsed->format('Y-m-d'));
            }
            if($period == 'MTD'){
                $leads = $leadsAll->whereYear('created_at',$dateParsed->format('Y'))->whereMonth('created_at',$dateParsed->format('m'))->whereDate('created_at','<=',$dateParsed->format('Y-m-d'));
            }
            if($period == 'YTD'){
                $leads = $leadsAll->whereBetween('created_at',[$fyStart,$fyEnd]);
            }
        }

		$pending   = clone $leads;
		$converted = clone $leads;
		$closed    = clone $leads;
		$dropped   = clone $leads;

		$data['pending']   = $pending->whereStatus('Pending')->count();
		$data['converted'] = $converted->whereStatus('Converted')->count();
		$data['closed']    = $closed->whereStatus('Closed')->count();
		$data['dropped']   = $dropped->whereStatus('Dropped')->count();

        return collect($data)->flatten()->all();
    }
    
    static function hrStats($request)
    {
        if(empty($request['date'])){
            $date = \Carbon\Carbon::parse($request['date'])->subDays(1)->format('Y-m-d');
        }else{
            $date = \Carbon\Carbon::parse($request['date'])->format('Y-m-d');
        }

        $data = [];

        $cadre = explode(',',ManagementComposition::whereType('mis_hr')->value('composition_cadre'));
        $staff = \App\Entities\Attendance::whereDate('date',$date)->distinct('caregiver_id')->whereHas('caregiver.user', function($q){
                                                $q->where('status',1);
                                            })->whereHas('caregiver.professional', function($q) use ($cadre){
                                                $q->select('caregiver_id','employment_type')->whereRole('Caregiver')->whereIn('designation',$cadre);
                                            });
        $total = clone $staff;
        $onduty = clone $staff;
        $training = clone $staff;
        $leave = clone $staff;
        $absent = clone $staff;
        $available = clone $staff;

        $data['total'] = $total->get()->count();
        $data['onduty'] = $onduty->where('schedule_id',null)->get()->count();
        $data['training'] = $training->whereHas('caregiver', function($q){
                                $q->where('work_status','Training');
                            })->count();
        $nonLeaveStats = ['A','P'];
        $data['leave'] = $leave->whereNotIn('status', $nonLeaveStats)->get()->count();
        $data['absent'] = $absent->where('status', 'A')->get()->count();
        $data['available'] = $available->where('schedule_id',null)->whereStatus('P')->get()->count() - $data['training'];
        
        foreach($cadre as $cd){
            $cadreName = strtoupper(Designation::whereId($cd)->value('designation_name'));
            $staffCadre = \App\Entities\Attendance::whereDate('date',$date)->distinct('caregiver_id')->whereHas('caregiver.user', function($q){
                                $q->where('status',1);
                            })->whereHas('caregiver.professional', function($q) use ($cd){
                                $q->select('caregiver_id','employment_type')->whereRole('Caregiver')->where('designation',$cd);
                            });

            $totalCadreCount = clone $staffCadre;
            $totalCadreMale = clone $staffCadre;
            $totalCadreFemale = clone $staffCadre;

            $onDutyCadreCount = clone $staffCadre;
            $onDutyCadreMale = clone $staffCadre;
            $onDutyCadreFemale = clone $staffCadre;

            $trainingCadreCount = clone $staffCadre;
            $trainingCadreMale = clone $staffCadre;
            $trainingCadreFemale = clone $staffCadre;
            
            $leaveCadreCount = clone $staffCadre;
            $leaveCadreMale = clone $staffCadre;
            $leaveCadreFemale = clone $staffCadre;

            $absentCadreCount = clone $staffCadre;
            $absentCadreMale = clone $staffCadre;
            $absentCadreFemale = clone $staffCadre;

            $availableCadreCount = clone $staffCadre;
            $availableCadreMale = clone $staffCadre;
            $availableCadreFemale = clone $staffCadre;


            $totalCadre = $totalCadreCount->get()->count();
            $totalCadreMale = $totalCadreMale->whereHas('caregiver', function($q){
                                    $q->where('gender','Male');
                                })->count();
            $totalCadreFemale = $totalCadreFemale->whereHas('caregiver', function($q){
                                    $q->where('gender','Female');
                                })->count();


            $onDutyCadre = $onDutyCadreCount->where('schedule_id',null)->get()->count();

            $onDutyCadreMale = $onDutyCadreMale->where('schedule_id',null)->whereHas('caregiver', function($q){
                                    $q->where('gender','Male');
                                })->count();
            $onDutyCadreFemale = $onDutyCadreFemale->where('schedule_id',null)->whereHas('caregiver', function($q){
                                    $q->where('gender','Female');
                                })->count();

            $trainingCadre = $trainingCadreCount->whereHas('caregiver', function($q){
                                    $q->where('work_status','Training');
                                })->count();
            $trainingCadreMale = $trainingCadreMale->whereHas('caregiver', function($q){
                                    $q->where('work_status','Training')->where('gender','Male');
                                })->count();
            $trainingCadreFemale = $trainingCadreFemale->whereHas('caregiver', function($q){
                                    $q->where('work_status','Training')->where('gender','Female');
                                })->count();

            $leaveCadre = $leaveCadreCount->whereNotIn('status', $nonLeaveStats)->get()->count();
            $leaveCadreMale = $leaveCadreMale->whereNotIn('status', $nonLeaveStats)->whereHas('caregiver', function($q){
                                    $q->where('gender','Male');
                                })->count();
            $leaveCadreFemale = $leaveCadreFemale->whereNotIn('status', $nonLeaveStats)->whereHas('caregiver', function($q){
                                    $q->where('gender','Female');
                                })->count();

            $absentCadre =  $absentCadreCount->where('status', 'A')->get()->count();
            $absentCadreMale = $absentCadreMale->where('status', 'A')->whereHas('caregiver', function($q){
                                    $q->where('gender','Male');
                                })->count();
            $absentCadreFemale = $absentCadreFemale->where('status', 'A')->whereHas('caregiver', function($q){
                                    $q->where('gender','Female');
                                })->count();

            $availableCadre = $availableCadreCount->where('schedule_id',null)->whereStatus('P')->get()->count() - $trainingCadre;
            $availableCadreMale = $availableCadreMale->where('schedule_id',null)->whereStatus('P')->whereHas('caregiver', function($q){
                                    $q->where('gender','Male');
                                })->count() - $trainingCadreMale;
            $availableCadreFemale = $availableCadreFemale->where('schedule_id',null)->whereStatus('P')->whereHas('caregiver', function($q){
                                    $q->where('gender','Female');
                                })->count() - $trainingCadreFemale;

            $data['cadre'][$cadreName] = [
                '1' => ($availableCadre)?$availableCadre.' (<span style="color:blue;">M</span>-'.$availableCadreMale.' | <span style="color:deeppink;">F</span>-'.$availableCadreFemale.')':$availableCadre,
                '2' => ($onDutyCadre)?$onDutyCadre.' (<span style="color:blue;">M</span>-'.$onDutyCadreMale.' | <span style="color:deeppink;">F</span>-'.$onDutyCadreFemale.')':$onDutyCadre,
                '3' => ($trainingCadre)?$trainingCadre.' (<span style="color:blue;">M</span>-'.$trainingCadreMale.' | <span style="color:deeppink;">F</span>-'.$trainingCadreFemale.')':$trainingCadre,
                '4' => ($leaveCadre)?$leaveCadre.' (<span style="color:blue;">M</span>-'.$leaveCadreMale.' | <span style="color:deeppink;">F</span>-'.$leaveCadreFemale.')':$leaveCadre,
                '5' => ($absentCadre)?$absentCadre.' (<span style="color:blue;">M</span>-'.$absentCadreMale.' | <span style="color:deeppink;">F</span>-'.$absentCadreFemale.')':$absentCadre
            ];
        }

        return $data;
    }
    
    static function servicesStats($request)
    {
        if(empty($request['date'])){
            $date = \Carbon\Carbon::parse($request['date'])->subDays(1);
            //Setting Financial Year On Page Load
            if(date('m') <= 3){
                $fyStart = date((date('Y') - 1).'-04-01');
                $fyEnd = date('Y-m-d',strtotime("-1 days"));
            }else{
                $fyStart = date('Y-04-01');
                $fyEnd = date('Y-m-d',strtotime("-1 days"));
            }
            $fyEndCa = date('Y-m-d',strtotime("+1 days"));
        }else{
            $date = \Carbon\Carbon::parse($request['date']);;
            $dateParsedMonth = $date->format('m');
            $dateParsedYear = $date->format('Y');

            //Setting Financial Year On Filter
            if($dateParsedMonth <= 3){
                $fyStart = date(($dateParsedYear - 1).'-04-01');
                $fyEnd = \Carbon\Carbon::parse($request['date'])->format('Y-m-d');
            }else{
                $fyStart = date($dateParsedYear.'-04-01');
                $fyEnd = \Carbon\Carbon::parse($request['date'])->format('Y-m-d');
            }
            $fyEndCa = \Carbon\Carbon::parse($request['date'])->addDay()->format('Y-m-d');
        }

        $data = [];
        $services = \App\Entities\Schedule::whereBetween('schedule_date',[$fyStart,$fyEnd])->distinct('service_required')->pluck('service_required');
        if(count($services)){
            // Graph datasets
            $data['graph'] = [];
            $todayArrShifts = $mtdArrShifts = $ytdArrShifts = $todayArrRevenue = $mtdArrRevenue = $ytdArrRevenue = $todayAverageRevenue = $mtdAverageRevenue = $ytdAverageRevenue = [];
            foreach ($services as $service) {  
            //For Todate Service Revenue
                $todayShifts = \App\Entities\Schedule::whereServiceRequired($service)->whereStatus('Completed')->whereScheduleDate($date->format('Y-m-d'))->count();
                $todaySchedules = \App\Entities\Schedule::with('service.ratecard.tax')->whereServiceRequired($service)->whereChargeable(1)->whereStatus('Completed')
                                    ->whereScheduleDate($date->format('Y-m-d'))->get();
                $todayRevenue = number_format(\Helper::getGrossMis($todaySchedules) - \Helper::getDiscountedMis($todaySchedules),2,'.','');
                $todayAverageRevenue = ($todayRevenue != 0)?number_format($todayRevenue/$todayShifts,2,'.',''):0;
            
            //For MTD Service Revenue
                $mtdShifts = \App\Entities\Schedule::whereServiceRequired($service)->whereStatus('Completed')->whereYear('schedule_date',$date->format('Y'))
                                    ->whereMonth('schedule_date',$date->format('m'))
                                    ->whereDate('schedule_date','<=',$date->format('Y-m-d'))->count();
                $monthSchedules = \App\Entities\Schedule::with('service.ratecard.tax')->whereServiceRequired($service)->whereChargeable(1)->whereStatus('Completed')
                                    ->whereYear('schedule_date',$date->format('Y'))
                                    ->whereMonth('schedule_date',$date->format('m'))
                                    ->whereDate('schedule_date','<=',$date->format('Y-m-d'))->get();
                $mtdRevenue = number_format(\Helper::getGrossMis($monthSchedules) - \Helper::getDiscountedMis($monthSchedules),2,'.','');
                $mtdAverageRevenue = ($mtdRevenue != 0)?number_format($mtdRevenue/$mtdShifts,2,'.',''):0;

            //For YTD Service Revenue
                $ytdShifts = \App\Entities\Schedule::whereServiceRequired($service)->whereStatus('Completed')->whereBetween('schedule_date',[$fyStart,$fyEnd])->count();
                $ytdSchedules = \App\Entities\Schedule::with('service.ratecard.tax')->whereServiceRequired($service)->whereChargeable(1)->whereStatus('Completed')
                                    ->whereBetween('schedule_date',[$fyStart,$fyEnd])->get();
                $ytdRevenue = number_format(\Helper::getGrossMis($ytdSchedules) - \Helper::getDiscountedMis($ytdSchedules),2,'.','');
                $ytdAverageRevenue = ($ytdRevenue != 0)?number_format($ytdRevenue/$ytdShifts,2,'.',''):0;

            $data['graph']['labels'][] = \App\Entities\Masters\Service::where('id',$service)->value('service_name');
            $todayArrShifts[] = $todayShifts;
            $todayArrRevenue[] = $todayRevenue;
            $todayArrAverageRevenue[] = $todayAverageRevenue;

            $mtdArrShifts[] = $mtdShifts;
            $mtdArrRevenue[] = $mtdRevenue;
            $mtdArrAverageRevenue[] = $mtdAverageRevenue;

            $ytdArrShifts[] = $ytdShifts;
            $ytdArrRevenue[] = $ytdRevenue;
            $ytdArrAverageRevenue[] = $ytdAverageRevenue;

                
            // Table datasets
            $data['table'][] = [
                'service' => \App\Entities\Masters\Service::where('id',$service)->value('service_name'),
                'todayShifts' => $todayShifts,
                'todayRevenue' => $todayRevenue,
                'todayAverageRevenue' => $todayAverageRevenue,

                'mtdShifts' => $mtdShifts,
                'mtdRevenue' => $mtdRevenue,
                'mtdAverageRevenue' => $mtdAverageRevenue,

                'ytdShifts' => $ytdShifts,
                'ytdRevenue' => $ytdRevenue,
                'ytdAverageRevenue' => $ytdAverageRevenue,
            ];

            }

            $data['graph']['labels'] = array_sort($data['graph']['labels'],'asc');

            $data['table'] = array_values(collect($data['table'])->sortBy('service')->toArray());

            $data['graph']['datasets'][] = [
                    'label' => 'Today',
                    'data' => $todayArrShifts,
                    'backgroundColor' => 'rgb(76, 169, 210)'
                ];
            $data['graph']['datasets'][] = 
                [
                    'label' => 'MTD',
                    'data' => $mtdArrShifts,
                    'backgroundColor' => 'rgb(56, 133, 157)'
                ];
            $data['graph']['datasets'][] = [
                    'label' => 'YTD',
                    'data' => $ytdArrShifts,
                    'backgroundColor' => 'rgb(11, 98, 164)'
                ];
        }
        unset($services);
        $data['creditTable'][] = [
            'todate' => \App\Entities\CreditMemo::whereDate('created_at',$date->format('Y-m-d'))->whereType('Others')->where('amount','>',0)->sum('amount'),
            'mtd' => \App\Entities\CreditMemo::whereYear('created_at',$date->format('Y'))->whereMonth('created_at',$date->format('m'))
                                          ->whereDate('created_at','<=',$date->format('Y-m-d'))->whereType('Others')->where('amount','>',0)->sum('amount'),
            'ytd' => \App\Entities\CreditMemo::whereBetween('created_at',[$fyStart,$fyEndCa])->whereType('Others')->where('amount','>',0)->sum('amount')
        ];
        
        return $data;
    }
    
    static function revenueStatsNew($request)
    {
        ini_set('memory_limit','350M');
        $date = $request['date'];
        $requestDate = "";
        $data = [];
        $data['graph'] = [];
        $data['graph']['labels'] = ["Gross", "Discount", "Revenue", "Taxed", "Credit Memo"];
        $todayArr = $mtdArr = $ytdArr = [];
        if(!empty($date)){
            $requestDate = $date;
        }

        $todayArr = self::getRevenueByPeriod('Today', $requestDate);
        $mtdArr = self::getRevenueByPeriod('MTD', $requestDate);
        $ytdArr = self::getRevenueByPeriod('YTD', $requestDate);

        $data['graph']['datasets'][] = [
                'label' => 'TODATE',
                'data' => $todayArr,
                'backgroundColor' => 'rgb(255, 153, 51)'
            ];
        $data['graph']['datasets'][] = 
            [
                'label' => 'MTD',
                'data' => $mtdArr,
                'backgroundColor' => 'rgb(56, 133, 157)'
            ];
        $data['graph']['datasets'][] = [
                'label' => 'YTD',
                'data' => $ytdArr,
                'backgroundColor' => 'rgb(60, 179, 113)'
            ];
        return $data;
    }

    static function getRevenueByPeriod($period, $date)
    {

        if(empty($date)){
            $date = null;
            //Setting Financial Year On Page Load
            if(date('m') <= 3){
                $fyStart = date((date('Y') - 1).'-04-01');
                $fyEnd = date('Y-m-d',strtotime("-1 days"));
            }else{
                $fyStart = date('Y-04-01');
                $fyEnd = date('Y-m-d',strtotime("-1 days"));
            }
        }else{
            $dateParsed = \Carbon\Carbon::parse($date);
            $dateParsedMonth = $dateParsed->format('m');
            $dateParsedYear = $dateParsed->format('Y');

            //Setting Financial Year On Filter
            if($dateParsedMonth <= 3){
                $fyStart = date(($dateParsedYear - 1).'-04-01');
                $fyEnd = \Carbon\Carbon::parse($date)->format('Y-m-d');
            }else{
                $fyStart = date($dateParsedYear.'-04-01');
                $fyEnd = \Carbon\Carbon::parse($date)->format('Y-m-d');
            }
        }

        $data = ['gross' => 0, 'discount' => 0, 'revenue' => 0, 'taxed' => 0, 'creditmemo' => 0];

        $schedulesAll = \App\Entities\Schedule::select('id','schedule_date','status','chargeable','service_required','amount','ratecard_id')
                                          ->with('service.ratecard.tax')
                                          ->whereStatus('Completed')
                                          ->whereChargeable(1);

        if(empty($date)){
            if($period == 'Today'){
                $schedules = $schedulesAll->where('schedule_date',date('Y-m-d',strtotime("-1 days")));
                $creditMemo = \App\Entities\CreditMemo::whereDate('created_at',date('Y-m-d',strtotime("-1 days")))->whereType('Others')->where('amount','>',0);
            }
            if($period == 'MTD'){
                $schedules = $schedulesAll->whereYear('schedule_date',date('Y'))
                                          ->whereMonth('schedule_date',date('m'))
                                          ->whereDate('schedule_date','<=',date('Y-m-d',strtotime("-1 days")));
                $creditMemo = \App\Entities\CreditMemo::whereYear('created_at',date('Y'))->whereMonth('created_at',date('m'))
                                          ->whereDate('created_at','<=',date('Y-m-d',strtotime("-1 days")))
                                          ->whereType('Others')->where('amount','>',0);
            }   
            if($period == 'YTD'){
                $schedules = $schedulesAll->whereBetween('schedule_date',[$fyStart,$fyEnd]);
                $creditMemo = \App\Entities\CreditMemo::whereBetween('date',[$fyStart,$fyEnd])
                                          ->whereType('Others')->where('amount','>',0);
            }
        }else{
            if($period == 'Today'){
                $schedules = $schedulesAll->where('schedule_date',$dateParsed->format('Y-m-d'));
                $creditMemo = \App\Entities\CreditMemo::whereDate('created_at',$dateParsed->format('Y-m-d'))->whereType('Others')->where('amount','>',0);
            }
            if($period == 'MTD'){
                $schedules = $schedulesAll->whereYear('schedule_date',$dateParsed->format('Y'))->whereMonth('schedule_date',$dateParsed->format('m'))
                                         ->where('schedule_date','<=',$dateParsed->format('Y-m-d'));
                $creditMemo = \App\Entities\CreditMemo::whereYear('created_at',$dateParsed->format('Y'))->whereMonth('created_at',$dateParsed->format('m'))
                                         ->whereDate('created_at','<=',$dateParsed->format('Y-m-d'))->whereType('Others')->where('amount','>',0);
            }
            if($period == 'YTD'){
                $schedules = $schedulesAll->whereBetween('schedule_date',[$fyStart,$fyEnd]);
                $creditMemo = \App\Entities\CreditMemo::whereBetween('date',[$fyStart,$fyEnd])->whereType('Others')->where('amount','>',0);
            }
        }
        $schedulesList      = $schedules->get();
        $date = isset($dateParsed)?$dateParsed->format('Y-m-d'):date('Y-m-d',strtotime("-1 days"));
        $data['gross']      = floatval(\Helper::getGrossMis($schedulesList));
        $data['discount']   = floatval(\Helper::getDiscountedMis($schedulesList));
        $data['revenue']    = $data['gross'] - $data['discount'];
        // $data['due']        = floatval(\Helper::getDueMis($dueInvoice->get()));
        $data['taxed']      = floatval(\Helper::getTaxedMis($schedulesList));
        $data['creditmemo'] = floatval(\Helper::getCreditMemoMis($creditMemo->get()));

        return collect($data)->flatten()->all();
    }
}