<?php
namespace App\Helpers\Report;

use App\Entities\Console\Subscriber;
use App\Entities\Profile;
use App\Entities\LoggingEvents;
use Carbon\Carbon;

class Activity {

    static function loginSummary($tenantId,$fromDate,$toDate)
    {
        $activity = array();

        if($tenantId){
                $subscriber = Subscriber::whereStatus('Active')->whereId($tenantId)->value('database_name');
                \Config::set('database.connections.api.database',$subscriber);
                \Config::set('database.default','api');
                \DB::setDefaultConnection('api');
                $provider = Profile::value('organization_name');
                $activityList = LoggingEvents::withoutGlobalScopes()->with('users')->whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->get();

                foreach ($activityList as $i => $a) {
                    $activity[] = [
                        'date' => $a->created_at->format('d-m-Y'),
                        'provider' => $provider,
                        'user' => $a->users->full_name,
                        'login' => isset($a->login_time)?Carbon::parse($a->login_time)->format('h:i:s A'):'',
                        'logout' => isset($a->logout_time)?Carbon::parse($a->logout_time)->format('h:i:s A'):'',
                        'ip' => $a->ip_address,
                        'agent' => $a->user_agent 
                    ];
                }
                \DB::purge('api');
        }else{
            $subscribers = Subscriber::whereStatus('Active')->get();
                foreach ($subscribers as $i => $s) {
                    \Config::set('database.connections.api.database',$s->database_name);
                    \Config::set('database.default','api');
                    \DB::setDefaultConnection('api');

                    $provider = Profile::value('organization_name');
                    $activityList = LoggingEvents::withoutGlobalScopes()->whereRaw(\DB::raw('DATE(created_at) BETWEEN "'.$fromDate.'" AND "'.$toDate.'"'))->get();
                    foreach ($activityList as $i => $a) {
                        $activity[] = [
                        'date' => $a->created_at->format('d-m-Y'),
                        'provider' => $provider,
                        'user' => $a->users->full_name,
                        'login' => isset($a->login_time)?Carbon::parse($a->login_time)->format('h:i:s A'):'',
                        'logout' => isset($a->logout_time)?Carbon::parse($a->logout_time)->format('h:i:s A'):'',
                        'ip' => $a->ip_address,
                        'agent' => $a->user_agent 
                        ];
                    }
                    \DB::purge('api');
                }
            }
        return $activity;
    }
}
