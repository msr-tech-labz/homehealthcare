<?php
namespace App\Helpers;
use App\Helpers\Helper;

class AuditHelper{
    static function message($audit, $table, $exception)
    {
        $strOld = $strNew = '';

        if(!empty($audit->old_values)){
            foreach (collect($audit->old_values)->except($exception)->toArray() as $key => $value) {
                if(!empty($value))
                $strOld .= ', <b>Old '.$key.'</b> >>> '.Helper::auditValue($key,$value);
            }
        }
        if(!empty($audit->new_values)){
            foreach (collect($audit->new_values)->except($exception)->toArray() as $key => $value) {
                if(!empty($value) || $value==0)
                $strNew .= ', <b>New '.$key.'</b> >>> '.Helper::auditValue($key,$value);
            }
        }

        $user = \App\Entities\User::whereId($audit->user_id)->first();
        $userName = $user->full_name;
        
        return '<div class="card comment-card">
                    <div class="header">
                        <h2 class="col-teal">'.$userName.' <span class="col-pink">IP ( '.$audit->ip_address.' )</span></h2>
                        <ul class="header-dropdown m-r--5" style="top: 5px;">
                            <li><span class="col-pink">'.$audit->created_at->format('d M y H:i:s').'</span></li>
                        </ul>                            
                        </span>
                    </div>
                    <div class="body">
                        '.ucwords($audit->event).' '.$table.'
                        '.$strOld.'
                        '.$strNew.'
                    </div>
                </div>';
    }

    static function getCaseHistory($leadID)
    {
        $str = '';
        if($leadID){
            $lead = \App\Entities\Lead::whereId($leadID)
            ->with('serviceOrders','schedules')
            ->first();
            if($lead){
                // CaseServices Audit
                $serviceOrders = $lead->serviceOrders->sortByDesc('updated_at');
                $exception = ['id','tenant_id','branch_id','user_id','lead_id','patient_id','payment_status','service_request_id'];
                foreach ($serviceOrders as $serviceOrder) {
                    $serviceName = $serviceOrder->serviceRequest->service->service_name;
                    $serviceFrom = $serviceOrder->from_date->format('d-m-Y');
                    $serviceTo = $serviceOrder->to_date->format('d-m-Y');
                    $serviceOrder = $serviceOrder->audits()->orderBy('audits.created_at','desc')->get();
                    foreach ($serviceOrder as $audit) {
                        $modified = [];

                        foreach ($audit->old_values as $column => $key) {
                            $modified[$column]['old'] = $key;
                        }

                        foreach ($audit->new_values as $column => $key) {
                            $modified[$column]['new'] = $key;
                        }

                        $collection = collect($modified)->except($exception)->toArray();
                        $str .= self::message($audit, ' ServiceOrder - '.$serviceName.' ('.$serviceFrom.' to '.$serviceTo.')', $exception);
                    }
                }
                // Schedules Audit
                $schedules = $lead->schedules->sortByDesc('updated_at');
                $exception = ['id','tenant_id','branch_id','lead_id','patient_id','user_id','service_request_id'];
                foreach ($schedules as $schedule) {
                    $scheduleId = $schedule->schedule_id;
                    $schedule = $schedule->audits()->orderBy('audits.created_at','desc')->get();
                    foreach ($schedule as $audit) {
                        $modified = [];

                        foreach ($audit->old_values as $column => $key) {
                            $modified[$column]['old'] = $key;
                        }

                        foreach ($audit->new_values as $column => $key) {
                            $modified[$column]['new'] = $key;
                        }

                        $collection = collect($modified)->except($exception)->toArray();
                        $str .= self::message($audit, ' Schedules ID - '.$scheduleId, $exception);
                    }
                }
            }
        }

        return $str;
    }
}
