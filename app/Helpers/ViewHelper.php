<?php
namespace App\Helpers;

class ViewHelper{

    public static function css($asset){
        if($asset){
            return asset('css/'.$asset);
        }
    }

    public static function js($asset){
        if($asset){
            return asset('js/'.$asset);
        }
    }

    public static function ThemeCss($asset){
        if($asset){
            return asset('themes/default/css/'.$asset);
        }
    }

    public static function ThemePlugin($asset){
        if($asset){
            return asset('themes/default/plugins/'.$asset);
        }
    }

    public static function ThemeJs($asset){
        if($asset){
            return asset('themes/default/js/'.$asset);
        }
    }

    public static function ThemeImage($asset){
        if($asset){
            return asset('themes/default/images/'.$asset);
        }
    }

    public static function classActivePath($path)
    {
        return \Request::is($path) ? ' class="active"' : '';
    }

    public static function classActiveSegment($path)
    {
         if(!is_array($value)) {
            return \Request::segment($segment) == $value ? ' class="active"' : '';
        }
        foreach ($value as $v) {
            if(\Request::segment($segment) == $v) return ' class="active"';
        }
        return '';
    }
}
