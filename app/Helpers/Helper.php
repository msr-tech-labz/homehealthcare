<?php
namespace App\Helpers;

use DB;
use App\Entities\Masters\Service;
use App\Entities\Masters\RateCard;
use App\Entities\Masters\Branch;
use App\Entities\Profile;
use App\Entities\Attendance;
use App\Entities\CaseBillables;
use App\Entities\InvoiceItems;
use App\Entities\ServiceInvoiceItems;
use App\Entities\Schedule;
use App\Entities\Console\Subscriber;
use App\Entities\Caregiver\Caregiver;
use App\Entities\Caregiver\ProfessionalDetails;
use App\Entities\Patient;
use App\Entities\PatientAddress;
use App\Entities\User;

class Helper{

    static function index()
    {
        return json_encode('Helper Function v1.0');
    }

    static function Itemname($item_id,$category,$invoice_id,$type)
    {
        if($type == "dec"){
            if($category == "Billable"){
                $item = CaseBillables::whereId($item_id)->first();
                if($item->category == "Laboratory"){
                    $item = $item->tests->test_name;
                }elseif($item->category == "Consumables"){
                    $item = $item->consumable->consumable_name;
                }elseif($item->category == "Equipments"){
                    $item = $item->surgical->surgical_name;
                }else{
                    $item = $item->item;
                }
            }else{
                $sch1 = InvoiceItems::whereInvoiceId($invoice_id)->whereCategory("Schedule")->first();
                $sch2 = InvoiceItems::whereInvoiceId($invoice_id)->whereCategory("Schedule")->get()->last();
                if($sch1->item_id == $sch2->item_id)
                {
                    $item1 = Schedule::find($sch1->item_id);
                    $item = $item1->schedule_date->format('d-m-Y');
                }else{
                    $item1 = Schedule::find($sch1->item_id);
                    $item11 = $item1->schedule_date->format('d-m-Y');

                    $item2 = Schedule::find($sch2->item_id);
                    $item22 = $item2->schedule_date->format('d-m-Y');

                    $item = $item11." to ".$item22;
                }
            }
        }else if($type == "qun"){
            if($category == "Billable"){
                $item = CaseBillables::whereId($item_id)->first();
                $item = $item->quantity;
            }else{
                $item = "-";
            }
        }else if($type == "net"){
            if($category == "Billable"){
                $item = CaseBillables::whereId($item_id)->first();
                $item = $item->rate;
            }else{
                $item = InvoiceItems::whereInvoiceId($invoice_id)->whereCategory("Schedule")->pluck('rate')->sum();
            }
        }else if($type == "service"){
            if($category == "Billable"){
                $item = "-";
            }else{
                $sch1 = InvoiceItems::whereInvoiceId($invoice_id)->whereCategory("Schedule")->first();
                $sch2 = InvoiceItems::whereInvoiceId($invoice_id)->whereCategory("Schedule")->get()->last();
                if($sch1->item_id == $sch2->item_id)
                {
                    $item1 = Schedule::find($sch1->item_id);
                    $item = $item1->service->service_name;
                }else{
                    $item1 = Schedule::find($sch1->item_id);
                    $item11 = $item1->schedule_date->format('d-m-Y');

                    $item2 = Schedule::find($sch2->item_id);
                    $item22 = $item2->schedule_date->format('d-m-Y');

                    $item = $item1->service->service_name;
                }
            }
        }else if($type == "amount"){
            if($category == "Billable"){
                $item = CaseBillables::whereId($item_id)->first();
                $item = $item->amount;
            }else{
                $item = InvoiceItems::whereInvoiceId($invoice_id)->whereCategory("Schedule")->pluck('total_amount')->sum();
            }
        }else{
            if($category == "Billable"){
                $item = CaseBillables::whereId($item_id)->first();
                $item = $item->category;
            }else{
                $item = "Schedule";
            }
        }
        return $item;
    }

    static function isAggregator()
    {
        return session('orgType') == 'Aggregator'?true:false;
    }

    static function getUserID()
    {
        return self::encryptor('decrypt',session('user_id'));
    }

    static function getAdminID()
    {
        return self::encryptor('decrypt',session('admin_id'));
    }

    static function getTenantID()
    {
        return self::encryptor('decrypt',session('tenant_id'));
    }

    static function uniqueID($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    static function getBranchID()
    {
        if(session('role') === 1)
            return 1;
        else
            return self::encryptor('decrypt',session('branch_id'));
    }

    static function getDateObject($date, $format='Y-m-d')
    {
        if($date){
            return date_format(new \DateTime($date),$format);
        }
    }

    static function getSetting($key)
    {
        if($key){
            return self::encryptor('decrypt',session('settings.'.$key));
        }
        return null;
    }

    static function getBranchSettings($key, $id)
    {
        $output = false;
        if($key){
            $output = Branch::whereId(Patient::whereId($id)->pluck('branch_id')->first())->pluck($key)->first();
        }
        return $output;
    }

    static function getSession($key)
    {
        if($key){
            return self::encryptor('decrypt',session($key));
        }
        return null;
    }

    static function checkDomain($domain)
    {
        if($domain){
            $res = \DB::table('providers')->where('sub_domain',$domain)->first();
            if(count($res)){
                return $res;
            }else{
                return false;
            }
        }
    }

    static function capitalizeWord($array){
        if(is_array($array)){
            foreach ($array as $a => $value){
                $array[$a] = $value;

                if(is_string($value) && !self::validBase64($value)){
                    $array[$a] = ucwords($value);
                }
            }
        }
        return $array;
    }

    static function validBase64($string)
    {
        $decoded = base64_decode($string, true);

        // Check if there is no invalid character in string
        if (!preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $string)) return false;

        // Decode the string in strict mode and send the response
        if (!base64_decode($string, true)) return false;

        // Encode and compare it to original one
        if (base64_encode($decoded) != $string) return false;

        return true;
    }

    static function encryptor($action, $string)
    {
        $output = false;

        $encrypt_method = "AES-256-CBC";
        //pls set your unique hashing key
        $secret_key = 'apnacare';
        $secret_iv = 'hhms';

        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        //do the encyption given text/string/number
        if( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        }
        else if( $action == 'decrypt' ){
            //decrypt the given text/string/number
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }

    static function str($value)
    {
        if(is_numeric($value)){
            return $value;
        }

        if($value == null){
            return '';
        }

        return trim($value);
    }

    static function getDate($dateObject, $dateOnly=false)
    {
        if($dateObject && !empty($dateObject) && $dateObject != '0000-00-00'){
            if($dateOnly)
                return $dateObject->toDateString();

            return $dateObject->toDateTimeString();
        }

        if($dateObject != '0000-00-00'){
            return '';
        }

        return '';
    }

    static function getServiceName($id)
    {
        $serviceName = $id;

        if(is_numeric($id)){
            if(!session(self::getTenantID().'_services')){
                $services = Service::all();
                session()->put(self::getTenantID().'_services',$services);
            }

            $services = collect(session(self::getTenantID().'_services'));
            $serviceName = $services->where('id',$id)->first()?$services->where('id',$id)->first()->service_name:'';
        }

        return $serviceName;
    }

    static function getServiceHsn($id)
    {
        $serviceHsn = '';
        if(is_numeric($id)){
            $services = Service::all();
            $serviceHsn = $services->where('id',$id)->first()?$services->where('id',$id)->first()->hsn:'';
        }

        return $serviceHsn;
    }

    static function getBillableName($id)
    {
        $billableName = $id;

        if(is_numeric($id)){
            $billable = CaseBillables::find($id);
            if($billable){
                if($billable->category == 'Consumables')
                    $billableName = $billable->consumable->consumable_name;

                if($billable->category == 'Equipments')
                    $billableName = $billable->surgical->surgical_name;

                if($billable->category == 'Laboratory')
                    $billableName = $billable->tests->test_name;

                if($billable->category == 'Others' || $billable->category == 'Pharmaceuticals')
                    $billableName = $billable->item;
            }
        }

        return $billableName;
    }

    static function getRateFromServiceOrder($id)
    {
        $serviceRate = 0;

        if(is_numeric($id)){
            $ratecard = \App\Entities\CaseServiceOrder::whereId(intval($id))->first();
            if($ratecard){
                $serviceRate = $ratecard->ratecard_rate;
            }
        }

        return $serviceRate;
    }

    static function getServiceRate($id)
    {
        $serviceRate = 0;

        if(is_numeric($id)){
            if(!session(self::getTenantID().'_service_rates')){
                $ratecard = RateCard::all();
                session()->put(self::getTenantID().'_service_rates',$ratecard);
            }
            $rates = collect(session(self::getTenantID().'_service_rates'));
            $serviceRate = $rates->where('service_id',$id)->where('status',1)->pluck('amount')->first();
        }

        return $serviceRate;
    }

    static function getDiscountRateByCase($id)
    {
        $serviceRate = 0;

        if(is_numeric($id)){
            $serviceRequest = \App\Entities\CaseServices::find($id);
            if($serviceRequest){
                $serviceRate = [
                    'rate' => $serviceRequest->gross_rate,
                    'type' => $serviceRequest->discount_type,
                    'value' => (float) $serviceRequest->discount_value,
                    'amount' => $serviceRequest->net_rate
                ];
            }
        }

        return $serviceRate;
    }

    static function getStaffID($id)
    {
        if($id){
            $c = \App\Entities\Caregiver\Caregiver::select('employee_id')->whereId($id)->first();
            if($c){
                return trim($c->employee_id);
            }
        }
        return '-';
    }

    static function getStaffName($id)
    {
        if($id){
            $c = \App\Entities\Caregiver\Caregiver::select('first_name','middle_name','last_name')->whereId($id)->first();
            if($c){
                return trim($c->full_name);
            }
        }
        return '-';
    }

    static function getDOJ($id)
    {
        if($id){
            $c = \App\Entities\Caregiver\ProfessionalDetails::select('date_of_joining')->whereCaregiverId($id)->first();
            if($c){
                return isset($c->date_of_joining)?$c->date_of_joining->format('d-m-Y'):'';
            }
        }
        return '-';
    }

    static function getDOR($id)
    {
        if($id){
            $c = \App\Entities\Caregiver\ProfessionalDetails::select('resignation_date')->whereCaregiverId($id)->first();
            if($c){
                return isset($c->resignation_date)?$c->resignation_date->format('d-m-Y'):'';
            }
        }
        return '-';
    }

    static function getStaffRole($id)
    {
        if($id){
            $c = \App\Entities\Caregiver\Caregiver::select('id','first_name','middle_name','last_name','employee_id')->whereId($id)->first();
            if($c){
                return $c->professional->role;
            }
        }
        return '-';
    }

    static function getStaffCadre($id)
    {
        if($id){
            $c = \App\Entities\Caregiver\Caregiver::select('id','first_name','middle_name','last_name','employee_id')->whereId($id)->first();
            if($c){
                return $c->professional->department;
            }
        }
        return null;
    }

    static function getStaffDeployable($id)
    {
        if($id){
            $c = \App\Entities\Caregiver\Caregiver::select('id','first_name','middle_name','last_name','employee_id')->whereId($id)->first();
            if($c){
                return $c->professional->deployable;
            }
        }
        return null;
    }

    static function getStaffDesignation($id)
    {
        if($id){
            $c = \App\Entities\Caregiver\Caregiver::select('id','first_name','middle_name','last_name','employee_id')->whereId($id)->first();
            if(isset($c->professional->designations)){
                return $c->professional->designations->designation_name;
            }
        }
        return '';
    }

    static function getTotalShifts($id,$fromDate,$toDate)
    {
        if($id){
            $fromDate = \Carbon\Carbon::parse($fromDate)->format('Y-m-d');
            $toDate = \Carbon\Carbon::parse($toDate)->format('Y-m-d');
            $c = \App\Entities\Attendance::whereCaregiverId($id)->whereBetween('date',[$fromDate,$toDate])->whereStatus('P')->whereNotNull('schedule_id')->count('id');
            return $c;
        }
        return 0;
    }

    static function getOnBench($id,$fromDate,$toDate)
    {
        if($id){
            $fromDate = \Carbon\Carbon::parse($fromDate)->format('Y-m-d');
            $toDate = \Carbon\Carbon::parse($toDate)->format('Y-m-d');
            $c = \App\Entities\Attendance::whereCaregiverId($id)->whereBetween('date',[$fromDate,$toDate])->whereStatus('P')->whereOnTraining('No')->distinct('date')->whereNull('schedule_id')->count('id');
            return $c;
        }
        return 0;
    }

    static function getOnTraining($id,$fromDate,$toDate)
    {
        if($id){
            $fromDate = \Carbon\Carbon::parse($fromDate)->format('Y-m-d');
            $toDate = \Carbon\Carbon::parse($toDate)->format('Y-m-d');
            $c = \App\Entities\Attendance::whereCaregiverId($id)->whereBetween('date',[$fromDate,$toDate])->whereStatus('P')->whereOnTraining('Yes')->distinct('date')->whereNull('schedule_id')->count('id');
            return $c;
        }
        return 0;
    }

    static function getAbsent($id,$fromDate,$toDate)
    {
        if($id){
            $fromDate = \Carbon\Carbon::parse($fromDate)->format('Y-m-d');
            $toDate = \Carbon\Carbon::parse($toDate)->format('Y-m-d');
            $c = \App\Entities\Attendance::whereCaregiverId($id)->whereBetween('date',[$fromDate,$toDate])->whereStatus('A')->distinct('date')->whereNull('schedule_id')->count('id');
            return $c;
        }
        return 0;
    }

    static function getMarkedDays($id,$fromDate,$toDate)
    {
        if($id){
            $fromDate = \Carbon\Carbon::parse($fromDate)->format('Y-m-d');
            $toDate = \Carbon\Carbon::parse($toDate)->format('Y-m-d');
            $a = \App\Entities\Attendance::whereCaregiverId($id)->whereBetween('date',[$fromDate,$toDate])->whereIn('status',['P','A'])->distinct('date')->count('date');
            $b = \App\Entities\Leaves::whereCaregiverId($id)->whereBetween('date',[$fromDate,$toDate])->whereStatus('Approved')->distinct('date')->count('date');
            $c = $a + $b;
            return $c;
        }
        return 0;
    }

    static function getProviderName($id)
    {
        if($id){
            $p = Profile::withoutGlobalScopes()->whereTenantId($id)->first();
            if($p){
                return $p->organization_name;
            }
        }
        return '-';
    }

    static function checkCaseToAggregator($id)
    {
        if($id){
            $p = Subscriber::withoutGlobalScopes()->whereId($id)->first();
            if($p->user_type == "Aggregator"){
                return true;
            }
        }
        return false;
    }

    static function getFolderName($id)
    {
        $hash = substr(md5($id), 0, 10);

        return $hash.'_'.$id;
    }

    static function random_color_part()
    {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }

    static function getColor()
    {
        return '#'.Helper::random_color_part() . Helper::random_color_part() . Helper::random_color_part();
    }

    static function getAttendanceStatus($empID, $date)
    {
        if($empID && $date){
            $att = Attendance::whereDate('punch_in','=',$date)->whereCaregiverId($empID)->first();

            return $att?$att->status:false;
        }
        return false;
    }

    static function getMonthlyAttendanceStatus($empID, $date)
    {
        if($empID && $date){
            if(session()->has('attendance_res')){
                $obj = collect(session('attendance_res'));
                $result = $obj->where('caregiver_id',$empID)->where('date',$date);
                $res = [];
                if(count($result)){
                    foreach ($result as $r) {
                        if($r['punch_in'] && $r['punch_out']){
                            $from = strtotime($r['punch_in']);
                            $to = strtotime($r['punch_out']);

                            $init = ($to - $from);
                            $hours = floor($init / 3600);
                            $minutes = floor(($init / 60) % 60);

                            if($init == 0 ){
                                $res[] = $r['punch_in'].' to '.$r['punch_out'].' | '. 24;
                            }else if($init < 0 ){
                                if($minutes == 0){
                                    $res[] = $r['punch_in'].' to '.$r['punch_out'].' | '.(24 - abs($hours));
                                }else{
                                    $res[] = $r['punch_in'].' to '.$r['punch_out'].' | '.(24 - abs($hours)).':'.(60 - abs($minutes));
                                }
                            }else{
                                if($minutes == 0){
                                    $res[] = $r['punch_in'].' to '.$r['punch_out'].' | '.$hours;
                                }else{
                                    $res[] = $r['punch_in'].' to '.$r['punch_out'].' | '.$hours.':'.$minutes;
                                }
                            }
                        }
                    }
                }
            }
        }
        return $res;
    }

    static function getDateLoginTime($empID, $date)
    {
        if($empID && $date){
            $res = Attendance::whereCaregiverId($empID)->whereDate('created_at','=',$date)->value('punch_in');
            if($res != '')
                $res = date_format(new \DateTime($res),'h:i A');
            else
                $res = null;

            return $res?$res:'-';
        }
        return '-';
    }

    static function getDateLogoutTime($empID, $date)
    {
        if($empID && $date){
            $res = Attendance::whereCaregiverId($empID)->whereDate('created_at','=',$date)->value('punch_out');
            if($res != '')
                $res = date_format(new \DateTime($res),'h:i A');
            else
                $res = null;

            return $res?$res:'-';
        }
        return '-';
    }

    static function getVendorItemName($item, $type)
    {
        if($item && $type){
            if($type == 'Consumables'){
                $name = $item->masterconsumable->consumable_name;
            }
            if($type == 'Surgicals'){
                $name = $item->mastersurgical->surgical_name;
            }
            if($type == 'Pharmaceuticals'){
                $name = $item->masterpharmaceutical->pharmaceutical_name;
            }

            return $name?$name:false;
        }
        return false;
    }

    static function getVendorItemPrice($item, $type)
    {
        if($item && $type){
            if($type == 'Consumables'){
                $price = $item->masterconsumable->final_price;
            }
            if($type == 'Surgicals'){
                $price = $item->mastersurgical->final_price;
            }
            if($type == 'Pharmaceuticals'){
                $price = $item->masterpharmaceutical->final_price;
            }

            return $price?$price:false;
        }
        return false;
    }

    static function getSchedulesIDsFromLeadId($scheduleID)
    {
        if($scheduleID){
            return \App\Entities\Schedule::whereLeadId($scheduleID)->pluck('id')->toArray();
        }

        return null;
    }

    static function getTaskJsonToArray($type='Master')
    {
        $jsonArray = [];
        $tasks = \App\Entities\Masters\Task::master()->first();
        if($tasks && !empty($tasks->tasks_json)){
            $jsonArray = json_decode($tasks->tasks_json, true);
        }

        return $jsonArray;
    }

    static function addTaskToJson($data, $type='Master',$leadID=null)
    {
        $arrayJson = "";
        if($data){
            if($type == 'Master'){
                $jsonArray = self::getTaskJsonToArray();
                array_push($jsonArray, $data);
                $arrayJson = json_encode($jsonArray);
            }
        }

        return $arrayJson;
    }

    static function getSchedulesByMonth($schedules)
    {
        $data = [];
        $groupByMonth = $schedules->sortBy('schedule_date')->groupBy(function ($item, $key){
            return date_format(new \DateTime($item['schedule_date']),'m-Y');
                        })->transform(function($item, $k) {
            return $item->groupBy('service_request_id');
        });

        if(count($groupByMonth)){
            $groupByRate = $groupByMonth->sortBy('amount');
            foreach ($groupByRate as $key => $values) {
                foreach ($values as $v => $value) {
                    if(count($value)){
                        $chargeable = 0;
                        foreach ($value->groupBy('amount') as $k => $val) {
                            if(count($val)){
                                $val = $val->sortBy('amount');
                                $first = $val[0];
                                $last = $val[sizeof($val)-1];
                                if(isset($first->service) && isset($first->ratecard)){
                                    $serviceName = $first->service->service_name;
                                    $rate = $first->amount;
                                    $fromDate = $first->schedule_date->format('d-m-Y');
                                    $toDate = $last->schedule_date->format('d-m-Y');
                                    $chargeable = $val->where('chargeable',1)->count();
                                    $nonChargeable = $val->where('chargeable',0)->count();
                                    $amount = ($chargeable - $nonChargeable) * $rate;
                                    $taxSlab = $first->ratecard->tax->tax_rate;

                                    $data[$key][] = [
                                        'service_name' => $serviceName,
                                        'chargeable' => $chargeable - $nonChargeable,
                                        'from_date' => $fromDate,
                                        'to_date' => $toDate,
                                        'rate' => $rate,
                                        'tax_amount' => $amount * ($taxSlab/100),
                                        'amount' => $amount + ($amount * ($taxSlab/100)),
                                    ];
                                }
                            }
                        }
                    }
                }
            }
        }

        return $data;
    }

    public static function getSubscribersByType($type)
    {
        $subscribers = [];
        if(!empty($type)){
            $subscribers = \App\Entities\Masters\EmailSubscriber::select('name','email')
                                ->mailer($type)->get()->toArray();
        }

        return $subscribers;
    }

    public static function getGrossMis($grossSchedules)
    {
        ini_set('max_execution_time', '120');
        $grossSum = 0;
        if(count($grossSchedules) && !empty($grossSchedules)){
            foreach ($grossSchedules as $schedule) {
                if(isset($schedule->ratecard)){
                    $grossSum += $schedule->ratecard->amount;
                }
            }
        }
        return $grossSum;
    }

    public static function getTaxedMis($taxedSchedules)
    {
        $taxedSum = 0;
        if(count($taxedSchedules) && !empty($taxedSchedules)){
            foreach ($taxedSchedules as $schedule) {
                if(isset($schedule->ratecard)){
                    $actualRate = $schedule->ratecard->amount;
                    $taxRate = $schedule->ratecard->tax->tax_rate;
                    $taxedSum += ($actualRate)*(($taxRate)/100);
                }
            }
        }
        return $taxedSum;
    }

    public static function getDiscountedMis($discountedSchedules)
    {
        $discountedSum = 0;
        if(count($discountedSchedules) && !empty($discountedSchedules)){
            foreach ($discountedSchedules as $schedule) {
                if(isset($schedule->ratecard)){
                    $actualRate = $schedule->ratecard->amount;
                    $discountedSum += ($actualRate) - ($schedule->amount);
                }
            }
        }
        return $discountedSum;
    }

    public static function getDueMis($dueInvoice)
    {
        $dueSum = 0;
        if(count($dueInvoice) && !empty($dueInvoice)){
            foreach ($dueInvoice as $invoice) {
                $dueSum += ($invoice->total_amount) - ($invoice->amount_paid);
            }
        }
        return $dueSum;
    }

    public static function getCreditMemoMis($dueCreditMemo)
    {   
        $cmSum = 0;
        if(count($dueCreditMemo) && !empty($dueCreditMemo)){
            foreach ($dueCreditMemo as $duecm) {
                $cmSum += $duecm->amount;
            }
        }
        return $cmSum;
    }
    public static function getInvoiceRange($invId,$invType,$itemStatus)
    {   
        $invoiceRange = null;
        if($invType == 'Proforma'){
            $items = InvoiceItems::whereInvoiceId($invId)->where('category','Schedule')->where('type',$itemStatus);
            $scheduleIDs = $items->pluck('item_id');
            $schedules = Schedule::whereIn('id',$scheduleIDs->toArray())->orderBy('schedule_date','Asc')->get();
            if(count($schedules)){
                $invoiceRange = [
                    'minMonth' => $schedules->min('schedule_date')->format('M'),
                    'maxMonth' => $schedules->max('schedule_date')->format('M'),
                ];
            }
        }else{
            $items = ServiceInvoiceItems::whereInvoiceId($invId)->where('category','Schedule')->where('type',$itemStatus);
            $scheduleIDs = $items->pluck('item_id');
            $schedules = Schedule::whereIn('id',$scheduleIDs->toArray())->orderBy('schedule_date','Asc')->get();
            if(count($schedules)){
                $invoiceRange = [
                    'minMonth' => $schedules->min('schedule_date')->format('M'),
                    'maxMonth' => $schedules->max('schedule_date')->format('M'),
                ];
            }
        }
        if($invoiceRange != null){
            if($invoiceRange['minMonth'] === $invoiceRange['maxMonth'])
                return ' - for the month of '.$invoiceRange['maxMonth'];
            else
                return ' - for the month of '.$invoiceRange['minMonth'].' - '.$invoiceRange['maxMonth'];
        }else{
            return '';
        }
    }

    public static function auditValue($key,$value)
    {
        switch ($key) {
            case "caregiver_id":
            $result = Caregiver::whereId($value)->pluck('employee_id')->first();
            break;

            case "chargeable":
            if($value==0){
                $result = 'No';
            } else {
                $result = 'Yes';
            }
            break;

            case "to_date":
            $result = \Carbon\Carbon::parse($value)->format('d-m-Y');;
            break;

            default:
            if($value==null && $value!=0){
                $result = 'NA';
            } else {
                $result = $value;
            }
        }        
        return $result;
    }

    public static function getDeliveryAddress($schedulePatientId,$scheduleAddress)
    {
        $str = '';
        if($scheduleAddress == 'Default'){
            $pt = Patient::find($schedulePatientId);
            if($pt)
                $str = trim($pt->street_address.' '.$pt->area.' '.$pt->city);
        }else{
            $str = PatientAddress::select('address')->whereId($scheduleAddress)->get()->pluck('address');
        }

        return $str;
    }

    public static function checkEmpType($id,$day,$month,$year,$status)
    {
        $date = $year.'-'.$month.'-'.$day;
        
        $managerCheck = ProfessionalDetails::whereCaregiverId($id)->whereRole("Manager")->whereDeployable("No")->first();
        $check = Attendance::whereCaregiverId($id)->Where('date','=',$date)->pluck('on_training')->first();

        if($managerCheck || $status== "A"){
            return "";
        }
        if($check == "No"){
            return "(NSS)";
        }else{
            return "(T)";
        }
    }

    public static function checkManagers($id)
    {
        $name = User::whereUserId($id)->pluck('full_name')->first();
        
        return $name;
    }
}
