<?php
namespace App\Helpers;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use App\Entities\PushMessages;

class PushMessaging {

    public static function send($token, $title, $body, $data)
    {
        $pushMessage = [
            'token' => json_encode($token),
            'title' => $title,
            'body' => $body,
            'data' => str_replace('"',"'",json_encode($data))
        ];
        
        $response = [];

        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($body)
                    ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($data);

        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);        

        $response['numberSuccess'] = $downstreamResponse->numberSuccess();
        $response['numberFailure'] = $downstreamResponse->numberFailure();
        $response['numberModification'] = $downstreamResponse->numberModification();    
            
        $pushMessage['success_count'] = $response['numberSuccess'];
        $pushMessage['failure_count'] = $response['numberFailure'];
        $pushMessage['modification_count'] = $response['numberModification'];
            
        PushMessages::create($pushMessage);

        return $response;
    }
}
