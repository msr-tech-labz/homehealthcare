<?php
namespace App\Helpers;

use DB;
use App\Entities\Patient;
use App\Entities\Schedule;
use App\Entities\Leaves;
use App\Entities\User;
use App\Entities\Caregiver\Caregiver;
use App\Entities\Masters\Service;
use App\Entities\PatientAddress;

use Log;

class IVRS{
	//API 4 by Service or Schedule
	static function visitBasedCreation($type, $schedule){
		$allowAPI = false;

		if($schedule->service->category->category_name == 'Visit Based'){
			$endPoint = 'http://ec2-34-216-212-60.us-west-2.compute.amazonaws.com:4003/api/Integrator/VisitBasedAssessments/';
			$patientId = Patient::whereId($schedule->patient_id)->value('patient_id');
			$caregiverId = Caregiver::whereId($schedule->caregiver_id)->value('employee_id');
			$serviceName = Service::whereId($schedule->service_required)->value('service_name');
			$streetAddress = Patient::whereId($schedule->patient_id)->value('street_address');
			$area = Patient::whereId($schedule->patient_id)->value('area');
			$city = Patient::whereId($schedule->patient_id)->value('city');

			if($schedule->delivery_address != 'Default'){
				$pAddr = PatientAddress::whereId($schedule->delivery_address)->first();
				$getDeliveryAddress = $pAddr->address;
			}else{
				$getDeliveryAddress = $streetAddress.','.$area.','.$city;
			}

			$cancelReason = null;
			$salesID = isset($schedule->lead->convertedBy)?$schedule->lead->convertedBy->employee_id:null;
			$userID = User::whereId($schedule->user_id)->value('user_id');

			if($userID == '0'){
				$ccmID = 'Admin';
			}else{
				$ccmID = Caregiver::whereId($userID)->value('employee_id');
			}

			$jsonData = array(
				'ScheduleID' => 'SCH'.$schedule->id,
				'ScheduleDate' => $schedule->schedule_date->format('Y-m-d'),			
				'ScheduleFromTime' => $schedule->start_time,
				'ScheduleToTime' => $schedule->end_time,
				'ScheduleStatus' => $schedule->status,
				'CancelReason' => $cancelReason,
				'StatusModified' => $schedule->created_at->format('Y-m-d H:i:s'),
				'PatientID' => $patientId,
				'ChargesApplied' => $schedule->amount,
				'ReportingPersonID' => $caregiverId,
				'SalesID' => $salesID,
				'CCMID' => $ccmID,
				'ServiceName' => $serviceName,
				'StreetAddress' => $getDeliveryAddress,
				'Area' => '',
				'City' => '',
				'Category' => 'Visit Based'
			);
			$allowAPI = true; 
		}
		if($allowAPI){
			$jsonDataEncoded = json_encode($jsonData);
			// $ch = curl_init($endPoint);
			// curl_setopt($ch, CURLOPT_POST,1);
			// curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        	// curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
    		// curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
			// curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			// $result = curl_exec($ch);
			// curl_close($ch);
			// Log::useDailyFiles(storage_path().'/logs/onelifeIvrs.log');
			Log::info(['IVRSEventLog-'.$type => $jsonDataEncoded]);

		}
	}
	//API 4 by schedule
	static function visitBasedCancellation($type, $schedule){
		$allowAPI = false;
		if($schedule->service->category->category_name == 'Visit Based'){
			$endPoint = 'http://ec2-34-216-212-60.us-west-2.compute.amazonaws.com:4003/api/Integrator/VisitBasedAssessments/';
			$patientId = Patient::whereId($schedule->patient_id)->value('patient_id');
			$caregiverId = Caregiver::whereId($schedule->caregiver_id)->value('employee_id');
			$serviceName = Service::whereId($schedule->service_required)->value('service_name');
			$streetAddress = Patient::whereId($schedule->patient_id)->value('street_address');
			$area = Patient::whereId($schedule->patient_id)->value('area');
			$city = Patient::whereId($schedule->patient_id)->value('city');

			if($schedule->delivery_address != 'Default'){
				$pAddr = PatientAddress::whereId($schedule->delivery_address)->first();
				$getDeliveryAddress = $pAddr->address;
			}else{
				$getDeliveryAddress = $streetAddress.','.$area.','.$city;
			}

			$cancelReason = $schedule->cancel_reason;
			$salesID = isset($schedule->lead->convertedBy)?$schedule->lead->convertedBy->employee_id:null;
			$userID = User::whereId($schedule->user_id)->value('user_id');
			if($userID == '0'){
				$ccmID = 'Admin';
			}else{
				$ccmID = Caregiver::whereId($userID)->value('employee_id');
			}

			$jsonData = array(
				'ScheduleID' => 'SCH'.$schedule->id,
				'ScheduleDate' => $schedule->schedule_date->format('Y-m-d'),
				'ScheduleFromTime' => $schedule->start_time,
				'ScheduleToTime' => $schedule->end_time,
				'ScheduleStatus' => $schedule->status,
				'CancelReason' => $cancelReason,
				'StatusModified' => $schedule->updated_at->format('Y-m-d H:i:s'),
				'PatientID' => $patientId,
				'ChargesApplied' => $schedule->amount,
				'ReportingPersonID' => $caregiverId,
				'SalesID' => $salesID,
				'CCMID' => $ccmID,
				'ServiceName' => $serviceName,
				'StreetAddress' => $getDeliveryAddress,
				'Area' => '',
				'City' => '',
				'DeliveryAddress' => $getDeliveryAddress,
				'Category' => 'Visit Based'
			);
			$allowAPI = true; 
		}
		if($allowAPI){
			$jsonDataEncoded = json_encode($jsonData);
			// $ch = curl_init($endPoint);
			// curl_setopt($ch, CURLOPT_POST,1);
			// curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        	// curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
    		// curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
			// curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			// $result = curl_exec($ch);
			// curl_close($ch);
			Log::info(['IVRSEventLog-'.$type => $jsonDataEncoded]);
		}
	}
	//API 12
	static function leaveResponse($type, $request){
		$allowAPI = false;
		$leave = Leaves::whereId(\Helper::encryptor('decrypt',$request->id))->first();
		$employeeId = Caregiver::whereId($leave->caregiver_id)->value('employee_id');
		$approverId = Caregiver::whereId($leave->approver_id)->value('employee_id');

		$endPoint = 'http://ec2-34-216-212-60.us-west-2.compute.amazonaws.com:4003/api/Integrator/LeaveRequestResponse';
		$jsonData = array(
			'UniqueID' => $employeeId,
			'LeaveFrom' => $leave->date->format('Y-m-d'),
			'LeaveTo' => $leave->date->format('Y-m-d'),
			'LeaveType' => $leave->type,
			'Notes' => $request->reason,
			'Approver' => $approverId,
			'ResponseDate' => date('Y-m-d'),
			'Status' => $leave->status
		);
		$allowAPI = true;
		if($allowAPI){
			$jsonDataEncoded = json_encode($jsonData);
			// $ch = curl_init($endPoint);
			// curl_setopt($ch, CURLOPT_POST,1);
			// curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        	// curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
    		// curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
			// curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			// $result = curl_exec($ch);
			// curl_close($ch);
			Log::info(['IVRSEventLog-'.$type => $jsonDataEncoded]);
		}
	}
	//API for Rescheduling
	static function scheduling($type, $schedule, $staff){
		$allowAPI = false;

		$endPoint = 'https://api.epscl.com/api/Integrator/StaffChange/';
		$patientId = Patient::whereId($schedule->patient_id)->value('patient_id');
		$oldCaregiverId = ($schedule->caregiver_id != 0)?Caregiver::whereId($schedule->caregiver_id)->value('employee_id'):null;
		$newCaregiverId = Caregiver::whereId($staff)->value('employee_id');
		$category = $schedule->service->category->category_name;

		$jsonData = array(
			'ScheduleID' => 'SCH'.$schedule->id,
			'OldStaffID' => $oldCaregiverId,			
			'NewStaffID' => $newCaregiverId,
			'Category' => $category,
			'PatientID' => $patientId,
		);
		$allowAPI = true;
		if($allowAPI){
			$jsonDataEncoded = json_encode($jsonData);
			// $ch = curl_init($endPoint);
			// curl_setopt($ch, CURLOPT_POST,1);
			// curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        	// curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
    		// curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
			// curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			// $result = curl_exec($ch);
			// curl_close($ch);
			Log::info(['IVRSEventLog-'.$type => $jsonDataEncoded]);
		}
	}
}