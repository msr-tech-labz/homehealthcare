@extends('layouts.main-layout')

@section('page_title',$title.' - ')

@section('plugin.styles')
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<style>
    .alert{
        width: 79% !important;
        margin-left: 20% !important;
    }
</style>
@endsection

@section('content')
    <aside id="leftsidebar" class="sidebar">
        <!-- Balance Info -->
        <div class="user-info" style="height: 110px;">
            <div class="info-container" style="text-align: center">
                <div style="color: #2196F3; font-size: 18px; font-weight: 500">Due Amount</div>
                <div style="font-size: 28px">Rs. {{ $balance }}/-</div><br>
            </div>
        </div>
        <div class="user-info" style="height: 95px;">
                <div style="color: #2196F3; font-size: 18px;">Total Limit<span style="position: absolute;left: 80%;">{{ $total_limit ?? '-' }}</span></div>
                <div style="color: #2196F3; font-size: 18px;">Used<span style="position: absolute;left: 80%;">{{ $used }}</span></div>
                <div style="color: #2196F3; font-size: 18px;">Remaining<span style="position: absolute;left: 80%;">{{ $total_limit-$used>0?$total_limit-$used:0 }}</span></div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 240px;"><ul class="list" style="height: 240px; overflow: hidden; width: auto;">
                <li class="header">SUBSCRIPTION PAYMENT</li>
                <li class="{{ $view =='buy-licence'?'active':'' }}">
                    <a href="{{ route('subscription.index') }}" class="toggled waves-effect waves-block">
                        <i class="material-icons">people</i>
                        <span>Buy Licences</span>
                    </a>
                </li>
                <li class="{{ $view =='payment-invoices'?'active':'' }}">
                    <a href="{{ route('subscription.payment-invoices') }}" class=" waves-effect waves-block">
                        <i class="material-icons">receipt</i>
                        <span>Pending Payments</span>
                    </a>
                </li>
                <li class="{{ $view =='payment-history'?'active':'' }}">
                    <a href="{{ route('subscription.payment-history') }}" class=" waves-effect waves-block">
                        <i class="material-icons">chrome_reader_mode</i>
                        <span>Payment History</span>
                    </a>
                </li>
            </ul><div class="slimScrollBar" style="background: rgba(0, 0, 0, 0.498039); width: 4px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 0px; z-index: 99; right: 1px; height: 61.9355px;"></div><div class="slimScrollRail" style="width: 4px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 0px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
        </div>
        <!-- #Menu -->
    </aside>

    <div class="col-md-10 col-lg-10" style="margin-left: 17%">
        @if(session('success'))
        <div class="alert alert-success" style="margin-left: 11% !important;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong>{{ session('success') }}</strong>
        </div>
        @endif

        @if(session('failure'))
        <div class="alert alert-danger" style="margin-left: 11% !important;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong>{{ session('failure') }}</strong>
        </div>
        @endif

        @include('subscription.'.$view)
    </div>
@endsection

@section('plugin.scripts')
<!-- Select Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<script>
    var chargePerUser = {{ $charge_per_user ?? 0 }};
</script>
@endsection
