<!doctype html public "-//w3c//dtd html 4.01//en" "http://www.w3.org/tr/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=8">
        <title>Payment Receipt</title>
        <style type="text/css">
            body {margin-top: 0px;margin-left: 0px;font-family: 'helvetica' !important; font-size: 12px !important;}
            div.image
            {
                background:url(/img/apnacare.png) 50% 50%;
                background-repeat:no-repeat;
                background-position:center;
                /*border:2px solid;*/
                /*border-color:#CD853F;*/
            }
            div.transparentbox
            {
                margin:30px 50px;
                background-color:#ffffff;
                /*border:1px solid;*/
                /*border-color:#CD853F;*/
                opacity:0.9;
                filter:alpha(opacity=60);
            }
            .text-right {text-align: right !important}
            .text-left {text-align: left !important}
            .text-center {text-align: center !important}

            .bg-grey td, .bg-grey th {background-color: #f5f5f5 !important}

            .service-table tr td,.service-table  tr th {border: 1px #cccccc solid !important}

            tr th {font-weight: 600; font-size: 12px !important; min-height: 20px;}

            .p5 {padding: 4px}

            .printBtn {float: right; margin-right: 5%; z-index: 9999 !important}
            .backBtn {float: left; margin-right: 5%; z-index: 9999 !important}
            @media print{
                .printBtn, .backBtn { display: none !important}
            }
        </style>
    </head>

    <body style="width: 98%; margin: 0 auto">
        <div class="image">
            <div class="transparentbox">
                <button type="button" class="backBtn" onclick="window.history.back()">Back</button>
                <button type="button" class="printBtn" onclick="window.print()">Print</button>
                <table width="100%" cellpadding=0 cellspacing=0>
                    <tr>
                        <td>
                            <img style="margin-top:10px;vertical-align:middle !important" src="http://www.chackojoseph.com/sites/chackojoseph.com/files/images/ApnaCare_Logo.PNG" width="140" alt="">
                        </td>
                        <td width="85%" style="text-align: right;padding-right: 40px">
                            <p>
                                <b style="font-size: 24px;color: #666">Receipt Voucher</b><br>
                                <small>Smart Health Connect</small>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center">
                            <b style="font-size: 17px">Smart Health Global</b><br>
                            <span style="line-height:25px">
                                <b>Address: </b>1583, Sahakar Nagar B Block, Bangalore, Karnataka - 560092, India
                            </span><br>
                            <b>GSTIN: </b>29AALCA8352M1ZZ, <b>Website:</b> <a href="https://www.apnacare.in">www.apnacare.in</a>,
                            <b>Phone no.:</b> +91 80889 19888, <b>E-mail:</b> <a href="mailto:info@apnacare.in">info@apnacare.in</a>
                        </td>
                    </tr>
                </table>
                <br><hr><br>
                <table width="100%" cellpadding=0 cellspacing=0>
                    <tr>
                        <td>
                            <b>Customer address:</b><br>
                            {{ session('organization_name') }}<br>
                            {{ session('organization_address') }}<br>
                            {{ session('organization_city') }}, {{ session('organization_state') }} - {{ session('organization_zipcode') }}<br>
                            {{ session('organization_country') }}
                        </td>
                        <td width="50%">
                            <table>
                                <tr>
                                    <th class="text-right">Customer GSTIN</th>
                                    <td>&nbsp;:&nbsp;</td>
                                    <td>{{ Helper::getSetting('gstin') }}</td>
                                </tr>
                                <tr>
                                    <th class="text-right">Receipt Voucher No</th>
                                    <td>&nbsp;:&nbsp;</td>
                                    <td>{{ $payment->transaction_id }}</td>
                                </tr>
                                <tr>
                                    <th class="text-right"
                                    >Receipt Voucher Date</th>
                                    <td>&nbsp;:&nbsp;</td>
                                    <td>{{ $payment->transaction_date->format('d-m-Y') }}</td>
                                </tr>
                                <tr>
                                    <th class="text-right"
                                    >Subscriber ID</th>
                                    <td>&nbsp;:&nbsp;</td>
                                    <td>{{ session('subscriber_id') }}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br><br>
                <table class="service-table" width="100%" cellpadding=0 cellspacing=0>
                    <tr class="bg-grey">
                        <th rowspan="2" class="text-center">#</th>
                        <th rowspan="2" class="text-center">Description</th>
                        <th rowspan="2" class="text-center">Quantity</th>
                        <th rowspan="2" class="text-center">Rate (INR)</th>
                        <th rowspan="2" class="text-center">Total</th>
                        <th rowspan="2" class="text-center">Discount</th>
                        <th rowspan="2" class="text-center">Taxable Value</th>
                        <th colspan="2" class="text-center">State GST</th>
                        <th colspan="2" class="text-center">Central GST</th>
                    </tr>
                    <tr class="bg-grey">
                        <th class="text-center">Rate</th>
                        <th class="text-center">Amount</th>
                        <th class="text-center">Rate</th>
                        <th class="text-center">Amount</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>ApnaFocus Subscription Charges</td>
                        <td class="text-center">{{ $payment->credit_count }}</td>
                        <td class="text-center">{{ number_format($payment->actual_amount/$payment->credit_count,2) }}</td>
                        <td class="text-center">{{ $payment->actual_amount }}</td>
                        <td class="text-center">-</td>
                        <td class="text-center">{{ $payment->actual_amount }}</td>
                        <td class="text-center">9.00 %</td>
                        <td class="text-center">{{ number_format(($payment->actual_amount * 9.00 / 100),2) }}</td>
                        <td class="text-center">9.00 %</td>
                        <td class="text-center">{{ number_format(($payment->actual_amount * 9.00 / 100),2) }}</td>
                    </tr>
                    <tr>
                        <th colspan="4" class="text-right p5">Total</th>
                        <th colspan="7" class="text-left p5">INR {{ number_format($payment->actual_amount,2) }}</th>
                    </tr>
                    <tr>
                        <th colspan="4" class="text-right p5">Total Amount of advance (rounded off)</th>
                        <th colspan="7" class="text-left p5">INR {{ number_format(($payment->total_amount + $payment->tds),2) }}</th>
                    </tr>
                    <tr>
                        <th colspan="4" class="text-right p5">Total amount of advance (in words)</th>
                        <th colspan="7" class="text-left p5">{? $f = new NumberFormatter("en", NumberFormatter::SPELLOUT); echo ucwords($f->format(($payment->total_amount + $payment->tds))).' Only'; ?}</th>
                    </tr>
                    <tr>
                        <th colspan="4" class="text-right p5">TDS deducted (pending remittance)</th>
                        <th colspan="7" class="text-left p5">INR {{ number_format($payment->tds,2) }}</th>
                    </tr>
                    <tr>
                        <th colspan="4" class="text-right p5">Amount received</th>
                        <th colspan="7" class="text-left p5">INR {{ number_format($payment->total_amount,2) }}</th>
                    </tr>
                </table>
                <br>
                <div style="text-indent: 2px">To purchase extra license, visit <a href="https://apnafocus.com/subscription" target="_blank">Buy License</a>. To to view your usage, please visit <a href="https://apnafocus.com/subscription/paymentHistory" target="_blank">Payment History</a></div>

                <br><br>

                <h4 style="width:48%;display:inline-block">Our banker's details for remitting funds:</h4>
                <h4 style="width:40%;display:inline-block">Other details:</h4>
                <table cellpadding=0 cellspacing=0>
                    <tr>
                        <td width="50%">
                            <table width="70%" cellpadding=0 cellspacing=0>
                                <tr>
                                    <th class="text-left p5">Bank Name</th>
                                    <td>&nbsp; : &nbsp;</td>
                                    <td>State Bank of India</td>
                                </tr>
                                <tr>
                                    <th class="text-left p5">Account no</th>
                                    <td>&nbsp; : &nbsp;</td>
                                    <td>33134274862</td>
                                </tr>
                                <tr>
                                    <th class="text-left p5">Beneficiary</th>
                                    <td>&nbsp; : &nbsp;</td>
                                    <td>ApnaCare India Private Limited</td>
                                </tr>
                                <tr>
                                    <th class="text-left p5">IFSC Code</th>
                                    <td>&nbsp; : &nbsp;</td>
                                    <td>SBIN0016335</td>
                                </tr>
                                <tr>
                                    <th class="text-left p5">Swift code</th>
                                    <td>&nbsp; : &nbsp;</td>
                                    <td>SBININBB177</td>
                                </tr>
                                <tr>
                                    <th class="text-left p5">MICR</th>
                                    <td>&nbsp; : &nbsp;</td>
                                    <td>560002213</td>
                                </tr>
                            </table>
                        </td>
                        <td width="40%" style="vertical-align: top">
                            <table width="100%" cellpadding=0 cellspacing=0>
                                <tr>
                                    <th class="text-left p5">HSN</th>
                                    <td>&nbsp;:&nbsp;</td>
                                    <td>998429 <br>(Other internet telecommunications services)</td>
                                </tr>
                                <tr>
                                    <th class="text-left p5">PAN No.</th>
                                    <td>&nbsp;:&nbsp;</td>
                                    <td>AALCA8352M</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br>
                <div style="width:100%; text-align: center;margin: 0 auto">
                    <p style="font-size: 12px !important; line-height:1.4">
                        <b>ApnaCare India Private Limited</b> is a private limited company with CIN no. <b>U85110KA2013FTC070132</b><br>
                        <b>Registered Office: </b>1583, Sahakar Nagar B Block, Bangalore 560092, Karnataka, India
                    </p>
                </div>
            </div>
        </div>
    </body>
</html>
