<!doctype html public "-//w3c//dtd html 4.01//en" "http://www.w3.org/tr/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=8">
        <title>Payment Receipt</title>        
        <style type="text/css">
            body {margin-top: 0px;margin-left: 0px;font-size: 16px !important;}

            #page_1 {position:relative; overflow: hidden;margin: 53px 0px 25px 53px;padding: 0px;border: none;width: 763px;}

            #page_1 #p1dimg1 {position:absolute;top:2px;left:0px;z-index:-1;width:100px;height:67px;}
            #page_1 #p1dimg1 #p1img1 {width:100px;height:67px;}

            .ft0{font: 1px 'helvetica';line-height: 1px;}
            .ft1{font: bold 13px 'helvetica';color: #333333;line-height: 16px;}
            .ft2{font: 9px 'helvetica';color: #333333;line-height: 12px;}
            .ft3{font: bold 12px 'helvetica';color: #333333;line-height: 15px;}
            .ft4{font: bold 9px 'helvetica';color: #333333;line-height: 11px;}
            .ft5{font: 12px 'helvetica';color: #333333;line-height: 15px;}
            .ft6{font: 9px 'helvetica';text-decoration: underline;color: #0000ff;line-height: 12px;}
            .ft7{font: bold 10px 'helvetica';color: #333333;line-height: 12px;}
            .ft8{font: 10px 'helvetica';color: #333333;line-height: 12px;}
            .ft9{font: 10px 'helvetica';color: #333333;line-height: 13px;}
            .ft10{font: 1px 'helvetica';line-height: 6px;}
            .ft11{font: 1px 'helvetica';line-height: 10px;}
            .ft12{font: 1px 'helvetica';line-height: 11px;}
            .ft13{font: 1px 'helvetica';line-height: 3px;}
            .ft14{font: 1px 'helvetica';line-height: 4px;}
            .ft15{font: 1px 'helvetica';line-height: 2px;}
            .ft16{font: 10px 'helvetica';text-decoration: underline;color: #0000ff;line-height: 13px;}
            .ft17{font: 11px 'times';line-height: 13px;}
            .ft18{font: 11px 'times';line-height: 12px;}
            .ft19{font: 11px 'times';line-height: 14px;}
            .ft20{font: 8px 'helvetica';color: #333333;line-height: 10px;}
            .ft21{font: bold 8px 'helvetica';color: #333333;line-height: 10px;}

            .p0{text-align: left;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p1{text-align: center;padding-right: 8px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p2{text-align: center;padding-right: 6px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p3{text-align: center;padding-right: 19px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p4{text-align: center;padding-left: 110px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p5{text-align: center;padding-right: 21px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p6{text-align: right;padding-right: 8px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p7{text-align: right;padding-right: 4px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p8{text-align: left;padding-left: 38px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p9{text-align: left;padding-left: 3px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p10{text-align: right;padding-right: 2px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p11{text-align: left;padding-left: 10px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p12{text-align: right;padding-right: 3px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p13{text-align: left;padding-left: 20px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p14{text-align: left;padding-left: 15px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p15{text-align: left;padding-left: 9px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p16{text-align: right;padding-right: 5px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p17{text-align: center;padding-right: 1px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p18{text-align: left;padding-left: 1px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p19{text-align: left;padding-left: 2px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p20{text-align: left;padding-right: 221px;margin-top: 34px;margin-bottom: 0px;}
            .p21{text-align: left;margin-top: 28px;margin-bottom: 0px;}
            .p22{text-align: left;padding-left: 7px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p23{text-align: right;padding-right: 55px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p24{text-align: left;padding-left: 3px;margin-top: 297px;margin-bottom: 0px;}
            .p25{text-align: left;padding-left: 3px;margin-top: 0px;margin-bottom: 0px;}
            .p26{text-align: left;padding-left: 169px;margin-top: 27px;margin-bottom: 0px;}
            .p27{text-align: left;padding-left: 159px;margin-top: 0px;margin-bottom: 0px;}

            .td0{padding: 0px;margin: 0px;width: 17px;vertical-align: bottom;}
            .td1{padding: 0px;margin: 0px;width: 129px;vertical-align: bottom;}
            .td2{padding: 0px;margin: 0px;width: 45px;vertical-align: bottom;}
            .td3{padding: 0px;margin: 0px;width: 52px;vertical-align: bottom;}
            .td4{padding: 0px;margin: 0px;width: 44px;vertical-align: bottom;}
            .td5{padding: 0px;margin: 0px;width: 156px;vertical-align: bottom;}
            .td6{padding: 0px;margin: 0px;width: 19px;vertical-align: bottom;}
            .td7{padding: 0px;margin: 0px;width: 26px;vertical-align: bottom;}
            .td8{padding: 0px;margin: 0px;width: 245px;vertical-align: bottom;}
            .td9{padding: 0px;margin: 0px;width: 539px;vertical-align: bottom;}
            .td10{padding: 0px;margin: 0px;width: 668px;vertical-align: bottom;}
            .td11{padding: 0px;margin: 0px;width: 146px;vertical-align: bottom;}
            .td12{padding: 0px;margin: 0px;width: 67px;vertical-align: bottom;}
            .td13{padding: 0px;margin: 0px;width: 242px;vertical-align: bottom;}
            .td14{padding: 0px;margin: 0px;width: 108px;vertical-align: bottom;}
            .td15{padding: 0px;margin: 0px;width: 134px;vertical-align: bottom;}
            .td16{padding: 0px;margin: 0px;width: 152px;vertical-align: bottom;}
            .td17{padding: 0px;margin: 0px;width: 70px;vertical-align: bottom;}
            .td18{border-bottom: #cccccc 1px solid;padding: 0px;margin: 0px;width: 17px;vertical-align: bottom;}
            .td19{border-bottom: #cccccc 1px solid;padding: 0px;margin: 0px;width: 129px;vertical-align: bottom;}
            .td20{border-bottom: #cccccc 1px solid;padding: 0px;margin: 0px;width: 45px;vertical-align: bottom;}
            .td21{border-bottom: #cccccc 1px solid;padding: 0px;margin: 0px;width: 52px;vertical-align: bottom;}
            .td22{border-bottom: #cccccc 1px solid;padding: 0px;margin: 0px;width: 44px;vertical-align: bottom;}
            .td23{border-bottom: #cccccc 1px solid;padding: 0px;margin: 0px;width: 67px;vertical-align: bottom;}
            .td24{border-left: #cccccc 1px solid;border-right: #cccccc 1px solid;border-top: #cccccc 1px solid;padding: 0px;margin: 0px;width: 15px;vertical-align: bottom;background: #f5f5f5;}
            .td25{border-right: #cccccc 1px solid;border-top: #f5f5f5 1px solid;padding: 0px;margin: 0px;width: 128px;vertical-align: bottom;background: #f5f5f5;}
            .td26{border-right: #cccccc 1px solid;border-top: #f5f5f5 1px solid;padding: 0px;margin: 0px;width: 44px;vertical-align: bottom;background: #f5f5f5;}
            .td27{border-right: #cccccc 1px solid;border-top: #f5f5f5 1px solid;padding: 0px;margin: 0px;width: 51px;vertical-align: bottom;background: #f5f5f5;}
            .td28{border-right: #cccccc 1px solid;border-top: #f5f5f5 1px solid;padding: 0px;margin: 0px;width: 43px;vertical-align: bottom;background: #f5f5f5;}
            .td29{border-right: #cccccc 1px solid;border-top: #f5f5f5 1px solid;padding: 0px;margin: 0px;width: 66px;vertical-align: bottom;background: #f5f5f5;}
            .td30{border-right: #cccccc 1px solid;border-top: #f5f5f5 1px solid;border-bottom: #f5f5f5 1px solid;padding: 0px;margin: 0px;width: 88px;vertical-align: bottom;background: #f5f5f5;}
            .td31{border-right: #cccccc 1px solid;border-top: #cccccc 1px solid;border-bottom: #f5f5f5 1px solid;padding: 0px;margin: 0px;width: 43px;vertical-align: bottom;background: #f5f5f5;}
            .td32{border-right: #f5f5f5 1px solid;border-top: #cccccc 1px solid;border-bottom: #f5f5f5 1px solid;padding: 0px;margin: 0px;width: 44px;vertical-align: bottom;background: #f5f5f5;}
            .td33{border-right: #cccccc 1px solid;border-top: #cccccc 1px solid;border-bottom: #f5f5f5 1px solid;padding: 0px;margin: 0px;width: 44px;vertical-align: bottom;background: #f5f5f5;}
            .td34{border-left: #cccccc 1px solid;border-right: #cccccc 1px solid;border-bottom: #f5f5f5 1px solid;padding: 0px;margin: 0px;width: 15px;vertical-align: bottom;background: #f5f5f5;}
            .td35{border-right: #cccccc 1px solid;border-bottom: #f5f5f5 1px solid;padding: 0px;margin: 0px;width: 128px;vertical-align: bottom;background: #f5f5f5;}
            .td36{border-right: #cccccc 1px solid;border-bottom: #f5f5f5 1px solid;padding: 0px;margin: 0px;width: 44px;vertical-align: bottom;background: #f5f5f5;}
            .td37{border-right: #cccccc 1px solid;border-bottom: #f5f5f5 1px solid;padding: 0px;margin: 0px;width: 51px;vertical-align: bottom;background: #f5f5f5;}
            .td38{border-right: #cccccc 1px solid;border-bottom: #f5f5f5 1px solid;padding: 0px;margin: 0px;width: 43px;vertical-align: bottom;background: #f5f5f5;}
            .td39{border-right: #cccccc 1px solid;border-bottom: #f5f5f5 1px solid;padding: 0px;margin: 0px;width: 66px;vertical-align: bottom;background: #f5f5f5;}
            .td40{border-left: #cccccc 1px solid;border-right: #cccccc 1px solid;border-top: #cccccc 1px solid;padding: 0px;margin: 0px;width: 15px;vertical-align: bottom;}
            .td41{border-right: #cccccc 1px solid;border-top: #cccccc 1px solid;padding: 0px;margin: 0px;width: 128px;vertical-align: bottom;}
            .td42{border-right: #cccccc 1px solid;border-top: #cccccc 1px solid;padding: 0px;margin: 0px;width: 44px;vertical-align: bottom;}
            .td43{border-right: #cccccc 1px solid;border-top: #cccccc 1px solid;padding: 0px;margin: 0px;width: 51px;vertical-align: bottom;}
            .td44{border-right: #cccccc 1px solid;border-top: #cccccc 1px solid;padding: 0px;margin: 0px;width: 43px;vertical-align: bottom;}
            .td45{border-right: #cccccc 1px solid;border-top: #cccccc 1px solid;padding: 0px;margin: 0px;width: 66px;vertical-align: bottom;}
            .td46{border-left: #cccccc 1px solid;border-right: #cccccc 1px solid;border-bottom: #cccccc 1px solid;padding: 0px;margin: 0px;width: 15px;vertical-align: bottom;}
            .td47{border-right: #cccccc 1px solid;border-bottom: #cccccc 1px solid;padding: 0px;margin: 0px;width: 128px;vertical-align: bottom;}
            .td48{border-right: #cccccc 1px solid;border-bottom: #cccccc 1px solid;padding: 0px;margin: 0px;width: 44px;vertical-align: bottom;}
            .td49{border-right: #cccccc 1px solid;border-bottom: #cccccc 1px solid;padding: 0px;margin: 0px;width: 51px;vertical-align: bottom;}
            .td50{border-right: #cccccc 1px solid;border-bottom: #cccccc 1px solid;padding: 0px;margin: 0px;width: 43px;vertical-align: bottom;}
            .td51{border-right: #cccccc 1px solid;border-bottom: #cccccc 1px solid;padding: 0px;margin: 0px;width: 66px;vertical-align: bottom;}
            .td52{border-bottom: #cccccc 1px solid;padding: 0px;margin: 0px;width: 19px;vertical-align: bottom;}
            .td53{border-right: #cccccc 1px solid;border-bottom: #cccccc 1px solid;padding: 0px;margin: 0px;width: 25px;vertical-align: bottom;}
            .td54{border-left: #cccccc 1px solid;padding: 0px;margin: 0px;width: 16px;vertical-align: bottom;}
            .td55{border-right: #cccccc 1px solid;padding: 0px;margin: 0px;width: 51px;vertical-align: bottom;}
            .td56{border-right: #cccccc 1px solid;padding: 0px;margin: 0px;width: 44px;vertical-align: bottom;}
            .td57{border-left: #cccccc 1px solid;border-bottom: #cccccc 1px solid;padding: 0px;margin: 0px;width: 16px;vertical-align: bottom;}
            .td58{border-right: #cccccc 1px solid;border-bottom: #cccccc 1px solid;padding: 0px;margin: 0px;width: 225px;vertical-align: bottom;}
            .td59{border-bottom: #cccccc 1px solid;padding: 0px;margin: 0px;width: 89px;vertical-align: bottom;}
            .td60{border-bottom: #cccccc 1px solid;padding: 0px;margin: 0px;width: 26px;vertical-align: bottom;}
            .td61{border-right: #cccccc 1px solid;padding: 0px;margin: 0px;width: 225px;vertical-align: bottom;}
            .td62{padding: 0px;margin: 0px;width: 89px;vertical-align: bottom;}
            .td63{border-bottom: #cccccc 1px solid;padding: 0px;margin: 0px;width: 245px;vertical-align: bottom;}
            .td64{border-right: #cccccc 1px solid;border-bottom: #cccccc 1px solid;padding: 0px;margin: 0px;width: 96px;vertical-align: bottom;}
            .td65{border-right: #cccccc 1px solid;padding: 0px;margin: 0px;width: 96px;vertical-align: bottom;}
            .td66{padding: 0px;margin: 0px;width: 66px;vertical-align: bottom;}
            .td67{padding: 0px;margin: 0px;width: 154px;vertical-align: bottom;}
            .td68{padding: 0px;margin: 0px;width: 147px;vertical-align: bottom;}
            .td69{padding: 0px;margin: 0px;width: 73px;vertical-align: bottom;}
            .td70{padding: 0px;margin: 0px;width: 81px;vertical-align: bottom;}
            .td71{padding: 0px;margin: 0px;width: 216px;vertical-align: bottom;}
            .td72{padding: 0px;margin: 0px;width: 310px;vertical-align: bottom;}
            .td73{padding: 0px;margin: 0px;width: 526px;vertical-align: bottom;}

            .tr0{height: 18px;}
            .tr1{height: 12px;}
            .tr2{height: 26px;}
            .tr3{height: 16px;}
            .tr4{height: 32px;}
            .tr5{height: 13px;}
            .tr6{height: 23px;}
            .tr7{height: 24px;}
            .tr8{height: 20px;}
            .tr9{height: 15px;}
            .tr10{height: 6px;}
            .tr11{height: 10px;}
            .tr12{height: 11px;}
            .tr13{height: 14px;}
            .tr14{height: 3px;}
            .tr15{height: 4px;}
            .tr16{height: 2px;}

            .t0{width: 685px;font: 10px 'helvetica';color: #333333;}
            .t1{width: 220px;margin-left: 2px;margin-top: 1px;font: 10px 'helvetica';color: #333333;}
            .t2{width: 526px;margin-top: 31px;font: 10px 'helvetica';color: #333333;}
        </style>
    </head>

    <body>
        <div id="page_1">
            <div id="p1dimg1">
                <img src="http://www.chackojoseph.com/sites/chackojoseph.com/files/images/ApnaCare_Logo.PNG" width="140" alt="">
            </div>

            <table cellpadding=0 cellspacing=0 class="t0">
                <tr>
                    <td class="tr0 td0"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr0 td1"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr0 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr0 td3"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr0 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr0 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td colspan=3 class="tr0 td5"><p class="p1 ft1">Receipt Voucher</p></td>
                    <td class="tr0 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr0 td6"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr0 td7"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr0 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr0 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr0 td6"><p class="p0 ft0">&nbsp;</p></td>
                </tr>
                <tr>
                    <td class="tr1 td0"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td1"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td3"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td colspan=3 class="tr1 td5"><p class="p1 ft2">(original for recipient)</p></td>
                    <td class="tr1 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td6"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td7"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td6"><p class="p0 ft0">&nbsp;</p></td>
                </tr>
                <tr>
                    <td class="tr2 td0"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr2 td1"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr2 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr2 td3"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr2 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td colspan=5 class="tr2 td8"><p class="p2 ft3">ApnaCare India Private Limited</p></td>
                    <td class="tr2 td6"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr2 td7"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr2 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr2 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr2 td6"><p class="p0 ft0">&nbsp;</p></td>
                </tr>
                <tr>
                    <td class="tr1 td0"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td1"><p class="p0 ft0">&nbsp;</p></td>
                    <td colspan=13 class="tr1 td9"><p class="p3 ft2"><span class="ft4">Address: </span>22, kensington terrace, kensington road, halasuru, Bangalore, Karnataka - 560092, india</p></td>
                </tr>
                <tr>
                    <td class="tr3 td0"><p class="p0 ft0">&nbsp;</p></td>
                    <td colspan=14 class="tr3 td10"><p class="p4 ft4">GSTIN: <span class="ft2">29AALCA8352M1ZZ </span><span class="ft5">, </span>Website: <a href="https://www.apnacare.in"><span class="ft6">www.apnacare.in</span></a><span class="ft5"> , </span>Phone no.: <span class="ft2">+91 80889 19888 </span><span class="ft5">, </span><nobr>E-mail:</nobr> <a href="mailto:info@apnacare.in"><span class="ft6">info@apnacare.in</span></a></p></td>
                </tr>
                <tr>
                    <td colspan=2 class="tr4 td11"><p class="p0 ft7">Customer address:</p></td>
                    <td class="tr4 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr4 td3"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr4 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr4 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr4 td12"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr4 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td colspan=7 class="tr4 td13"><p class="p5 ft4">Customer GSTIN: <span class="ft2">29aalca8352m1zz</span></p></td>
                </tr>
                <tr>
                    <td colspan=2 class="tr1 td11"><p class="p0 ft8">Apnacare</p></td>
                    <td class="tr1 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td3"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td12"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td colspan=3 class="tr1 td14"><p class="p6 ft7">Receipt Voucher No:</p></td>
                    <td colspan=4 class="tr1 td15"><p class="p0 ft8">rd00000000001802</p></td>
                </tr>
                <tr>
                    <td colspan=2 class="tr5 td11"><p class="p0 ft9">1583, , sahakar nagar b block</p></td>
                    <td class="tr5 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td3"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td12"><p class="p0 ft0">&nbsp;</p></td>
                    <td colspan=4 class="tr5 td16"><p class="p6 ft7">Receipt Voucher Date:</p></td>
                    <td colspan=2 class="tr5 td17"><p class="p0 ft9"><nobr>11-Sep-2017</nobr></p></td>
                    <td class="tr5 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td6"><p class="p0 ft0">&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan=2 class="tr1 td11"><p class="p0 ft2">bengaluru, karnataka - 560092</p></td>
                    <td class="tr1 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td3"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td12"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td colspan=3 class="tr1 td14"><p class="p6 ft7">tenant name:</p></td>
                    <td colspan=2 class="tr1 td17"><p class="p0 ft8">apnacare</p></td>
                    <td class="tr1 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td6"><p class="p0 ft0">&nbsp;</p></td>
                </tr>
                <tr>
                    <td colspan=2 class="tr1 td11"><p class="p0 ft8">india</p></td>
                    <td class="tr1 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td3"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td12"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td colspan=3 class="tr1 td14"><p class="p6 ft7">place of supply:</p></td>
                    <td colspan=2 class="tr1 td17"><p class="p0 ft8">29 - karnataka</p></td>
                    <td class="tr1 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr1 td6"><p class="p0 ft0">&nbsp;</p></td>
                </tr>
                <tr>
                    <td class="tr6 td18"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr6 td19"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr6 td20"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr6 td21"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr6 td22"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr6 td20"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr6 td23"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr6 td22"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr6 td20"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr6 td22"><p class="p0 ft0">&nbsp;</p></td>
                    <td colspan=2 class="tr6 td20"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr6 td22"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr6 td20"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr7 td6"><p class="p0 ft0">&nbsp;</p></td>
                </tr>
                <tr>
                    <td rowspan=2 class="tr8 td24"><p class="p7 ft9">#</p></td>
                    <td rowspan=2 class="tr8 td25"><p class="p8 ft9">Description</p></td>
                    <td rowspan=2 class="tr8 td26"><p class="p9 ft9">Quantity</p></td>
                    <td rowspan=2 class="tr8 td27"><p class="p10 ft9">Rate (inr)</p></td>
                    <td rowspan=2 class="tr8 td28"><p class="p11 ft9">Total</p></td>
                    <td rowspan=2 class="tr8 td26"><p class="p12 ft9">Discount</p></td>
                    <td rowspan=2 class="tr8 td29"><p class="p12 ft2">Taxable Value</p></td>
                    <td colspan=2 class="tr5 td30"><p class="p13 ft9">State GST</p></td>
                    <td colspan=3 class="tr5 td30"><p class="p14 ft9">Central GST</p></td>
                    <td colspan=2 class="tr5 td30"><p class="p15 ft9">Integrated GST</p></td>
                    <td class="tr9 td6"><p class="p0 ft0">&nbsp;</p></td>
                </tr>
                <tr>
                    <td rowspan=2 class="tr9 td31"><p class="p11 ft9">Rate</p></td>
                    <td rowspan=2 class="tr9 td32"><p class="p16 ft9">Amount</p></td>
                    <td rowspan=2 class="tr9 td31"><p class="p11 ft9">Rate</p></td>
                    <td colspan=2 rowspan=2 class="tr9 td32"><p class="p17 ft2">Amount</p></td>
                    <td rowspan=2 class="tr9 td31"><p class="p11 ft9">Rate</p></td>
                    <td rowspan=2 class="tr9 td33"><p class="p16 ft9">Amount</p></td>
                    <td class="tr10 td6"><p class="p0 ft10">&nbsp;</p></td>
                </tr>
                <tr>
                    <td class="tr11 td34"><p class="p0 ft11">&nbsp;</p></td>
                    <td class="tr11 td35"><p class="p0 ft11">&nbsp;</p></td>
                    <td class="tr11 td36"><p class="p0 ft11">&nbsp;</p></td>
                    <td class="tr11 td37"><p class="p0 ft11">&nbsp;</p></td>
                    <td class="tr11 td38"><p class="p0 ft11">&nbsp;</p></td>
                    <td class="tr11 td36"><p class="p0 ft11">&nbsp;</p></td>
                    <td class="tr11 td39"><p class="p0 ft11">&nbsp;</p></td>
                    <td class="tr12 td6"><p class="p0 ft12">&nbsp;</p></td>
                </tr>
                <tr>
                    <td class="tr5 td40"><p class="p12 ft9">1.</p></td>
                    <td class="tr5 td41"><p class="p18 ft9">Smart Health Connect Subscription Charges</p></td>
                    <td class="tr5 td42"><p class="p19 ft9">2,000.00</p></td>
                    <td class="tr5 td43"><p class="p10 ft9">1.00</p></td>
                    <td class="tr5 td44"><p class="p19 ft2">2,000.00</p></td>
                    <td class="tr5 td42"><p class="p10 ft9">-</p></td>
                    <td class="tr5 td45"><p class="p12 ft9">2,000.00</p></td>
                    <td class="tr5 td44"><p class="p11 ft9">9.00 %</p></td>
                    <td class="tr5 td42"><p class="p12 ft9">180.00</p></td>
                    <td class="tr5 td44"><p class="p11 ft9">9.00 %</p></td>
                    <td colspan=2 class="tr5 td42"><p class="p12 ft9">180.00</p></td>
                    <td class="tr5 td44"><p class="p10 ft9">-</p></td>
                    <td class="tr5 td42"><p class="p12 ft9">-</p></td>
                    <td class="tr13 td6"><p class="p0 ft0">&nbsp;</p></td>
                </tr>
                <tr>
                    <td class="tr14 td46"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td47"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td48"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td49"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td50"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td48"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td51"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td50"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td48"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td50"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td52"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td53"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td50"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td48"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr15 td6"><p class="p0 ft14">&nbsp;</p></td>
                </tr>
                <tr>
                    <td class="tr5 td54"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td1"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td55"><p class="p10 ft7">Total</p></td>
                    <td class="tr5 td4"><p class="p18 ft7">2,000.00</p></td>
                    <td class="tr5 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td12"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td6"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td7"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td56"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td6"><p class="p0 ft0">&nbsp;</p></td>
                </tr>
                <tr>
                    <td class="tr14 td57"><p class="p0 ft13">&nbsp;</p></td>
                    <td colspan=3 class="tr14 td58"><p class="p0 ft13">&nbsp;</p></td>
                    <td colspan=2 class="tr14 td59"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td23"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td22"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td20"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td22"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td52"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td60"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td22"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td48"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr15 td6"><p class="p0 ft14">&nbsp;</p></td>
                </tr>
                <tr>
                    <td class="tr5 td54"><p class="p0 ft0">&nbsp;</p></td>
                    <td colspan=3 class="tr5 td61"><p class="p10 ft7">Total Amount of advance (rounded off)</p></td>
                    <td colspan=2 class="tr5 td62"><p class="p18 ft7">INR 2,360.00</p></td>
                    <td class="tr5 td12"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td6"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td7"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td56"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td6"><p class="p0 ft0">&nbsp;</p></td>
                </tr>
                <tr>
                    <td class="tr14 td57"><p class="p0 ft13">&nbsp;</p></td>
                    <td colspan=3 class="tr14 td58"><p class="p0 ft13">&nbsp;</p></td>
                    <td colspan=5 class="tr14 td63"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td22"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td52"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td60"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td22"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td48"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr15 td6"><p class="p0 ft14">&nbsp;</p></td>
                </tr>
                <tr>
                    <td class="tr5 td54"><p class="p0 ft0">&nbsp;</p></td>
                    <td colspan=3 class="tr5 td61"><p class="p10 ft7">Total amount of advance (in words)</p></td>
                    <td colspan=5 class="tr5 td8"><p class="p18 ft7">two thousand three hundred sixty rupees only</p></td>
                    <td class="tr5 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td6"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td7"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td56"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td6"><p class="p0 ft0">&nbsp;</p></td>
                </tr>
                <tr>
                    <td class="tr16 td57"><p class="p0 ft15">&nbsp;</p></td>
                    <td colspan=3 class="tr16 td58"><p class="p0 ft15">&nbsp;</p></td>
                    <td colspan=2 class="tr16 td59"><p class="p0 ft15">&nbsp;</p></td>
                    <td class="tr16 td23"><p class="p0 ft15">&nbsp;</p></td>
                    <td class="tr16 td22"><p class="p0 ft15">&nbsp;</p></td>
                    <td class="tr16 td20"><p class="p0 ft15">&nbsp;</p></td>
                    <td class="tr16 td22"><p class="p0 ft15">&nbsp;</p></td>
                    <td class="tr16 td52"><p class="p0 ft15">&nbsp;</p></td>
                    <td class="tr16 td60"><p class="p0 ft15">&nbsp;</p></td>
                    <td class="tr16 td22"><p class="p0 ft15">&nbsp;</p></td>
                    <td class="tr16 td48"><p class="p0 ft15">&nbsp;</p></td>
                    <td class="tr14 td6"><p class="p0 ft13">&nbsp;</p></td>
                </tr>
                <tr>
                    <td class="tr5 td54"><p class="p0 ft0">&nbsp;</p></td>
                    <td colspan=3 class="tr5 td61"><p class="p10 ft7">TDS deducted (pending remittance)</p></td>
                    <td colspan=2 class="tr5 td62"><p class="p18 ft7">INR 200.00</p></td>
                    <td class="tr5 td12"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td6"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td7"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td56"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td6"><p class="p0 ft0">&nbsp;</p></td>
                </tr>
                <tr>
                    <td class="tr14 td57"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td19"><p class="p0 ft13">&nbsp;</p></td>
                    <td colspan=2 class="tr14 td64"><p class="p0 ft13">&nbsp;</p></td>
                    <td colspan=2 class="tr14 td59"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td23"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td22"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td20"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td22"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td52"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td60"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td22"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td48"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr15 td6"><p class="p0 ft14">&nbsp;</p></td>
                </tr>
                <tr>
                    <td class="tr5 td54"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td1"><p class="p0 ft0">&nbsp;</p></td>
                    <td colspan=2 class="tr5 td65"><p class="p10 ft7">Amount received</p></td>
                    <td colspan=2 class="tr5 td62"><p class="p18 ft7">INR 2,160.00</p></td>
                    <td class="tr5 td12"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td2"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td6"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td7"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td4"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td56"><p class="p0 ft0">&nbsp;</p></td>
                    <td class="tr5 td6"><p class="p0 ft0">&nbsp;</p></td>
                </tr>
                <tr>
                    <td class="tr14 td57"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td19"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td20"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td49"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td22"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td20"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td23"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td22"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td20"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td22"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td52"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td60"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td22"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr14 td48"><p class="p0 ft13">&nbsp;</p></td>
                    <td class="tr15 td6"><p class="p0 ft14">&nbsp;</p></td>
                </tr>
            </table>
            <p class="p20 ft9">to purchase extra license, visit <a href="https://smarthealthconnect.com/subscription"><span class="ft16">https://smarthealthconnect.com/subscription</span></a>. To to view your usage, please visit <a href="https://smarthealthconnect.com/subscription/paymentHistory"><span class="ft16">https://smarthealthconnect.com/subscription/paymentHistory</span></a></p>
            <p class="p21 ft7">Our banker's details for remitting funds:</p>
            <table cellpadding=0 cellspacing=0 class="t1">
                <tr>
                    <td class="tr9 td66"><p class="p0 ft9">Bank Name</p></td>
                    <td colspan=2 class="tr9 td67"><p class="p22 ft9">: hdfc bank limited</p></td>
                </tr>
                <tr>
                    <td class="tr1 td68"><p class="p0 ft2">Current a/c no.</p></td>
                    <td colspan=2 class="tr1 td69"><p class="p23 ft8"> :1758 2560 000325</p></td>
                </tr>
                <tr>
                    <td class="tr9 td66"><p class="p0 ft9">Beneficiary</p></td>
                    <td colspan=2 class="tr9 td67"><p class="p22 ft2">: ApnaCare India Private Limited</p></td>
                </tr>
                <tr>
                    <td class="tr13 td66"><p class="p0 ft9">Swift code</p></td>
                    <td class="tr13 td70"><p class="p22 ft9">: hdfcinbb</p></td>
                    <td class="tr13 td69"><p class="p0 ft0">&nbsp;</p></td>
                </tr>
                <tr>
                    <td class="tr13 td66"><p class="p0 ft9">MICR</p></td>
                    <td class="tr13 td70"><p class="p22 ft9">: 560240065</p></td>
                    <td class="tr13 td69"><p class="p0 ft0">&nbsp;</p></td>
                </tr>
                <tr>
                    <td class="tr13 td66"><p class="p0 ft9">IFSC Code</p></td>
                    <td class="tr13 td70"><p class="p22 ft2">: hdfc0001755</p></td>
                    <td class="tr13 td69"><p class="p0 ft0">&nbsp;</p></td>
                </tr>
            </table>
            <table cellpadding=0 cellspacing=0 class="t2">
                <tr>
                    <td class="tr9 td71"><p class="p0 ft7">Other details:</p></td>
                    <td class="tr9 td72"><p class="p0 ft0">&nbsp;</p></td>
                </tr>
                <tr>
                    <td class="tr5 td71"><p class="p19 ft9">HSN</p></td>
                    <td class="tr5 td72"><p class="p0 ft2">: 998429 (other internet telecommunications services)</p></td>
                </tr>
                <tr>
                    <td colspan=2 class="tr13 td73"><p class="p19 ft9">whether tax is payable on reverse charge basis : no</p></td>
                </tr>
                <tr>
                    <td class="tr13 td71"><p class="p19 ft9">PAN No.</p></td>
                    <td class="tr13 td72"><p class="p0 ft9">: aacce7697j</p></td>
                </tr>
            </table>
            <p class="p26 ft21">Smart Health Connect<span class="ft20">is a private limited company with CIN no. </span>U85110KA2013FTC070132</p>
            <p class="p27 ft20"><span class="ft21">Registered Office: </span>21, Hiprofiles Building, Church Street, Bangalore 560001, Karnataka, India</p>
        </div>
    </body>
</html>
