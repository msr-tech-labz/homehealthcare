<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Smart Health Connect RECEIPT</title>
    <!-- Favicon-->
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <style>
    html,body
    {
        height: 100%;
        margin: 0px;
    }
    @page
    {
        size: auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }

    html, body{
        font-size: 13px;
        font-family: 'Roboto', Arial, Tahoma, sans-serif !important;
    }
    h1,h2,h3,h4,h5,h6{
        font-family: 'Roboto', Arial, Tahoma, sans-serif !important;
    }
    .border-grey{
        border-bottom: 0.1em solid #ccc !important;
    }
    .border-bottom{
        border-bottom: 2px solid #00b0e4 !important;
    }
    </style>
</head>

<body class="theme-blue" style="background: transparent !important;">
    <div style="position: absolute;top: 10px;">
        <div class="inv-foot-bar" style="margin-top:1%;width:100% !important;height:10px !important; background: #00b0e4 !important;text-align:center !important;line-height: 30px;font-size: 14px">
        </div>
    </div>
    <div style="border: 0px solid black;background: white;padding: 10px 30px !important;">
        <br>
        <div style="width:70%;display:inline-block;padding: 5px;margin-left:5px;font-size:13px;height:40px;vertical-align:middle !important">
            <h4 style="display:inline-block;font-weight:500;font-size:25px;color:#00b0e4 !important;border-bottom:1px solid #00b0e4;margin-right:0 !important;padding-right: 20px !important">Smart Health Connect RECEIPT FOR - {{ date("F Y", strtotime($sip->invoiceGenerated->created_at)) }}</h4>
        </div>
        <div style="width:30%;display:inline-block;margin-right:5px;vertical-align:top;padding-top:0px">
            <center><img src="http://homehealth.apnacare.app/uploads/provider/d0p4M1paYXJpU3pZeEN4dXlkUHQ2QT09/d0p4M1paYXJpU3pZeEN4dXlkUHQ2QT09.jpeg" class="org-img" style="width: auto;max-height:70px;clear:both;"></center>
        </div>
        <br>
    </div>
    <div style="width:99%;overflow:hidden;margin: 0 auto !important;padding:10px;font-size:15px !important;margin-left:5% !important">
        <div style="width:100%;height:50px !important;display:block;">
            <div style="width:50%;height:40px !important;display:inline-block">
                <div style="display:inline-block;width:30%;font-weight:600">Invoice Date :</div>
                <div class="border-grey" style="display:inline-block;width:60%;padding-left:10px;margin-top: 50px;">{{ \Carbon\Carbon::parse($sip->invoiceGenerated->created_at)->format('d-m-Y') }}</div>
            </div>
            <div style="width:50%;height:40px !important;display:inline-block">
                <div style="display:inline-block;width:40%;font-weight:600">Payment Date :</div>
                <div class="border-grey" style="display:inline-block;width:40%;padding-left:10px;margin-top: 50px;">{{ \Carbon\Carbon::parse($sip->transaction_date)->format('d-m-Y') }}</div>
            </div>
        </div>
        <div style="height:50px !important;display:block;">
            <div style="display:inline-block;font-weight:600">Organization :</div>
            <div class="border-grey" style="width:90%;padding-left:20px">{{ $profile->organization_name }}</div>
        </div>
        <div style="height:50px !important;display:block;">
            <div style="display:inline-block;font-weight:600">Name :</div>
            <div class="border-grey" style="width:90%;padding-left:20px">{{ $profile->contact_person }}</div>
        </div>
        <div style="height:50px !important;display:block;">
            <div style="display:inline-block;font-weight:600">Mode of payment :</div>
            <div class="border-grey" style="width:90%;padding-left:20px">Online</div>
        </div>
        <div style="width:100%;height:50px !important;display:block;">
            <div style="width:50%;height:50px !important;display:inline-block">
                <div style="display:inline-block;width:40%;font-weight:600">Payment Ref No. :</div>
                <div class="border-grey" style="width:60%;padding-left:20px">{{ $sip->transaction_id }}</div>
            </div>
            <div style="width:50%;height:50px !important;display:inline-block">
                <div style="display:inline-block;width:40%;font-weight:600">Payment Against :</div>
                <div class="border-grey" style="width:60%;padding-left:20px">{{ date("F", strtotime($sip->invoiceGenerated->created_at)) }}</div>
            </div>
        </div>
        <br>
        <div style="height:50px !important;display:block;">
            <div style="display:inline-block;font-weight:600">Amount in Rs. :</div>
            <div class="border-grey" style="width:90%;padding-left:20px">{{ $sip->total_amount }}</div>
        </div>
        <div style="height:50px !important;display:block;">
            <div style="display:inline-block;font-weight:600">Amount in words :</div>
            <div class="border-grey" style="width:90%;padding-left:20px">
                {? $f = new \NumberFormatter("en", \NumberFormatter::SPELLOUT); ?}
                {{ ucwords($f->format((round($sip->total_amount)))).' Rupees Only' }}
            </div>
        </div>
        <div style="height:50px !important;display:block;">
            <div style="display:inline-block;width:auto;font-weight:600">Description of service :</div>
            <div class="border-grey" style="width:90%;padding-left:20px">
                Payment for the Usage of Smart Health Connect Web Portal
            </div>
        </div>
    </div>

    <div style="position: absolute;bottom: 30px;">
        <div class="inv-foot-add" style="width:100% !important;color: #00b0e4 !important;text-align:center !important;line-height: 30px;font-size: 14px;margin-top:10px !important;">
            Smart Health Connect<br>
            <div style="color: #333 !important;font-size:12px !important;line-height:18px !important">
                #21,High Profiles Building,Church Street,Bengaluru,Karnataka,India,560016<br>
                <b>Ph:</b> 8688279500  <b>Website:</b> https://www.apnacare.in
            </div>
        </div>

        <div class="inv-foot-bar" style="margin-top:1%;width:100% !important;height:10px !important; background: #00b0e4 !important;text-align:center !important;line-height: 30px;font-size: 14px">
        </div>

    </div>
</body>

</html>
