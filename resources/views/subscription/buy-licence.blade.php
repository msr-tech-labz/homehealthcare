<div class="card" style="margin-left: 30px;">
    <div class="header clearfix">
        <h2>Buy Licences</h2>
    </div>
    <div class="body">
        <div class="col-md-10 form-horizontal col-md-offset-2">
            <div class="row clearfix">
                <div class="col-lg-3 col-md-4 col-sm-5 col-xs-5 form-control-label" style="text-align: right">
                    <label for="first_name">Number of Licenses</label>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <div class="form-group">
                        <select class="form-control show-tick" id="credit_count">
                            <option value="">-</option>
                            <option value="50">50</option>
                            <option value="75">75</option>
                            <option value="100">100</option>
                            <option value="150">150</option>
                            <option value="200">200</option>
                            <option value="Custom">Custom</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-4">
                    <label id="charge_per_user"> x Rs {{ $charge_per_user }} /-( Charge Per User )</label>
                </div>
            </div>
            <div class="row clearfix hide" id="customCreditBlock">
                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
                    <label for="first_name">Custom</label>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                    <div class="form-group">
                        <div class="form-line">
                            <input type="number" min="10" max="9999" class="form-control" id="custom_credit_count"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-3 col-md-4 col-sm-5 col-xs-5 form-control-label" style="text-align: right">
                    <label for="amount">Amount</label>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                    <div class="form-group">
                        <div class="form-line">
                            <input type="number" min="10" max="99999" class="form-control" id="amount" readonly disabled/>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row clearfix hide" id="amountBlock">
                <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 text-center">
                    <h4 style="font-weight:500">Amount for <b class="cc_count">0</b> credits is <b class="cc_amount">₹ 0.00</b></h4>
                </div>
            </div>

            <input type="checkbox" id="tds-checkbox" class="filled-in chk-col-light-blue" name="applyTds" value="10" checked="">
            <label for="tds-checkbox"><b>Deduct TDS at 10%?</b></label>
            <table border="0">
                <tbody>
                    <tr>
                    </tr>
                    <tr height="35">
                        <td>GST (at 18.00%)</td>
                        <td>&nbsp;:&nbsp;</td>
                        <td id="gst">₹ 0.00</td>
                    </tr>
                    <tr height="35" class="tds-block">
                        <td id="tds-text" style="">TDS Deduction amount</td>
                        <td id="tds-col">&nbsp;:&nbsp;</td>
                        <td id="tds-span" style="">₹ 0.00</td>
                    </tr>
                    <tr height="20"></tr>
                    <tr height="35">
                        <td id="totalAmountDescription"><b>Total Amount to be paid<b></td>
                        <td>&nbsp;:&nbsp;</td>
                        <td id="totalAmountText"><b>₹ 0.00</b></td>
                    </tr>
                    <tr height="30"></tr>
                    <tr>
                        <td colspan="3">
                            <form class="text-center" name="payment-form" action="{{ route('subscription.process-payment') }}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" id="payment_data" name="payment_data" value="">
                                <button type="submit" onclick="javascript: return validateForm();" class="btn btn-primary btn-block waves-effect">Proceed</button>
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="clearfix"></div>
    </div>
</div>

@section('page.scripts')
@parent
<script>
    var GST = 18.00;
    var tdsPercentValue = $("input[name=applyTds]:checked").val() || 0;

    $(function(){
        $('.content').on('change','#credit_count', function(){
            if($(this).val() == 'Custom'){
                $('#customCreditBlock').removeClass('hide');
                $('#customCreditBlock input[type=text]').val('');
            }else{
                calculateCreditAmount();
                $('#customCreditBlock input[type=text]').val('');
                $('#customCreditBlock').addClass('hide');
            }
        });

        $('.content').on('change','#custom_credit_count', function(){
            if($(this).val() < 50){
                alert("Min. credit should be greater than 49");
                $('#custom_credit_count').focus();
                $('#custom_credit_count').val(50);
            }
            calculateCreditAmount();
        });

        $('.content').on('change','input[name=applyTds]', function(){
            if($(this).is(':checked')){
                $(".tds-block").show();
            }else{
                $(".tds-block").hide();
            }
        });
    });

    function validateForm(){
        if($('#credit_count option:selected').val() < 50 || $('#credit_count option:selected').val() == ''){
            alert("Please select no. of credits");
            $('#credit_count').focus();
            return false;
        }

        return true;
    }

    $('.content').on('change','input[name="applyTds"]', function(){
        calculateCreditAmount();
    })

    function calculateCreditAmount(){
        var creditCount = $('#credit_count option:selected').val();
        if(creditCount == 'Custom'){
            creditCount = $('#custom_credit_count').val();
        }

        actualAmount = creditCount * chargePerUser;
        totalAmt = 0;

        $('.cc_count').html(creditCount);
        $('.cc_amount').html('₹ '+actualAmount.toFixed(2));
        $('#amount').val(actualAmount.toFixed(2));

        // Calculate Tax and other charges
        gst = Number(actualAmount) * GST / 100;

        amount = Number(actualAmount) + Number(gst);
        tdsAmount = 0;

        // TDS Deduction
        if(tdsPercentValue && $('input[name="applyTds"]').is(':checked')) {
            $("#tds-text").show();
            $("#tds-col").show();
            $("#tds-span").show();
            tdsAmount = Math.round(tdsPercentValue / 100 * Number(actualAmount));
            totalAmt = Number(amount) - Number(tdsAmount);
        }else{
            $("#tds-text").hide();
            $("#tds-col").hide();
            $("#tds-span").hide();
            totalAmt = Number(amount);
        }

        $('#gst').html('₹ '+gst.toFixed(2));
        $('#tds-span').html('₹ '+tdsAmount.toFixed(2));
        $('#totalAmountText').html('₹ '+totalAmt.toFixed(2));

        var paymentData = {
            credit: creditCount,
            actual_amount: parseFloat(actualAmount).toFixed(2),
            gst: parseFloat(gst).toFixed(2),
            tds: parseFloat(tdsAmount).toFixed(2),
            total_amount: parseFloat(totalAmt).toFixed(2),
        };

        $('input[name="payment_data"]').val(JSON.stringify(paymentData));
        // console.log(paymentData);
    }

</script>
@endsection
