<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Renew Subscription - ApnaFocus</title>
    <!-- Favicon-->
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{ ViewHelper::ThemePlugin('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ ViewHelper::ThemePlugin('node-waves/waves.css') }}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ ViewHelper::ThemePlugin('animate-css/animate.css') }}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ ViewHelper::ThemeCss('style.css') }}" rel="stylesheet">
    <style>
        html, body{
            font-size: 13px;
            font-family: 'Roboto', Arial, Tahoma, sans-serif !important;
        }
        h1,h2,h3,h4,h5,h6{
            font-family: 'Roboto', Arial, Tahoma, sans-serif !important;
        }
        .form-group{
            margin-bottom: 0 !important;
        }
    </style>
</head>

<body class="theme-blue" style="background: #f3f3f3 !important;">
    <div class="card" style="margin: 20px !important">
        <div class="col-md-12" style="margin: 0">
            <div class="text-center"><h3 style="color: #3f51b5;font-weight: normal">ApnaFocus Subscription Payment</h3></div>
            <hr>
            <div class="text-center"><h5 style="font-weight:normal">Please enter your Admin Credentials to continue</h5></div><br>
            
            <div class="col-md-12">
                <div class="col-md-4">
                    <div class="col-md-5">
                        <label><h5>Organisation Code</h5></label>
                    </div>
                    <div class="col-sm-7">
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" class="form-control" id="org_code" name="org_code" placeholder="Organization Code" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-5">
                        <label><h5>Admin Email</h5></label>
                    </div>
                    <div class="col-sm-7">
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" class="form-control" id="admin_email" name="admin_email" placeholder="Email" required autofocus>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-5">
                        <label><h5>Admin Password</h5></label>
                    </div>
                    <div class="col-sm-7">
                        <div class="form-group">
                            <div class="form-line">
                                <input type="password" class="form-control" id="admin_password" name="admin_password" placeholder="Password" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div><br>        
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary waves-effect verifyOrg" style="margin-top: 0px !important;">Continue</button>
                </div>
            </div>            
            
            <div class="col-md-12 hide orgNotFound">
               <hr>
               <code>* The Entered Credentials do not match/are invalid/have been disabled. </code><br>
               <code>* Please check the credentials that has an Admin privilege. </code><br>
               <code>* Please check the credentials again. </code>
            </div>
            
            <div class="col-md-12 hide orgDetails">
                <hr>
                <div class="col-md-6">
                    <h2 class="card-inside-title text-center">Billing Details</h2>
                    <table class="table table-bordered">
                        <tr>
                            <th>Organization Name</th>
                            <td class="orgName"></td>
                        </tr>
                        <tr>
                            <th>Address</th>
                            <td class="orgAddess"></td>
                        </tr>
                        <tr>
                            <th>Active Licenses</th>
                            <td class="licenses"></td>
                        </tr>
                        <tr>
                            <th>Rate</th>
                            <td id="charge_per_user"></td>
                        </tr>
                        <tr>
                            <td colspan="2"><code>* If the above details match your Organisation details, proceed to payment.</code></td>
                        </tr>
                    </table>
                </div>                    
                
                <div class="col-md-6">
                    <h2 class="card-inside-title text-center">Payment Details</h2>
                    <table class="table table-bordered">
                        <tr>
                            <th width="40%">Number of Licenses</th>
                            <td id="credit_count"></td>
                        </tr>
                        <tr>
                            <th>Amount</th>
                            <td id="amount"></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="checkbox" id="tds-checkbox" class="filled-in chk-col-light-blue" name="applyTds" value="10" checked="">
                                <label for="tds-checkbox"><b>Deduct TDS at 10%?</b></label>
                            </td>
                        </tr>
                        <tr height="35">
                            <td>GST (at 18.00%)</td>
                            <td id="gst">₹ 0.00</td>
                        </tr>
                        <tr height="35" class="tds-block">
                            <td id="tds-text" style="">TDS Deduction amount</td>
                            <td id="tds-span" style="">₹ 0.00</td>
                        </tr>
                        <tr height="35">
                            <td id="totalAmountDescription"><b>Total Amount to be paid<b></td>
                            <th id="totalAmountText">₹ 0.00</th>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <form class="text-center" name="payment-form" action="{{ route('subscription.process-payment') }}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" id="payment_data" name="payment_data" value="">
                                    <button type="submit" onclick="javascript: return validateForm();" class="btn btn-primary waves-effect">Proceed to Payment</button>
                                </form>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>             
        </div>
        <div class="clearfix"></div><br>       
    </div>
    
    <div class="row text-center">
        <?php 
        if (!empty($_SERVER["HTTP_CLIENT_IP"]))
        {
         //check for ip from share internet
         $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"]))
        {
         // Check for the Proxy User
         $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        else
        {
         $ip = $_SERVER["REMOTE_ADDR"];
        }
        ?>
        <span>Your IP address is : <code>{{ $ip }}</code></span>
    </div>

    
    <!-- Jquery Core Js -->
    <script src="{{ ViewHelper::ThemePlugin('jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ ViewHelper::ThemePlugin('bootstrap/js/bootstrap.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('node-waves/waves.js') }}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('jquery-validation/jquery.validate.js') }}"></script>    
    <script>
        var GST = 18.00;
        var tdsPercentValue = $("input[name=applyTds]:checked").val() || 0;

        $(document).on('change','input[name=applyTds]', function(){
            if($(this).is(':checked')){
                $(".tds-block").show();
            }else{
                $(".tds-block").hide();
            }
        });

        $(document).on('change','input[name="applyTds"]', function(){
            calculateCreditAmount();
        })

        $(document).on('click', '.verifyOrg', function(){
            var org_code       = $('#org_code').val();
            var admin_email    = $('#admin_email').val();
            var admin_password = $('#admin_password').val();

            if(org_code && admin_email && admin_password){
                $('.orgDetails').addClass('hide');
                $('.orgNotFound').addClass('hide');
                $('.creditsCard').addClass('hide');
                $.ajax({
                    url : '{{ route('subscription.renewPaymentPage') }}',
                    type: 'POST',
                    data: {org_code: org_code, admin_email: admin_email, admin_password: admin_password,_token: '{{ csrf_token() }}' },
                    success: function (data){
                        if(data.user && data.user.user_type == 'Admin'){
                            $('.orgDetails').removeClass('hide');
                            $('.orgName').text(data.profile.organization_name);
                            $('.orgAddess').text(data.profile.organization_address+' '+data.profile.organization_city);
                            $('.licenses').text(data.active_users);
                            $('.creditsCard').removeClass('hide');

                            $('#credit_count').attr('val',data.active_users);
                            $('#credit_count').text(data.active_users);
                            
                            $('#charge_per_user').attr('val',data.subscriber.charge_per_user);
                            $('#charge_per_user').text('&#8377; '+data.subscriber.charge_per_user+' per user');

                            $('#amount').attr('val',data.active_users*data.subscriber.charge_per_user);
                            $('#amount').text(data.active_users*data.subscriber.charge_per_user+' @ '+data.subscriber.charge_per_user+' per user');

                            actualAmount = data.active_users * data.subscriber.charge_per_user;

                            // Calculate Tax and other charges
                            gst = Number(actualAmount) * GST / 100;
                            amount = Number(actualAmount) + Number(gst);
                            
                            tdsAmount = Math.round(tdsPercentValue / 100 * Number(actualAmount));
                            totalAmt = Number(amount) - Number(tdsAmount);

                            $('#gst').html('₹ '+gst.toFixed(2));
                            $('#tds-span').html('₹ '+tdsAmount.toFixed(2));
                            $('#totalAmountText').html('₹ '+totalAmt.toFixed(2));

                            var paymentData = {
                                credit: data.active_users,
                                actual_amount: parseFloat(actualAmount).toFixed(2),
                                gst: parseFloat(gst).toFixed(2),
                                tds: parseFloat(tdsAmount).toFixed(2),
                                total_amount: parseFloat(totalAmt).toFixed(2),
                            };

                            $('input[name="payment_data"]').val(JSON.stringify(paymentData));
                        }else{
                            $('.orgNotFound').removeClass('hide');                                
                        }
                    },
                    error: function (error){
                        console.log(error);
                    }
                });
            }else{
                if(!org_code) alert('Please Enter Organisation Code');
                if(!admin_email) alert('Please Enter Admin Email');
                if(!admin_password) alert('Please Enter Admin Password');
            }
        });

        function calculateCreditAmount(){
            var creditCount = $('#credit_count').attr('val');
            var chargePerUser = $('#charge_per_user').attr('val');
            console.log(creditCount+' - '+chargePerUser);

            actualAmount = creditCount * chargePerUser;
            totalAmt = 0;

            // Calculate Tax and other charges
            gst = Number(actualAmount) * GST / 100;

            amount = Number(actualAmount) + Number(gst);
            tdsAmount = 0;

            // TDS Deduction
            if(tdsPercentValue && $('input[name="applyTds"]').is(':checked')) {
                $("#tds-text").show();
                $("#tds-col").show();
                $("#tds-span").show();
                tdsAmount = Math.round(tdsPercentValue / 100 * Number(actualAmount));
                totalAmt = Number(amount) - Number(tdsAmount);
            }else{
                $("#tds-text").hide();
                $("#tds-col").hide();
                $("#tds-span").hide();
                totalAmt = Number(amount);
            }

            $('#gst').html('₹ '+gst.toFixed(2));
            $('#tds-span').html('₹ '+tdsAmount.toFixed(2));
            $('#totalAmountText').html('₹ '+totalAmt.toFixed(2));

            var paymentData = {
                credit: creditCount,
                actual_amount: parseFloat(actualAmount).toFixed(2),
                gst: parseFloat(gst).toFixed(2),
                tds: parseFloat(tdsAmount).toFixed(2),
                total_amount: parseFloat(totalAmt).toFixed(2),
            };

            $('input[name="payment_data"]').val(JSON.stringify(paymentData));
        }
    </script>           
</body>

</html>
