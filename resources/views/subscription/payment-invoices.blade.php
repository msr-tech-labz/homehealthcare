<style>
    td{
        vertical-align: middle !important;
    }
</style>
<div class="card" style="margin-left: 30px;">
    <div class="header clearfix">
        <h2>Pending Payments</h2>
    </div>
    <div class="body">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Date</th>
                    <th>Due Date</th>
                    <th>Amount</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @forelse ($payments as $index => $p)
                <tr>
                    <td>{{ ($index + 1) }}</td>
                    <td style="font-weight: bold;">{{ $p->created_at->format('d-m-Y')}}</td>
                    <td style="font-weight: bold;">{{ $p->created_at->addDays(5)->format('d-m-Y')}}</td>
                    <td>₹ {{ number_format($p->total_amount,2) }}</td>
                    <td>{{ $p->status }}</td>
                    @if($p->status == 'PENDING')
                    <td class="text-center"><a href="{{ route('subscription.payment-invoices-pay',['pid' => Helper::encryptor('encrypt',$p->id),'tid' => session('tenant_id')]) }}" class="btn btn-primary">PAY</a></td>
                    @else
                    <td class="text-center"><a href="{{ route('subscription.payment-invoice-download',['pid' => Helper::encryptor('encrypt',$p->id),'tid' => session('tenant_id')]) }}" class="btn bg-teal btn-circle waves-effect waves-circle waves-float"><i class="material-icons">arrow_downward</i></a></td>
                    @endif
                </tr>
            @empty
                <tr>
                    <td colspan="10" class="text-center">All settled up.</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>
