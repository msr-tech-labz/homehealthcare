<div class="card" style="margin-left: 30px;">
    <div class="header clearfix">
        <h2>Payment History</h2>
    </div>
    <div class="body">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th class="text-center">Date of Payment</th>
                    <th class="text-center"># of Licenses</th>
                    <th class="text-center">Actual Amount</th>
                    <th class="text-center">GST</th>
                    <th class="text-center">TDS</th>
                    <th class="text-center">Amount Paid</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Receipt</th>
                </tr>
            </thead>
            <tbody>
            @forelse ($payments as $index => $p)
                <tr>
                    <td class="text-center">{{ ($index + 1) }}</td>
                    <td class="text-center">{{ $p->transaction_date->format('d-M-Y').' '.$p->created_at->format('h:i A') }}</td>
                    <td class="text-center">{{ $p->credit_count }}</td>
                    <td class="text-center">₹ {{ number_format($p->actual_amount,2) }}</td>
                    <td class="text-center">₹ {{ number_format($p->gst,2) }}</td>
                    <td class="text-center">₹ {{ number_format($p->tds,2) }}</td>
                    <td class="text-center">₹ {{ number_format($p->total_amount,2) }}</td>
                    <td class="text-center">{{ $p->status }}</td>
                    <td class="text-center">
                        @if($p->status == 'SUCCESS')
                        <a href="{{ route('subscription.payment-receipt', Helper::encryptor('encrypt',$p->id)) }}" title="View Receipt"><img src="{{ asset('img/icons/pdf_icon.png') }}" alt="Receipt"/></a>
                        @endif
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="10" class="text-center">No record(s)</td>
                </tr>
            @endforelse
            @if(count($payments))

            @endif
            </tbody>
        </table>
    </div>
</div>
