@extends('layouts.corporate-layout')

@section('page_title','Dashboard - ')

@section('active_home','active')

@section('page.styles')
<style>
a{text-decoration:none !important;}
.icon {
  max-width: 100%;
  -moz-transition: all 0.3s;
  -webkit-transition: all 0.3s;
  transition: all 0.3s;
}
.icon:hover {
  -moz-transform: scale(1.1);
  -webkit-transform: scale(1.1);
  transform: scale(1.1);
}
.icon:hover a>div{
  font-weight: normal !important;
  font-size: 11px !important;
  color: #7138c0  !important;
}
.loader{
    background-image: url('img/loading.gif');
    background-repeat: no-repeat;
    background-position: center;
    background-size: 100px 100px;
    position: relative;
}
.dashstats{
    font-size:10px !important;
}
.panelTab {
    height: 210px;
    box-shadow: 0 20px 35px rgba(0, 0, 0, 0.12);
    background-color:#fff;
    padding:0;
}
.theme {
    background:#f1f6f6!important;
    background-image:url('../img/dashboard_background1.png')!important;
    background-size:cover !important;
}
.left-menu {margin-top:12px; margin-bottom:12px;}
.left-menu a {color:black;}
.left-menu img {
    height:60px;
    width: 60px;
    margin-bottom:6px;
}
.left-menu div {font-weight:500; letter-spacing:0.2em; font-size:10px;}
.header img {max-height:70px; vertical-align:middle;}
</style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 text-center">
                <div class="col-lg-12 col-sm-12 col-xs-6 icon left-menu" id="tourLead">
                    <a href="{{ route('corporate.lead.index') }}">
                        <img src="{{ asset('img/flaticons2/leads.png') }}"><br>
                        <div>LEADS</div>
                    </a>
                </div>

                <div class="col-lg-12 col-sm-12 col-xs-6 icon left-menu" id="tourLead">
                    <a href="#{{--route('corporate.billing')--}}">
                        <img src="{{ asset('img/flaticons2/billing.png') }}"><br>
                        <div>BILLING</div>
                    </a>
                </div>
                
                <div class="col-lg-12 col-sm-12 col-xs-6 icon left-menu" style="" id="tourHrm">
                    <a href="{{route('corporate.team.index')}}">
                        <img src="{{ asset('img/flaticons2/users.png') }}"><br>
                        <div>CORPORATE<br>TEAM</div>
                    </a>
                </div>

                <div class="col-lg-12 col-sm-12 col-xs-6 icon left-menu" id="tourReports">
                    <a href="#{{--route('corporate.reports')--}}">
                        <img src="{{ asset('img/flaticons2/reports.png') }}"><br>
                        <div>REPORTS</div>
                    </a>
                </div>
            </div>
            <div class="col-md-10">
              <div class="block-header"><h2>Branches</h2></div>
                <div style="max-height:460px; overflow-y:auto;">
                    @foreach($profiles as $key => $profile)
                        <a href="#">
                            <div class="card">
                                <div class="header">
                                    <div class="row">
                                        <div class="col-md-1">
                                            <img src="{{ empty($profile->organization_logo)?asset('img/default-avatar.jpg'):asset('../uploads/provider/'.\Helper::encryptor('encrypt',$profile->tenant_id).'/'.$profile->organization_logo) }}" class="img-responsive logo-tilt">
                                        </div>
                                        <div class="col-md-11">
                                            <h2>{{ $profile->organization_name }}</h2>
                                            <b>Address</b> - 
                                            {{ $profile->organization_address }}, 
                                            {{ $profile->organization_area }}, 
                                            {{ $profile->organization_city }}.
                                            @if(!empty($profile->contact_person))
                                                <br><b>Contact person</b> - {{ $profile->contact_person }}.
                                            @endif 
                                            @if(!empty($profile->phone_number))
                                                <br><b>Phone Number</b> - {{ $profile->phone_number }}. 
                                            @endif
                                            @if(!empty($profile->landline_number))
                                                / <b>Landline Number</b> - {{ $profile->landline_number }}.
                                            @endif
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

<div style="width:101%;height:40px;position: fixed;bottom: 0;background:#8835a7;left: -1px !important;line-height:40px;color:#fff;z-index: 1;">
    <span style="display:inline-block;float:left;padding-left: 10px"><small>Build v2.5.1 - 27-Feb-2019</small></span>
    <span style="display:inline-block;float:right;padding-right: 25px">{{ date('Y') }} &copy; Smart Health Connect</span>
</div>