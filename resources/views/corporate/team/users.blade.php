@extends('layouts.corporate-layout')

@section('page_title','Users - ')

@section('plugin.styles')
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />

<style>
.demo-radio-button label {min-width:60px !important;}
</style>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="row clearfix">
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('corporate.team.index') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-8">
                    Corporate Users
                    <small>List of all users</small>
                </h2>
                <button style="float:right;" class="btn btn-info waves-effect" data-toggle="modal" data-target="#addUser">Add new user</button>
            </div>
            <div class="body">
                <div class="row clearfix">
                    <form method="post" action="{{ route('corporate.users.update') }}">
                       {{ csrf_field() }}
                        <div class="table-responsive" style="width:96%;margin:0 auto">
                            <div class="clearfix"></div><br>
                            <table class="table table-bordered table-striped table-hover table-condensed nsTtable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Gender</th>
                                        <th>Password</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($users as $j => $user)
                                    <tr id="{{ \Helper::encryptor('encrypt',$user->id) }}">
                                        <td class="text-center">{{ $j + 1 }}</td>
                                        <td><input type="text" class="form-control" name="full_name" placeholder="Full name" value="{{ $user->full_name }}"></td>
                                        <td><input type="email" class="form-control" name="email" placeholder="Email id" value="{{ $user->email }}"></td>
                                        <td>
                                            <div class="demo-radio-button">
                                                <input name="gender{{ \Helper::encryptor('encrypt',$user->id) }}" type="radio" id="Male{{ $j }}" class="with-gap radio-col-deep-purple" value="Male" <?php if (isset($user->gender) && $user->gender=="Male") echo "checked";?> >
                                                <label for="Male{{ $j }}">Male</label>

                                                <input name="gender{{ \Helper::encryptor('encrypt',$user->id) }}" type="radio" id="Female{{ $j }}" class="with-gap radio-col-deep-purple" value="Female" <?php if (isset($user->gender) && $user->gender=="Female") echo "checked";?> >
                                                <label for="Female{{ $j }}">Female</label>
                                            </div>
                                        </td>
                                        <td><input type="password" class="form-control" name="new_password" placeholder="Enter new password"></td>
                                        <td>
                                            <div class="switch">
                                                <label><input type="checkbox" name="userStatus" data-id="{{ Helper::encryptor('encrypt',$user->id) }}" {{ $user->status=='Active'?'checked':'' }}><span class="lever switch-col-deep-purple"></span></label>
                                            </div>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-primary updateUser" data-id="{{ \Helper::encryptor('encrypt',$user->id) }}">Update</button>
                                        </td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="6" class="text-center">No user(s) found</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table><br>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- User Modal -->
<div class="modal fade" id="addUser" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <form class="form-horizontal emailForm" method="POST" action="{{ route('corporate.users.add') }}" autocomplete="off">
            {{ method_field('POST') }}
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header bg-blue">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title action_email">New User</h4>
                </div>
                <div class="modal-body">
                    <div class="row clearfix">
                        <div class="col-sm-12 form-horizontal">
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="name">Name</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="name" class="form-control" placeholder="Full name of the user" required="required">
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="email">Email</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="email" name="email" class="form-control" placeholder="Email id of the user" autocomplete="off" required="required">
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="name">Gender</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="demo-radio-button">
                                            <input name="gender" type="radio" id="Male" class="with-gap radio-col-deep-purple" value="Male" checked>
                                            <label for="Male">Male</label>
                                            <input name="gender" type="radio" id="Female" class="with-gap radio-col-deep-purple" value="Female">
                                            <label for="Female">Female</label>
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="user_password">Password</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="password" name="user_password" class="form-control" placeholder="**********" autocomplete="off" required="required">
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="email">Status</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <input name="status" type="radio" id="Active" class="with-gap radio-col-deep-purple" value="Active" checked>
                                        <label for="Active">Active</label>
                                        <input name="status" type="radio" id="Inactive" class="with-gap radio-col-deep-purple" value="Inactive">
                                        <label for="Inactive">Inactive</label>
                                    </div>
                                </div>
                            </div><br>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success waves-effect">Save</button>
                    <button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection

@section('plugin.scripts')
<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
<!-- Bootstrap Notify Plugin Js -->
<script src="http://smarthealthconnect.localhost/themes/default/plugins/bootstrap-notify/bootstrap-notify.js"></script>
<script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    if({{ count($users) }}){
        $('.nsTtable').DataTable({
            "lengthMenu": [ [25, 50, 100, -1], [25, 50, 100, "All"] ],
            searching: false
        });
    }
});

$('.content').on('change','input[name=userStatus]', function(){
    var userId = $(this).data('id');
    var status = $(this).is(':checked')?'Active':'Inactive';
    $('input[name=userStatus]').attr('disabled', true);

    $.ajax({
        url: '{{ route('corporate.users.update')}}',
        type: 'POST',
        data: {_token: '{{ csrf_token() }}', userId:userId, status:status},
        success: function(data){
            showNotification('bg-green', 'User Updated Successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');

            $('input[name=userStatus]').attr('disabled', false).html('Update');
        },
        error: function (err){
            console.log(err);
            showNotification('bg-red', 'Something Went Wrong, Please Refresh', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
        }
    });
});

$('.content').on('click','.updateUser',function(){
    var userId = $(this).attr('data-id');
    var full_name = $('#'+userId+' input[name=full_name]').val();
    var email = $('#'+userId+' input[name=email]').val();
    var gender = $('input[name=gender'+userId+']:checked').val();
    var new_password = $('#'+userId+' input[name=new_password]').val();
    $('.updateUser').attr('disabled', true);
    $(this).html('Updating..');

    $.ajax({
        url: '{{ route('corporate.users.update') }}',
        type: 'POST',
        data: {_token:'{{csrf_token()}}', userId:userId, full_name:full_name, email:email, gender:gender, new_password:new_password},
        success: function(data){
            showNotification('bg-green', 'User Updated Successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');

            $('.updateUser').attr('disabled', false).html('Update');
        },
        error: function (err){
            console.log(err);
            showNotification('bg-red', 'Something Went Wrong, Please Refresh', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
        }
    })
});
</script>
@endsection