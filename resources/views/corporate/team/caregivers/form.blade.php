@extends('layouts.corporate-layout')

@section('page_title',isset($e)?'Edit Employee - ':'Add Employee - ')

@section('active_caregivers','active')

@section('plugin.styles')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('passtrength/passtrength.css') }}" rel="stylesheet" />
@endsection

@section('page.styles')
<style>
	.card .header .header-dropdown{
		top: 15px;
	}
	hr{
		margin-top: 10px;
		margin-bottom: 10px;
	}
	/* Map Styles */
	#infowindow-content .title {
		font-weight: bold;
	}

	#infowindow-content {
		display: none;
	}

	#map #infowindow-content {
		display: inline;
	}

	.pac-card {
		margin: 10px 10px 0 0;
		border-radius: 2px 0 0 2px;
		box-sizing: border-box;
		-moz-box-sizing: border-box;
		outline: none;
		box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
		background-color: #fff;
		font-family: Roboto;
		width: 80%;
	}

	#pac-container {
		padding-bottom: 12px;
		margin-right: 12px;
		z-index: 10000 !important;
	}

	.pac-container{
		top: 605px !important;
	}

	.pac-controls {
		display: inline-block;
		padding: 5px 11px;
	}

	.pac-controls label {
		font-family: Roboto;
		font-size: 13px;
		font-weight: 300;
	}

	#pac-input-p {
		background-color: #fff;
		font-family: Roboto;
		font-size: 15px;
		font-weight: 300;
		margin-left: 12px;
		padding: 0 11px 0 13px;
		text-overflow: ellipsis;
		width: 400px;
	}

	#pac-input-p:focus {
		border-color: #4d90fe;
	}
	#title {
		color: #fff;
		background-color: #4d90fe;
		font-size: 20px;
		font-weight: 500;
		padding: 6px 12px;
	}
	.table-bordered tbody tr td, .table-bordered tbody tr th {
	    padding: 5px 1px 0px 10px !important;
	}
</style>
@endsection

@section('content')
<div class="page-loader-wrapper" style="display: none;">
    <div class="loader">
        <div class="md-preloader pl-size-md">
            <svg viewbox="0 0 75 75">
                <circle cx="37.5" cy="37.5" r="33.5" class="pl-red" stroke-width="4" />
            </svg>
        </div>
        <p>Please wait while the documents is uploading...</p>
    </div>
</div>
@if (count($errors) > 0)
<div class="row clearfix">
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif

<div id="upload-message">
</div>

<div class="row">
	<form id="caregiverForm" action="{{ isset($e)?route('corporate.updateEmployee', Helper::encryptor('encrypt',$e->id)):route('corporate.storeEmployee') }}" method="POST" enctype="multipart/form-data">
		{{ csrf_field() }}
		@if(isset($e)){{ method_field('PUT') }}@endif
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<a href="{{ route('corporate.employee') }}" class="clearLocalStore pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
						<i class="material-icons">arrow_back</i>
					</a>
					<h2 class="col-sm-10">
						{{ isset($e)?'Edit':'New'}} Employee
						<small>Employee {{ isset($e)?'Updation':'Creation'}} Form</small>
					</h2>
					<ul class="header-dropdown m-r--5">
						<div class="btn-group" role="group">
							<button type="button" class="btn btn-success m-l-15 clearLocalStore waves-effect"><i class="material-icons" style="color: #fff">save</i></button>
							<button type="submit" onclick="javascript: return validator();" class="btn btn-success clearLocalStore waves-effect" style="line-height: 1.9;padding-left:0">Save Employee</button>
						</div>&nbsp;&nbsp;
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="body" style="padding-top: 0">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active">
							<a href="#basic_details_tab" data-toggle="tab" aria-expanded="false">
								<i class="material-icons">person</i> Basic Details
							</a>
						</li>
						<li role="presentation" class="">
							<a href="#professional_details_tab" data-toggle="tab" aria-expanded="false">
								<i class="material-icons">business_center</i> Professional Details
							</a>
						</li>
						<li role="presentation" class="">
							<a href="#official_details_tab" data-toggle="tab" aria-expanded="true">
								<i class="material-icons">work</i> Official Details
							</a>
						</li>
						<li role="presentation" class="">
							<a href="#salary_details_tab" data-toggle="tab" aria-expanded="true">
								<i class="material-icons">account_balance_wallet</i> Salary Details
							</a>
						</li>
						@if(isset($e))
						<li role="presentation" class="">
							<a href="#documents_tab" data-toggle="tab">
								<i class="material-icons">cloud_upload</i> Documents
							</a>
						</li>
						<li role="presentation" class="">
							<a href="#verification_tab" data-toggle="tab">
								<i class="material-icons">cloud_upload</i> Verification
							</a>
						</li>
						@endif
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane fade active in" id="basic_details_tab">
							<div class="row clearfix">
								<div class="col-sm-6 form-horizontal">
									<h2 class="card-inside-title">
										Basic Details
										<small>Name, Gender, Blood Group</small>
									</h2>
									@if(isset($e))
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="employee_id">Employee ID</label>
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
											<div class="input-group">
												<div class="form-line">
													<input autocomplete = "off" style="text-transform: uppercase;" type="text" id="employee_id" name="employee_id" class="form-control" placeholder="Employee ID" value="{{ isset($e)?$e->employee_id:'' }}" />
													<span id="emp_id_status" style="font-weight: bold;"></span>
												</div>
											</div>
										</div>
										<div class="col-lg-3">
										<button type="button" class="btn bg-purple btn-circle waves-effect waves-circle waves-float" name="saveEmp" onclick="javascript: return checkEmpID();">
											<i class="material-icons notranslate" style="color: #fff">save</i>
										</button>
										</div>
									</div>
									@endif
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="first_name">First Name</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="first_name" name="first_name" class="form-control" placeholder="First Name" value="{{ isset($e)?$e->first_name:'' }}" required>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="last_name">Last Name</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="last_name" name="last_name" class="form-control" placeholder="Last Name" value="{{ isset($e)?$e->last_name:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="date_of_birth">Date of Birth</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" class="datepast form-control" id="date_of_birth" name="date_of_birth" placeholder="Ex: 30-07-1990" value="{{ (isset($e) && $e->date_of_birth != null)?$e->date_of_birth->format('d-m-Y'):'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="gender">Gender</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select class="form-control show-tick" id="gender" name="gender" required>
													<option value="">-- Please select--</option>
													<option value="Male">Male</option>
													<option value="Female">Female</option>
													<option value="Other">Other</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="blood_group">Blood Group</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select class="form-control show-tick" id="blood_group" name="blood_group">
													<option value="">-- Please select --</option>
													<option value="A+">A+</option>
													<option value="A-">A-</option>
													<option value="B+">B+</option>
													<option value="B-">B-</option>
													<option value="O+">O+</option>
													<option value="O-">O-</option>
													<option value="AB+">AB+</option>
													<option value="AB-">AB-</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="marital_status">Marital Status</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select class="form-control show-tick" id="marital_status" name="marital_status">
													<option value="">-- Please select --</option>
													<option value="Single">Single</option>
													<option value="Married">Married</option>
													<option value="Other">Other</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="religion">Religion</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<select class="form-control show-tick" id="religion" name="religion">
														<option value="">-- Please select --</option>
														<option value="Buddhism">Buddhism</option>
														<option value="Christianity">Christianity</option>
														<option value="Hinduism">Hinduism</option>
														<option value="Islam">Islam</option>
														<option value="Jainism">Jainism</option>
														<option value="Sikhism">Sikhism</option>
														<option value="Judaism">Judaism</option>
														<option value="Other">Other</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="religion">Food Habits</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="demo-checkbox food_habits">
													<input name="food_habits" type="radio" id="food_habits_veg" class="with-gap radio-col-green" value="Veg" checked="" />
													<label for="food_habits_veg">Vegetarian</label>
													<input name="food_habits" type="radio" id="food_habits_nonveg" class="with-gap radio-col-red" value="Non-Veg"/>
													<label for="food_habits_nonveg">Non Vegetarian</label>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="col-sm-6 form-horizontal">
									<h2 class="card-inside-title">
										Communication Details
										<small>Phone Number, Email</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="mobile_number">Mobile Number</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="mobile_number" name="mobile_number" class="form-control" placeholder="e.g. +919876543210" value="{{ isset($e)?$e->mobile_number:'' }}" required >
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="alternate_number">Alternate Number</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="alternate_number" name="alternate_number" class="form-control" placeholder="e.g. +919876543210" value="{{ isset($e)?$e->alternate_number:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="email">Email</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
												<input autocomplete = "off" type="email" id="email" onkeyup="checkemail();" name="email" class="form-control" placeholder="Email address" value="{{ isset($e)?$e->email:'' }}">
												<span id="email_status" style="font-weight: bold;"></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="personal_email">Personal Email</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
												<input autocomplete = "off" type="email" id="personal_email" name="personal_email" class="form-control" placeholder="Personal email address" value="{{ isset($e)?$e->personal_email:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="password">Password</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="password" @if(isset($e)) pattern=".{0}|.{8,}" @else pattern=".{0}|.{8,}" required @endif  autocomplete="new-password" id="password" name="password" class="form-control" placeholder="{{ !empty($e->user->password)?'***********':'Please Choose a password'}}" value="">
												</div>
											</div>
										</div>
									</div>
									<h2 class="card-inside-title">
										Profile Picture
										<small>Caregiver Picture</small>
									</h2>
									<div class="row clearfix">
										<div class="col-sm-1"></div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 profile-img-block">
											{? $filePath = asset('img/default-avatar.jpg'); ?}
											@if(isset($e) && $e->profile_image)
												@if(file_exists(public_path('uploads/provider/'.\Helper::encryptor('encrypt',$e->tenant_id).'/caregivers/'.\Helper::encryptor('encrypt',$e->id)).'/'.$e->profile_image))
													 {? $filePath = asset('uploads/provider/'.\Helper::encryptor('encrypt',$e->tenant_id).'/caregivers/'.\Helper::encryptor('encrypt',$e->id).'/'.$e->profile_image); ?}
												@endif
											@endif
											<img src="{{ $filePath }}" style="width: 160px; height: 160px;" class="img img-thumbnail img-profile"/>
										</div>
										<div class="col-lg-5 col-md-4 col-sm-4 col-xs-4 text-center">
											<br>
											<input type="file" id="profile_picture" name="profile_picture" class="form-control profile-picture"><br><br>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-sm-6 form-horizontal">
									<h2 class="card-inside-title">
										Current Address
										<small>Address / Area, City and State</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col-md-offset-4">
											<a class="btn btn-primary btnGetFromMap" data-target="#locationModal" data-toggle="modal">Get from Map</a>
										</div>
									</div>
									<div class="row clearfix" id="mapView" style="display:none">
										<div class="col-sm-12 form-horizontal">
											<div class="pac-card" id="pac-card">
												<div>
													<div id="title">
														Location search
													</div>
													<div id="type-selector" class="pac-controls">
														<input type="radio" name="type" id="changetype-all" checked="checked">
														<label for="changetype-all">All</label>

														<input type="radio" name="type" id="changetype-establishment">
														<label for="changetype-establishment">Establishments</label>

														<input type="radio" name="type" id="changetype-address">
														<label for="changetype-address">Addresses</label>

														<input type="radio" name="type" id="changetype-geocode">
														<label for="changetype-geocode">Geocodes</label>
													</div>
													<div id="strict-bounds-selector" class="pac-controls">
														<input type="checkbox" id="use-strict-bounds" value="">
														<label for="use-strict-bounds">Strict Bounds</label>
													</div>
												</div>
												<div id="pac-container">
													<input id="pac-input-p" type="text"
													placeholder="Enter a location">
												</div>
											</div>
											<div id="map" style="width: 98%; height:400px;"></div>
											<div id="infowindow-content">
												<img src="" width="16" height="16" id="place-icon">
												<span id="place-name"  class="title"></span><br>
												<span id="place-address"></span>
											</div>
										</div>
									</div>
									{{-- <div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label">
											<label><i class="material-icons">search</i></label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="pac-input2" data-mode="current" class="pac-input form-control" placeholder="Type here to search location"/>
												</div>
											</div>
										</div>
									</div> --}}
									<hr style="margin-top: 0px !important">
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="current_address">Flat No./Building</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="current_address" name="current_address" class="form-control" placeholder="Flat No. / Building Name / Street Name" value="{{ isset($e)?$e->current_address:'' }}"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="current_area">Area</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="current_area" name="current_area" class="form-control" placeholder="Area" value="{{ isset($e)?$e->current_area:'' }}"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="current_city"><span class="required">City</span> / Zipcode</label>
										</div>
										<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="current_city" name="current_city" class="form-control" placeholder="City" value="{{ isset($e)?$e->current_city:'' }}"/>
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
											<div class="form-group">
												<div class="form-line">
													<input type="number" id="current_zipcode" name="current_zipcode" class="form-control" placeholder="Zip Code" value="{{ isset($e)?$e->current_zipcode:'' }}"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="current_state"><span class="required">State</span> / Country</label>
										</div>
										<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="current_state" name="current_state" class="form-control" placeholder="State" value="{{ isset($e)?$e->current_state:'' }}"/>
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="current_country" name="current_country" class="form-control" placeholder="Country" value="{{ isset($e)?$e->current_country:'' }}"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="latitude">Latitude &amp; Longitude</label>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="latitude" name="latitude" class="form-control" placeholder="Latitude" value="{{ isset($e)?$e->latitude:'' }}"/>
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="longitude" name="longitude" class="form-control" placeholder="Longitude" value="{{ isset($e)?$e->longitude:'' }}"/>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6 form-horizontal">
									<h2 class="card-inside-title">
										<div class="col-sm-6" style="margin-bottom: 0!important">
											Permanent Address
											<small>Address / Area, City and State</small>
										</div>
										<div class="col-sm-6" style="margin-bottom: 0!important">
											<input type="checkbox" id="copy_current_address" class="filled-in chk-col-light-blue" />
											<label for="copy_current_address">Same as Current Address</label>
										</div>
										<div class="clearfix"></div>
									</h2>
									<div class="row clearfix hide">
										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label">
											<label><i class="material-icons">search</i></label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="pac-input" data-mode="permanent" class="pac-input form-control" placeholder="Type here to search location"/>
												</div>
											</div>
										</div>
									</div>
									<hr style="margin-top: 0px !important">
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="permanent_address">Flat No./Building</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="permanent_address" name="permanent_address" class="form-control" placeholder="Address / Area" value="{{ isset($e)?$e->permanent_address:'' }}"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="permanent_area">Area</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="permanent_area" name="permanent_area" class="form-control" placeholder="Area" value="{{ isset($e)?$e->permanent_area:'' }}"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="permanent_city"><span class="required">City</span> / Zipcode</label>
										</div>
										<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="permanent_city" name="permanent_city" class="form-control" placeholder="City" value="{{ isset($e)?$e->permanent_city:'' }}"/>
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
											<div class="form-group">
												<div class="form-line">
													<input type="number" id="permanent_zipcode" name="permanent_zipcode" class="form-control" placeholder="Zip Code" value="{{ isset($e)?$e->permanent_zipcode:'' }}"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="permanent_state"><span class="required">State</span> / Country</label>
										</div>
										<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="permanent_state" name="permanent_state" class="form-control" placeholder="State" value="{{ isset($e)?$e->permanent_state:'' }}"/>
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="permanent_country" name="permanent_country" class="form-control" placeholder="Country" value="{{ isset($e)?$e->permanent_country:'' }}"/>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="professional_details_tab">
							<div class="row clearfix">
								<div class="col-sm-6 form-horizontal">
									<h2 class="card-inside-title">
										Professional Skills
										<small>Languages, Experience, Achievements</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="specialization">Specialization</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="">
													<select class="form-control show-tick" id="specialization" name="specialization">
														<option value="0">-- Please select --</option>
														@if(isset($specializations) && count($specializations))
														@foreach($specializations as $s)
														<option value="{{ $s->id }}">{{ $s->specialization_name }}</option>
														@endforeach
														@endif
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="qualification">Qualification</label>
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="add_qualification" name="add_qualification" class="form-control doNotSave" placeholder="Qualification Name">
												</div>
											</div>
										</div>
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<a class="btn btn-primary pull-left add-qualification">+</a>
										</div><br>
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-md-offset-3" style="width: 75% !important">
											<div class="form-group">
												<div class="qualification-block">
													@if(isset($e->professional->qualification))
														{? $qualifications = explode(",",$e->professional->qualification); ?}
														@foreach ($qualifications as $qualification)
															@if(!empty($qualification))
																{? $id = Helper::uniqueID(5); ?}
																<div id="qualification_{{ $id }}" style="display:inline-block; margin: 5px; margin-right:10px;"><div style="display:inline-block;background: #f1eab0;padding:5px 10px;">{{ $qualification }}</div>
																<div data-id="{{ $id }}" style="display:inline-block !important;padding: 0 10px !important;height:20px !important;border-radius:20px;margin-left:-2px" class="btn btn-danger del-qualification">x</div></div>
															@endif
														@endforeach
													@endif
												</div>
												<input type="hidden" id="qualification" name="qualification" value="{{ isset($e->professional)?$e->professional->qualification:'' }}">
											</div>
										</div>
									</div>									
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="experience_level">Staff Level</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="">
													<select class="form-control show-tick" id="experience_level" name="experience_level">
														<option value="">-- Please select --</option>
														<option value="1">Level 1</option>
														<option value="2">Level 2</option>
														<option value="3">Level 3</option>
														<option value="4">Level 4</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="college_name">College/Board Name</label>
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="add_college" name="add_college" class="form-control doNotSave" placeholder="College Name">
												</div>
											</div>
										</div>
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<a class="btn btn-primary pull-left add-college">+</a>
										</div><br>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7 col-md-offset-3">
											<div class="form-group">
												<div class="college-block">
													@if(isset($e->professional->college_name))
														{? $colleges = explode(",",$e->professional->college_name); ?}
														@foreach ($colleges as $college)
															@if(!empty($college))
																{? $id = Helper::uniqueID(6); ?}
																<div id="college_{{ $id }}" style="display:inline-block; margin: 5px; margin-right:10px;"><div style="display:inline-block;background: #f1eab0;padding:5px 10px;">{{ $college }}</div>
																<div data-id="{{ $id }}" style="display:inline-block !important;padding: 0 10px !important;height:20px !important;border-radius:20px;margin-left:-2px" class="btn btn-danger del-college_">x</div></div>
															@endif
														@endforeach
													@endif
												</div>
												<input type="hidden" id="college_name" name="college_name" value="{{ isset($e->professional)?$e->professional->college_name:'' }}">
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="experience">Experience<sup>(In Years)</sup></label>

										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="number" step="0.1" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" id="experience" name="experience" class="form-control" placeholder="Eg. 5 or 0.5" value="{{ isset($e->professional)?$e->professional->experience:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="current_address_line">Languages Known</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="demo-checkbox" id="lang" style="padding-top: 1.5%">
													@if(isset($languages) && count($languages))
													@foreach($languages as $l)
													<input type="checkbox" id="languages_{{ strtolower($l->language_name) }}" name="languages_known[]" class="filled-in chk-col-light-blue" value="{{ $l->language_name }}" />
													<label for="languages_{{ strtolower($l->language_name) }}">{{ $l->language_name }}</label>
													@endforeach
													@endif
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="achievements">Achievements</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<textarea style="text-transform: capitalize;" id="achievements" name="achievements" class="form-control" placeholder="Achievements">{{ isset($e->professional)?$e->professional->achievements:'' }}</textarea>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="col-sm-6 form-horizontal">
									<h2 class="card-inside-title">
										Skills
										<small>Specialization Skills</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 form-control-label">
											<label for="skill_id">Skills</label>
										</div>
										<button type="button" class="btn bg-purple waves-effect m-r-20" data-toggle="modal" data-target="#skillModal">{{isset($e)?'Update':'Select'}} Skills</button>
									</div>
									<div class="row clearfix">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<table id="skills_table" class="table table-bordered">
												<thead>
													<tr>
														<th>Skill Name</th>
														<th>Proficiency</th>
														<th></th>
													</tr>
												</thead>
												<tbody>
													<tr id="default"><td colspan="3" class="text-center">No skill(s) added</td></tr>
												</tbody>
											</table>
										</div>
									</div>
									<h2 class="card-inside-title">
										Workings Days
										<small>Select Days that you prefer to work on.</small><br>
											<div class="demo-checkbox workdays">
											    <input type="checkbox" id="md_checkbox_21" name="workdays[]" value="Mon" class="filled-in chk-col-red" @if(!isset($e)) checked @elseif(isset($e) && strpos($e->professional->working_days,'Mon') !== false) checked @endif/>
											    <label for="md_checkbox_21">Monday</label>
											    <input type="checkbox" id="md_checkbox_22" name="workdays[]" value="Tue" class="filled-in chk-col-light-green" @if(!isset($e)) checked @elseif(isset($e) && strpos($e->professional->working_days,'Tue') !== false) checked @endif/>
											    <label for="md_checkbox_22">Tuesday</label>
											    <input type="checkbox" id="md_checkbox_23" name="workdays[]" value="Wed" class="filled-in chk-col-purple" @if(!isset($e)) checked @elseif(isset($e) && strpos($e->professional->working_days,'Wed') !== false) checked @endif/>
											    <label for="md_checkbox_23">Wednesday</label>
											    <input type="checkbox" id="md_checkbox_24" name="workdays[]" value="Thu" class="filled-in chk-col-deep-purple" @if(!isset($e)) checked @elseif(isset($e) && strpos($e->professional->working_days,'Thu') !== false) checked @endif/>
											    <label for="md_checkbox_24">Thursday</label>
											    <input type="checkbox" id="md_checkbox_25" name="workdays[]" value="Fri" class="filled-in chk-col-indigo" @if(!isset($e)) checked @elseif(isset($e) && strpos($e->professional->working_days,'Fri') !== false) checked @endif/>
											    <label for="md_checkbox_25">Friday</label>
											    <input type="checkbox" id="md_checkbox_26" name="workdays[]" value="Sat" class="filled-in chk-col-brown" @if(!isset($e)) checked @elseif(isset($e) && strpos($e->professional->working_days,'Sat') !== false) checked @endif/>
											    <label for="md_checkbox_26">Saturday</label>
											    <input type="checkbox" id="md_checkbox_27" name="workdays[]" value="Sun" class="filled-in chk-col-deep-orange" @if(!isset($e)) checked @elseif(isset($e) && strpos($e->professional->working_days,'Sun') !== false) checked @endif/>
											    <label for="md_checkbox_27">Sunday</label>
											</div>
									</h2>
									<h2 class="card-inside-title">
										Working Hours
										<small>Select Duration that you prefer to work on.</small><br>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label">
												<label for="work_from_time">From Time</label>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
												<div class="form-group">
													<div class="form-line">
														<input type="text" class="time form-control" id="work_from_time" name="work_from_time" placeholder="Ex: 8:00 AM" value="{{ (isset($e->professional->working_hours_from) && $e->professional->working_hours_from != null)?Carbon\Carbon::parse($e->professional->working_hours_from)->format('h:i A'):'' }}">
													</div>
												</div>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label">
												<label for="work_to_time">To Time</label>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
												<div class="form-group">
													<div class="form-line">
														<input type="text" class="time form-control" id="work_to_time" name="work_to_time" placeholder="Ex: 6:00 PM" value="{{ (isset($e->professional->working_hours_to) && $e->professional->working_hours_to != null)?Carbon\Carbon::parse($e->professional->working_hours_to)->format('h:i A'):'' }}">
													</div>
												</div>
											</div>
										</div>
									</h2>
								</div>
							</div>
						</div>

						<div role="tabpanel" class="tab-pane fade" id="official_details_tab">
							<div class="row clearfix">
								<div class="col-sm-6">
									<div class="col-sm-12 form-horizontal">
										<h2 class="card-inside-title">
											Employment Details
											<small>Type, Department, Designation</small>
										</h2>
										@if(count($branches))
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="branch">Branch</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group">
													<select class="form-control show-tick" data-live-search="true" id="branch_id" name="branch_id">
														@foreach($branches as $b)
														<option value="{{ $b->id }}">{{ $b->branch_name }}</option>
														@endforeach
													</select>
												</div>
											</div>
										</div>
										@else
										<input type="hidden" name="branch_id" id="branch_id" value="0">
										@endif
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label class="required" for="role">Field Staff</label>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-8 col-xs-7">
												<div class="form-group" style="margin-top: 1%">
													<input name="role" type="radio" id="role_caregiver" class="with-gap radio-col-deep-purple" value="Caregiver" checked="" />
													<label for="role_caregiver">Yes</label>
													<input name="role" type="radio" id="role_manager" class="with-gap radio-col-deep-purple" value="Manager"/>
													<label for="role_manager">No</label>
												</div>
											</div>
											<div class="col-lg-3 deployable_check" style="display: none;">
												<input name="deployable" value="Yes" type="checkbox" id="deployable" class="filled-in chk-col-deep-purple" />
												<label class="col-lg-5 text-left" for="deployable" style="height: 30px;">Deployable</label>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="designation">Designation</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group">
													<select class="form-control show-tick" data-live-search="true" id="designation" name="designation">
														<option value="0">-- Please select --</option>
														@if(count($designations))
														@foreach($designations as $d)
														<option value="{{ $d->id }}">{{ $d->designation_name }}</option>
														@endforeach
														@endif
													</select>
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="department">Department</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group">
													<select class="form-control show-tick" data-live-search="true" id="department" name="department">
														<option value="0">-- Please select --</option>
														@if(count($departments))
														@foreach($departments as $d)
														<option value="{{ $d->id }}">{{ $d->department_name }}</option>
														@endforeach
														@endif
													</select>
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="manager">Manager</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group">
													<select class="form-control show-tick" data-live-search="true" id="manager" name="manager">
														<option value="0">-- Please select --</option>
														@if(count($managers))
														@foreach($managers as $m)
														<option value="{{ $m->id }}">{{ $m->first_name.' '.$m->last_name }}</option>
														@endforeach
														@endif
													</select>
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="source">Source</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group">
													<select class="form-control show-tick" data-live-search="true" id="source" name="source">
														<option value="">-- Please select --</option>
														<option value="College">College</option>
														<option value="Staff Referral">Staff Referral</option>
														<option value="Consultant">Consultant</option>
														<option value="Direct/Walk-in">Direct/Walk-in</option>
														<option value="DEO">DEO</option>
														<option value="Others">Others</option>
													</select>
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="source">Source Name</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group">
													<div class="form-line">
														<input type="text" class="form-control" id="source_name" name="source_name" value="{{isset($e->professional)?$e->professional->source_name:''}}" placeholder="Source Name(If any)"/>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="col-sm-12 form-horizontal">
										<h2 class="card-inside-title">
											Previous Employment Details
											<small>Company, Period</small>
										</h2>
										<div id="add-company-block">
											<div class="row clearfix">
												<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 form-control-label">
													<label>Add Employment</label>
												</div>
											</div>
											<div class="row clearfix">
												<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
													<label for="company_name">Company Name</label>
												</div>
												<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
													<div class="form-group">
														<div class="form-line">
															<input type="text" class="form-control" id="company_name" name="company_name" placeholder="Ex: ABC Inc">
														</div>
													</div>
												</div>
											</div>
											<div class="row clearfix">
												<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
													<label for="company_from_period">Period</label>
												</div>
												<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7" style="margin-bottom:0">
													<div class="col-sm-4">
														<div class="form-group">
															<div class="form-line">
																<input type="text" class="form-control" id="company_from_period" name="company_from_period" placeholder="Ex: Jan 2015">
															</div>
														</div>
													</div>
													<div class="col-sm-2 form-control-label" style="text-align: center !important">
														<label>to</label>
													</div>
													<div class="col-sm-4" style="margin-bottom:0">
														<div class="form-group">
															<div class="form-line">
																<input type="text" class="form-control" id="company_to_period" name="company_to_period" placeholder="Ex: Dec 2016">
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row clearfix text-center" style="margin-top:0">
												<a class="btn btn-primary btnAddEmployment">Add</a>
											</div>
											<input type="hidden" id="employments" name="employments" value="" />
											<hr>
											<table class="table table-bordered table-responsive employmentTable">
												<thead>
													<tr>
														<th>Company</th>
														<th>Period</th>
														<th width="10%"></th>
													</tr>
												</thead>
												<tbody>
													@if(isset($e->employments))
														@foreach ($e->employments as $employment)
															@if(!empty($employment))
																{? $id = Helper::uniqueID(6); ?}
																<tr data-id="{{ $id }}">
																	<td>{{ $employment->company_name }}</td>
																	<td>{{ $employment->period }}</td>
																	<td><a class="btn btn-danger btn-xs btn-sm btnRemoveEmployment" data-id="{{ $id }}"><i class="material-icons">delete</i></a></td>
																</tr>
															@endif
														@endforeach
													@else
														<tr>
															<td colspan="6" class="text-center">No data found</td>
														</tr>
													@endif
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="col-sm-6 form-horizontal">
									<br><br>
									@if(!isset($e))
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label class="required" for="access_role">Access Role</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group">
													<select class="form-control show-tick" data-live-search="true" id="access_role" name="access_role">
														<option value="">-- Please Select Access Role --</option>
														@foreach($roles as $role)
														<option value="{{ $role->id }}">{{ $role->display_name }}</option>
														@endforeach
													</select>
												</div>
											</div>
										</div>
									@endif
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="employment_type">Employment Type</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group" style="margin-top: 1%">
												<input name="employment_type" type="radio" id="employment_staff" class="with-gap radio-col-deep-purple" value="Staff" checked="" />
												<label for="employment_staff">Staff</label>
												<input name="employment_type" type="radio" id="employment_freelancer" class="with-gap radio-col-deep-purple" value="Freelancer"/>
												<label for="employment_freelancer">Freelancer</label>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="date_of_joining">Date of Joining</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" class="form-control" id="date_of_joining" name="date_of_joining" placeholder="Ex: 30-07-2014" value="{{ (isset($e->professional) && $e->professional->date_of_joining != null)?$e->professional->date_of_joining->format('d-m-Y'):'' }}">
												</div>
											</div>
										</div>
									</div>									
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="employment_status">Employment Status</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select class="form-control show-tick" data-live-search="true" id="employment_status" name="employment_status">
													<option value="1" {{ (isset($e) && $e->user->status == 0)?'selected':'' }}>Active</option>
													<option value="0" {{ (isset($e) && $e->user->status == 0)?'selected':'' }}>Inactive</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="" for="resignation_date">Date of Resignation</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" class="form-control date" id="resignation_date" name="resignation_date" placeholder="Ex: 30-07-2014" value="{{ (isset($e->professional) && $e->professional->resignation_date != null)?$e->professional->resignation_date->format('d-m-Y'):'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="resignation_type">Resignation Type</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select class="form-control show-tick" data-live-search="true" id="resignation_type" name="resignation_type">
													<option value>-- Please select --</option>
													<option {{ (isset($e) && $e->professional->resignation_type == 'Abscond')?'selected':'' }} value="Abscond">Abscond</option>
													<option {{ (isset($e) && $e->professional->resignation_type == 'Voluntary Resignation')?'selected':'' }} value="Voluntary Resignation">Voluntary Resignation</option>
													<option {{ (isset($e) && $e->professional->resignation_type == 'Forced resignation')?'selected':'' }} value="Forced resignation">Forced Resignation</option>
													<option {{ (isset($e) && $e->professional->resignation_type == 'Termination')?'selected':'' }} value="Termination">Termination</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="resignation_reason">Reason for resignation</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<textarea class="form-control" id="resignation_reason" name="resignation_reason" placeholder="Resignation Reason">{{ isset($e->professional)?$e->professional->resignation_reason:'' }}</textarea>
												</div>
											</div>
										</div>
									</div>
									<h2 class="card-inside-title">
										Accommodation Details
										<small>Staff Accommodation details</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="arranged_by">Arranged By</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group" style="margin-top: 1%">
												<input name="arranged_by" type="radio" id="arranged_by_self" class="with-gap radio-col-deep-purple" value="Self" />
												<label for="arranged_by_self">Self</label>
												<input name="arranged_by" type="radio" id="arranged_by_company" class="with-gap radio-col-deep-purple" value="Company"/>
												<label for="arranged_by_company">Company</label>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="accommodation_address">Address</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<textarea style="text-transform: capitalize;" id="accommodation_address" name="accommodation_address" class="form-control" placeholder="Address">{{ isset($e->accommodation)?$e->accommodation->accommodation_address:'' }}</textarea>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="accommodation_city">City</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="accommodation_city" name="accommodation_city" class="form-control" placeholder="City" value="{{ isset($e->accommodation)?$e->accommodation->accommodation_city:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="accommodation_state">State</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="accommodation_state" name="accommodation_state" class="form-control" placeholder="State" value="{{ isset($e->accommodation)?$e->accommodation->accommodation_state:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="accommodation_from_date">From Date</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" class="form-control" id="accommodation_from_date" name="accommodation_from_date" placeholder="Ex: 30-07-2014" value="{{ (isset($e->accommodation) && $e->accommodation->accommodation_from_date != null)?$e->accommodation->accommodation_from_date->format('d-m-Y'):'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="accommodation_to_date">To Date</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" class="form-control" id="accommodation_to_date" name="accommodation_to_date" placeholder="Ex: 30-07-2015" value="{{ (isset($e->accommodation) && $e->accommodation->accommodation_to_date != null)?$e->accommodation->accommodation_to_date->format('d-m-Y'):'' }}">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div role="tabpanel" class="tab-pane fade" id="salary_details_tab">
							<div class="col-sm-6 form-horizontal">
								<div class="col-sm-12 form-horizontal">
									<h2 class="card-inside-title">
										Account Details
										<small>PAN, Bank Name,Account Number, IFSC Code</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="pan_number">PAN Number</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: uppercase;" type="text" class="form-control" id="pan_number" name="pan_number" placeholder="PAN Number" value="{{ isset($e->account)?$e->account->pan_number:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="account_name">Account Name</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" class="form-control" id="account_name" name="account_name" placeholder="Account Name" value="{{ isset($e->account)?$e->account->account_name:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="account_number">Account Number</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="number" class="form-control" id="account_number" name="account_number" placeholder="Account Number" value="{{ isset($e->account)?$e->account->account_number:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="aadhar_number">Aadhar Number</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="number" class="form-control" id="aadhar_number" name="aadhar_number" placeholder="Aadhar Number" value="{{ isset($e->account)?$e->account->aadhar_number:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="bank_name">Bank Name</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select class="form-control show-tick" data-live-search="true" id="bank_name" name="bank_name">
													<option value="">-- Please select --</option>
													<option value="Allahabad Bank">Allahabad Bank</option>
													<option value="Andhra Bank">Andhra Bank</option>
													<option value="Axis Bank">Axis Bank</option>
													<option value="Bank of Baroda">Bank of Baroda</option>
													<option value="Bank of India">Bank of India</option>
													<option value="Bank of Maharashtra">Bank of Maharashtra</option>
													<option value="Bandhan Bank">Bandhan Bank</option>
													<option value="Canara Bank">Canara Bank</option>
													<option value="Catholic Syrian Bank">Catholic Syrian Bank</option>
													<option value="Central Bank of India">Central Bank of India</option>
													<option value="City Union Bank">City Union Bank</option>
													<option value="Citibank">Citibank</option>
													<option value="Corporation Bank">Corporation Bank</option>
													<option value="DCB Bank">DCB Bank</option>
													<option value="Dena Bank">Dena Bank</option>
													<option value="Dhanlaxmi Bank">Dhanlaxmi Bank</option>
													<option value="Federal Bank">Federal Bank</option>
													<option value="HDFC Bank">HDFC Bank</option>
													<option value="ICICI Bank">ICICI Bank</option>
													<option value="IDFC Bank">IDFC Bank</option>
													<option value="Indian Bank">Indian Bank</option>
													<option value="Indian Overseas Bank">Indian Overseas Bank</option>
													<option value="IndusInd Bank">IndusInd Bank</option>
													<option value="IDBI Ban">IDBI Bank</option>
													<option value="Jammu and Kashmir Bank">Jammu and Kashmir Bank</option>
													<option value="Karnataka Bank">Karnataka Bank</option>
													<option value="Karur Vysya Bank">Karur Vysya Bank</option>
													<option value="Kotak Mahindra Bank">Kotak Mahindra Bank</option>
													<option value="Lakshmi Vilas Bank">Lakshmi Vilas Bank</option>
													<option value="Nainital Bank">Nainital Bank</option>
													<option value="Oriental Bank of Commerce">Oriental Bank of Commerce</option>
													<option value="Punjab & Sindh Bank">Punjab & Sindh Bank</option>
													<option value="Punjab National Bank">Punjab National Bank</option>
													<option value="RBL Bank">RBL Bank</option>
													<option value="South Indian Bank">South Indian Bank</option>
													<option value="State Bank of India">State Bank of India</option>
													<option value="Syndicate Bank">Syndicate Bank</option>
													<option value="Tamilnad Mercantile Bank">Tamilnad Mercantile Bank</option>
													<option value="UCO Bank">UCO Bank</option>
													<option value="Union Bank of India">Union Bank of India</option>
													<option value="United Bank of India">United Bank of India</option>
													<option value="Vijaya Bank">Vijaya Bank</option>
													<option value="YES Bank">YES Bank</option>
													<option value="Other">Other</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="bank_branch">Branch</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" class="form-control" id="bank_branch" name="bank_branch" placeholder="Branch" value="{{ isset($e->account)?$e->account->bank_branch:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="ifsc_code">IFSC Code</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: uppercase;" type="text" class="form-control" id="ifsc_code" name="ifsc_code" placeholder="IFSC Code" value="{{ isset($e->account)?$e->account->ifsc_code:'' }}">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-sm-6 form-horizontal">
								<h2 class="card-inside-title">
									Salary Details
									<small>Basic, Allowances, Deductions</small>
								</h2>
								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
										<label for="basic_salary">Basic Salary</label>
									</div>
									<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
										<div class="form-group">
											<div class="form-line">
												<input type="number" class="form-control money-inr" id="basic_salary" name="basic_salary" placeholder="Amount in ₹" value="{{ isset($e->payroll)?$e->payroll->basic_salary:'' }}">
											</div>
										</div>
									</div>
								</div>
								<div class="salary-block">
									<div class="row clearfix">
										<table class="table table-bordered table-condensed">
											<tr style="background:#ccc">
												<th colspan="2" class="text-center">Allowances</th>
											</tr>
											<tr>
												<th style="padding: 2px;padding-left:5px;vertical-align:middle;">HRA</th>
												<td width="35%" style="padding: 2px">
													<div class="form-group" style="width: 90%;border: 1px solid #ccc; padding-left: 5px;margin-left: 5px;">
														<input type="number" class="form-control" id="hra" name="hra" placeholder="Amount in ₹" value="{{ isset($e->payroll)?$e->payroll->hra:'' }}">
													</div>
												</td>
											</tr>
											<tr>
												<th style="padding: 2px;padding-left:5px;vertical-align:middle;">Food and Accommodation</th>
												<td width="35%" style="padding: 2px">
													<div class="form-group" style="width: 90%;border: 1px solid #ccc; padding-left: 5px;margin-left: 5px;">
														<input type="number" class="form-control" id="food_and_accommodation" name="food_and_accommodation" placeholder="Amount in ₹" value="{{ isset($e->payroll)?$e->payroll->food_and_accommodation:'' }}">
													</div>
												</td>
											</tr>
											<tr>
												<th style="padding: 2px;padding-left:5px;vertical-align:middle;">Conveyance</th>
												<td width="35%" style="padding: 2px">
													<div class="form-group" style="width: 90%;border: 1px solid #ccc; padding-left: 5px;margin-left: 5px;">
														<input type="number" class="form-control" id="conveyance" name="conveyance" placeholder="Amount in ₹" value="{{ isset($e->payroll)?$e->payroll->conveyance:'' }}">
													</div>
												</td>
											</tr>
											<tr>
												<th style="padding: 2px;padding-left:5px;vertical-align:middle;">LTA</th>
												<td width="35%" style="padding: 2px">
													<div class="form-group" style="width: 90%;border: 1px solid #ccc; padding-left: 5px;margin-left: 5px;">
														<input type="number" class="form-control" id="lta" name="lta" placeholder="Amount in ₹" value="{{ isset($e->payroll)?$e->payroll->lta:'' }}">
													</div>
												</td>
											</tr>
											<tr>
												<th style="padding: 2px;padding-left:5px;vertical-align:middle;">Medical Allowance</th>
												<td width="35%" style="padding: 2px">
													<div class="form-group" style="width: 90%;border: 1px solid #ccc; padding-left: 5px;margin-left: 5px;">
														<input type="number" class="form-control" id="medical_allowance" name="medical_allowance" placeholder="Amount in ₹" value="{{ isset($e->payroll)?$e->payroll->medical_allowance:'' }}">
													</div>
												</td>
											</tr>
											<tr>
												<th style="padding: 2px;padding-left:5px;vertical-align:middle;">Other Allowances</th>
												<td width="35%" style="padding: 2px">
													<div class="form-group" style="width: 90%;border: 1px solid #ccc; padding-left: 5px;margin-left: 5px;">
														<input type="number" class="form-control" id="other_allowance" name="other_allowance" placeholder="Amount in ₹" value="{{ isset($e->payroll)?$e->payroll->other_allowance:'' }}">
													</div>
												</td>
											</tr>
											<tr>
												<th style="padding: 2px;padding-left:5px;vertical-align:middle;">Stipend</th>
												<td width="35%" style="padding: 2px">
													<div class="form-group" style="width: 90%;border: 1px solid #ccc; padding-left: 5px;margin-left: 5px;">
														<input type="number" class="form-control" id="stipend" name="stipend" placeholder="Amount in ₹" value="{{ isset($e->payroll)?$e->payroll->stipend:'' }}">
													</div>
												</td>
											</tr>
											<tr style="background:#ccc">
												<th colspan="2" class="text-center">Deductions</th>
											</tr>
											<tr>
												<th style="padding: 2px;padding-left:5px;vertical-align:middle;">PF Deduction</th>
												<td width="35%" style="padding: 2px">
													<div class="form-group" style="width: 90%;border: 1px solid #ccc; padding-left: 5px;margin-left: 5px;">
														<input type="number" class="form-control" id="pf_deduction" name="pf_deduction" placeholder="Amount in ₹" value="{{ isset($e->payroll)?$e->payroll->pf_deduction:'' }}">
													</div>
												</td>
											</tr>
											<tr>
												<th style="padding: 2px;padding-left:5px;vertical-align:middle;">ESI Deduction</th>
												<td width="35%" style="padding: 2px">
													<div class="form-group" style="width: 90%;border: 1px solid #ccc; padding-left: 5px;margin-left: 5px;">
														<input type="number" class="form-control" id="esi_deduction" name="esi_deduction" placeholder="Amount in ₹" value="{{ isset($e->payroll)?$e->payroll->esi_deduction:'' }}">
													</div>
												</td>
											</tr>
											<tr>
												<th style="padding: 2px;padding-left:5px;vertical-align:middle;">Other Deduction</th>
												<td width="35%" style="padding: 2px">
													<div class="form-group" style="width: 90%;border: 1px solid #ccc; padding-left: 5px;margin-left: 5px;">
														<input type="number" class="form-control" id="other_deduction" name="other_deduction" placeholder="Amount in ₹" value="{{ isset($e->payroll)?$e->payroll->other_deduction:'' }}">
													</div>
												</td>
											</tr>
										</table>
									</div>
									<div class="row clearfix">
										<div class="col-lg-5 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="effective_date_of_salary">Effective Date of Salary</label>
										</div>
										<div class="col-lg-7 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" class="date form-control" id="effective_date_of_salary" name="effective_date_of_salary" placeholder="Ex: 30-07-2016" value="{{ (isset($e->payroll) && $e->payroll->effective_date_of_salary != null)?$e->payroll->effective_date_of_salary:'' }}">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>

						<div role="tabpanel" class="tab-pane fade" id="documents_tab">
							<b>My Documents</b>
							{{-- <div style="text-align:center;color:red;margin-bottom: 30px;">
								<b>You must upload atleast one document from each of these Sub-sections.</b>
							</div> --}}
							<div class="row clearfix">
								<div class="form-horizontal">
									<div class="col-lg-6">
										<div class="col-lg-5 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="specialization">Verification Documents</label>
										</div>
										<div class="col-lg-7 col-md-9 col-sm-8 col-xs-7" style="margin-bottom: 0 !important;">
											<div class="form-group">
												<select class="form-control show-tick" id="verification_documents" name="verification_documents">
													<optgroup label="'Government Issued Identification">
														<option value="Government Issued Identification_PAN Card">PAN Card</option>
														<option value="Government Issued Identification_Aadhar Card">Aadhar Card</option>
														<option value="Government Issued Identification_Driving License">Driving License</option>
														<option value="Government Issued Identification_Voter ID">Voter ID</option>
													</optgroup>
													<optgroup label="Permanent Address">
														<option value="Permanent Address_Cooking Gas Bill">Cooking Gas Bill</option>
														<option value="Permanent Address_Voter ID">Voter ID</option>
														<option value="Permanent Address_BSNL Bill">BSNL Bill</option>
													</optgroup>
													<optgroup label="Current Address">
														<option value="Current Address_Rental Agreement">Rental Agreement</option>
														<option value="Current Address_Elecrticity Bill">Elecrticity Bill</option>
														<option value="Current Address_Water Bill">Water Bill</option>
														<option value="Current Address_Cooking Gas Bill">Cooking Gas Bill</option>
													</optgroup>
													<optgroup label="Educational Document">
														<option value="Educational Document_Degree Certificate">Degree Certificate</option>
														<option value="Educational Document_UG Certificate">UG Certificate</option>
														<option value="Educational Document_PG Certificate">PG Certificate</option>
													</optgroup>
													<optgroup label="IMC Document">
														<option value="IMC Document_Indian Medical Council Certificate">Indian Medical Council Certificate</option>
													</optgroup>
												</select>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="col-lg-5 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="doc_desc">Document Description</label>
											</div>
											<div class="col-lg-7 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group">
													<div class="form-line">
														<input style="text-transform: capitalize;" type="text" name="verification_documents_comment" id="verification_documents_comment" class="form-control" placeholder="Document Name,Institute Name,etc">
													</div>
												</div>
											</div>
										</div>
										<div class="col-lg-12 col-md-4 col-sm-4 col-xs-4 text-center" style="margin-top: 0px;margin-bottom: 0px;">
											<input type="file" id="doc_upload" class="form-control"><br><br>
										</div>
									</div>
									@if(isset($e))
									<div class="col-lg-6">
										<table class="table table-striped table-bordered"  style="text-align:center;">
											<th colspan="2" style="text-align: center;">Progress</th>
											<tr>
												<th>Government Issued Identification</th>
												<td>
													@if(in_array('Government Issued Identification', $uploaded_document_categories))
													<i class="material-icons" style="color: green;font-size: 20px;">check_circle</i>
													@else
													<i class="material-icons" style="color: red;font-size: 20px;">error</i>
													@endif
												</td>
											</tr>
											<tr>
												<th>Permanent Address</th>
												<td>
													@if(in_array('Permanent Address', $uploaded_document_categories))
													<i class="material-icons" style="color: green;font-size: 20px;">check_circle</i>
													@else
													<i class="material-icons" style="color: red;font-size: 20px;">error</i>
													@endif
												</td>
											</tr>
											<tr>
												<th>Current Address</th>
												<td>
													@if(in_array('Current Address', $uploaded_document_categories))
													<i class="material-icons" style="color: green;font-size: 20px;">check_circle</i>
													@else
													<i class="material-icons" style="color: red;font-size: 20px;">error</i>
													@endif
												</td>
											</tr>
											<tr>
												<th>Educational Document(s)</th>
												<td>
													@if(in_array('Educational Document', $uploaded_document_categories))
													<i class="material-icons" style="color: green;font-size: 20px;">check_circle</i>
													@else
													<i class="material-icons" style="color: red;font-size: 20px;">error</i>
													@endif
												</td>
											</tr>
											<tr>
												<th>IMC Document</th>
												<td>
													@if(in_array('IMC Document', $uploaded_document_categories))
													<i class="material-icons" style="color: green;font-size: 20px;">check_circle</i>
													@else
													<i class="material-icons" style="color: red;font-size: 20px;">error</i>
													@endif
												</td>
											</tr>
										</table>
									</div>
									@endif
									<div class="row clearfix">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-left: 0px !important;">
											<button type="button" class="btn btn-primary m-t-15 waves-effect btnDocumentUpload">UPLOAD</button>
										</div>
									</div>
									@if(isset($e))
									<div class="col-lg-12">
										<table class="table table-striped table-bordered table-striped uploaded-documents"  style="text-align:center;">
											<div style="text-align:center;color:red;"><b>Your Uploaded Documents will be displayed here, you must remove an uploaded document before reuploading for the same.</b></div>
											<tr>
												<th><u>Document</u></th>
												<th><u>Document Name</u></th>
												<th><u>View</u></th>
												<th><u>Action</u></th>
											</tr>
											@if(isset($e))
											@foreach($e->verifications as $i=>$ver)
											<tr style="text-align:center;">
												<td>
													<b><i>{{ $ver->doc_type }}</i></b>
												</td>
												<td>
													<b><i>{{ $ver->doc_name }}</i></b>
												</td>
												<td>
													@if(str_contains( $ver->doc_path, ['.pdf'] ) == false)
													<img style="width: 100px;height: 100px;pointer-events: none;" src="{{ asset('uploads/provider/'.\Helper::encryptor('encrypt',$e->tenant_id).'/caregivers/'.Helper::encryptor('encrypt',$e->id).'/document/'.$ver->doc_path)}}" class="btn btn-info fa fa-search">
													@endif
													<a href="{{ asset('uploads/provider/'.\Helper::encryptor('encrypt',$e->tenant_id).'/caregivers/'.Helper::encryptor('encrypt',$e->id).'/document/'.$ver->doc_path)}}" target="_blank" class="btn btn-info"><i class="material-icons">pageview</i></a>
												</td>
												<td>
													<button type="button" name="data" class="btn btn-danger m-t-15 waves-effect remove" data-docId="{{ $ver->id }}" data-docFolder="{{ \Helper::encryptor('encrypt',$e->id) }}" data-caregiverId="{{ $e->id }}" data-docName="{{$ver->doc_path}}" >Remove</button>
												</td>
											</tr>
											@endforeach
											@else
											<td class="text-center empty" colspan="5">
												<b><i>No Documents Uploaded</i></b>
											</td>
											@endif
										</table>
									</div>
									@else
									<div class="col-lg-12">
									<table class="table table-striped table-bordered table-striped to-be-uploaded-documents"  style="text-align:center;">
											<tr>
												<th class="text-center"><u>Document</u></th>
												<th class="text-center"><u>Document Name</u></th>
												<th class="text-center"><u>Status</u></th>
											</tr>
									</table>
									</div>
									@endif
								</div>
							</div>
						</div>
						@if(isset($e))
						<div role="tabpanel" class="tab-pane fade" id="verification_tab">
						    <b>Verify Me !!</b>
						    <div class="pre-req-info">
						        <b style="color: red;">Before Applying for verifications,make sure you have uploaded the required documents.</b><br>
						        <br>
						        <u><i>Basic Requirements</i></u><br>
						        <br>
						        1)Government Issued Identification Check (Atleast one of PAN card / Aadhar card / Driving License / Voter ID)<br>
						        2)Permanent Address (Atleast one of Cooking gas Bill / Voter ID / BSNL bill)<br>
						        3)Current Address (Atleast one of Rental Agreement / Electricity Bill / Water Bill / Cooking Gas Bill)<br>
						        4)Education (Atleast one of Xth / Intermediate / Degree / UG / PG certificate. Preferably your highest qualification.)<br>
						        5)IMC registration Number with Certificate.<br>
						        <br>
						        Verification is optional,but the verified caregiver would occur on top of non verified caregiver for searches made by customers.<br>
						        <br>
						        <b style="color: blue;">Note:-To be a verified professional,you must be verified in documents, police, educational and employment check.</b><br>
						        	<div class="row clearfix">
						        		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-left: 0px !important;">
						        			<div class="row clearfix">
						        				<div class="col-lg-2 col-md-2 col-sm-5 col-xs-5 form-control-label">
						        					<label for="agencySelect">Choose an Agency</label>
						        				</div>
						        				<div class="col-lg-3 col-md-2 col-sm-8 col-xs-7">
						        					<div class="form-group">
						        						<select class="form-control show-tick" id="agencySelect" name="agencySelect">
						        							<option value="0">-- Please select --</option>
						        							@if(count($agencies))
						        							@foreach($agencies as $a)
						        							<option value="{{ $a->id }}">{{ $a->agency_name }}</option>
						        							@endforeach
						        							@endif
						        						</select>
						        					</div>
						        				</div>
						        				<div class="col-lg-2 col-md-2 col-sm-8 col-xs-7">
						        					<button type="button" class="btn btn-primary m-t-15 waves-effect" id="caregiverVerify">APPLY</button>
						        				</div>
						        			</div>
						        		</div>
						        	</div>
						        	<input type="hidden" name="caregiverIdVerify" id="caregiverIdVerify" value="{{ $e->id }}">
						        	<input type="hidden" name="agencyIdVerify" id="agencyIdVerify" value="0">
						    </div>
						</div>
						@endif
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" name="skills" id="skills" value="">
		<input type="hidden" name="professional_details_id" value="{{ isset($e->professional)?Helper::encryptor('encrypt',$e->professional->id):0}}" />
		<input type="hidden" name="account_details_id" value="{{ isset($e->account)?Helper::encryptor('encrypt',$e->account->id):0}}" />
		<input type="hidden" name="payroll_details_id" value="{{ isset($e->payroll)?Helper::encryptor('encrypt',$e->payroll->id):0}}" />
		<input type="hidden" name="accommodation_details_id" value="{{ isset($e->accommodation)?Helper::encryptor('encrypt',$e->accommodation->id):0}}" />
		<input type="hidden" id="uploaded_doc_ids" name="uploaded_doc_ids"/>
		<input type="hidden" name="database_name" id="database_name" value="{{ $database_name }}" />
		@if(isset($e))
		<input type="hidden" name="curr_EmpId" id="curr_EmpId" value="{{ $e->employee_id }}" />
		@endif
	</form>
</div>

<div class="modal fade in" id="skillModal" tabindex="-1" role="dialog" style="padding-right: 17px;">
	<div class="modal-dialog" role="document" style="width: 60%;">
		<div class="modal-content bg-purple">
			<div class="modal-header">
				<h4 class="modal-title" id="skillModalLabel">Skills List</h4>
			</div>
			<div class="modal-body">
				<div class="row clearfix">
				@if(count($skills))
				@foreach($skills as $s)
					<input style="display: none;" name="skillcheck[]" value="{{ $s->id }}" type="checkbox" id="basic_checkbox_{{ $s->id }}" class="filled-in" />
					<label class="col-lg-5 text-left" for="basic_checkbox_{{ $s->id }}" style="height: 30px;">{{ $s->skill_name }}</label>

					<input style="display: none;" name="skillcheckproficiency[]_{{ $s->id }}" value="Excellent" id="basic_radio_{{ $s->id }}_Excellent" type="radio" class="filled-in" disabled="disabled"/>
					<label class="col-lg-2 text-left" for="basic_radio_{{ $s->id }}_Excellent" style="height: 30px;">Excellent</label>

					<input style="display: none;" name="skillcheckproficiency[]_{{ $s->id }}" value="Very Good" id="basic_radio_{{ $s->id }}_Very_Good" type="radio" class="filled-in" disabled="disabled"/>
					<label class="col-lg-3 text-left" for="basic_radio_{{ $s->id }}_Very_Good" style="height: 30px;">Very Good</label>

					<input style="display: none;" name="skillcheckproficiency[]_{{ $s->id }}" value="Good" id="basic_radio_{{ $s->id }}_Good" type="radio" class="filled-in" disabled="disabled"/>
					<label class="col-lg-2 text-left" for="basic_radio_{{ $s->id }}_Good" style="height: 30px;">Good</label>
				@endforeach
				@endif
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn bg-purple waves-effect btnAddSkill" data-dismiss="modal">SAVE CHANGES</button>
				<button type="button" class="btn bg-purple waves-effect" data-dismiss="modal">CLOSE</button>
			</div>
		</div>
	</div>
</div>
@endsection

@section('plugin.scripts')
<!-- Select Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<!-- Input Mask Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>
<!-- Moment Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<!-- Save As You Type Plugin Js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script src="{{ ViewHelper::ThemeJs('sayt.min.jquery.js') }}"></script>
<!-- Bootstrap Notify Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-notify/bootstrap-notify.js') }}"></script>
<script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('passtrength/passtrength.js') }}"></script>
@endsection

@section('page.scripts')
	<script type="text/javascript">
		$('#password').passtrength({
		  minChars: 8,
		  passwordToggle: true,
		  tooltip: true,
		  textWeak: "Weak",
		  textMedium: "Medium",
		  textStrong: "Strong",
		  textVeryStrong: "Very Strong",
		  eyeImg : '{{ asset('img/eye.svg') }}'
		});

		$('input[name=role]').change(function(){
		if($('#role_manager').is(':checked')){
				$('.deployable_check').css('display','block');
		    } else {
				$('.deployable_check').css('display','none');
				$('#deployable').attr('checked',false);
		    }
		});
	</script>
	<script type="text/javascript">
		$(function(){

			@if(!isset($e))
			$('#caregiverForm').sayt();
			$('#college_name').val('');
			$('#qualification').val('')
			$('.content').on('click','.clearLocalStore', function() {
				$('#caregiverForm').sayt({'erase': true});
			    return localStorage.removeItem('empFormLastTab');
			});
			@endif


			// for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
		    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		        // save the latest tab; use cookies if you like 'em better:
		        localStorage.setItem('empFormLastTab', $(this).attr('href'));
		    });

		    // go to the latest tab, if it exists:
		    var lastTab = localStorage.getItem('empFormLastTab');
		    if (lastTab) {
		        $('[href="' + lastTab + '"]').tab('show');
		    }
		});
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2-azeUdXbeAqQrvGtLZrbsXIFcofE_B8&libraries=places&region=IN"></script>
	<script type="text/javascript">
		var mapInit = 0;

		$(function(){
			$('.content').on('click','.btnGetFromMap',function(){
				if(!mapInit){
					initMap();
				}
				$('#mapView').toggle();
			});
		});

		var componentForm = {
			sublocality_level_2: 'short_name',
			street_number: 'short_name',
			route: 'long_name',
			locality: 'long_name',
			administrative_area_level_1: 'long_name',
			country: 'long_name',
			postal_code: 'short_name'
		};

		var componentIds = {
			street_number: 'current_address',
			route: 'current_address',
			sublocality_level_2: 'current_area',
			locality: 'current_city',
			administrative_area_level_1: 'current_state',
			country: 'current_country',
			postal_code: 'current_zipcode'
		};

		function initMap() {
			var map = new google.maps.Map(document.getElementById('map'), {
				center: {lat: 16.28004385258969, lng: 78.10594640625004},
				zoom: 6
			});
			var card = document.getElementById('pac-card');
			var input = document.getElementById('pac-input-p');
			var types = document.getElementById('type-selector');
			var strictBounds = document.getElementById('strict-bounds-selector');

			map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

			var autocomplete = new google.maps.places.Autocomplete(input);

			// Bind the map's bounds (viewport) property to the autocomplete object,
			// so that the autocomplete requests use the current map bounds for the
			// bounds option in the request.
			autocomplete.bindTo('bounds', map);

			var infowindow = new google.maps.InfoWindow();
			var infowindowContent = document.getElementById('infowindow-content');
			infowindow.setContent(infowindowContent);
			var marker = new google.maps.Marker({
				map: map,
				animation: google.maps.Animation.DROP,
				draggable: true,
				anchorPoint: new google.maps.Point(0, -29)
			});

			// Try HTML5 geolocation.
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function(position) {
					var pos = {
						lat: position.coords.latitude,
						lng: position.coords.longitude
					};
					map.setCenter(pos);
				}, function() {
					handleLocationError(true, infowindow, map.getCenter());
				});
			} else {
				// Browser doesn't support Geolocation
				handleLocationError(false, infowindow, map.getCenter());
			}

			google.maps.event.addListener(
				marker,
				'drag',
				function(event) {
					infowindow.close();
					document.getElementById('latitude').value = this.position.lat();
					document.getElementById('longitude').value = this.position.lng();
					//alert('drag');
				});


			google.maps.event.addListener(marker,'dragend',function(event) {
				infowindow.close();
				document.getElementById('latitude').value = this.position.lat();
				document.getElementById('longitude').value = this.position.lng();
					//alert('Drag end');
				});

			autocomplete.addListener('place_changed', function() {
				infowindow.close();
				marker.setVisible(false);
				var place = autocomplete.getPlace();
				if (!place.geometry) {
					// User entered the name of a Place that was not suggested and
					// pressed the Enter key, or the Place Details request failed.
					window.alert("No details available for input: '" + place.name + "'");
					return;
				}

				// If the place has a geometry, then present it on a map.
				if (place.geometry.viewport) {
					map.fitBounds(place.geometry.viewport);
				} else {
					map.setCenter(place.geometry.location);
					map.setZoom(17);  // Why 17? Because it looks good.
				}
				marker.setPosition(place.geometry.location);
				marker.setVisible(true);

				// Update Location Co-ordinates Values
				$('#latitude').val(place.geometry.location.lat());
				$('#longitude').val(place.geometry.location.lng());

				var address = '';
				if (place.address_components) {
					address = [
					(place.address_components[0] && place.address_components[0].short_name || ''),
					(place.address_components[1] && place.address_components[1].short_name || ''),
					(place.address_components[2] && place.address_components[2].short_name || '')
					].join(' ');

					// Get each component of the address from the place details
					// and fill the corresponding field on the form.
					for (var i = 0; i < place.address_components.length; i++) {
						var addressType = place.address_components[i].types[0];
						if (componentForm[addressType]) {
							var val = place.address_components[i][componentForm[addressType]];
							if(typeof componentIds[addressType] != 'undefined'){
								document.getElementById(componentIds[addressType]).value = val;
							}
						}
					}
				}

				infowindowContent.children['place-icon'].src = place.icon;
				infowindowContent.children['place-name'].textContent = place.name;
				infowindowContent.children['place-address'].textContent = address;
				infowindow.open(map, marker);
			});

			// Sets a listener on a radio button to change the filter type on Places
			// Autocomplete.
			function setupClickListener(id, types) {
				var radioButton = document.getElementById(id);
				radioButton.addEventListener('click', function() {
					autocomplete.setTypes(types);
				});
			}

			setupClickListener('changetype-all', []);
			setupClickListener('changetype-address', ['address']);
			setupClickListener('changetype-establishment', ['establishment']);
			setupClickListener('changetype-geocode', ['geocode']);

			document.getElementById('use-strict-bounds')
			.addEventListener('click', function() {
				console.log('Checkbox clicked! New state=' + this.checked);
				autocomplete.setOptions({strictBounds: this.checked});
			});

			mapInit = 1;
		}

		$('.content').on('input','.pac-input', function (){
			//initMap(this);
		});

		$('.content').on('change','input[name=employment_type]', function (){
			if($(this).val() == 'Staff'){
				$('.salary-block').show();
				$('label[for=basic_salary]').html('Basic Salary');
			}
			else{
				$('.salary-block').hide();
				$('label[for=basic_salary]').html('Rate/Day');
			}
		});

		function uniqueID(n){
		    if(n){
		        var text = "";
		        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		        for( var i=0; i < n; i++ )
		            text += possible.charAt(Math.floor(Math.random() * possible.length));

		        return text;
		    }
		}

		Array.prototype.remove = function() {
		    var what, a = arguments, L = a.length, ax;
		    while (L && this.length) {
		        what = a[--L];
		        while ((ax = this.indexOf(what)) !== -1) {
		            this.splice(ax, 1);
		        }
		    }
		    return this;
		};
		
		$('.content').on('click','.add-qualification',function(){
			if($('#add_qualification').val() != ''){
				id = uniqueID(4);
				html = `<div id="qualification_`+id+`" style="display:inline-block; margin: 5px; margin-right:10px;"><div style="display:inline-block;background: #f1eab0;padding:5px 10px;">`+ $('#add_qualification').val() +`</div>
						<div data-id="`+id+`" style="display:inline-block !important;padding: 0 10px !important;height:20px !important;border-radius:20px;margin-left:-2px" class="btn btn-danger del-qualification">x</div></div>`;
				$('.qualification-block').append(html);

				// Update the qualification
				var qualifications = $('#professional_details_tab input[name=qualification]').val();
				if(qualifications == ''){
					qualifications = $('#add_qualification').val();
				}else{
					qualifications += ","+$('#add_qualification').val();
				}
				$('#professional_details_tab input[name=qualification]').val(qualifications);

				$('#add_qualification').val('');
				$('#add_qualification').focus();
			}else{
				alert("Please enter qualification name");
				return false;
			}
		});

		$('.content').on('click','.del-qualification',function(){
			if(confirm('Do you really want to remove?') && $('#qualification_'+$(this).data('id')).length > 0){
				var txt = $('#qualification_'+$(this).data('id')+' div:eq(0)').html();
				// Update the qualifications
				var qualifications = $('#professional_details_tab input[name=qualification]').val().split(",");
				var str = '';
				if(qualifications.length > 0){
					qualifications.remove(txt);
					$('#professional_details_tab input[name=qualification]').val(qualifications.toString());
				}

				$('#qualification_'+$(this).data('id')).remove();
			}
		});

		$('.content').on('click','.add-college',function(){
			if($('#add_college').val() != ''){
				id = uniqueID(4);
				html = `<div id="college_`+id+`" style="display:inline-block; margin: 5px; margin-right:10px;"><div style="display:inline-block;background: #f1eab0;padding:5px 10px;">`+ $('#add_college').val() +`</div><div data-id="`+id+`" style="display:inline-block !important;padding: 0 10px !important;height:20px !important;border-radius:20px;margin-left:-2px" class="btn btn-danger del-college">x</div></div>`;
				$('.college-block').append(html);

				// Update the college
				var colleges = $('#professional_details_tab input[name=college_name]').val();
				if(colleges == ''){
					colleges = $('#add_college').val();
				}else{
					colleges += ","+$('#add_college').val();
				}
				$('#professional_details_tab input[name=college_name]').val(colleges);

				$('#add_college').val('');
				$('#add_college').focus();
			}else{
				alert("Please enter college name");
				return false;
			}
		});

		$('.content').on('click','.del-college',function(){
			if(confirm('Do you really want to remove?') && $('#college_'+$(this).data('id')).length > 0){
				var txt = $('#college_'+$(this).data('id')+' div:eq(0)').html();
				// Update the college
				var colleges = $('#professional_details_tab input[name=college_name]').val().split(",");
				var str = '';
				if(colleges.length > 0){
					colleges.remove(txt);
					$('#professional_details_tab input[name=college_name]').val(colleges.toString());
				}

				$('#college_'+$(this).data('id')).remove();
			}
		});

		var employmentArray = [];
		$('.content').on('click','.btnAddEmployment', function(){
			companyName = $('#company_name').val();
			fromPeriod = $('#company_from_period').val();
			toPeriod = $('#company_to_period').val();

			if(companyName && fromPeriod && toPeriod){
				id = uniqueID(5);
				employmentArray.push({
					id: id,
					company_name: companyName,
					from_period: fromPeriod,
					to_period: toPeriod,
				});

				html = `<tr data-id="`+ id +`">
					<td>`+ companyName +`</td>
					<td>`+ fromPeriod + ` - ` + toPeriod +`</td>
					<td><a class="btn btn-danger btn-xs btn-sm btnRemoveEmployment" data-id="`+ id +`"><i class="material-icons">delete</i></a></td>
				</tr>`;

				if($('#official_details_tab .employmentTable tbody tr:eq(0) td').length == 1){
					$('#official_details_tab .employmentTable tbody').html(html);
				}else{
					$('#official_details_tab .employmentTable tbody').append(html);
				}
			}
			else{
				alert("Company name and period cannot be blank!");
				return false;
			}
		});

		$('.content').on('click','.btnRemoveEmployment',function(){
			if(confirm('Do you really want to remove?')){
				if($(this).data('id') && employmentArray.length){
					for(var key in employmentArray){
						if(employmentArray.hasOwnProperty(key)){
							if(employmentArray[key]['id'] == $(this).data('id')){
								employmentArray.splice(key, 1);
								$('#official_details_tab .employmentTable tbody tr[data-id='+$(this).data('id')+']').remove();
							}
						}
					}
				}

				if(employmentArray.length == 0){
					$('#official_details_tab .employmentTable tbody').html('<tr><td colspan="3" class="text-center">No records</td></tr>')
				}
			}
		});

		$('.content').on('change','#copy_current_address', function(){
			if($(this).is(':checked')){
				$('#permanent_address').val($('#current_address').val());
				$('#permanent_area').val($('#current_area').val());
				$('#permanent_city').val($('#current_city').val());
				$('#permanent_zipcode').val($('#current_zipcode').val());
				$('#permanent_state').val($('#current_state').val());
				$('#permanent_country').val($('#current_country').val());
			}else{
				$('#permanent_address').val('');
				$('#permanent_area').val('');
				$('#permanent_city').val('');
				$('#permanent_zipcode').val('');
				$('#permanent_state').val('');
				$('#permanent_country').val('');
			}
		});

		function handleLocationError(browserHasGeolocation, infoWindow, pos) {
		}

		var componentForm1 = {
			sublocality_level_2: 'short_name',
			street_number: 'short_name',
			route: 'long_name',
			locality: 'long_name',
			administrative_area_level_1: 'long_name',
			country: 'long_name',
			postal_code: 'short_name'
		};

		var componentIds1 = {
			sublocality_level_2: 'area',
			locality: 'city',
			administrative_area_level_1: 'state',
			country: 'country',
			postal_code: 'zipcode'
		};
	</script>
	<script type="text/javascript">
	    window.addEventListener('keydown', function(e) {
	        if (e.keyIdentifier == 'U+000A' || e.keyIdentifier == 'Enter' || e.keyCode == 13) {
	            if (e.target.nodeName == 'INPUT' && e.target.type == 'text' && !$(e.path[1]).hasClass('bootstrap-tagsinput')) {
	                e.preventDefault();
	                return false;
	            }
	        }
	    }, true);
	</script>
	<script>
	var skillsArray = [];
	var saveMode = 'a';

	$(function(){
		@if(isset($e))
		$("input[name=email]").attr('readonly',true);
		saveMode = 'e';
		$('#gender').selectpicker('val', '{{ $e->gender }}');
		$('#bank_name').selectpicker('val', '{{ isset($e->account)?$e->account->bank_name:'' }}');
		$('#blood_group').selectpicker('val', '{{ $e->blood_group }}');
		$('#marital_status').selectpicker('val', '{{ $e->marital_status }}');
		$('#religion').selectpicker('val', '{{ $e->religion }}');

		@if(count($branches))
		$('#branch_id').selectpicker('val', '{{ isset($e->professional)?$e->professional->branch_id:'' }}');
		@else
		$('#branch_id').val('0');
		@endif
		$('#specialization').selectpicker('val', '{{ isset($e->professional)?$e->professional->specialization:0 }}');
		$('#experience_level').selectpicker('val', '{{ isset($e->professional)?$e->professional->experience_level:0 }}');
		$('#designation').selectpicker('val', '{{ isset($e->professional)?$e->professional->designation:0 }}');
		$('#department').selectpicker('val', '{{ isset($e->professional)?$e->professional->department:0 }}');
		$('#manager').selectpicker('val', '{{ isset($e->professional)?$e->professional->manager:0 }}');
		$('#source').selectpicker('val', '{{ isset($e->professional)?$e->professional->source:'' }}');

		@if(!empty($e->professional->languages_known))
		{? $languages = explode(",", $e->professional->languages_known); ?}
		@foreach($languages as $l)
		$('input[id=languages_{{ strtolower($l) }}]').attr('checked',true);
		@endforeach
		@endif
		// Builds Data inside the SkillModal
		{? foreach($caregiver_skills as $skill){ ?}
			$("input[name='skillcheck[]'][value='{{ $skill->skill_id }}']").attr('checked',true);
			$("input[name='skillcheckproficiency[]_{{ $skill->skill_id }}']").each( function () {
				$(this).prop('disabled', false);
				if($(this).val() == '{{ $skill->proficiency }}' ){
					$(this).prop('checked', true);
				}
			});
		{? } ?}
		// Builds Data in the Skill Form Table
		{? if(isset($caregiver_skills)){ ?}
		{? $html = ''; ?}
		{? foreach($caregiver_skills as $skill){ ?}
		{? 	  $html .= '<tr data-id="'.$skill->skill_id.'"><td data-skill="'.$skill->skill_id.'">'.$skill->skill->skill_name.'</td><td data-proficiency="'.$skill->proficiency.'">'.$skill->proficiency.'</td><td><a class="btn btn-danger btn-xs btnRemoveSkill" data-skill="'.$skill->skill_id.'"><i class="material-icons">delete</i></a></td></tr>'; ?}
		$('#skill_id option[value={{ $skill->skill_id }}]').attr('disabled',true);
		{? } ?}
		$('#skill_id').selectpicker('val','');
		$('#skill_id').selectpicker('refresh');
		$('#skills_table tbody').html('{!! $html !!}');
		{? } ?}

		$('input[name=role][value="{{ isset($e->professional)?$e->professional->role:'' }}"]').attr('checked',true);
		@if(isset($e->professional))
			if('{{ $e->professional->role == 'Manager' }}'){
				$('.deployable_check').css('display','block');
			}
			if('{{ $e->professional->deployable == 'Yes'}}'){
				$('input[name=deployable]').attr('checked',true);
			}
			if('{{ $e->professional->employment_type }}'){
				$('input[name=employment_type][value="{{ $e->professional->employment_type }}"]').attr('checked',true);
			}
		@endif
		$('input[name=food_habits][value="{{ isset($e->food_habits)?$e->food_habits:'' }}"]').attr('checked',true);
		$('input[name=arranged_by][value="{{ isset($e->accommodation)?$e->accommodation->arranged_by:'' }}"]').attr('checked',true);
		@endif

		$('.date').bootstrapMaterialDatePicker({
			format: 'DD-MM-YYYY',
			time: false,
			clearButton: true,
		});

		$('.datepast').bootstrapMaterialDatePicker({
			format: 'DD-MM-YYYY',
			time: false,
			clearButton: true,
			maxDate: moment().subtract(18, 'years'),
			// maxDate: new Date(),
		});

		$('input[name=date_of_joining]').bootstrapMaterialDatePicker({
			format: 'DD-MM-YYYY',
			time: false,
			clearButton: true,
			// minDate : new Date(new Date() - (1000*60*60*24*5)),
			maxDate: new Date(),
		});

		$('#accommodation_from_date').bootstrapMaterialDatePicker({
			format: 'DD-MM-YYYY',
			time: false,
			clearButton: true,
			weekStart: 1,
			maxDate: moment().add(7,'d'),
		}).on('change', function (e, date){
			$('#accommodation_to_date').bootstrapMaterialDatePicker({
				format: 'DD-MM-YYYY',
				time: false,
				clearButton: true,
				weekStart: 1,
				minDate: date,
			});
		});

		$('.time').bootstrapMaterialDatePicker({
			format: 'hh:mm A',
			time: true,
			date: false,
			shortTime: true,
			clearButton: true,
		});

		$('#agencySelect').change(function(){
			$('#agencyIdVerify').val($("#agencySelect option:selected").val());
		});

		$("#caregiverVerify").click(function(){
			var database_name = $('#database_name').val();
			if($('#agencyIdVerify').val() == "0"){
				alert("Please choose an Agency");
				return false;
			}
			if(confirm('Are you sure to send email for verification ?')){
				var caregiverId = $('#caregiverIdVerify').val();
				var agencyId = $('#agencyIdVerify').val();

				var form_data = {
					caregiver_Id:caregiverId,
					agency_Id:agencyId,
					database_name:database_name,
					_token: '{{csrf_token()}}'
				};

				$.ajax({
					url: '{{route('corporate.document-verify')}}',
					type: 'POST',
					data: form_data,
					success: function (state){
						if(state == "ImcMissing"){
							showNotification('bg-red', 'Please Upload IMC Document.', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
						}
						if(state == "EduMissing"){
							showNotification('bg-red', 'Please Upload Educational Document', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
						}
						if(state == "CurrAddMissing"){
							showNotification('bg-red', 'Please Upload Current Address Document', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
						}
						if(state == "PermAddMissing"){
							showNotification('bg-red', 'Please Upload Permanent Address Document', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
						}
						if(state == "GovtMissing"){
							showNotification('bg-red', 'Please Upload Government ID Document', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
						}
						if(state == "Applied"){
							showNotification('bg-green', 'Successfully Applied for Verification', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
						}
						if(state == "OOPS"){
							showNotification('bg-green', 'Oops !! Something Went Wrong', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
						}
					},
					error: function(e){
						console.log(e);
					}
				});
			}
		});

		$('.content').on('click','.btnAddSkill', function(){
			$('#skills_table tbody').html('');
			$("input[name='skillcheck[]']").each( function () {
				if($(this).is(":checked")){
					skill_id = $(this).val();
					skill = $('label[for="basic_checkbox_'+$(this).val()+'"]').text();
					proficiency = $("input[name='skillcheckproficiency[]_"+$(this).val()+"']:checked()").val();

					html = '<tr data-id="'+skill_id+'"><td data-skill="'+skill_id+'">'+skill+'</td><td data-proficiency="'+proficiency+'">'+proficiency+'</td><td><a class="btn btn-danger btn-xs btnRemoveSkill" data-skill="'+skill_id+'"><i class="material-icons">delete</i></a></td></tr>';

					$('#skill_id option[value='+skill_id+']').attr('disabled',true);
					$('#skill_id').selectpicker('val','');
					$('#skill_id').selectpicker('refresh');

					if($('#skills_table tbody tr#default').length == 1){
						$('#skills_table tbody').html(html);
					}else
					$('#skills_table tbody').append(html);
				}
			});
		});

		$('.content').on('click','.btnRemoveSkill', function(){
			skill = $(this).data('skill');
			$('#skills_table tbody').find('tr[data-id='+skill+']').remove();

			if($('#skills_table tbody tr').length == 0){
				$('#skills_table tbody').html('<tr id="default"><td colspan="3" class="text-center">No skill(s) added</td></tr>');
			}
			$("input[name='skillcheck[]'][value='"+skill+"']").prop('checked', false);
			$("input[name='skillcheckproficiency[]_"+skill+"']").each( function () {
				$(this).prop('checked', false);
				$(this).prop('disabled', true);
			});
		});

		$('.content').on('click','.btnDocumentUpload', function(e){
			e.preventDefault();

			var database_name = $('#database_name').val();
			var file_select = $('#doc_upload')[0];
			files = file_select.files;

			if(files.length>0){
				e.stopPropagation(); // Stop stuff happening
				e.preventDefault(); // Totally stop stuff happening

				// Create a formdata object and add the files
				var formData = new FormData();
				formData.append("database_name",database_name);
				formData.append('csrf_token','{{  csrf_token() }}');
				formData.append("documents",files[0]);
				formData.append('type',saveMode);
				formData.append('caregiver_id','{{ isset($e)?$e->id:null }}');
				formData.append("verification_documents_comment",$('#verification_documents_comment').val());
				formData.append("verification_documents",$('#verification_documents option:selected').val());

				$('.page-loader-wrapper').show();
				$.ajax({
					url: '{{ route('corporate.update-documents') }}',
					type: 'POST',
					data: formData,
					cache: false,
				   processData: false, // Don't process the files
				   contentType: false, // Set content type to false as jQuery will tell the server its a query string request
				   success: function(data, textStatus, jqXHR){
console.log(data.message);
				   	$('.page-loader-wrapper').hide();
				   	$('html, body').animate({ scrollTop: 0 }, 0);
				   	$('#upload-message').html('<div class="alert alert-warning"><ul class="upload-message">'+data.message+'</ul></div>');

				   	if(data.duplicate != true){
				   		html = 		`<tr>
				   		<td>`+$('select[name=verification_documents] option:selected').text()+`</td>
				   		<td>`+$('#verification_documents option:selected').val()+`</td>
				   		<td>Uploaded</td>
				   		</tr>`;

				   		$('.to-be-uploaded-documents tbody').append(html);


						// Clear Selection
						$('#verification_documents_comment').val('');
						$('#doc_upload').val('');

						if(saveMode == 'a'){
							if(data.doc_id != 'undefined'){
								doc_ids = $('#uploaded_doc_ids').val();
								if(doc_ids == '')
									doc_ids = data.doc_id;
								else
									doc_ids += ','+data.doc_id;

								$('#uploaded_doc_ids').val(doc_ids);
							}
						}
					}
					/*if(data.duplicate != true && saveMode == 'e'){
						location.reload();
					}*/
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					$('.page-loader-wrapper').hide();
					   // Handle errors here
					   console.log('ERRORS: ' + textStatus);
					}
				});
			}
		});

		$(".remove").click(function(){
			var database_name = $('#database_name').val();
			if(confirm('Are you sure?')){
				var docId       = $(this).attr('data-docId');
				var docName     = $(this).attr('data-docName');
				var docFolder   = $(this).attr('data-docFolder');
				var caregiverId = $(this).attr('data-caregiverId');

				var form_data = {
					database_name: database_name,
					doc_Id       : docId,
					doc_Name     : docName,
					doc_Folder   : docFolder,
					caregiver_Id : caregiverId,
					_token       : '{{csrf_token()}}'
				};
				console.log(form_data);
				$.ajax({
					url: '{{route('corporate.document-remove')}}',
					type: 'POST',
					data: form_data,
					success: function(d){
						console.log(d);
								  setTimeout(function(){// wait for 0 secs
											 location.reload(); // then reload the page.
											}, 500);
								},
								error: function(e){
									console.log(e);
								}
							});
			}
		});
	});


    function handleFileSelect(evt) {
		var files = evt.target.files; // FileList object

		// Loop through the FileList and render image files as thumbnails.
		for (var i = 0, f; f = files[i]; i++) {

			// Only process image files.
			if (!f.type.match('image.*')) {
				continue;
			}

			var reader = new FileReader();

			// Closure to capture the file information.
			reader.onload = (function(theFile) {
				return function(e) {
					// Render thumbnail.
					var span = document.createElement('span');
					span.innerHTML = ['<img style="width:160px;height: 160px;" class="img-responsive img-thumbnail" src="', e.target.result,
					'" title="', escape(theFile.name), '"/>'].join('');
					$('.profile-img-block').html(span);
				};
			})(f);

			// Read in the image file as a data URL.
			reader.readAsDataURL(f);
		}
	}

	document.getElementById('profile_picture').addEventListener('change', handleFileSelect, false);

	function validator() {
		var eid = $('#employee_id').val();
		var fname = $('#first_name').val();
		var gender = $('#gender option:selected').val();

		var currAddress = $('#current_address').val();
		var currCity = $('#current_city').val();
		var currState = $('#current_state').val();

		var permAddress = $('#permanent_address').val();
		var permCity = $('#permanent_city').val();
		var permState = $('#permanent_state').val();

		var caregiverNumber = $('#mobile_number').val();
		var alternateNumber = $('#alternate_number').val();
		var caregiverEmail = $('#email').val();
		var caregiverPassword = $('#password').val();
		var caregiverAccessRole = $('#access_role option:selected').val();

		var specialization = $('#specialization option:selected').val();
		var qualification = $('#qualification').val();
		var experience_level = $('#experience_level option:selected').val();
		var experience = $('#experience').val();

		var languages = $('#lang input[type=checkbox]:checked').length;
		var role = $('input[name=role]:checked').length;

		if (eid == ""){
			$('[href="#' + $('#employee_id').closest(".tab-pane").attr('id') + '"]').tab('show');
			setTimeout(function() { 
				alert("Please Enter Employee id");
				document.getElementById("employee_id").focus();
			}, 1000);

			return false;
		}
		if (fname == ""){
			$('[href="#' + $('#first_name').closest(".tab-pane").attr('id') + '"]').tab('show');
			setTimeout(function() { 
				alert("Please Enter First Name");
				document.getElementById("first_name").focus();
			}, 1000);
			

			return false;
		}
		if (gender == ""){
			$('[href="#' + $('#gender').closest(".tab-pane").attr('id') + '"]').tab('show');
			setTimeout(function() { 
				alert("Please Enter Gender");
				document.getElementById("gender").focus();
			}, 1000);
			

			return false;
		}
		if (currAddress == ""){
			$('[href="#' + $('#current_address').closest(".tab-pane").attr('id') + '"]').tab('show');
			setTimeout(function() { 
				alert("Please Enter Current Address");
				document.getElementById("current_address").focus();
			}, 1000);
			

			return false;
		}
		if (currCity == ""){
			$('[href="#' + $('#current_city').closest(".tab-pane").attr('id') + '"]').tab('show');
			setTimeout(function() { 
				alert("Please Enter Current City");
				$("label[for='"+$('#city').attr('id')+"']").addClass('required');
			}, 1000);
			
			document.getElementById("current_city").focus();

			return false;
		}
		if (currState == ""){
			$('[href="#' + $('#current_state').closest(".tab-pane").attr('id') + '"]').tab('show');
			setTimeout(function() { 
				alert("Please Enter Current State");
				document.getElementById("current_state").focus();
			}, 1000);
			

			return false;
		}
		if (permAddress == ""){
			$('[href="#' + $('#permanent_address').closest(".tab-pane").attr('id') + '"]').tab('show');
			setTimeout(function() { 
				alert("Please Enter Permanent Address");
				document.getElementById("permanent_address").focus();
			}, 1000);
			

			return false;
		}
		if (permCity == ""){
			$('[href="#' + $('#permanent_city').closest(".tab-pane").attr('id') + '"]').tab('show');
			setTimeout(function() { 
				alert("Please Enter Permanent City");
				document.getElementById("permanent_city").focus();
			}, 1000);
			

			return false;
		}
		if (permState == ""){
			$('[href="#' + $('#permanent_state').closest(".tab-pane").attr('id') + '"]').tab('show');
			setTimeout(function() { 
				alert("Please Enter Permanent State");
				document.getElementById("permanent_state").focus();
			}, 1000);
			

			return false;
		}
		if (caregiverNumber == ""){
		    $('[href="#' + $('#mobile_number').closest(".tab-pane").attr('id') + '"]').tab('show');
			alert("Please Enter Caregiver Number");
			document.getElementById("mobile_number").focus();

			return false;
		}
		if (caregiverNumber != "" && ((!/^(\+91)(\d{10})$/.test(caregiverNumber)) && (!/^(\d{10})$/.test(caregiverNumber)) && (!/^((0)[0-9]{10})$/.test(caregiverNumber)))){
		    $('[href="#' + $('#mobile_number').closest(".tab-pane").attr('id') + '"]').tab('show');
			setTimeout(function() { 
				alert("Please Enter Caregiver Number in the required format: +919876543210 or 9876543210 or 09876543210");
				document.getElementById("mobile_number").focus();
			}, 1000);

			return false;
		}
		if (alternateNumber != "" && ((!/^(\+91)(\d{10})$/.test(alternateNumber)) && (!/^(\d{10})$/.test(alternateNumber)) && (!/^((0)[0-9]{10})$/.test(alternateNumber)))){
		    $('[href="#' + $('#alternate_number').closest(".tab-pane").attr('id') + '"]').tab('show');
			setTimeout(function() { 
				alert("Please Enter Alternate Number in the required format: +919876543210 or 9876543210 or 09876543210");
				document.getElementById("alternate_number").focus();
			}, 1000);

			return false;
		}
		if (caregiverEmail == ""){
			$('[href="#' + $('#email').closest(".tab-pane").attr('id') + '"]').tab('show');
			setTimeout(function() { 
				alert("Please Enter Email of caregiver account");
				document.getElementById("email").focus();
			}, 1000);

			return false;
		}
		@if(!isset($e))
		if (caregiverPassword == ""){
			    $('[href="#' + $('#password').closest(".tab-pane").attr('id') + '"]').tab('show');
				setTimeout(function() { 
					alert("Please Enter Password of caregiver account");
					document.getElementById("password").focus();
				}, 1000);

			return false;
		}
		if (caregiverPassword.length < 8){
			    $('[href="#' + $('#password').closest(".tab-pane").attr('id') + '"]').tab('show');
				setTimeout(function() { 
					alert("Please Enter Password of minimum 8 characters length");
					document.getElementById("password").focus();
				}, 1000);

			return false;
		}
		if (caregiverAccessRole == ""){
			    $('[href="#' + $('#access_role').closest(".tab-pane").attr('id') + '"]').tab('show');
				setTimeout(function() { 
					alert("Please Select an Access Role for the employee");
					document.getElementById("access_role").focus();
				}, 1000);

			return false;	
		}
		@endif
		if (specialization == ""){
			    $('[href="#' + $('#specialization').closest(".tab-pane").attr('id') + '"]').tab('show');
				setTimeout(function() { 
					alert("Please Enter Specialization");
					document.getElementById("specialization").focus();
				}, 1000);

			return false;
		}
		if (qualification == ""){
			    $('[href="#' + $('#qualification').closest(".tab-pane").attr('id') + '"]').tab('show');
				setTimeout(function() { 
					alert("Please Enter Qualification");
					document.getElementById("qualification").focus();
				}, 1000);

			return false;
		}
	    if (experience == ""){
	    	    $('[href="#' + $('#experience').closest(".tab-pane").attr('id') + '"]').tab('show');
	    	    setTimeout(function() { 
	    	    	alert("Please Enter the Experience");
	    	    	document.getElementById("experience").focus();
	    	    }, 1000);

	    	return false;
	    }
	    if (languages == 0){
	    	    $('[href="#professional_details_tab"]').tab('show');
	    		setTimeout(function() { 
	    			alert("Please Enter Languages Known by the Caregiver");
	    		}, 1000);

	    	return false;
	    }
	    if (role == 0){
	    	    $('[href="#' + $('#role').closest(".tab-pane").attr('id') + '"]').tab('show');
	    		setTimeout(function() { 
	    			alert("Please Enter Role of the Caregiver");
	    		}, 1000);
	    	
	    	return false;
	    }

	    checkSkills();

		checkPreviousEmployments();
	}

	function checkSkills(){
		if($('#skills_table tbody tr').length > 0){
			$('#skills_table tbody tr').each(function(){
				skill = $(this).find('td:eq(0)').data('skill');
				proficiency = $(this).find('td:eq(1)').data('proficiency');

				skillsArray.push({skill_id: skill, proficiency: proficiency});
			});

			$('#skills').val(JSON.stringify(skillsArray));
			console.log(JSON.stringify(skillsArray));
		}

		return true;
	}

	employmentsArray = [];
	function checkPreviousEmployments(){
		if($('#official_details_tab .employmentTable tbody tr:eq(0) td').length > 1 && $('#official_details_tab .employmentTable tbody tr').length > 0){
			$('#official_details_tab .employmentTable tbody tr').each(function(){
				companyName = $(this).find('td:eq(0)').text();
				period = $(this).find('td:eq(1)').text();

				employmentsArray.push({company_name: companyName, period: period});

				$('#employments').val(JSON.stringify(employmentsArray));
				console.log(JSON.stringify(employmentsArray));
			});
		}

		return true;
	}

	@if(!isset($e))
	function checkemail(){
		var email=document.getElementById("email").value;
		if(email){
			$.ajax({
				type: 'post',
				url: '{{ route('corporate.employee.checkemail') }}',
				data: { user_email:email, _token: '{{ csrf_token() }}'},
				success: function (response){
					$( '#email_status' ).html(response);
					if(response=="OK"){
						$('#email_status').css('color','green');
						return true;
					}else if(response == 'Enter a valid email id.'){
						$('#email_status').css('color','red');
						return false;
					}else if(response == 'Email Already Exist. Please Choose a unique email id.'){
						$('#email_status').css('color','red');
						  setTimeout(function(){
									 $('#email').val('');
									 $('#email_status').html('');
									 alert('Email Already Exist. Please Choose a unique email id.');
									}, 2000);
						return false;
					}
				}
			});
		}else{
			$( '#email_status' ).html("");
			return false;
		}
	}
	@endif

	@if(isset($e))
	function checkEmpID(){
		var empID = document.getElementById("employee_id").value;
		var curr_emp_id = '{{$e->employee_id}}';
		var db = '{{ $database_name }}';
		if(empID){
			$.ajax({
				type: 'post',
				url: '{{ route('corporate.employee.checkEmpID') }}',
				data: { currEmpId:curr_emp_id, employee_id:empID, database_name:db, _token: '{{ csrf_token() }}'},
				success: function (response){
					$( '#emp_id_status' ).html(response);
					if(response=="Updated Successfully"){
						$('#emp_id_status').css('color','green');
						return true;
					}else if(response == 'Employee Id already Taken. Please Choose a unique id.'){
						$('#emp_id_status').css('color','red');
						return false;
					}
				}
			});
		}else{
			$( '#employee_id' ).val('{{$e->employee_id}}');
			$( '#emp_id_status' ).html('');
			return false;
		}
	}
	@endif

	$("input[name='skillcheck[]']").change( function () {
		if(!$(this).is(":checked")){
			$("input[name='skillcheckproficiency[]_"+$(this).val()+"']").each( function () {
				$(this).prop('checked', false);
				$(this).prop('disabled', true);
			});
		}else{
			$("input[name='skillcheckproficiency[]_"+$(this).val()+"']").each( function () {
				$(this).prop('disabled', false);
				if($(this).val() == 'Excellent'){
					$(this).prop('checked', true);
				}
			});
		}
	});

</script>
@endsection
