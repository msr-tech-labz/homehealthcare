@extends('layouts.corporate-layout')

@section('page_title','Employees - ')
@section('active_caregivers','active')
@section('plugin.styles')
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />
<style>
    .table-bordered tbody tr td, .table-bordered tbody tr th {
        white-space: nowrap;
        font-size: 13px;
    }
</style>
@endsection
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('corporate.team.index') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <div class="row">
                    <h2 class="col-sm-7">
                        Employees
                        <small>List of all employees</small>
                    </h2>
                    <div class="col-sm-4">
                        <div class="pull-right">
                        {{--@ability('admin','employees-create')--}}
                            <select class="select-tick" data-live-search="true" id="branchForCreateEmp">
                                <option value="">-- Select Branch ID --</option>
                                @foreach($subscribers as $subscriber)
                                <option value="{{ \Helper::encryptor('encrypt',$subscriber->database_name) }}">{{ $subscriber->org_code }}</option>
                                @endforeach
                            </select>
                            <a id="createEmpLink"><button type="button" class="btn btn-success waves-effect" title="Select the branch first" disabled>New Employee</button></a>
                        {{--@endability--}}
                        </div>
                    </div>
                </div>
                <br>
                <form action="{{ route('corporate.employee') }}" method="post">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-offset-4 col-md-2">
                            <select name="branch_filter" class="form-control" id="branch_filter">
                                <option {{ (isset($filterBranch) && $filterBranch == "All")?'selected':'selected' }} value="All">All Branch</option>
                                @foreach($subscribers as $subscriber)
                                <option {{ (isset($filterBranch) && $filterBranch == \Helper::encryptor('encrypt',$subscriber->database_name))?'selected':'' }} value="{{ \Helper::encryptor('encrypt',$subscriber->database_name) }}">{{ $subscriber->org_code }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2">
                                <button type="submit" class="btn btn-primary waves-effect">FILTER</button>
                        </div>
                    </div>
                </form>
            </div>
            {{--@ability('admin','employees-list')--}}
            <div class="body">
                <table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable" style="zoom:90%;">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Field Staff</th>
                            <th>Type</th>
                            <th>Branch</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Contact</th>
                            <th>Specialization</th>
                            <th>Exp</th>
                            <th>Work Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($caregivers as $caregiver)
                        @foreach($caregiver as $e)
                        <tr>
                            <td>{{ isset($e->employee_id)?$e->employee_id:'' }}</td>
                            @if(isset($e->professional->role) && $e->professional->role == "Manager")
                            <td style="color:red; font-weight:bold;">No</td>
                            @else
                            <td>Yes</td>
                            @endif
                            @if(isset($e->professional->employment_type) && $e->professional->employment_type == "Freelancer")
                            <td class="col-pink">{{ $e->professional->employment_type }}</td>
                            @else
                            <td>{{ $e->professional->employment_type }}</td>
                            @endif
                            <td>{{ $e->org_code }}</td>
                            <td>
                                <img src="@if($e->profile_image && file_exists(public_path('uploads/provider/'.\Helper::encryptor('encrypt',$e->tenant_id).'/caregivers/'.\Helper::encryptor('encrypt',$e->id)).'/'.$e->profile_image)) {{ asset('uploads/provider/'.\Helper::encryptor('encrypt',$e->tenant_id).'/caregivers/'.\Helper::encryptor('encrypt',$e->id).'/'.$e->profile_image)}} @else {{ asset('img/default-avatar.jpg') }} @endif" width="40" height="40" class="img-circle" alt="{{ $e->first_name }}" class="img-responsive img-thumbnail" title="{{ $e->first_name }}" /> 
                            </td>
                            <td>{!! $e->first_name !!}<br>{!! $e->last_name !!}</td>
                            <td>{!! $e->mobile_number.'<br><small>'.$e->email.'</small>' !!}</td>
                            <td>{{ isset($e->professional->specializations)?$e->professional->specializations->specialization_name:'-' }}</td>
                            <td>{{ (isset($e->professional) && !empty($e->professional->experience) && is_numeric($e->professional->experience))?$e->professional->experience.' yr(s)':'-' }}</td>
                            <td width="10%">
                                {{--@ability('admin','employees-edit')--}}
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info waves-effect workstatus" data-id="{{ Helper::encryptor('encrypt',$e->id) }}">{{ $e->work_status ?? 'Change Status' }}</button>
                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="javascript:void(0);" data-status="Available" data-id="{{ Helper::encryptor('encrypt',$e->id) }}" data-branch-db="{{ $e->branch_db }}" class="work-status waves-effect waves-block">Available</a></li>
                                        <li><a href="javascript:void(0);" data-status="Training" data-id="{{ Helper::encryptor('encrypt',$e->id) }}" data-branch-db="{{ $e->branch_db }}" class="work-status waves-effect waves-block">Training</a></li>
                                    </ul>
                                </div>
                                {{--@else
                                    {{ $e->work_status ?? 'Change Status' }}
                                @endability--}}
                            </td>
                            <td>
                                {{--@ability('admin','employees-edit')--}}
                                <a href="{{ route('corporate.editEmployee',[Helper::encryptor('encrypt',$e->id),$e->branch_db]) }}" class="btn btn-warning btn-sm"><i class="material-icons">edit</i></a>
                                {{--@endability--}}
                            </td>
                        </tr>                        
                        @endforeach
                        @empty
                        <tr>
                            <td colspan="20" class="text-center">No Employee(s) found. <a href="{{ route('employee.create') }}" class="btn btn-info btn-sm">Create Employee</a></td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            {{--@endability--}}
        </div>
    </div>
</div>
@endsection

@section('page.scripts')
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
    <!-- Bootstrap Notify Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-notify/bootstrap-notify.js') }}"></script>
    <!-- Select Plugin Js -->
    <script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.dataTable').DataTable({
                processing: true,
                lengthMenu: [[50,100,200,-1],[50,100,200,"All"]],
                order: [[1,"asc"]],  
                language: {
                    processing: "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate' style='color:#ff0000'> Loading....</span>"
                },
            });
        });
        $(function(){
            $('#branchForCreateEmp').on('change', function(){
                var branch = $(this).val();
                if(branch==''){
                    $('#createEmpLink').attr('href','#');
                    $('#createEmpLink button').prop('disabled',true);
                }else{
                    $('#createEmpLink').attr('href','{{ route('corporate.createEmployee') }}/'+branch);
                    $('#createEmpLink button').prop('disabled',false);
                }
            });

            $('.content').on('click','.work-status',function(){
                if(confirm("Are your sure ?")){
                    var id = $(this).data('id');
                    var branchDb = $(this).data('branch-db');
                    var status = $(this).data('status');

                    // Make AJAX Call
                    $.ajax({
                        url: '{{ route('corporate.employee.update-status') }}',
                        type: 'POST',
                        data: {_token:'{{ csrf_token() }}', id:id, branchDb:branchDb, status:status},
                        success: function (data){
                            // Set the current status to button
                            $('button.workstatus[data-id="'+id+'"]').text(status);

                            showNotification('bg-green', 'Status changed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                        },
                        error: function (error){
                            console.log(error);
                        }
                    });
                }
                return true;
            });

            $('.content').on('click','.work-status-availability',function(){
                if(confirm("Are your sure ?")){
                    var id = $(this).data('id');
                    var status = $(this).data('status-availibility');

                    // Make AJAX Call
                    $.ajax({
                        url: '{{ route('employee.update-availability') }}',
                        type: 'POST',
                        data: {_token:'{{ csrf_token() }}', id: id, status: status},
                        success: function (data){
                            // Set the current status to button
                            $('button.workstatusavailability[data-id="'+id+'"]').text(status);

                            showNotification('bg-green', 'Availability changed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                        },
                        error: function (error){
                            console.log(error);
                        }
                    });
                }
                return true;
            });
        });
    </script>
@endsection
