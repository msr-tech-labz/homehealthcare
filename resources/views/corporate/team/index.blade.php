@extends('layouts.corporate-layout')

@section('page_title','HR - Dashboard | ')

@section('plugin.styles')
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
    <link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
    <style>
        .plus:after{
            content: ' + ';
            font-size: 18px;
            font-weight: bold;
        }
        .wizard .content{
            min-height: auto !important;
        }
        .wizard > .steps a{
            text-align: left !important;
            border: 1px solid #ccc;
        }
        .nav-justified li a{
            font-size: 16px;
        }
        .nav-justified li:hover{
            background: #eee !important;
        }
        .info-box .content .text{
            font-size: 13px !important;
            margin-top: 6px !important;
            margin-bottom: 6px !important;
            text-decoration: none !important;
        }
        .info-box .content .number {
            font-size: 18px !important;
        }
        .info-box:hover, .info-box:focus{
            font-size: 19px !important;
            text-decoration: none !important;
        }
    </style>
@endsection

@section('content')
<div class="row clearfix">
    <div class="col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('corporate.dashboard') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-6">
                    Corporate Team
                    <small>Manage your Corporate Users & Employees</small>
                </h2>
            </div>
        </div>
    </div>
    {{--<div class="row clearfix">
        <div class="col-lg-4 col-lg-offset-2 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-pink hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">people</i>
                </div>
                <div class="content">
                    <div class="text">Employees</div>
                    <div class="number count-to">{{ $stats['emp'] ?? 0 }}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-cyan hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">receipt</i>
                </div>
                <div class="content">
                    <div class="text">Leave Approval</div>
                    <div class="number count-to">{{ $stats['leave'] ?? 0 }} <small>Pending</small></div>
                </div>
            </div>
        </div>
    </div>--}}

    
    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
        <div class="card">
            <div class="body">
                <div>
                    <a href="{{ route('corporate.users.view') }}">
                        <i class="material-icons col-pink" style="font-size: 50px;">list</i>
                        <div style="font-size: 20px;text-align: right;margin-top: -50px;">Corporate Users</div>
                        <small style="float: right;" class="col-pink">Corporate User master record</small><br>
                    </a>
                </div>
            </div>
        </div>
    </div>

    {{--@ability('admin','employees-list')--}}
        <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
            <div class="card">
                <div class="body">
                    <div>
                        <a href="{{ route('corporate.employee') }}">
                            <i class="material-icons col-cyan" style="font-size: 50px;">people</i>
                            <div style="font-size: 20px;text-align: right;margin-top: -50px;">Employees</div>
                            <small style="float: right;" class="col-cyan">Employee master record</small><br>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    {{--@endability--}}
</div>
@endsection


@section('page.scripts')
    <!-- Select Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
    <script>

    </script>
@endsection
