@extends('layouts.corporate-layout')

@section('page_title','Leads - ')

@section('page.styles')
<style>
.plus:after{
    content: ' + ';
    font-size: 18px;
    font-weight: bold;
}
</style>
@endsection

@section('plugin.styles')
<style>
.tab-nav-right > li > a{
    min-width: 180px !important;
    text-align: center;
}
.nav-tabs > li{
    margin-bottom:2px !important;
}
.nav-tabs > li.active{
    border-left: 1px solid #aabcfe;
    border-right: 1px solid #aabcfe;
    border-top: 4px solid #aabcfe;
    margin-bottom: 2px;
}
.nav-tabs > li.active > a{
    font-weight: bold;
    color: #2196F3 !important;
}
.nav-tabs > li > a:before{
    border-bottom: 0 !important;
}
.allocations:hover{
    cursor: pointer;
}

.followups:hover{
    cursor: pointer;
}

.serviceExpiry:hover{
    cursor: pointer;    
}

.distributionsTable, .serviceExpiryTable {
    margin-bottom: 0 !important;
}

.modal-body .table tr th, .modal-body .table  tr td {
    font-size: 12px !important;
    padding: 7px !important;
}

.filter{
    display: inline-block;
}
.filter-div > label{
    margin-right: 10px !important;
}
.bgagglead{
    background-color: lightblue !important;
}
.table-bordered tbody tr td, .table-bordered tbody tr th {
    white-space: nowrap;
    font-size: 13px;
}
.info-box .content {margin:1px 5px !important;}
.info-box .content .text {margin-top:5px !important;}
.info-box .content .number {font-size:18px !important;}
</style>
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
@endsection

@section('side-menu')
    @include('menus.leads')
@endsection

@section('content')
<div class="row clearfix" style="margin-bottom:30px;">
    <div class="col-lg-offset-2 col-lg-4 tile">
        <div class="info-box bg-red hover-expand-effect">
            <div class="icon">
                <i class="material-icons">people</i>
            </div>
            <div class="content">
                <div class="text">Pending</div>
                <div class="number count-to">{{ $pendingLeads }}</div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 tile">
        <div class="info-box bg-pink hover-expand-effect followups" data-toggle="modal" data-target="#followupsModal">
            <div class="icon">
                <i class="material-icons">settings_phone</i>
            </div>
            <div class="content">
                <div class="text">Follow Up</div>
                <div class="number count-to">{{ $followupLeads }}</div>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('corporate.dashboard') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <div class="row">
                    <h2 class="col-sm-7">
                        Leads Dashboard
                        <small>List of all leads</small>
                    </h2>
                    <div class="col-md-4">
                        {{--@ability('admin','leads-create')--}}
                        <div class="pull-right">
                            <select class="select-tick" data-live-search="true" id="branchForCreateLead">
                                <option value="">-- Select Branch ID --</option>
                                @foreach($subscribers as $subscriber)
                                <option value="{{ \Helper::encryptor('encrypt',$subscriber->database_name) }}">{{ $subscriber->org_code }}</option>
                                @endforeach
                            </select>
                            <a id="createLeadLink"><button type="button" class="btn btn-success waves-effect" title="Select the branch first" disabled>Create Lead</button></a>
                        </div>
                        {{--@endability--}}
                    </div>
                </div>
                {{--@ability('admin','leads-index')--}}
            </div>
            <div class="body">
                <form action="{{ route('corporate.lead.index') }}" method="post">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-offset-1 col-md-2">
                            <select name="branch_filter" class="form-control" id="branch_filter">
                                <option {{ (isset($filterBranch) && $filterBranch == "All")?'selected':'selected' }} value="All">All Branch</option>
                                @foreach($subscribers as $subscriber)
                                <option {{ (isset($filterBranch) && $filterBranch == \Helper::encryptor('encrypt',$subscriber->database_name))?'selected':'' }} value="{{ \Helper::encryptor('encrypt',$subscriber->database_name) }}">{{ $subscriber->org_code }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select name="status_filter" class="form-control" id="status_filter">
                                <option {{ (isset($filterStatus) && $filterStatus == "All")?'selected':'selected' }} value="All">All Statuses</option>
                                <option {{ (isset($filterStatus) && $filterStatus == "Pending")?'selected':'' }} value="Pending">Pending</option>
                                <option {{ (isset($filterStatus) && $filterStatus == "Follow Up")?'selected':'' }} value="Follow Up">Follow Up</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                                <input type="text" id="from_date" name="from_date" class="form-control datepicker" style="background-color: #fff !important;border-bottom: 1px solid #ddd !important;border: transparent;border-radius: none;margin-top: -1px;" placeholder="From Date" value="{{ isset($filterFromDate)?\Carbon\Carbon::parse($filterFromDate)->format('d-m-Y'):date('01-m-Y') }}" />
                        </div>
                        <div class="col-sm-2">
                                <input type="text" id="to_date" name="to_date" class="form-control datepicker" style="background-color: #fff !important;border-bottom: 1px solid #ddd !important;border: transparent;border-radius: none;margin-top: -1px;" placeholder="To Date" value="{{ isset($filterToDate)?\Carbon\Carbon::parse($filterToDate)->format('d-m-Y'):date('t-m-Y') }}" />
                        </div>
                        <div class="col-md-2">
                                <button type="submit" class="btn btn-primary waves-effect">FILTER</button>
                        </div>
                    </div>
                </form>
                <table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable" style="zoom:90%;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Status</th>
                            <th>Branch</th>
                            <th>Date</th>
                            <th>Source</th>
                            <th>Episode Id</th>
                            <th>Patient</th>
                            <th>Enquirer</th>
                            <th>Phone</th>
                            <th>Location</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i = 0; @endphp
                    @foreach ($tableLeads as $tableLead)
                        @foreach ($tableLead as $lead)
                        <tr>
                            <td> {{ ++$i }} </td>
                            <td> {{ $lead->status }} </td>
                            <td>{{ $lead->org_code }}</td>
                            <td> {{ $lead->created_at->format('d-m-y') }} </td>
                            <td> {{ !empty($lead->referralSource)?$lead->referralSource->source_name:'-' }} </td>
                            <td> {{ !empty($lead->episode_id)?$lead->episode_id:'TEMPLD0AF'.$lead->id }} </td>
                            <td> {{ !empty($lead->patient->full_name)?$lead->patient->full_name:'-' }} </td>
                            <td> {{ !empty($lead->patient->enquirer_name)?$lead->patient->enquirer_name:'-' }} </td>
                            <td> {{ isset($lead->patient->contact_number)?$lead->patient->contact_number:'' }} </td>
                            <td> {{ (isset($lead->patient) && !empty($lead->patient->city))?$lead->patient->city:'-' }} </td>
                            <?php if($lead->aggregator_lead && $lead->aggregator_lead_status == 'Pending'){
                                if(\Entrust::hasRole('admin') || \Entrust::can('leads-view')){
                                    $patientData = json_encode(collect($lead->patient)->only('first_name','last_name','patient_age','patient_weight','gender','street_address','area','city'));
                                    $leadData = json_encode(collect($lead)->only('case_description','medical_conditions','medications','procedures','aggregator_service','language_preference','gender_preference','aggregator_rate','aggregator_rate_negotiable'));
                                    $html = '<a href="javascript:void(0);" target="_blank" class="btn btn-md bg-primary btnViewLead" data-id="'. \Helper::encryptor('encrypt',$lead->id).'" data-patient-id="'.\Helper::encryptor('encrypt',$lead->patient_id).'" data-patient="'.str_replace("\"", "'", $patientData).'" data-case="'.str_replace("\"", "'", $leadData).'" title="View Lead Details">View</a>';
                                }
                            }elseif($lead->aggregator_lead && $lead->aggregator_lead_status == 'Accepted'){
                                if(\Entrust::hasRole('admin') || \Entrust::can('leads-view')){
                                    $html = '<a href="'.route('lead.view',\Helper::encryptor('encrypt',$lead->id)).'" class="btn btn-md bg-primary" data-id="'.\Helper::encryptor('encrypt',$lead->id).'" title="Create Lead">View</a>';
                                }
                            }elseif(!$lead->aggregator_lead && $lead->aggregator_lead_status == 'Pending'){
                                if(\Entrust::hasRole('admin') || \Entrust::can('leads-view')){
                                    $html = '<a href="'.route('lead.view',\Helper::encryptor('encrypt',$lead->id)).'" class="btn btn-md bg-primary" data-id="'.\Helper::encryptor('encrypt',$lead->id).'" title="View Lead">View</a>';
                                }
                            } 
                            ?>
                            <td><a href="{{ route('corporate.lead.view',[\Helper::encryptor('encrypt',$lead->id),$lead->branch_db]) }}" class="btn btn-md bg-primary" data-id="{{ \Helper::encryptor('encrypt',$lead->id) }}" title="View Lead">View</a></td>
                        </tr>
                        @endforeach
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

    <div class="modal fade" id="viewAggregatorLeadModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content" >
                <div class="modal-header bg-blue">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="largeModalLabel">Lead Details</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <tr>
                                <th>Patient Name</th>
                                <td class="aggLead_patient"></td>
                            </tr>
                            <tr>
                                <th>Age, Gender &amp; Weight</th>
                                <td class="aggLead_gender_age_weight"></td>
                            </tr>
                            <tr>
                                <th>Location</th>
                                <td class="aggLead_location"></td>
                            </tr>
                            <tr>
                                <th>Case Description</th>
                                <td class="aggLead_case"></td>
                            </tr>
                            <tr>
                                <th>Medical Condition</th>
                                <td class="aggLead_condition"></td>
                            </tr>
                            <tr>
                                <th>Medications</th>
                                <td class="aggLead_medication"></td>
                            </tr>
                            <tr>
                                <th>Surgeries Undergone/Continuing</th>
                                <td class="aggLead_procedures"></td>
                            </tr>
                            <tr>
                                <th>Service Required</th>
                                <td class="aggLead_service"></td>
                            </tr>
                            <tr>
                                <th>Language Preference</th>
                                <td class="aggLead_lang_pref"></td>
                            </tr>
                            <tr>
                                <th>Gender Preference</th>
                                <td class="aggLead_gender_pref"></td>
                            </tr>
                            <tr>
                                <th>Rate Agreed</th>
                                <td class="aggLead_rate"></td>
                            </tr>
                            <tr>
                                <th>Rate Negotiable</th>
                                <td class="aggLead_negotiable"></td>
                            </tr>
                        </table>
                        <form action="{{ route('lead.aggregator-lead-status') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="lead_id" value="" />
                            <input type="hidden" name="patient_id" value="" />
                            <div class="col-sm-12 text-center">
                                <div class="col-sm-6 text-center">
                                    <button type="submit" onclick="javascript: return confirm('Are you sure?')" name="leadStatus" value="1" class="btn btn-block btn-success">Accept, I have staff</button>
                                </div>
                                <div class="col-sm-6 text-center">
                                    <button type="submit" onclick="javascript: return confirm('Are you sure?')" name="leadStatus" value="0" class="btn btn-block btn-warning">Decline, I do not have staff</button>
                                </div>
                            </div><br><br>
                        </form>
                        <code style="font-size: 10px;line-height:1">
                            <i class="material-icons" style="font-size:16px">info_outline</i></small> By accepting the case, you acknowledge the availability of staff to render the service to the patient.
                        </code>
                    </div>
                    <div class="clearfix"></div><br>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="followupsModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-col-blue" >
                <div class="modal-header">                    
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <a class="btn btn-sm bg-teal pull-right" style="margin-right: 20px;margin-top:-1%" onclick="javascript: printData('.followUpTable');">Print</a>
                    <h4 class="modal-title" id="largeModalLabel">Required Follow Up List</h4>
                </div>
                <div class="modal-body">
                    
                    <div class="clearfix"></div><br>
                    <div class="table-responsive followUpTable" style="margin-bottom: 0;max-height:600px;overflow: auto;">
                        <table class="table table-bordered table-condensed table-hover followupsTable" style="margin-bottom: 0">
                            <thead>
                                <tr>
                                    <th width="3%">#</th>
                                    <th>Patient</th>
                                    <th>Enquirer</th>
                                    <th>Date to follow up</th>
                                    <th>Time</th>
                                    <th>Comment</th>
                                    <th>Phone</th>
                                    <th class="hidden-print">Link</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(isset($followupArray) && count($followupArray))
                                @foreach($followupArray as $i => $f)
                                    <tr>
                                        <td>{{ $i + 1 }}</td>
                                        <td>{{ $f['patient'] }}</td>
                                        <td>{{ $f['enquirer'] }}</td>
                                        <td>{{ $f['date'] }}</td>
                                        <td>{{ $f['time'] }}</td>
                                        <td>{{ $f['comment'] }}</td>
                                        <td>{{ $f['phone'] }}</td>
                                        <td class="hidden-print"><a rel="noreferrer noopener" target="_blank" href="{{ route('lead.view',$f['id']) }}" class="btn btn-xs btn-success">View</a></td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="8" class="text-center">
                                        No Required Follow Up(s) found.
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div><br>
                </div>
                <div class="modal-footer" style="border-top: 1px solid #ccc">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="allocationsModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-col-blue" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title" id="largeModalLabel">Required Allocations List</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive" style="margin-bottom: 0;max-height:600px;overflow: auto;">
                        <table class="table table-bordered table-condensed table-hover distributionsTable" style="margin-bottom: 0">
                            <thead>
                                <tr>
                                    <th width="3%">#</th>
                                    <th>Patient</th>
                                    <th>Enquirer</th>
                                    <th>Phone</th>
                                    <th>Status</th>
                                    <th>Link</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(isset($allocationArray) && count($allocationArray))
                                @foreach($allocationArray as $i => $a)
                                    <tr>
                                        <td>{{ $i + 1 }}</td>
                                        <td>{{ $a['patient'] }}</td>
                                        <td>{{ $a['enquirer'] }}</td>
                                        <td>{{ $a['phone'] }}</td>
                                        <td>{{ $a['status'] }}</td>
                                        <td><a rel="noreferrer noopener" target="_blank" href="{{ route('lead.view',$a['id']) }}" class="btn btn-xs btn-success">View</a></td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="text-center">
                                        No Required Allocation(s) found.
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div><br>
                </div>
                <div class="modal-footer" style="border-top: 1px solid #ccc">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>    
    
    <div class="modal fade" id="serviceExpiryModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-col-blue">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title" id="largeModalLabel">Upcoming Service Expiry List</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive" style="margin-bottom: 0;max-height:600px;overflow: auto;">
                        <table class="table table-bordered table-condensed table-hover serviceExpiryTable">
                            <thead>
                                <tr>
                                    <th width="3%">#</th>
                                    <th>Episode ID</th>
                                    <th>Service Name</th>
                                    <th>Patient</th>
                                    <th>Enquirer</th>
                                    <th>Phone</th>
                                    <th width="10%">Expiry Date</th>
                                    <th>Link</th>
                                </tr>
                            </thead>
                            <tbody>                            
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div><br>
                </div>
                <div class="modal-footer" style="border-top: 1px solid #ccc">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
    {{--@endability--}}
@endsection

@push('page.scripts')
    
@endpush

@section('page.scripts')
    @parent
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.dataTable').DataTable({
                processing: true,
                lengthMenu: [[50,100,200,-1],[50,100,200,"All"]], 
                language: {
                    processing: "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate' style='color:#ff0000'> Loading....</span>"
                },
            });
        });
        $(function(){
            $(".datepicker").flatpickr({
                dateFormat: "d-m-Y",
                mode: "single",
            });
        });
    </script>    
    <script>
    $(function(){
        $('#branchForCreateLead').on('change', function(){
            var branch = $(this).val();
            if(branch==''){
                $('#createLeadLink').attr('href','#');
                $('#createLeadLink button').prop('disabled',true);
            }else{
                $('#createLeadLink').attr('href','{{ route('corporate.lead.create') }}/'+branch);
                $('#createLeadLink button').prop('disabled',false);
            }
        });

        $('.content').on('click','.btnViewLead',function(){
            var id = $(this).data('id');
            var patientID = $(this).data('patient-id');
            var patient = JSON.parse($(this).data('patient').replace(/\'/g, '\"'));
            var data = JSON.parse($(this).data('case').replace(/\'/g, '\"'));
            
            modal = '#viewAggregatorLeadModal';

            $(modal+' input[name=lead_id]').val(id);
            $(modal+' input[name=patient_id]').val(patientID);
            $(modal+' .aggLead_patient').html(patient.first_name+' '+patient.last_name);
            $(modal+' .aggLead_gender_age_weight').html(patient.patient_age+', '+patient.gender+', '+patient.patient_weight);
            $(modal+' .aggLead_location').html(patient.area+','+patient.street_address+', '+patient.city);
            $(modal+' .aggLead_case').html(data.case_description);
            $(modal+' .aggLead_condition').html(data.medical_conditions);
            $(modal+' .aggLead_medication').html(data.medications);
            $(modal+' .aggLead_procedures').html(data.procedures);
            $(modal+' .aggLead_service').html(data.aggregator_service);
            $(modal+' .aggLead_lang_pref').html(data.language_preference);
            $(modal+' .aggLead_gender_pref').html(data.gender_preference);
            $(modal+' .aggLead_rate').html(data.aggregator_rate);
            $(modal+' .aggLead_negotiable').html(data.aggregator_rate_negotiable);

            $(modal).modal();
        });

        $('.content').on('click','.btnLeadType', function(){
            $('#leadTypeModal').modal();
            $('#leadTypeModal .btnOtherRequest').attr('data-id',$(this).data('id'));

            $('#leadTypeModal .openMedicalCase').attr('data-id',$(this).data('id'));
            $('#leadTypeModal .openMedicalCase').attr('href','');
            url = $('#leadTypeModal .openMedicalCase').data('href');
            url += '?ref='+$(this).data('id');
            $('#leadTypeModal .openMedicalCase').attr('href',url);
        });

        $('.content').on('click','.btnOtherRequest', function(){
            $('#leadTypeModal').modal('hide');
            id = $(this).attr('data-id');
            block = '#leadsTable tbody tr[data-id='+id+']';

            $('#otherRequestForm input[name=name]').val($(block+' td:eq(3)').html().replace(/<\/?[^>]+(>|$)/g, "").trim());
            $('#otherRequestForm input[name=phone_number]').val($(block+' td:eq(4)').html().replace(/<\/?[^>]+(>|$)/g, "").trim());
            $('#otherRequestForm input[name=email]').val($(block+' td:eq(5)').html().replace(/<\/?[^>]+(>|$)/g, "").trim());
            $('#otherRequestForm input[name=city]').val(removeTags($(block+' td:eq(6)').text()));

            // console.log($(block+' td:eq(6)').html()+"  :: "+removeTags($(block+' td:eq(6)').text()));

            $('#otherRequestForm #lead_id').val(id);

            $('#otherRequestsModal').modal();
        });

        $('.content').on('click', '.markLeadAsChecked', function(){
            id = $(this).data('id');
            if(confirm("Are you sure?")){
                $.ajax({
                    url: '{{ route('ajax.mark-lead-as-checked') }}',
                    type: 'POST',
                    data: {_token:'{{ csrf_token() }}',id: id},
                    success: function(data){
                        console.log(data);
                        $('#leadsTable tbody tr[data-id='+id+']')
                        .children('td, th')
                        .animate({ padding: 0 })
                        .wrapInner('<div />')
                        .children()
                        .slideUp(function() { $(this).closest('tr').remove(); });

                        //toastr.success("Lead Status has been updated");
                        alert("Lead Status has been updated");
                    },
                    error: function(err){
                        console.log(err);
                    }
                });
            }
        });
    });

    function removeTags(txt){
        var rex = /(<([^>]+)>)/ig;
        //return txt.replace(rex , ",").replace(/\s/g, '').replace(/,\s*$/, '');
        return txt.replace(/\s+/g,' ')
        .replace(/^\s+|\s+$/,'');
    }
    
    getServiceExpiry();
    
    function getServiceExpiry(){
        $.ajax({
            url: '{{ route('leads.get-upcoming-service-expiry-ajax') }}',
            type: 'POST',
            data: {_token: '{{ csrf_token() }}'},
            success: function (d){                    
                if(d.length){
                    html = ``;
                    link = '{{ url('leads/view') }}';
                    $.each(d, function(i){
                        html += `<tr>
                            <td>`+ (i + 1) +`</td>
                            <td>`+ d[i]['episode_id'] +`</td>
                            <td>`+ d[i]['service_name'] +`</td>
                            <td>`+ d[i]['patient'] +`</td>
                            <td>`+ d[i]['enquirer'] +`</td>
                            <td>`+ d[i]['phone'] +`</td>
                            <td>`+ d[i]['date'] +`</td>
                            <td><a target="_blank" href="`+ link + `/` + d[i]['id'] +`" class="btn btn-xs btn-success">View</a></td>
                        </tr>`;
                    });
                    $('.serviceExpiry .count-to').html(d.length);
                    $('.serviceExpiry .count-to').removeClass('loader');
                }else{
                    html = `<tr>
                        <td colspan="8" class="text-center">
                            No record(s) found
                        </td>
                    </tr>`;
                    $('.serviceExpiry .count-to').html('0');
                    $('.serviceExpiry .count-to').removeClass('loader');
                }
                $('.serviceExpiryTable tbody').html(html);
            },
            error: function (err){
                console.log(err);
            }
        });
    }

    function printData(elem) {
        data = $(elem).html();
        var myWindow = window.open('', 'my div', 'height=600,width=800');
        myWindow.document.write('<html><head><title>Follow-Up List</title>');
        myWindow.document.write('<link rel="stylesheet" href="{{ ViewHelper::ThemePlugin('bootstrap/css/bootstrap.min.css') }}" type="text/css" />');
        myWindow.document.write('</head><body >');
        myWindow.document.write(data);
        myWindow.document.write('</body></html>');
        myWindow.document.close(); // necessary for IE >= 10
        
        myWindow.onload = function(){ // necessary if the div contain images
            myWindow.focus(); // necessary for IE >= 10
            myWindow.print();
            myWindow.close();
        };
    }
    </script>
@endsection
