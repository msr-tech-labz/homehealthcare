@extends('layouts.corporate-layout')

@section('page_title','View Case - Case |')

@section('plugin.styles')
	<!-- Bootstrap Select Css -->
	<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
	<!-- Pepper Grinder Css for Multi Datespicker-->
	<link href="https://code.jquery.com/ui/1.12.1/themes/pepper-grinder/jquery-ui.css" rel="stylesheet" />
	<!-- Multiple Dates Picker Css-->
	<link href="https://cdn.rawgit.com/dubrox/Multiple-Dates-Picker-for-jQuery-UI/master/jquery-ui.multidatespicker.css" rel="stylesheet" />
	<!-- Bootstrap Tagsinput Css -->
	<link href="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
	<!-- NoUiSlider Css -->
	<link href="{{ ViewHelper::ThemePlugin('nouislider/nouislider.min.css') }}" />
	<!-- Font Awesome Css -->
	<link href="{{ ViewHelper::ThemePlugin('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
	<!-- Bootstrap datetimepicker Css -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">

	<!-- Med Autocomplete Css -->
	<link rel="stylesheet" href="https://clin-table-search.lhc.nlm.nih.gov/autocomplete-lhc-versions/13.0.2/autocomplete-lhc_jQueryUI.min.css"/>
	<!-- Print Css -->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/print.css') }}" media="print" />
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section('page.styles')
	<link rel="stylesheet" href="{{ ViewHelper::css('vtabs.css') }}" />
	<style>
		.card{
			box-shadow: none !important;
		}
		.flatpickr-calendar{
			left: 294.109px !important;
		}
		.daterangepicker{
			top: 22% !important;
		}
		#searchResults{
			z-index: 9999 !important;
			left:40% !important;
			left: 36% !important;
			width: 17%;
		}
		.autocomp_selected {
			width: 100% !important;
		}
		.jcrop-active{
			width: 300px !important;
			height: 300px !important;
		}
		.modal-body .row{
			margin-bottom: 5px !important;
		}
		.modal-body{
			padding-top: 2px !important;
		}
		.modal-body h4.card-inside-title{
			font-size: 14px;
			color: #009688 !important;
		}
		.bootstrap-datetimepicker-widget{
			width: 300px !important;
		}
		.bootstrap-datetimepicker-widget table td.disabled{
			color: red !important;
		}
		.bootstrap-notify-container{
			z-index: 99999 !important;
		}
		.ui-datepicker .ui-datepicker-calendar .ui-state-highlight a{
			background: #4caf50 none !important;
			color: #fff !important;
		}

		.daterangepicker td.highlight, .daterangepicker option.highlight{
		    color: #ff9800 !important;
		}
		.multi-col{
			-webkit-column-count: 4; /* Chrome, Safari, Opera */
			-moz-column-count: 4; /* Firefox */
			column-count: 4;
			-webkit-column-gap: 20px; /* Chrome, Safari, Opera */
			-moz-column-gap: 20px; /* Firefox */
			column-gap: 20px;
			-webkit-column-rule: 1px solid lightblue; /* Chrome, Safari, Opera */
			-moz-column-rule: 1px solid lightblue; /* Firefox */
			column-rule: 1px solid lightblue;
		}
		.multi-col-worklog{
			-webkit-column-count: 2; /* Chrome, Safari, Opera */
			-moz-column-count: 2; /* Firefox */
			column-count: 2;
			-webkit-column-gap: 20px; /* Chrome, Safari, Opera */
			-moz-column-gap: 20px; /* Firefox */
			column-gap: 20px;
			-webkit-column-rule: 1px solid lightblue; /* Chrome, Safari, Opera */
			-moz-column-rule: 1px solid lightblue; /* Firefox */
			column-rule: 1px solid lightblue;
		}
		@page
		{
			size: auto;   /* auto is the initial value */
			margin: 0mm;  /* this affects the margin in the printer settings */
		}

		.light-red{
			background-color: rgba(239, 152, 152, 0.46) !important;
		}
		.light-blue{
			background-color: rgba(51, 153, 255, 0.46) !important;
		}
		.form-control-label{
			text-align: left;
		}
		.form-group .form-line, .form-group{
			text-transform: uppercase; !important;
		}
		.form-group label{
			text-transform: capitalize !important;
		}
		.pac-container {
			z-index: 9999 !important;
		}
		.nav-tabs > li{
			/*min-width: 12%;*/
		}
		.card .header .header-dropdown{
			top: 15px;
		}
		hr{
			margin-top: 10px;
			margin-bottom: 10px;
		}
		.staffSearchResults thead tr th{
			font-size: 12px !important;
		}
		.table-patient tbody tr td, .table-patient tbody tr th{
			padding: 5px !important;
			vertical-align: middle;
			height: 20px !important;
			border: 1px solid #f6f6f6;
			color: #777;
			font-size: 0.9em;
			font-weight: 600;
			white-space: normal;
			width: 150px;
		}
		.table-patient tbody tr td{
			font-weight: 500;
			color: #333;
			letter-spacing: 0.05em;
			padding-left: 8px !important;
		}
		.table-schedules thead tr th, .table-schedules tbody tr td{
			font-size: 12px !important;
		}
		.condensed thead tr th{
			padding: 5px 10px;
		}
		.table-borderless tbody tr td, .table-borderless tbody tr th{
			border: none;
			padding: 6px;
		}
		.table-borderless tbody tr td{
			min-width: 150px;
		}
		.routines tbody tr td{
			white-space: nowrap;
			width: 25% !important;
		}
		.table-borderless tbody tr td:before{
			content: ' :';
			padding-right: 10px;
			font-weight: bold;
		}
		.bootstrap-tagsinput{
			min-height: 40px;
			vertical-align: top;
			padding: 4px 0;
			width: 100%;
		}
		.bootstrap-tagsinput input{
			padding-left: 0;
		}
		.plus:after{
			content: ' + ';
			font-size: 18px;
			font-weight: bold;
		}
		.care-plan-card .header{
			padding: 15px 20px;
		}
		.modal-lg{
			width: 70%;
		}
		.modal .modal-header{
			padding: 20px 25px 15px 25px;
		}
		.modal-footer{
			border-top: 1px solid #ddd !important;
		}
		.details-pane .details{
			margin-top: 6px;
		}
		.details-pane .details .form-line{
			border-bottom: none;
		}
		.details-pane .details .form-line .form-control, .details-pane .details .form-line .form-control:focus{
			outline: 0;
			border-bottom: 1px solid #ff5722 !important;
		}

		.card .body .col-lg-9, .card .body .col-lg-3{
			margin-bottom: 8px !important;
		}
		.careplan-status button.waves-effect{
			width: 160px !important;
		}
		.comment-card { margin-bottom: 15px;}
		.comment-card .header { padding: 5px 10px;}
		.comment-card .body { padding: 10px;}
		.comment-card .header h2 {font-size: 16px}
		.comment-card .header .header-dropdown li > span {font-size: 13px}
		.comment-card th{ background: #ccc}
		.comment-card .table{ margin-bottom: 0}

		.details-pane div.col-lg-8, .details-pane div.col-lg-4{ margin-bottom: 8px !important}

		.noUi-horizontal .noUi-handle{
			background-color: #ff0000;
		}
		.text-muted {
			color: #777;
			position: absolute;
			right: 10%;
		}
		.search-table-outter {border:2px solid #b5b5b5;}
		.search-table, td, th{border-collapse:collapse; border:none;}
		th{padding:7px 7px; font-size:1em; font-weight: 600; color:#555; background:#f6f6f6;}
		td{padding:7px 10px; height:35px; font-size:1em; font-weight: 500; color:#333;}
		.search-table-outter {
			overflow-x: scroll;
		}

		.card-inside-title{
			color: #444 !important;
		}

		.condensed tbody tr td, .condensed tbody tr th{
			padding: 3px 10px !important;
			line-height: 1.5 !important;
			font-size: 12px;
		}

		.dataTables_length .bootstrap-select{
			display: inline !important;
		}
		.dataTables_empty{
			text-align: center !important;
		}

		#ui-datepicker-div{
			top:640px !important;
		}
		/* Map Styles */
		#infowindow-content .title {
			font-weight: bold;
		}

		#infowindow-content {
			display: none;
		}

		#map #infowindow-content {
			display: inline;
		}

		.pac-card {
			margin: 10px 10px 0 0;
			border-radius: 2px 0 0 2px;
			box-sizing: border-box;
			-moz-box-sizing: border-box;
			outline: none;
			box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
			background-color: #fff;
			font-family: Roboto;
			right: -1px;
			width: 75%;
		}

		#pac-container {
			padding-bottom: 12px;
			margin-right: 12px;
		}

		#pac-input {
			width: 99%;
			background-color: #fff;
			font-family: Roboto;
			font-size: 15px;
			font-weight: 300;
			margin-left: 12px;
			margin-top: 5px;
			padding: 0 11px 0 13px;
			text-overflow: ellipsis;
			/*width: 400px;*/
		}

		#pac-input:focus {
			border-color: #4d90fe;
		}

		#title {
			color: #fff;
			background-color: #4d90fe;
			font-size: 20px;
			font-weight: 500;
			padding: 6px 12px;
		}

		#viewSchedulesModal .dataTables_wrapper > .top > div{
			min-width: 25% !important;
			display: inline-block !important;
			margin: 0 10px !important;
			margin-bottom: 20px;
		}

		#viewSchedulesModal{
			left: -15%;
			width: 130%;
		}

		#viewSchedulesModal .dataTables_paginate{
			text-align: center !important;
			vertical-align: middle !important;
		}

		#viewSchedulesModal .dataTables_paginate > .pagination{
			margin: 0 !important;

		}

		#viewSchedulesModal .dataTables_filter{
			float: right !important;
		}

		.demo-checkbox label, .demo-radio-button label {
			min-width: 0;
		}

		.btn:not(.btn-link):not(.btn-circle){
			box-shadow: none !important;
		}

		.current-schedule-chargeable:hover{
			color: #fff !important;
		}
		.chargeability{
			display: unset !important;
		}
		.schedule-sel[type="checkbox"] + label {
		    margin-bottom: 0px !important;
		    margin-top: 8px;
		}
		.caret-right{
			border-left: 4px solid white;
			    border-right: 0;
			    border-top: 4px solid transparent;
			    border-bottom: 4px solid transparent;
		}
		#createSchedulesForm .dataTables_scrollBody{
			overflow: inherit !important;
		}
		.dataTables_scrollHeadInner{
			padding-left:0 !important; 
		}
		.swal-button--notdone {
		  background-color: red;
		  border: 1px solid red;
		}
		.swal-button--done {
		  background-color: green;
		  border: 1px solid green;
		}
		.popover .popover-content {
		    font-size: 13px !important;
		    color: #000 !important;
		}
	</style>
@endsection

@section('content')

	@if (count($errors) > 0)
	<div class="row clearfix">
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	</div>
	@endif

	<div class="loader2"></div>
	<div class="row hidden-print">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card" style="min-height: 138px;">
				<div class="header" style="padding-bottom:5px">
					<a href="{{ route('corporate.lead.index') }}" class="pull-left btn btn-info btn-circle clearLocalStore waves-effect waves-circle waves-float">
						<i class="material-icons">arrow_back</i>
					</a>
					<h2 class="col-sm-8">
						{{ isset($l)?'View':'New'}}
						{{ isset($l) && in_array($l->status,["Pending","Follow Up"])?'Lead':'Episode'}} - {{ $l->episode_id ?? 'TEMPLD0AF'.$l->id }}

						<small>Patient Case File</small>
					</h2>
					
					<a href="{{ route('corporate.lead.prevservicerecords',[Helper::encryptor('encrypt',$l->patient->id),$database_name]) }}" target="_blank" class="pull-right btn btn-primary"><span>Previous Records</span></a>
					<div class="col-sm-2 pull-right" style="width:20%">
						<span class="pull-left">Patient ID: <b>{{ isset($l->patient->patient_id)?$l->patient->patient_id:'TEMPPT0AF'.$l->patient->id }}</b></span><br>
						<span class="pull-left">Status: <b>{{ $l->status }}</b>&nbsp;&nbsp;
						{{--@ability('admin','leads-disposition-add')--}}
							@caseNotClosed($l->status)
							<button type="button" class="btn bg-cyan btn-circle waves-effect waves-circle waves-float btnChangeLeadStatus" style="height:30px;width:30px;line-height:1">
								<i class="material-icons" style="left: -4px !important;font-size:15px">edit</i>
							</button>
							@endCaseNotClosed
						{{--@endability--}}
						</span>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="body" style="padding: 10px 8px;">
					<div class="row">
						<div class="@if(empty($l->special_instructions)){{ 'col-lg-12 col-md-12 col-sm-12 col-xs-12' }}@else{{ 'col-lg-9 col-md-9 col-sm-9 col-xs-12' }}@endif" style="margin-bottom:0px !important;padding-right:0;@if(empty($l->special_instructions)){{ 'width:98%' }}@endif">
							<div class="card" style="margin-bottom:10px !important;margin-left:15px">
								<div class="body">
									<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style="margin-top: 5px;margin-bottom:7px;padding-left:10px;text-align: center;">
										@if(file_exists(public_path('uploads/provider/'.$tenant_id.'/case_documents/'.Helper::encryptor('encrypt',$l->id))))
											@php $path = url('uploads/provider/'.$tenant_id.'/case_documents/'.Helper::encryptor('encrypt',$l->id).'/'.Helper::encryptor('encrypt',$l->patient_id).'.jpg'); @endphp
										@else
											{? $path = '/img/'; ?}
											{? $path .= in_array($l->patient->gender,['Male','Female'])?'icon_'.strtolower($l->patient->gender).'.jpg':'default-avatar.jpg'; ?}
										@endif
										<img class="img-responsive img-thumbnail" style="width:120px;height:120px" src="{{ $path }}" />
										<center><a class="btn btn-xs btn-warning text-center" data-toggle="modal" data-target="#patientPictureModal"><i style="font-size:15px" class="material-icons">edit</i> Change Picture</a></center>
										<br>
										{{-- <a href="{{ route('corporate.patient.addemailaddress',[Helper::encryptor('encrypt',$l->patient->id),$database_name]) }}" target="_blank" class="btn btn-primary"><span>Email - Address</span></a> --}}
										<a href="{{ route('corporate.patient.addemailaddress',[Helper::encryptor('encrypt',$l->patient->id),$database_name]) }}" target="_blank">
										<button type="button" class="btn btn-sm btn-primary waves-effect">
											<i class="material-icons">add_circle_outline</i>
											<span style="position: relative;top: -2px;margin-left: 3px;">Email - Address</span>
										</button>
										</a>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" style="padding-left:0;height:inherit;margin-bottom:-1px !important;">
										<table class="table table-patient condensed">
											<tr>
												<th>Patient Name</th>
												<td colspan="3">{{ $l->patient->full_name ?? '' }}</td>
												<th>Patient ID</th>
												<td>{{ $l->patient->patient_id ?? '' }}</td>
											</tr>
											<tr>
												<th>Gender</th>
												<td>{{ $l->patient->gender ?? '' }}</td>
												<th>Age</th>
												<td>{{ $l->patient->patient_age ?? '' }}</td>
												<th>Weight (kg)</th>
												<td>{{ $l->patient->patient_weight ?? '' }}</td>
											</tr>
											<tr>
												<th>Contact Number</th>
												<td><a href="tel:{{ $l->patient->contact_number ?? '' }}">{{ $l->patient->contact_number ?? '' }}</a></td>
												<th>Email</th>
												<td colspan="3">{{ $l->patient->email ?? '' }}</td>
											</tr>
											<tr>
												<th>Address</th>
												<td colspan="5">
													{{ !empty($l->patient->street_address)?($l->patient->street_address.','):'' }}
													{{ !empty($l->patient->area)?($l->patient->area.','):'' }}
													{{ !empty($l->patient->city)?($l->patient->city.' - '):''}}
													{{ !empty($l->patient->zipcode)?($l->patient->zipcode.','):'' }}
													{{ !empty($l->patient->state)?$l->patient->state:'' }}
													@if(!empty($l->patient->area) || (!empty($l->patient->latitude) && !empty($l->patient->longitude)))
													| <a href="javascript:void(0);" data-target="#patientLocationModal" data-toggle="modal">View Map</a>

													<div class="modal fade" id="patientLocationModal" aria-labelledby="modalLabel" role="dialog" tabindex="-1">
														<div class="modal-dialog" role="document" style="width:60%">
															<div class="modal-content">
																<div class="modal-header bg-cyan">
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																	<h5 class="modal-title" id="modalLabel">Patient Location</h5>
																</div>
																<div class="modal-body">
																@if(!empty($l->patient->latitude) && !empty($l->patient->longitude))
																	{? $url = $l->patient->latitude.",".$l->patient->longitude; ?}
																@else
																	<br><span><strong class="col-red">Warning!</strong> Map shown below may not have patient's exact location</span><br><br>
																	{? $url = $l->patient->area.','.$l->patient->city.','.$l->patient->state; ?}
																@endif
																	<iframe src="https://maps.google.com/maps?q={{ $url }}&z=14&amp;output=embed" width="770" height="400" frameborder="0" style="border:0"></iframe>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
																</div>
															</div>
														</div>
													</div>
													@endif
												</td>
											</tr>
											<tr>
												<th>Enquirer</th>
												<td>{{ $l->patient->enquirer_name ?? '' }}</td>
												<th>Contact Number</th>
												<td colspan="3"><a href="tel:{{ $l->patient->alternate_number ?? '' }}">{{ $l->patient->alternate_number ?? '' }}</a></td>
											</tr>
										</table>
									</div>
									{{--@ability('admin','leads-patient-details-add,leads-patient-details-edit')--}}
									<div class="col-lg-2">
										<button type="button" data-toggle="modal" title="Edit Patient" class="btn bg-teal btn-circle pull-right waves-effect waves-circle waves-float editPatient">
											<i class="material-icons">edit</i>
										</button>
									</div>
									{{--@endability--}}
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						@if(!empty($l->special_instructions))
							<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="width:23.8%;">
								<div class="card" style="margin-bottom: 0">
									<div class="header bg-orange" style="padding: 5px">
										<h2 style="font-size:16px;">
											Special Instructions
										</h2>
									</div>
									<div class="body" style="padding: 5px;height: 138px;overflow-y:auto;font-size: 15px">
										{{ $l->special_instructions }}
									</div>
								</div>
							</div>
						@endif
						<div class="clearfix"></div>
					</div>

					<div class="clearfix"></div><br>
				</div>
			</div>
		</div>
	</div>

	<!-- Patient Image -->
	<div class="modal fade" id="patientPictureModal" aria-labelledby="modalLabel" role="dialog" tabindex="-1">
		<div class="modal-dialog" role="document">
			<form action="{{ route('corporate.patient.upload-picture') }}" method="POST" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="modal-content">
					<div class="modal-header bg-cyan">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h5 class="modal-title" id="modalLabel">Patient Photo</h5>
					</div>
					<div class="modal-body">
						<div class="row clearfix" style="margin-top:5px">
							<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
								<label for="lead_status">Choose File</label>
							</div>
							<div class="col-lg-7 col-md-7 col-sm-5 col-xs-7">
								<div class="form-group">
									<input type="file" class="form-control" id="pic_upload" name="pic_upload" accept="image/*" required=""/>
								</div>
							</div>
						</div>
						<div class="row clearfix">
							<center>
								<div class="profile-img-block"></div>
							</center>
						</div>
					</div>
					<div class="modal-footer">
						<input type="hidden" name="crop_values" value="" />
						<input type="hidden" name="database_name" value="{{ $database_name }}" />
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-success">Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<!-- Case Status Modal -->
	<div class="modal fade" id="leadStatusModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" style="z-index:1052 !important">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header bg-blue">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="largeModalLabel">Case Status</h4>
				</div>
				<div class="modal-body" style="padding-top:15px !important">
					<div class="row clearfix">
						<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
							<label for="lead_status">Status</label>
						</div>
						<div class="col-lg-7 col-md-7 col-sm-5 col-xs-7">
							<div class="form-group">
								<select class="form-control show-tick" id="lead_status" name="checkState" onchange="checkCaseLoss(this.value);">
									<option value="Pending" {{ $l->status=='Pending'?'selected':'' }}>Pending</option>
									<option value="Converted" {{ $l->status=='Converted'?'selected':'' }}>Converted</option>
									<option value="Follow Up" {{ $l->status=='Follow Up'?'selected':'' }}>Follow Up</option>
									<option value="Assessment Pending" {{ $l->status=='Assessment Pending'?'selected':'' }}>Assessment Pending</option>
									<option value="Assessment Completed" {{ $l->status=='Assessment Completed'?'selected':'' }}>Assessment Completed</option>
									<option value="Closed" {{ $l->status=='Closed'?'selected':'' }}>Closed</option>
									<option value="Dropped" {{ $l->status=='Dropped'?'selected':'' }}>Dropped / Case Loss</option>
									<option value="No Supply" {{ $l->status=='No Supply'?'selected':'' }}>No Supply</option>
								</select>
							</div>
						</div>
					</div>

					<div class="row clearfix" id="dropped" style="display:none;border-radius: 5px;background: rgba(162, 18, 18, 0.06); padding: 15px;margin-bottom: 10px;">
						<div class="col-lg-12 form-control-label" style="text-align: center !important;">
							<label for="dropped_reason">Dropped / Case Loss Reason</label>
						</div>
						<div class="col-lg-12 text-left">
							<div class="form-group" style="margin-top: 1%">
								<input name="dropped_reason[]" type="checkbox" id="0" class="with-gap filled-in chk-col-amber loss" value="Emergency Service"/>
								<label for="0">Emergency Service</label><br>

								<input name="dropped_reason[]" type="checkbox" id="1" class="with-gap filled-in chk-col-amber loss" value="No Response" />
								<label for="1">No Response</label><br>

								<input name="dropped_reason[]" type="checkbox" id="2" class="with-gap filled-in chk-col-amber loss" value="Prices are High" />
								<label for="2">Prices are High</label><br>

								<input name="dropped_reason[]" type="checkbox" id="3" class="with-gap filled-in chk-col-amber loss" value="Household Helper" />
								<label for="3">Household Helper</label><br>

								<input name="dropped_reason[]" type="checkbox" id="4" class="with-gap filled-in chk-col-amber loss" value="Patient is hospitaized" />
								<label for="4">Patient is hospitaized</label><br>

								<input name="dropped_reason[]" type="checkbox" id="5" class="with-gap filled-in chk-col-amber loss" value="No Staff/Out of servic area" />
								<label for="5">No Staff/Out of servic area</label><br>

								<input name="dropped_reason[]" type="checkbox" id="6" class="with-gap filled-in chk-col-amber loss" value="Service taken from others" />
								<label for="6">Service taken from others</label><br>

								<input name="dropped_reason[]" type="checkbox" id="7" class="with-gap filled-in chk-col-amber loss" value="Patient Passed away" />
								<label for="7">Patient Passed away</label><br>

								<input name="dropped_reason[]" type="checkbox" id="8" class="with-gap filled-in chk-col-amber loss" value="General Inquiry about services and charges" />
								<label for="8">General Inquiry about our services and charges</label><br>

								<input name="dropped_reason[]" type="checkbox" id="9" class="with-gap filled-in chk-col-amber loss" value="Not willing to share any details" />
								<label for="9">Not willing to share any details</label><br>

								<input name="dropped_reason[]" type="checkbox" id="10" class="with-gap filled-in chk-col-amber loss" value="Bad Lead" />
								<label for="10">Bad Lead</label><br>

								<input name="dropped_reason[]" type="checkbox" id="11" class="with-gap filled-in chk-col-amber loss" value="Advance Payment" />
								<label for="11">Advance Payment</label><br>

								<input name="dropped_reason[]" type="checkbox" id="12" class="with-gap filled-in chk-col-amber loss" value="Duplicate Lead" />
								<label for="12">Duplicate Lead</label><br>
							</div>
						</div>
					</div>

					<div class="row clearfix" id="followup" style="display:none;border-radius: 5px;background: rgba(162, 18, 18, 0.06); padding: 15px;margin-bottom: 10px;">
						<div class="col-lg-12 text-left">
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label">
									<label for="follow_up_date">Follow Up Date</label>
								</div>
								<div class="col-lg-5 col-md-5 col-sm-5 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" class="form-control datetime" id="follow_up_date" autocomplete="off">
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label">
									<label for="follow_up_comment">Follow Up Comment</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<textarea class="form-control" id="follow_up_comment"></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row clearfix">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label">
							<label for="lead_disposition_date">Disposition Date</label>
						</div>
						<div class="col-lg-5 col-md-5 col-sm-5 col-xs-7">
							<div class="form-group">
								<div class="form-line">
									<input type="text" class="form-control date" id="lead_disposition_date" placeholder="Date" autocomplete="off">
								</div>
							</div>
						</div>
					</div>
					<div class="row clearfix">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label">
							<label for="lead_comment">Comment</label>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line">
									<textarea class="form-control" id="lead_comment" placeholder="Comments / Reason / Notes (if any)"></textarea>
								</div>
							</div>
						</div>
					</div><br>
					<div class="row clearfix">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="form-group text-center">
								<a class="btn btn-warning btn-lg waves-effect updateLeadStatus" data-lead-id="{{ Helper::encryptor('encrypt',$l->id) }}">Update Status</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Edit Patient Modal -->
	<div class="modal fade" id="editPatientModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" role="document" style="width:85%">
			<form id="editPatientForm" method="POST" action="{{ route('corporate.patient.save-patient') }}">
				<div class="modal-content">
					<div class="modal-header bg-blue">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="largeModalLabel">Edit Patient <span class="crn_no"></span></h4>
					</div>
					<div class="modal-body">
						<div class="row clearfix">
							<div class="col-sm-6 form-horizontal">
								<h4 class="card-inside-title col-teal">
									Basic Details
								</h4>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
										<label for="first_name">First Name</label>
									</div>
									<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" id="first_name" name="first_name" class="form-control searchCol" placeholder="Ashok" value="{{ isset($l->patient)?$l->patient->first_name:'' }}" title="Only Characters Allowed" required>
											</div>
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
										<div class="form-group">
											<div class="form-line">
												<input type="text" id="last_name" name="last_name" class="form-control searchCol" placeholder="Kumar" value="{{ isset($l->patient)?$l->patient->last_name:'' }}" title="Only Characters Allowed">
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
										<label for="date_of_birth">Date of Birth</label>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
										<div class="form-group">
											<div class="form-line">
												<input type="text" id="dob" name="dob" class="form-control bdate" placeholder="20-03-1992" value="{{ (isset($l->patient) && $l->patient != null && isset($l->patient->date_of_birth))?$l->patient->date_of_birth->format('d-m-Y'):'' }}">
											</div>
										</div>
									</div>
									<div class="col-sm-1" style="line-height: 3"><b> or </b></div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
										<div class="form-group">
											<div class="form-line">
												<input type="number" min="5" id="patient_age" name="patient_age" class="form-control" placeholder="25" value="{{ (isset($l->patient) && isset($l->patient->patient_age))?$l->patient->patient_age:'' }}">
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
										<label for="weight">Patient Weight</label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="number" min="2" id="patient_weight" name="patient_weight" class="form-control" placeholder="70" value="{{ isset($l->patient->patient_weight)?$l->patient->patient_weight:'' }}">
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
										<label for="gender">Gender</label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<div class="form-group">
											<select class="form-control show-tick" id="gender" name="gender" required>
												<option value="">-- Please select--</option>
												<option value="Male" {{ (isset($l->patient) && $l->patient->gender == 'Male')?'selected':'' }}>Male</option>
												<option value="Female" {{ (isset($l->patient) && $l->patient->gender == 'Female')?'selected':'' }}>Female</option>
												<option value="Other" {{ (isset($l->patient) && $l->patient->gender == 'Other')?'selected':'' }}>Other</option>
											</select>
										</div>
									</div>
								</div>

								<h4 class="card-inside-title col-teal">
									Patient Address
								</h4>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label ">
										<label for="street_address">Address</label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" id="street_address" name="street_address" class="form-control" placeholder="Smart Health Global Private Limited" value="{{ isset($l->patient)?$l->patient->street_address:'' }}" >
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
										<label for="area">Area</label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" id="area" name="area" class="form-control" placeholder="Sahakar Nagar" value="{{ isset($l->patient)?$l->patient->area:'' }}">
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
										<label for="city">City</label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" id="city" name="city" class="form-control" placeholder="Bengaluru" value="{{ isset($l->patient)?$l->patient->city:'' }}" >
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
										<label for="zipcode">Zip Code</label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" id="zipcode" name="zipcode" class="form-control" placeholder="560092" value="{{ isset($l->patient)?$l->patient->zipcode:'' }}">
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
										<label for="state">State</label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" id="state" name="state" class="form-control" placeholder="Karnataka" value="{{ isset($l->patient)?$l->patient->state:'' }}" >
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
										<label for="country">Country</label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" id="country" name="country" class="form-control" placeholder="India" value="{{ isset($l->patient)?$l->patient->country:'' }}" >
											</div>
										</div>
									</div>
								</div>
								<h4 class="card-inside-title">
									Location Co-ordinates
									<small>Lattitude and Longitude</small>
								</h4>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
										<label for="latitude">Latitude</label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" id="latitude" name="latitude" class="form-control" placeholder="Latitude" value="{{ isset($l->patient)?$l->patient->latitude:'' }}">
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
										<label for="longitude">Longitude</label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" id="longitude" name="longitude" class="form-control" placeholder="Longitude" value="{{ isset($l->patient)?$l->patient->longitude:'' }}">
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-sm-6 form-horizontal">
								<h4 class="card-inside-title col-teal">
									Communication Details
								</h4>
								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label required">
										<label for="contact_number">Contact Number</label>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" id="contact_number" name="contact_number" onkeyup="checknumber();" class="form-control intl" value="{{ isset($l->patient)?$l->patient->contact_number:'' }}" required>
											</div>
											<span id="number_status" style="font-weight: bold;"></span>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
										<label for="alternate_number">Alternate Number</label>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="tel" id="alternate_number" name="alternate_number" class="form-control intl" placeholder="Alternate Number" value="{{ isset($l->patient)?$l->patient->alternate_number:'' }}">
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
										<label for="email">Email</label>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" id="email" name="email" class="form-control searchCol" placeholder="someone@example.com"value="{{ isset($l->patient)?$l->patient->email:'' }}">
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
										<label for="enquirer_name">Enquirer Name</label>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" id="enquirer_name" name="enquirer_name" class="form-control" placeholder="Nitish Kumar" value="{{ isset($l->patient)?$l->patient->enquirer_name:'' }}">
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
										<label for="relationship_with_patient">Relationship with Patient</label>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<select id="relationship_with_patient" name="relationship_with_patient" class="form-control show-tick" data-live-search="true">
													<option value="">-- Please select --</option>
													<option value="FATHER">FATHER</option>
													<option value="MOTHER">MOTHER</option>
													<option value="BROTHER">BROTHER</option>
													<option value="SISTER">SISTER</option>
													<option value="HUSBAND">HUSBAND</option>
													<option value="WIFE">WIFE</option>
													<option value="DAUGHTER">DAUGHTER</option>
													<option value="SON">SON</option>
													<option value="MOTHER-IN-LAW">MOTHER-IN-LAW</option>
													<option value="FATHER-IN-LAW">FATHER-IN-LAW</option>
													<option value="BROTHER-IN-LAW">BROTHER-IN-LAW</option>
													<option value="GRANDFATHER">GRANDFATHER</option>
													<option value="GRANDMOTHER">GRANDMOTHER</option>
													<option value="UNCLE">UNCLE</option>
													<option value="AUNT">AUNT</option>
													<option value="FRIEND">FRIEND</option>
													<option value="SELF">SELF</option>
													<option value="OTHER">OTHER</option>
												</select>
											</div>
										</div>
									</div>
								</div>

								<h4 class="card-inside-title">
									Location Map
								</h4>
								<div class="pac-card" id="pac-card">
									<div>
										<div id="title">
											Location search
										</div>
									</div>
									<div id="pac-container">
										<input type="text" id="pac-input" placeholder="Type here to search" />
									</div>
								</div>
								<div id="map" style="width: 500px; height:300px;"></div>
								<div id="infowindow-content">
									<img src="" width="16" height="16" id="place-icon">
									<span id="place-name"  class="title"></span><br>
									<span id="place-address"></span>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						{{ csrf_field() }}
						<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-success waves-effect pull-right btnSavePatientDetails">Save</button>
						<input type="hidden" name="database_name" value="{{ $database_name }}" />
						<input type="hidden" name="patient_id" value="{{ $l->patient->id }}">
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection

@section('plugin.scripts')
	<!-- Select Plugin Js -->
	<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>

	<!-- Multiple Dates Picker -->
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
	<script src="https://cdn.rawgit.com/dubrox/Multiple-Dates-Picker-for-jQuery-UI/master/jquery-ui.multidatespicker.js"></script>

	<!-- Input Mask Plugin Js -->
	<script src="{{ ViewHelper::ThemePlugin('jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>
	<!-- Moment Plugin Js -->
	<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
	<!-- Bootstrap Material Datetime Picker Plugin Js -->
	<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>

	<!-- Bootstrap Tags Input Plugin Js -->
	<script src="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
	<script src="{{ ViewHelper::ThemePlugin('bootstrap-3-typeahead/bootstrap3-typeahead.js') }}"></script>

	<!-- Bootstrap Notify Plugin Js -->
	<script src="{{ ViewHelper::ThemePlugin('bootstrap-notify/bootstrap-notify.js') }}"></script>
	<script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
	<!-- noUiSlider-->
	<script src="{{ asset('themes/default/plugins/nouislider/nouislider.js') }}"></script>
	<script src="{{ asset('js/autocomplete.js')}}"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2-azeUdXbeAqQrvGtLZrbsXIFcofE_B8&libraries=places&region=IN"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.js"></script>
	<script src="https://clin-table-search.lhc.nlm.nih.gov/autocomplete-lhc-versions/13.0.2/autocomplete-lhc_jQueryUI.min.js"></script>
	<!--  Sweetalert -->
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection

@section('page.scripts')
	<script src="{{ ViewHelper::js('leads/form.js') }}"></script>
	<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
	<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
	<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
	<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
	<script src="/vendor/datatables/buttons.server-side.js"></script>

	<!-- Include Date Range Picker -->
	<script type="text/javascript" src="{{ ViewHelper::ThemeJs('daterangepicker.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
	<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.4.3/plugins/confirmDate/confirmDate.js"></script>
    <script src="{{ asset('themes/default/js/pages/ui/tooltips-popovers.js') }}"></script>
	<script type="text/javascript">
		var assessmentFlag = '';
		var assessmentDone = '';
		var outstanding = 0;
		var nurseForm  = `@include('partials.forms.assessment.nurse')`;
		var doctorForm = `@include('partials.forms.assessment.doctor')`;
		var physioForm = `@include('partials.forms.assessment.physiotherapist')`;
		var labForm    = `@include('partials.forms.lab.labtest')`;
		var scheduledArr = [];
		var dateRangePickerConfig = {
			autoApply: true,
			autoUpdateInput: true,
			alwaysShowCalendars: true,
			opens: "center",
			parentEl: '#createSchedulesForm',
			locale: {
				format: 'DD-MM-YYYY',
				applyLabel: 'Apply',
				cancelLabel: 'Clear'
			},
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			isHighlightDate: function(arg){
			    if(scheduledArr.length){
			        // Prepare the date comparision
			        var thisMonth = arg._d.getMonth()+1;   // Months are 0 based
			        if (thisMonth<10){
			            thisMonth = "0"+thisMonth; // Leading 0
			        }
			        var thisDate = arg._d.getDate();
			        if (thisDate<10){
			            thisDate = "0"+thisDate; // Leading 0
			        }
			        var thisYear = arg._d.getYear()+1900;   // Years are 1900 based

			        var thisCompare = thisDate +"-"+ thisMonth +"-"+ thisYear;
			        //console.log('thisCompare: '+thisCompare+' => '+$.inArray(thisCompare,disabledArr));

			        if($.inArray(thisCompare,scheduledArr)!=-1){
			            //console.log("      ^--------- DATE FOUND HERE");
			            return true;
			        }
			    }
			}
		};

		function initDateRangePicker(){
			$('.daterange').daterangepicker(dateRangePickerConfig ,function(start, end, label) {
				$('input[name=create_schedule_period_from]').val(start.format('YYYY-MM-DD'));
				$('input[name=create_schedule_period_to]').val(end.format('YYYY-MM-DD'));
				rate = $('#createSchedulesForm .addScheduleBlock input[name=service_rate]').val();
				if(rate > 0){
					noOfDays = end.diff(start, 'days');
					totalAmount = noOfDays * rate;
					if(outstanding != 0){
						if(outstanding > 0 || totalAmount > outstanding){
							$('#createSchedulesForm .outstanding-msg-block-schedules > div').html('Customer has an outstanding balance, Schedules amount should not exceed outstanding amount!');
							$('#createSchedulesForm .outstanding-msg-block-schedules').show();
							$('#createSchedulesForm .btnChooseStaff').prop('disabled',true);
							$('#createSchedulesForm .btnScheduleAction').prop('disabled',true);
							$('#createSchedulesForm .btnChooseStaff').hide();
							$('#createSchedulesForm .btnScheduleAction').hide();
						}
					}else{
						$('#createSchedulesForm .outstanding-msg-block-schedules > div').html('');
						$('#createSchedulesForm .outstanding-msg-block-schedules').hide();
						$('#createSchedulesForm .btnChooseStaff').prop('disabled',false);
						$('#createSchedulesForm .btnScheduleAction').prop('disabled',false);
						$('#createSchedulesForm .btnChooseStaff').show();
						$('#createSchedulesForm .btnScheduleAction').show();
					}
				}
			});

			$('.daterange').on('apply.daterangepicker', function(ev, picker) {
				$(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
				$('#createSchedulesForm input[name=create_schedule_period_from]').val(picker.startDate.format('DD-MM-YYYY'));
				$('#createSchedulesForm input[name=create_schedule_period_to]').val(picker.endDate.format('DD-MM-YYYY'));
			});
		}

		function validator() {
			var fname = $('#editCaseForm #first_name').val();
			var gender = $('#editCaseForm #gender option:selected').val();
			var address = $('#editCaseForm #street_address').val();
			var city = $('#editCaseForm #city').val();
			var state = $('#editCaseForm #state').val();
			var country = $('#editCaseForm #country').val();
			var relationship = $('#editCaseForm #relationship_with_patient option:selected').val();
			var contactNumber = $('#contact_number').val();
			var email = $('#editCaseForm #email').val();
			{{--@ability('admin','leads-service-details-add')--}}
			var serviceRequired = $('#editCaseForm #service_required option:selected').val();
			var branchId = $('#editCaseForm #branch_id option:selected').val();
			{{--@endability--}}

			{{--@ability('admin','leads-payment-details-add')--}}
			var rateAgreed = $('#editCaseForm #rate_agreed').val();
			{{--@endability--}}

			if (fname == ""){
				alert("Please Enter First Name");
				return false;
			}
			if (gender == ""){
				alert("Please Enter Gender");
				return false;
			}
			if (address == ""){
				alert("Please Enter Address");
				return false;
			}
			if (city == ""){
				alert("Please Enter City");
				return false;
			}
			if (state == ""){
				alert("Please Enter State");
				return false;
			}
			if (country == ""){
				alert("Please Enter Country");
				return false;
			}
			if (relationship == ""){
				alert("Please Enter Relationship");
				return false;
			}
			if (contactNumber == ""){
				alert("Please Enter Contact Number");
				return false;
			}
			@if(Helper::getSetting('patient_email_mandatory')==1)
			if (email == ""){
				alert("Please Enter Email");
				return false;
			}
			@endif

			$('input[name=manager_id]').val($('#manager_id').val().join().toString());

			return true;
		}

		$(function(){
			@if(isset($outstanding))
			outstanding = parseFloat('{{ $outstanding['outstanding_balance'] }}');
			@endif
			// console.log(outstanding);

			var autocomplete_init = false;
			var autocomplete;
			var map;

			$('.content').on('click','.editPatient',function(){
				$("#editPatientModal")
				.modal()
				.on("shown.bs.modal", function() {
					document.getElementById('pac-input').value = "";
					if(!autocomplete_init){
						initMap();
					}
					google.maps.event.trigger(map, 'resize');
				});
			});

			var componentForm = {
				sublocality_level_2: 'short_name',
				street_number: 'short_name',
				route: 'long_name',
				locality: 'long_name',
				administrative_area_level_1: 'long_name',
				country: 'long_name',
				postal_code: 'short_name'
			};

			var componentIds = {
				sublocality_level_2: 'area',
				locality: 'city',
				administrative_area_level_1: 'state',
				country: 'country',
				postal_code: 'zipcode'
			};

			function handleLocationError(browserHasGeolocation, infoWindow, pos) {
			}

			function initMap() {
				map = new google.maps.Map(document.getElementById('map'), {
					center: {lat: 16.28004385258969, lng: 78.10594640625004},
					zoom: 6
				});

				var card = document.getElementById('pac-card');
				var input = document.getElementById('pac-input');
				var types = document.getElementById('type-selector');
				var strictBounds = document.getElementById('strict-bounds-selector');

				map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

				autocomplete = new google.maps.places.Autocomplete(input);
				autocomplete_init = true;

				// Bind the map's bounds (viewport) property to the autocomplete object,
				// so that the autocomplete requests use the current map bounds for the
				// bounds option in the request.
				autocomplete.bindTo('bounds', map);

				var infowindow = new google.maps.InfoWindow();
				var infowindowContent = document.getElementById('infowindow-content');
				infowindow.setContent(infowindowContent);
				var marker = new google.maps.Marker({
					map: map,
					animation: google.maps.Animation.DROP,
					draggable: true,
					anchorPoint: new google.maps.Point(0, -29)
				});

				// Try HTML5 geolocation.
				if (navigator.geolocation) {
					navigator.geolocation.getCurrentPosition(function(position) {
						var pos = {
							lat: position.coords.latitude,
							lng: position.coords.longitude
						};
						map.setCenter(pos);
					}, function() {
						handleLocationError(true, infowindow, map.getCenter());
					});
				}
				else {
					// Browser doesn't support Geolocation
					handleLocationError(false, infowindow, map.getCenter());
				}

				google.maps.event.addListener(
					marker,
					'drag',
					function(event) {
						infowindow.close();
						document.getElementById('latitude').value = this.position.lat();
						document.getElementById('longitude').value = this.position.lng();
						//alert('drag');
				});

				google.maps.event.addListener(marker,'dragend',function(event) {
					infowindow.close();
					document.getElementById('latitude').value = this.position.lat();
					document.getElementById('longitude').value = this.position.lng();
					//alert('Drag end');
				});

				autocomplete.addListener('place_changed', function() {
					infowindow.close();
					marker.setVisible(false);
					var place = autocomplete.getPlace();
					if (!place.geometry) {
						// User entered the name of a Place that was not suggested and
						// pressed the Enter key, or the Place Details request failed.
						window.alert("No details available for input: '" + place.name + "'");
						return;
					}

					// If the place has a geometry, then present it on a map.
					if (place.geometry.viewport) {
						map.fitBounds(place.geometry.viewport);
					} else {
						map.setCenter(place.geometry.location);
						map.setZoom(17);  // Why 17? Because it looks good.
					}
					marker.setPosition(place.geometry.location);
					marker.setVisible(true);

					// Update Location Co-ordinates Values
					$('#latitude').val(place.geometry.location.lat());
					$('#longitude').val(place.geometry.location.lng());

					var address = '';
					if (place.address_components) {
						address = [
							(place.address_components[0] && place.address_components[0].short_name || ''),
							(place.address_components[1] && place.address_components[1].short_name || ''),
							(place.address_components[2] && place.address_components[2].short_name || '')
						].join(' ');

						// Get each component of the address from the place details
						// and fill the corresponding field on the form.
						for (var i = 0; i < place.address_components.length; i++) {
							var addressType = place.address_components[i].types[0];
							if (componentForm[addressType]) {
								var val = place.address_components[i][componentForm[addressType]];
								if(typeof componentIds[addressType] != 'undefined'){
									document.getElementById(componentIds[addressType]).value = val;
								}
							}
						}
					}

					infowindowContent.children['place-icon'].src = place.icon;
					infowindowContent.children['place-name'].textContent = place.name;
					infowindowContent.children['place-address'].textContent = address;
					infowindow.open(map, marker);
				});
			}


			initDateRangePicker();

			$('.content').on('change','#referral_category', function(){
				var val = $(this).val();
				var sourcesHtml = '<option value="">-- Select --</option>';
				var sources = sourcesList.filter(function(item){ return item.key === val; });

				if(sources.length > 0){
					for(key in sources){
						if(sources.hasOwnProperty(key)){
							sourcesHtml += '<option value="'+sources[key].id+'">'+sources[key].value+' - '+ sources[key].phone+'</option>';
						}
					}
					$('#referral_source').html(sourcesHtml);
					$('#referral_source').prop('disabled',false);
					$('#referral_source').selectpicker('refresh');
				}
			});

			var sourcesList = [];
			var percentagesList = [];
			@if(isset($sources))
			@foreach ($sources as $s)
			sourcesList.push({id: '{{ $s->id }}', key: '{{ $s->category_id }}', value:'{{ $s->source_name }}', phone: '{{ $s->phone_number }}', percentage: '{{ $s->referral_value }}'});
			percentagesList.push({id: '{{ $s->id }}', percentage: '{{ $s->referral_value }}', type: '{{ $s->charge_type }}' });
			@endforeach
			@endif

			$('.content').on('change','#referral_source', function(){
				var val = $(this).val();
				var result = $.grep(percentagesList, function(e){ return e.id == val; });
				$('#referral_value').val(result[0].percentage);
				$('#referral_type').val(result[0].type);
			});

			// Fill case details to edit modal
			@if(isset($l))
			$('#editCaseModal #referral_category').selectpicker('val','{{ $l->referral_category }}');
			$('#editCaseModal #referral_category').selectpicker('refresh');
			$('#editCaseModal #referral_category').trigger("change");

			$('#editCaseModal #referral_source').selectpicker('val','{{ $l->referral_source }}');
			$('#editCaseModal #referral_source').selectpicker('refresh');

			$('#editCaseModal #assessment_required').prop('checked','{{ (!empty($l->assessment_date) || $l->assessment_date != null)?true:false }}').trigger("change");

			$('#medical_conditions').tagsinput({
				typeahead: {
					minLength: 0,
					showHintOnFocus:"all",
					allowDuplicates: true,
				}
			});

			$('#medications').tagsinput({
				typeahead: {
					minLength: 0,
					showHintOnFocus:"all",
					allowDuplicates: true,
				}
			});

			$('#procedures').tagsinput({
				typeahead: {
					minLength: 0,
					showHintOnFocus:"all",
					allowDuplicates: true,
				}
			});

			var api_url = 'https://clin-table-search.lhc.nlm.nih.gov/api/';
			var conditionsArray = [];
			if($('#procedures_list').length){
				new Def.Autocompleter.Search('conditions', conditionsArray, {maxSelect: '*'});
				var searchConditions = new Def.Autocompleter.Search('conditions',api_url+'conditions/v3/search');

				searchConditions.options.onShow = function(d){
					$('#searchResults').attr('style', 'visibility:visible;color:green;top:302px !important');
				}

				searchConditions.onMouseDownListener = function(d){
					$('#medical_conditions').tagsinput('add',d.currentTarget.innerHTML);
				}
			}

			var medicationsArray = [];
			if($('#procedures_list').length){
				new Def.Autocompleter.Search('medications_list', medicationsArray, {maxSelect: '*'});
				var searchMedications = new Def.Autocompleter.Search('medications_list',api_url+'rxterms/v3/search?ef=STRENGTHS_AND_FORMS');

				searchMedications.onMouseDownListener = function(d){
					$('#medications').tagsinput('add',d.currentTarget.innerHTML);
				}
			}

			var proceduresArray = [];
			if($('#procedures_list').length){
				new Def.Autocompleter.Search('procedures_list', proceduresArray, {maxSelect: '*'});
				var searchProcedures = new Def.Autocompleter.Search('procedures_list',api_url+'procedures/v3/search');
				searchProcedures.onMouseDownListener = function(d){
					$('#procedures').tagsinput('add',d.currentTarget.innerHTML);
				}
			}

			$('.autocomp_selected ul').hide();
			if($('#medicine_name').length){
				new Def.Autocompleter.Search('medicine_name',api_url+'rxterms/v3/search?ef=STRENGTHS_AND_FORMS');
			}
			@endif

			@if($l->medical_conditions)
			$('#editCaseModal #medical_conditions').removeAttr('placeholder');
			@foreach(explode(",",$l->medical_conditions) as $mc)
			$('#editCaseModal #medical_conditions').tagsinput('add','{{ $mc }}');
			@endforeach
			@endif

			@if($l->medications)
			$('#editCaseModal #medications').removeAttr('placeholder');
			@foreach(explode(",",$l->medications) as $m)
			$('#editCaseModal #medications').tagsinput('add','{{ $m }}');
			@endforeach
			@endif

			@if($l->procedures)
			$('#editCaseModal #procedures').removeAttr('placeholder');
			@foreach(explode(",",$l->procedures) as $p)
			$('#editCaseModal #procedures').tagsinput('add','{{ $p }}');
			@endforeach
			@endif
			$('#editPatientModal #relationship_with_patient').val('{{ $l->patient->relationship_with_patient }}').change();
		});
	</script>
	<script>
		function checkSimpleAssessment(){
			var x = document.forms["clear_form1"]["schedule_details"].value;
		    if (x == "") {
		        alert("Please select a Manger for Assessment");
		        return false;
		    }
		}

		function editassessment(element,status) {
		    var str = ``;
		    document.getElementById("case_forms_id").value = element;
		    str=$.parseHTML(nurseForm);
		    $('.forms_editassessment').html( str );
		    $('.forms_editassessment #assessment_date_nursing').datetimepicker({
		        format: 'DD-MM-YYYY',
		        showClear: true,
		        showClose: true,
		        useCurrent: false,
		        keepInvalid:true
		    });

		    var form_data;
			$( ".assessmentId_"+element).each(function() {
				form_data = $( this ).attr("data-form-data");
			});
			var formData = JSON.parse(form_data);
			$('.getdate').val(formData.assessment_date);
			$('input[name="assessment_method[]"][value="'+formData.assessment_method+'"]').prop('checked', true);
			$('input[name="patient_mobility[]"][value="'+formData.patient_mobility+'"]').prop('checked', true);
			$('input[name="patient_consciousness[]"][value="'+formData.patient_consciousness+'"]').prop('checked', true);
			var med_pro = formData.medical_procedures;
			$.each(med_pro, function(i){
				$('input[name="medical_procedures[]"][value="'+med_pro[i]+'"]').prop('checked', true);
			});
			$('input[name="suggested_home_care_professional"][value="'+formData.suggested_professional+'"]').prop('checked', true);
			$('input[name="assessment_status"][value="'+status+'"]').prop('checked','checked');
			$('textarea[name="notes"]').val(formData.comment);
			document.getElementById("assessor_name").value = formData.assessor_name;

			$('#editSimpleAssessmentModal').on('hidden.bs.modal', function (e) {
				document.getElementById("clear_form1").reset();
				document.getElementById("clear_form2").reset();
			});
		}
	</script>
	<script type="text/javascript">
		$(function(){
			$(document).ready(function() {
				$("div.vertical-tab-menu>div.list-group>a").click(function(e) {
					e.preventDefault();
					$(this).siblings('a.active').removeClass("active");
					$(this).addClass("active");
					var index = $(this).index();
					$("div.vertical-tab>div.vertical-tab-content").removeClass("active");
					$("div.vertical-tab>div.vertical-tab-content").eq(index).addClass("active");
				});

				$('form').append(`<input type="hidden" name="lead_id" id="lead_id" value="{{ Helper::encryptor('encrypt',$l->id) }}">
				<input type="hidden" name="patient_id" id="patient_id" value="{{ Helper::encryptor('encrypt',$l->patient->id) }}">`);
			});

			$('.vertical-tab-menu .list-group > a').on('click', function (e) {
				// save the latest tab; use cookies if you like 'em better:
				localStorage.setItem('leadVerticalLastTab', $(this).index());
			});

			// go to the latest tab, if it exists:
			var lastTab = localStorage.getItem('leadVerticalLastTab');
			if (lastTab >= 0) {
				selectedItem = $('.vertical-tab-menu .list-group > a:eq('+ lastTab +')');
				$(selectedItem).siblings('a.active').removeClass("active");
				$(selectedItem).addClass("active");
				$("div.vertical-tab>div.vertical-tab-content").removeClass("active");
				$("div.vertical-tab>div.vertical-tab-content").eq(lastTab).addClass("active");
			}

			$('.content').on('click','.clearLocalStore', function() {
				return localStorage.removeItem('leadVerticalLastTab');
			});
		});
	</script>

	<script type="text/javascript">
		$(document).ready(function() {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': '{{ csrf_token() }}'//$('meta[name="csrf-token"]').attr('content')
				}
			});

			$('.content').on('change','#case_vendor', function(){
				// console.log($(this).val());
				if($(this).val() != null){
					$('#vendor_id').val($(this).val());
				}else{
					$('#vendor_id').val('');
				}
			});

			var dataTable = {
				"order": [[ 1, 'asc' ]],
				"displayLength": 50,
				"drawCallback": function ( settings ) {
					var api = this.api();
					var rows = api.rows( {page:'current'} ).nodes();
					var last = null;

					api.column(1, {page:'current'} ).data().each( function ( group, i ) {
						if(group != null){
							if ( last !== group ) {
								$(rows).eq( i ).before(
									'<tr class="group"><td colspan="6"><b>'+group.toUpperCase()+'</b></td></tr>'
								);
								last = group;
							}
						}
					});
				}
			};
		});

		$(function(){
			$('#editCaseModal #source').selectpicker('val','{{ isset($l->source)?$l->source:'' }}');

			$('#feedback_time').bootstrapMaterialDatePicker({
				format: 'DD-MM-YYYY - hh:mm A',
				time: true,
				clearButton: true,
			});
			$('.getdate').bootstrapMaterialDatePicker({
				format: 'DD-MM-YYYY',
				time: false,
				clearButton: true,
			});
			fpDate = $('.datepicker').flatpickr({
			    altInput: true,
			    allowInput:true,
			    altFormat: "d-m-Y",
			    dateFormat: "Y-m-d",
			    onChange: function(selectedDates, dateStr, instance) {
			        if(instance.element.id == 'cancel_from_date'){
			            fpDate[1].set('minDate',new Date($('#cancel_from_date').val()));
			            fpDate[1].setDate(new Date($('#cancel_from_date').val()));

			            fpDate[2].set('minDate',new Date($('#cancel_from_date').val()));
			            fpDate[2].setDate(new Date(new Date($('#cancel_from_date').val())));
			        }
			    },
			});
		});

		$('.content').on('click','.btnsaveFeedbackComment',function(){
			var feedbackTime    = $('#feedback_time').val();
			var feedbackComment = $('#feedback_comment').val();
			var careplanID      = $('#careplan_id').val();

			if(feedbackTime && feedbackComment){
				$.ajax({
					url : '{{ route('case.save-feedback-comment') }}',
					type: 'POST',
					data: {feedback_time: feedbackTime, feedback_comment: feedbackComment, careplan_id: careplanID},
					success: function (data){
						window.location.reload();
					},
					error: function (error){
						console.log(error);
					}
				});
			}
		});

		function checkCaseLoss(optionValue)
		{
			if(optionValue){
				if(optionValue == 'Dropped'){
					document.getElementById("dropped").style.display = "block";
				}
				else{
					document.getElementById("dropped").style.display = "none";
				}

				if(optionValue == 'Follow Up'){
					document.getElementById("followup").style.display = "block";
				}
				else{
					document.getElementById("followup").style.display = "none";
				}
			}
			else{
				document.getElementById("dropped").style.display = "none";
				document.getElementById("followup").style.display = "none";
			}
		}

		function printValue(value) {
			if(typeof value != 'undefined'){
				return value;
			}
			return '-';
		}

		function formatKey(str){
			if(str){
				str = str.split("_").join(" ");
			}
			return str.toUpperCaseWords();
		}

		function viewAssessment(element)
		{
			var form_data;
			$( ".assessmentId_"+element).each(function() {
				form_data = $( this ).attr("data-form-data");
			});

			var data = JSON.parse(form_data);
			var html = `<tbody>`;

			var blockedVariables = ['careplan_id','schedule_id','assessform'];
			$.each(data, function(key, data) {
				//alert(key + ' is ' + data);
				if(!blockedVariables.contains(key)){
					html += `<tr>
					<th width="20%">`+formatKey(key)+`</th>
					<td>`+data+`</td>
					</tr>`;
				}
			});

			html += `</tbody>`;
			$('#viewassessmentModal .modal-body #viewAssessmentTable').html(html);
		}

		String.prototype.toUpperCaseWords = function () {
			return this.replace(/\w+/g, function(a){
				return a.charAt(0).toUpperCase() + a.slice(1).toLowerCase()
			})
		}

		Array.prototype.contains = function(obj) {
			var i = this.length;
			while (i--) {
				if (this[i] === obj) {
					return true;
				}
			}
			return false;
		}

		$(function(){
			$('.content').on('click', '.btnSelectProvider', function(){
				var id = $(this).attr('data-visit-id');

				var provider = $('input[name=choosen_provider]:checked').val();
				var name = $('input[name=choosen_provider]:checked').data('name');
				var location = $('input[name=choosen_provider]:checked').data('location');
				var phone = $('input[name=choosen_provider]:checked').data('phone');
				var logo = $('input[name=choosen_provider]:checked').data('logo');

				if(typeof provider != 'undefined' && provider != ''){
					$('#providerSearchModal').modal('hide');
					$('#editCaseModal #provider_id').val(provider);
					$('#editCaseModal #selectedProviderName').html('<div class="col-sm-3"><img src="{{ asset('img/provider') }}/'+logo+'" class="img-responsive" style="width: 100px"></div><div class="col-sm-8"><b>'+name+'</b><br>'+location+'<br>Phone: '+phone+'</div>');
				}else{
					alert("Please select provider !");
				}
			});

			$('.content').on('click','.toggle-row-view', function(){
				var id = $(this).data('row');
				// console.log(id);
				$('.providerSearchResults tr#'+id).toggle();
			});

			$('.content').on('click','.btnAddWorklog', function(){
				$('#addWorklogModal table tr td > input').val('');
				$('#addWorklogModal #worklog_date').selectpicker('val','');
				$('#addWorklogModal .loading-img').hide();
				$('#addWorklogModal .worklog-block').hide();
				$('#addWorklogModal .tasks-block').html('');
				$('#addWorklogModal .tasks-block').removeClass('multi-col-worklog');
				$('#addWorklogModal .tasks-block').html('<center>No task(s) assigned.</center>');
				$('#addWorklogModal .btnSaveWorklog').prop('disabled',true);

				$('#addWorklogModal').modal();
			});

			$('.content').on('change','.worklog-task', function(){
				id = $(this).data('id');
				$('#addWorklogModal .tasks-block #worklog_task_'+ id +'_time').prop('disabled', !$(this).is(':checked'));
				$('#addWorklogModal .tasks-block #worklog_task_'+ id +'_time').val('');
				if($(this).is(':checked') && $(this).data('date') != ''){
					$('#addWorklogModal .tasks-block #worklog_task_'+ id +'_time').val($(this).data('date'));
				}
			});

			$('.content').on('click','.btnGetWorklog', function(){
				var worklogDate = $('#addWorklogModal #worklog_date option:selected').val();
				if(worklogDate == ''){
					alert("Please select date!");
					return false;
				}
				$('#addWorklogModal .loading-img').show();
				$('#addWorklogModal .worklog-block').hide();

				$.ajax({
					url: '{{ route('ajax.get-worklog-by-date') }}',
					type: 'POST',
					data: {_token: '{{ csrf_token() }}', lead_id: $('#lead_id').val(), worklog_date: worklogDate},
					success: function (d){
						//console.log(d);
						var tasksHtml = ``;
						$('#addWorklogModal table tr td > input').val('');
						if(d.result){
							if(d.caregiver_id && d.caregiver_name){
								$('#addWorklogModal #worklog_caregiver_id').val(d.caregiver_id);
								$('#addWorklogModal #worklog_caregiver').html(d.caregiver_name);
							}
							if(d.schedule_id){
								$('#addWorklogModal #worklog_schedule_id').val(d.schedule_id);
							}

							// Get assigned tasks
							if(d.tasks){
								var tasksJson = $.parseJSON(d.tasks);
								if(tasksJson.length){
									$(tasksJson).each(function(i){
										tasksHtml += `<input type="checkbox" id="worklog_task_`+ tasksJson[i].id +`" name="worklog_tasks[]" data-id="`+ tasksJson[i].id +`" data-task="`+ tasksJson[i].task +`" class="filled-in chk-col-indigo worklog-task" />
										<label for="worklog_task_`+ tasksJson[i].id +`" title="`+ tasksJson[i].task +`" style="margin-right: 2px">`+ tasksJson[i].task +`</label>
										<input type="text" style="width:35%; display:inline-block !important" class="form-control input-sm worklog-time" id="worklog_task_`+ tasksJson[i].id +`_time" placeholder="Time" /><br>`;
									});
									$('#addWorklogModal .tasks-block').html(tasksHtml);
									$('.worklog-time').datetimepicker({
										format: 'hh:mm A',
										showClose: true,
										keepOpen: false
									});
								}
							}

							// Get worklog data, if any
							if(d.worklog){
								var vitalsJson = $.parseJSON(d.worklog.vitals);
								// console.log(vitalsJson);
								if(vitalsJson.length > 0){
									vitalsJson = vitalsJson[0];
									var keys = Object.keys(vitalsJson);
									//  console.log(keys);
									if(keys.length){
										$(keys).each(function(i){
											if(vitalsJson[keys[i]]){
												var vitals = Object.keys(vitalsJson[keys[i]]);
												// console.log(vitals);
												if(vitals.length){
													$(vitals).each(function(j){

														if($('#addWorklogModal #worklog_'+vitals[j]+'_'+keys[i]).length){
															$('#addWorklogModal #worklog_'+vitals[j]+'_'+keys[i]).val(vitalsJson[keys[i]][vitals[j]]);
														}
													});
												}
											}
										});
									}
								}

								var routinesJson = $.parseJSON(d.worklog.routines);
								if(routinesJson.length > 0 && $('#addWorklogModal .tasks-block .worklog-task').length > 0){
									$('#addWorklogModal .tasks-block').addClass('multi-col-worklog');
									$(routinesJson).each(function(i){
										var keys = Object.keys(routinesJson[i]);
										if(keys.length){
											$(keys).each(function(j){
												var chk = '#addWorklogModal .tasks-block input.worklog-task[data-task="'+ keys[j] +'"]';
												if($(chk).length > 0){
													$(chk).prop('checked', true);
													$('#addWorklogModal .tasks-block #worklog_task_'+ $(chk).data('id') +'_time').val(routinesJson[i][keys[j]]);
													$(chk).data('date', routinesJson[i][keys[j]]);
												}
											});
										}
									});
								} else {
									$('#addWorklogModal .tasks-block').removeClass('multi-col-worklog');
									$('#addWorklogModal .tasks-block').html('<center>No task(s) assigned.</center>');
								}
							}
						} else {
							$('#addWorklogModal .tasks-block').html(tasksHtml);
							$('#addWorklogModal .tasks-block').removeClass('multi-col-worklog');
							$('#addWorklogModal .tasks-block').html('<center>No task(s) assigned.</center>');
						}

						$('#addWorklogModal .loading-img').hide();
						$('#addWorklogModal .worklog-block').show();
						$('#addWorklogModal .btnSaveWorklog').prop('disabled',false);
					},
					error: function (err){
						console.log(err);
					}
				});
			});

			$('.content').on('click','.btnSaveWorklog', function(){
				var vitalsJson = "";
				var routinesJson = "";
				var caregiverId = null;

				// Get Vitals information
				var sessions = ["morning","afternoon","evening","night"];
				var vitals = ["blood_pressure","sugar_level","temperature","pulse_rate"];
				var vitalsArr = {};
				var vitalsArray = [];

				$(sessions).each(function(i){
					var temp = {};
					vitalsArr[sessions[i]] = "";
					$(vitals).each(function(j){
						var block = '#addWorklogModal #worklog_'+vitals[j]+'_'+sessions[i];
						if(typeof $(block).val() != 'undefined' && $(block).val() != ''){
							temp[vitals[j]] = $(block).val();
						}
					});
					if(Object.keys(temp).length)
					vitalsArr[sessions[i]] = temp;
				});
				if(Object.keys(vitalsArr).length)
				vitalsArray.push(vitalsArr);

				// Get Tasks information
				var tasksArray = {};
				var routinesArray = [];
				$('#addWorklogModal .worklog-task').each(function(i){
					if($(this).is(':checked')){
						id = $(this).data('id');
						task = $(this).data('task');
						time = $('#addWorklogModal #worklog_task_'+id+'_time').val();
						tasksArray[task] = time;
					}
				});
				if(Object.keys(tasksArray).length)
				routinesArray.push(tasksArray);

				emptyInputs = $("#addWorklogModal .table tr td input").filter(function () {
					return $.trim($(this).val()).length == 0
				}).length;

				if(Object.keys(vitalsArray).length && emptyInputs < $('#addWorklogModal .table tr td input').length){
					vitalsJson = JSON.stringify(vitalsArray);
				} else{
					alert("Please enter atleast one vital entry!");
					return false;
				}

				if(Object.keys(routinesArray).length) routinesJson = JSON.stringify(routinesArray);

				leadId = $('#lead_id').val();
				scheduleId = $('#addWorklogModal #worklog_schedule_id').val();
				caregiverId = $('#addWorklogModal #worklog_caregiver_id').val();
				worklogDate = $('#addWorklogModal #worklog_date option:selected').val();

				if(confirm("Are you sure ?")) {
					$('#addWorklogModal input').prop('disabled', true);
					$('#addWorklogModal .btnGetWorklog').hide();
					$('#addWorklogModal .btnSaveWorklog').hide();
					$.ajax({
						url: '{{ route('lead.save-worklog-by-date') }}',
						type: 'POST',
						data: {'lead_id': leadId, 'schedule_id': scheduleId, 'caregiver_id': caregiverId, 'worklog_date': worklogDate, 'vitals': vitalsJson, 'routines': routinesJson},
						success: function(d) {
							// console.log(d);
							location.reload();
						},
						error: function (err){
							console.log(err);
							$('#addWorklogModal .btnSaveWorklog').show();
							alert("Unable to save worklog!");
							return;
						}
					});
				}
			});
		});

		//CODING FOR WORKLOG STARTS
		function viewWorklog(element){
			var vitals = $(element).data("vitals");
			var routines = $(element).data("routines");

			var vitalsData =  $.parseJSON(JSON.stringify(vitals));
			var routinesData =  $.parseJSON(JSON.stringify(routines));
			var html = `<tbody>`;

			if(vitalsData[0]){
				html += `<tr><th colspan="5"><center>Vitals Information</center></th></tr>`;

				html += `<tr>
				<th style="text-align:center;">Vitals</th>
				<th style="text-align:center;">Morning</th>
				<th style="text-align:center;">Noon</th>
				<th style="text-align:center;">Evening</th>
				<th style="text-align:center;">Night</th>
				</tr>`;

				// Fetch Blood Pressure Information
				html += `<tr>
				<th style="vertical-align: middle;" >Blood Pressure</th>`;
				if(vitalsData[0]['morning'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['morning'], function(k, v){
						if( typeof(v['blood_pressure']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['blood_pressure']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				if(vitalsData[0]['afternoon'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['afternoon'], function(k, v){
						if( typeof(v['blood_pressure']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['blood_pressure']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				if(vitalsData[0]['evening'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['evening'], function(k, v){
						if( typeof(v['blood_pressure']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['blood_pressure']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				if(vitalsData[0]['night'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['night'], function(k, v){
						if( typeof(v['blood_pressure']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['blood_pressure']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				html += `</tr>`;

				// Fetch Sugar Level Information
				html += `<tr>
				<th style="vertical-align: middle;" >Sugar Level</th>`;
				if(vitalsData[0]['morning'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['morning'], function(k, v){
						if( typeof(v['sugar_level']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['sugar_level']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				if(vitalsData[0]['afternoon'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['afternoon'], function(k, v){
						if( typeof(v['sugar_level']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['sugar_level']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				if(vitalsData[0]['evening'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['evening'], function(k, v){
						if( typeof(v['sugar_level']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['sugar_level']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				if(vitalsData[0]['night'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['night'], function(k, v){
						if( typeof(v['sugar_level']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['sugar_level']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				html += `</tr>`;

				// Fetch Temperature Information
				html += `<tr>
				<th style="vertical-align: middle;" >Temperature</th>`;
				if(vitalsData[0]['morning'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['morning'], function(k, v){
						if( typeof(v['temperature']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['temperature']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				if(vitalsData[0]['afternoon'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['afternoon'], function(k, v){
						if( typeof(v['temperature']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['temperature']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				if(vitalsData[0]['evening'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['evening'], function(k, v){
						if( typeof(v['temperature']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['temperature']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				if(vitalsData[0]['night'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['night'], function(k, v){
						if( typeof(v['temperature']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['temperature']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				html += `</tr>`;

				// Fetch Pulse Rate Information
				html += `<tr>
				<th style="vertical-align: middle;" >Pulse Rate</th>`;
				if(vitalsData[0]['morning'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['morning'], function(k, v){
						if( typeof(v['pulse_rate']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['pulse_rate']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				if(vitalsData[0]['afternoon'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['afternoon'], function(k, v){
						if( typeof(v['pulse_rate']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['pulse_rate']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				if(vitalsData[0]['evening'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['evening'], function(k, v){
						if( typeof(v['pulse_rate']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['pulse_rate']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				if(vitalsData[0]['night'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['night'], function(k, v){
						if( typeof(v['pulse_rate']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['pulse_rate']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				html += `</tr>`;
			}
			else if(vitalsData[0] == null || vitalsData[0] == undefined){
				html += `<tr> <th style="text-align:center;"> NO VITALS ADDED </th> </tr>`;
			}

			html += `</tbody>`;
			$('#viewWorklogModal .modal-body #viewWorklogTableVitals').html(html);

			var htmlRoutines = `<tbody>`;

			if(routinesData.length){
				htmlRoutines += `<tr><th colspan="100%" style="text-align:center;">Tasks</th></tr>`;
				htmlRoutines += `<tr>`;
				if(routinesData[0] != null){
					$.each(routinesData[0], function(k, v) {
							htmlRoutines += `<tr><td>`+printValue('<b>'+k+ '</b>')+`</td><td>`+printValue('<b>'+v+'</b>')+`</td></tr>`;
					});
					htmlRoutines += `</tr>`;
				}else{
					htmlRoutines += `<td>-</td>`;
					htmlRoutines += `</tr>`;
				}
			}else{
				htmlRoutines += `<tr> <th style="text-align:center;"> NO ROUTINES ADDED </th> </tr>`;
			}

			htmlRoutines += `</tbody>`;
			$('#viewWorklogModal .modal-body #viewWorklogTableRoutines').html(htmlRoutines);
		}
		//CODING FOR WORKLOG ENDS
	</script>

	<script type="text/javascript">
		$(function(){
			initBlock();
			/*  New Code Block Starts */
			$('.content').on('click','.btnCloseServiceRequestBlock, .btnResetServiceRequest', function(){
				$('#service-request-block').toggle();
				$('#serviceRequestForm').trigger("reset");
				$('#serviceRequestForm select').selectpicker('val','');
				$('#serviceRequestForm code span').html('');
				$('#serviceRequestForm #gross_rate').val('');
				$('#serviceRequestForm #service_request_amount').html('');
				$('.date').data('DateTimePicker').clear();
				$('#serviceRequestForm #frequency').selectpicker('val','Daily');
				$('html, body').stop().animate({
					'scrollTop': $('#service-request-block').offset().top-20
				}, 700, 'swing');
			});

			$('.content').on('click','.btnAddSchedule, .btnCloseScheduleBlock, .btnResetServiceSchedule', function(){
				$('#service-schedule-block').toggle();
				$('html, body').stop().animate({
					'scrollTop': $('#service-schedule-block').offset().top-20
				}, 700, 'swing', function () {
					//window.location.hash = $('#service-schedule-block');
				});
			});

			$('.content').on('click','.btnSelectTasks', function(e){
				e.preventDefault();
				var assignedTasks = [];
				var assignedTasksString = [];
				$('#assignTasksModal input[type=checkbox]:checked').each(function(i){
					assignedTasks.push({id: $(this).data('id'), task: $(this).data('task')});
					assignedTasksString.push($(this).data('task'));
				});
				if(assignedTasks.length > 0){
					$('#serviceRequestForm input[name="assigned_tasks"]').val(JSON.stringify(assignedTasks).toString());
					$('#serviceRequestForm #selectedTasks').html(assignedTasksString.join(", ").toString());
					$('#assignTasksModal').modal('hide');
				}else{
					alert("Please select atleast 1 task!");
					return;
				}
			});

			$('.content').on('click','.btnViewSchedules', function(){
				$('.loading-img').show();
				var data = $(this).data();
				var html = ``;
				if($.fn.DataTable.isDataTable('#viewSchedulesModal .table-schedules')){
					$('#DataTables_Table_0').dataTable().fnDestroy();
				}
				$('#viewSchedulesModal .table-schedules tbody').html(html);
				$('#createSchedulesForm .addScheduleBlock').hide();
				$('#viewSchedulesModal .modal-title').html('View Schedules - '+data.serviceName);
				$('#viewSchedulesModal .modal-body input[name=service_request_id]').val(data.serviceRequestId);
				$('#viewSchedulesModal .modal-body input[name=service_required]').val(data.serviceRequired);
				$('#viewSchedulesModal .modal-body input[name=service_rate]').val(data.serviceRate);

				if(data.status == 'Cancelled'){
					$('#createSchedulesForm .btnAddScheduleFromService').hide();
				}else{
					$('#createSchedulesForm .btnAddScheduleFromService').show();
				}

				if(data.serviceRequestId && data.leadId){
					setTimeout(function(){
						$.ajax({
							url: '{{ route('lead.get-schedules-from-service-request') }}',
							type: 'POST',
							data: {data,_token: '{{ csrf_token() }}'},
							success: function(d){
								if(d[1] == 'Custom'){
									$('.btnAddScheduleFromService').hide();
									$('#extensionWarning').css("display","block");
								}else{
									$('.btnAddScheduleFromService').show();
									$('#extensionWarning').css("display","none");
								}
								if(d[0].length){
									scheduledArr = [];
								    $(d[0]).each(function(i){
								    		scheduledArr.push(d[0][i]['schedule_date']);
								    });

									html = ``;
									$(d[0]).each(function(i){
										statusHtml = ``;
										assignTasksHtml = ``;
										settingsHtml = ``;
										if(d[0][i].status != 'Cancelled' && d[0][i].status != 'Completed'){
											$('#viewSchedulesModal .table-schedules thead th:eq(6)').css('width','15%');
											statusHtml = `
											<div class="btn-group chargeability" style="position: relative; display: block !important; float: left;">
											<button type="button" class="btn btn-info waves-effect current-schedule-status" data-id="`+d[0][i].id+`">`+d[0][i].status+`</button>
											<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
											<span class="caret caret-right"></span>
											<span class="sr-only">Toggle Dropdown</span>
											</button>`;

											if(d[0][i].changeability == 'Y'){
												statusHtml +=
												`<ul class="dropdown-menu" style="position: absolute;z-index: 99999;left:110%">
												<li><a href="javascript:void(0);" data-status="Completed" data-lead="`+d[0][i].lead_id+`" data-id="`+d[0][i].id+`" class="schedule-status waves-effect waves-block">Completed</a></li>
												</ul>`;
											}

											statusHtml += `</div>`;

											assignTasksHtml += `&nbsp;&nbsp;&nbsp;&nbsp;<a class="btn btn-primary btnAssignTasks" data-service-id="`+ d[0][i].service_request_id +`" data-id="`+ d[0][i].id +`" data-schedule-date="`+ d[0][i].schedule_date +`" data-assigned-tasks='`+ JSON.stringify(d[0][i].assigned_tasks).replace(/'/g, "\\'") +`'>Assign Tasks</a>`;
										}else{
											statusHtml += d[0][i].status;
										}

										chargeable = d[0][i].chargeable == 1?'Chargeable':'Non-Chargeable';
										if(d[0][i].chargeable == 1){
										settingsHtml = `&nbsp;&nbsp;&nbsp;
										<div class="btn-group chargeability" style="position: relative; display: block !important; float: left;">
											<button type="button" class="btn bg-indigo waves-effect current-schedule-chargeable" data-id="`+d[0][i].id+`" style="width:115px" title="Schedule Chargeable Setting">`+ chargeable +`</button>
											<button type="button" class="btn bg-indigo dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
												<span class="caret caret-right"></span>
												<span class="sr-only">Toggle Dropdown</span>
											</button>
											<ul class="dropdown-menu" style="position: absolute;z-index: 99999;left:110%">
												<li style="background:#f9f5e9;border-bottom:1px solid #ccc"><a href="javascript:void(0);" data-value="1" data-lead="`+d[0][i].lead_id+`" data-id="`+d[0][i].id+`" class="schedule-chargeable waves-effect waves-block">Yes</a></li>
												<li style="background:#f9f5e9"><a href="javascript:void(0);" data-value="0" data-lead="`+d[0][i].lead_id+`" data-id="`+d[0][i].id+`" class="schedule-chargeable waves-effect waves-block">No</a></li>
											</ul>
										</div>`;
										}else{
											settingsHtml = `Non Chargeable`;
										}

										html += `
										<tr data-id=`+ d[0][i].id +`>
										<td class="text-center schedule-sel">
										<input id="schedule_`+ d[0][i].id +`" type="checkbox" class="with-gap chk-col-blue filled-in schedule-sel-chk" data-date="`+d[0][i].date+`" data-status="`+d[0][i].status+`" data-delivery-address="`+d[0][i].delivery_address_id+`" value="`+d[0][i].id+`"/><label for="schedule_`+ d[0][i].id +`">&nbsp;</label>
										</td>
										<td>`+ (i+1) +`</td>
										<td style="min-width:50px !important;">`+ d[0][i].schedule_date +`</td>
										<td style="min-width:50px !important;">`+ d[0][i].schedule_no +`</td>
										<td style="min-width:50px !important;">`+ d[0][i].timings+`</td>
										<td>`+ d[0][i].employee_id +`</td>
										<td>`+ d[0][i].employee_name +`</td>
										<td>`+ d[0][i].employee_phone +`</td>
										<td style="text-align:center;" title="`+ d[0][i].delivery_location +`"><img src="{{ asset('img/icons/location.png') }}" style="height: 30px;width: 30px;"></td>`;

										html += `<td>`+ statusHtml +`</td>
										<td style="text-align:right;">
										`+ assignTasksHtml +`
										`+ settingsHtml +`</td><td>`+ d[0][i].amount +`</td>
										</tr>`;
									});
								}
								else{
									html = `<tr><td colspan="11" class="text-center">No schedule(s) found.</td></tr>`;
									$('#viewSchedulesModal .table-schedules thead th:eq(6)').css('width','auto');
								}

								$('label[for=from_rate_card]').html('RateCard Price - <b style="color:navy">'+d[2]['ratecardAmount']+' /-</b>');
								$('#from_rate_card').val(d[2]['ratecardAmount']);

								if(d[0].length != 0){
									$('label[for=from_prev_schedule]').html('Last Schedule Price - <b style="color:green">'+d[2]['lastScheduleAmount']+' /-</b>');
									$('#from_prev_schedule').val(d[2]['lastScheduleAmount']);
									$('label[for="from_prev_schedule"]').show();
								}else{
									$('label[for="from_prev_schedule"]').hide();
								}

								if($.fn.DataTable.isDataTable('#viewSchedulesModal .table-schedules')){
									$('#DataTables_Table_0').dataTable().fnDestroy();
								}

								$('#viewSchedulesModal .table-schedules tbody').html(html);

								if(d[0].length){
									$('#viewSchedulesModal .table-schedules').dataTable({
										"dom": '<"top"lpf>t<"bottom"><"clear">',
										"scrollY": '50vh',
										"scrollCollapse": true,
										"paging": false,
										"columnDefs": [
											{ "width"    : "auto" , "targets": 8},
											{ "orderable": false, "targets": 0 }
										 ]
									});
								}
								$('.loading-img').hide();
								// setTimeout(function(){ $('.loading-img').hide(); },1000);
							},
							error: function(d, err){
								console.log(err);
								$('.loading-img').hide();
							}
						});
					},1000);
				}
				else {
					$('#viewSchedulesModal .table-schedules tbody').html(html);
				}

				$('#viewSchedulesModal').modal();
			});

			$('.content').on('click','.btnScheduleAction', function(e){
				e.preventDefault();

				if($(this).data('mode') == 'Schedule'){
					if($('#createSchedulesForm .addScheduleBlock input[name=create_schedule_period_from]').val() == '' || $('#createSchedulesForm .addScheduleBlock input[name=create_schedule_period_to]').val() == ''){
						alert("Please select period!");
						return false;
					}
				}
				if($('#create_schedule_start_time').val() == '' || $('#create_schedule_start_time').val() == ''){
					alert("Please select time interval!");
					return false;
				}

				var emplat = $('input[name=choosen_emp]:checked').data('emplat');
				var emplng = $('input[name=choosen_emp]:checked').data('emplng');
				var patlat = $('input[name=choosen_emp]:checked').data('patlat');
				var patlng = $('input[name=choosen_emp]:checked').data('patlng');

				if(emplat!=0 && emplng!=0 && patlat!=0 && patlng!=0) {
					if(typeof(emplat)!='undefined' && typeof(emplng)!='undefined' && typeof(patlat)!='undefined' && typeof(patlng)!='undefined') {
						emplat = emplat.toString().split(" ").join("");
						emplng = emplng.toString().split(" ").join("");
						patlat = patlat.toString().split(" ").join("");
						patlng = patlng.toString().split(" ").join("");
					getMapMyIndia('https://apis.mapmyindia.com/advancedmaps/v1/89oegcgbhdv1il54kcry8hrlfwet71kf/distance?center='+emplat+','+emplng+'|&pts='+patlat+','+patlng+'');
					} else { 
						$(".road_distance").val(''); 
					}
				} else {
					$(".road_distance").val('');
				}

				$(this).attr('disabled', true);
				$('#createSchedulesForm').submit();
			});

			$('#viewSchedulesModal').on('hidden.bs.modal', function (e) {
				$(this).find("input.daterange").val('');
				$(this).find("input[name=create_schedule_period_from]").val('');
				$(this).find("input[name=create_schedule_period_to]").val('');
				$(this).find("input[name=create_schedule_start_time]").val('');
				$(this).find("input[name=create_schedule_end_time]").val('');
				$(this).find("input[name=caregiver_id]").val('');
				$(this).find("div#selectedStaffName").html('');

				initDateRangePicker();
			});

			$('.content').on('click','.btnAddScheduleFromService', function(e){
				initDateRangePicker();
				$(".daterange").val(moment().format('DD-MM-YYYY') + ' - ' + moment().format('DD-MM-YYYY'));
				$('input[name=create_schedule_period_from]').val(moment().format('YYYY-MM-DD'));
				$('input[name=create_schedule_period_to]').val(moment().format('YYYY-MM-DD'));
				$('.addScheduleBlock').toggle();
			});

			$('.content').on('click','.btnReplaceStaff', function(e){
				e.preventDefault();
			});

			$('.content').on('click','.btnAssignTasks', function(e){
				var _data = $(this).data();
				assignedTasks = JSON.parse(_data.assignedTasks);
				tasksJson = [];
				if(assignedTasks != null && assignedTasks.length > 0){
					tasksJson = JSON.parse(assignedTasks);
				}
				$('#scheduleTasksModal input[type=checkbox]').each(function(i){
					$(this).prop('checked',false);
				});
				$('#scheduleTasksModal input[type=checkbox]').each(function(i){
					if(checkIfTaskExist(tasksJson, $(this).data('id'))){
						$(this).prop('checked',true);
					}
				});
				$('#scheduleTasksModal #scheduleTasksLabel').html('Schedule Tasks - '+_data.scheduleDate);
				$('#scheduleTasksModal #scheduleTasksForm input[name=service_id]').val(_data.serviceId);
				$('#scheduleTasksModal input[name=task_date]').val(_data.scheduleDate);
				$('#scheduleTasksModal #task_date').val(_data.scheduleDate);
				$('#scheduleTasksModal #task_to_date').val('');
				$('#scheduleTasksModal').modal();
			});

			$('.content').on('click','.btnUpdateScheduleTasks', function(e){
				e.preventDefault();
				var assignedTasks = [];
				$('#scheduleTasksModal input[type=checkbox]:checked').each(function(i){
					assignedTasks.push({id: $(this).data('id'), task: $(this).data('task')});
				});
				if(assignedTasks.length > 0){
					$('#scheduleTasksModal input[name="assigned_tasks"]').val(JSON.stringify(assignedTasks).toString());
					$('#scheduleTasksModal #scheduleTasksForm').submit();
				}else{
					alert("Please select atleast 1 task!");
					return;
				}
			});

			function checkIfTaskExist(taskJson, id){
				var hasMatch =false;
				for (var index = 0; index < taskJson.length; ++index) {
					var task = taskJson[index];
					if(task.id == id){
						hasMatch = true;
						break;
					}
				}
				return hasMatch;
			}

			function formatDate(date){
				const [day, month, year] = date.split("-")
				return new Date(year, month - 1, day)
			}

			$('.content').on('click','.cancel-service',function(e){
				$('#cancellationModal #cancellation_id').val($(this).data('id'));
				$('#cancellationModal #cancellation_lead_id').val($(this).data('lead'));
				$('#cancellationModal #cancellation_status').val($(this).data('status'));
				$('#cancellationModal').modal();
			});

			$('.content').on('click','.service-status',function(){
				if(confirm("Are your sure ?")){
					var id = $(this).data('id');
					var leadID = $(this).data('lead');
					var status = $(this).data('status');

					// Make AJAX Call
					$.ajax({
						url: '{{ route('lead.update-service-status') }}',
						type: 'POST',
						data: {id: id, lead_id: leadID, status: status},
						success: function (data){

							if(data.result){
								// Set the current status to button
								if(status == 'Cancelled'){
									if(data.schedulesDiff == 0){
										$('.table-services tbody tr[data-id="'+id+'"] td:eq(10)').text(status);
									}
									$('.table-services tbody tr[data-id="'+id+'"] ').addClass('light-red');
									$('.table-services tbody tr[data-id="'+id+'"] td:eq(10)').addClass('col-red');
								}

								if(status == 'Complete Service'){
									if(data.schedulesDiff == 0){
										$('.table-services tbody tr[data-id="'+id+'"] td:eq(9)').text('Completed');
									}
									$('.table-services tbody tr[data-id="'+id+'"] ').addClass('light-blue');
									$('.table-services tbody tr[data-id="'+id+'"] td:eq(10)').addClass('col-blue');
								}

								if(status == 'Pending' || status == 'No Supply'){
									setTimeout(function(){
										window.location.reload();
									},1000);
								}

								showNotification('bg-green', 'Status changed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
							}else{
								showNotification('bg-red', 'Unable to change status! Try again later', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
							}
						},
						error: function (error){
							console.log(error);
						}
					});
				}

				return true;
			});

			$('.content').on('click','.assessment-status',function(){
				if(confirm("Are your sure ?")){
					var id = $(this).data('id');
					var leadID = $(this).data('lead');
					var status = $(this).data('status');

					// Make AJAX Call
					$.ajax({
						url: '{{ route('lead.update-assessment-status') }}',
						type: 'POST',
						data: {id: id, lead_id: leadID, status: status},
						success: function (data){

							if(data.result){
								$('.table-assessment tbody tr[data-id="'+id+'"] td .current-assessment-status').html(status);
								showNotification('bg-green', 'Status changed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
							}else{
								showNotification('bg-red', 'Unable to change status! Try again later', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
							}
						},
						error: function (error){
							console.log(error);
						}
					});
				}

				return true;
			});

			$('.content').on('click','.schedule-status',function(){
				if(confirm("Are your sure ?")){
					var id = $(this).data('id');
					var leadID = $(this).data('lead');
					var status = $(this).data('status');

					//Make AJAX Call
					$.ajax({
						url: '{{ route('lead.complete-schedule') }}',
						type: 'POST',
						data: {id: id, lead_id: leadID, status: status},
						success: function (data){
							if(data.result){
								$('#viewSchedulesModal .table-schedules tbody tr[data-id="'+id+'"] td:eq(9)').text(status);
								$('#viewSchedulesModal .table-schedules tbody tr[data-id="'+id+'"] td:eq(0) .schedule-sel-chk').attr('data-status',status);
								$('#viewSchedulesModal .table-schedules tbody tr[data-id="'+id+'"] td:eq(10) .btnAssignTasks').addClass('hide');
								showNotification('bg-green', 'Status changed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
							}else{
								showNotification('bg-red', 'Unable to change status! Try again later', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
							}
						},
						error: function (error){
							console.log(error);
						}
					});
				}

				return true;
			});

			$('.content').on('click','.schedule-chargeable',function(){
				if(confirm("Are your sure ?")){
					var id = $(this).data('id');
					var value = $(this).data('value');

					//Make AJAX Call
					$.ajax({
						url: '{{ route('lead.update-schedule-chargeable') }}',
						type: 'POST',
						data: {id: id, value: value},
						success: function (data){
							console.log(data);
							if(data.result){
								chargeable = value==1?'CHARGEABLE':'NON CHARGEABLE';
								if(value == 1)
									$('#viewSchedulesModal .table-schedules tbody tr[data-id="'+id+'"] td:eq(10) .current-schedule-chargeable').text(chargeable);
								else{
									$('#viewSchedulesModal .table-schedules tbody tr[data-id="'+id+'"] td:eq(10) .chargeability').removeClass('btn-group');
									$('#viewSchedulesModal .table-schedules tbody tr[data-id="'+id+'"] td:eq(10) .chargeability').html(chargeable);
								}

								showNotification('bg-green', 'Schedule Chargeable changed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
							}else{
								showNotification('bg-red', 'Unable to change Chargeable value! Try again later', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
							}
						},
						error: function (error){
							console.log(error);
						}
					});
				}

				return true;
			});

			$('.content').on('change','#service_requested_id', function(){
				rate = $('#service_requested_id option:selected').data('rate');
				if(parseFloat(rate) > 0){
					$('input[name=discount_applicable]').prop('disabled',false);
					$('#serviceRequestForm #rate_lbl').html('&#8377; '+rate);
				}else if(parseFloat(rate) == 0){
					$('input[name=discount_applicable]').prop('disabled',true);
					$('#serviceRequestForm #rate_lbl').html('&#8377; '+rate);
				}else{
					$('input[name=discount_applicable]').prop('disabled',true);
					$('#serviceRequestForm #rate_lbl').html('Not defined!');
				}
				$('#serviceRequestForm #gross_rate').val(rate);
				$('#service_request_from_date').focus();
			});

			$('.content').on('change','input[name=period_type]', function(){
				$('#serviceRequestForm .btnChooseStaff').attr("disabled","disabled");
				if($(this).val() == 'Period'){
					$('#serviceRequestForm #daterange-block').show();
					$('#serviceRequestForm #custom-dates-block').hide();
					$('#mdates').val('');
					$('.mdate').multiDatesPicker('resetDates');
					$('.ui-datepicker-calendar tbody td a.ui-state-active').removeClass('ui-state-active');
					$('#serviceRequestForm input[name=custom_dates]').val('');
				}else{
					$('#serviceRequestForm #daterange-block').hide();
					$('#serviceRequestForm #custom-dates-block').show();
					$('#service_request_from_date').val('');
					$('#service_request_to_date').val('');
				}
			});

			$('.content').on('change','input[name=discount_applicable]', function(){
				if($(this).val() == 'Y'){
					$('input[name=discount_type]').prop('disabled',false);
					$('input[name=discount_value]').prop('disabled',false);
					$('#serviceRequestForm #discount_block').css('display','block');
				}else{
					$('input[name=discount_type]').prop('disabled',true);
					$('input[name=discount_value]').prop('disabled',true);
					$('input[name=discount_value]').val('');
					$('input[name=discounted_amount]').val('');
					$('#serviceRequestForm #discount_block').css('display','none');
				}
			});

			$('.content').on('input','.amtCal', function(){
				calculateTotal();
			});

			$('.content').on('change','input[name=discount_type]', function(){
				calculateTotal();
			});

			$('.content').on('change','#billable_category', function(){
				$('#billable_item').selectpicker('val','');
				$('#billable_item').selectpicker('show');
				$('input[name=item_name]').hide();
				$('input[name=item_name]').val('');

				if($(this).val() == ''){
					$('#billable_item').prop('disabled',true);
					$('#billable_item').prop('required',true);
					$('#billable_rate').val('');
					$('#billable_tax').val('');
					$('#billable_quantity').val(1);
					$('#billable_amount').val('');
				}else if($(this).val() == 'Pharmaceuticals' || $(this).val() == 'Others'){
					$('#billable_item').prop('required',false);
					$('#billable_item').val('0');
					$('#billable_tax').val('');
					$('#billable_rate').prop('disabled',false);
					$('#billable_rate').prop('readonly',false);
					$('#billable_item').selectpicker('hide');
					if($(this).val() == 'Others'){
						$('input[id=medicine_name]').hide();
						$('input[id=other_name]').show();
					}else{
						$('input[id=medicine_name]').show();
						$('input[id=other_name]').hide();
					}
					$('#billable_rate').val('');
					$('#billable_quantity').val(1);
					$('#billable_amount').val('');
				}
				else{
					$('#billable_item').prop('disabled',false);
				}

				$('#billable_item optgroup option').hide();
				$('#billable_item optgroup[label='+$(this).val()+'] option').show();
				$('#billable_item').selectpicker('refresh');
			});

			$('.content').on('change','#billable_item, #billable_rate', function(){
				calculateBillableItem();
			});

			$('.content').on('input','#billable_quantity', function(){
				calculateBillableItem();
			});

			$('.content').on('click','#mdates',function(){
				$('#service_request_from_date').val('');
				$('#service_request_to_date').val('');

				var dates = $('#mdates').val();
				dates = dates.replace(/,\s*$/, "");
				$('#serviceRequestForm input[name=custom_dates]').val(dates);				
				if($('#serviceRequestForm input[name=custom_dates]').val(dates)!=''){
					$('#serviceRequestForm .btnChooseStaff').removeAttr('disabled');
				}
			});

			/*-------------------------------------------
			/*  Handles Case Disposition Update
			/*-------------------------------------------*/
			$('.content').on('click','.lead-status',function(){
				$('#leadStatusModal #lead_status').val($(this).data('status')).change();
				$('#leadStatusModal #lead_disposition_date').val('{{ date('d-m-Y') }}');
				$('#leadStatusModal #comment').val('');
				$('#leadStatusModal').modal();
			});

			$('.content').on('click','.btnChangeLeadStatus',function(){
				$('#leadStatusModal #comment').val('');
				$('#leadStatusModal').modal();
			});

			$('.content').on('click','.updateLeadStatus', function(e){
				e.preventDefault();
				var db = '{{ $database_name }}';
				$(this).prop('readonly', true);
				$(this).prop("disabled", true);
				$(this).unbind("click").click(function(e) {
					e.preventDefault();
				});
				var id = $(this).data('lead-id');
				var status = $('#lead_status option:selected').val();

				var caselosscomment = $("input[name='dropped_reason[]']:checked").val();

				var date = $('#lead_disposition_date').val();
				var comment = $('#lead_comment').val();

				var followUpDate = $('#follow_up_date').val();
				var followUpComment = $('#follow_up_comment').val();
				
				if(status == 'Follow Up' && (followUpDate == '' || followUpComment == '')){
					alert('Choose a Follow Up Date and Comment');
					$(this).prop('readonly', false);
					$(this).prop("disabled", false);
					return false;
				}
				if(date == ''){
					alert('Choose the Disposition Date !');
					$(this).prop('readonly', false);
					$(this).prop("disabled", false);
					return false;
				}
				if(status){
					$.ajax({
						url: '{{ route('corporate.lead.update-disposition') }}',
						type: 'POST',
						data: {id:id, status:status, date:date, comment:comment, caselosscomment:caselosscomment, followupdate:followUpDate, followupcomment:followUpComment, database_name:db},
						success: function (data){
							console.log(data);
							if(data){
								$('#leadStatusModal').modal('hide');
								showNotification('bg-light-green', 'Status changed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
								setTimeout(function(){
									window.location.reload();
								},1000);
							}
						},
						error: function (error){
							console.log(error);
							$('.updateLeadStatus').attr('disabled', false);
						}
					});
				}
			});

			$('.content').on('click','.btnRemoveBillableItem',function(){
				id = $(this).data('id');
				if(confirm('Do you really want to remove?')){
					$.ajax({
						url: '{{ route('lead.remove-billable-item') }}',
						type: 'POST',
						data: {id: $(this).data('id')},
						success: function(d){
							if(d){
								$('.billablesTable tbody tr[data-id='+id+']').remove();
								var totals=[0,0.00];
								var $dataColumns = [
									$(".billablesTable tbody tr td:nth-child(8)"),
									$(".billablesTable tbody tr td:nth-child(9)"),
								];
								var $dataRows = $('.billablesTable tbody tr').length-1;
								for(var i=0; i < $dataColumns.length ;i++){
									for (var j = 0; j < $dataRows; j++) {
										totals[i] += parseFloat( $dataColumns[i][j].innerHTML );
										console.log(totals);
									}
								}

								$(".totalCol").each(function(i){
									$(this).text(totals[i]);
								});
								showNotification('bg-orange', 'Item removed', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
							}
						},
						error: function (d, err){
							console.log(err);
						}
					});
				}
			});

			/*  New Code Block Ends */

			$('#editCaseModal #source').selectpicker('val','{{ $l->source }}');

			$('.content').on('click','.btnEditCarePlan',function(){
				//$('#editCaseModal #comment').val('');
				$('#editCaseModal').modal();
			});

			$('.content').on('click','.btnChooseProvider', function(){
				var id = $(this).data('visit-id');
				$('#providerSearchForm #filter_service').selectpicker('val',$('#editCaseModal #service_id').val());
				$('#providerSearchForm #filter_city').val($('#city').val());
				$('.btnFilterProvider').trigger( "click" );
				$('#providerSearchModal .btnSelectProvider').attr('data-visit-id',id);
				$('#providerSearchModal').modal();
			});

			/*-------------------------------------------
			/*  Handles Existing Customer Check
			/*-------------------------------------------*/
			$('.content').on('click', '#checkForPreviousCarePlans', function(){
				checkIfExistingCustomer();
			});

			$('.content').on('click','.btnChooseStaff', function(){
				var block = $(this).attr('data-block');
				var profile = $(this).attr('data-profile');
				var caregiver = $(this).attr('data-caregiver');

				//$('#staffSearchModal .btnSelectStaff').attr('data-visit-id',id);
				$('#staffSearchModal .btnSelectStaff').attr('data-block',block);
				$('#staffSearchModal .btnSelectStaff').attr('data-profile',profile);
				$('#staffSearchModal .btnSelectStaff').attr('data-caregiver',caregiver);

				if(block == '.serviceRequestCaregiverBlock'){
					$('#filter_from_date').val($('#serviceRequestForm #service_request_from_date').val());
					$('#filter_to_date').val($('#serviceRequestForm #service_request_to_date').val());
					$('#datesFromSchedules').val('');
					$('#delivery_address_value').val($('input[name=delivery_address]:checked').val());
				}
				if(block == '.viewScheduleCaregiverBlock'){
					$('#filter_from_date').val($('#createSchedulesForm input[name=create_schedule_period_from]').val());
					$('#filter_to_date').val($('#createSchedulesForm input[name=create_schedule_period_to]').val());
					$('#custom_dates').val('');
					$('#datesFromSchedules').val('');
					$('#delivery_address_value').val($('input[name=delivery_address_schedule]:checked').val());
				}
				if(block == '.replaceScheduleCaregiverBlock'){
					$('#filter_from_date').val('');
					$('#filter_to_date').val('');
					$('#custom_dates').val('');
					$('#delivery_address_value').val($('input[name=delivery_address_assign]:checked').val());
				}

				$('#staffSearchModal').modal();
			});

			// Define a variable for your dataTable object to use
			var reportListDataTable = null;

			$('.content').on('click','.btnFilterStaff', function(){
				var flag = 'uninterrupted';
				var replacement = $('#staffSearchForm #replacement').val();
				var interruptedDates = $('#custom_dates').val();
				var datesFromSchedules = $('#datesFromSchedules').val();

				if(interruptedDates != ''){
					var flag = 'interrupted';
				}

				serviceDeliveryAddress = $('#delivery_address_value').val();

				fDate = $('#filter_from_date').val();
				tDate = $('#filter_to_date').val();
				
				fTime = $('#filter_from_time').val();
				tTime = $('#filter_to_time').val();

				specialization = $('#filter_specialization option:selected').val();
				experience = $('#filter_experience option:selected').val();
				experience_level = $('#filter_experience_level option:selected').val();
				city = $('#filter_city').val();

				showOnDutyStaff = $('#include_onduty_staff').is(':checked')?1:0;

				var language = [];
				$('#filter_language :selected').each(function(i, selected){
					language[i] = $(selected).text();
				});

				var skill = [];
				$('#filter_skill :selected').each(function(i, selected){
					skill[i] = $(selected).val();
				});

				$.ajax({
					url: '{{ route('ajax.caregiver.filter') }}',
					type: 'POST',
					data: {_token: '{{ csrf_token() }}', pid: '{{ Helper::encryptor('encrypt',$l->patient_id) }}',fd: fDate, td: tDate, ft: fTime, tt: tTime, s: specialization, e: experience, el: experience_level, c: city, l: language, sk: skill, sods: showOnDutyStaff, flag: flag, dates: interruptedDates, replacement: replacement,datesFromSchedules: datesFromSchedules,sda: serviceDeliveryAddress },
					success: function (data){
						html = '<tr><td class="text-center" colspan="7">No caregiver(s) found. Modify Filters to get results</td></tr>';
						if(data['result'].length){
							html = '';
							rdata = data['result'];
							patientLocation = data['patient_location'];
							$.each(rdata, function(i){
								distanceTxt = '-';
								lat = lng = patLat = patLng = 0;
								trClass = rdata[i].onduty?'light-red':'';
								trTitle = rdata[i].onduty?'Already scheduled for a visit':'';

								var coordinates = rdata[i].coordinates;
								if(coordinates != '' && patientLocation != ''){
									lat = coordinates.split("_")[0];
									lng = coordinates.split("_")[1];

									patLat = patientLocation.split("_")[0];
									patLng = patientLocation.split("_")[1];
									distanceTxt = calculateDistance(lat, lng, patLat, patLng, "K");
								}

								html += `<tr class="`+trClass+`" title="`+trTitle+`">
								<td>`+rdata[i].employee_id+`</td>
								<td>`+rdata[i].full_name+`<br><small>`+rdata[i].location+`</small></td>
								<td>`+rdata[i].specialization+`</td>
								<td>`+rdata[i].experience+`</td>
								<td>`+rdata[i].level+`</td>
								<td>`+distanceTxt+`</td>
								<td class="text-center"><input name="choosen_emp" type="radio" id="choosen_emp_`+rdata[i].id+`" class="with-gap radio-col-blue" data-name="`+rdata[i].full_name+`" data-mobile="`+rdata[i].mobile+`" data-empLat="`+lat+`" data-empLng="`+lng+`" data-patLat="`+patLat+`" data-patLng="`+patLng+`" value="`+rdata[i].id+`" /><label for="choosen_emp_`+rdata[i].id+`"></label></td>
								</tr>`;
							});

							// Destroy the dataTable and empty because the columns may change!
							if (reportListDataTable !== null ) {
								// for this version use fnDestroy() instead of destroy()
								reportListDataTable.fnDestroy();
								reportListDataTable = null;
								// empty in case the columns change
								$('.staffSearchResults tbody').empty();
							}

							$('.staffSearchResults tbody').html(html);

							// Build dataTable with ajax, columns, etc.
							reportListDataTable = $('.staffSearchResults').dataTable({
								"dom": 'frtip',
								"order": [[ 5, 'asc' ]],
								"scrollY": '35vh',
								"scrollCollapse": true,
								"paging": false
							});

							if(localStorage.getItem('helpMode') == 'true'){
								swal({
									title: "Staffs Loaded !",
									text: "We have loaded staff as per the filters. Please use them to refine your search",
									icon: "success",
									closeOnClickOutside: false,
									closeOnEsc: false,
									buttons: {
										ok: {
											text: "Ok! I got it.",
										}
									},
								});
							}
						}else{
							$('.staffSearchResults tbody').html(html);
							if(localStorage.getItem('helpMode') == 'true'){
								swal({
									title: "No Staffs Loaded !",
									text: "We could not find staff as per the filters. Please refine your search",
									icon: "warning",
									closeOnClickOutside: false,
									closeOnEsc: false,
									buttons: {
										ok: {
											text: "Ok! I will refine it.",
										}
									},
								});
							}
						}
					},
					error: function (error){
						console.log(error);
					}
				});
			});

			$('.content').on('click','.btnReset', function(e){
				e.preventDefault();
				if (reportListDataTable !== null ) {
					reportListDataTable.fnDestroy();
					reportListDataTable = null;
					$('.staffSearchResults tbody').empty();
				}
				clearSearchForm();
			});

			$('.content').on('click', '.btnSelectStaff', function(){
				var block = $(this).attr('data-block');
				var profile = $(this).attr('data-profile');
				var caregiver = $(this).attr('data-caregiver');
				var emp = $('input[name=choosen_emp]:checked').val();
				var name = $('input[name=choosen_emp]:checked').data('name');
				var mobile = $('input[name=choosen_emp]:checked').data('mobile');
				var emplat = $('input[name=choosen_emp]:checked').data('emplat');
				var emplng = $('input[name=choosen_emp]:checked').data('emplng');
				var patlat = $('input[name=choosen_emp]:checked').data('patlat');
				var patlng = $('input[name=choosen_emp]:checked').data('patlng');

				if(typeof emp != 'undefined' && emp != ''){
					if(emplat!=0 && emplng!=0 && patlat!=0 && patlng!=0) {
						if(typeof(emplat)!='undefined' && typeof(emplng)!='undefined' && typeof(patlat)!='undefined' && typeof(patlng)!='undefined') {
							emplat = emplat.toString().split(" ").join("");
							emplng = emplng.toString().split(" ").join("");
							patlat = patlat.toString().split(" ").join("");
							patlng = patlng.toString().split(" ").join("");

						getMapMyIndia('https://apis.mapmyindia.com/advancedmaps/v1/89oegcgbhdv1il54kcry8hrlfwet71kf/distance?center='+emplat+','+emplng+'|&pts='+patlat+','+patlng+'');
						} else {
							$(".road_distance").val('');
						}
					} else {
						$(".road_distance").val('');
					}

					$('#staffSearchModal').modal('hide');

					$(block + ' ' + caregiver).val(emp);
					$(block + ' ' + profile).html('<div class="col-sm-3"><img src="/img/user.png" width="40" height="40" class="img-circle"></div><div class="col-sm-9">'+name+'<br>Mob: '+mobile+'</div><br>');
					$(block).show();
					$('.btnSaveServiceRequest').prop('disabled',false);
				}else{
					alert("Please select staff !");
				}
			});

			$('.content').on('click','.btnSaveServiceRequest', function(e){
				e.preventDefault();

				if($('#service_requested_id option:selected').val() == ''){
					alert("Please select service!");
					return false;
				}

				if( ($('#service_request_from_date').val() == '' || $('#service_request_to_date').val() == '') && $('#custom_dates').val() == '' ){
					alert("Please select period!");
					return false;
				}

				if( ($('#service_request_start_time').val() == '' || $('#service_request_end_time').val() == '')){
					alert("Please select time interval!");
					return false;
				}

				$(this).attr('disabled',true);
				$('#serviceRequestForm').submit();
			});
		});
		
		$('.content').on('click','.btnAddService', function(){
			if(assessmentFlag != ''){
				assessmentDone = assessmentFlag;
			}else{
				assessmentDone = '{{ $l->assessment_done }}'
			}

			if(assessmentDone == ''){
				swal({
				  title: "Has the assessment been done for this Episode?",
				  text: "Your choice will be saved for this episode !",
				  icon: "warning",
				  closeOnClickOutside: false,
				  closeOnEsc: false,
				  buttons: {
				  	done: {
				  		text: "Yes!",
				  		value: "Done",
				  	},
				  	notdone: {
				  		text: "No",
				  		value: "No",
				  	},
				  	notrequired: {
				  		text: "Not Required!",
				  		value: "Not Required",
				  	},
				  },
				})
				.then((value) => {
					switch (value) {
						case "Done":
							$.ajax({
								url: '{{ route('lead.assessent-done-status') }}',
								type: 'POST',
								data: {value: value, lead: '{{ $l->id}}' },
								success: function (d){
									swal("Great!", "Continue for service order!", "success");
									$('#service-request-block').toggle();
									$('#serviceRequestForm').trigger("reset");
									$('#serviceRequestForm select').selectpicker('val','');
									$('#serviceRequestForm code span').html('');
									$('#serviceRequestForm #gross_rate').val('');
									$('#serviceRequestForm #service_request_amount').html('');
									$('.date').data('DateTimePicker').clear();
									$('#serviceRequestForm #frequency').selectpicker('val','Daily');
									$('html, body').stop().animate({
										'scrollTop': $('#service-request-block').offset().top-20
									}, 700, 'swing');
									assessmentFlag = 'Done';
								},
								error: function (err){
									console.log(err);
								}
							});
							break;

						case "Not Required":
							$.ajax({
								url: '{{ route('lead.assessent-done-status') }}',
								type: 'POST',
								data: {value: value, lead: '{{ $l->id}}' },
								success: function (d){
									swal("Fine!", "We will remember that for you!", "success");
									$('#service-request-block').toggle();
									$('#serviceRequestForm').trigger("reset");
									$('#serviceRequestForm select').selectpicker('val','');
									$('#serviceRequestForm code span').html('');
									$('#serviceRequestForm #gross_rate').val('');
									$('#serviceRequestForm #service_request_amount').html('');
									$('.date').data('DateTimePicker').clear();
									$('#serviceRequestForm #frequency').selectpicker('val','Daily');
									$('html, body').stop().animate({
										'scrollTop': $('#service-request-block').offset().top-20
									}, 700, 'swing');
									assessmentFlag = 'Not Required';
								},
								error: function (err){
									console.log(err);
								}
							});
							break;

						default:
							swal("Please complete the assessment first!");
					}
				});
			}else{
				$('#service-request-block').toggle();
				$('#serviceRequestForm').trigger("reset");
				$('#serviceRequestForm select').selectpicker('val','');
				$('#serviceRequestForm code span').html('');
				$('#serviceRequestForm #gross_rate').val('');
				$('#serviceRequestForm #service_request_amount').html('');
				$('.date').data('DateTimePicker').clear();
				$('#serviceRequestForm #frequency').selectpicker('val','Daily');
				$('html, body').stop().animate({
					'scrollTop': $('#service-request-block').offset().top-20
				}, 700, 'swing');
			}
		});

		function initBlock()
		{
			var holidayList = ["<?php echo ($holidays); ?>"];
			var holidayListRev = ["<?php echo ($holidaysr); ?>"];
			var date = new Date();

			$('.taskMultiAssign').datetimepicker({
				format: 'DD-MM-YYYY',
				disabledDates: holidayListRev,
				showClose: true,
				showClear: true,
				useCurrent: false,
				keepInvalid:true,
			}).on('dp.show', function(e){
				$(this).data('DateTimePicker').minDate($('#task_date').val());
			});

			$('.bdate').datetimepicker({
				format: 'DD-MM-YYYY',
				showClose: true,
				useCurrent: false,
				keepInvalid:true,
				showClear: true,
				maxDate : moment(),
			});

			$('.date').datetimepicker({
				format: 'DD-MM-YYYY',
				disabledDates: holidayListRev,
				showClose: true,
				showClear: true,
				// minDate: date,
				useCurrent: false,
				keepInvalid:true
			}).on('dp.change', function(e){
				if($(e.target).attr('id') == 'service_request_from_date' || $(e.target).attr('id') == 'service_request_to_date'){
					$('.serviceRequestCaregiverBlock').css('display','none');
					$('.serviceRequestCaregiverBlock #caregiver_id').val('');
					calculateTotal();

					if($(e.target).attr('id') == 'service_request_from_date'){
						$('#service_request_to_date').focus();
					}

					if($('#service_request_from_date').val()!='' && $('#service_request_to_date').val()!=''){
						$('#serviceRequestForm .btnChooseStaff').removeAttr('disabled');
					} else {
						$('#serviceRequestForm .btnChooseStaff').attr("disabled","disabled");
					}

					$('#service_request_to_date').on('dp.change',function(){
						if(localStorage.getItem('helpMode') == 'true'){
							swal({
								title: "Staff allocation !",
								text: "Do you want to allocate a staff for this service entry ?",
								icon: "warning",
								closeOnClickOutside: false,
								closeOnEsc: false,
								buttons: {
									yes: {
										text: "Yes!",
										value: "Yes",
									},
									notNow: {
										text: "Not Now",
										value: "Not Now",
									},
								},
							})
							.then((value) => {
								switch (value) {
									case "Yes":
									$("#scheduleBlock .btnChooseStaff").trigger('click');
									$(".btnFilterStaff").trigger('click');
									break;

									case "Not Now":
									swal("Ok!", "", "success")
									break;
								}
							});
						}
					});
				}
			});

			$('.datetime').datetimepicker({
				format: 'DD-MM-YYYY hh:mm A',
				showClose: true,
				keepOpen: false,
				useCurrent: false,
				keepInvalid:true,
				minDate: moment(),
			});

			$('.schedule-from').datetimepicker({
				format: 'DD-MM-YYYY',
				showClear: true,
				minDate: moment().subtract(5,'d')
			});

			$('.content').on('dp.change','.schedule-from',function(e){
				if(e.oldDate != null){
					$('.schedule-to').datetimepicker({
						format: 'DD-MM-YYYY',
						showClear: true,
						useCurrent: false,
						keepInvalid:true,
						minDate: e.date,
						// maxDate: moment(e.date).add(5,'d')
					});
				}
			});

			$('.content').on('dp.change','input[name=start_time], input[name=end_time]', function(){
				var fromTime = $('input[name=start_time]').val();
				var toTime = $('input[name=end_time]').val();

				if(fromTime && toTime){
					startTime = moment(fromTime,'HH:mm');
					endTime = moment(toTime,'HH:mm');

					duration = moment.duration(endTime.diff(startTime));

					$('.timing_duration').show();
					$('.timing_duration').html('Duration: '+ parseFloat(duration.asHours()).toFixed(1) + ' hr(s) ');
				}
			});

			$('.mdate').multiDatesPicker({
				dateFormat: "dd-mm-yy",
				addDisabledDates: holidayList,
				altField: '#mdates',
				onSelect: function(){
					$('#mdates').trigger("click");
					calculateTotal();
				},
			});
		}

		$('.content').on('change','#frequency, #frequency_period, #discount_applicable_yes, #discount_applicable_no',function(){
			calculateTotal();
		});

		function formatCurrency(nStr){
			nStr += '';
			var x = nStr.split('.');
			var x1 = x[0];
			var x2 = x.length > 1 ? '.' + x[1] : '';
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(x1)) {
				x1 = x1.replace(rgx, '$1' + ',' + '$2');
			}
			return x1 + x2;
		}

		function calculateTotal()
		{
			var totalAmount = 0;
			var days = 0;

			var block = '#service-request-block';
			var amount = $(block+' #gross_rate').val();
			amount = amount?parseFloat(amount):0;

			var discount = $(block+' input[name=discount_applicable]:checked').val();
			var dt = $(block+' input[name=discount_type]:checked').val();

			var dv = $(block+' #discount_value').val();
			dv = dv?parseFloat(dv):0;

			actualAmount = amount;
			if(discount == 'Y'){
				if(dt == 'P'){
					actualAmount = parseFloat(actualAmount) - parseFloat(actualAmount) * (dv/100);
					$(block+' #discounted_amount').val((parseFloat(actualAmount) * (dv/100)).toFixed(2));
				}

				if(dt == 'F'){
					actualAmount = parseFloat(actualAmount) - dv;
					$(block+' #discounted_amount').val(parseFloat(dv).toFixed(2));
				}
			}
			totalAmount = actualAmount;

			var fromDate = $(block+' #service_request_from_date').val();
			var toDate = $(block+' #service_request_to_date').val();
			var frequency = $(block+' #frequency').val();
			var frequencyPeriod = $(block+' #frequency_period').val();
			if (fromDate != '' || toDate != ''){
				if(fromDate != ''){
					fromDate = moment(fromDate, "DD-MM-YYYY");
					if(toDate != ''){
						toDate = moment(toDate, "DD-MM-YYYY");
					}else{
						toDate = fromDate;
					}

					days = toDate.diff(fromDate, 'days') + 1;
				}
			}else{
				if($('#mdates').val() == '')
					days = 0;
				else
					days = $('#mdates').val().split(",").length;
			}

			if(frequency == 'Hourly'){
				totalAmount = frequencyPeriod * totalAmount;
			}else{
				totalAmount = days * totalAmount;
			}

			if(totalAmount > 0){
				if(outstanding != 0){
					if(outstanding < 0){
						noOfDays = Math.floor(Math.abs(outstanding) / actualAmount);
					}

					if(outstanding > 0 || totalAmount > outstanding){
						$(block+' .outstanding-msg-block > div').html('Customer has an outstanding balance, Service order cannot be created! <i class="material-icons" title="To override this, modify the setting in configuration">info</i>');
						$(block+' .outstanding-msg-block').show();
						$(block+' .btnSaveServiceRequest').prop('disabled',true);
						$(block+' .btnSaveServiceRequest').hide();
						$(block+' #scheduleBlock').hide();
						$(block+' #scheduleTasksBlock').hide();
					}
				}else{
					$(block+' .outstanding-msg-block').hide();
					$(block+' .outstanding-msg-block > div').html('');
					$(block+' .btnSaveServiceRequest').prop('disabled',false);
					$(block+' .btnSaveServiceRequest').show();
					$(block+' #scheduleBlock').show();
					$(block+' #scheduleTasksBlock').show();
				}
				$(block+' #service_request_amount').html(''+ formatMoney(totalAmount) +' ( Rate: '+formatMoney(parseFloat(actualAmount)) + ')');
				$(block+' #net_rate').hide();
				$(block+' #net_rate').val(parseFloat(actualAmount).toFixed(2));
			}else{
				$(block+' #net_rate').val(0);
			}
		}

		function calculateBillableItem()
		{
			var category = $('#billable_category option:selected').val();
			rateFromList = (category == 'Pharmaceuticals' || category == 'Others')?false:true;
			if(rateFromList)
			var rate = parseFloat($('#billable_item option:selected').data('rate'));
			else
			var rate = parseFloat($('#billable_rate').val());

			var quantity = parseInt($('#billable_quantity').val());
			var tax = parseFloat($('#billable_item option:selected').data('tax'));

			$('#billable_rate').val(rate);
			$('#billable_tax').val(tax);
			$('#billable_amount').val(parseFloat((rate + (rate * (tax/100))) * quantity).toFixed(2));
		}

		function clearSearchForm()
		{
			$('#staffSearchModal #staffSearchForm #filter_specialization').selectpicker('val','0');
			$('#staffSearchModal #staffSearchForm #filter_experience').selectpicker('val','0');
			$('#staffSearchModal #staffSearchForm #filter_experience_level').selectpicker('val','0');
			$('#staffSearchModal #staffSearchForm #filter_language').selectpicker('val','0');
			$('#staffSearchModal #staffSearchForm #filter_skill').selectpicker('val','0');
			$('#staffSearchModal .staffSearchResults tbody').html('<tr><td class="text-center" colspan="8">Click \'Search\' to getresults</td></tr>');
		}

		var visitListArray = [];

		$('.content').on('change','#filter_from_date',function(){
			var value = $(this).val();
			if(!$('.filter_period:eq(0)').is(':visible')){
				$('#filter_to_date').val(value);
			}
		});

		function getServiceInterruptionsList(scheduleID){
			if(scheduleID){
				var leadID = $('#lead_id').val();
				$.ajax({
					url: '{{ route('lead.get-service-interruption') }}',
					type: 'POST',
					data: {lead_id: leadID, schedule_id: scheduleID},
					success: function (data){
						if(data.length){
							html = ``;
							$(data).each(function(i){
								var fdate = new Date(data[i].from_date);
								var tdate = new Date(data[i].to_date);
								var cdate = new Date(data[i].created_at);
								noOfDays = moment.duration(moment(tdate).diff(moment(fdate)));

								html += `<tr>
								<td>`+(i+1)+`</td>
								<td>`+moment(fdate).format('DD-MM-YYYY')+`</td>
								<td>`+moment(tdate).format('DD-MM-YYYY')+`</td>
								<td>`+noOfDays.asDays()+`</td>
								<td>`+data[i].reason+`</td>
								<td>`+moment(cdate).format('DD-MM-YYYY')+`</td>
								<td>`+data[i].user.full_name+`</td>
								</tr>`;

								logHtml = `<ul>`;
								if(data[i].audits.length){
									var audits = data[i].audits;
									$(audits).each(function(j){

									});
									logHtml += `</ul>`;
								}
							});
							$('.serviceInterruptionsTable tbody').html(html);
						}
					},
					error: function (err){
						console.log(err);
					}
				});
			}
		}

		$('.content').on('click','.btnEditServiceRequests', function(){
			data = $(this).data();
			if(data){
				block = '#serviceRequestModal #serviceRequestForm ';
				$(block+' #service_requested_id').selectpicker('val',data.serviceRequested);
				$(block+' #service_requested_id').selectpicker('refresh');
				$(block+' #service_recommended_id').selectpicker('val',data.serviceRecommended);
				$(block+' #service_recommended_id').selectpicker('refresh');
				$(block+' #service_request_from_date').val(data.fromDate);
				$(block+' #service_request_to_date').val(data.toDate);
				$(block+' #frequency_period').val(data.frequencyPeriod);
				$(block+' #frequency').selectpicker('val',data.frequency);
				$(block+' #frequency').selectpicker('refresh');
				$(block+' input[name=id]').val(data.id);

				$('#serviceRequestModal').modal();
			}
		});

		$('.content').on('click','.btnSendToVendor', function(){
			var items = [];
			var vendorID = $('select[name=vendor_id] option:selected').val();
			if(vendorID == 0){
				alert("Please select vendor!");
				return false;
			}

			$('.billable-sel-chk').each(function(i){
				if($(this).is(':checked')){
					items.push($(this).val());
				}
			});

			if(items.length <= 0){
				alert("Please select items!");
				return false;
			}

			if(vendorID > 0 && items.length > 0){
				$(this).attr('disabled', true);
				$.ajax({
					url: '{{ route('lead.send-billables-to-vendor') }}',
					type: 'POST',
					data: {vendor_id: vendorID, items: items},
					success: function (d){
						if(d){
							showNotification('bg-green', 'Email sent successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');

							$('select[name=vendor_id]').val('0').change();
							$('.billable-sel-chk').each(function(i){
								if($(this).is(':checked')){
									$(this).prop('checked', false);
									items = [];
								}
							});
						}
						$(this).attr('disabled', false);
					},
					error: function (err){
						console.log(err);
						$(this).attr('disabled', false);
					}
				})
			}
		});

		function showAlert(type, message, block=''){
			var typeClass = 'alert-info';
			if(type == 'success') typeClass = 'alert-success';
			if(type == 'error') typeClass = 'alert-danger';

			if(block == ''){
				block = '.content > .container-fluid';
			}
			html = `<div class="alert ` + typeClass + ` alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			`+ message +`
			</div>`;

			$(block).prepend(html);
		}

		function calculateDistance(lat1, lon1, lat2, lon2, unit) {
			var radlat1 = Math.PI * lat1/180;
			var radlat2 = Math.PI * lat2/180;
			var theta = lon1-lon2;
			var radtheta = Math.PI * theta/180;
			var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
			dist = Math.acos(dist);
			dist = dist * 180/Math.PI;
			dist = dist * 60 * 1.1515;
			if (unit=="K") { dist = dist * 1.609344; }
			if (unit=="N") { dist = dist * 0.8684; }

			return dist.toFixed(2);
		}

		function calcCrow(lat1, lon1, lat2, lon2)
		{
			var R = 6371; // km
			var dLat = toRad(lat2-lat1);
			var dLon = toRad(lon2-lon1);
			var lat1 = toRad(lat1);
			var lat2 = toRad(lat2);

			var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
			Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
			var d = R * c;

			return d;
		}

		function getMapMyIndia(api_url)
		{
			$(".loader2").html(`<div class="page-loader-wrapper">
                <div class="loader">
                    <div class="md-preloader pl-size-md">
                        <svg viewbox="0 0 75 75">
                            <circle cx="37.5" cy="37.5" r="33.5" class="pl-red" stroke-width="4" />
                        </svg>
                    </div>
                    <p>Please wait...</p>
                </div>
            </div>`);
            
			$.ajax({
                url: '{{ route('lead.get-distance-from-map-my-india') }}',
                type: "POST",
                data: {url: api_url},
                success: function (data) {
                    var jsondata = JSON.parse(data);
                    if (jsondata.responseCode == 200) {
                    	if (jsondata.results != null && jsondata.results[0].status==6) {
                    		var length = (jsondata.results[0].length / 1000).toFixed(1) + " km";
                        	$(".road_distance").val(length);
	                    } else {
	                    	$(".road_distance").val('');
	                    }                        
                    } else {
                    	$(".road_distance").val('');
                        console.log('Something went wrong in the getMapMyIndia response');
                    }
                    $(".loader2").html("");
                }
            })
		}

		// Converts numeric degrees to radians
		function toRad(Value){
			return Value * Math.PI / 180;
		}

		function formatMoney(value){
			var formatter = new Intl.NumberFormat('en-US', {
			  style: 'currency',
			  currency: 'INR',
			  minimumFractionDigits: 2,
			});

			return formatter.format(value);
		}
	</script>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/2.0.4/css/Jcrop.min.css"></link>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/2.0.4/js/Jcrop.min.js"></script>
	<script type="text/javascript">
		function handleFileSelect(evt) {
			var files = evt.target.files; // FileList object

			// Loop through the FileList and render image files as thumbnails.
			for (var i = 0, f; f = files[i]; i++) {
				// Only process image files.
				if (!f.type.match('image.*')) {
					continue;
				}
				var reader = new FileReader();

				// Closure to capture the file information.
				reader.onload = (function(theFile) {
					return function(e) {
						// Render thumbnail.
						var span = document.createElement('span');
						span.innerHTML = ['<img style="width:160px !important;height: 160px  !important;" id="patient_image" class="img-responsive img-thumbnail" src="', e.target.result,
						'" title="', escape(theFile.name), '"/>'].join('');
						$('.profile-img-block').html(span);
					};
				})(f);

				// Read in the image file as a data URL.
				reader.readAsDataURL(f);
			}
		}

		document.getElementById('pic_upload').addEventListener('change', handleFileSelect, false);
	</script>

	<script type="text/javascript">
		$('.content').on('click','.addDiscount',function(){
			serviceID = $(this).data('service-id')
			$.ajax({
				type: 'POST',
				url: '{{ route('lead.get-service-order-for-discount') }}',
    			dataType: 'json',
				data: {serviceRequestId:serviceID ,_token:'{{csrf_token()}}'},
				success: function (d) {
					if(d){
						html = `<option value=''>Select Service Period</option>`;
						$.each(d, function(i){
							html += `<option data-gross-rate='`+d[i].gross_rate+`' data-schedule-dates='`+d[i].schedule_dates+`' value='`+d[i].service_order_id+`'>`+d[i].service_order_date+`</option>`;
						});

						$("#servicePeriod_"+serviceID).html(html); // Dropdown service order period
						$("#servicePeriod_"+serviceID).selectpicker('refresh'); // Dropdown service order period refreshed
						$(".apply_discount_"+serviceID).attr('disabled','disabled'); // Discount Period calender disabled
						$(".rateCardInput_after_"+serviceID).attr('disabled','disabled'); // Discount Value textbox disabled
						$(".discountSubmit_"+serviceID).attr('disabled','disabled'); // Submit button disabled
					}
				},
				error: function (err){
					console.log('Error: '+err);
				}
			})

			$('.showDiscountRow_'+serviceID).children('.row_'+serviceID).slideToggle();
		});
    	$('.servicePeriod').on('change', function(){
    		serviceID = $(this).data('service-id');
			$(".apply_discount_"+serviceID).attr('disabled',false); // Discount Period calender enabled
			$(".rateCardInput_after_"+serviceID).attr('disabled','disabled'); // Discount Value textbox disabled
			$(".discountSubmit_"+serviceID).attr('disabled','disabled'); // submit button disabled
    		var scheduleDates = $(this).find(':selected').data('schedule-dates');
			if(scheduleDates.length === 0){
				$(".apply_discount_"+serviceID).attr('disabled','disabled'); // Discount Period calender disabled
				$(".apply_discount_"+serviceID).attr('placeholder','No Pending Schedules for Discounting.');
				$(".discountSubmit_"+serviceID).attr('disabled','disabled'); // Submit button disabled
			}else{
				$(".apply_discount_"+serviceID).attr('placeholder','Discount Period');
				calculateTotalAfter(serviceID);
				dates = scheduleDates.split(',');
				$(".apply_discount_"+serviceID).flatpickr({
					enable:dates,
					dateFormat:"d-m-Y",
					mode:"range",
				});
			}
		});

		$('.content').on('change','input[name=apply_discount]', function(){
			calculateTotalAfter($(this).data('service-id'));
			$(".rateCardInput_after_"+serviceID).attr('disabled',false); // Discount Value textbox enabled
		});

		$('.content').on('input','.amtCal_after', function(){
			calculateTotalAfter($(this).data('service-id'));
			$(".discountSubmit_"+serviceID).attr('disabled',false); // Submit button enabled
		});

		$('.content').on('change','input[name=discount_type_after]', function(){
			calculateTotalAfter($(this).data('service-id'));
		});

		function calculateTotalAfter($serviceID)
		{
			var totalAmount = 0;
			var grossRate = $('#servicePeriod_'+$serviceID).find(':selected').data('gross-rate');

			var amount = grossRate;
			amount = amount?parseFloat(amount):0;

			var dt = $('input[name=discount_type_after_'+$serviceID+']:checked').val();

			var dv = $('#discount_value_after_'+$serviceID).val();
			dv = dv?parseFloat(dv):0;

			actualAmount = amount;
			if(dt == 'P'){
				actualAmount = parseFloat(actualAmount) - parseFloat(actualAmount) * (dv/100);
			}

			if(dt == 'F'){
				actualAmount = parseFloat(actualAmount) - dv;
			}



			// var noOfDays = $('.addDiscount[data-service-id='+$serviceID+']').data('no-of-days');
			var noOfDays = $('.apply_discount_'+$serviceID).val().split(',').length;
			totalAmount = actualAmount * noOfDays;

			if(totalAmount > 0){
				$('#service_request_amount_after_'+$serviceID).html(formatMoney(totalAmount));
				$('#net_rate_after_'+$serviceID).hide();
				$('#net_rate_after_'+$serviceID).val(parseFloat(actualAmount).toFixed(2));
			}else{
				$('#net_rate_after_'+$serviceID).val(0);
			}
		}
	</script>
	<script type="text/javascript">
		$('#schedules-sel-all').on('click',function(){
			if(this.checked){
				$('.schedule-sel-chk').each(function(){
					this.checked = true;
					$('.table-schedules tr').css('background-color', '#a5ddd5');
				});
			}else{
				$('.schedule-sel-chk').each(function(){
					this.checked = false;
					$('.table-schedules tr').css('background-color', '#ffffff');
				});
			}
		});

		$('.content').on('click','.schedule-sel-chk',function(){
			if($('.schedule-sel-chk:checked').length == $('.schedule-sel-chk').length){
				$('#schedules-sel-all').prop('checked',true);
			}else{
				$('#schedules-sel-all').prop('checked',false);
			}
			dataID = this.value.split("_").pop();
			if(this.checked){
				$('.table-schedules tr[data-id='+dataID+']').css('background-color', '#a5ddd5');
			}else{
				$('.table-schedules tr[data-id='+dataID+']').css('background-color', '#ffffff');
			}
		});

		$('.content').on('click','#btnCancelSchedule',function(){
			var scheduleDates = [];
			$('.schedule-sel-chk').each(function(i){
				if($(this).is(':checked')){
					scheduleDates.push($(this).attr('data-date'));
				}
			});
			if(scheduleDates.length <= 0){
				alert("There are no schedules selected for any Operation!");
				return false;
			}
		});

		$('.content').on('click','#passDates',function(){
			var scheduleDates = [];
			var deliveryAddress =[];
			var scheduleStatus =[];
			$('.schedule-sel-chk').each(function(i){
				if($(this).is(':checked')){
					scheduleDates.push($(this).attr('data-date'));
					scheduleStatus.push($(this).attr('data-status'));
					deliveryAddress.push($(this).attr('data-delivery-address'));
				}
			});
			if(scheduleDates.length <= 0){
				alert("There are no schedules selected for any Operation!");
				return false;
			}
			if(jQuery.inArray("Completed",scheduleStatus) != -1){
				swal("Selected schedule has completed service!","Select schedules with only pending status","warning");
				return false;
			}
			deliveryAddress.every(item => item == deliveryAddress[0])?'':swal("Selected schedules has multiple delivery addresses!","All selected schedule address will be changed to the new address","warning");

			$('#datesFromSchedules').val(scheduleDates);
		});

		$('.content').on('click','#assignStaff',function(){
			var schedules = [];
			var staffID = $('#replaced_caregiver_id').val();
			var daa = $('input[name=delivery_address_assign]:checked').val();
			if(staffID == 0){
				alert("Please select a staff for deployment!");
				return false;
			}

			var ivrsType = $('#scheduleStaffAssignModal .ivrs_type').val();

			$('.schedule-sel-chk').each(function(i){
				if($(this).is(':checked')){
					schedules.push($(this).val());
				}
			});

			if(schedules.length <= 0){
				alert("There are no schedules selected for any Operation!");
				return false;
			}

			if(staffID > 0 && schedules.length > 0){

				var roadDistance = $('.road_distance').val();

				$(this).attr('disabled', true);
				$.ajax({
					type: 'POST',
					url: '{{ route('lead.assign-staff-from-schedules') }}',
					data: {ivrs_type:ivrsType, staff_id:staffID, road_distance:roadDistance, schedules:schedules, delivery_address_assign:daa, _token:'{{csrf_token()}}'},
					success: function (d){
						if(d){
							showNotification('bg-green', 'Staff assigned successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
							setTimeout(function(){
								window.location.reload();
							},1000);
						}
						$('#assignStaff').attr('disabled', false);
					},
					error: function (err){
						console.log('Assign Staff error: '+err);
						$('#assignStaff').attr('disabled', false);
					}
				})
			}
		});

		$('.content').on('click','#cancelSchedules',function(){
			var schedules = [];
			var cancelType = $('input[name=cancelType]:checked').val();
			var cancelScheduleComment = $('#cancelScheduleComment').val();
			var ivrsType = $('#cancellationScheduleModal .ivrs_type').val();

			if(cancelType == ''){
				alert("Please select a reason for cancellation!");
				return false;
			}

			$('.schedule-sel-chk').each(function(i){
				if($(this).is(':checked')){
					schedules.push($(this).val());
				}
			});

			if(schedules.length <= 0){
				alert("There are no schedules selected for any Operation!");
				return false;
			}

			if(cancelType != '' && schedules.length > 0){
				$(this).attr('disabled', true);
				$.ajax({
					url: '{{ route('lead.cancel-schedules') }}',
					type: 'POST',
					data: {cancelType: cancelType, cancelScheduleComment: cancelScheduleComment,schedules: schedules,_token:'{{csrf_token()}}'},
					success: function (d){
						if(d){
							showNotification('bg-green', 'Schedule(s) cancelled successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
							setTimeout(function(){
								window.location.reload();
							},1000);
						}
						$(this).attr('disabled', false);
					},
					error: function (err){
						console.log(err);
						$(this).attr('disabled', false);
					}
				})
			}
		});

		$('.content').on('click','#unconfirmSchedule',function(){
			var schedules = [];
			var isNotComplete = false;
			$('.schedule-sel-chk').each(function(i){
				if($(this).is(':checked')){
					schedules.push($(this).val());

					if($(this).attr("data-status") != 'Completed'){
						alert("Please select only Completed schedule!");
						isNotComplete = true;
						return false;
					}
				}
			});
			if(schedules.length <= 0){
				alert("There are no schedules selected for any Operation!");
				return false;
			}

			if(schedules.length > 0 && isNotComplete == false){
				var retVal = confirm("The employee attendance for unconfirmed schedules will also be removed!");
            	if( retVal == true ){
                	$(this).attr('disabled', true);
					$.ajax({
						url: '{{ route('lead.unconfirm-schedules') }}',
						type: 'POST',
						data: {schedules: schedules, _token:'{{csrf_token()}}'},
						success: function (d){
							if(d){
								showNotification('bg-green', 'Schedule(s) Unconfirmed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
								setTimeout(function(){
									$('#viewSchedulesModal').modal('hide');
								},1000);
							}
							$(this).attr('disabled', false);
						},
						error: function (err){
							console.log(err);
							$(this).attr('disabled', false);
						}
					})
                	return true;
            	} else {return false;}
			}
		});

		$('input[type=radio][name=delivery_address]').change(function() {
			$('.serviceRequestCaregiverBlock').css('display','none');
			$('.serviceRequestCaregiverBlock #caregiver_id').val('');
		});

		$('input[type=radio][name=delivery_address_schedule]').change(function() {
			$('.viewScheduleCaregiverBlock').css('display','none');
			$('.viewScheduleCaregiverBlock #caregiver_id').val('');
		});

		$('input[type=radio][name=delivery_address_assign]').change(function() {
			$('.replaceScheduleCaregiverBlock #selectedStaffName').empty();
			$('.replaceScheduleCaregiverBlock #selectedStaffName').append('-');
			$('.replaceScheduleCaregiverBlock #replaced_caregiver_id').val('');
		});

	</script>
	<script type="text/javascript">
		$(window).load(function(){
			if(localStorage.getItem('helpMode') == 'true'){
				if("{{$l->status != 'Converted'}}"){
					swal({
						title: "The lead is in "+ '{{$l->status}}' +" status !",
						text: "Do you want to convert the Lead ?",
						icon: "warning",
						closeOnClickOutside: false,
						closeOnEsc: false,
						buttons: {
							yes: {
								text: "Yes!",
								value: "Yes",
							},
							no: {
								text: "No",
								value: "No",
							},
						},
					})
					.then((value) => {
						switch (value) {
							case "Yes":
								$(".btnChangeLeadStatus").trigger('click');
								break;

							case "No":
								swal("Ok!", "", "success")
								break;
						}
					});
				}else if("{{$l->status == 'Converted'}}"){
					if("{{!count($l->serviceRequests)}}"){
						swal({
							title: "The Episode has no Services !",
							text: "Do you want to create a service entry for the patient ?",
							icon: "warning",
							closeOnClickOutside: false,
							closeOnEsc: false,
							buttons: {
								yes: {
									text: "Yes!",
									value: "Yes",
								},
								no: {
									text: "No",
									value: "No",
								},
							},
						})
						.then((value) => {
							switch (value) {
								case "Yes":
									selectedItem = $('.vertical-tab-menu .list-group > a:eq(2)');
									$(selectedItem).siblings('a.active').removeClass("active");
									$(selectedItem).addClass("active");
									$("div.vertical-tab>div.vertical-tab-content").removeClass("active");
									$("div.vertical-tab>div.vertical-tab-content").eq(2).addClass("active");
									$(".btnAddService").trigger('click');
									break;

								case "No":
									swal("Ok!", "", "success")
									break;
							}
						});
					}
				}
			}
		})
	</script>
@endsection
