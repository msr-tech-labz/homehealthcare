<!DOCTYPE html>
<html>
<head>
	<title>Address and Email Records</title>
	<!-- Bootstrap Core Css -->
	<link href="{{ ViewHelper::ThemePlugin('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/print.css') }}" media="print" />
	<style>
		body {
			font-family: 'Roboto', Arial, Tahoma, sans-serif;
			background: #fff !important;
		}
		.addBtnDiv {
			position: sticky !important;
			top: 0 !important;
		}
		/* Map Styles */
		#infowindow-content .title {
			font-weight: bold;
		}

		#infowindow-content {
			display: none;
		}

		#map #infowindow-content {
			display: inline;
		}

		.pac-card {
			margin: 10px 10px 0 0;
			border-radius: 2px 0 0 2px;
			box-sizing: border-box;
			-moz-box-sizing: border-box;
			outline: none;
			box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
			background-color: #fff;
			font-family: Roboto;
			right: -1px;
		}

		#pac-container {
			padding-bottom: 12px;
			margin-right: 12px;
		}

		#pac-input {
			width: 99%;
			background-color: #fff;
			font-family: Roboto;
			font-size: 15px;
			font-weight: 300;
			margin-left: 12px;
			margin-top: 5px;
			padding: 0 11px 0 13px;
			text-overflow: ellipsis;
			/*width: 400px;*/
		}

		#pac-input:focus {
			border-color: #4d90fe;
		}

		#title {
			color: #fff;
			background-color: #4d90fe;
			font-size: 20px;
			font-weight: 500;
			padding: 6px 12px;
		}

		.pac-container {
		  z-index: 2000;
		}
	</style>
	<link href="{{ ViewHelper::ThemeCss('style.css') }}" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
	<link href="{{ ViewHelper::ThemePlugin('node-waves/waves.css') }}" rel="stylesheet" />
</head>
<body>
    @include('partials.alert')
	<div class="container-fluid content">
		<h2 class="text-center">{{ session('organization_name') }}</h2>
		<table style="width:100%;">
			<tr>
				<td style="width:33%;">
					<h4>
					{{ $patient->first_name.' '.$patient->last_name.' - '.$patient->patient_id }}
					</h4>
				</td>
				<td style="width:33%;">
					<h4 class="text-center">Patient Additional Data</h4>
				</td>
			</tr>
		</table>
		<hr>
		<p class="text-center"><code><strong><u>NOTE</u></strong></code></p>
		<div class="col-md-6" style="border: 1px solid darkgrey;height: 500px;overflow-y: auto;">
			<p class="text-center"><code><strong>One of the below active addresses will be choosen for service delivery address</strong></code></p>
			<h3 class="text-center" style="text-decoration: underline;">Registered Addresses</h3>
			<div class="text-center addBtnDiv" style="margin-bottom: 15px;">
				<button class="btn btn-info waves-effect addr" data-toggle="modal" data-target="#addAddr">Add new address</button>
			</div>
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Type</th>
						<th>Address</th>
						<th>Status</th>
						<th>Edit</th>
					</tr>
				</thead>
				<tbody>
					@forelse($addresses as $address)
					<tr>
						<td>{{ $address->type }}</td>
						<td>{{ $address->address }}</td>
						<td>
							<div class="switch">
								<label><input type="checkbox" name="statusAddr" data-id="{{ Helper::encryptor('encrypt',$address->id) }}" {{ $address->status=='active'?'checked':'' }}><span class="lever"></span></label>
							</div>
						</td>
						<td>
							<button type="button" data-id="{{ Helper::encryptor('encrypt',$address->id) }}" data-type="{{ $address->type }}" data-lat="{{ $address->latitude }}" data-lng="{{ $address->longitude }}" data-address="{{ $address->address }}"  data-url="{{ route('corporate.patient.extraAddressUpdate', ['id' => Helper::encryptor('encrypt',$address->id)]) }}" class="btn btn-success btn-sm waves-effect btnAddr"><i class="material-icons small">edit</i></button>
						</td>
					</tr>
					@empty
					<tr><td colspan="4" class="text-center">No Address found.</td></tr>
					@endforelse
				</tbody>
			</table>
		</div>
		<div class="col-md-6" style="border: 1px solid darkgrey;height: 500px;overflow-y: auto;">
			<p class="text-center"><code><strong>The below active emails will be choosen for sending emails of invoice for the patient.</strong></code></p>
			<h3 class="text-center" style="text-decoration: underline;">Registered Emails</h3>
			<div class="text-center addBtnDiv" style="margin-bottom: 15px;">
				<button class="btn btn-info waves-effect" data-toggle="modal" data-target="#addEmail">Add new email</button>
			</div>
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Name</th>
						<th>Email</th>
						<th>Status</th>
						<th>Edit</th>
					</tr>
				</thead>
				<tbody>
					@forelse($emails as $email)
					<tr>
						<td>{{ $email->name }}</td>
						<td>{{ $email->email }}</td>
						<td>
							<div class="switch">
								<label><input type="checkbox" name="statusEmail" data-id="{{ Helper::encryptor('encrypt',$email->id) }}" {{ $email->status=='active'?'checked':'' }}><span class="lever"></span></label>
							</div>
						</td>
						<td>
							<button type="button" data-id="{{ Helper::encryptor('encrypt',$email->id) }}" data-name="{{ $email->name }}"  data-email="{{ $email->email }}"  data-url="{{ route('corporate.patient.extraEmailUpdate', ['id' => Helper::encryptor('encrypt',$email->id)]) }}" class="btn btn-success btn-sm waves-effect btnEmail"><i class="material-icons small">edit</i></button>
						</td>
					</tr>
					@empty
					<tr><td colspan="4" class="text-center">No Emails found.</td></tr>
					@endforelse
				</tbody>
			</table>
		</div>
			
		<!-- Address Modal -->
		<div class="modal fade" id="addAddr" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-md" role="document">
		        <form class="form-horizontal addrForm" method="POST" action="{{ route('corporate.patient.extraAddressStore') }}">
		            {{ method_field('POST') }}
		            {{ csrf_field() }}
		            <div class="modal-content" style="margin-top: -15px;">
		                <div class="modal-header bg-blue">
		                    <button type="button" class="close" data-dismiss="modal">&times;</button>
		                    <h4 class="modal-title action_addr">New Address</h4>
		                </div>
		                <div class="modal-body">
		                    <div class="row clearfix">
		                        <div class="col-sm-12 form-horizontal">
		                            <div class="row clearfix">
		                            	<div class="col-md-12">
		                            		<div class="col-md-9">
		                            			<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
		                            			    <label for="type">Type</label>
		                            			</div>
		                            			<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
		                            			    <div class="form-group">
		                            			        <div class="form-line">
		                            			            <input type="text" name="type" class="form-control" placeholder="Type (Home, Hospital, etc)" required="required">
		                            			        </div>
		                            			    </div>
		                            			</div>
		                            			<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
		                            			    <label for="address">Address</label>
		                            			</div>
		                            			<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
		                            			    <div class="form-group">
		                            			        <div class="form-line">
		                            			            <textarea rows="3" id="address" name="address" class="form-control" placeholder="Address (Include Full Address)" required="required"></textarea>
		                            			        </div>
		                            			    </div>
		                            			</div>
		                            		</div>
		                            		<div class="col-md-3" style="padding: 0;">
		                            			<input type="text" id="latitude" name="latitude" class="form-control readonly" placeholder="Latitude" required="required">
		                            			<input type="text" id="longitude" name="longitude" class="form-control readonly" placeholder="Longitude" required="required">
		                            		</div>
			                            </div>
			                            <div class="row clearfix"></div>
			                            <div class="pac-card" id="pac-card">
			                            	<div>
			                            		<div id="title">
			                            			Location search
			                            		</div>
			                            	</div>
			                            	<div id="pac-container">
			                            		<input type="text" id="pac-input" placeholder="Type here to search" />
			                            	</div>
			                            </div>
			                            <div id="map" style="width: 570px; height:300px;"></div>
			                            <div id="infowindow-content">
			                            	<img src="" width="16" height="16" id="place-icon">
			                            	<span id="place-name"  class="title"></span><br>
			                            	<span id="place-address"></span>
			                            </div>
		                    		</div>
		                		</div>
		                	</div>		                	
		                </div>
		                <div class="modal-footer">
		                    <button type="submit" class="btn btn-success waves-effect">Save</button>
		                    <button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Cancel</button>
		                    <input type="hidden" name="database_name" value="{{ $database_name }}">
		                    <input type="hidden" name="patient_id" value="{{ \Helper::encryptor('encrypt',$patient->id) }}">
		                </div>
					</div>
		        </form>
			</div>
		</div>
		
		<!-- Email Modal -->
		<div class="modal fade" id="addEmail" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-md" role="document">
		        <form class="form-horizontal emailForm" method="POST" action="{{ route('corporate.patient.extraEmailStore') }}">
		            {{ method_field('POST') }}
		            {{ csrf_field() }}
		            <div class="modal-content">
		                <div class="modal-header bg-blue">
		                    <button type="button" class="close" data-dismiss="modal">&times;</button>
		                    <h4 class="modal-title action_email">New Email</h4>
		                </div>
		                <div class="modal-body">
		                    <div class="row clearfix">
		                        <div class="col-sm-12 form-horizontal">
		                            <div class="row clearfix">
		                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
		                                    <label for="name">Name</label>
		                                </div>
		                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
		                                    <div class="form-group">
		                                        <div class="form-line">
		                                            <input type="text" name="name" class="form-control" placeholder="Name of the Recipient" required="required">
		                                        </div>
		                                    </div>
		                                </div>
		                            </div><br>
		                            <div class="row clearfix">
		                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
		                                    <label for="email">Email</label>
		                                </div>
		                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
		                                    <div class="form-group">
		                                        <div class="form-line">
		                                            <input type="email" name="email" class="form-control" placeholder="Email of the Recipient" required="required">
		                                        </div>
		                                    </div>
		                                </div>
		                            </div><br>
		                        </div>
		                    </div>
		                </div>
		                <div class="modal-footer">
		                    <button type="submit" class="btn btn-success waves-effect">Save</button>
		                    <button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Cancel</button>
		                    <input type="hidden" name="database_name" value="{{ $database_name }}">
		                    <input type="hidden" name="patient_id" value="{{ \Helper::encryptor('encrypt',$patient->id) }}">
		                </div>
					</div>
		        </form>
			</div>
		</div>
			
	</div>
    <script src="{{ ViewHelper::ThemePlugin('jquery/jquery.min.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('bootstrap/js/bootstrap.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('node-waves/waves.js') }}"></script>
    <script src="{{ ViewHelper::ThemeJs('admin.js') }}"></script>
	<script src="{{ ViewHelper::ThemePlugin('bootstrap-notify/bootstrap-notify.js') }}"></script>
	<script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2-azeUdXbeAqQrvGtLZrbsXIFcofE_B8&libraries=places&region=IN"></script>
	
	<script type="text/javascript">
		$('.table').on('click', '.btnAddr', function(){
			id = $(this).data('id');
			type = $(this).data('type');
			address = $(this).data('address');
			lat = $(this).data('lat');
			lng = $(this).data('lng');

			$('.action_addr').html('Edit Address');
			$('.addrForm').attr('action',$(this).data('url'));
			$('.addrForm').attr('method','POST');
			$('.addrForm input[name=_method]').val('POST');
			$('.addrForm input[name=_token]').val('{{ csrf_token() }}');

			$('.addrForm #id').val(id);
			$('.addrForm input[name=type]').val(type);
			$('.addrForm textarea[name=address]').val(address);
			$('.addrForm input[name=latitude]').val(lat);
			$('.addrForm input[name=longitude]').val(lng);

			$('#addAddr').modal('show');
		});

		$('#addAddr').on('hidden.bs.modal', function (e) {
		    $('.action_addr').html('New Address');
		    $('.addrForm').attr('action','{{ route('corporate.patient.extraAddressStore') }}');
		    $('.addrForm').attr('method','POST');
		    $('.addrForm input[name=_method]').val('PUT');
		    $('.addrForm input[name=_token]').val('{{ csrf_token() }}');

		    $('.addrForm input[name=type]').val('');
		    $('.addrForm textarea[name=address]').val('');
			$('.addrForm input[name=latitude]').val('');
			$('.addrForm input[name=longitude]').val('');
		});

		$('.table').on('click', '.btnEmail', function(){
			id = $(this).data('id');
			name = $(this).data('name');
			email = $(this).data('email');

			$('.action_email').html('Edit Email');
			$('.emailForm').attr('action',$(this).data('url'));
			$('.emailForm').attr('method','POST');
			$('.emailForm input[name=_method]').val('POST');
			$('.emailForm input[name=_token]').val('{{ csrf_token() }}');

			$('.emailForm #id').val(id);
			$('.emailForm input[name=name]').val(name);
			$('.emailForm input[name=email]').val(email);

			$('#addEmail').modal('show');
		});

		$('#addEmail').on('hidden.bs.modal', function (e) {
		    $('.action_email').html('New Email');
		    $('.emailForm').attr('action','{{ route('corporate.patient.extraEmailStore') }}');
		    $('.emailForm').attr('method','POST');
		    $('.emailForm input[name=_method]').val('PUT');
		    $('.emailForm input[name=_token]').val('{{ csrf_token() }}');

		    $('.emailForm input[name=name]').val('');
		    $('.emailForm input[name=email]').val('');
		});

		$('.table').on('change','input[name=statusAddr]', function(){
		    var id = $(this).data('id');
		    var status = $(this).is(':checked')?1:0;
		    var db = '{{ $database_name }}';

		    $.ajax({
		        url: '{{ route('corporate.patient.update-status-extra-addr')}}',
		        type: 'POST',
		        data: {id:id, database_name:db, status:status, _token: '{{ csrf_token() }}'},
		        success: function (state){
		            if(state == "statusChanged"){
		                showNotification('bg-green', 'Status changed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
		            }
		        },
		        error: function (error){
		            showNotification('bg-red', 'Something went Wrong !', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
		        }
		    });
		});

		$('.table').on('change','input[name=statusEmail]', function(){
		    var id = $(this).data('id');
		    var status = $(this).is(':checked')?1:0;
		    var db = '{{ $database_name }}';

		    $.ajax({
		        url: '{{ route('corporate.patient.update-status-extra-email')}}',
		        type: 'POST',
		        data: {id:id, database_name:db, status:status, _token: '{{ csrf_token() }}'},
		        success: function (state){
		            if(state == "statusChanged"){
		                showNotification('bg-green', 'Status changed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
		            }
		        },
		        error: function (error){
		            showNotification('bg-red', 'Something went Wrong !', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
		        }
		    });
		});

		function initMap() {

		  var map = new google.maps.Map(document.getElementById('map'), {
		    center: {
		      lat: 16.28004385258969,
		      lng: 78.10594640625004
		    },
		    zoom: 6,
		    mapTypeId: google.maps.MapTypeId.ROADMAP
		  });

		  var marker = new google.maps.Marker({
		    position: {
		      lat: 34.3630003,
		      lng: -84.332207
		    },
		    map: map,
		    animation: google.maps.Animation.DROP,
		    draggable: true
		  });

		  var searchBox = new google.maps.places.SearchBox(document.getElementById('pac-input'));

		  google.maps.event.addListener(searchBox, 'places_changed', function() {
		    var places = searchBox.getPlaces();
		    var bounds = new google.maps.LatLngBounds();
		    var i, place;

		    for (i = 0; place = places[i]; i++) {
		      bounds.extend(place.geometry.location);
		      marker.setPosition(place.geometry.location);
		    }

		    map.fitBounds(bounds);
		    map.setZoom(14);
		    $('#address').html(places[0].formatted_address);
		  });

		  google.maps.event.addListener(marker, 'position_changed', function() {
		    var lat = marker.getPosition().lat();
		    var lng = marker.getPosition().lng();

		    $('#latitude').val(lat);
		    $('#longitude').val(lng);
		  });
		}

		$(".addr").on("shown.bs.modal", function() {
		  google.maps.event.trigger(map, "resize");
		});

		google.maps.event.addDomListener(window, 'load', initMap);

		$(".readonly").on('keydown paste', function(e){
			e.preventDefault();
		});
	</script>
</body>
</html>
