@extends('layouts.corporate-layout')

@section('page_title','Administration -     ')

@section('page.styles')
@endsection

@section('content')
<style>
    .info-box{
        cursor: pointer;
        text-decoration: none !important;
        margin-bottom: 10px !important;
    }
    .info-box .content .text{
        margin-top: 2px;
        font-size: 15px;
        color: #777;
    }
    .info-box .content .number{
        margin-top: -2px;
        font-size: 20px
    }
</style>
<div class="block-header">
    <h2>Administration</h2>
</div>

<div class="block-body">
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{ route('corporate.profile.view') }}" class="info-box hover-expand-effect">
            <div class="icon bg-red">
                <i class="material-icons">person</i>
            </div>
            <div class="content">
                <div class="number">Profile Settings</div>
                <div class="text">Manage your company's profile</div>
            </div>
        </a>
    </div>
    <div class="clearfix"></div><br>
</div>
@endsection
