@extends('layouts.corporate-layout')

@section('page_title','Reports - ')

@section('plugin.styles')
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />

<style>
.demo-radio-button label {min-width:60px !important;}
</style>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="row clearfix">
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('corporate.dashboard') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-8">
                    Reports
                    <small>List of all reports</small>
                </h2>
            </div>
            <div class="body">
            </div>
        </div>
    </div>
</div>

@endsection

@section('plugin.scripts')
<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
<!-- Bootstrap Notify Plugin Js -->
<script src="http://smarthealthconnect.localhost/themes/default/plugins/bootstrap-notify/bootstrap-notify.js"></script>
<script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
@endsection