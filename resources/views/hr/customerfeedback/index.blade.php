@extends('layouts.main-layout')

@section('page_title','Appraisals - ')

@section('plugin.styles')
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
@endsection

@section('page.styles')
<style>
    .card{overflow-x: scroll;}
    .dt-buttons{position:static !important; float:right;}
    .paginate_button {
        cursor: pointer;
        margin:2px;
    }
    .label{font-size:12px !important; color:#333 !important;}
    .radio-buttons div {margin-bottom:0 !important}
    .bootstrap-datetimepicker-widget td,.bootstrap-datetimepicker-widget th{
        border:none !important;
    }
    .timepicker-picker a{box-shadow: none !important;}
    .timepicker-picker tbody{
        max-height: 100px !important;
        max-width: 100px !important;
    }
</style>
<!-- JQuery DataTable Css -->
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header clearfix">
                    <a href="{{ route('hr.index') }}" class="pull-left btn btn-info btn-circle clearLocalStore waves-effect waves-circle waves-float">
                        <i class="material-icons">arrow_back</i>
                    </a>
                    <h2 class="col-sm-8">
                        Customer Feedback
                        <small>Customer Feedback Aganist Staff</small>
                    </h2>
                </div>
                <div class="body">
                    <form action="{{ route('customerfeedback.index') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="month">Month</label>
                                    <div class="col-sm-9">
                                        <div class="form-line">
                                            <select class="form-control input-sm show-tick" name="filter_month" id="filter_month" required>
                                                <option value="" disabled="" selected="">-- Choose Month --</option>
                                                <option value="01">Jan</option>
                                                <option value="02">Feb</option>
                                                <option value="03">Mar</option>
                                                <option value="04">Apr</option>
                                                <option value="05">May</option>
                                                <option value="06">Jun</option>
                                                <option value="07">Jul</option>
                                                <option value="08">Aug</option>
                                                <option value="09">Sep</option>
                                                <option value="10">Oct</option>
                                                <option value="11">Nov</option>
                                                <option value="12">Dec</option>
                                           </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="year">Year</label>
                                    <div class="col-sm-9">
                                        <div class="form-line">
                                            <select class="form-control input-sm show-tick" name="filter_year" id="filter_year" required>
                                                <option value="" disabled="" selected="">-- Choose Year --</option>
                                                <option value="2021">2021</option>
                                                <option value="2020">2020</option>
                                                <option value="2019">2019</option>
                                                <option value="2018">2018</option>
                                           </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-warning">Submit</button>
                            </div>
                        </div>
                    </form>
                    <table class="table table-bordered table-striped table-hover table-condensed appraisalTtable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Emp Id</th>
                                <th>Name</th>
                                <th>Patient</th>
                                <th>Schedule Id</th>
                                <th>Schedule Date</th>
                                <th>Status</th>
                                <th>View/Action</th>
                            </tr>
                        </thead>
                        <tbody>
                           @if(count($feedbacks))
                           		@foreach ($feedbacks as $i => $feedback)
                           			<tr>
                           				<td>{{ $i + 1 }}</td>
                           				<td>{{ $feedback->worklog->caregiver->employee_id }}</td>
                           				<td>{{ $feedback->worklog->caregiver->full_name }}</td>
                           				<td>{{ $feedback->lead->patient->full_name }} - {{ $feedback->lead->patient->patient_id }}</td>
                           				<td>{{ $feedback->worklog->schedule->schedule_id }}</td>
                                        <td>{{ $feedback->worklog->schedule->schedule_date->format('d-m-Y') }}</td>
                                        <td>{{ $feedback->status }}</td>
                           				<td>
                                            @ability('admin','customer-feedback-action-update')
                                            <a class="btn btn-primary btn-sm feedbackmodeldata" data-cp="{{ $feedback->caring_and_patience }}" data-kno="{{ $feedback->knowledge }}" data-cli="{{ $feedback->cleanliness }}" data-rap="{{ $feedback->reliabilty_and_punctuality }}" data-cwp="{{ $feedback->communication_with_patient }}" data-oar="{{ $feedback->over_all_rating }}"      data-ac="{{ $feedback->additional_comments }}" data-id="{{ $feedback->id }}"          data-status="{{ $feedback->status }}" data-action="{{ $feedback->action }}" data-toggle='modal' data-target='#feedbackmodel'>View</a>   
                                            @endability
                                        </td>
                           			</tr>
                           		@endforeach
                           @endif   
                        </tbody>
                    </table><br>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="feedbackmodel" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" style="z-index: 1052 !important">
        <div class="modal-dialog modal-lg" role="document"> 
            <div class="modal-content">
                <div class="modal-header bg-blue">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="largeModalLabel">Feedback</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered table-striped table-hover table-condensed appraisalTtable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Caring And Patience</th>
                                <th>Knowledge</th>
                                <th>Cleanliness</th>
                                <th>Reliabilty and Punctuality</th>
                                <th>Communication with patient</th>
                                <th>Over All Rating(/5)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td id="caring_and_patience"></td>
                                <td id="knowledge"></td>
                                <td id="cleanliness"></td>
                                <td id="reliabilty_and_punctuality"></td>
                                <td id="communication_with_patient"></td>
                                <td id="over_all_rating"></td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr>
                                <th colspan="8">Comment</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="8" rowspan="2" id="additional_comments"></td>
                            </tr>
                        </tbody>
                    </table><br>
                    <form class="form-horizontal feedback_status" action="{{ route('customerfeedback.actionsave') }}" method="POST">
                        {{  csrf_field() }}
                        <textarea style="font-family: monospace !important;" id="fedback_action" name="fedback_action" cols="100" rows="7" wrap="off" onfocus="this.value='';this.onfocus=null;">Write your action...</textarea>
                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                            <label for="actionstatus">Action Status</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <div class="form-group">
                                <select class="form-control show-tick" id="feedback_status" name="feedback_status" data-live-search="true">
                                    <option value="Pending">Pending</option>
                                    <option value="Completed">Completed</option>
                                </select>
                            </div>
                        </div><br><br>
                        <input type="hidden" name="feedback_id" id="feedback_id">
                        <center><button type="submit" id="actionbutton" class="btn bg-teal executesql waves-effect">Action Update</button></center>
                    </form>
                </div>
            </div>
        </div>
    </div>   
@endsection


@section('page.scripts')
<!-- Select Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <!-- Moment Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
    <script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
    <script>
    $(document).ready(function() {
        $('.content').on('click','.feedbackmodeldata', function(e){
            var caring_and_patience = $(this).data('cp');
            var knowledge = $(this).data('kno');
            var cleanliness = $(this).data('cli');
            var reliabilty_and_punctuality = $(this).data('rap');
            var over_all_rating = $(this).data('oar');
            var communication_with_patient = $(this).data('cwp');
            var additional_comments = $(this).data('ac');
            var feedback_id = $(this).data('id');
            var status = $(this).data('status');
            var action = $(this).data('action');
            if(status == 'Completed'){
                $(':input[type="submit"]').prop('disabled', true);
            }
           
            $('#caring_and_patience').html(caring_and_patience);
            $('#knowledge').html(knowledge);
            $('#cleanliness').html(cleanliness);
            $('#reliabilty_and_punctuality').html(reliabilty_and_punctuality);
            $('#over_all_rating').html(over_all_rating);
            $('#communication_with_patient').html(communication_with_patient);
            $('#additional_comments').html(additional_comments);
            $('#feedback_id').val(feedback_id);
            $('#feedback_status').selectpicker('val', status);
            $('#fedback_action').val(action);
        });
    });
    </script>
@endsection
