@extends('layouts.main-layout')

@section('page_title','Appraisals - ')

@section('plugin.styles')
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
@endsection

@section('page.styles')
<style>
    .card{overflow-x: scroll;}
    .dt-buttons{position:static !important; float:right;}
    .paginate_button {
        cursor: pointer;
        margin:2px;
    }
    .label{font-size:12px !important; color:#333 !important;}
    .radio-buttons div {margin-bottom:0 !important}
    .bootstrap-datetimepicker-widget td,.bootstrap-datetimepicker-widget th{
        border:none !important;
    }
    .timepicker-picker a{box-shadow: none !important;}
    .timepicker-picker tbody{
        max-height: 100px !important;
        max-width: 100px !important;
    }
</style>
<!-- JQuery DataTable Css -->
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection

@section('content')
    @ability('admin','employees-list')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header clearfix">
                    <a href="{{ route('hr.index') }}" class="pull-left btn btn-info btn-circle clearLocalStore waves-effect waves-circle waves-float">
                        <i class="material-icons">arrow_back</i>
                    </a>
                    <h2 class="col-sm-8">
                        Appraisals
                        <small>Appraisal of Stars & Warnings of all Employees</small>
                    </h2>
                </div>
                <div class="body">
                    <form action="{{ route('appraisals.index') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="email">Month</label>
                                    <div class="col-sm-9">
                                        <div class="form-line">
                                            <select class="form-control input-sm show-tick" name="filter_month" id="filter_month" required>
                                                <option value="" disabled="" selected="">-- Choose Month --</option>
                                                <option value="01">Jan</option>
                                                <option value="02">Feb</option>
                                                <option value="03">Mar</option>
                                                <option value="04">Apr</option>
                                                <option value="05">May</option>
                                                <option value="06">Jun</option>
                                                <option value="07">Jul</option>
                                                <option value="08">Aug</option>
                                                <option value="09">Sep</option>
                                                <option value="10">Oct</option>
                                                <option value="11">Nov</option>
                                                <option value="12">Dec</option>
                                           </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="email">Year</label>
                                    <div class="col-sm-9">
                                        <div class="form-line">
                                            <select class="form-control input-sm show-tick" name="filter_year" id="filter_year" required>
                                                <option value="" disabled="" selected="">-- Choose Year --</option>
                                                <option value="2020">2020</option>
                                                <option value="2019">2019</option>
                                                <option value="2018">2018</option>
                                           </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-warning">Submit</button>
                            </div>
                        </div>
                    </form>
                    <table class="table table-bordered table-striped table-hover table-condensed appraisalTtable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Emp Id</th>
                                <th>Name</th>
                                <th>Star Of The Month (<span id="starForMonth"></span>)</th>
                                <th>Warning</th>
                                <th>Warning Dates</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($caregivers as $c)
                                <tr>
                                    <td class="text-center">{{ $c['srno'] }}</td>
                                    <td>{{ $c['employee_id'] }}</td>
                                    <td>{{ $c['first_name'].' '.$c['middle_name'].' '.$c['last_name'] }}</td>
                                    <td>
                                        <input type="checkbox" id="star_checkbox_{{ \Helper::encryptor('encrypt',$c['id']) }}" class="filled-in chk-col-amber addStar" data-date="{{ $year }}-{{ $month }}-01" value="{{ \Helper::encryptor('encrypt',$c['id']) }}" @if($c['star']==1) checked @endif >
                                        <label for="star_checkbox_{{ \Helper::encryptor('encrypt',$c['id']) }}">Star</label>
                                    </td>
                                    <td><span class="warning_count_{{ \Helper::encryptor('encrypt',$c['id']) }}">{{ $c['warning'] }}</span> - 
                                        <button type="button" id="add_warning_button_{{ \Helper::encryptor('encrypt',$c['id']) }}" class="btn btn-primary btn-sm addWarning" data-warning="{{ $c['warning'] }}" value="{{ \Helper::encryptor('encrypt',$c['id']) }}" @if($c['warning']>='3') disabled @endif>Add Warning</button>
                                    </td>
                                    <td>
                                        @if($c['warning'] > 0)
                                            1W- @if(!empty($c['warning_1'])) {{ $c['warning_1']->format('d-m-Y') }}, @else Not Captured, @endif
                                        @endif    
                                        @if($c['warning'] > 1)    
                                            2W- @if(!empty($c['warning_2'])) {{ $c['warning_2']->format('d-m-Y') }}, @else Not Captured, @endif
                                        @endif    
                                        @if($c['warning'] > 2)    
                                            3W- @if(!empty($c['warning_3'])) {{ $c['warning_3']->format('d-m-Y') }} @else Not Captured @endif
                                        @endif    
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5" class="text-center">No appraisal(s) found</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table><br>
                </div>
            </div>
        </div>
    </div>
    @endability
@endsection


@section('page.scripts')
<!-- Select Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <!-- Moment Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
    <script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
    <script>
    $(document).ready(function() {
        $('#filter_month').selectpicker('val','{{ isset($month)?$month:0 }}');
        $('#filter_year').selectpicker('val','{{ isset($year)?$year:0 }}');

        $('#starForMonth').text($('#filter_month option:selected').html());

        if({{ count($caregivers) }}){
            $('.appraisalTtable').DataTable({
                "lengthMenu": [ [25, 50, 100, -1], [25, 50, 100, "All"] ],
            });
        }
    });

    $('.content').on('click','.addStar',function(){
        var cid = $(this).val();
        var date = $(this).attr('data-date');
        var checkbox = $(this).prop('checked');
        if(checkbox==true) status='1';
        else status='0';
        $('.addStar').attr('disabled', true);

        $.ajax({
            url: '{{ route('appraisals.mark') }}',
            type: 'POST',
            data: {_token:'{{csrf_token()}}', cid:cid, date:date, status:status, star:true},
            success: function(data){
                showNotification('bg-green', 'Appraisal Marked Successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                $('.addStar').attr('disabled', false);
            },
            error: function (err){
                console.log(err);
                showNotification('bg-red', 'Something Went Wrong, Please Refresh', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
            }
        })
    });

    $('.content').on('click','.addWarning',function(){
        var cid = $(this).val();
        var oldWarning = parseInt($('.warning_count_'+cid).text());
        if(oldWarning >= 3){
            showNotification('bg-red', 'Already reached the maximum warning!', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
            return false;
        }
        $(this).attr('disabled', true);

        $.ajax({
            url: '{{ route('appraisals.mark') }}',
            type: 'POST',
            data: {_token:'{{csrf_token()}}', cid:cid, warning:true},
            success: function(data){
                showNotification('bg-green', 'Appraisal Marked Successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                $('.warning_count_'+cid).text(oldWarning+1);
                if($('.warning_count_'+cid).text() < '3') $('#add_warning_button_'+cid).attr('disabled', false);
            },
            error: function (err){
                console.log(err);
                showNotification('bg-red', 'Something Went Wrong, Please Refresh', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
            }
        })
    });
    </script>
@endsection
