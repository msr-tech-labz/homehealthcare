@extends('layouts.main-layout')

@section('page_title','Leaves - HR | ')

@section('plugin.styles')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">

@endsection
<style type="text/css">
  .bootstrap-datetimepicker-widget table td.disabled{
    color: red !important;
  }
  .leavesSearch{
      background-image: url('/img/searchicon.png');
      background-position: 10px 12px;
      background-repeat: no-repeat;
      width: 100%;
      font-size: 14px;
      padding: 12px 35px 10px 35px;
      border: none;
  }
</style>
@section('content')
@ability('admin','leaves-view')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('hr.index') }}" class="pull-left btn btn-info btn-circle clearLocalStore waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-6">
                   Employee Leaves
                   <small>Leave Stats of all Employees</small>
               </h2>
               <div class="col-sm-5 text-right">
                   @ability('admin','leaves-create')
                       <button type="button" class="btn btn-primary waves-effect" data-toggle="modal" data-target="#leaveRequestModal" style="line-height: 1.9;">Add Leave Request</button>
                   @endability
               </div>
           </div>
           <div class="body">
               <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#home_with_icon_title" data-toggle="tab">
                            <i class="material-icons">announcement</i> PENDING
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#profile_with_icon_title" data-toggle="tab">
                            <i class="material-icons">done</i> APPROVED
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#messages_with_icon_title" data-toggle="tab">
                            <i class="material-icons">clear</i> DECLINED
                        </a>
                    </li>
                </ul>

               <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="home_with_icon_title">
                        <b>Pending Leaves</b>
                        <div class="body">
                        <input type="text" class="leavesSearch" id="pendingTableInput" placeholder="Search for caregiver's first name">
                            <div id="tablePendingLeaves">
                                <div class="table-responsive">
                                    <table style="zoom:85%;" class="table table-bordered table-striped table-hover table-condensed js-basic-example">
                                       <thead>
                                           <tr>
                                               <th width="3%">#</th>
                                               <th width="15%">Caregiver Name</th>
                                               <th>Date Received</th>
                                               <th>Leave Date</th>
                                               <th width="15%">Reason</th>
                                               <th>Type of leave</th>
                                               <th>Status</th>
                                               <th>Approver</th>
                                               <th style="text-align: center;">Action</th>
                                           </tr>
                                       </thead>
                                       <tbody>
                                           @forelse($pendingLeaves as $i => $pl)
                                           <tr>
                                               <td>{{ $i + 1 }}</td>
                                               <td>
                                                   {{ $pl->caregiver->fullname }}
                                               </td>
                                               <td>
                                                   {{ $pl->created_at->format('d-m-Y') }}
                                               </td>
                                               <td>
                                                   {{ $pl->date->format('d-m-Y') }}
                                               </td>
                                               <td>
                                                   {{ $pl->reason }}
                                               </td>
                                               <td>
                                                   {{ $pl->type }}
                                               </td>
                                               <td>
                                                   {{ $pl->status }}
                                               </td>
                                               <td>
                                                   {{ $pl->approver->fullname ?? 'Super Admin' }}
                                               </td>
                                               <td style="text-align: center;">
                                                  <a href="javascript:void(0);" data-leave-id="{{ Helper::encryptor('encrypt',$pl->id) }}" data-emp="{{ $pl->caregiver->fullname }}" data-employee-id="{{ $pl->caregiver->employee_id }}" data-date="{{ $pl->date->format('d-m-Y') }}" data-type="{{ $pl->type }}" data-reason="{{ $pl->reason }}" data-approver-id="{{ Helper::encryptor('encrypt',$pl->approver_id) }}" class="btn btn-primary btn-lg respondLeave">View Request</a>
                                               </td>
                                           </tr>
                                           @empty
                                           <tr>
                                               <td colspan="9" class="text-center">
                                                   No record(s) found.
                                               </td>
                                           </tr>
                                           @endforelse
                                       </tbody>
                                    </table>
                                    {{ $pendingLeaves->links() }}
                                    <p class="text-right" style="margin-bottom:0;">
                                        {{ 'Showing '.$pendingLeaves->firstItem().' to '.$pendingLeaves->lastItem().' out of Total '.$pendingLeaves->total() }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="profile_with_icon_title">
                        <b>Approved Leaves</b>
                        <div class="body">
                        <input type="text" class="leavesSearch" id="approvedTableInput" placeholder="Search for caregiver's first name">
                            <div id="tableApprovedLeaves">
                                <div class="table-responsive">
                                    <table style="zoom:85%;" class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTableApprove">
                                       <thead>
                                           <tr>
                                               <th width="3%">#</th>
                                               <th width="15%">Caregiver Name</th>
                                               <th>Date Received</th>
                                               <th>Leave Date</th>
                                               <th width="15%">Reason</th>
                                               <th>Type of leave</th>
                                               <th>Status</th>
                                               <th>Approver</th>
                                               <th>Approved By</th>
                                               <th class="text-center">Action</th>
                                           </tr>
                                       </thead>
                                       <tbody>
                                                 @forelse($approvedLeaves as $i => $al)
                                                 <tr>
                                                     <td>{{ $i + 1 }}</td>
                                                     <td>
                                                         {{ $al->caregiver->fullname }}
                                                     </td>
                                                     <td>
                                                         {{ $al->created_at->format('d-m-Y') }}
                                                     </td>
                                                     <td>
                                                         {{ $al->date->format('d-m-Y') }}
                                                     </td>
                                                     <td>
                                                         {{ $al->reason }}
                                                     </td>
                                                     <td>
                                                         {{ $al->type }}
                                                     </td>
                                                     <td>
                                                         {{ $al->status }}
                                                     </td>
                                                     <td>
                                                         {{ $al->approver->full_name ?? 'Super Admin' }}
                                                     </td>
                                                     <td>
                                                         {{ $al->approvedby->full_name ?? 'Super Admin' }}
                                                     </td>
                                                     <td style="text-align: center;">
                                                        <a data-toggle='modal' data-target='#leaveEditModal' data-leave-id="{{ Helper::encryptor('encrypt',$al->id) }}" data-caregiver-name="{{ $al->caregiver->fullname }}" data-employee-id="{{ $al->caregiver->employee_id }}" data-date="{{ $al->date->format('d-m-Y') }}" data-lType="{{ $al->type }}" data-reason="{{ $al->reason }}" data-approver-id="{{ Helper::encryptor('encrypt',$al->approver_id) }}" class='btn waves-effect btn-warning leaveEdit' title='Edit leave'>Edit</a>
                                                        <a href="{{ route('leaves.leavesrevoke',['id' => Helper::encryptor('encrypt',$al->id)]) }}" onclick=\"return confirm('Are you sure to revoke the Leave?')\" class='btn waves-effect btn-danger' style='float: right;' title='Cancel leave'>Revoke</a>
                                                     </td>
                                                 </tr>
                                                 @empty
                                                 <tr>
                                                     <td colspan="10" class="text-center">
                                                         No record(s) found.
                                                     </td>
                                                 </tr>
                                                 @endforelse
                                       </tbody>
                                    </table>
                                    {{ $approvedLeaves->links() }}
                                    <p class="text-right" style="margin-bottom:0;">
                                        {{ 'Showing '.$approvedLeaves->firstItem().' to '.$approvedLeaves->lastItem().' out of Total '.$approvedLeaves->total() }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="messages_with_icon_title">
                       <b>Declined Leaves</b>
                       <div class="body">
                        <input type="text" class="leavesSearch" id="declinedTableInput" placeholder="Search for caregiver's first name">
                            <div id="tableDeclinedLeaves">
                                <div class="table-responsive">
                                    <table style="zoom:85%;" class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTableDecline">
                                       <thead>
                                           <tr>
                                               <th width="3%">#</th>
                                               <th width="15%">Caregiver Name</th>
                                               <th>Date Received</th>
                                               <th>Leave Date</th>
                                               <th width="15%">Reason</th>
                                               <th>Type of leave</th>
                                               <th>Status</th>
                                               <th>Approver</th>
                                               <th>Declined By</th>
                                           </tr>
                                       </thead>
                                       <tbody>
                                           @forelse($declinedLeaves as $i => $dl)
                                           <tr>
                                               <td>{{ $i + 1 }}</td>
                                               <td>
                                                   {{ (empty($dl->name) && isset($dl->caregiver))?$dl->caregiver->first_name.' '.$dl->caregiver->middle_name.' '.$dl->caregiver->last_name:$dl->name }}
                                               </td>
                                               <td>
                                                   {{ $dl->created_at->format('d-m-Y') }}
                                               </td>
                                               <td>
                                                   {{ $dl->date->format('d-m-Y') }}
                                               </td>
                                               <td>
                                                   {{ $dl->reason }}
                                               </td>
                                               <td>
                                                   {{ $dl->type }}
                                               </td>
                                               <td>
                                                   {{ $dl->status }}
                                               </td>
                                               <td>
                                                   {{ $dl->approver->full_name ?? 'Super Admin' }}
                                               </td>
                                               <td>
                                                   {{ $dl->approvedby->full_name ?? 'Super Admin' }}
                                               </td>
                                           </tr>
                                           @empty
                                           <tr>
                                               <td colspan="9" class="text-center">
                                                   No record(s) found.
                                               </td>
                                           </tr>
                                           @endforelse
                                       </tbody>
                                    </table>
                                    {{ $declinedLeaves->links() }}
                                    <p class="text-right" style="margin-bottom:0;">
                                        {{ 'Showing '.$declinedLeaves->firstItem().' to '.$declinedLeaves->lastItem().' out of Total '.$declinedLeaves->total() }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

   <!-- New Leave Request Modal -->
   <div class="modal fade" id="leaveRequestModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" style="z-index: 1052 !important">
       <div class="modal-dialog" role="document">
           <form id="leaveRequestForm" action="{{ route('leaves.submit-request') }}" method="POST">
               {{  csrf_field() }}
               <div class="modal-content">
                   <div class="modal-header bg-blue">
                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                       <h4 class="modal-title" id="largeModalLabel">Leave Request Form</h4>
                   </div>
                   <div class="modal-body">
                       <div class="row clearfix">
                           <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                               <label for="caregiver_id">Employee</label>
                           </div>
                           <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                               <div class="form-group">
                                   <div class="form-line">
                                       <select class="form-control show-tick cList" name="caregiver_id" data-live-search="true" required="">
                                           <option value="">-- Select Employee --</option>
                                    @if(isset($caregivers))
                                        @foreach ($caregivers as $e)
                                            <option value="{{ Helper::encryptor('encrypt',$e->id) }}" data-content='<img src="@if($e->profile_image && file_exists(public_path('uploads/caregivers/'.$e->profile_image))){{ asset('uploads/caregivers/'.$e->profile_image)}}@else{{ asset('/img/user.png') }}@endif" width="40" height="40" class="img-circle" style="float:left;">&nbsp;&nbsp;{!! $e->full_name.' - <code>'.$e->employee_id.'</code>' !!}<small style="display:block;padding-left:48px;color:black !important;">{{ isset($e->professional->specializations)?$e->professional->specializations->specialization_name:'' }}</small>'></option>
                                        @endforeach
                                    @endif
                                       </select>
                                   </div>
                               </div>
                           </div>
                       </div>
                       <div class="row clearfix">
                           <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label">
                               <label for="leave_period">Leave Dates</label>
                           </div>
                           <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                               <div class="form-group">
                                   <div class="form-line">
                                    <div class=flatpickr>
                                       <input type="text" class="form-control leavedates" name="leavedates" placeholder="Leave Dates" required="">
                                       <a class="input-button" title="clear" data-clear>
                                        <i class="icon-close"></i>
                                        </a>
                                      </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                       <div class="row clearfix">
                           <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                               <label for="type">Leave Type</label>
                           </div>
                           <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                               <div class="form-group">
                                   <select class="form-control show-tick" name="type" data-live-search="true" required="">
                                       <option value="">-- Select Leave Type --</option>
                                       <option value="CO">Comp-Off (CO)</option>
                                       <option value="CL">Casual Leave (CL)</option>
                                       <option value="SL">Sick Leave (SL)</option>
                                       <option value="ML">Maternity Leave (ML)</option>
                                       <option value="AC">Abscond (AC)</option>
                                   </select>
                               </div>
                           </div>
                       </div>
                       <div class="row clearfix">
                           <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label">
                               <label for="reason">Reason</label>
                           </div>
                           <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                               <div class="form-group">
                                   <div class="form-line">
                                       <textarea class="form-control" name="reason" placeholder="Comments / Reason / Notes (if any)"></textarea>
                                   </div>
                               </div>
                           </div>
                       </div>
                       <div class="row clearfix">
                           <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                               <label for="approver_id">Approver</label>
                           </div>
                           <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                               <div class="form-group">
                                   <select class="form-control show-tick" name="approver_id" data-live-search="true" required="">
                                       <option value="">-- Select Approver --</option>
                               @if(isset($managers))
                                   @foreach ($managers->all() as $m)
                                       <option value="{{ Helper::encryptor('encrypt',$m->id) }}" data-content='<img src="@if($m->profile_image && file_exists(public_path('uploads/caregivers/'.$m->profile_image))){{ asset('uploads/caregivers/'.$m->profile_image)}}@else{{ asset('/img/user.png') }}@endif" width="40" height="40" class="img-circle" style="float:left;">&nbsp;&nbsp;{!! $m->full_name.' - <code>'.$m->employee_id.'</code>' !!}<small style="display:block;padding-left:48px;color:black !important;">{{ isset($m->professional->specializations)?$m->professional->specializations->specialization_name:'' }}</small>'></option>
                                   @endforeach
                               @endif
                                   </select>
                               </div>
                           </div>
                       </div><hr>
                       <div class="row clearfix">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                               <div class="form-group text-center">
                                   <button type="submit" class="btn btn-success btn-lg waves-effect submitLeaveRequest">Submit</button>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </form>
       </div>
   </div>

   <!-- Leave Process Modal -->
   <div class="modal fade" id="leaveProcessModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" style="z-index: 1052 !important">
       <div class="modal-dialog" role="document">
           <form id="leaveProcessForm" action="{{ route('leaves.process-request') }}" method="POST">
               {{  csrf_field() }}
               <div class="modal-content">
                   <div class="modal-header bg-blue">
                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                       <h4 class="modal-title" id="largeModalLabel">View - Leave Request</h4>
                   </div>
                   <div class="modal-body">
                      <div class="row clearfix">
                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                          <label for="caregiver_id">Employee</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <div class="form-group">
                              <p class="form-control leave_req_emp"></p>
                          </div>
                        </div>
                      </div>
                      <div class="row clearfix">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label">
                          <label for="leave_period">Leave Date</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <div class="form-group">
                              <p class="form-control leave_req_date"></p>
                          </div>
                        </div>
                      </div>
                      <div class="row clearfix">
                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                          <label for="type">Leave Type</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <div class="form-group">
                            <select class="form-control show-tick leave_req_type" name="type" data-live-search="true" required="">
                              <option value="">-- Select Leave Type --</option>
                              <option value="CO">Comp-Off (CO)</option>
                              <option value="CL">Casual Leave (CL)</option>
                              <option value="SL">Sick Leave (SL)</option>
                              <option value="ML">Maternity Leave (ML)</option>
                              <option value="AC">Abscond (AC)</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="row clearfix">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label">
                          <label for="reason">Reason</label>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                          <div class="form-group">
                            <div class="form-line">
                              <textarea class="form-control leave_req_reason" name="reason" placeholder="Comments / Reason / Notes (if any)"></textarea>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row clearfix">
                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                          <label for="approver_id">Approver</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <div class="form-group">
                            <select class="form-control show-tick approver_id" name="approver_id" data-live-search="true" required="">
                              <option value="">-- Select Approver --</option>
                              @if(isset($caregivers))
                              @foreach ($caregivers->where('professional.role','Manager')->all() as $m)
                              <option value="{{ Helper::encryptor('encrypt',$m->id) }}" data-content='<img src="@if($m->profile_image && file_exists(public_path('uploads/caregivers/'.$m->profile_image))){{ asset('uploads/caregivers/'.$m->profile_image)}}@else{{ asset('/img/user.png') }}@endif" width="40" height="40" class="img-circle" style="float:left;">&nbsp;&nbsp;{!! $m->full_name.' - <code>'.$m->employee_id.'</code>' !!}<small style="display:block;padding-left:48px;color:black !important;">{{ isset($m->professional->specializations)?$m->professional->specializations->specialization_name:'' }}</small>'></option>
                              @endforeach
                              @endif
                            </select>
                          </div>
                        </div>
                      </div>                      
                      <div class="row clearfix">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label">
                          <label for="reason">Status</label>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                          <div class="form-group">
                              <input name="status" type="radio" id="status_approve" class="with-gap radio-col-deep-purple status_radio" value="Approved" checked/>
                              <label for="status_approve">Approve</label>
                              <input name="status" type="radio" id="status_reject" class="with-gap radio-col-deep-purple status_radio" value="Declined" />
                              <label for="status_reject">Reject</label>
                          </div>
                        </div>
                      </div>
                       <hr>
                       <div class="row clearfix">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                               <div class="form-group text-center">
                                   <input type="hidden" name="id" value="" />
                                   <input type="hidden" name="ivrs_type" value="ivrs_leave_event_status">
                                   <button type="submit" class="btn btn-success btn-lg waves-effect">Update</button>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </form>
       </div>
   </div>

   <!-- Leave Edit Modal-->
   <div class="modal fade" id="leaveEditModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" style="z-index: 1052 !important">
      <div class="modal-dialog" role="document">
        <form id="leaveRequestForm" action="{{ route('leaves.leavesedit') }}" method="POST">
          {{  csrf_field() }}
          <div class="modal-content">
            <div class="modal-header bg-blue">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" id="largeModalLabel">Leave Approved Edit Form</h4>
            </div>
            <div class="modal-body">
              <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                  <label for="caregiver_id">Employee</label>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                  <div class="form-group">
                      <p class="form-control caregiver_name"></p>
                  </div>
                </div>
              </div>
              <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label">
                  <label for="leave_period">Leave Date</label>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                  <div class="form-group">
                      <p class="form-control leave_date"></p>
                  </div>
                </div>
              </div>
              <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                  <label for="type">Leave Type</label>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                  <div class="form-group">
                    <select class="form-control show-tick leave_req_type" name="type" data-live-search="true" required="">
                      <option value="">-- Select Leave Type --</option>
                      <option value="CO">Comp-Off (CO)</option>
                      <option value="CL">Casual Leave (CL)</option>
                      <option value="SL">Sick Leave (SL)</option>
                      <option value="ML">Maternity Leave (ML)</option>
                      <option value="AC">Abscond (AC)</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label">
                  <label for="reason">Reason</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                  <div class="form-group">
                    <div class="form-line">
                      <textarea class="form-control leave_req_reason" name="reason" placeholder="Comments / Reason / Notes (if any)"></textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                  <label for="approver_id">Approver</label>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                  <div class="form-group">
                    <select class="form-control show-tick approver_id" name="approver_id" data-live-search="true" required="">
                      <option value="">-- Select Approver --</option>
                      @if(isset($caregivers))
                      @foreach ($caregivers->where('professional.role','Manager')->all() as $m)
                      <option value="{{ Helper::encryptor('encrypt',$m->id) }}" data-content='<img src="@if($m->profile_image && file_exists(public_path('uploads/caregivers/'.$m->profile_image))){{ asset('uploads/caregivers/'.$m->profile_image)}}@else{{ asset('/img/user.png') }}@endif" width="40" height="40" class="img-circle" style="float:left;">&nbsp;&nbsp;{!! $m->full_name.' - <code>'.$m->employee_id.'</code>' !!}<small style="display:block;padding-left:48px;color:black !important;">{{ isset($m->professional->specializations)?$m->professional->specializations->specialization_name:'' }}</small>'></option>
                      @endforeach
                      @endif
                    </select>
                  </div>
                </div>
              </div><hr>
              <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group text-center">
                    <button type="submit" class="btn btn-success btn-lg waves-effect editLeaveRequest">Save</button>
                  </div>
                </div>
              </div>
              <input type="hidden" name="leave_id" id="leave_id">
            </div>
          </div>
        </form>
      </div>
  </div>
@endability
@endsection


@section('plugin.scripts')
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript">
    $(function(){
      $('.content').on('click','.clearLocalStore', function() {
          return localStorage.removeItem('leavesLastTab');
      });

      // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            // save the latest tab; use cookies if you like 'em better:
            localStorage.setItem('leavesLastTab', $(this).attr('href'));
        });

        // go to the latest tab, if it exists:
        var lastTab = localStorage.getItem('leavesLastTab');
        if (lastTab) {
            $('[href="' + lastTab + '"]').tab('show');
        }
    });
  </script>
    <!-- Moment Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <!-- <script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script> -->

    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.4.3/plugins/confirmDate/confirmDate.js"></script>

    <!-- Bootstrap Select-->
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script> -->

  <!--  Sweetalert -->
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script type"text/javascript">
        $(function(){
          $('.leavedates').attr('disabled',true);
          fpDate = $('.leavedates').flatpickr({
              mode: "multiple",
              altFormat: "d-m-Y"
          });
        });

        $('.content').on('click','.respondLeave', function(e){
            var leaveID = $(this).data('leave-id');
            var caregiverName = $(this).data('emp');
            var employeeID = $(this).data('employee-id');
            var approverID = $(this).data('approver-id');
            var date = $(this).data('date');
            var type = $(this).data('type');
            var reason = $(this).data('reason');

            $('#leaveProcessModal .leave_req_emp').html(caregiverName+' - <small>'+employeeID+'</small>');
            $('#leaveProcessModal .leave_req_date').html(date);
            $('#leaveProcessModal .leave_req_type').selectpicker('val',type);
            $('#leaveProcessModal .leave_req_reason').html(reason);
            $('#leaveProcessModal .approver_id').selectpicker('val',approverID);
            $('#leaveProcessModal input[name=id]').val(leaveID);
            $('#leaveProcessModal').modal();
        });

        $('.content').on('click','.leaveEdit', function(e){
            var leaveID = $(this).data('leave-id');
            var caregiverName = $(this).data('caregiver-name');
            var employeeID = $(this).data('employee-id');
            var approverID = $(this).data('approver-id'); console.log(approverID);
            var date = $(this).data('date');
            var type = $(this).data('ltype');
            var reason = $(this).data('reason');

            $('#leaveEditModal .caregiver_name').html(caregiverName+' - <small>'+employeeID+'</small>');
            $('#leaveEditModal .leave_date').html(date);
            $('#leaveEditModal .leave_req_type').selectpicker('val',type);
            $('#leaveEditModal .leave_req_reason').html(reason);
            $('#leaveEditModal .approver_id').selectpicker('val',approverID);
            $('#leaveEditModal input[name=leave_id]').val(leaveID);
          });

        $('.content').on('change','.status_radio', function(e){
            if($(this).val() == 'Escalate'){
                $('#leaveProcessModal table tr#escalate_row').removeClass('hide');
                $('#escalate_row > td > div > div > select').attr('required',true);
            }else{
                $('#leaveProcessModal table tr#escalate_row').addClass('hide');
                $('#escalate_row > td > div > div > select').attr('required',false);
            }
        });

        $('.content').on('click','.submitLeaveRequest',function(e){
            e.preventDefault();

            validateForm();
        });

        function validateForm(){
            $('.submitLeaveRequest').prop('disabled',true);
            var caregiverID = $('#leaveRequestModal select[name=caregiver_id]').val();
            var leaveType = $('#leaveRequestModal select[name=type]').val();
            var approverID = $('#leaveRequestModal select[name=approver_id]').val();
            var leaveDates = $('#leaveRequestModal input[name=leavedates]').val();

            if(caregiverID == ''){
                swal("Select Employee!","","warning");
                $('.submitLeaveRequest').prop('disabled',false);
                return false;
            }

            if(leaveDates == ''){
                swal("Select atleast one Date!","","warning");
                $('.submitLeaveRequest').prop('disabled',false);
                return false;
            }

            if(approverID == ''){
                swal("Select Approver!","","warning");
                $('.submitLeaveRequest').prop('disabled',false);
                return false;
            }

            if(leaveType == ''){
                swal("Select Leave Type!","","warning");
                $('.submitLeaveRequest').prop('disabled',false);
                return false;
            }

            if(caregiverID == approverID){
                swal("Employee and Approver cannot be same!","","warning");
                $('.submitLeaveRequest').prop('disabled',false);
                return false;
            }
                        
            $('#leaveRequestModal #leaveRequestForm').submit();
            $('.submitLeaveRequest').prop('disabled',true);
        }

        $(".cList").change(function() {
            var eID = $(".cList option:selected").val();
            dates = [];
            $.ajax({
                url: '{{ route('leaves.get-blocked-dates') }}',
                type: 'POST',
                data: {id: eID, '_token': '{{ csrf_token() }}'},
                success: function(data){
                  $('#leaveRequestForm .leavedates').attr('disabled',false);
                  dates = data;
                  fpDate = $('#leaveRequestForm .leavedates').flatpickr({
                          minDate: moment().subtract(1, 'months').startOf('month').toDate(),
                          disable: dates,
                          mode: "multiple",
                          altFormat: "d-m-Y"
                      });                     

                    $(".fromdate").on("dp.change", function (e) {
                      $('.todate').data("DateTimePicker").minDate(e.date);
                    });
                    $(".todate").on("dp.change", function (e) {
                      $('.fromdate').data("DateTimePicker").maxDate(e.date);
                    });

                    $(".fromdate,.todate").on("dp.change",function (e) {
                        if ($(".fromdate").val() && $(".todate").val()) {
                            var date1Arr = $(".fromdate").val().split('-');
                            var newDate1Arr = date1Arr[2] + '-' + date1Arr[1] + '-' + date1Arr[0];

                            var date2Arr = $(".todate").val().split('-');
                            var newDate2Arr = date2Arr[2] + '-' + date2Arr[1] + '-' + date2Arr[0];

                            $.each(dates, function (item,blockedDate) {
                                var date = blockedDate;
                                console.log(blockedDate);
                                if (new Date(newDate1Arr) <= new Date(date) && new Date(date) <= new Date(newDate2Arr)) {
                                    alert("The range contains not selectable dates.");
                                    $(".todate").val("");
                                    $('.fromdate').data("DateTimePicker").maxDate(false);
                                    $('.todate').data("DateTimePicker").minDate(false);
                                    return false;
                                }
                            });
                        }
                    });
                },
                error: function(err){
                    console.log(err);
                }
            });
        });

        $(".fromdate").on("dp.change", function (e) {
          $('input[name=date_from]').val(moment(e.date).format('YYYY-MM-DD'));
        });
        $(".todate").on("dp.change", function (e) {
          $('input[name=date_to]').val(moment(e.date).format('YYYY-MM-DD'));
        });
    </script>

<script>
// pending leaves paginator
$('.content').on('keyup','#pendingTableInput', function(){
    var searchString = $(this).val();
    $.ajax({
        url: '{{ route('leaves.pending-leaves-paginator') }}',
        data: { searchString: searchString, _token: '{{ csrf_token() }}'},
    }).done(function(data){
        $('#tablePendingLeaves').html(data);
    });
});
$(document).on('click','#tablePendingLeaves .pagination a', function(e){
    e.preventDefault();
    var searchString = $('#pendingTableInput').val();
    var url = $(this).attr('href').split('page=')[1];
    $.ajax({
        url: '{{ route('leaves.pending-leaves-paginator') }}?page='+url,
            data: { searchString: searchString, _token: '{{ csrf_token() }}'},
    }).done(function(data){
        $('#tablePendingLeaves').html(data);
    });
});

// approved leaves paginator
$('.content').on('keyup','#approvedTableInput', function(){
    var searchString = $(this).val();
    $.ajax({
        url: '{{ route('leaves.approved-leaves-paginator') }}',
        data: { searchString: searchString, _token: '{{ csrf_token() }}'},
    }).done(function(data){
        $('#tableApprovedLeaves').html(data);
    });
});
$(document).on('click','#tableApprovedLeaves .pagination a', function(e){
    e.preventDefault();
    var searchString = $('#approvedTableInput').val();
    var url = $(this).attr('href').split('page=')[1];
    $.ajax({
        url: '{{ route('leaves.approved-leaves-paginator') }}?page='+url,
        data: { searchString: searchString, _token: '{{ csrf_token() }}'},
    }).done(function(data){
        $('#tableApprovedLeaves').html(data);
    });
});

// declined leaves paginator
$('.content').on('keyup','#declinedTableInput', function(){
    var searchString = $(this).val();
    $.ajax({
        url: '{{ route('leaves.declined-leaves-paginator') }}',
        data: { searchString: searchString, _token: '{{ csrf_token() }}'},
    }).done(function(data){
        $('#tableDeclinedLeaves').html(data);
    });
});
$(document).on('click','#tableDeclinedLeaves .pagination a', function(e){
    e.preventDefault();
    var searchString = $('#declinedTableInput').val();
    var url = $(this).attr('href').split('page=')[1];
    $.ajax({
        url: '{{ route('leaves.declined-leaves-paginator') }}?page='+url,
        data: { searchString: searchString, _token: '{{ csrf_token() }}'},
    }).done(function(data){
        $('#tableDeclinedLeaves').html(data);
    });
});
</script>
@endsection
