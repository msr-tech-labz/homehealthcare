@extends('layouts.main-layout')

@section('page_title','HR - Dashboard | ')

@section('plugin.styles')
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
<style>
    .plus:after{
        content: ' + ';
        font-size: 18px;
        font-weight: bold;
    }
    .wizard .content{
        min-height: auto !important;
    }
    .wizard > .steps a{
        text-align: left !important;
        border: 1px solid #ccc;
    }
    .nav-justified li a{
        font-size: 16px;
    }
    .nav-justified li:hover{
        background: #eee !important;
    }
    .info-box .content .text{
        font-size: 13px !important;
        margin-top: 6px !important;
        margin-bottom: 6px !important;
        text-decoration: none !important;
    }
    .info-box .content .number {
        font-size: 18px !important;
    }
    .info-box:hover, .info-box:focus{
        font-size: 19px !important;
        text-decoration: none !important;
    }
</style>
@endsection

@section('content')
<div class="row clearfix" style="margin-bottom:30px;">
    <div class="col-lg-3  col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-expand-effect">
            <div class="icon">
                <i class="material-icons">people</i>
            </div>
            <div class="content">
                <div class="text">Employees</div>
                <div class="number count-to">{{ $stats['emp'] ?? 0 }}</div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-cyan hover-expand-effect">
            <div class="icon">
                <i class="material-icons">people</i>
            </div>
            <div class="content">
                <div class="text">Active Employees</div>
                <div class="number count-to">{{ $stats['active'] ?? 0 }} </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3  col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-expand-effect">
            <div class="icon">
                <i class="material-icons">people</i>
            </div>
            <div class="content">
                <div class="text">Bench Employees</div>
                <div class="number count-to">{{ $stats['bench'] ?? 0 }}</div>
            </div>
        </div>
    </div>
    
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-cyan hover-expand-effect">
            <div class="icon">
                <i class="material-icons">receipt</i>
            </div>
            <div class="content">
                <div class="text">Leave Approval</div>
                <div class="number count-to">{{ $stats['leave'] ?? 0 }} <small>Pending</small></div>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('dashboard') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-6">
                    Resources
                    <small>Manage your Employees, Leaves, Attendance and Payroll</small>
                </h2>
            </div>
        </div>
    </div>
    @ability('admin','employees-list')
        <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
            <div class="card">
                <div class="body">
                    <div>
                        <a href="{{ route('employee.index') }}">
                            <i class="material-icons col-pink" style="font-size: 50px;">people</i>
                            <div style="font-size: 20px;text-align: right;margin-top: -50px;">Employees</div>
                            <small style="float: right;" class="col-pink">Employee master record</small><br>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @endability
    @ability('admin','leaves-list')
        <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
            <div class="card">
                <div class="body">
                    <div>
                        <a href="{{ route('leaves.index') }}">
                            <i class="material-icons col-cyan" style="font-size: 50px;">receipt</i>
                            <div style="font-size: 20px;text-align: right;margin-top: -50px;">Leaves</div>
                            <small style="float: right;" class="col-cyan">Employee leave management</small><br>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @endability
    @ability('admin','attendance-view')
        <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
            <div class="card">
                <div class="body">
                    <div>
                        <a href="{{ route('attendanceShifts.index') }}">
                            <i class="material-icons col-light-green" style="font-size: 50px;">list</i>
                            <div style="font-size: 20px;text-align: right;margin-top: -50px;">Attendance</div>
                            <small style="float: right;" class="col-light-green">Employee attendance management</small><br>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @endability
    @ability('admin','caregiver-schedules-view')
        <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
            <div class="card">
                <div class="body">
                    <div>
                        <a href="{{ route('scheduling.caregiver.index') }}">
                            <i class="material-icons col-pink" style="font-size: 50px;">date_range</i>
                            <div style="font-size: 20px;text-align: right;margin-top: -50px;">Caregiver Schedules</div>
                            <small style="float: right;" class="col-pink">Overview of Caregiver Schedules</small><br>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @endability
    @ability('admin','patients-schedules-view')
        <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
            <div class="card">
                <div class="body">
                    <div>
                        <a href="{{ route('scheduling.patient.index') }}">
                            <i class="material-icons col-cyan" style="font-size: 50px;">list</i>
                            <div style="font-size: 20px;text-align: right;margin-top: -50px;">Patient Schedules</div>
                            <small style="float: right;" class="col-cyan">Overview of Patient Schedules</small><br>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @endability
    @ability('admin','employees-list')
        <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
            <div class="card">
                <div class="body">
                    <div>
                        <a href="{{ route('appraisals.index') }}">
                            <i class="material-icons col-light-green" style="font-size: 50px;">important_devices</i>
                            <div style="font-size: 20px;text-align: right;margin-top: -50px;">Appraisals</div>
                            <small style="float: right;" class="col-light-green">Appraisal of Stars & Warnings</small><br>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @endability
    @ability('admin','customer-feedback')
        <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
            <div class="card">
                <div class="body">
                    <div>
                        <a href="{{ route('customerfeedback.index') }}">
                            <i class="material-icons col-pink" style="font-size: 50px;">important_devices</i>
                            <div style="font-size: 20px;text-align: right;margin-top: -50px;">Customer Feedback</div>
                            <small style="float: right;" class="col-pink">Customer Feedback on Staff</small><br>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @endability
</div>
@endsection

@section('page.scripts')
    <!-- Select Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
    <script>

    </script>
@endsection