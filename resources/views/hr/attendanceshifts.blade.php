@extends('layouts.main-layout')

@section('page_title','Daily Attendance - ')

@section('plugin.styles')
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
    <!-- Bootstrap datetimepicker Css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
@endsection

@section('page.styles')
<style>
    .card{
        overflow-x: scroll;
    }
    .dt-buttons{
        position: static !important;
        float: right;
    }
    .paginate_button {
        cursor: pointer;
        margin:2px;
    }
    div.dataTables_wrapper div.dataTables_filter {
        text-align: right !important;
    }
    table.table-bordered tbody tr td {
        text-transform: uppercase; !important;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        font-size: 12px !important;
    }
    table.table-bordered tbody tr td div.col-md-2{
        margin-bottom: 0 !important;
    }
    .label{
        font-size: 12px !important;
        color: #333 !important;
    }
    .radio-buttons div {
        margin-bottom: 0 !important
    }
    .bootstrap-datetimepicker-widget td,.bootstrap-datetimepicker-widget th{
        border:none !important;
    }
    .timepicker-picker a{
        box-shadow: none !important;
    }
    .timepicker-picker tbody{
        max-height: 100px !important;
        max-width: 100px !important;
    }
</style>
<!-- JQuery DataTable Css -->
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection

@section('content')
    @ability('admin','attendance-view')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header clearfix">
                    <a href="{{ route('hr.index') }}" class="pull-left btn btn-info btn-circle clearLocalStore waves-effect waves-circle waves-float">
                        <i class="material-icons">arrow_back</i>
                    </a>
                    <h2 class="col-sm-8">
                        Daily Attendance
                        <small>Daily Attendance of all Employees</small>
                    </h2>
                </div>
                <div class="body">
                    <div class="row clearfix" style="margin-bottom: 0">
                        <form action="{{ route('attendanceShifts.index') }}" method="post">
                            {{ csrf_field() }}
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label" style="margin-bottom: 0">
                                    <label for="date">Date</label>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-7" style="margin-bottom: 0">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control date" value="{{ date_format(new DateTime(old('attendance_date')?old('attendance_date'):$date),'d-m-Y') }}" />
                                            <input type="hidden" name="attendanceDate" value="{{ date_format(new DateTime(old('attendance_date')?old('attendance_date'):$date),'Y-m-d') }}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2" style="margin-bottom: 0">
                                    <button type="submit" class="btn btn-warning">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        @ability('admin','attendance-staff-mark')
                        <li role="presentation" class="active">
                            <a href="#staff_attendance_tab" data-toggle="tab" aria-expanded="false">
                                <i class="material-icons">people</i> Scheduled Staff
                            </a>
                        </li>
                        @endability
                        @ability('admin','attendance-staff-mark')
                        <li role="presentation" class="">
                            <a href="#nonscheduled_attendance_tab" data-toggle="tab" aria-expanded="false">
                                <i class="material-icons">people_outline</i> Non Scheduled Staff
                            </a>
                        </li>
                        @endability
                        @ability('admin','attendance-manager-mark')
                        <li role="presentation" class="">
                            <a href="#manager_attendance_tab" data-toggle="tab" aria-expanded="false">
                                <i class="material-icons">business_center</i> Managers
                            </a>
                        </li>
                        @endability
                        @ability('admin','attendance-training-mark')
                        <li role="presentation" class="">
                            <a href="#training_attendance_tab" data-toggle="tab" aria-expanded="false">
                                <i class="material-icons">airline_seat_legroom_normal</i> Trainees
                            </a>
                        </li>
                        @endability
                    </ul>
                    <div class="tab-content">
                        @ability('admin','attendance-staff-mark')
                        <div role="tabpanel" class="tab-pane fade active in" id="staff_attendance_tab">
                            <div class="row clearfix">
                                <div class="table-responsive" style="width:96%;margin:0 auto">
                                    <div class="col-lg-8 pull-left" style="margin-bottom: 0 !important">
                                        <h4 style="padding-left: 60%;font-weight:normal;color: #3F51B5">Attendance - {{ date_format(new DateTime(old('attendance_date')?old('attendance_date'):$date),'d-m-Y') }}</h4>
                                    </div>
                                    <div class="clearfix"></div><br>
                                    <table class="table table-bordered table-striped table-hover table-condensed stable">
                                        <thead>
                                            <tr>
                                                <th>Employee</th>
                                                <th>
                                                    <b>Caregiver</b>
                                                    <span style="float: right;">Patient</span>
                                                </th>
                                                <th>In-Time</th>
                                                <th>Out-Time</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($scheduledCaregivers as $i => $c)
                                            <tr id="sid_{{ $c->scheduleId }}" @if($c->attStatus){! style = "background-color:lightgreen;" !}@endif>
                                                <td>{{ $c->caregiverFname.' '.$c->caregiverMname.' '.$c->caregiverLname.'-'.$c->caregiverCode }}</td>
                                                <td style="float: right;">{{ $c->patientFname.' '.$c->patientLname }} - {{ $c->serviceName }}</td>
                                                <td>
                                                    <input class="form-control input-sm time" id="att_{{ $c->caregiverId }}_{{ $c->scheduleId }}_in_time" data-cid="{{ $c->caregiverId }}" data-sid="{{ $c->scheduleId }}" name="att_{{ $c->caregiverId }}_{{ $c->scheduleId }}_in_time" @if($c->eIn) value="{{ $c->eIn }}" @else value="{{ $c->sft }}" {!!'style="width:150px;background-color:#5de7c7;"'!!} @endif>
                                                </td>
                                                <td>
                                                    <input class="form-control input-sm time" id="att_{{ $c->caregiverId }}_{{ $c->scheduleId }}_out_time" data-cid="{{ $c->caregiverId }}" data-sid="{{ $c->scheduleId }}" name="att_{{ $c->caregiverId }}_{{ $c->scheduleId }}_out_time" @if($c->eIn)value="{{ $c->eOut }}" @else value="{{ $c->stt }}" {!!'style="width:150px;background-color:#5de7c7;"'!!} @endif @if(!empty($c->eIn) && empty($c->eOut)){!!'style="width:150px;background-color:#f3c8c8;"'!!} @else{!!'style="width:150px;"'!!} @endif>
                                                </td>
                                                <td>
                                                    @ability('admin','attendance-staff-mark')
                                                    <button type="button" class="btn btn-primary schMarkAttendance" data-id="att_{{ $c->caregiverId }}_{{ $c->scheduleId }}" data-cid="{{ $c->caregiverId }}" data-sid="{{ $c->scheduleId }}" @if($c->eIn) data-intime="{{ $c->eIn }}" @else data-intime="{{ $c->sft }}" @endif @if($c->eOut) data-outtime="{{ $c->eOut }}" @else data-outtime="{{ $c->stt }}" @endif name='att_date' value="{{ $date }}">Mark as Present</button>
                                                    @endability
                                                </td>
                                            </tr>
                                            @empty
                                            <tr>
                                                <td colspan="7" class="text-center">No schedule(s) found</td>
                                            </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @endability
                        
                        @ability('admin','attendance-staff-mark')
                        <div role="tabpanel" class="tab-pane fade" id="nonscheduled_attendance_tab">
                            <div class="row clearfix">
                                <form method="post" action="{{ route('attendance.mark-attendance') }}">
                                   {{ csrf_field() }}
                                    <div class="table-responsive" style="width:96%;margin:0 auto">
                                        <div class="col-lg-8 pull-left" style="margin-bottom: 0 !important">
                                            <h4 style="padding-left: 60%;font-weight:normal;color: #3F51B5">Attendance - {{ date_format(new DateTime(old('attendance_date')?old('attendance_date'):$date),'d-m-Y') }}</h4>
                                        </div>
                                        @ability('admin','attendance-staff-mark')
                                        <div class="col-lg-2 pull-right clearfix" style="margin-right: 0%;margin-bottom: 0 !important">
                                            <input type="hidden" name="attendance_date" value="{{ $date }}" />
                                            <button type="submit" class="btn btn-primary">Mark Attendance</button>
                                        </div>
                                        @endability
                                        <div class="clearfix"></div><br>
                                        <table class="table table-bordered table-striped table-hover table-condensed nsCtable">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Employee</th>
                                                    <th>Flags</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse($nonScheduledCaregivers as $j => $c)
                                                    <tr id="cid_{{ $c->caregiverId }}" @if($c->attStatus){! style = "background-color:lightgreen;" !}@endif>
                                                        <td class="text-center">{{ $j + 1 }}</td>
                                                        <td>
                                                            {{ $c->caregiverFname.' '.$c->caregiverMname.' '.$c->caregiverLname.'-'.$c->caregiverCode }}<br>
                                                        </td>
                                                        <td>
                                                        @if($c->leaveType)
                                                            <p>Leave applied- {{ $c->leaveType }}, Status- {{ $c->leaveStatus }}</p>
                                                        @else
                                                            <div class="row clearfix radio-buttons">
                                                                <div class="col-md-2">
                                                                    <input type="radio" id="present_{{ $c->caregiverId }}" name="att_{{ $c->caregiverId }}" class="with-gap radio-col-green" value="P" checked="">
                                                                    <label class="label" for="present_{{ $c->caregiverId }}">Present</label>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <input type="radio" id="absent_{{ $c->caregiverId }}" name="att_{{ $c->caregiverId }}" class="with-gap radio-col-red" value="A" {{ $c->attStatus=='A'?'checked=""':'' }}>
                                                                    <label class="label" for="absent_{{ $c->caregiverId }}">Absent</label>
                                                                </div>
                                                            </div>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                @empty
                                                    <tr>
                                                        <td colspan="4" class="text-center">No schedule(s) found</td>
                                                    </tr>
                                                @endforelse
                                            </tbody>
                                        </table>
                                        <div class="pull-right clearfix" style="margin-right: 5%">
                                            <button type="submit" class="btn btn-primary">Mark Attendance</button>
                                        </div>
                                        <div class="clearfix"></div><br>
                                    </div>
                                </form>
                            </div>
                        </div>
                        @endability

                        @ability('admin','attendance-manager-mark')
                        <div role="tabpanel" class="tab-pane fade" id="manager_attendance_tab">
                            <div class="row clearfix">
                                <form method="post" action="{{ route('attendance.mark-attendance') }}">
                                   {{ csrf_field() }}
                                    <div class="table-responsive" style="width:96%;margin:0 auto">
                                        <div class="col-lg-8 pull-left" style="margin-bottom: 0 !important">
                                            <h4 style="padding-left: 60%;font-weight:normal;color: #3F51B5">Attendance - {{ date_format(new DateTime(old('attendance_date')?old('attendance_date'):$date),'d-m-Y') }}</h4>
                                        </div>
                                        @ability('admin','attendance-manager-mark')                                        
                                        <div class="col-lg-2 pull-right clearfix" style="margin-right: 0%;margin-bottom: 0 !important">
                                            <input type="hidden" name="attendance_date" value="{{ $date }}" />
                                            <button type="submit" class="btn btn-primary">Mark Attendance</button>
                                        </div>
                                        @endability
                                        <div class="clearfix"></div><br>
                                        <table class="table table-bordered table-striped table-hover table-condensed nsMtable">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Employee</th>
                                                    <th>Flags</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse($nonScheduledManagers as $j => $c)
                                                    <tr id="cid_{{ $c->caregiverId }}" @if($c->attStatus){! style = "background-color:lightgreen;" !}@endif>
                                                        <td class="text-center">{{ $j + 1 }}</td>
                                                        <td>
                                                            {{ $c->caregiverFname.' '.$c->caregiverMname.' '.$c->caregiverLname.'-'.$c->caregiverCode }}<br>
                                                        </td>
                                                        <td>
                                                        @if($c->leaveType)
                                                            <p>Leave applied- {{ $c->leaveType }}, Status- {{ $c->leaveStatus }}</p>
                                                        @else
                                                            <div class="row clearfix radio-buttons">
                                                                <div class="col-md-2">
                                                                    <input type="radio" id="present_{{ $c->caregiverId }}" name="att_{{ $c->caregiverId }}" class="with-gap radio-col-green" value="P" checked="">
                                                                    <label class="label" for="present_{{ $c->caregiverId }}">Present</label>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <input type="radio" id="absent_{{ $c->caregiverId }}" name="att_{{ $c->caregiverId }}" class="with-gap radio-col-red" value="A" {{ $c->attStatus=='A'?'checked=""':'' }}>
                                                                    <label class="label" for="absent_{{ $c->caregiverId }}">Absent</label>
                                                                </div>
                                                            </div>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                @empty
                                                    <tr>
                                                        <td colspan="4" class="text-center">No schedule(s) found</td>
                                                    </tr>
                                                @endforelse
                                            </tbody>
                                        </table>
                                        <div class="pull-right clearfix" style="margin-right: 5%">
                                            <button type="submit" class="btn btn-primary">Mark Attendance</button>
                                        </div><div class="clearfix"></div><br>
                                    </div>
                                </form>
                            </div>
                        </div>
                        @endability
        
                        @ability('admin','attendance-training-mark')
                        <div role="tabpanel" class="tab-pane fade" id="training_attendance_tab">
                            <div class="row clearfix">
                                <form method="post" action="{{ route('attendance.mark-attendance') }}">
                                   {{ csrf_field() }}
                                    <div class="table-responsive" style="width:96%;margin:0 auto">
                                        <div class="col-lg-8 pull-left" style="margin-bottom: 0 !important">
                                            <h4 style="padding-left: 60%;font-weight:normal;color: #3F51B5">Attendance - {{ date_format(new DateTime(old('attendance_date')?old('attendance_date'):$date),'d-m-Y') }}</h4>
                                        </div>
                                        <div class="clearfix"></div><br>
                                        <table class="table table-bordered table-striped table-hover table-condensed nsTtable">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Employee</th>
                                                    <th>Flags</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse($nonScheduledTrainees as $j => $c)
                                                    <tr id="cid_{{ $c->caregiverId }}" @if($c->attStatus){! style = "background-color:lightgreen;" !}@endif>
                                                        <td class="text-center">{{ $j + 1 }}</td>
                                                        <td>
                                                            {{ $c->caregiverFname.' '.$c->caregiverMname.' '.$c->caregiverLname.'-'.$c->caregiverCode }}<br>
                                                        </td>
                                                        @if($c->leaveType)                                                            
                                                        <td><p>Leave applied- {{ $c->leaveType }}, Status- {{ $c->leaveStatus }}</p></td>
                                                        <td><button type="button" class="btn btn-primary trainMarkAttendance" disabled>Mark Attendance</button></td>
                                                        @else
                                                        <td>
                                                            <div class="row clearfix radio-buttons">
                                                                <div class="col-md-2">
                                                                    <input type="radio" id="present_{{ $c->caregiverId }}" name="att_{{ $c->caregiverId }}" class="with-gap radio-col-green" value="P" checked="">
                                                                    <label class="label" for="present_{{ $c->caregiverId }}">Present</label>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <input type="radio" id="absent_{{ $c->caregiverId }}" name="att_{{ $c->caregiverId }}" class="with-gap radio-col-red" value="A" {{ $c->attStatus=='A'?'checked=""':'' }}>
                                                                    <label class="label" for="absent_{{ $c->caregiverId }}">Absent</label>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            @ability('admin','attendance-training-mark')
                                                            <button type="button" class="btn btn-primary trainMarkAttendance" data-cid="{{ $c->caregiverId }}" data-status-type="Training" value="{{ $date }}">Mark Attendance</button>
                                                            @endability
                                                        </td>
                                                        @endif
                                                    </tr>
                                                @empty
                                                    <tr>
                                                        <td colspan="4" class="text-center">No schedule(s) found</td>
                                                    </tr>
                                                @endforelse
                                            </tbody>
                                        </table><br>
                                    </div>
                                </form>
                            </div>
                        </div>
                        @endability
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endability
@endsection


@section('page.scripts')
    <script type="text/javascript">
        $(function(){
            $('.content').on('click','.clearLocalStore', function() {
                return localStorage.removeItem('markAttendanceLastTab');
            });

            // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // save the latest tab; use cookies if you like 'em better:
                localStorage.setItem('markAttendanceLastTab', $(this).attr('href'));
            });

            // go to the latest tab, if it exists:
            var lastTab = localStorage.getItem('markAttendanceLastTab');
            if (lastTab) {
                $('[href="' + lastTab + '"]').tab('show');
            }
        });
    </script>
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <!-- Moment Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
    <script>
    $(function(){
        window.history.replaceState({}, document.title, "/hr/dailyattendanceShifts");
        $('.date').bootstrapMaterialDatePicker({
            format: 'DD-MM-YYYY',
            clearButton: true,
            time: false,
            maxDate : new Date(),
        }).on('change', function(e, date){
            $('input[name=attendanceDate]').val(date.format('YYYY-MM-DD'));
        });

        $('.time').datetimepicker({
            format: 'hh:mm A',
            showClose: true,
            showClear: true,
            keepOpen: true,
        }).on('dp.change', function(){
            if((this.id).includes('_in_time'))
                $('*[data-id="'+(this.id).replace('_in_time','')+'"]').attr('data-intime',$('#'+this.id).val());
            if((this.id).includes('_out_time'))
                $('*[data-id="'+(this.id).replace('_out_time','')+'"]').attr('data-outtime',$('#'+this.id).val());

            // $('#present_'+$(this).data('cid')+'_'+$(this).data('sid')).prop('checked',true);
            $('*[data-id="att_'+$(this).data('cid')+'_'+$(this).data('sid')).attr('data-status','P');
        });
    });

    $('.content').on('change','.withSchedules',function(){
        // $('*[data-id="'+(this.name)+'"]').attr('data-status',$(this).val());
        $('#'+(this.name)+'_in_time').val('');
        $('#'+(this.name)+'_out_time').val('');
        $('*[data-id="'+(this.name)+'"]').attr('data-intime','');
        $('*[data-id="'+(this.name)+'"]').attr('data-outtime','');
    });

    $(document).ready(function() {
        if({{ count($scheduledCaregivers) }}){
            var table = $('.stable').DataTable({
                "columnDefs": [
                    { "visible": false, "targets": 0 }
                ],
                "order": [[ 0, 'asc' ]],
                "lengthMenu": [ [25, 50, 100, -1], [25, 50, 100, "All"] ],
                "drawCallback": function ( settings ) {
                    var api = this.api();
                    var rows = api.rows( {page:'current'} ).nodes();
                    var last=null;
         
                    api.column(0, {page:'current'} ).data().each( function ( group, i ) {
                        if ( last !== group ) {
                            $(rows).eq( i ).before(
                                '<tr class="group"><td colspan="7"><b>'+group+'</b></td></tr>'
                            );
         
                            last = group;
                        }
                    } );
                }
            } );   
        }
        if({{ count($nonScheduledCaregivers) }}){
            $('.nsCtable').DataTable({
                "lengthMenu": [ [25, 50, 100, -1], [25, 50, 100, "All"] ],
            });
        }
        if({{ count($nonScheduledManagers) }}){
            $('.nsMtable').DataTable({
                "lengthMenu": [ [25, 50, 100, -1], [25, 50, 100, "All"] ],
            });
        }
        if({{ count($nonScheduledTrainees) }}){
            $('.nsTtable').DataTable({
                "lengthMenu": [ [25, 50, 100, -1], [25, 50, 100, "All"] ],
            });
        }
    } );
    </script>
    <script>
        $('.content').on('click','.schMarkAttendance',function(){
            cid = $(this).attr('data-cid');
            sid = $(this).attr('data-sid');
            intime = $(this).attr('data-intime');
            outtime = $(this).attr('data-outtime');
            // status = $(this).attr('data-status');
            nc = $(this).attr('data-chargeable');
            date = $(this).val();
            $('.schMarkAttendance').attr('disabled', true);
            $(this).html('Processing..');

            $.ajax({
                url: '{{ route('attendance.mark-attendance') }}',
                type: 'POST',
                data: {_token:'{{csrf_token()}}' ,cid:cid, sid:sid, intime:intime, outtime:outtime, date:date, nc:nc, shift:true},
                success: function(data){
                    showNotification('bg-green', 'Attendance Marked Successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');

                    $('#sid_'+sid).attr('style','background-color: lightgreen');
                    $('.schMarkAttendance').attr('disabled', false).html('Mark as Present');
                },
                error: function (err){
                    console.log(err);
                    showNotification('bg-red', 'Something Went Wrong, Please Refresh', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                }
            })
        });

        $('.content').on('click','.trainMarkAttendance',function(){
            cid = $(this).attr('data-cid');
            date = $(this).val();
            status = $('input[name=att_'+cid+']:checked').val();
            $('.trainMarkAttendance').attr('disabled', true);
            $(this).html('Processing..');

            $.ajax({
                url: '{{ route('attendance.mark-attendance') }}',
                type: 'POST',
                data: {_token:'{{csrf_token()}}', cid:cid, date:date, status:status, training:true},
                success: function(data){
                    showNotification('bg-green', 'Attendance Marked Successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');

                    $('#cid_'+cid).attr('style','background-color: lightgreen');
                    $('.trainMarkAttendance').attr('disabled', false).html('Mark as Present');
                },
                error: function (err){
                    console.log(err);
                    showNotification('bg-red', 'Something Went Wrong, Please Refresh', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                }
            })
        });
    </script>
@endsection
