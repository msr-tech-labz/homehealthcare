<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Forgot Password | Smart Health Connect</title>
    <!-- Favicon-->
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{ ViewHelper::ThemePlugin('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ ViewHelper::ThemePlugin('node-waves/waves.css') }}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ ViewHelper::ThemePlugin('animate-css/animate.css') }}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ ViewHelper::ThemeCss('style.css') }}" rel="stylesheet">
</head>

<body class="fp-page" style="background-color: #e9e9e9 !important;">
    <div class="fp-box">
        <div class="logo">
            <a href="javascript:void(0);" style="color:#1f91f3 !important">Smart Health Connect</a>
            <small style="color:#1f91f3">Home Healthcare Management System</small>
        </div>
        <div class="card">
            <div class="header">
                <h2 class="card-title-inside">Forgot Password</h2>
            </div>
            <div class="body">
                @if (session('status'))
                    <div class="alert alert-warning">
                        {{ session('status') }}
                    </div>
                @endif
                <form id="forgot_password" class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                    {{ csrf_field() }}
                    <div class="msg">
                        Enter your email address that you used to register. We'll send you an email with your username and a
                        link to reset your password.
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">domain</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="org_code" placeholder="Organization Code" value="{{ old('org_code') }}" required autofocus>
                            @if ($errors->has('org_code'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('org_code') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            <input type="email" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <button class="btn btn-block btn-primary btn-lg waves-effect" type="submit">RESET MY PASSWORD</button>

                    <div class="row m-t-20 m-b--5 align-center">
                        <a href="/login">Sign In!</a>
                    </div><br>
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="{{ ViewHelper::ThemePlugin('jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ ViewHelper::ThemePlugin('bootstrap/js/bootstrap.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('node-waves/waves.js') }}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('jquery-validation/jquery.validate.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ ViewHelper::ThemeJs('admin.js') }}"></script>
    <script src="{{ ViewHelper::ThemeJs('pages/examples/forgot-password.js') }}"></script>
</body>

</html>
