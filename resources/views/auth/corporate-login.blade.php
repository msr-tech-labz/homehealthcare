﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign In | Corporate Office Management System</title>
    <meta name="csrf_token" content="{{ csrf_token() }}">
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{ ViewHelper::ThemePlugin('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ ViewHelper::ThemePlugin('node-waves/waves.css') }}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ ViewHelper::ThemePlugin('animate-css/animate.css') }}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ ViewHelper::ThemeCss('style.css') }}" rel="stylesheet">
</head>
<style>
    html{height: -webkit-fill-available;}
    .login-page {
        max-width:600px !important;
        margin-top:8%;
        background-image:url('../img/corporate-wallpaper.jpg')!important;
        background-repeat:no-repeat!important;
        background-size:cover!important;
    }
    .logo {
        border-top-left-radius:25px;
        box-shadow:5px 10px 30px rgba(0,0,0,0.7);
        margin-bottom:0!important;
        padding:10px;
        background:rgba(38,46,59,0.9);
    }
    .card{
        border-bottom-right-radius:25px;
        box-shadow:5px 10px 30px rgba(0,0,0,0.9);
    }
</style>
<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);" style="color: #f6f6f6 !important">Smart Health Connect</a>
            <small style="font-size: 18px;color: #f6f6f6 !important">Corporate Office Management System</small>
        </div>
        <div class="card">
            <div class="body">
                @if(session('alert_error'))
                <div class="row clearfix">
                    <div class="alert alert-danger">
                        <strong>{{ session('alert_error') }}</strong>
                    </div>
                </div>
                @endif
                @if(session('alert_success'))
                <div class="row clearfix">
                    <div class="alert alert-success">
                        <strong>{{ session('alert_success') }}</strong>
                    </div>
                </div>
                @endif
                <div class="col-sm-5" style="position:relative;margin: 0;height: 100%;border-right: 1px solid #ccc;height: 250px;text-align: center;">
                    <span data-tilt></span>
                        <img src="{{ !empty(Cookie::get('corporateCookieLogo'))?asset('../uploads/corporate/'.Cookie::get('corporateCookieLogo')):asset('img/smarthealthglobal.png') }}" style="width: 170px; position:absolute;top:0;bottom:0;margin:auto;" class="img-responsive"><br>
                </div>
                <div class="col-sm-7" style="margin: 0">
                    <form id="sign_in" method="POST" action="{{ route('corporate.authenticate') }}">
                        {{-- {{ csrf_field() }} --}}
                        <div class="msg">Sign in to start your session</div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">touch_app</i>
                            </span>
                            <div class="form-line{{ $errors->has('corporate.orgcode') ? ' focused error' : '' }}">
                                <input type="text" class="form-control" name="corporate_id" placeholder="Corporate Code" value="{{ !is_null(Cookie::get('corporateCookieCorporateId'))?\Helper::encryptor('decrypt',Cookie::get('corporateCookieCorporateId')):'' }}" required autocomplete="new-password">
                                @if ($errors->has('corporate_id'))
                                    <label class="form-label">{{ $errors->first('corporate_id') }}</label>
                                @endif
                            </div>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">email</i>
                            </span>
                            <div class="form-line{{ $errors->has('email') ? ' focused error' : '' }}">
                                <input type="text" class="form-control" name="email" placeholder="Email" required autofocus>
                                @if ($errors->has('email'))
                                    <label class="form-label">{{ $errors->first('email') }}</label>
                                @endif
                            </div>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">lock</i>
                            </span>
                            <div class="form-line{{ $errors->has('passwordemail') ? ' focused error' : '' }}">
                                <input type="password" autocomplete="new-password" class="form-control" name="password" placeholder="Password" required>
                                @if ($errors->has('password'))
                                    <label class="form-label">{{ $errors->first('password') }}</label>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-8 p-t-5">
                                <input type="checkbox" name="remember" id="remember" value="true" class="filled-in chk-col-blue">
                                <label for="remember">Remember Me</label>
                            </div>
                            <div class="col-xs-4">
                                <button class="btn btn-block bg-teal btn-lg waves-effect signin" type="submit">Sign In</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="{{ ViewHelper::ThemePlugin('jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ ViewHelper::ThemePlugin('bootstrap/js/bootstrap.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('node-waves/waves.js') }}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('jquery-validation/jquery.validate.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ ViewHelper::ThemeJs('admin.js') }}"></script>
    <script src="{{ ViewHelper::ThemeJs('pages/examples/sign-in.js') }}"></script>
    <script>
        $(document).on('submit', 'form', function(e){
            $(this).append('<input type="hidden" name="_token" value="'+$('[name="csrf_token"]').attr('content')+'">');
        });

        var csrfToken = $('[name="csrf_token"]').attr('content');

        function refreshToken(){
            $.get('refresh-csrf').done(function(data){
                csrfToken = data; // the new token
            });
        }
        $('.signin').click(function(){
            $('.signin').html('Signing In..');
            $('.signin').css("width","auto");
            setTimeout(function(){
                        $('.signin').html('Sign In');
                        $('.signin').css("width","auto"); 
            }, 60000);
        });

        setInterval(refreshToken, 3600000); // 1 hour
    </script>
</body>
</html>
