﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Central Console Login | Smart Health Connect</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{ ViewHelper::ThemePlugin('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ ViewHelper::ThemePlugin('node-waves/waves.css') }}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ ViewHelper::ThemePlugin('animate-css/animate.css') }}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ ViewHelper::ThemeCss('style.css') }}" rel="stylesheet">
</head>
<style>
    html{
        height: -webkit-fill-available;
    }
    .login-page{
        background-image: url('../img/console-datacenter.jpg') !important;
        background-size: cover !important;
        max-width: 400px;
    }
    .card{
        border-bottom-right-radius: 25px;
        box-shadow: 5px 10px 30px black;
    }
    .logo{
        border-top-left-radius: 25px;
        box-shadow: 5px 10px 30px black;
        font-family: fantasy;
        margin-bottom: 0px !important;
        padding: 10px;
        background: #87aab0;
    }
</style>
<body class="login-page bg-teal">
    <div class="login-box" style="margin-top:150px">
        <div class="logo">
            <a href="javascript:void(0);" style="color: #000 !important">Smart Health Connect</a>
            <small style="font-size: 18px;color: #000 !important">Central Console</small>
        </div>
        <div class="card">
            <div class="body">
                @if(session('alert_error'))
                <div class="row clearfix">
                    <div class="alert alert-danger">
                        <strong>{{ session('alert_error') }}</strong>
                    </div>
                </div>
                @endif
                <form id="sign_in" method="POST" action="{{ route('console.authenticate') }}">
                    {{ csrf_field() }}
                    <div class="msg">Sign in to start your session</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line{{ $errors->has('email') ? ' focused error' : '' }}">
                            <input type="text" class="form-control" name="email" placeholder="Email" required autofocus>
                            @if ($errors->has('email'))
                                <label class="form-label">{{ $errors->first('email') }}</label>
                            @endif
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line{{ $errors->has('passwordemail') ? ' focused error' : '' }}">
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                            @if ($errors->has('password'))
                                <label class="form-label">{{ $errors->first('password') }}</label>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <button class="btn btn-block bg-pink waves-effect" type="submit">SIGN IN</button>
                        </div>
                    </div>                    
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="{{ ViewHelper::ThemePlugin('jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ ViewHelper::ThemePlugin('bootstrap/js/bootstrap.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('node-waves/waves.js') }}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('jquery-validation/jquery.validate.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ ViewHelper::ThemeJs('admin.js') }}"></script>
    <script src="{{ ViewHelper::ThemeJs('pages/examples/sign-in.js') }}"></script>
</body>

</html>