@extends('layouts.main-layout')

@section('page_title','My Profile - ')

@section('active_administration','active')

@section('plugin.styles')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
@endsection

@section('content')

@if (count($errors) > 0)
<div class="row clearfix">
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('dashboard') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-10">
                    Profile
                    <small>Your profile details</small>
                </h2>
            </div>
            <div class="body">
                <form class="form-horizontal userForm" method="POST" action="{{ route('profile.save') }}">
                    {{ csrf_field() }}
                    <div class="row clearfix">
                        <div class="col-sm-12 form-horizontal">
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="notes">Full Name</label>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="full_name" class="form-control" placeholder="Full Name" value="{{ $p->full_name }}">
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="notes">Email</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="email" name="email" class="form-control" placeholder="Email" value="{{ $p->email }}">
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="notes">Mobile Number</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="mobile" class="form-control" placeholder="Mobile Number" value="{{ $p->mobile }}">
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="password">Password</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="password" class="form-control" placeholder="Password">
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="{{ Helper::encryptor('encrypt',$p->id) }}">
                    <button type="submit" class="btn btn-success btn-lg waves-effect pull-right">Save</button>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('plugin.scripts')
<!-- Select Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<!-- Input Mask Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>
<!-- Moment Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<!-- Bootstrap Notify Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-notify/bootstrap-notify.js') }}"></script>
<script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
@endsection

@section('page.scripts')
<script>
    $(function(){
        $('.date').inputmask('dd-mm-yyyy', { placeholder: '__-__-____' });

        $('.content').on('change','input[name=status]', function(){
            var id = $(this).data('id');
            var status = $(this).is(':checked')?1:0;

            $.ajax({
                url: '{{ route('user.update-status')}}',
                type: 'POST',
                data: {id: id, status: status, _token: '{{ csrf_token() }}'},
                success: function (data){
                    console.log(data);
                    showNotification('bg-green', 'Status changed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                },
                error: function (error){
                    console.log(error);
                }
            });
        });

        $('.content').on('change','input[name=status]', function(){
            if($(this).is(':checked')){
                $(this).val('1');
                $(this).attr('checked',true);
            }else{
                $(this).val('0');
                $(this).removeAttr('checked');
            }
        });

        $('.btnEdit').on('click', function(){
            id = $(this).data('id');
            fname = $(this).data('first-name');
            lname = $(this).data('last-name');
            email = $(this).data('email');
            mobile = $(this).data('mobile');
            role = $(this).data('role');
			branch = $(this).data('branch');

            $('.action_lbl').html('Edit User - '+fname);
            $('.userForm').attr('action',$(this).data('url'));
            $('.userForm').attr('method','POST');
            $('.userForm input[name=_method]').val('PUT');
            $('.userForm input[name=_token]').val('{{ csrf_token() }}');

            $('.userForm #id').val(id);
            $('.userForm input[name=first_name]').val(fname);
            $('.userForm input[name=last_name]').val(lname);
            $('.userForm input[name=email]').val(email);
            $('.userForm input[name=mobile]').val(mobile);
            $('.userForm select[name=role_id]').selectpicker('val',role);
			$('.userForm select[name=branch_id]').selectpicker('val',branch);

            $('#userModal').modal();
        });
    });
</script>
@endsection
