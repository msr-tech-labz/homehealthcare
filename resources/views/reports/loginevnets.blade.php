
@extends('layouts.report-layout')

@section('page_title','Login Events - Management | Reports')

@section('report-title','Login Events Report')

@section('report-summary','Summary of login events')

@section('report-filter')
    <style>
        table.table-bordered thead tr th, table.table-bordered tbody tr td {
            text-transform: capitalize; !important;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            font-size: 12px !important;
        }
    </style>
    <div class="report-filter">
        <form action="{{ route('report.loginevent-report') }}" class="form-horizontal" method="post">
            @csrf
            <div class="col-sm-2">
                <label class="label-control">User</label>
                <select class="form-control input-sm show-tick" name="filter_user" data-live-search="true">
                    <option value="">-- Select --</option>
                    @if(isset($users) && count($users))
                        @foreach ($users as $user)
                            <option value="{{ $user->id }}">{{ $user->full_name }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="col-sm-2">
                <label class="label-control">Period</label>
                <input type="text" id="period" class="form-control input-sm daterange" placeholder="Select Period"/>
                <input type="hidden" name="filter_period" />
            </div>
            <div class="col-sm-2 pull-right" style="margin-top: 1%">
                <input type="submit" class="btn btn-block btn-primary btnFilter" value="Filter">
            </div>
            <div class="clearfix"></div>
        </form>
    </div>
@endsection

@section('report-content')
<div class="body" style="min-height:200px">
    <div class="table-responsive" style="overflow-x: scroll;">
        <table class="table table-bordered table-hover table-condensed reportTable" id="reportTable" style="width:100%">
            <thead>
                <tr>
                    <th>#<</th>
                    <th>Employee Name</th>
                    <th>Ip Address</th>
                    <th>Login Date</th>
                    <th>Login Time</th>
                    <th>Logout Time</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($events as $i => $event)
                <tr>
                    <td>{{ $i + 1 }}</td>
                    <td>{{ $event->users->full_name }}</td>
                    <td>{{ $event->ip_address }}</td>
                    <td>{{ $event->created_at->format('d-m-Y') }}</td>
                    <td>{{ \Carbon\Carbon::parse($event->created_at)->format('h:i A') }}</td>
                    <td>{{ !empty($event->logout_time)?$event->updated_at->format('h:i A'):'-' }}</td>
                    <td>{{ $event->login_status }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@push('report.scripts')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
    <script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
    <script type="text/javascript" src="{{ ViewHelper::ThemeJs('daterangepicker.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<script>
    var rTable;
    $(document).ready(function() {
        $('.daterange').daterangepicker({
            autoApply: false,
            autoUpdateInput: false,
            alwaysShowCalendars: true,
            locale: {
              format: 'DD-MM-YYYY',
              applyLabel: 'Apply',
              cancelLabel: 'Clear'
            },
            ranges: {
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'Last 30 Days': [moment().subtract(29, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
           }
        },
            function(start, end, label) {
                $('input[name=filter_period]').val(start.format('YYYY-MM-DD')+'_'+end.format('YYYY-MM-DD'));
                $('#period').val(start.format('DD-MM-YYYY')+' - '+end.format('DD-MM-YYYY'))
            }
        );

        $(".cancelBtn").click(function() {
            $('input[name=filter_period]').val('');
            $('#period').val('');
        });
        @if(count($events))
            $('.reportTable').DataTable({
                scrollY: 260,
                scrollX: true,
                scrollCollapse: true,
                lengthMenu: [[50,100,200,-1],[50,100,200,"All"]],
                language: {
                    processing: "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate' style='color:#ff0000'> Loading....</span>"
                },
                order: [[0,'asc']],
                // displayLength: -1,
                dom: 'Bflrpt',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'Login-Events-Data_{{ date("d-m-Y_H:i:s") }}',
                        exportOptions: {
                            format: {
                                body: function ( data, row, column ) {
                                            // Strip $ from salary column to make it numeric
                                            return (column === 40)?'"'+data.replace( /<br\s*\/?>/ig, '\r\n' )+'"':data;
                                        }
                                    }
                                }
                    },
                    {
                        extend: 'print',
                        title: '<h3 style="text-align:center;">Login Events Data</h3>',
                        customize: function ( win ) {
                            $(win.document.body).css( 'font-size', '7pt' );

                            $(win.document.body).find( 'table' ).addClass( 'compact' ).css( 'font-size', 'inherit' );
                        }
                    }
                ],
            });
        @endif    
    });  
</script>
@endpush
