@extends('layouts.report-layout')

@section('page_title','Schedules Data - Financial | Reports')

@section('report-title','Schedules Data')

@section('report-summary','Summary of schedules')

@section('report-filter')
    <style>
        table.table-bordered thead tr th, table.table-bordered tbody tr td {
            text-transform: capitalize; !important;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            font-size: 12px !important;
        }
    </style>
    <div class="report-filter">
        <form id="filter-form" class="form-horizontal" method="post">
            <div class="col-sm-4">
                <label class="label-control">Patient</label>
                <select class="form-control input-sm show-tick" name="filter_patient" data-live-search="true">
                    <option value="0">-- Select --</option>
                @if(isset($patients) && count($patients))
                    @foreach ($patients as $patient)
                        <option value="{{ $patient->id }}">{{ $patient->full_name.' - '.$patient->patient_id }}</option>
                    @endforeach
                @endif
                </select>
            </div>
            <div class="col-sm-2">
                <label class="label-control">Period</label>
                <input type="text" id="period" class="form-control input-sm daterange" placeholder="Select Period"/>
                <input type="hidden" name="filter_period" />
            </div>
            <div class="col-sm-2 pull-right" style="margin-top: 1%">
                <input type="submit" class="btn btn-block btn-primary" value="Filter">
            </div>
            <div class="clearfix"></div>
        </form>
    </div>
@endsection

@section('report-content')
<div class="body table-responsive" style="min-height:200px">
    <table class="table table-bordered table-hover table-condensed reportTable" style="width:100%">
        <thead>
            <tr>
                <th>Date</th>
                <th>Episode ID</th>
                <th>Patient</th>
                <th>Service Offered</th>
                <th>Employee ID</th>
                <th>Employee Name</th>
                <th>Number</th>
                <th>Designation</th>
                <th>Chargeable</th>
                <th>Gross Rate</th>
                <th>Net Rate</th>
                <th>Discount</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <tr><td colspan="13" class="text-center">Select Employee and Period to filter</td></tr>
        </tbody>
    </table>
</div>
@endsection

@push('report.scripts')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
    <script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
    <script type="text/javascript" src="{{ ViewHelper::ThemeJs('daterangepicker.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<script>
    var rTable;
    $(document).ready(function() {
        $('.daterange').daterangepicker({
            autoApply: true,
            autoUpdateInput: false,
            alwaysShowCalendars: true,
            locale: {
              format: 'DD-MM-YYYY',
              applyLabel: 'Apply',
              cancelLabel: 'Clear'
            },
            ranges: {
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'Last 30 Days': [moment().subtract(29, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
           }
         },
         function(start, end, label) {
             $('input[name=filter_period]').val(start.format('YYYY-MM-DD')+'_'+end.format('YYYY-MM-DD'));
             $('#period').val(start.format('DD-MM-YYYY')+' - '+end.format('DD-MM-YYYY'))
         }
        );

        $('#filter-form').on('submit', function(e) {
            if($('input[name=filter_period]').val() != ''){
                if ( ! $.fn.DataTable.isDataTable('.reportTable') ) {
                    getData();
                }else{
                    rTable.draw();
                }
                e.preventDefault();                
            }else{
                alert("Please select a period filter!");
                return false;
            }
        });
    });

    function getData(){
        rTable = $('.reportTable').DataTable({
            processing: true,
            serverSide: true,
            lengthMenu: [[50,100,200,-1],[50,100,200,"All"]],
            language: {
                processing: "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate' style='color:#ff0000'> Loading....</span>"
            },
            order: [[0,'asc']],
            ajax: {
                url: '{{ route('clinical.schedule-data','ajax=true') }}',
                type: 'POST',
                data: function (d) {
                    d._token = '{{ csrf_token() }}';
                    d.patient = $('select[name=filter_patient] option:selected').val();
                    d.period = $('input[name=filter_period]').val();
                }
            },
            // displayLength: -1,
            dom: 'Bflrpti',
            columns: [
                {data : 'date', name : 'date', searchable: true},
                {data : 'episode_id', name : 'episode_id', searchable: true},
                {data : 'patient_name', name : 'patient_name', searchable: true},
                {data : 'service_requested', name : 'service_requested', searchable: true},
                {data : 'employee_id', name : 'employee_id', searchable: true},
                {data : 'employee_name', name : 'employee_name', searchable: true},
                {data : 'number', name : 'number', searchable: true},
                {data : 'designation', name : 'designation', searchable: true},
                {data : 'chargeable', name : 'chargeable'},
                {data : 'gross_rate', name : 'gross_rate'},
                {data : 'net_rate', name : 'net_rate'},
                {data : 'discount', name : 'discount'},
                {data : 'status', name : 'status', searchable: true},                
            ],
            drawCallback: function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last = null;

                api.column(0, {page:'current'} ).data().each( function ( group, i ) {
                    if(group != null){
                        if ( last !== group ) {
                            $(rows).eq( i ).before(
								'<tr class="group"><td colspan="13"><b>'+group.toUpperCase()+'</b></td></tr>'
                            );
                            last = group;
                        }
                    }
                });
            },
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: 'Revenue-with-Schedules_{{ date("d-m-Y_H:i:s") }}'
                },
                {
                    extend: 'pdfHtml5',
                    pageSize: 'A3'
                },
                {
                    extend: 'print',
                    title: '<h3 style="text-align:center;">Schedules Data Report</h3>',
                    customize: function ( win ) {
                        $(win.document.body).css( 'font-size', '7pt' );

                        $(win.document.body).find( 'table' ).addClass( 'compact' ).css( 'font-size', 'inherit' );
                    }
                }
            ],
        });
    }
</script>
@endpush
