@extends('layouts.report-layout')

@section('page_title','Unconfirmed Schedules - Clinical | Reports')

@section('report-title','Unconfirmed Schedules Data')

@section('report-summary','Summary of unconfirmed schedules')

@section('report-filter')
    <style>
        table.table-bordered thead tr th, table.table-bordered tbody tr td {
            text-transform: capitalize; !important;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            font-size: 12px !important;
        }
    </style>
    <div class="report-filter">
        <form id="filter-form" class="form-horizontal" method="post">
            <div class="col-sm-2">
                <label class="label-control">Period</label>
                <input type="text" id="period" class="form-control input-sm daterange" placeholder="Select Period"/>
                <input type="hidden" name="filter_period" />
            </div>
            <div class="col-sm-2 pull-right" style="margin-top: 1%">
                <input type="submit" class="btn btn-block btn-primary" value="Filter">
            </div>
            <div class="clearfix"></div>
        </form>
    </div>
@endsection

@section('report-content')
<div class="body table-responsive" style="min-height:200px">
    <table class="table table-bordered table-hover table-condensed reportTable" style="width:100%">
        <thead>
            <tr>
                <th>Date</th>
                <th>Episode ID</th>
                <th>Customer</th>
                <th>Phone Number</th>
                <th>Service Requested</th>
                <th>Caregiver</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <tr><td colspan="12" class="text-center">Select Period to filter</td></tr>
        </tbody>
    </table>
</div>
@endsection

@push('report.scripts')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
    <script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
    <script type="text/javascript" src="{{ ViewHelper::ThemeJs('daterangepicker.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<script>
    var rTable;
    $(document).ready(function() {
        // $.ajaxSetup({
		//     headers: {
		//         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		//     }
		// });

        $('.daterange').daterangepicker({
            autoApply: true,
            autoUpdateInput: false,
            alwaysShowCalendars: true,
            locale: {
              format: 'DD-MM-YYYY',
              applyLabel: 'Apply',
              cancelLabel: 'Clear'
            },
            ranges: {
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'Last 30 Days': [moment().subtract(29, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
           }
         },
         function(start, end, label) {
             $('input[name=filter_period]').val(start.format('YYYY-MM-DD')+'_'+end.format('YYYY-MM-DD'));
             $('#period').val(start.format('DD-MM-YYYY')+' - '+end.format('DD-MM-YYYY'))
         }
        );

        $('#filter-form').on('submit', function(e) {
            if($('select[name=filter_patient] option:selected').val() > 0 || $('input[name=filter_period]').val() != ''){
                if ( ! $.fn.DataTable.isDataTable('.reportTable') ) {
                    getData();
                }else{
                    rTable.draw();
                }
                e.preventDefault();                
            }else{
                alert("Please select period filter!");
                return false;
            }
        });
    });

    function getData(){
        rTable = $('.reportTable').DataTable({
            processing: true,
            serverSide: true,
            lengthMenu: [[50,100,200,-1],[50,100,200,"All"]],
            language: {
                processing: "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate' style='color:#ff0000'> Loading....</span>"
            },
            order: [[2,'asc']],
            ajax: {
                url: '{{ route('clinical.unconfirmed-schedules','ajax=true') }}',
                type: 'POST',
                data: function (d) {
                    d._token = '{{ csrf_token() }}';
                    d.patient = $('select[name=filter_patient] option:selected').val();
                    d.period = $('input[name=filter_period]').val();
                }
            },
            // displayLength: -1,
            dom: 'Bflrpt',
            columns: [
                {data : 'date', name : 'date', searchable: true},
                {data : 'episode_id', name : 'episode_id', searchable: true},
                {data : 'patient_name', name : 'patient_name', searchable: true},
                {data : 'patient_phone', name : 'patient_phone', searchable: true},
                {data : 'service_requested', name : 'service_requested', searchable: true},
                {data : 'caregiver', name : 'caregiver', searchable: true},
                {data : 'status', name : 'status'}                
            ],
            drawCallback: function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last = null;

                api.column(2, {page:'current'} ).data().each( function ( group, i ) {
                    if(group != null){
                        if ( last !== group ) {
                            $(rows).eq( i ).before(
								'<tr class="group"><td colspan="12"><b>'+group.toUpperCase()+'</b></td></tr>'
                            );
                            last = group;
                        }
                    }
                });
            },
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: 'Unconfirmed-Schedules_{{ date("d-m-Y_H:i:s") }}'
                },
                {
                    extend: 'pdfHtml5',
                    pageSize: 'A3'
                },
                {
                    extend: 'print',
                    title: '<h3 style="text-align:center;">Unconfirmed Schedules Report</h3>',
                    customize: function ( win ) {
                        $(win.document.body).css( 'font-size', '7pt' );

                        $(win.document.body).find( 'table' ).addClass( 'compact' ).css( 'font-size', 'inherit' );
                    }
                }
            ],
        });
    }
</script>
@endpush
