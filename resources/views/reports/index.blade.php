@extends('layouts.main-layout')

@section('page_title','Reports - ')

@section('active_reports','active')

@section('page.styles')
<style>
     .report-menu-item{
         line-height: 1.8;
     }
     .report-menu-item a.green{
         color: #ff5c00;
     }
     .card{
        height: 250px !important;
     }
</style>
@endsection

@section('content')
<div class="row clearfix">
    @if(\Helper::encryptor('decrypt',session('organisationType')) == 'Provider')
    @ability('admin','report-hr')
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="card">
            <div class="header bg-deep-purple">
                <h2>
                    HR Report I
                </h2>
            </div>
            <div class="body">
                <ul>
                    <li class="report-menu-item"><a class="green" href="{{ route('hr.master-data') }}">HR Report - Master</a></li>
                    <li class="report-menu-item"><a class="green" href="{{ route('hr.attendance-report') }}">Daily Attendance Report</a></li>
                    <li class="report-menu-item"><a class="green" href="{{ route('hr.leaves-stats-report') }}">Leaves Stats Report</a></li>
                    <li class="report-menu-item"><a class="green" href="{{ route('hr.monthly-attendance-report') }}">Monthly Attendance Report</a></li>
                    <li class="report-menu-item"><a class="green" href="{{ route('hr.leaves.reports') }}">Leaves Report</a></li>
                    <li class="report-menu-item"><a class="green" href="{{ route('hr.old-leaves.reports') }}">Old Leaves Report</a></li>
                    @if(\Helper::encryptor('decrypt',session('organisationType')) == 'Aggregator')
                    <li class="report-menu-item"><a class="green" href="{{ route('hr.resource-stats') }}">Resource Stats</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
    @endability
    @endif

    @ability('admin','report-sales')
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="card">
            <div class="header bg-deep-purple">
                <h2>
                    Sales Report
                </h2>
            </div>
            <div class="body">
                <ul>
                    <li class="report-menu-item"><a class="green" href="{{ route('sales.master-data') }}">Lead Report</a></li>
                    <li class="report-menu-item"><a class="green" href="{{ route('sales.leads-converted') }}">Lead Status Report</a></li>
                    @if(\Helper::encryptor('decrypt',session('organisationType')) == 'Aggregator')
                    <li class="report-menu-item"><a class="green" href="{{ route('sales.daily-stats') }}">Daily Lead Stats</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
    @endability

    @if(\Helper::encryptor('decrypt',session('organisationType')) == 'Provider')
    @ability('admin','report-financial')
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="card">
            <div class="header bg-deep-purple">
                <h2>
                    Financial Report
                </h2>
            </div>
            <div class="body">
                <ul>
                    <li class="report-menu-item"><a class="green" href="{{ route('financial.outstanding') }}">Outstanding Report</a></li>
                    <li class="report-menu-item"><a class="green" href="{{ route('financial.revenue-schedules') }}">Schedule-wise Revenue Report</a></li>
                    <li class="report-menu-item"><a class="green" href="{{ route('financial.manual-creditmemo') }}">Manual Credits Report</a></li>
                    <li class="report-menu-item"><a class="green" href="{{ route('financial.statement-of-account') }}">Statement of Account - Email</a></li>
                    <li class="report-menu-item"><a class="green" href="{{ route('financial.invoice-report') }}">Invoice Report</a></li>
                    <li class="report-menu-item"><a class="green" href="{{ route('financial.invoice-report-with-service') }}">Invoice Report with Service</a></li>
                    <li class="report-menu-item"><a class="green" href="{{ route('financial.receipt-report') }}">Receipts Report</a></li>
                </ul>
            </div>
        </div>
    </div>
    @endability

    @if(\Helper::encryptor('decrypt',session('organisationType')) == 'Provider')
    @ability('admin','report-hr')
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="card">
            <div class="header bg-deep-purple">
                <h2>
                    HR Report II
                </h2>
            </div>
            <div class="body">
                <ul>
                    <li class="report-menu-item"><a class="green" href="{{ route('hr.employee-worktime1') }}">Employee WorkTime Report I</a></li>
                    <li class="report-menu-item"><a class="green" href="{{ route('hr.employee-worktime2') }}">Employee WorkTime Report II</a></li>
                    {{--<li class="report-menu-item"><a class="green" href="{{ route('hr.employee-worktime-old') }}">Employee WorkTime Report Old</a></li>--}}
                    <li class="report-menu-item"><a class="green" href="{{ route('sales.referral-commission') }}">Referral Commission Report</a></li>
                    <li class="report-menu-item"><a class="green" href="{{ route('sales.conveyance') }}">Conveyance Report</a></li>
                    <li class="report-menu-item"><a class="green" href="{{ route('hr.warning-star-uniform-document') }}">Warning, Star, Uniform, Document Report</a></li>
                    <li class="report-menu-item"><a class="green" href="{{ route('hr.customer-feedback') }}">Feedback Report</a></li>
                </ul>
            </div>
        </div>
    </div>
    @endability
    @endif

    @ability('admin','report-management')
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="card">
            <div class="header bg-deep-purple">
                <h2>
                    Management Report
                </h2>
            </div>
            <div class="body">
                <ul>
                    <li class="report-menu-item"><a class="green" href="{{ route('report.mis-report') }}">MIS Report</a></li>
                    <li class="report-menu-item"><a class="green" href="{{ route('report.loginevent-report') }}">Login Events Report</a></li>
                </ul>
            </div>
        </div>
    </div>
    @endability

    @ability('admin','report-clinical')
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="card">
            <div class="header bg-deep-purple">
                <h2>
                    Clinical Report
                </h2>
            </div>
            <div class="body">
                <ul>
                    <li class="report-menu-item"><a class="green" href="{{ route('clinical.service-summary') }}">Service Summary</a></li>
                    <li class="report-menu-item"><a class="green" href="{{ route('clinical.clientnoservicedays') }}">Client No Service Days</a></li>
                    <li class="report-menu-item"><a class="green" href="{{ route('clinical.schedule-data') }}">Schedule Report</a></li>
                    <li class="report-menu-item"><a class="green" href="{{ route('clinical.renewable-service-orders') }}">Renewable Service Orders Report</a></li>
                    <li class="report-menu-item"><a class="green" href="{{ route('clinical.unconfirmed-schedules') }}">Unconfirmed Schedules Report</a></li>
                    <li class="report-menu-item"><a class="green" href="{{ route('clinical.billables') }}">Billables Report</a></li>
                    <li class="report-menu-item"><a class="green" href="{{ route('clinical.bench-forecast-report') }}">Bench Forecast Report</a></li>
                </ul>
            </div>
        </div>
    </div>
    @endability
    @endif
</div>
@endsection
