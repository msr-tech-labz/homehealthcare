@extends('layouts.report-layout')

@section('page_title','Statement of Account - Financial | Reports')

@section('report-title','Statement of Account - Email')

@section('report-summary','Email notifications to patient')

@section('report-content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
<style>
    .bg-red{
        background-color: rgba(244, 67, 54, 0.26) !important;
    }
    .bg-green{
        background-color: rgba(76, 175, 80, 0.35) !important;
    }
</style>
<div class="body">
    <div class="row clearfix">
        <form id="filter-form" class="form-horizontal" action="{{ route('financial.statement-of-account') }}" method="post">
            {{ csrf_field() }}
            <div class="col-sm-4 hide">
                <label class="label-control">Customer</label>
                <select class="form-control input-sm" name="filter_patient" data-live-search="true" required>
                    <option value="0">-- Select --</option>
                @if(isset($leads) && count($leads))
                    @foreach ($leads as $lead)
                        <option value="{{ $lead->patient_id }}">{{ $lead->patient->full_name.' - '.$lead->episode_id }}</option>
                    @endforeach
                @endif
                </select>
            </div>
            <div class="col-sm-2 col-md-offset-2">
                <label class="label-control">Select Period</label>
                <input type="text" id="period" class="form-control input-sm daterange" placeholder="Select Period" value="{{ isset($period) && !empty($period)?date_format(new DateTime(explode("_",$period)[0]),'d-m-Y').' - '.date_format(new DateTime(explode("_",$period)[1]),'d-m-Y'):'' }}" required/>
                <input type="hidden" name="filter_period" value="{{ isset($period)?$period:'' }}" />
            </div>
            <div class="col-sm-2" style="margin-top: 2%">
                <input type="submit" class="btn btn-block btn-primary" data-sync="false" value="Get Report">
            </div>
            @if(isset($data) && count($data))
            <div class="col-sm-2 pull-right" style="margin-top: 2%;margin-right: 2%">
                <a class="btn btn-block bg-indigo btnSendSOA">Send SOA</a>
            </div>
            @endif
            <div class="clearfix"></div>
        </form>
    </div>
    <div class="alert alert-info alert-dismissible hide" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        Mails have been sent to queue. It will take some time to trigger emails to patient.
    </div>
    <table class="table table-bordered table-hover table-condensed reportTable" style="width:100%">
        <thead>
            <tr>
                <th width="5%" class="text-center">
                    <input type="checkbox" name="patient_id_all" id="patient_all" value="All" class="chk-col-blue filled-in" @if(isset($data) && count($data)){{ '' }}@else{{ 'disabled' }}@endif>
                    <label for="patient_all" style="height: 10px !important"></label>
                </th>
                <th>Patient ID</th>
                <th>Patient</th>
                <th>Phone</th>
                <th>Total Charges</th>
                <th>Total Payments</th>
                <th width="16%">Outstanding</th>
            </tr>
        </thead>
        <tbody>
        @if(isset($data) && count($data))
            @foreach ($data as $d)
            <tr>
                <td class="text-center">
                    <input type="checkbox" name="patient_ids" id="patient_{{ $d['id'] }}" value="{{ $d['id'] }}" class="chk-col-blue filled-in patient_sel" {!! filter_var($d['email'], FILTER_VALIDATE_EMAIL)?'':' disabled="true" title="Invalid email id!"' !!}>
                    <label for="patient_{{ $d['id'] }}" {!! filter_var($d['email'], FILTER_VALIDATE_EMAIL)?'':'title="Invalid email id!"' !!} style="height: 10px !important"></label>
                </td>
                <td>{{ $d['patient_id'] }}</td>
                <td>{{ $d['patient_name'] }}</td>
                <td>{{ $d['patient_phone'] }}</td>
                <td>{{ $d['total_charges'] }}</td>
                <td>{{ $d['total_payments'] }}</td>
                <td>{{ $d['outstanding_balance'] }}</td>
            </tr>
            @endforeach
        @else
            <tr>
                <td colspan="7" class="text-center">Click 'Get Report' to fetch records</td>
            </tr>
        @endif
        </tbody>
    </table>
</div>
@endsection

@push('report.scripts')
    
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<script type="text/javascript" src="{{ ViewHelper::ThemeJs('daterangepicker.js') }}"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<script>
    var rTable;
    $(document).ready(function() {
        var dateRangePickerConfig = {
            autoApply: true,
            autoUpdateInput: false,
            alwaysShowCalendars: true,
            locale: {
              format: 'DD-MM-YYYY',
              applyLabel: 'Apply',
              cancelLabel: 'Clear'
            },
            ranges: {
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'Last 30 Days': [moment().subtract(29, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        };
        
        $('.daterange').daterangepicker(dateRangePickerConfig, function(start, end, label) {
            $('input[name=filter_period]').val(start.format('YYYY-MM-DD')+'_'+end.format('YYYY-MM-DD'));
        });

        $('.daterange').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        });
        
        @if(isset($data) && count($data))
        $('.table').DataTable({
            "bPaginate": false,
        });
        @endif
        
        function checkOrUncheckPatients(e){            
            if($('input[name=patient_id_all]').is(':checked')){
                $('.patient_sel:not(:disabled)').prop('checked',true);
            }else{
                $('.patient_sel:not(:disabled)').prop('checked',false);
            }
        }
        
        $('.content').on('change','input[name=patient_id_all]', function(){
            checkOrUncheckPatients();
        });
        
        $('.content').on('click', '.btnSendSOA', function(e) {
            e.preventDefault();
            var selectedIDs = [];
            
            $('.patient_sel').each(function(i){
                if($(this).is(':checked'))
                    selectedIDs.push($(this).val());
            });
            
            if(selectedIDs.length <= 0){ alert("Please select atleast 1 record!"); return false; }
            
            console.log(selectedIDs);
            if(selectedIDs){
                $.ajax({
                    url: '{{ route('financial.soa-email') }}',
                    type: 'POST',
                    data: {_token: '{{ csrf_token() }}', patient_ids: selectedIDs, period: $('input[name=filter_period]').val() },
                    success: function (d){
                        console.log(d);
                        $('.alert').removeClass('hide');
                        selectedIDs = [];                        
                        $('input[name=patient_id_all]').prop('checked',false);
                        checkOrUncheckPatients();
                    },
                    error: function (err){
                        console.log(err);
                    }
                });
            }
        });
    });    
</script>
@endpush
