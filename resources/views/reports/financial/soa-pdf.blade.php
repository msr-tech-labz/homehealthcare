{? $fmt = new NumberFormatter( 'en_IN', NumberFormatter::CURRENCY ); ?}
<link href="{{ ViewHelper::ThemePlugin('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('css/print.css') }}" media="print" />

<style>
img.img-topbar{ margin-top: -5px !important;}
.username:after{
    content: ' \25BE';
    padding-left: 10px;
    font-size: 20px;
}
@page
{
    size: auto;   /* auto is the initial value */
    margin: 0mm;  /* this affects the margin in the printer settings */
}

body{
    font-size: 13px;
}
.border-top{
    border-top: 2px solid #333 !important;
}
.border-bottom{
    border-bottom: 2px solid #333 !important;
}
.border-right{
    border-right: 2px solid #333 !important;
}
.border-left{
    border-left: 2px solid #333 !important;
}

.no-padding, .no-padding tbody tr td, .no-padding tbody tr th{
    padding: 0 !important;
    border: 2px solid #333;
}
.no-border, .no-border tbody tr td, .no-border tbody tr th{
    border: 0;
}
.services-table, .services-table thead tr th, .services-table tfoot tr th, .services-table tfoot tr td, .services-table  tbody tr th,.services-table tbody tr td{
    padding: 1px !important;
    border: 0;
}

.tbl-text{
    margin-top:1px !important;
    padding:2px !important;
    margin-bottom:1px !important;
}

h6{
    font-weight: bold;
}

.user-info{ background: rgba(96, 125, 139, 0.10) !important; border-bottom: 0.1em solid #3F51B5 !important; padding: 10px !important;}
</style>

<body class="theme-blue">
    <div class="col-lg-10 col-lg-offset-1 invoicereport" style="border: 0px solid black;background: white;">
        <br>
        <table class="table table-bordered no-padding" style="margin-bottom: 0 !important">
            <!-- Heading -->
            <tr>
                <th colspan="3" class="text-center">
                    <h6 style="font-size:20px; margin-top:2px !important;margin-bottom:2px !important; padding:0px !important">
                        Statement of Accounts
                    </h6>
                </th>
            </tr>
            <!-- Logo, address and payment mode information -->
            <tr>
                <td width="44%">
                    <table class="no-border" style="margin-bottom:0 !important">
                        <tr>
                            <td style="vertical-align: top !important">
                                <center><img src="{{ asset('uploads/provider/'.Helper::encryptor('encrypt',$profile->tenant_id).'/'.$profile->organization_logo) }}" class="org-img" style="width: 60px;margin-top: 15px;clear:both;border-right:0 !important"></center>
                            </td>
                            <td style="vertical-align: top !important">
                                <div style="width:98%;display:inline-block;padding: 5px;margin-left:5px;font-size:12px;padding-top:10px;border-left:0 !important;vertical-align: top !important">
                                    <span style="font-size: 12px !important;font-weight: bold">{{ $profile->organization_name }}</span><br>
                                    {!! !empty($profile->organization_address)?$profile->organization_address.'<br>':'' !!}
                                    {!! !empty($profile->organization_area)?$profile->organization_area.'<br>':'' !!}
                                    {!! !empty($profile->organization_city)?$profile->organization_city:'' !!}
                                    {!! !empty($profile->organization_zipcode)?'-'.$profile->organization_zipcode.'<br>':'<br>' !!}
                                    Tel No.: {!! !empty($profile->phone_number)?'+91-'.$profile->phone_number:'' !!}  {!! !empty($profile->landline_number)?'+91-'.$profile->landline_number:'' !!}
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                
                <td width="25%">
                    <table class="table no-border no-padding no-bottom" style="margin-bottom: 5px !important">
                        <tr>
                            <td class="border-bottom no-bottom">
                                <h6 style="display:inline-block;margin-top:2px !important;padding:5px !important;margin-bottom:5px !important">Stmt</h6><br>
                                <h6 style="display:inline-block;margin-top:2px !important;margin-right:5px;font-weight:normal;padding:5px !important;margin-bottom:5px !important"></h6>
                            </td>
                        </tr>
                        <tr>
                            <td class="no-bottom" style="border:0 !important">
                                <h6 style="margin-top:2px !important;padding:5px !important;margin-bottom: 2px !important">MODE OF PAYMENT</h6>
                                <div class="col-md-12" style="margin-top:2px !important;margin-left:0px;padding-left:5px !important;font-size: 12px !important">
                                    <table>
                                        <tr>
                                            <td>Cheque</td>
                                            <td style="padding-left: 20px !important;">DD</td>
                                        </tr>
                                        <tr>
                                            <td>Bank Transfer</td>
                                            <td style="padding-left: 20px !important;">Cash</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Other</td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                
                <td>
                    <table class="table no-border no-padding" style="margin-bottom: 5px !important">
                        <tr>
                            <td class="border-bottom border-right">
                                <h6 style="margin-top:2px !important;padding:5px !important">DATED</h6>
                            </td>
                            <td></td>
                            <td style="margin-top:2px !important;padding:5px !important;font-style:italic;">{{ date('d-M-Y') }}</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding: 5px !important">
                                Immediate by cheque / DD favouring<br>
                                <h6 style="margin-top:2px !important;padding:5px !important">{{ strtoupper($settings->invoice_payee) }}</h6>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- Customer and bank information -->
            <tr>
                <td width="40%">
                    <div style="padding: 1px 4px !important;margin-bottom: 0px !important">
                        <h6 style="margin-top:2px !important;vertical-align: top !important">
                            <h6 style="font-size:13px;margin-bottom: 1px !important;">{{ $patient->full_name }}</h6>
                            <address style="font-weight:500;margin-bottom: 0px !important;vertical-align: top !important">
                                {!! !empty($patient->street_address)?$patient->street_address.'<br>':'' !!}<br>
                                {!! !empty($patient->area)?$patient->area.'<br>':'' !!}
                                {{ $patient->city.' - '.$patient->zipcode.', '.$patient->state }}<br>
                                Phone: {{ $patient->contact_number.','.$patient->alternate_number }}<br>
                                Email: {{ strtolower($patient->email) }}
                            </address>
                        </h6>
                    </div>
                </td>
                
                <td colspan="2">
                    <table class="table no-padding no-border" style="margin-bottom: 0px !important">
                        <tr>
                            <th width="44.7%" class="border-right border-bottom"><h6 class="tbl-text">PATIENT REFERENCE</h6></th>
                            <td class="border-bottom" style="padding-left: 5px !important">{{ $patient->patient_id }}</td>
                        </tr>
                        <tr>
                            <th class="border-right border-bottom"><h6 class="tbl-text">SO REFERENCE</h6></th>
                            <td class="border-bottom" style="padding-left: 5px !important;padding-top:2px !important"></td>
                        </tr>
                        <tr>
                            <th class="border-right border-bottom"><h6 class="tbl-text">SO DATE</h6></th>
                            <td class="border-bottom" style="padding-left: 5px !important;padding-top:2px !important">{{ isset($fromDate)?$fromDate:'' }} {{ isset($toDate)?' to '.$toDate:'' }}</td>
                        </tr>
                        <tr>
                            <th class="border-right border-bottom"><h6 class="tbl-text">BANK NAME</h6></th>
                            <td class="border-bottom" style="padding-left: 5px !important;padding-top:2px !important">{{ $settings->bank_name ?? '' }}</td>
                        </tr>
                        <tr>
                            <th class="border-right border-bottom"><h6 class="tbl-text">ADDRESS</h6></th>
                            <td class="border-bottom" style="padding-left: 5px !important;padding-top:2px !important">{{ $settings->bank_address ?? '' }}</td>
                        </tr>
                        <tr>
                            <th class="border-right border-bottom"><h6 class="tbl-text">BANK A/C NO</h6></th>
                            <td class="border-bottom" style="padding-left: 5px !important;padding-top:2px !important">{{ $settings->acc_num ?? '' }}</td>
                        </tr>
                        <tr>
                            <th class="border-right"><h6 class="tbl-text">IFSC CODE</h6></th>
                            <td style="padding-left: 5px !important;padding-top:2px !important">{{ $settings->ifsc_code ?? '' }}</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        
        <table class="table services-table" style="margin-top: -1px !important;margin-bottom: 0px !important">
            <thead>
                <tr>
                    <th class="text-center border-top border-right border-bottom" style="font-size:12px !important">Description Of Service</th>
                    <th width="10%" class="text-center border-top border-right border-bottom" style="font-size:12px !important">Unit</th>
                    <th width="15%" class="text-center border-top border-right border-bottom" style="font-size:12px !important">RATE in Rs.</th>
                    <th width="15%" class="text-center border-top border-right border-bottom" style="font-size:12px !important">Tax Amount in Rs.</th>
                    <th width="15%" class="text-center border-top border-right border-bottom" style="font-size:12px !important">Amount in Rs.</th>
                </tr>
            </thead>
            <tbody>
                {? $total = 0; ?}
                {? $serviceCount = 0; ?}
                @if(count($summary))
                    @foreach ($summary as $month => $serviceItem)
                        @if(count($serviceItem))
                            <tr>
                                <th class="border-left border-right" style="line-height:1 !important;padding-top:6px !important;padding-bottom:6px !important;color: #555 !important">{{ $month }}</th>
                                <td class="text-center border-right" style="padding-left:20px !important;line-height:1 !important"></td>
                                <td class="text-center border-right" style="padding-left:20px !important;line-height:1 !important"></td>
                                <td class="text-center border-right" style="padding-left:20px !important;line-height:1 !important"></td>
                                <td class="text-right border-right" style="padding-left:20px !important;padding-right: 20px !important;line-height:1.2 !important"></td>
                            </tr>
                            @foreach ($serviceItem as $item)
                            <tr>
                                <td class="text-left border-left border-right" style="padding-left:20px !important; line-height:1.2 !important">{{ ucwords($item['service_name']) }} - <br><small>{{ $item['from_date'].' to '.$item['to_date'] }}</small></td>
                                <td class="text-center border-right" style="line-height:1.2 !important">{{ $item['chargeable'] }}</td>
                                <td class="text-right border-right" style="line-height:1.2 !important">{{ $item['rate'] }}</td>
                                <td class="text-right border-right" style="line-height:1.2 !important">{{ $item['tax_amount'] }}</td>
                                <td class="text-right border-right" style="padding-right: 20px !important;line-height:1.2 !important">{{ number_format($item['amount'],2) }}</td>
                            </tr>
                            {? $total += floatval($item['amount']); ?}
                            {? $serviceCount += $item['chargeable']; ?}
                            @endforeach
                        @endif
                    @endforeach
                @endif


            {? $billableArray = []; ?}
            {? $totalBillables = 0; ?}
            {? $creditTotal = 0; ?}
            {? $debitTotal = 0; ?}
            {? $totalReceipts = 0; ?}
            {? $totalRefunds = 0; ?}
            @if($to_period)
                {? $billables = \App\Entities\CaseBillables::where('created_at','<=',$to_period)->whereIn('lead_id', \App\Entities\Lead::wherePatientId($patient->id)->pluck('id')->toArray())->get(); ?}
            @else
                {? $billables = \App\Entities\CaseBillables::whereIn('lead_id', \App\Entities\Lead::wherePatientId($patient->id)->pluck('id')->toArray())->get(); ?}
            @endif
            @if(count($billables))
                {? $taxSlab = 0; ?}
                @foreach ($billables as $billable)
                <?php
                            $item = $billable->item;
                            if($billable->item_id != null){
                                switch ($billable->category) {
                                    case 'Consumables': $item = $billable->consumable->consumable_name; $taxSlab = $billable->consumable->tax->tax_rate; break;
                                    case 'Equipments': $item = $billable->surgical->surgical_name; $taxSlab = $billable->surgical->tax->tax_rate; break;
                                }
                            }
                            $amount = $billable->amount;
                            $billableArray[] = [
                                'item' => $item.' - '.$billable->category,
                                'quantity' => $billable->quantity,
                                'rate' => $billable->rate,
                                'tax_amount' => $amount - ($billable->rate * $billable->quantity),
                                'amount' => $amount,
                            ];
                            $totalBillables += floatval($amount);
                ?>
                @endforeach
            @endif
            @if(count($billableArray))
                <tr>
                    <td class="border-left border-right" style="line-height:1.2 !important;color: #000;font-weight: 500 !important;font-size: 15px;padding-top:20px !important">Consumables</td>
                    <td class="border-left border-right"></td>
                    <td class="border-left border-right"></td>
                    <td class="border-left border-right"></td>
                    <td class="border-left border-right"></td>
                </tr>
                @foreach ($billableArray as $ba)
                    <tr>
                        <td class="border-left border-right" style="padding-left:20px !important;line-height:1.2 !important">{{ ucwords($ba['item']) }}</td>
                        <td class="text-right border-right" style="padding-left:20px !important;line-height:1.2 !important">{{ $ba['quantity'] }}</td>
                        <td class="text-right border-right" style="padding-left:20px !important;line-height:1.2 !important">{{ $ba['rate'] }}</td>
                        <td class="text-right border-right" style="padding-left:20px !important;line-height:1.2 !important">{{ $ba['tax_amount'] }}</td>
                        <td class="text-right border-right" style="padding-left:20px !important;padding-right: 20px !important;line-height:0.8 !important">{{ number_format($ba['amount'],2) }}</td>
                    </tr>
                @endforeach
            @endif

            {? $totalPayments = 0; ?}
            @if(count($payments) || count($credit_memos))
                <tr>
                    <td class="border-left border-right" style="line-height:1.2 !important;color: #000;font-weight: 500 !important;font-size: 15px;padding-top:20px !important">Payments</td>
                    <td class="border-left border-right"></td>
                    <td class="border-left border-right"></td>
                    <td class="border-left border-right"></td>
                    <td class="border-left border-right"></td>
                </tr>
            @endif

            @if($payments->sum('total_amount') > 0)
                <tr>
                    <td class="border-left border-right" style="padding-left:20px !important;line-height:0.8 !important">Payment Received</td>
                    <td class="border-left border-right" style="line-height:1.2 !important"></td>
                    <td class="border-left border-right" style="line-height:1.2 !important"></td>
                    <td class="border-left border-right" style="line-height:1.2 !important"></td>
                    {? $totalPayments += floatVal($payments->sum('total_amount')); ?}
                    <td class="text-right border-left border-right" style="padding-right: 20px !important;line-height:1.2 !important">{{ number_format($payments->sum('total_amount'),2) }}</td>
                </tr>
            @endif

            @if(count($credit_memos))
                @foreach ($credit_memos as $credit)
                    @if($credit->type != 'Schedule')
                        <tr>
                            <td class="border-left border-right" style="padding-left:20px !important;line-height:0.8 !important">Memo No. - {{ $credit->credit_memo_no }} on {{ $credit->date->format('d-m-Y') }} <br><br> <b>{{ $credit->comment }}</b></td>
                            <td class="border-left border-right" style="line-height:1.2 !important"></td>
                            <td class="border-left border-right" style="line-height:1.2 !important"></td>
                            <td class="border-left border-right" style="line-height:1.2 !important"></td>
                            @if(empty($credit->receipt))
                                @if($credit->amount >= 0)
                                {? $creditTotal += floatVal($credit->amount); ?}
                                @else
                                {? $debitTotal += floatVal($credit->amount); ?}
                                @endif
                            @endif
                            <td class="text-right border-left border-right" style="padding-right: 20px !important;line-height:1.2 !important">{{ $credit->amount }}</td>
                        </tr>
                    @endif
                    @if($credit->type == 'Schedule' && $credit->invoices->count() == 0 && $credit->receipt)
                        {? $creditTotal -= floatVal($credit->amount); ?}
                    @endif
                @endforeach
            @endif
            
            @if(count($receipts))
                @foreach ($receipts as $receipt)
                    <tr>
                        <td class="border-left border-right" style="padding-left:20px !important;line-height:0.8 !important">Refunds vide Memo No. - {{ $receipt->creditMemo->credit_memo_no }}-{{ $receipt->receipt_date->format('d-m-Y') }}</td>
                        <td class="border-left border-right" style="line-height:0.8 !important"></td>
                        <td class="border-left border-right" style="line-height:0.8 !important"></td>
                        <td class="border-left border-right" style="line-height:0.8 !important"></td>
                        <td class="text-right border-left border-right" style="padding-right: 20px !important;line-height:0.8 !important">{{ $receipt->receipt_amount }}</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
            <tfoot>
                <tr>
                    <td class="border-right border-left border-top border-bottom" style="padding: 8px !important;background: #eee !important">Due / Balance</td>
                    <th class="text-center border-right border-top border-bottom" style="padding: 8px !important;background: #eee !important">{{ $serviceCount }}</th>
                    <td class="border-right border-top border-bottom" style="padding: 8px !important;background: #eee !important"></td>
                    <td class="border-right border-top border-bottom" style="padding: 8px !important;background: #eee !important"></td>
                    <th class="text-right border-right border-top border-bottom" style="padding: 8px !important;padding-right: 20px !important;background: #eee !important">
                        Rs {{ ($total + $totalBillables - $totalPayments - $creditTotal + abs($debitTotal) - $totalRefunds) }} /-</th>
                </tr>
            </tfoot>
        </table>
        
        <table class="table no-border" style="margin-bottom: 2px !important;position:relative;bottom:0">
            <tr>
                <td colspan="2" class="border-left border-top border-right">
                    AMOUNT CHARGABLE IN WORDS:
                    {? $f = new \NumberFormatter("en", \NumberFormatter::SPELLOUT); ?}
                    <b style="margin-top:2px !important">{{ ucwords($f->formatCurrency(($total + $totalBillables - $totalPayments - $creditTotal + abs($debitTotal) - $totalRefunds), "INR")) }}</b><br>
                    <h6 class="tbl-text text-right">E. &amp; O.E</h6><br>
                </td>
            </tr>
            <tr>
                <td class="border-left border-bottom border-right" width="50%">
                    <div style="font-size: 11px !important;padding-left:0 !important">PAN : {{ $settings->pan_number ?? '' }}</div>
                    <div style="font-size: 11px !important;padding-left:0 !important">GSTIN : {{ $settings->gstin ?? '' }}</div>
                    <div style="font-size: 11px !important;margin-top:5px">
                        <small style="font-weight:500">Note: Payment has to be made immediately.<br> Delayed payment shall attract interest at 18% p.a. <br><br> We declare that this statement of account shows the actual price of the goods/ service described and that all particulars are true and correct</small>
                    </div>
                </td>
                <td class="no-padding border-left border-top border-bottom border-right">
                    <h6 class="tbl-text text-right" style="font-size:13px;margin-top:5px !important; margin-right: 8% !important;">for {{ strtoupper($profile->organization_name) }}</h6>
                    <h6 class="sig" style="text-align: right;margin-top: 10%; margin-bottom: 0 !important; margin-right: 20%">AUTHORISED SIGNATORY</h6>
                </td>
            </tr>
        </table>
        
        <center><b>SUBJECT TO {{ strtoupper($profile->organization_city) }} JURISDICTION</b></center>
        <span style="font-size:13px;margin-left:20px;">This is a computer generated statement of accounts.</span><br><br>
    </div>
</div>