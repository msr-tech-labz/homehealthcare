
@extends('layouts.report-layout')

@section('page_title','Receipts Report - Financial | Reports')

@section('report-title','Receipts Report')

@section('report-summary','Summary of receipts report')

@section('report-filter')
    <style>
        table.table-bordered thead tr th, table.table-bordered tbody tr td {
            text-transform: capitalize; !important;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            font-size: 12px !important;
        }
    </style>
    <div class="report-filter">
        <form id="filter-form" class="form-horizontal" method="post">
            <div class="col-sm-4">
                <label class="label-control">Customer</label>
                <select class="form-control input-sm show-tick" name="filter_patient" data-live-search="true">
                    <option value="">-- Select --</option>
                @if(isset($patients) && count($patients))
                    @foreach ($patients as $patient)
                        <option value="{{ $patient->id }}">{{ $patient->full_name.' - '.$patient->patient_id }}</option>
                    @endforeach
                @endif
                </select>
            </div>
            <div class="col-sm-2">
                <label class="label-control">Period</label>
                <input type="text" id="period" class="form-control input-sm daterange" placeholder="Select Period"/>
                <input type="hidden" name="filter_period" />
            </div>
            <div class="col-sm-2 pull-right" style="margin-top: 1%">
                <input type="button" class="btn btn-block btn-primary btnFilter" value="Filter">
            </div>
            <div class="clearfix"></div>
        </form>
    </div>
@endsection

@section('report-content')
<div class="body" style="min-height:200px">
    <div class="table-responsive" style="overflow-x: scroll;">
        <table class="table table-bordered table-hover table-condensed reportTable" style="width:100%">
            <thead>
                <tr>
                    <th>Receipt Date</th>
                    <th>Patient ID</th>
                    <th>Patient</th>
                    <th>Receipt No.</th>
                    <th>Type</th>
                    <th>Receipt Against</th>
                    <th>Payment Mode</th>
                    <th>Reference Number</th>
                    <th>Total Amount</th>
                </tr>
            </thead>
            <tbody>
                <tr><td colspan="14" class="text-center">Select Patient and Period to filter</td></tr>
            </tbody>
        </table>
    </div>
</div>
@endsection

@push('report.scripts')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
    <script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
    <script type="text/javascript" src="{{ ViewHelper::ThemeJs('daterangepicker.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<script>
    var rTable;
    $(document).ready(function() {
        $('.daterange').daterangepicker({
            autoApply: false,
            autoUpdateInput: false,
            alwaysShowCalendars: true,
            locale: {
              format: 'DD-MM-YYYY',
              applyLabel: 'Apply',
              cancelLabel: 'Clear'
            },
            ranges: {
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'Last 30 Days': [moment().subtract(29, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
           }
         },
         function(start, end, label) {
             $('input[name=filter_period]').val(start.format('YYYY-MM-DD')+'_'+end.format('YYYY-MM-DD'));
             $('#period').val(start.format('DD-MM-YYYY')+' - '+end.format('DD-MM-YYYY'))
         }
        );

        $(".cancelBtn").click(function() {
            $('input[name=filter_period]').val('');
            $('#period').val('');
        });

        $('.content').on('click', '.btnFilter',function(e) {
            if ( ! $.fn.DataTable.isDataTable('.reportTable') ) {
                getData();
            }else{
                rTable.draw();
            }
            e.preventDefault();
        });
    });

    function getData(){
        rTable = $('.reportTable').DataTable({
            processing: true,
            serverSide: true,
            lengthMenu: [[25,50,100,200,-1],[25,50,100,200,"All"]],
            language: {
                processing: "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate' style='color:#ff0000'> Loading....</span>"
            },
            ajax: {
                url: '{{ route('financial.receipt-report','ajax=true') }}',
                type: 'POST',
                data: function (d) {
                    d._token = '{{ csrf_token() }}';
                    d.patient = $('select[name=filter_patient] option:selected').val();
                    d.period = $('input[name=filter_period]').val();
                }
            },
            // displayLength: -1,
            dom: 'Bflrpt',
            columns: [
                {data : 'receipt_date', name : 'receipt_date', searchable: true},
                {data : 'patient_id', name : 'patient_id', searchable: true},
                {data : 'patient_name', name : 'patient_name', searchable: true},
                {data : 'receipt_no', name : 'receipt_no', searchable: true},
                {data : 'type', name : 'type', searchable: true},
                {data : 'receipt_against', name : 'receipt_against', searchable: true},
                {data : 'payment_mode', name : 'payment_mode', searchable: true},
                {data : 'reference_no', name : 'reference_no', searchable: true},
                {data : 'total_amount', name : 'total_amount', searchable: true}
            ],
            drawCallback: function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last = null;

                api.column(0, {page:'current'} ).data().each( function ( group, i ) {
                    if(group != null){
                        if ( last !== group ) {
                            $(rows).eq( i ).before(
                                '<tr class="group"><td colspan="9"><b>'+group.toUpperCase()+'</b></td></tr>'
                            );
                            last = group;
                        }
                    }
                });
            },
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: 'Report-Invoice_report_{{ date("d-m-Y_H:i:s") }}'
                }
            ],
        });
    }
</script>
@endpush
