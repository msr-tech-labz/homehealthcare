@extends('layouts.report-layout')

@section('page_title','Outstanding Report - Financial | Reports')

@section('report-title','Outstanding Report')

@section('report-summary','Pending Payments Summary')

@section('report-content')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.material.min.css">
<style>
    .bg-red{
        background-color: rgba(244, 67, 54, 0.26) !important;
    }
    .dataTables_wrapper .dt-buttons a.dt-button {
        background-color: #4f616a;
        background-image: none !important;
    }
    div.dataTables_wrapper div.dataTables_filter {
        text-align: right !important; 
    }
    .pagination li.active a,.pagination li.active a:hover {
        background-color: #3f51b5;
        box-shadow:  0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12)
    }
</style>
<div class="body">
    <div class="loader"></div>
    <div class="col-md-12">
        <form action="{{ route('financial.outstanding') }}" method="post">
            {{ csrf_field() }}
            <div class="col-sm-2">
                    <input type="text" id="till_date" name="till_date" class="form-control datepicker" style="background-color: #fff !important;" placeholder="Till Date" value="{{ isset($filterTillDate)?\Carbon\Carbon::parse($filterTillDate)->format('d-m-Y'):date('01-m-Y') }}" />
            </div>
            <div class="col-sm-2">
                    <button type="submit" class="btn btn-primary waves-effect">FILTER</button>
            </div>
        </form>
    </div>
    <div class="row clearfix outstandingTable"></div>
    {!! $dataTable->table(['style="width:100%"']) !!}
</div>
@endsection

@push('report.scripts')
    
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
<script type="text/javascript" href="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" href="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script rel="stylesheet" href="https://cdn.datatables.net/1.10.19/js/dataTables.material.min.js"></script>
{!! $dataTable->scripts() !!}
<script type="text/javascript">
    $(function(){
        $(".datepicker").flatpickr({
            dateFormat: "d-m-Y",
            mode: "single",
        });
        $('.dataTables_filter input').unbind();
        $('.dataTables_filter input').bind('keyup', function(e){
            if(e.keyCode == 13){
                window.LaravelDataTables["dataTableBuilder"].search(this.value).draw();
            }
        });
    });
</script>
@endpush
