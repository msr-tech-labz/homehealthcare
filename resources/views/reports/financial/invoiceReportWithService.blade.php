
@extends('layouts.report-layout')

@section('page_title','Proforma Invoice with Service Report - Financial | Reports')

@section('report-title','Proforma Invoice with Service Report')

@section('report-summary','Summary of unpaid proforma invoices report with service duration')

@section('report-filter')
    <style>
        table.table-bordered thead tr th, table.table-bordered tbody tr td {
            text-transform: capitalize; !important;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            font-size: 12px !important;
        }
    </style>
    <div class="report-filter">
        <form id="filter-form" class="form-horizontal" method="post">
            <div class="col-sm-4">
                <label class="label-control">Customer</label>
                <select class="form-control input-sm show-tick" name="filter_patient" data-live-search="true">
                    <option value="">-- Select --</option>
                @if(isset($patients) && count($patients))
                    @foreach ($patients as $patient)
                        <option value="{{ $patient->id }}">{{ $patient->full_name.' - '.$patient->patient_id }}</option>
                    @endforeach
                @endif
                </select>
            </div>
            <div class="col-sm-2">
                <label class="label-control">Period</label>
                <input type="text" id="period" class="form-control input-sm daterange" data-toggle="tooltip" data-placement="top" title="" data-original-title="Max 30 days" placeholder="Select Period"/>
                <input type="hidden" name="filter_period" />
            </div>
            <div class="col-sm-2 pull-right" style="margin-top: 1%">
                <input type="button" class="btn btn-block btn-primary btnFilter" value="Filter">
            </div>
            <div class="clearfix"></div>
        </form>
    </div>
@endsection

@section('report-content')
<div class="body" style="min-height:200px">
    <div class="table-responsive" style="overflow-x: scroll;">
        <table class="table table-bordered table-hover table-condensed reportTable" style="width:100%">
            <thead>
                <tr>
                    <th>Invoice Date</th>
                    <th>Patient ID</th>
                    <th>Patient</th>
                    <th>Invoice No.</th>
                    <th>Serviced From</th>
                    <th>Serviced to</th>
                    <th>Invoice Due Date</th>
                    <th>Days left for Due</th>
                    <th>Ageing</th>
                    <th>Receipts Info</th>
                    <th>Total Amount</th>
                    <th>Amount Paid</th>
                    <th>Outstanding</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                <tr><td colspan="14" class="text-center">Select Period to filter</td></tr>
            </tbody>
        </table>
    </div>
</div>
@endsection

@push('report.scripts')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
    <script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
    <script src="../../themes/default/js/pages/ui/tooltips-popovers.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
    var rTable;
    $(document).ready(function() {
        $('.daterange').daterangepicker({
            autoApply: false,
            autoUpdateInput: false,
            alwaysShowCalendars: true,
            locale: {
              format: 'DD-MM-YYYY',
              applyLabel: 'Apply',
              cancelLabel: 'Clear'
            },
            maxSpan: {
                    days: 30
            },
            ranges: {
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'Last 30 Days': [moment().subtract(29, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
           }
         },
         function(start, end, label) {
             $('input[name=filter_period]').val(start.format('YYYY-MM-DD')+'_'+end.format('YYYY-MM-DD'));
             $('#period').val(start.format('DD-MM-YYYY')+' - '+end.format('DD-MM-YYYY'))
         }
        );

        $(".cancelBtn").click(function() {
            $('input[name=filter_period]').val('');
            $('#period').val('');
        });

        $('.content').on('click', '.btnFilter',function(e) {
            if($('input[name=filter_period]').val() != ''){
                if ( ! $.fn.DataTable.isDataTable('.reportTable') ) {
                    getData();
                }else{
                    rTable.draw();
                }
                e.preventDefault();
            }else{
                alert('Please Choose Date Range');
            }
        });
    });

    function getData(){
        rTable = $('.reportTable').DataTable({
            processing: true,
            serverSide: true,
            lengthMenu: [[25,75,50,100,150,200,-1],[25,75,50,100,150,200,"All"]],
            language: {
                processing: "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate' style='color:#ff0000'> Loading....</span>"
            },
            ajax: {
                url: '{{ route('financial.invoice-report-with-service','ajax=true') }}',
                type: 'POST',
                data: function (d) {
                    d._token = '{{ csrf_token() }}';
                    d.patient = $('select[name=filter_patient] option:selected').val();
                    d.period = $('input[name=filter_period]').val();
                }
            },
            // displayLength: -1,
            dom: 'Bflrpt',
            columns: [
                {data : 'invoice_date', name : 'invoice_date', searchable: true},
                {data : 'patient_id', name : 'patient_id', searchable: true},
                {data : 'patient_name', name : 'patient_name', searchable: true},
                {data : 'invoice_no', name : 'invoice_no', searchable: true},
                {data : 'serviced_from', name : 'serviced_from', searchable: true},
                {data : 'serviced_to', name : 'serviced_to', searchable: true},
                {data : 'invoice_due_date', name : 'invoice_due_date', searchable: true},
                {data : 'days_left', name : 'days_left', searchable: true},
                {data : 'ageing', name : 'ageing', searchable: true},
                {data : 'receipts_info', name : 'receipts_info', searchable: false, orderable: false ,render: function(data, type, row){
                        return data.split(",").join("<br/>");
                }},
                {data : 'total_amount', name : 'total_amount', searchable: true},
                {data : 'amount_paid', name : 'amount_paid', searchable: true},
                {data : 'outstanding', name : 'outstanding', searchable: true},
                {data : 'status', name : 'status', searchable: true},
            ],
            drawCallback: function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last = null;

                api.column(0, {page:'current'} ).data().each( function ( group, i ) {
                    if(group != null){
                        if ( last !== group ) {
                            $(rows).eq( i ).before(
                                '<tr class="group"><td colspan="14"><b>'+group.toUpperCase()+'</b></td></tr>'
                            );
                            last = group;
                        }
                    }
                });
            },
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: 'Report-Invoice_with_service_report_{{ date("d-m-Y_H:i:s") }}'
                }
            ],
        });
    }
</script>
@endpush
