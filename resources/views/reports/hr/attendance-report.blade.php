@extends('layouts.report-layout')

@section('page_title','Attendance Report - HR | Reports')

@section('report-title','Attendance Report')

@section('report-summary','Employee attendance data')
<style type="text/css">
    td,th{
        padding: 0 !important;
        border:1px solid #eee !important;
        font-size: 12px !important;
    }
</style>
@section('report-filter')
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
    <div class="report-filter">
        <form action="{{ route('hr.attendance-report') }}" method="post">
            {{ csrf_field() }}
            <table class="table table-bordered">
                <tr>
                    <th>
                        <select class="form-control" name="filter_user">
                            <option value="">-All Employees-</option>
                            <optgroup label="Filter by Department">
                        @if(isset($departments))
                           @foreach($departments as $d)
                                <option style="text-transform: uppercase;" value="dept_{{ $d->id }}" {{ $filters['filter_user']=='dept_'.$d->id?'selected':'' }}>{{ $d->department_name }}</option>
                           @endforeach
                        @endif
                            </optgroup>
                            <optgroup label="Filter by Employee">
                         @if(isset($employees))
                            @foreach($employees as $e)
                                 <option style="text-transform: uppercase;" value="emp_{{ $e->id }}" {{ $filters['filter_user']=='emp_'.$e->id?'selected':'' }}>{{ $e->full_name }}</option>
                            @endforeach
                         @endif
                             </optgroup>
                        </select>
                    </th>
                    <th>
                        <input type="text" name="filter_date" id="filter_date">
                    </th>
                    <th><input type="submit" class="btn btn-success"></th>
                </tr>
            </table>
        </form>
    </div>
@endsection

@section('report-content')
    <div class="body table-responsive" style="overflow-x: scroll !important">
        <table id="resourceReport" style="min-width:100% !important;font-size: 12px !important;margin-right: 20px !important" class="table table-bordered table-hover table-condensed reportTable">
            <thead>
                <tr>
                    <th>#</th>
                    <th width="8%">Employee ID</th>
                    <th>Name</th>
                    <th>Number</th>
                    <th>Designation</th>
                    <th>Schedule</th>
                    <th>Punch In</th>
                    <th>Punch Out</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($caregivers as $i => $c)
                    <tr>
                        <td>{{ $i + 1 }}</td>
                        <td>{{ $c->employee_id }}</td>
                        <td>{{ $c->full_name }}</td>
                        <td>{{ $c->mobile_number }}</td>
                        <td>{{ isset($c->professional->designations)?$c->professional->designations->designation_name:'' }}</td>
                        <td>
                            <table width="100%">
                                @forelse($c->attendance->where('date',$date) as $ca)
                                <tr><td>{{ isset($ca->schedule)?$ca->schedule->patient->full_name.' - '.$ca->schedule->service->service_name:'-' }}</td></tr>
                                @empty
                                <tr><td> - </td></tr>
                                @endforelse
                            </table>
                        </td>
                        <td>
                            <table width="100%">
                                @forelse($c->attendance->where('date',$date) as $ca)
                                <tr><td>{{ $ca->punch_in ?? '-' }}</td></tr>
                                @empty
                                <tr><td> - </td></tr>
                                @endforelse
                            </table>
                        </td>
                        <td>
                            <table width="100%">
                                @forelse($c->attendance->where('date',$date) as $ca)
                                <tr>
                                    <td @if($ca->punch_in && !$ca->punch_out){!!'style="background-color:#f3c8c8;height:15px;"'!!}@endif>{{ $ca->punch_out ?? '-'}}</td>
                                </tr>
                                @empty
                                <tr><td> - </td></tr>
                                @endforelse
                            </table>
                        </td>
                        <td>
                            <table width="100%">
                                @forelse($c->attendance->where('date',$date) as $ca)
                                <tr>
                                    <td>{{ $ca->status == "P" ?$ca->status:'-' }}</td>
                                </tr>
                                @empty
                                <tr><td> - </td></tr>
                                @endforelse
                            </table>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection


@push('report.scripts')
<!-- Moment Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<script>
$(function(){
    $('.reportTable').DataTable({
        scrollY: false,
        scrollX: true,
        scrollCollapse: true,
        lengthMenu: [[50,100,200,-1],[50,100,200,"All"]],
        language: {
            processing: "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate' style='color:#ff0000'> Loading....</span>"
        },
        order: [[0,'asc']],
        // displayLength: -1,
        dom: 'Bflrpt',
        buttons: [
            {
                extend: 'excelHtml5',
                title: 'Attendance-Data_{{ date("d-m-Y") }}'
            },
            {
                extend: 'pdfHtml5',
                pageSize: 'A3'
            },
            {
                extend: 'print',
                title: '<h3 style="text-align:center;">Attendance Data for.{{ date("d-m-Y") }}.</h3>',
                customize: function ( win ) {
                    $(win.document.body).css( 'font-size', '7pt' );

                    $(win.document.body).find( 'table' ).addClass( 'compact' ).css( 'font-size', 'inherit' );
                }
            }
        ],
    });
    $('#filter_date').bootstrapMaterialDatePicker({
        format: 'DD-MM-YYYY',
        clearButton: true,
        time: false,
        maxDate : new Date(),
    });
});
</script>
@endpush
