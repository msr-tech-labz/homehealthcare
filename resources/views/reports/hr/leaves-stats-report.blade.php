@extends('layouts.report-layout')

@section('page_title','Leave Stats Report - HR | Reports')

@section('report-title','Leave Stats Report')

@section('report-summary','Employee Leave stats data')

@section('report-filter')
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
    <div class="report-filter">
        <form action="{{ route('hr.leaves-stats-report') }}" method="post">
            {{ csrf_field() }}
            <table class="table table-bordered">
                <tr>
                    <th>
                        <label class="label-control">From Date</label>
                        <input type="text" name="from_date" class="filter_date" value={{ isset($filters['from_date'])?$filters['from_date']:'' }}>
                    </th>
                    <th>
                        <label class="label-control">To Date</label>
                        <input type="text" name="to_date" class="filter_date" value={{ isset($filters['to_date'])?$filters['to_date']:'' }}>
                    </th>
                    <th><input type="submit" class="btn btn-success"></th>
                </tr>
            </table>
        </form>
    </div>
@endsection

@section('report-content')
    <div class="body table-responsive" style="overflow-x: scroll !important">
        <table id="resourceReport" style="width:100%;font-size: 12px !important;margin-right: 20px !important" class="table table-bordered table-hover table-condensed reportTable">
            <thead style="white-space: nowrap;">
                <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th width="10%">DOJ</th>
                    <th width="10%">DOR</th>
                    <th>Role</th>
                    <th>Specialization</th>
                    <th>Shifts</th>
                    <th>Bench</th>
                    <th>Training</th>
                    <th>Absent</th>
                    <th>Comp-Off</th>
                    <th>Sick Leave</th>
                    <th>Casual Leave</th>
                    <th>Maternity Leave</th>
                    <th>Abscond</th>
                    <th>Marked Days</th>
                    <th>Unmarked Days</th>
                </tr>
            </thead>
            <tbody>
                {? $i=1; ?}
                @foreach($leaves as $cid => $l)
                    {? $totalShifts = \Helper::getTotalShifts($cid,$filters['from_date'],$filters['to_date']) ?}
                    {? $onBench = \Helper::getOnBench($cid,$filters['from_date'],$filters['to_date']) ?}
                    {? $onTraining = \Helper::getOnTraining($cid,$filters['from_date'],$filters['to_date']) ?}
                    {? $absent = \Helper::getAbsent($cid,$filters['from_date'],$filters['to_date']) ?}
                    {? $markedDays = \Helper::getMarkedDays($cid,$filters['from_date'],$filters['to_date']) ?}
                    <tr>
                        <td>{{ $i }}</td>
                        <td>{{ \Helper::getStaffID($cid) }}</td>
                        <td>{{ \Helper::getStaffName($cid) }}</td>
                        <td>{{ \Helper::getDOJ($cid) }}</td>
                        <td>{{ \Helper::getDOR($cid) }}</td>
                        <td>{{ \Helper::getStaffRole($cid) }}</td>
                        <td>{{ \Helper::getStaffDesignation($cid) }}</td>
                        <td>{{$totalShifts}}</td>
                        <td>{{$onBench}}</td>
                        <td>{{$onTraining}}</td>
                        <td>{{$absent}}</td>
                        <td>{!! isset($l['CO'])?'<b style="color:red;">'.$l['CO'].'</b>':'0' !!}</td>
                        <td>{!! isset($l['SL'])?'<b style="color:red;">'.$l['SL'].'</b>':'0' !!}</td>
                        <td>{!! isset($l['CL'])?'<b style="color:red;">'.$l['CL'].'</b>':'0' !!}</td>
                        <td>{!! isset($l['ML'])?'<b style="color:red;">'.$l['ML'].'</b>':'0' !!}</td>
                        <td>{!! isset($l['AC'])?'<b style="color:red;">'.$l['AC'].'</b>':'0' !!}</td>
                        <td>{{ $markedDays }}</td>
                        <td>{{ $totalDays - $markedDays }}</td>
                    </tr>
                {? $i++; ?}
                @endforeach
                @foreach($excludedEmployees as $exEmp)
                <tr>
                    {? $totalShifts = \Helper::getTotalShifts($exEmp->id,$filters['from_date'],$filters['to_date']) ?}
                    {? $onBench = \Helper::getOnBench($exEmp->id,$filters['from_date'],$filters['to_date']) ?}
                    {? $onTraining = \Helper::getOnTraining($exEmp->id,$filters['from_date'],$filters['to_date']) ?}
                    {? $absent = \Helper::getAbsent($exEmp->id,$filters['from_date'],$filters['to_date']) ?}
                    {? $markedDays = \Helper::getMarkedDays($exEmp->id,$filters['from_date'],$filters['to_date']) ?}
                    <td>{{ $i }}</td>
                    <td>{{ $exEmp->employee_id}}</td>
                    <td>{{ $exEmp->first_name.' '.$exEmp->middle_name.' '.$exEmp->last_name }}</td>
                    <td>{{ isset($exEmp->professional->date_of_joining)?$exEmp->professional->date_of_joining->format('d-m-Y'):'' }}</td>
                    <td>{{ isset($exEmp->professional->resignation_date)?$exEmp->professional->resignation_date->format('d-m-Y'):'' }}</td>
                    <td>{{ $exEmp->professional->role }}</td>
                    <td>{{ isset($exEmp->professional->designations)?$exEmp->professional->designations->designation_name:'' }}</td>
                    <td>{{$totalShifts}}</td>
                    <td>{{$onBench}}</td>
                    <td>{{$onTraining}}</td>
                    <td>{{$absent}}</td>
                    <td><b style="color:blue;">0</b></td>
                    <td><b style="color:blue;">0</b></td>
                    <td><b style="color:blue;">0</b></td>
                    <td><b style="color:blue;">0</b></td>
                    <td><b style="color:blue;">0</b></td>
                    <td>{{ $markedDays }}</td>
                    <td>{{ $totalDays - $markedDays }}</td>
                </tr>
                {? $i++; ?}
                @endforeach
            </tbody>
        </table>
    </div>
@endsection


@push('report.scripts')
<!-- Moment Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<script>
$(function(){
    if({{count($leaves)}} || {{count($excludedEmployees)}}){
        $('.reportTable').DataTable({
            scrollY: 260,
            scrollX: true,
            scrollCollapse: true,
            lengthMenu: [[50,100,200,-1],[50,100,200,"All"]],
            language: {
                processing: "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate' style='color:#ff0000'> Loading....</span>"
            },
            order: [[0,'asc']],
            // displayLength: -1,
            dom: 'Bflrpt',
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: 'Attendance-Data_{{ $filters['from_date'] or date('d-m-Y') }}_to_{{ $filters['to_date'] or date('d-m-Y') }}'
                }
            ],
        });
    }

    $('.filter_date').bootstrapMaterialDatePicker({
        format: 'DD-MM-YYYY',
        clearButton: true,
        time: false,
        maxDate : new Date(),
    });
});
</script>
@endpush