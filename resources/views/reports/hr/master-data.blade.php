@extends('layouts.report-layout')

@section('page_title','Master Data - HR | Reports')

@section('report-title','HR Master Data')

@section('report-summary','Employee master data')
<style type="text/css">
    .table-responsive{
        overflow-x: scroll !important;
    }
    .toggle-vis{
        cursor: pointer;
    }
</style>
@section('report-filter')
    <div class="report-filter">
        <form id="filter-form" class="form-horizontal" method="post">
            <div class="col-sm-3">
                <label class="label-control">Employee ID</label>
                <input type="text" id="employee_id" class="form-control input-sm" placeholder="Employee ID" />                
            </div>
            <div class="col-sm-3">
                <label class="label-control">Gender</label>
                <select id="gender" class="form-control">
                    <option value="">-- All --</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                </select>
            </div>
            <div class="col-sm-3">
                <label class="label-control">Marital Status</label>
                <select id="marital_status" class="form-control">
                    <option value="">-- All --</option>
                    <option value="Single">Single</option>
                    <option value="Married">Married</option>
                    <option value="Other">Other</option>
                </select>
            </div>
            <div class="col-sm-3">
                <label class="label-control">Religion</label>
                <select class="form-control show-tick" data-live-search="true" id="religion">
                    <option value="">-- All --</option>
                    <option value="Buddhism">Buddhism</option>
                    <option value="Christianity">Christianity</option>
                    <option value="Hinduism">Hinduism</option>
                    <option value="Islam">Islam</option>
                    <option value="Jainism">Jainism</option>
                    <option value="Sikhism">Sikhism</option>
                    <option value="Judaism">Judaism</option>
                    <option value="Other">Other</option>
                </select>
            </div>
            <div class="clearfix"></div><br>
            <div class="col-sm-3">
                <label class="label-control">Specialization</label>
                <select class="form-control show-tick" data-live-search="true" id="specialization">
                    <option value="">-- All --</option>
            @if(isset($specializations) && count($specializations))
                @foreach ($specializations as $specialization)
                    <option value="{{ $specialization->specialization_name }}">{{ $specialization->specialization_name }}</option>
                @endforeach
            @endif
                </select>               
            </div>
            <div class="col-sm-3">
                <label class="label-control">Designation</label>
                <select class="form-control show-tick" data-live-search="true" id="designation">
                    <option value="">-- All --</option>
            @if(isset($designations) && count($designations))
                @foreach ($designations as $designation)
                    <option value="{{ $designation->designation_name }}">{{ $designation->designation_name }}</option>
                @endforeach
            @endif
                </select>               
            </div>
            <div class="col-sm-3">
                <label class="label-control">Department</label>
                <select class="form-control show-tick" data-live-search="true" id="department">
                    <option value="">-- All --</option>
            @if(isset($departments) && count($departments))
                @foreach ($departments as $department)
                    <option value="{{ $department->department_name }}">{{ $department->department_name }}</option>
                @endforeach
            @endif
                </select>
            </div>
            <div class="col-sm-3">
                <label class="label-control">Manager</label>
                <select class="form-control show-tick" data-live-search="true" id="manager">
                    <option value="">-- All --</option>
            @if(isset($managers) && count($managers))
                @foreach ($managers as $manager)
                    <option value="{{ $manager->full_name }}">{{ $manager->full_name.' - '.$manager->employee_id }}</option>
                @endforeach
            @endif
                </select>
            </div>
            <div class="clearfix"></div><br>
            <div class="col-sm-3">
                <label class="label-control">Role</label>
                <select class="form-control show-tick" id="role">
                    <option value="">-- All --</option>
                    <option value="Caregiver">Caregiver</option>
                    <option value="Manager">Manager</option>
                </select>
            </div>
            <div class="col-sm-3">
                <label class="label-control">Employment Type</label>
                <select class="form-control show-tick" id="employment_type">
                    <option value="">-- All --</option>
                    <option value="Staff">Staff</option>
                    <option value="Freelancer">Freelancer</option>
                </select>
            </div>
            <div class="col-sm-3">
                <label class="label-control">Status</label>
                <select class="form-control show-tick" id="status">
                    <option value="">-- All --</option>
                    <option value="Active">Active</option>
                    <option value="Inactive">Inactive</option>
                </select>
            </div>
            <div class="col-sm-2 pull-right" style="margin-top: 2.5%">
                <input type="button" class="btn btn-block btn-primary btnFilter" value="Filter">
            </div>
            <div class="clearfix"></div>
        </form>
    </div>
@endsection

@section('report-content')
<div class="body" style="min-height:200px">
    <div class="table-responsive">
        <div>
            Toggle column: 
            <a class="toggle-vis" data-column="0">Employee ID</a> - 
            <a class="toggle-vis" data-column="1">Name</a> - 
            <a class="toggle-vis" data-column="2">DOB</a> - 
            <a class="toggle-vis" data-column="3">Gender</a> - 
            <a class="toggle-vis" data-column="4">Blood Group</a> - 
            <a class="toggle-vis" data-column="5">Marital Status</a> - 
            <a class="toggle-vis" data-column="6">Religion</a> - 
            <a class="toggle-vis" data-column="7">Mobile Number</a> - 
            <a class="toggle-vis" data-column="8">Alternate Number</a> - 
            <a class="toggle-vis" data-column="9">Email</a> - 
            <a class="toggle-vis" data-column="10">Current Address</a> - 
            <a class="toggle-vis" data-column="11">Current Area</a> - 
            <a class="toggle-vis" data-column="12">Current City</a> - 
            <a class="toggle-vis" data-column="13">Current Zipcode</a> - 
            <a class="toggle-vis" data-column="14">Current State</a> - 
            <a class="toggle-vis" data-column="15">Current Country</a> - 
            <a class="toggle-vis" data-column="16">Permanent Address</a> - 
            <a class="toggle-vis" data-column="17">Permanent Area</a> - 
            <a class="toggle-vis" data-column="18">Permanent City</a> - 
            <a class="toggle-vis" data-column="19">Permanent Zipcode</a> - 
            <a class="toggle-vis" data-column="20">Permanent State</a> - 
            <a class="toggle-vis" data-column="21">Permanent Country</a> - 
            <a class="toggle-vis" data-column="22">Specialization</a> - 
            <a class="toggle-vis" data-column="23">Qualification</a> - 
            <a class="toggle-vis" data-column="24">Experience</a> - 
            <a class="toggle-vis" data-column="25">Languages Known</a> - 
            <a class="toggle-vis" data-column="26">Role</a> - 
            <a class="toggle-vis" data-column="27">Employment Type</a> - 
            <a class="toggle-vis" data-column="28">Designation</a> - 
            <a class="toggle-vis" data-column="29">Department</a> - 
            <a class="toggle-vis" data-column="30">Manager</a> - 
            <a class="toggle-vis" data-column="31">DOJ</a> - 
            <a class="toggle-vis" data-column="32">DOR</a> - 
            <a class="toggle-vis" data-column="33">Resign Type</a> - 
            <a class="toggle-vis" data-column="34">Source</a> - 
            <a class="toggle-vis" data-column="35">Source Name</a> - 
            <a class="toggle-vis" data-column="36">PAN Number</a> - 
            <a class="toggle-vis" data-column="37">Aadhar Number</a> - 
            <a class="toggle-vis" data-column="38">Account Name</a> - 
            <a class="toggle-vis" data-column="39">Bank Name</a> - 
            <a class="toggle-vis" data-column="40">Branch</a> - 
            <a class="toggle-vis" data-column="41">IFSC Code</a> - 
            <a class="toggle-vis" data-column="42">Status</a>
        </div>
        <table class="table table-bordered table-hover table-condensed reportTable dispaly nowrap" cellspacing="0" style="width:100%;font-size: 12px !important;">
            <thead>
                <tr>
                    <th>Employee ID</th>
                    <th>Name</th>
                    <th>DOB</th>
                    <th>Gender</th>
                    <th>Blood Group</th>
                    <th>Marital Status</th>
                    <th>Religion</th>
                    <th>Mobile Number</th>
                    <th>Alternate Number</th>
                    <th>Email</th>
                    <th>Current Address</th>
                    <th>Current Area</th>
                    <th>Current City</th>
                    <th>Current Zipcode</th>
                    <th>Current State</th>
                    <th>Current Country</th>
                    <th>Permanent Address</th>
                    <th>Permanent Area</th>
                    <th>Permanent City</th>
                    <th>Permanent Zipcode</th>
                    <th>Permanent State</th>
                    <th>Permanent Country</th>
                    <th>Specialization</th>
                    <th>Qualification</th>
                    <th>Experience</th>
                    <th>Languages Known</th>
                    <th>Role</th>
                    <th>Employment Type</th>
                    <th>Designation</th>
                    <th>Department</th>
                    <th>Manager</th>
                    <th>DOJ</th>
                    <th>DOR</th>
                    <th>Resign Type</th>
                    <th>Source</th>
                    <th>Source Name</th>
                    <th>PAN Number</th>
                    <th>Aadhar Number</th>
                    <th>Account Name</th>
                    <th>Bank Name</th>
                    <th>Branch</th>
                    <th>IFSC Code</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
            @if(count($employees))
                @foreach ($employees as $e)
                    <tr>
                        <td>{{ $e->employee_id ?? '' }}</td>
                        <td>{{ $e->full_name ?? '' }}</td>
                        <td>{{ $e->date_of_birth?$e->date_of_birth->format('d-m-Y'):'' }}</td>
                        <td>{{ $e->gender ?? '' }}</td>
                        <td>{{ $e->blood_group ?? '' }}</td>
                        <td>{{ $e->marital_status ?? '' }}</td>
                        <td>{{ $e->religion ?? '' }}</td>
                        <td>{{ $e->mobile_number ?? '' }}</td>
                        <td>{{ $e->alternate_number ?? '' }}</td>
                        <td>{{ $e->email ?? '' }}</td>
                        <td>{{ $e->current_address ?? '' }}</td>
                        <td>{{ $e->current_area ?? '' }}</td>
                        <td>{{ $e->current_city ?? '' }}</td>
                        <td>{{ $e->current_zipcode ?? '' }}</td>
                        <td>{{ $e->current_state ?? '' }}</td>
                        <td>{{ $e->current_country ?? '' }}</td>
                        <td>{{ $e->permanent_address ?? '' }}</td>
                        <td>{{ $e->permanent_area ?? '' }}</td>
                        <td>{{ $e->permanent_city ?? '' }}</td>
                        <td>{{ $e->permanent_zipcode ?? '' }}</td>
                        <td>{{ $e->permanent_state ?? '' }}</td>
                        <td>{{ $e->permanent_country ?? '' }}</td>
                        <td>{{ $e->professional->specializations->specialization_name ?? '' }}</td>
                        <td>{{ $e->professional->qualification ?? '' }}</td>
                        <td>{{ $e->professional->experience ?? '' }}</td>
                        <td>{{ $e->professional->languages_known ?? '' }}</td>
                        <td>{{ $e->professional->role ?? '' }}</td>
                        <td>{{ $e->professional->employment_type ?? '' }}</td>
                        <td>{{ $e->professional->designations->designation_name ?? '' }}</td>
                        <td>{{ $e->professional->departments->department_name ?? '' }}</td>
                        <td>{{ isset($e->professional->reportingmanager)?$e->professional->reportingmanager->full_name:'' }}</td>
                        <td>{{ !empty($e->professional->date_of_joining)?$e->professional->date_of_joining->format('d-m-Y'):'' }}</td>
                        <td>{{ !empty($e->professional->resignation_date)?$e->professional->resignation_date->format('d-m-Y'):'' }}</td>
                        <td>{{ $e->professional->resignation_type ?? '' }}</td>
                        <td>{{ $e->professional->source ?? '' }}</td>
                        <td>{{ $e->professional->source_name ?? '' }}</td>
                        <td>{{ $e->account->pan_number ?? '' }}</td>
                        <td>{{ $e->account->aadhar_number ?? '' }}</td>
                        <td>{{ $e->account->account_number ?? '' }}</td>
                        <td>{{ $e->account->bank_name ?? '' }}</td>
                        <td>{{ $e->account->bank_branch ?? '' }}</td>
                        <td>{{ $e->account->ifsc_code ?? '' }}</td>
                        <td>{{ (isset($e->user) && $e->user->status == 1)?'Active':'Inactive' }}</td>
                    </tr>
                @endforeach
            @else
                <tr><td colspan="46" class="text-center">No records</td></tr>
            @endif
            </tbody>
        </table>
    </div>
</div>
@endsection

@push('report.scripts')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
    <script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
    <script type="text/javascript" src="{{ ViewHelper::ThemeJs('daterangepicker.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<script>
    var rTable;
    $(document).ready(function() {
        $.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});

        rTable = $('.reportTable').DataTable({
            bSearchable:true,
            bFilter: true,        
            lengthMenu: [[50,100,200,-1],[50,100,200,"All"]],
            language: {
                processing: "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate' style='color:#ff0000'> Loading....</span>"
            },
            processing: true,
            serverSide: false,
            //order: [[0,'asc']],
            dom: 'Bflirpt',
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: 'HR-Master-Data_{{ date("d-m-Y_H:i:s") }}'
                },
                {
                    extend: 'pdfHtml5',
                    pageSize: 'A3'
                },
                {
                    extend: 'print',
                    title: '<h3 style="text-align:center;">HR Master Data</h3>',
                    customize: function ( win ) {
                        $(win.document.body).css( 'font-size', '7pt' );
                        $(win.document.body).find( 'table' ).addClass( 'compact' ).css( 'font-size', 'inherit' );
                    }
                }
            ],
        });

        $('a.toggle-vis').on( 'click', function (e) {
                e.preventDefault();
                var column = rTable.column( $(this).attr('data-column') );
                column.visible( ! column.visible() );
        } );
    
        $('.content').on('click', '.btnFilter', function(e) {
            var filter1 = $('#employee_id').val();
            var filter2 = $('#gender option:selected').val();
            var filter3 = $('#marital_status option:selected').val();
            var filter4 = $('#religion option:selected').val();
            var filter5 = $('#specialization option:selected').val();
            var filter6 = $('#designation option:selected').val();
            var filter7 = $('#department option:selected').val();
            var filter8 = $('#manager option:selected').val();
            var filter9 = $('#role option:selected').val();
            var filter10 = $('#employment_type option:selected').val();
            var filter11 = $('#status option:selected').val();
            
            filterColumn(0, filter1);
            filterColumn(3, filter2);
            filterColumn(5, filter3);            
            filterColumn(6, filter4);
            filterColumn(22, filter5);
            filterColumn(26, filter9);
            filterColumn(27, filter10);
            filterColumn(28, filter6);
            filterColumn(29, filter7);
            filterColumn(30, filter8);
            filterColumn(42, filter11);
            
            navigationFn.goToSection('.body');
        });
        
        function filterColumn ( i, searchString ) {
            //console.log(i+' -> '+searchString);
            if(typeof rTable != 'undefined'){
                if(searchString != '')
                    $('.reportTable').DataTable().column( i ).search("^" + searchString + "$", true, false).draw();
                else
                    $('.reportTable').DataTable().column( i ).search("").draw();
            }
        }
        
        var navigationFn = {
            goToSection: function(id) {
                $('html, body').animate({
                    scrollTop: $(id).offset().top
                }, 1000);
            }
        }
    });
</script>
@endpush
