@extends('layouts.report-layout')

@section('page_title','Customer Feedback | Reports')

@section('report-title','Customer Feedback Report')

@section('report-summary','Summary feedback Status Report')

@section('report-filter')
    <style>
        table.table-bordered thead tr th, table.table-bordered tbody tr td {
            text-transform: capitalize; !important;
            text-overflow: ellipsis;
            white-space: nowrap;
            font-size: 12px !important;
        }

        table {
            display: block;
            overflow: scroll;
        }
    </style>
    <div class="report-filter">
        <form id="filter-form" class="form-horizontal" method="post">
            <div class="col-sm-2">
                <label class="label-control">Action Status</label>
                <select class="form-control input-sm" id="filter_status" name="filter_status" data-live-search="true">
                    <option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- Any --</option>
                    <option value="Pending">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pending</option>
                    <option value="Completed">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Completed</option>
                </select>
            </div>
            <div class="col-sm-2">
                <label class="label-control">Period</label>
                <input type="text" id="period" class="form-control input-sm daterange" placeholder="Select Period"/>
                <input type="hidden" name="filter_period" />
            </div>
            <div class="col-sm-2 pull-right" style="margin-top: 1%">
                <input type="button" class="btn btn-block btn-primary btnFilter" value="Filter">
            </div>
            <div class="clearfix"></div>
        </form>
    </div>
@endsection

@section('report-content')
<div class="body table-responsive" style="min-height:200px">
    <table class="table table-bordered table-hover table-condensed reportTable" id="reportTable" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th width="90%">Name</th>
                <th>Patient Name</th>
                <th>Schedule Id</th>
                <th>Schedule Date</th>
                <th>Caring And Patience</th>
                <th>Knowledge</th>
                <th>Cleanliness</th>
                <th>Reliabilty and Punctuality</th>
                <th>Communication with patient</th>
                <th>Over All Rating(/5)</th>
                <th>Comment</th>
                <th>Action</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <tr><td colspan="15" class="text-center">Select Period to filter</td></tr>
        </tbody>
    </table>
</div>
@endsection

@push('report.scripts')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
    <script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
    <script type="text/javascript" src="{{ ViewHelper::ThemeJs('daterangepicker.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<script>
    var rTable;
    $(document).ready(function() {
        $('.daterange').daterangepicker({
            autoApply: true,
            autoUpdateInput: false,
            alwaysShowCalendars: true,
            locale: {
              format: 'DD-MM-YYYY',
              applyLabel: 'Apply',
              cancelLabel: 'Clear'
            },
            ranges: {
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'Last 30 Days': [moment().subtract(29, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
           }
        },
        function(start, end, label) {
            $('input[name=filter_period]').val(start.format('YYYY-MM-DD')+'_'+end.format('YYYY-MM-DD'));
            $('#period').val(start.format('DD-MM-YYYY')+' - '+end.format('DD-MM-YYYY'))
        }
        );

        $('.content').on('click', '.btnFilter',function(e) {
            if($('input[name=filter_period]').val() == ''){
                alert("Please select a period filter!");
                return false;          
            }
            if ( ! $.fn.DataTable.isDataTable('.reportTable') ) {
                getData();
            }else{
                rTable.draw();
            }
            e.preventDefault();
        });
    });

    function getData(){
        rTable = $('.reportTable').DataTable({
            processing: true,
            serverSide: true,
            lengthMenu: [[50,100,200,-1],[50,100,200,"All"]],
            language: {
                processing: "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate' id='span' style='color:#ff0000'> Loading....</span>"
            },
            ajax: {
                url: '{{ route('hr.customer-feedback','ajax=true') }}',
                type: 'POST',
                data: function (d) {
                    d._token = '{{ csrf_token() }}';
                    d.status = $('select[name=filter_status] option:selected').val();
                    d.period = $('input[name=filter_period]').val();
                }
            },
            // displayLength: -1,
            dom: 'Bflrpt',
            columns: [
                {data : 'count', name : 'count'},
                {data : 'name', name : 'name', searchable: true},
                {data : 'patient_name', name : 'patient_name', searchable: true},
                {data : 'schedule_id', name : 'schedule_id', searchable: true},
                {data : 'schedule_date', name : 'schedule_date', searchable: true},
                {data : 'caring_and_patience', name : 'caring_and_patience', searchable: true},
                {data : 'knowledge', name : 'knowledge', searchable: true},
                {data : 'cleanliness', name : 'cleanliness', searchable: true},
                {data : 'reliabilty_and_punctuality', name : 'reliabilty_and_punctuality', searchable: true},
                {data : 'communication_with_patient', name : 'communication_with_patient', searchable: true},
                {data : 'over_all_rating', name : 'over_all_rating', searchable: true},
                {data : 'comment', name : 'comment', searchable: true},
                {data : 'action', name : 'action', searchable: true},
                {data : 'status', name : 'status', searchable: true},
            ],

            drawCallback: function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last = null;
            },
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: 'Report-Invoice_report_{{ date("d-m-Y_H:i:s") }}'
                },
                {
                    extend: 'print',
                    title: '<h3 style="text-align:center;">Lead Status Report</h3>',
                    customize: function ( win ) {
                        $(win.document.body).css( 'font-size', '7pt' );

                        $(win.document.body).find( 'table' ).addClass( 'compact' ).css( 'font-size', 'inherit' );
                    }
                }
            ],
        });
    }
</script>
@endpush
