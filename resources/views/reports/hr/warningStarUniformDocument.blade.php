@extends('layouts.report-layout')

@section('page_title','Warning, Star, Uniform, Document Report - HR | Reports')

@section('page.styles')
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">

<style>
    div.dataTables_wrapper div.dataTables_filter {
    text-align: center !important;
    }
    .dt-buttons {
        position: static !important;
        float: right;
        margin-top: 15px;
    }
    table.table-bordered thead tr th, table.table-bordered tbody tr td {
        text-transform: capitalize; !important;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        font-size: 12px !important;
    }
    .report-filter {
    width: 95%;
    margin: auto 0 !important;
    background: #f6f6f6;
    border-radius: 4px;
    border: 1px solid #ccc;
    padding: 10px;
    margin-top: 10px !important;
    margin-bottom: 10px !important;
    min-height: 50px;
    margin-left: 2.5% !important;
}
</style>
@endsection

@section('content')
<div class="card">
    <div class="header clearfix">
        <a href="{{ route('reports.index') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
            <i class="material-icons">arrow_back</i>
        </a>
        <h2 class="col-sm-10">
           Warning, Star, Uniform, Document Report
           <small>Employee Warning, Star, Uniform, Document details</small>
       </h2>
   </div>

    <div class="body" style="min-height:200px">
        <div class="table-responsive" style="overflow-x:scroll;">
            <table class="table table-bordered table-hover table-condensed reportTable" style="width:100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Uniform Issued</th>
                        <th>Document</th>
                        <th>Star of month</th>
                        <th>Warning</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($caregivers as $i => $c)
                        <tr>
                            <td class="text-center">{{ ++$i }}</td>
                            <td>{{ $c->full_name }} - {{ $c->employee_id }}</td>
                            <td> @if($c->user->status == "1") Active @else Not @endif</td>
                            <td>{{ $c->professional->uniform_issued }}</td>
                            <td>
                            @isset($c->verifications)
                                @foreach($c->verifications as $verification){{ $verification->doc_name }}@if(!$loop->last),@endif @endforeach
                            @endisset
                            </td>
                            <td>
                            @isset($c->appraisal)
                                @foreach($c->appraisal as $appraisal){{ $appraisal->created_at->format('d-m-Y') }}@if(!$loop->last),@endif @endforeach
                            @endisset
                            </td>
                            <td>{{ $c->professional->warning }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="7" class="text-center">No warning, star, uniform, document found</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection


@section('page.scripts')
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
    <script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>

    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>

    <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>

<script>
    var rTable;
    $(document).ready(function() {
        $('#filter_month').selectpicker('val','{{ isset($month)?$month:0 }}');
        $('#filter_year').selectpicker('val','{{ isset($year)?$year:0 }}');
    });

    if({{ count($caregivers) }}){
        $('.reportTable').DataTable({
            processing: true,
            lengthMenu: [[50,100,200,-1],[50,100,200,"All"]],
            language: {
                processing: "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate' style='color:#ff0000'> Loading....</span>"
            },
            dom: 'Bflrpt',
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: 'Employee_Warning_Star_Uniform_Document_details_{{ date("d-m-Y_H:i:s") }}'
                },
                {
                    extend: 'pdfHtml5',
                    pageSize: 'A3'
                },
                {
                    extend: 'print',
                    title: '<h3 style="text-align:center;">Employee Warning, Star, Uniform, Document details .{{ date("d-m-Y") }}.</h3>',
                    customize: function ( win ) {
                        $(win.document.body).css( 'font-size', '7pt' );
                        $(win.document.body).find( 'table' ).addClass( 'compact' ).css( 'font-size', 'inherit' );
                    }
                }
            ],
        });
    }
</script>
@endsection