@extends('layouts.report-layout')

@section('page_title','Monthly Attendance Report - HR | Reports')

@section('page.styles')
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">

<style>
    div.dataTables_wrapper div.dataTables_filter {
    text-align: center !important;
    }
    .dt-buttons {
        position: static !important;
        float: right;
        margin-top: 15px;
    }
    table.table-bordered thead tr th, table.table-bordered tbody tr td {
        text-transform: capitalize; !important;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        font-size: 12px !important;
    }
    .report-filter {
    width: 95%;
    margin: auto 0 !important;
    background: #f6f6f6;
    border-radius: 4px;
    border: 1px solid #ccc;
    padding: 10px;
    margin-top: 10px !important;
    margin-bottom: 10px !important;
    min-height: 50px;
    margin-left: 2.5% !important;
}
</style>
@endsection

@section('content')
<div class="card">
    <div class="header clearfix">
                <a href="{{ route('reports.index') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-10">
                   Monthly Attendance Report
                   <small>Employee Attendance dates</small>
               </h2>
           </div>
    <div class="report-filter">
        <form id="filter-form" action="{{ route('hr.monthly-attendance-report') }}" method="post">
            {{ csrf_field() }}
            <div class="col-sm-2">
                <label class="label-control">Month</label>
                <select class="form-control input-sm" id="filter_month" name="filter_month" required="required">
                    <option value="" disabled="" selected="">-- Choose Month --</option>
                    <option value="01">Jan</option>
                    <option value="02">Feb</option>
                    <option value="03">Mar</option>
                    <option value="04">Apr</option>
                    <option value="05">May</option>
                    <option value="06">Jun</option>
                    <option value="07">Jul</option>
                    <option value="08">Aug</option>
                    <option value="09">Sep</option>
                    <option value="10">Oct</option>
                    <option value="11">Nov</option>
                    <option value="12">Dec</option>
               </select>
            </div>
            <div class="col-sm-2">
                <label class="label-control">Year</label>
                <select class="form-control input-sm" id="filter_year" name="filter_year" required="required">
                    <option value="" disabled="" selected="">-- Choose Year --</option>
                    <option value="2021">2021</option>
                    <option value="2020">2020</option>
                    <option value="2019">2019</option>
                    <option value="2018">2018</option>
               </select>
            </div>
            <div class="col-sm-2 pull-right" style="margin-top: 1%">
                <input type="submit" class="btn btn-block btn-primary btnFilter" value="Filter">
            </div>
            <div class="clearfix"></div>
        </form>
    </div>

    <div class="body" style="min-height:200px">
        <div class="table-responsive" style="overflow-x:scroll;">
            <table class="table table-bordered table-hover table-condensed reportTable" style="width:100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Present On Schedule</th>
                        <th>Present On Bench</th>
                        <th>Absent</th>
                        <th>On Leave</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($caregivers as $c)
                        <tr>
                            <td class="text-center">{{ $c['srno'] }}</td>
                            <td>{{ $c['id'] }}</td>
                            <td>{{ $c['name'] }}</td>
                            <td>{{ $c['present_on_schedule'] }}</td>
                            <td>{{ $c['present_on_bench'] }}</td>
                            <td>{{ $c['absent'] }}</td>
                            <td>{{ $c['on_leave'] }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="7" class="text-center">No attendance(s) found</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection


@section('page.scripts')
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
    <script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>

    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>

    <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>

<script>
    var rTable;
    $(document).ready(function() {
        $('#filter_month').selectpicker('val','{{ isset($month)?$month:0 }}');
        $('#filter_year').selectpicker('val','{{ isset($year)?$year:0 }}');
    });

    if({{ count($caregivers) }}){
        $('.reportTable').DataTable({
            processing: true,
            lengthMenu: [[50,100,200,-1],[50,100,200,"All"]],
            language: {
                processing: "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate' style='color:#ff0000'> Loading....</span>"
            },
            dom: 'Bflrpt',
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: 'Monthly_attendance_{{ date("d-m-Y_H:i:s") }}'
                },
                {
                    extend: 'pdfHtml5',
                    pageSize: 'A3'
                },
                {
                    extend: 'print',
                    title: '<h3 style="text-align:center;">Monthly attendance Data .{{ date("d-m-Y") }}.</h3>',
                    customize: function ( win ) {
                        $(win.document.body).css( 'font-size', '7pt' );
                        $(win.document.body).find( 'table' ).addClass( 'compact' ).css( 'font-size', 'inherit' );
                    }
                }
            ],
        });
    }
</script>
@endsection