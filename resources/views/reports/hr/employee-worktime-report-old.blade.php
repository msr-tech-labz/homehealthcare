@extends('layouts.report-layout')

@section('page_title','Employee WorkTime Report - HR | Reports')

@section('report-title','Employee WorkTime Report')
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
@section('report-summary','Employee Employee WorkTime data')
@section('report-content')
<style type="text/css">
    .card{
        margin-bottom: 0 !important;
    }
    .dataTables_wrapper{
        padding: 30px;
    }
    .dataTables_filter {
        float: right !important;
    }
    td{
        color: black !important;
    }
</style>
<div class="report-filter" style="margin-bottom: 20px !important">
    <form class="col-lg-12" action="{{ route('hr.employee-worktime-old') }}" method="post">
        {{ csrf_field() }}
        <div class="col-lg-12">
            <div class="col-lg-3" style="text-align: center;">
                <select class="form-control" name="filter_month" required="required">
                    <option value="" disabled="" selected="">-- Choose Month --</option>
                    <option value="01">[01] Jan</option>
                    <option value="02">[02] Feb</option>
                    <option value="03">[03] Mar</option>
                    <option value="04">[04] Apr</option>
                    <option value="05">[05] May</option>
                    <option value="06">[06] Jun</option>
                    <option value="07">[07] Jul</option>
                    <option value="08">[08] Aug</option>
                    <option value="09">[09] Sep</option>
                    <option value="10">[10] Oct</option>
                    <option value="11">[11] Nov</option>
                    <option value="12">[12] Dec</option>
                </select>
            </div>
            <div class="col-lg-3" style="text-align: center;">
                <select class="form-control" name="filter_year" required="required">
                    <option value="" disabled="" selected="">-- Choose Year --</option>
                    <option value="2019">2019</option>
                    <option value="2018">2018</option>
                </select>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <div class="form-line">
                        <input type="input" name="email" class="form-control" placeholder="Email Receipient">
                    </div>
                </div>
            </div>
            <div class="col-lg-3" style="text-align: center;">
                <input type="submit" class="btn btn-success" value="SEND MAIL">
            </div>
        </div>
    </form>
</div>
<div class="row clearfix"></div>
<h2 class="text-center">GENERATION LOG</h2>
    <table class="table table-bordered table-hover table-condensed reportTable" style="zoom:85%;">
        <thead>
            <tr>
                <th>Sl</th>
                <th>Date of Generation</th>
                <th>Receipient</th>
                <th>Log Month-Year</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            @if(isset($worktimeJobs) && count($worktimeJobs))
            @foreach ($worktimeJobs as $index => $w)
            <tr style="@if($w->status == 'Pending') background-color:#1dc8cd @elseif($w->status == 'Failed') background-color:#ecd2d2 @else background-color:#27bd38 @endif">
                <td class="text-center">{{ $index+1 }}</td>
                <td class="text-center">{{ $w->created_at->format('d-m-Y h:i A') }}</td>
                <td class="text-center">{{ unserialize(json_decode($w->payload)->data->command)->request['email'] }}</td>
                <td class="text-center">{{ unserialize(json_decode($w->payload)->data->command)->request['filter_month'].' - '.unserialize(json_decode($w->payload)->data->command)->request['filter_year'] }}</td>
                <td class="text-center">{{ $w->status }}</td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
@endsection


@push('report.scripts')
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<script>
    $('.reportTable').DataTable();
</script>
@endpush