@extends('layouts.report-layout')

@section('page_title','Employee WorkTime Report II | Reports')

@section('page.styles')
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">

<style>
    div.dataTables_wrapper div.dataTables_filter {
    text-align: center !important;
    }
    .dt-buttons {
        position: static !important;
        float: right;
        margin-top: 15px;
    }
    table.table-bordered thead tr th, table.table-bordered tbody tr td {
        text-transform: capitalize; !important;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        font-size: 12px !important;
    }
    .report-filter {
    width: 95%;
    margin: auto 0 !important;
    background: #f6f6f6;
    border-radius: 4px;
    border: 1px solid #ccc;
    padding: 10px;
    margin-top: 10px !important;
    margin-bottom: 10px !important;
    min-height: 50px;
    margin-left: 2.5% !important;
}
</style>
@endsection

@section('content')
<div class="card">
    <div class="header clearfix">
                <a href="{{ route('reports.index') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-6">
                   Employee WorkTime Report II
                   <small>Employee Employee WorkTime data</small>
               </h2>
               <p class="col-sm-5" id="demo"></p>
           </div>
    <div class="report-filter">
        <form id="formReport" class="form-horizontal" action="{{ route('hr.employee-worktime2') }}" method="post">
            {{ csrf_field() }}
            <div class="col-sm-offset-3 col-sm-2">
                <select class="form-control input-sm filter_month" name="filter_month">
                    <option value="" disabled="" selected="">-- Choose Month --</option>
                    <option value="01">Jan</option>
                    <option value="02">Feb</option>
                    <option value="03">Mar</option>
                    <option value="04">Apr</option>
                    <option value="05">May</option>
                    <option value="06">Jun</option>
                    <option value="07">Jul</option>
                    <option value="08">Aug</option>
                    <option value="09">Sep</option>
                    <option value="10">Oct</option>
                    <option value="11">Nov</option>
                    <option value="12">Dec</option>
               </select>
            </div>
            <div class="col-sm-2">
                <select class="form-control input-sm filter_year" name="filter_year">
                    <option value="" disabled="" selected="">-- Choose Year --</option>
                    <option value="2021">2021</option>
                    <option value="2020">2020</option>
                    <option value="2019">2019</option>
                    <option value="2018">2018</option>
               </select>
            </div>
            <div class="col-sm-2">
                <button type="button" class="btn btn-block btn-primary btnFilter">Filter</button>
            </div>
            <div class="clearfix"></div>
        </form>
    </div>
    @php
        $month = $filter_month;
        $year =  $filter_year;
    @endphp
    <div class="body" style="min-height:200px">
        <div class="table-responsive" style="overflow-x:scroll;">
            <table class="table table-bordered table-hover table-condensed reportTable" style="width:100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Designation</th>
                        <th>Employee Id</th>
                        <th>1</th>
                        <th>2</th>
                        <th>3</th>
                        <th>4</th>
                        <th>5</th>
                        <th>6</th>
                        <th>7</th>
                        <th>8</th>
                        <th>9</th>
                        <th>10</th>
                        <th>11</th>
                        <th>12</th>
                        <th>13</th>
                        <th>14</th>
                        <th>15</th>
                        <th>16</th>
                        <th>17</th>
                        <th>18</th>
                        <th>19</th>
                        <th>20</th>
                        <th>21</th>
                        <th>22</th>
                        <th>23</th>
                        <th>24</th>
                        <th>25</th>
                        <th>26</th>
                        <th>27</th>
                        <th>28</th>
                        <th>29</th>
                        <th>30</th>
                        <th>31</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($employeeWorkTime as $key => $r)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td>{{ isset($r['name'])?$r['name']:'-' }}</td>
                        <td>{{ isset($r['designation'])?$r['designation']:'-' }}</td>
                        <td>{{ isset($r['empolyee_id'])?$r['empolyee_id']:'-' }}</td>
                        <td>{{ isset($r['day_01'])?$r['day_01']:'-' }}</td>
                        <td>{{ isset($r['day_02'])?$r['day_02']:'-' }}</td>
                        <td>{{ isset($r['day_03'])?$r['day_03']:'-' }}</td>
                        <td>{{ isset($r['day_04'])?$r['day_04']:'-' }}</td>
                        <td>{{ isset($r['day_05'])?$r['day_05']:'-' }}</td>
                        <td>{{ isset($r['day_06'])?$r['day_06']:'-' }}</td>
                        <td>{{ isset($r['day_07'])?$r['day_07']:'-' }}</td>
                        <td>{{ isset($r['day_08'])?$r['day_08']:'-' }}</td>
                        <td>{{ isset($r['day_09'])?$r['day_09']:'-' }}</td>
                        <td>{{ isset($r['day_10'])?$r['day_10']:'-' }}</td>
                        <td>{{ isset($r['day_11'])?$r['day_11']:'-' }}</td>
                        <td>{{ isset($r['day_12'])?$r['day_12']:'-' }}</td>
                        <td>{{ isset($r['day_13'])?$r['day_13']:'-' }}</td>
                        <td>{{ isset($r['day_14'])?$r['day_14']:'-' }}</td>
                        <td>{{ isset($r['day_15'])?$r['day_15']:'-' }}</td>
                        <td>{{ isset($r['day_16'])?$r['day_16']:'-' }}</td>
                        <td>{{ isset($r['day_17'])?$r['day_17']:'-' }}</td>
                        <td>{{ isset($r['day_18'])?$r['day_18']:'-' }}</td>
                        <td>{{ isset($r['day_19'])?$r['day_19']:'-' }}</td>
                        <td>{{ isset($r['day_20'])?$r['day_20']:'-' }}</td>
                        <td>{{ isset($r['day_21'])?$r['day_21']:'-' }}</td>
                        <td>{{ isset($r['day_22'])?$r['day_22']:'-' }}</td>
                        <td>{{ isset($r['day_23'])?$r['day_23']:'-' }}</td>
                        <td>{{ isset($r['day_24'])?$r['day_24']:'-' }}</td>
                        <td>{{ isset($r['day_25'])?$r['day_25']:'-' }}</td>
                        <td>{{ isset($r['day_26'])?$r['day_26']:'-' }}</td>
                        <td>{{ isset($r['day_27'])?$r['day_27']:'-' }}</td>
                        <td>{{ isset($r['day_28'])?$r['day_28']:'-' }}</td>
                        <td>{{ isset($r['day_29'])?$r['day_29']:'-' }}</td>
                        <td>{{ isset($r['day_30'])?$r['day_30']:'-' }}</td>
                        <td>{{ isset($r['day_31'])?$r['day_31']:'-' }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection


@section('page.scripts')
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
    <script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>

    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>

    <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>

<script>
    var rTable;
    $(document).ready(function() {
        @isset($employeeWorkTime[0]['name'])
            if ( ! $.fn.DataTable.isDataTable('.reportTable') ) {
                getData();
            }else{
                rTable.draw();
            }
        @endisset

        $('.filter_month').val('{{ $filter_month }}').change();
        $('.filter_year').val('{{ $filter_year }}').change();
    });

    function getData(){
        rTable = $('.reportTable').DataTable({
            lengthMenu: [[50,100,200,-1],[50,100,200,"All"]],
            language: {
                processing: "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate' style='color:#ff0000'> Loading....</span>"
            },
            // displayLength: -1,
            dom: 'Bflrpt',
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: 'EmployeeWorkTime2_{{ date("d-m-Y_H:i:s") }}'
                }
            ],
        });
    }

    $('.content').on('click','.btnFilter',function(){
        $('.btnFilter').attr('disabled', true);
        if($('select[name=filter_month] option:selected').val() == ''){
            alert('Month is required');
            $('.btnFilter').attr('disabled', false);
            return false;
        }
        if($('select[name=filter_year] option:selected').val() == ''){
            alert('Year is required');
            $('.btnFilter').attr('disabled', false);
            return false;
        }
        $('.btnFilter').html('Processing..');
        $('#formReport').submit();
        countDown();
    });

    function countDown(){
        var counter = 400;
        var interval = setInterval(function() {
            counter--;
            document.getElementById("demo").innerHTML = "<small class='col-red pull-right'>Please wait for approx: "+counter + " (coundown)</small>";
            if (counter == 0) {
                clearInterval(interval);
            }
        }, 1000);
    }
</script>
@endsection