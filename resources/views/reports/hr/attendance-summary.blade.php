@extends('layouts.report-layout')

@section('page_title','Attendance Summary - HR | Reports')

@section('report-title','Attendance Summary')

@section('report-summary','Summary of Attendance')

@section('report-filter')
    <div class="report-filter">
        <form id="filter-form" action="{{ route('hr.attendance-summary') }}" class="form-horizontal" method="post">
            {{ csrf_field() }}
            <div class="col-sm-2">
                <label class="label-control">Period</label>
                <input type="text" id="period" class="form-control input-sm daterange" placeholder="Select Period" value="{{ !empty($filters['filter_period'])?$filters['filter_period']:'' }}" />
                <input type="hidden" name="filter_period" value="{{ !empty($filters['filter_period'])?$filters['filter_period']:'' }}"/>
            </div>
            <div class="col-sm-2 pull-right" style="margin-top: 1%">
                <input type="submit" class="btn btn-block btn-primary" value="Filter">
            </div>
            <div class="clearfix"></div>
        </form>
    </div>
@endsection

@section('report-content')
<div class="body table-responsive" style="min-height:200px;margin:auto 0;">
    <table class="table table-bordered table-hover table-condensed reportTable" style="width:60%;margin-left:20%">
        <thead>
            <tr>
                <th width="25%">Particulars</th>
                <th class="text-center">Total</th>
                <th class="text-center" colspan="2">PCA</th>
                <th class="text-center" colspan="2">Sr.PCA</th>
                <th class="text-center" colspan="2">RN</th>
            </tr>
            <tr>
                <td>Total Field staff strength</td>
                <td></td>
                <td class="text-center">Male</td>
                <td class="text-center">Female</td>
                <td class="text-center">Male</td>
                <td class="text-center">Female</td>
                <td class="text-center">Male</td>
                <td class="text-center">Female</td>
            </tr>
            <tr>
                <th colspan="8">Break-up</th>
            </tr>
            <tr>
                <td>Effective Utilisation</td>
                <td class="text-center"></td>
                <td class="text-center">{{ $data['effective_utilization']['pca_male'] }}</td>
                <td class="text-center">{{ $data['effective_utilization']['pca_female'] }}</td>
                <td class="text-center">{{ $data['effective_utilization']['srpca_male'] }}</td>
                <td class="text-center">{{ $data['effective_utilization']['srpca_female'] }}</td>
                <td class="text-center">{{ $data['effective_utilization']['rn_male'] }}</td>
                <td class="text-center">{{ $data['effective_utilization']['rn_female'] }}</td>
            </tr>
            <tr>
                <td>Utilisation NC</td>
                <td class="text-center"></td>
                <td class="text-center">{{ $data['utilization_nc']['pca_male'] }}</td>
                <td class="text-center">{{ $data['utilization_nc']['pca_female'] }}</td>
                <td class="text-center">{{ $data['utilization_nc']['srpca_male'] }}</td>
                <td class="text-center">{{ $data['utilization_nc']['srpca_female'] }}</td>
                <td class="text-center">{{ $data['utilization_nc']['rn_male'] }}</td>
                <td class="text-center">{{ $data['utilization_nc']['rn_female'] }}</td>
            </tr>
            <tr>
                <td>Bench</td>
                <td class="text-center"></td>
                <td class="text-center">{{ $data['bench']['pca_male'] }}</td>
                <td class="text-center">{{ $data['bench']['pca_female'] }}</td>
                <td class="text-center">{{ $data['bench']['srpca_male'] }}</td>
                <td class="text-center">{{ $data['bench']['srpca_female'] }}</td>
                <td class="text-center">{{ $data['bench']['rn_male'] }}</td>
                <td class="text-center">{{ $data['bench']['rn_female'] }}</td>
            </tr>
            <tr>
                <td>Off &amp; Comp-Off</td>
                <td class="text-center"></td>
                <td class="text-center">{{ $data['off_compoff']['pca_male'] }}</td>
                <td class="text-center">{{ $data['off_compoff']['pca_female'] }}</td>
                <td class="text-center">{{ $data['off_compoff']['srpca_male'] }}</td>
                <td class="text-center">{{ $data['off_compoff']['srpca_female'] }}</td>
                <td class="text-center">{{ $data['off_compoff']['rn_male'] }}</td>
                <td class="text-center">{{ $data['off_compoff']['rn_female'] }}</td>
            </tr>
            <tr>
                <td>Leave</td>
                <td class="text-center"></td>
                <td class="text-center">{{ $data['leave']['pca_male'] }}</td>
                <td class="text-center">{{ $data['leave']['pca_female'] }}</td>
                <td class="text-center">{{ $data['leave']['srpca_male'] }}</td>
                <td class="text-center">{{ $data['leave']['srpca_female'] }}</td>
                <td class="text-center">{{ $data['leave']['rn_male'] }}</td>
                <td class="text-center">{{ $data['leave']['rn_female'] }}</td>
            </tr>
            <tr>
                <td>Absence</td>
                <td class="text-center"></td>
                <td class="text-center">{{ $data['absence']['pca_male'] }}</td>
                <td class="text-center">{{ $data['absence']['pca_female'] }}</td>
                <td class="text-center">{{ $data['absence']['srpca_male'] }}</td>
                <td class="text-center">{{ $data['absence']['srpca_female'] }}</td>
                <td class="text-center">{{ $data['absence']['rn_male'] }}</td>
                <td class="text-center">{{ $data['absence']['rn_female'] }}</td>
            </tr>
            <tr>
                <td>Training</td>
                <td class="text-center"></td>
                <td class="text-center">{{ $data['training']['pca_male'] }}</td>
                <td class="text-center">{{ $data['training']['pca_female'] }}</td>
                <td class="text-center">{{ $data['training']['srpca_male'] }}</td>
                <td class="text-center">{{ $data['training']['srpca_female'] }}</td>
                <td class="text-center">{{ $data['training']['rn_male'] }}</td>
                <td class="text-center">{{ $data['training']['rn_female'] }}</td>
            </tr>
            <tr>
                <td>Total</td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
            </tr>
        </thead>
        <tbody>
        @if(isset($summary) && count($summary))
            @foreach ($summary as $s)
                <tr style="@if($s['count'] == 0 && $s['mtd'] == 0 && $s['ytd'] == 0){{ 'background: #ecd2d2' }}@endif">
                    <td>{{ $s['service_name'] }}</td>
                    <td class="text-center">{{ $s['count'] }}</td>
                    <td class="text-center">{{ $s['mtd'] }}</td>
                    <td class="text-center">{{ $s['ytd'] }}</td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
@endsection

@push('report.scripts')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
    <script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
    <script type="text/javascript" src="{{ ViewHelper::ThemeJs('daterangepicker.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<script>
    var rTable;
    $(document).ready(function() {
        $.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});

        $('.daterange').daterangepicker({
            // autoApply: true,
            // autoUpdateInput: true,
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
              format: 'DD-MM-YYYY',
              applyLabel: 'Apply',
              cancelLabel: 'Clear'
            }
         },
         function(start, end, label) {
             $('input[name=filter_period]').val(start.format('YYYY-MM-DD'));
            // $('#period').val(start.format('DD-MM-YYYY'))
         }
        );

        // $('#filter-form').on('submit', function(e) {
        //     if ( ! $.fn.DataTable.isDataTable('.reportTable') ) {
        //         getData();
        //     }else{
        //         rTable.draw();
        //     }
        //     e.preventDefault();
        // });
    });

    function getData(){
        rTable = $('.reportTable').DataTable({
            processing: true,
            serverSide: true,
            lengthMenu: [[50,100,200,-1],[50,100,200,"All"]],
            language: {
                processing: "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate' style='color:#ff0000'> Loading....</span>"
            },
            order: [[0,'asc']],
            ajax: {
                url: '{{ route('financial.revenue-schedules','ajax=true') }}',
                type: 'POST',
                data: function (d) {
                    d.period = $('input[name=filter_period]').val();
                }
            },
            // displayLength: -1,
            dom: 'Bflrpt',
            columns: [
                {data : 'date', name : 'date', searchable: true},
                {data : 'episode_id', name : 'episode_id', searchable: true},
                {data : 'patient_name', name : 'patient_name', searchable: true},
                {data : 'service_requested', name : 'service_requested', searchable: true},
                {data : 'employee_id', name : 'employee_id', searchable: true},
                {data : 'employee_name', name : 'employee_name', searchable: true},
                {data : 'count', name : 'count'}
            ],
            drawCallback: function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last = null;

                api.column(2, {page:'current'} ).data().each( function ( group, i ) {
                    if(group != null){
                        if ( last !== group ) {
                            $(rows).eq( i ).before(
								'<tr class="group"><td colspan="10"><b>'+group.toUpperCase()+'</b></td></tr>'
                            );
                            last = group;
                        }
                    }
                });
            },
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: 'Revenue-with-Schedules_{{ date("d-m-Y_H:i:s") }}'
                },
                {
                    extend: 'pdfHtml5',
                    pageSize: 'A3'
                },
                {
                    extend: 'print',
                    title: '<h3 style="text-align:center;">Outstanding Payment Report</h3>',
                    customize: function ( win ) {
                        $(win.document.body).css( 'font-size', '7pt' );

                        $(win.document.body).find( 'table' ).addClass( 'compact' ).css( 'font-size', 'inherit' );
                    }
                }
            ],
        });
    }
</script>
@endpush
