@extends('layouts.main-layout')

@section('page_title','Attendance - Report')

@section('active_monthly_report','active')

@section('page.styles')
<style>
    .card{
        overflow-x: scroll;
    }
    .dt-buttons{
        position: static !important;
        float: right;
    }
    .paginate_button {
        cursor: pointer;
        margin:2px;
    }
    div.dataTables_wrapper div.dataTables_filter {
        text-align: center !important;
    }
    table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td {
      text-transform: uppercase; !important;
      overflow: hidden;
      text-overflow: ellipsis;
      white-space: nowrap;
    }
    .text-center{
        padding: 10px !important;
    }
    .red-color{
        background-color: #ecd2d2 !important;
    }
</style>
<!-- JQuery DataTable Css -->
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection

@section('plugin.styles')
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<style>
</style>
@endsection

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('hr.index') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-8">
                   Attendance Stats
                   <small>Attendance Stats of all Employees</small>
               </h2>
           </div>
           <div class="body">
               <div class="row clearfix">
                   <form action="{{ route('attendance.reports') }}" method="post">
                       {{ csrf_field() }}
                       <table class="table table-bordered">
                           <tr>
                               <th>
                                   <select class="form-control" name="filter_user">
                                       <option value="">-- All Employees --</option>
                                       <optgroup label="Filter by Department">
                                   @if(isset($departments))
                                      @foreach($departments as $d)
                                           <option style="text-transform: uppercase;" value="dept_{{ $d->id }}" {{ $filters['filter_user']=='dept_'.$d->id?'selected':'' }}>{{ $d->department_name }}</option>
                                      @endforeach
                                   @endif
                                       </optgroup>
                                       <optgroup label="Filter by Employee">
                                    @if(isset($employees))
                                       @foreach($employees as $e)
                                            <option style="text-transform: uppercase;" value="emp_{{ $e->id }}" {{ $filters['filter_user']=='emp_'.$e->id?'selected':'' }}>{{ $e->full_name }}</option>
                                       @endforeach
                                    @endif
                                        </optgroup>
                                   </select>
                               </th>
                               <th>
                                   <select class="form-control" name="filter_month">
                                       <option value="" disabled="" selected="">-- Choose Month --</option>
                                        <option value="01" {{ $filters['filter_month']=="01"?'selected':'' }}>Jan</option>
                                        <option value="02" {{ $filters['filter_month']=="02"?'selected':'' }}>Feb</option>
                                        <option value="03" {{ $filters['filter_month']=="03"?'selected':'' }}>Mar</option>
                                        <option value="04" {{ $filters['filter_month']=="04"?'selected':'' }}>Apr</option>
                                        <option value="05" {{ $filters['filter_month']=="05"?'selected':'' }}>May</option>
                                        <option value="06" {{ $filters['filter_month']=="06"?'selected':'' }}>Jun</option>
                                        <option value="07" {{ $filters['filter_month']=="07"?'selected':'' }}>Jul</option>
                                        <option value="08" {{ $filters['filter_month']=="08"?'selected':'' }}>Aug</option>
                                        <option value="09" {{ $filters['filter_month']=="09"?'selected':'' }}>Sep</option>
                                        <option value="10" {{ $filters['filter_month']=="10"?'selected':'' }}>Oct</option>
                                        <option value="11" {{ $filters['filter_month']=="11"?'selected':'' }}>Nov</option>
                                        <option value="12" {{ $filters['filter_month']=="12"?'selected':'' }}>Dec</option>
                                   </select>
                               </th>
                               <th>
                                   <select class="form-control" name="filter_year">
                                       <option value="" disabled="" selected="">-- Choose Year --</option>
                                        <option value="2017">2017</option>
                                        <option value="2016">2016</option>
                                   </select>
                               </th>
                               <th><input type="submit" class="btn btn-success"></th>
                           </tr>
                       </table>
                   </form>
               </div>
            <table id="resourceReport" style="font-size: 12px !important;margin-right: 20px !important" class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Employee</th>
                @if(isset($dates))
                    @foreach ($dates as $ua)
                        {? $dateObj = explode("-", $ua); ?}
                        <th class="text-center {{ $dateObj[3]=='Sun'?'red-color':'' }}">
                            {{ $dateObj[2] }}<br>
                            <small>{{ $dateObj[3] }}</small>
                        </th>
                    @endforeach
                @endif
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                {? $cnt = 0; ?}
                @foreach($caregivers as $c)
                    <tr>
                        <td>{{ $cnt + 1 }}</td>
                        <td>{{ $c->full_name }}</td>
                        @if(isset($dates))
                            @foreach ($dates as $ua)
                                {? $dateObj = explode("-",$ua); ?}
                                {? $date = $dateObj[0]."-".$dateObj[1]."-".$dateObj[2]; ?}
                                {? $att = Helper::getMonthlyAttendanceStatus($c->id,$date); ?}
                                <th class="text-center {{ $dateObj[3]=='Sun'?'red-color':'' }}">
                                    <small>{{ $att }}</small>
                                </th>
                            @endforeach
                        @endif
                        <th></th>
                    </tr>
                {? $cnt++; ?}
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
@endsection


@section('page.scripts')
<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
<!-- Moment Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<script>
$(function(){
    $('.min').bootstrapMaterialDatePicker({
        format: 'DD-MM-YYYY',
        clearButton: true,
        time: false,
        maxDate : new Date(),
    });

    $('.max').bootstrapMaterialDatePicker({
        format: 'DD-MM-YYYY',
        clearButton: true,
        time: false,
        maxDate : new Date(),
    });
});
</script>
@endsection
