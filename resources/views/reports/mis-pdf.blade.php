<link href="{{ ViewHelper::ThemePlugin('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('css/print.css') }}" media="print" />
<link href="{{ ViewHelper::ThemePlugin('morrisjs/morris.css') }}" rel="stylesheet" />
<style>
     .chart-table tr td.c, .chart-table tr th.c{
         text-align: center !important;
     }
     .block-header h2{
         color: #fff !important;
     }
     img {
         display: -dompdf-image !important;
     }
     html, body {
         position: fixed !important;
         top: 0 !important;
         bottom: 0 !important;
         left: 0 !important;
         right: 0 !important;
         margin: 0 !important;
         padding: 0 !important;
         height: 100% !important;
         overflow-y: overlay !important;
     }
     .card{
         background: #fff;
         min-height: 50px;
         box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
         position: relative;
         margin-bottom: 30px;
     }
     .card .header {
         color: #555;
         padding: 20px;
         position: relative;
         border-bottom: 1px solid rgba(204, 204, 204, 0.35);
     }
     .card .header h2 {
         margin: 0;
         font-size: 18px;
         font-weight: normal;
         color: #111;
     }
     .card .body {
         font-size: 14px;
         color: #555;
         padding: 10px;
     }
     /*table {
         display: inline;
     }
     .tr {
         display: table-row;
     }*/
     .theme{
         /*background: -moz-radial-gradient(at bottom,#427a93,#05264f,#041124);
         background: -o-radial-gradient(at bottom,#427a93,#05264f,#041124);
         background: radial-gradient(at bottom,#427a93,#05264f,#041124);
         background-color:  #072951;*/
     }
</style>

<body class="theme-indigod theme">
    <nav class="navbar hidden-print" style="width:100%;background: #3F51B5 !important;display:inline-block;">
        <div class="container-fluid" style="width:100%">
            <div style="width:48%;padding: 14px;display:inline-block">
                <h2 style="font-size: 18px !important;font-weight:normal;color: #fff !important;margin:0 !important;padding-bottom:5px;">MIS Report</h2>
                <small style="color:#54bdd5 !important;"><i>As on {{ date('d-m-Y') }}</i></small>
            </div>
            <div style="width:40%;display:inline-block;padding: 14px;text-align:right;">
                <h2 style="font-size: 18px !important;font-weight:normal;color: #fff !important;margin:0 !important;padding-right: 10px;">Smart Health Connect</h2>
                <small  style="color:#54bdd5 !important;text-align:right;padding-right: 10px;"><i>v2.0</i></small>
            </div>
        </div>
    </nav>
    
    <section class="content" style="margin: 30px 15px 0;page-break-after:always;">
        <div class="container-fluid">
            <div class="row clearfix">
                <!-- Lead Stats Chart -->
                <div style="width:100%;display:block;page-break-after:always;">
                    <div class="card">
                        <div class="header">
                            <h2>Leads</h2>
                        </div>
                        <div class="body">
                            <div class="col-lg-3 col-md-3">
                                <div id="lead_stats_table" class="lead-table" style="margin-top:20px;height:300px;vertical-align: middle;">
                                    <table class="table table-bordered chart-table">
                                        <tr style="background-color: #f6f6f6">
                                            <th class="text-center">Total Leads</th>
                                            <th class="text-center">215</th>
                                        </tr>
                                        <tr>
                                            <th>Pending Leads</th>
                                            <td class="c"></td>
                                        </tr>
                                        <tr>
                                            <th>Converted Leads</th>
                                            <td class="c"></td>
                                        </tr>
                                        <tr>
                                            <th>Case Started</th>
                                            <td class="c"></td>
                                        </tr>
                                        <tr>
                                            <th>Dropped / Case Loss</th>
                                            <td class="c"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div id="lead_stats_chart" class="lead-graph" style="height:250px !important"></div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <canvas id="lead_trend_chart" class="services-graph" data-img="#lead_trend_img" style="display:none"></canvas>
                                <img id="lead_trend_img" alt="Lead Trend" style="border: 1px solid #333"/>
                            </div>                            
                            <div class="clearfix"></div><br>
                        </div>
                    </div>
                </div>
                
                <!-- HR Stats Chart -->
                <div style="width:100%;display:block;page-break-after:always;">
                    <div class="card">
                        <div class="header">
                            <h2>HR</h2>
                        </div>
                        <div class="body" style="height:300px !important">
                            <div class="col-lg-4 col-md-4">
                                <div id="hr_stats_table" class="hr-table">
                                    <table class="table table-bordered chart-table">
                                        <tr style="background-color: #f6f6f6">
                                            <th class="text-center">Total Staff</th>
                                            <th class="text-center">149</th>
                                            <th></th>
                                        </tr>
                                        <tr>
                                            <th>Available</th>
                                            <td class="c">40</td>
                                            <td class="c">11.63 %</td>
                                        </tr>
                                        <tr>
                                            <th>On Duty</th>
                                            <td class="c">25</td>
                                            <td class="c">18.60 %</td>
                                        </tr>
                                        <tr>
                                            <th>Training</th>
                                            <td class="c">34</td>
                                            <td class="c">4.65 %</td>
                                        </tr>
                                        <tr>
                                            <th>Leave</th>
                                            <td class="c">23</td>
                                            <td class="c">11.63 %</td>
                                        </tr>
                                        <tr>
                                            <th>Absent</th>
                                            <td class="c">34</td>
                                            <td class="c">4.65 %</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <canvas id="hr_stats_chart" class="hr-graph" data-img="#hr_stats_img" style="display:none"></canvas>
                                <img id="hr_stats_img"  alt="HR Stats" style="border: 1px solid #333"/>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Services Stats Chart -->
                <div style="width:100%;display:block;page-break-after:always;">
                    <div class="card">
                        <div class="header">
                            <h2>Services</h2>
                        </div>
                        <div class="body" style="overflow:scroll !important">
                            <div class="col-lg-5 col-md-5" style="border-right: 1px solid #ccc">
                                <div id="services_table" class="services-table">
                                    <table class="table table-bordered chart-table">
                                        <tr style="background-color: #f6f6f6">
                                            <th>Service Name</th>
                                            <th>Today</th>
                                            <th>MTD</th>
                                            <th>YTD</th>
                                        </tr>
                                @if(count($services['table']))
                                    @foreach ($services['table'] as $service)
                                        <tr>
                                            <td>{{ $service['service'] }}</td>
                                            <td>{{ $service['today'] }}</td>
                                            <td>{{ $service['mtd'] }}</td>
                                            <td>{{ $service['ytd'] }}</td>
                                        </tr>    
                                    @endforeach
                                @endif
                                    </table>
                                </div>
                            </div>
                            <div class="col-lg-7 col-md-7">
                                <canvas id="services_chart" class="services-graph" data-img="#services_img" style="display:none"></canvas>
                                <img id="services_img" />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                
                <!-- Revenue Stats Chart -->
                <div style="width:100%;display:block;page-break-after:always;">
                    <div class="card">
                        <div class="header">
                            <h2>Revenue</h2>
                        </div>
                        <div class="body" style="overflow:scroll !important">
                            <div id="revenue_table" class="revenue-table" style="width: 29%;height:300px;vertical-align: middle;display:inline-block">
                                <table class="table table-bordered chart-table">
                                    <tr>
                                        <th>Gross</th>
                                        <td>&#8377; {{ $revenue['gross'] }}</td>
                                    </tr>
                                    <tr>
                                        <th>Net</th><td>&#8377; {{ $revenue['net'] }}</td>
                                    </tr>
                                    <tr>
                                        <th>Discount</th><td>&#8377; {{ $revenue['discount'] }}</td>
                                    </tr>
                                    <tr>
                                        <th>Realisation</th><td>&#8377; {{ $revenue['realisation'] }}</td>
                                    </tr>
                                    <tr>
                                        <th>Due</th><td>&#8377; {{ $revenue['due'] }}</td>
                                    </tr>
                                </table>
                            </div>
                            <div style="width: 60% !important;display:inline-block">
                                <canvas id="revenue_chart" class="revenue-graph" data-img="#revenue-img" style="display:none"></canvas>
                                <img id="revenue-img"  alt="Revenue Stats" style="border: 1px solid #333"/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="{{ ViewHelper::ThemePlugin('jquery/jquery.min.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('bootstrap/js/bootstrap.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('raphael/raphael.min.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('morrisjs/morris.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('chartjs/Chart.bundle.js') }}"></script>
    <script>
        var originalSize = "";
        $(function(){            
            leadStatsChart();
            hrStatsChart();
            servicesStatsChart();
            revenueStatsChart();
        });
        
        function showLoader(block){
            html = `<div class="loading-img" style="display: block;text-align:center">
                <img src="{{ asset('img/loading.gif') }}" style="width:9%;margin: auto 0 !important;"/>
            </div>`;
            $('#'+block).parent().parent().prepend(html);
        }
        
        function formatMoney(data){
            return Number(parseFloat(data)).toLocaleString('en-IN', { style: 'currency', currency: 'INR'});
        }
        
        function leadStatsChart(){
            showLoader('lead_stats_chart');
            $('#lead_stats_table .table').hide();
            
            var data = [];
            data = $.parseJSON('{!! json_encode($lead['today']) !!}');
            Morris.Donut({
                element: 'lead_stats_chart',
                resize: true,
                data: [
                {
                    label: 'Pending Leads',
                    value: data.total
                },{
                    label: 'Converted Leads',
                    value: data.converted
                },{
                    label: 'Case Started',
                    value: data.started
                },{
                    label: 'Dropped / Lost',
                    value: data.dropped
                }],
                colors: ['rgb(255, 152, 0)', 'rgb(0, 150, 136)', 'rgb(0, 188, 212)', 'rgba(239, 52, 52, 0.85)'],
                formatter: function (y) {
                    return y;
                },
            });
            
            new Chart(document.getElementById("lead_trend_chart").getContext("2d"), {
                type: 'line',
                data: {
                    labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                    datasets: {!! json_encode($lead['trend']) !!}
                },
                options: {
                    responsive: true,
                    legend: {
                        display: true,
                        position: 'top'
                    },
                    title:{
                        display:true,
                        text:'Leads Trend'
                    },
                    tooltips: {
                        enabled: true,
                        mode: 'label',
                        position: 'nearest'
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Month'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Value'
                            },
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                },
                plugins: ['zeroCompensation'],
            });
            
            // Table data
            $('#lead_stats_table .table tr:eq(0) th:eq(1)').html(parseInt(data.total));
            $('#lead_stats_table .table tr:eq(1) td:eq(0)').html(parseInt(data.pending));
            $('#lead_stats_table .table tr:eq(2) td:eq(0)').html(parseInt(data.started));
            $('#lead_stats_table .table tr:eq(3) td:eq(0)').html(parseInt(data.started));
            $('#lead_stats_table .table tr:eq(4) td:eq(0)').html(parseInt(data.dropped));
            $('#lead_stats_table .table').show();
            
            $('#lead_stats_chart').parent().parent().find('.loading-img').remove();                        
        }
        
        function hrStatsChart(){
            showLoader('hr_stats_chart');
            $('#hr_stats_table .table').hide();
            
            var data = [];
            data = $.parseJSON('{!! json_encode($hr) !!}');
            d = data;
                        
            new Chart(document.getElementById("hr_stats_chart").getContext("2d"), {
                type: 'doughnut',
                data: {
                    datasets: [{ 
                        data: [d.available, d.onduty, d.training, d.leave, d.absent],
                        backgroundColor: [
                            'rgb(255, 152, 0)', 'rgb(0, 150, 136)', 'rgb(0, 188, 212)', 'rgba(239, 52, 52, 0.85)', 'rgba(239, 52, 52, 0.85)'
                        ],
                    }],
                    labels: ['Available','On Duty','Training','Leave','Absent'],
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            });
            
            // Table data
            $('#hr_stats_table .table tr:eq(0) th:eq(1)').html(data.total);
            $('#hr_stats_table .table tr:eq(1) td:eq(0)').html(parseInt(data.available));
            $('#hr_stats_table .table tr:eq(1) td:eq(1)').html(parseFloat(data.available/data.total*100).toFixed(2)+' %');
            $('#hr_stats_table .table tr:eq(2) td:eq(0)').html(parseInt(data.onduty));
            $('#hr_stats_table .table tr:eq(2) td:eq(1)').html(parseFloat(data.onduty/data.total*100).toFixed(2)+' %');
            $('#hr_stats_table .table tr:eq(3) td:eq(0)').html(parseInt(data.training));
            $('#hr_stats_table .table tr:eq(3) td:eq(1)').html(parseFloat(data.training/data.total*100).toFixed(2)+' %');
            $('#hr_stats_table .table tr:eq(4) td:eq(0)').html(parseInt(data.leave));
            $('#hr_stats_table .table tr:eq(4) td:eq(1)').html(parseFloat(data.leave/data.total*100).toFixed(2)+' %');
            $('#hr_stats_table .table tr:eq(5) td:eq(0)').html(parseInt(data.absent));
            $('#hr_stats_table .table tr:eq(5) td:eq(1)').html(parseFloat(data.absent/data.total*100).toFixed(2)+' %');
            $('#hr_stats_table .table').show();
            
            $('#hr_stats_chart').parent().parent().find('.loading-img').remove();
        }
        
        function servicesStatsChart(){
            showLoader('services_chart');            
            
            var data = [];
            data = $.parseJSON('{!! json_encode($services) !!}');            
            
            if(data.graph){
                //$('#services_chart').attr('height',$('#services_table .chart-table').height()/3);
                // console.log($('#services_table .chart-table').height());
                new Chart(document.getElementById("services_chart").getContext("2d"), {
                    type: 'horizontalBar',
                    data: {
                        labels: data.graph.labels,
                        datasets: data.graph.datasets,
                    },
                    options: {
                        responsive: true,
                        legend: true,
                    },
                    plugins: ['zeroCompensation'],
                });
            }
            
            $('#services_chart').parent().parent().find('.loading-img').remove();
        }
        
        function revenueStatsChart(){
            showLoader('revenue_chart');
            
            var data = [];
            data = $.parseJSON('{!! json_encode($revenue) !!}');
            if(data.graph){
                //$('#revenue_chart').attr('height','300');                            
                new Chart(document.getElementById("revenue_chart").getContext("2d"), {
                    type: 'horizontalBar',
                    data: {
                        labels: data.labels,
                        datasets: data.graph,
                    },
                    options: {
                        responsive: true,
                        legend: true,
                        tooltips: {
                            enabled: true,
                            mode: 'single',
                            callbacks: {
                                label: function(tooltipItem, data) {                                                
                                    return formatMoney(tooltipItem.xLabel);
                                }
                            }
                        },
                    },                                
                    plugins: ['zeroCompensation'],
                });
            }            
            
            $('#revenue_chart').parent().parent().find('.loading-img').remove();        
        }
        
        const zeroCompensation = {
          renderZeroCompensation: function (chartInstance, d) {
              // get postion info from _view
              const view = d._view
              const context = chartInstance.chart.ctx

              // the view.x is the centeral point of the bar, so we need minus half width of the bar.
              const startX = view.x - view.width / 2
              // common canvas API, Check it out on MDN
              context.beginPath();
              // set line color, you can do more custom settings here.
              context.strokeStyle = '#aaaaaa';
              context.moveTo(startX, view.y);
              // draw the line!
              context.lineTo(startX + view.width, view.y);
              // bam！ you will see the lines.
              context.stroke();
          },

          afterDatasetsDraw: function (chart, easing) {
              // get data meta, we need the location info in _view property.
              const meta = chart.getDatasetMeta(0)
              // also you need get datasets to find which item is 0.
              const dataSet = chart.config.data.datasets[0].data
              meta.data.forEach((d, index) => {                  
                  // for the item which value is 0, reander a line.
                  if(dataSet[index] === 0) {
                    this.renderZeroCompensation(chart, d)
                  }
              })
          }
        };
        
        Chart.plugins.register(zeroCompensation);
        
        Chart.pluginService.register({
            afterDraw: function(chart, easing){
                var ctx = $(chart.chart.ctx.canvas);
                saveChartAsImage(ctx);
            }
        });
        
        function saveChartAsImage(ctx){
            if(ctx.length){
                var c = document.getElementById(ctx.attr('id'));
                var d = c.toDataURL("image/png");
                // console.log(d);
                //$(ctx.data('img')).attr('src',d);
                $(ctx.attr('id')).hide();
                
                $.ajax({
                    type: 'POST',
                    url: '{{ route('report.upload-chart-img') }}',
                    async: false,
                    data: {
                        _token: '{{ csrf_token() }}',
                        tenant_id: '{{ session('tenant_id') }}',
                        type: ctx.attr('id'),
                        imgBase64: d
                    }
                }).done(function(data) {                    
                    if(data.path){
                        $(ctx.data('img')).attr('src',data.path);
                    }
                });
            }
        }
    </script>
</body>