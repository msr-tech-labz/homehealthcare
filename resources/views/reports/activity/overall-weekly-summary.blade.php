<style type="text/css">
th,td{
    text-align: center !important;
}
@media only screen and (max-width: 500px) {
    #orgname{
        font-size: 12px !important;
    }
    #header{
        min-height: 50px !important;
    }
    #maincontent{
        padding: 0;
    }
    .stats{
        width: 49% !important;
    }
    .stats>h5{
        font-size: 150% !important;
    }
}
</style>
<body style="background: #e2e1e0; font-family: 'Roboto', sans-serif !important;">
    <div style="display:block; width: 95%; margin: 0 auto; background: #fff; border-radius: 2px; box-shadow: 0 10px 20px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);">
        <!-- Header -->
        <div id="header" style="display: block; max-width: 99%; min-height: 80px; border-bottom: 3px solid #fa6423; padding: 10px 10px 5px 10px; ">
            <div style="text-align:center;">
                <div  id="orgname" style="font-size: 28px; padding-left: 15px; font-weight: 300; height: 45px; color: #009688; line-height: 45px; vertical-align: middle; margin-right: 10px; letter-spacing: 1px">{{ $data['profile']['organization_name'] }}</div>
            </div>
        </div>

        <!-- Main Content -->
        <div id="maincontent" style="min-height: 200px; padding: 10px 15px; font-size: 14px; line-height: 22px; text-align: justify; margin: 10px 0; border-bottom: 1px solid #E0E0E0">
            <div class="body" style="min-height:200px;">
                <div style="min-height: 200px; padding: 10px 15px; line-height: 22px; text-align: justify; margin: 10px 0;">
                    <h2 style="margin-top: 0;">Your Weekly Report</h2>
                    <span style="color:#999;font-size: 16px;font-weight: 400;line-height: 1.3;">{{ date('d-M-Y') }}</span>
                    @if(count($data['totalLeads']) != 0)
                    <div style="border-top: 1px solid #e8e2e2;margin-bottom: 15px;">
                        <div style="text-align: center;margin-right: 2%;margin-bottom: 30px;">
                            <h5 style="font-family: Helvetica,Arial,sans-serif;font-size: 180%;font-weight: bolder;line-height: 1.3;    margin: 0;color: #009688;">Total Leads Received By Smart Health Connect this Week : {{ count($data['totalLeads']) }}</h5>
                        </div>
                        <div class="stats" style="text-align: center;width: 33%;display: inline-block;">
                            <h5 style="font-family: Helvetica,Arial,sans-serif;font-size: 160%;font-weight: bolder;line-height: 1.3;    margin: 0;color: darkmagenta;">Accepted/Pending : {{ count($data['totalLeadsAccepted']) }}</h5>
                        </div>  
                        <div class="stats" style="text-align: center;width: 33%;display: inline-block;">
                            <h5 style="font-family: Helvetica,Arial,sans-serif;font-size: 160%;font-weight: bolder;line-height: 1.3;    margin: 0;color: green;">Converted : {{ count($data['totalLeadsConverted']) }}</h5>
                        </div>              
                        <div class="stats" style="text-align: center;width: 33%;display: inline-block;">
                            <h5 style="font-family: Helvetica,Arial,sans-serif;font-size: 160%;font-weight: bolder;line-height: 1.3;    margin: 0;color: red;">Declined : {{ $data['totalLeadsDeclined'] }}</h5>
                        </div>
                    </div>
                    <div style="border-top:1px solid #E0E0E0;">
                        <h3 style="text-align: center;">Below Are the Customers Details</h3>
                        <table class="table table-bordered" style="width: 100%;">
                            <tr>
                                <th>Sl. No.</th>
                                <th>Date</th>
                                <th>Patient Name</th>
                                <th>Phone Number</th>
                                <th>Enquirer Name</th>
                                <th>Alternate Name</th>
                            </tr>
                            @foreach($data['totalLeads'] as $index => $dtl)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td>{{ $dtl->created_at->format('d-M-Y') }}</td>
                                <td>{{ $dtl->patient->full_name }}</td>
                                <td>{{ $dtl->patient->contact_number }}</td>
                                <td>{{ $dtl->patient->enquirer_name }}</td>
                                <td>{{ $dtl->patient->alternate_number }}</td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                    @else
                    <div style="border-top: 1px solid #e8e2e2;text-align: center;">
                        <h3>No Leads were provided ths week by Smart Health Connect</h3>
                    </div>
                    @endif
                </div>
            </div>
        </div>

        <!-- Footer -->
        <div style="display: block; position:relative; bottom: 0; max-width: 99%; min-height: 90px; padding: 5px 10px 0px 10px; ">
            <div style="width: 49%; display: inline-block; font-size: 12px;">
                <span>Copyright &copy; {{date('Y')}} <br> Smart Health Connect <br> All Rights Reserved</span><br><br>
                <span style="font-size: 11px; font-weight: 600">
                    <a href="" style="color: #2899dc !important">Terms and Conditions</a> |
                    <a href="" style="color: #2899dc !important">Privacy Policy</a>
                </span>
            </div>
            <div style="width: 49%; display: inline-block; text-align: right; vertical-align: top; font-size: 14px">
                <p style="margin-bottom: 10px;margin-top:0;padding-top:0">Powered by</p>
                <b><small>Smart Health Connect</small></b>
            </div>
            <br><br>
        </div>
    </div>
</body>

@push('report.scripts')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<script type="text/javascript" src="{{ ViewHelper::ThemeJs('daterangepicker.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
@endpush
