@extends('layouts.report-layout')

@section('page_title','Login Summary | Reports')

@section('report-title','Login Summary')

@section('report-summary','Login Summary')
<style type="text/css">
    .table-responsive{
        overflow-x: scroll !important;
    }
</style>
@section('report-filter')
    <div class="report-filter">
        <form id="filter-form" action="{{ route('activity.login-summary') }}" class="form-horizontal" method="post">
            {{ csrf_field() }}
            <div class="col-sm-4">
                <label class="label-control">Status</label>
                <select class="form-control input-sm" name="tenant_id" data-live-search="true">
                    <option value="">-- Any --</option>
                    @foreach($profileArray as $pl)
                    <option value="{{ \Helper::encryptor('encrypt',$pl['tenant_id']) }}">{{ $pl['organization_name'] }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-2">
                <label class="label-control">Period</label>
                <input type="text" id="period" class="form-control input-sm daterange" placeholder="Select Period"/>
                <input type="hidden" name="filter_period" />
            </div>
            <div class="col-sm-2 pull-right" style="margin-top: 1%">
                <input type="submit" class="btn btn-block btn-primary" value="Filter">
            </div>
            <div class="clearfix"></div>
        </form>
    </div>
@endsection

@section('report-content')
<div class="body" style="min-height:200px">
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-condensed loginsTable" style="width:100%;margin-bottom:30px !important;font-size: 14px !important;">
            <thead>
                <tr>
                    <th width="10%">Date</th>
                    <th width="25%">Provider</th>
                    <th width="10%">User</th>
                    <th width="10%">Login</th>
                    <th width="10%">Logout</th>
                    <th>IP</th>
                    <th>Agent</th>
                </tr>
            </thead>
            <tbody>
                @forelse($summary as $sumArr)
                    <tr>
                        <td>{{ $sumArr['date'] }}</td>
                        <td>{{ $sumArr['provider'] }}</td>
                        <td>{{ $sumArr['user'] }}</td>
                        <td>{{ $sumArr['login'] }}</td>
                        <td>{{ $sumArr['logout'] }}</td>
                        <td>{{ $sumArr['ip'] }}</td>
                        <td>{{ $sumArr['agent'] }}</td>
                    </tr>
                @empty
                <tr><td colspan="7" class="text-center">No logins</td></tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection

@push('report.scripts')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
    <script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
    <script type="text/javascript" src="{{ ViewHelper::ThemeJs('daterangepicker.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<script>
    var rTable;
    $(document).ready(function() {
        @if($summary) //Count Logins
        $('.loginsTable').DataTable({
            lengthMenu: [[50,100,200,-1],[50,100,200,"All"]],
            language: {
                processing: "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate' style='color:#ff0000'> Loading....</span>"
            },
            order: [[0,'asc']],
            // displayLength: -1,
            dom: 'Bflrpt',
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: 'Leads-Master-Data_{{ date("d-m-Y_H:i:s") }}'
                },
                {
                    extend: 'pdfHtml5',
                    pageSize: 'A3'
                },
                {
                    extend: 'print',
                    title: '<h3 style="text-align:center;">Leads Master Data</h3>',
                    customize: function ( win ) {
                        $(win.document.body).css( 'font-size', '7pt' );

                        $(win.document.body).find( 'table' ).addClass( 'compact' ).css( 'font-size', 'inherit' );
                    }
                }
            ],
        });
        @endif

        $('.daterange').daterangepicker({
            autoApply: true,
            autoUpdateInput: false,
            alwaysShowCalendars: true,
            locale: {
              format: 'DD-MM-YYYY',
              applyLabel: 'Apply',
              cancelLabel: 'Clear'
            },
            ranges: {
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'Last 30 Days': [moment().subtract(29, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
           }
         },
         function(start, end, label) {
             $('input[name=filter_period]').val(start.format('YYYY-MM-DD')+'_'+end.format('YYYY-MM-DD'));
             $('#period').val(start.format('DD-MM-YYYY')+' - '+end.format('DD-MM-YYYY'))
         }
        );

        $('#filter-form').on('submit', function(e) {
            if ( ! $.fn.DataTable.isDataTable('.reportTable') ) {
                getData();
            }else{
                rTable.draw();
            }
            e.preventDefault();
        });
    });
</script>
@endpush
