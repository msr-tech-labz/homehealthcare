@extends('layouts.report-layout')

@section('page_title','Bench Staff - Forecast | Reports')

@section('report-title','Bench Forecast Report')

@section('report-summary','5 Day Forecast of Active Employees on Bench')

@section('report-content')
<div class="body table-responsive" style="min-height:200px">
    <table class="table table-condensed" style="width:100%">
        <thead>
            <tr><th class="text-center" colspan="5">Forecasted Active Employees on Bench</th></tr>
        </thead>
        
        <tbody>
            @foreach($benchData as $date => $data)
                <th class="text-center" style="border: 1px solid darkgray; background: #1dc8cd;">
                    {{ explode('_',$date)[0] }} - <span style="color: #fff">{{ explode('_',$date)[1] }}
                </th>
            @endforeach
            <tr>
            @foreach($benchData as $date => $data)
                <td style="border: 1px solid darkgrey;">@foreach($data['benchers'] as $benchers){{ ($benchers) }}<br>@endforeach</td>
            @endforeach
            </tr>
        </tbody>
    </table>
</div>
@endsection
