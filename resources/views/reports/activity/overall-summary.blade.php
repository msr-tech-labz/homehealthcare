@extends('layouts.basic-mail-layout')

@section('content')
<div class="body" style="min-height:200px;">
    <div style="min-height: 200px; padding: 10px 15px; line-height: 22px; text-align: justify; margin: 10px 0; border-bottom: 1px solid #E0E0E0;">
        <h2 style="margin-top: 0;">Your Daily Report</h2>
        <span style="color:#999;font-size: 16px;font-weight: 400;line-height: 1.3;">{{ date('d-M-Y') }}</span>
        <div style="border-top: 1px solid #e8e2e2">
            <div style="text-align: center;margin-right: 2%;">
                <h4 style="color: #827e7e;"><u>Total Employees</u></h4>
                <h5 style="font-family: Helvetica,Arial,sans-serif;font-size: 180%;font-weight: bolder;line-height: 1.3;    margin: 0;color: lightblue;">{{ $data['totalEmployees'] }}</h5>
            </div>
            <div class="stats" style="text-align: center;width: 24%;display: inline-block;">
                <h4 style="color: #999;">On Duty</h4>
                <h5 style="font-family: Helvetica,Arial,sans-serif;font-size: 180%;font-weight: bolder;line-height: 1.3;    margin: 0;color: green;">{{ $data['totalEmployeesOnDuty'] }}</h5>
            </div>  
            <div class="stats" style="text-align: center;width: 24%;display: inline-block;">
                <h4 style="color: #999;">Available</h4>
                <h5 style="font-family: Helvetica,Arial,sans-serif;font-size: 180%;font-weight: bolder;line-height: 1.3;    margin: 0;color: orange;">{{ $data['totalEmployeesAvailable'] }}</h5>
            </div>              
            <div class="stats" style="text-align: center;width: 24%;display: inline-block;">
                <h4 style="color: #999;">On Leave</h4>
                <h5 style="font-family: Helvetica,Arial,sans-serif;font-size: 180%;font-weight: bolder;line-height: 1.3;    margin: 0;color: red;">{{ $data['totalEmployeesOnLeave'] }}</h5>
            </div>
            <div class="stats" style="text-align: center;width: 24%;display: inline-block;">
                <h4 style="color: #999;">On Training</h4>
                <h5 style="font-family: Helvetica,Arial,sans-serif;font-size: 180%;font-weight: bolder;line-height: 1.3;    margin: 0;color: blue;">{{ $data['totalEmployeesOnTraining'] }}</h5>
            </div>
        </div>
        <div style="border-top: 1px solid #e8e2e2">
            <div class="col-md-12" style="text-align: center;">
                <h4 style="color: #827e7e;"><u>Payment Stats</u></h4>
            </div>
            <div class="stats" style="text-align: center;width: 49%;display: inline-block;">
                <h4 style="color: #999;">Pending</h4>
                <h5 style="font-family: Helvetica,Arial,sans-serif;font-size: 180%;font-weight: bolder;line-height: 1.3;    margin: 0;color: red;">&#8377;{{ $data['pendingPayments'] }}</h5>
            </div>  
            <div class="stats" style="text-align: center;width: 49%;display: inline-block;">
                <h4 style="color: #999;">Received</h4>
                <h5 style="font-family: Helvetica,Arial,sans-serif;font-size: 180%;font-weight: bolder;line-height: 1.3;    margin: 0;color: green;">&#8377;{{ $data['receivedPayments'] }}</h5>
            </div>
        </div>
        <div style="border-top: 1px solid #e8e2e2">
            <div class="col-md-12" style="text-align: center;">
                <h4 style="color: #827e7e;"><u>Service Stats</u></h4>
            </div>
            <div class="stats" style="text-align: center;width: 49%;display: inline-block;">
                <h4 style="color: #999;">Registered</h4>
                <h5 style="font-family: Helvetica,Arial,sans-serif;font-size: 180%;font-weight: bolder;line-height: 1.3;margin: 0;color: green;">{{ $data['servicesResgistered'] }}</h5>
            </div>
            <div class="stats" style="text-align: center;width: 49%;display: inline-block;">
                <h4 style="color: #999;">Staff Visits</h4>
                <h5 style="font-family: Helvetica,Arial,sans-serif;font-size: 180%;font-weight: bolder;line-height: 1.3;margin: 0;color: green;">{{ $data['visitsCompleted'] }}</h5>
            </div>
        </div>
        <div style="border-top: 1px solid #e8e2e2">
            <div style="text-align: center;">
                <h4 style="color: #827e7e;"><u>Lead Stats</u></h4>
            </div>
            <div class="stats" style="text-align: center;width: 24%;display: inline-block;">
                <h4 style="color: #999;">Converted</h4>
                <h5 style="font-family: Helvetica,Arial,sans-serif;font-size: 180%;font-weight: bolder;line-height: 1.3;margin: 0;color: green;">{{ $data['convertedToday'] }}</h5>
            </div>
            <div class="stats" style="text-align: center;width: 24%;display: inline-block;">
                <h4 style="color: #999;">Pending</h4>
                <h5 style="font-family: Helvetica,Arial,sans-serif;font-size: 180%;font-weight: bolder;line-height: 1.3;margin: 0;color: orange;">{{ $data['pendingToday'] }}</h5>
            </div>
            <div class="stats" style="text-align: center;width: 24%;display: inline-block;">
                <h4 style="color: #999;">Lost</h4>
                <h5 style="font-family: Helvetica,Arial,sans-serif;font-size: 180%;font-weight: bolder;line-height: 1.3;margin: 0;color: red;">{{ $data['lostToday'] }}</h5>
            </div>
            <div class="stats" style="text-align: center;width: 24%;display: inline-block;">
                <h4 style="color: #999;">Closed</h4>
                <h5 style="font-family: Helvetica,Arial,sans-serif;font-size: 180%;font-weight: bolder;line-height: 1.3;margin: 0;color: blue;">{{ $data['closedToday'] }}</h5>
            </div>
        </div>
    </div>
</div>
@endsection

@push('report.scripts')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
    <script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
    <script type="text/javascript" src="{{ ViewHelper::ThemeJs('daterangepicker.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
@endpush
