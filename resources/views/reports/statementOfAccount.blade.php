@extends('layouts.main-layout')

{? $ptitle = ""; ?}
@if(isset($patient))
    {? $ptitle = $patient." - "; ?}
@endif
@section('page_title','Statement of Account - '.$ptitle)

@section('page.styles')
<style>
.card{
    overflow-x: scroll;
}
.table-bordered tbody tr th {
    text-align: center;
}
.dt-buttons{
    position: static !important;
    float: right;
}
.paginate_button {
    cursor: pointer;
    margin:2px;
}
div.dataTables_wrapper div.dataTables_filter {
    text-align: center !important;
}
table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td {
    text-transform: capitalize; !important;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
}
</style>
<!-- JQuery DataTable Css -->
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('billing.index') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-6">
                    Statement of Account
                    <small>Customer Service Summary</small>
                </h2>
            </div>
            <div class="body">
                <div class="row clearfix">
                    <form action="{{ route('reports.statement-of-account') }}" method="post">
                        {{ csrf_field() }}
                        <table class="table table-bordered">
                            <tr>
                                <th width="20%">
                                    <select class="form-control" name="filter_patient" required="">
                                        <option value="" disabled="" selected="">-- Select Customer --</option>
                                        @if(isset($customers))
                                            @foreach ($customers as $customer)
                                                <option value="{{ $customer->id }}" {{ (isset($patient_id) && $patient_id == $customer->id)?'selected':'' }}>{{ $customer->full_name.' ['.$customer->patient_id.']' }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </th>
                                <th width="20%">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="min" name="start_date" placeholder="From Date">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="max" name="end_date" placeholder="To Date">
                                    </div>
                                </th>
                                <th width="15%">
                                    <input type="submit" class="btn btn-success">
                                </th>
                            </tr>
                        </table>
                    </form>
                </div>
                <table id="casesConsolidatedReport" class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable">
                    <caption class="text-right"><small>All figures are in INR</small></caption>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Patient ID</th>
                            <th>Name</th>
                            <th>Service</th>
                            <th>Date</th>
                            <th>Timings</th>
                            <th>Employee ID</th>
                            <th>Employee Name</th>
                            <th>Chargeable</th>
                            <th>Net Rate</th>
                            <th>Discount</th>
                            <th>Gross Rate</th>
                        </tr>
                    </thead>
                    <tbody>
                        {? $totalNetRate = 0; $totalDiscount = 0; $totalAmount = 0; ?}
                        @if(isset($schedules) && count($schedules))
                            @foreach ($schedules as $i => $schedule)
                                <tr>
                                    <td>{{ $i + 1 }}</td>
                                    <td>{{ $schedule->patient->patient_id ?? '' }}</td>
                                    <td>{{ $schedule->patient->full_name ?? '' }}</td>
                                    <td>{{ $schedule->service->service_name ?? '' }}</td>
                                    <td>{{ !empty($schedule->schedule_date)?$schedule->schedule_date->format('d-m-Y'):'' }}</td>
                                    <td>{{ $schedule->start_time }} {{ !empty($schedule->end_time)?' to '.$schedule->end_time:'' }}</td>
                                    <td>{{ $schedule->caregiver->employee_id ?? '' }}</td>
                                    <td>{{ $schedule->caregiver->full_name ?? '' }}</td>
                                    <td>{{ $schedule->chargeable?'Yes':'No' }}</td>
                                    {? $totalNetRate += floatVal($schedule->serviceOrder->serviceRequest->userRateCardService->rate); ?}
                                    <td class="text-right">{{ $schedule->serviceOrder->serviceRequest->userRateCardService->rate ?? 0.00 }}</td>
                                    {? $discount = 0; ?}
                                    @if(isset($schedule->serviceOrder->serviceRequest->userRateCardService))
                                        @if($schedule->serviceOrder->serviceRequest->userRateCardService->discount_type == 'P')
                                            {? $rate = $schedule->serviceOrder->serviceRequest->userRateCardService->rate; ?}
                                            {? $discount_value = $schedule->serviceOrder->serviceRequest->userRateCardService->discount_value; ?}
                                            {? $discount =  $rate * ($discount_value / 100); ?}
                                        @else
                                            {? $discount = $schedule->serviceOrder->serviceRequest->userRateCardService->discount_value; ?}
                                        @endif
                                        {? $totalDiscount += floatVal($discount); ?}
                                    @endif
                                    <td class="text-right">{{ number_format($discount,2) }}</td>
                                    {? $totalAmount += floatVal($schedule->serviceOrder->serviceRequest->userRateCardService->total_amount); ?}
                                    <td class="text-right">{{ $schedule->serviceOrder->serviceRequest->userRateCardService->total_amount ?? 0.00 }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td style="color: transparent">{{ count($schedules) + 1 }}</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                                <th style="font-size: 19px;font-weight:600;" class="text-center">Total</th>
                                <th class="text-right" style="font-size: 19px;font-weight:500;">{{ number_format($totalNetRate,2) }}</th>
                                <th class="text-right" style="font-size: 19px;font-weight:500;">{{ number_format($totalDiscount,2) }}</th>
                                <th class="text-right" style="font-size: 19px;font-weight:500;">{{ number_format($totalAmount,2) }}</th>
                            </tr>
                        @else
                            <tr>
                                <td colspan="14" class="text-center">Please select patient</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection


@section('page.scripts')
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    {{-- <script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.13/filtering/row-based/range_dates.js"></script> --}}
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#casesConsolidatedReport').DataTable( {
            "lengthMenu": [[50, 100, 200, -1], [50, 100, 200, "All"]],
            ordering: false,
            dom: 'BfrtF',
            buttons: [
                'excel',
                {
                    extend: 'pdfHtml5',
                    pageSize: 'A3'
                },
                {
                    extend: 'print',
                    title: '<h4 style="text-align:center;">Statement of Account{{ isset($patient)?' - '.$patient:'' }}</h4>',
                    customize: function ( win ) {
                        $(win.document.body)
                        .css( 'font-size', '5pt' );

                        $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', '6pt' );
                    }
                }
            ],
        } );
    } );

    </script>
@endsection
