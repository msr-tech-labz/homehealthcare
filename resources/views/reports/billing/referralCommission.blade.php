@extends('layouts.main-layout')

@section('page_title','Referral Commission - Billing Reports |')

@section('page.styles')
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
<style>
    div.dataTables_wrapper div.dataTables_filter {
    text-align: center !important;
    }
    .dt-buttons {
        position: static !important;
        float: right;
        margin-top: 15px;
    }
    table.table-bordered thead tr th, table.table-bordered tbody tr td {
        text-transform: capitalize; !important;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        font-size: 12px !important;
    }
    .report-filter {
    width: 95%;
    margin: auto 0 !important;
    background: #f6f6f6;
    border-radius: 4px;
    border: 1px solid #ccc;
    padding: 10px;
    margin-top: 10px !important;
    margin-bottom: 10px !important;
    min-height: 50px;
    margin-left: 2.5% !important;
}
</style>
@endsection

@section('content')
<div class="card">
    <div class="header clearfix">
                <a href="{{ route('reports.index') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-10">
                   Referral Commission
                   <small>Referral Commission Summary</small>
               </h2>
           </div>
<div class="report-filter">
        <form id="filter-form" class="form-horizontal" method="post">
            <div class="col-sm-4">
                <label class="label-control">Referral Source</label>
                <select class="form-control input-sm show-tick" name="filter_source" data-live-search="true">
                    <option value="">-- All Source --</option>
                @if(isset($source_names) && count($source_names))
                    @foreach ($source_names as $source_name)
                        <option value="{{ $source_name->id }}">{{ $source_name->source_name }}</option>
                    @endforeach
                @endif
                </select>
            </div>
            <div class="col-sm-2">
                <label class="label-control">Month</label>
                <select class="form-control input-sm" name="filter_month">
                    <option value="" disabled="" selected="">-- Choose Month --</option>
                    <option value="01">Jan</option>
                    <option value="02">Feb</option>
                    <option value="03">Mar</option>
                    <option value="04">Apr</option>
                    <option value="05">May</option>
                    <option value="06">Jun</option>
                    <option value="07">Jul</option>
                    <option value="08">Aug</option>
                    <option value="09">Sep</option>
                    <option value="10">Oct</option>
                    <option value="11">Nov</option>
                    <option value="12">Dec</option>
               </select>
            </div>
            <div class="col-sm-2">
                <label class="label-control">Year</label>
                <select class="form-control input-sm" name="filter_year">
                    <option value="" disabled="" selected="">-- Choose Year --</option>
                    <option value="2021">2021</option>
                    <option value="2020">2020</option>
                    <option value="2019">2019</option>
                    <option value="2018">2018</option>
               </select>
            </div>
            <div class="col-sm-2 pull-right" style="margin-top: 1%">
                <input type="button" class="btn btn-block btn-primary btnFilter" value="Filter">
            </div>
            <div class="clearfix"></div>
        </form>
    </div>

<div class="body" style="min-height:200px">
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-condensed reportTable" style="width:100%">
            <thead>
                <tr>
                    <th>Source</th>
                    <th>Referrer Name</th>
                    <th>Patient Name</th>
                    <th>Category</th>
                    <th>Invoice Created</th>
                    <th>Invoive Amount (Rs)</th>
                    <th>Referral Value</th>
                    <th>Commission Amount (Rs)<br></th>
                </tr>
            </thead>
            <tbody>
                <tr><td colspan="11" class="text-center">Select Month and Year to filter</td></tr>
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="7" style="text-align:right">Total Current Page:<br>Total All Page:</th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
</div>
@endsection


@section('page.scripts')
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
    <script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>

    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>

    <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>

<script>
    var rTable;
    $(document).ready(function() {
        $('.content').on('click', '.btnFilter',function(e) {
            if ( ! $.fn.DataTable.isDataTable('.reportTable') ) {
                getData();
            }else{
                rTable.draw();
            }
            e.preventDefault();
        });
    });

    function getData(){
        rTable = $('.reportTable').DataTable({
            processing: true,
            serverSide: true,
            lengthMenu: [[50,100,200,-1],[50,100,200,"All"]],
            language: {
                processing: "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate' style='color:#ff0000'> Loading....</span>"
            },
            ajax: {
                url: '{{ route('sales.referral-commission','ajax=true') }}',
                type: 'POST',
                data: function (d) {
                    d._token = '{{ csrf_token() }}';
                    d.filter_source = $('select[name=filter_source] option:selected').val();
                    d.filter_month = $('select[name=filter_month] option:selected').val();
                    d.filter_year = $('select[name=filter_year] option:selected').val();
                }
            },
            // displayLength: -1,
            dom: 'Bflrpt',
            columns: [
                {data : 'source_name', value : 'source_name', searchable: true},
                {data : 'referrer_name', value : 'referrer_name', searchable: true},
                {data : 'full_name', value : 'full_name', searchable: true},
                {data : 'category_name', value : 'category_name', searchable: true},
                {data : 'invoice_date', value : 'invoice_date', searchable: true},
                {data : 'invoice_amount', name : 'invoice_amount'},
                {data : 'referral_value', value : 'referral_value', searchable: true},
                {data : 'commission_amount', value : 'commission_amount', searchable: true},
            ],
            drawCallback: function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last = null;

                api.column(0, {page:'current'} ).data().each( function ( group, i ) {
                    if(group != null){
                        if ( last !== group ) {
                            $(rows).eq( i ).before(
                                '<tr class="group"><td colspan="11"><b>'+group.toUpperCase()+'</b></td></tr>'
                            );
                            last = group;
                        }
                    }
                });
            },
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: 'Revenue-with-Schedules_{{ date("d-m-Y_H:i:s") }}'
                }
            ],

            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
                
                // Remove the formatting to get integer data for summation
                var floatVal = function ( i ) {
                    return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                    i : 0;
                };

                // Total over all pages
                total = api
                .column( 7 )
                .data()
                .reduce( function (a, b) {
                    return parseFloat(floatVal(a)) + parseFloat(floatVal(b));
                }, 0 ).toFixed(2);

                // Total over this page
                pageTotal = api
                .column( 7, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return parseFloat(floatVal(a)) + parseFloat(floatVal(b));
                }, 0 ).toFixed(2);

                // Update footer
                $( api.column( 7 ).footer() ).html (
                    'Rs '+pageTotal +' <br>Rs '+ total
                );
            }

        });
    }
</script>
@endsection