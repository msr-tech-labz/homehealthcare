@extends('layouts.main-layout')

@section('page_title','Cases Consolidated Report - Billing Reports')

@section('page.styles')
<style>
    .card{
        overflow-x: scroll;
    }
    .table-bordered tbody tr th {
        text-align: center;
    }
    .dt-buttons{
        position: static !important;
        float: right;
    }
    .paginate_button {
        cursor: pointer;
        margin:2px;
    }
    div.dataTables_wrapper div.dataTables_filter {
        text-align: center !important;
    }
    table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td {
      text-transform: uppercase; !important;
      overflow: hidden;
      text-overflow: ellipsis;
      white-space: nowrap;
    }
</style>
<!-- JQuery DataTable Css -->
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection

@section('plugin.styles')
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<style>
</style>
@endsection

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <h2 class="col-sm-10">
                   Consolidated Report
                   <small>Schedules Consolidated Summary</small>
               </h2>
           </div>
           <div class="body">
           <div class="row clearfix">
               <form action="{{ route('case.reports') }}" method="post">
                   {{ csrf_field() }}
                   <table class="table table-bordered">
                       <tr>
                           <th width="20%">
                               <select class="form-control" name="filter_status">
                                   <option value="">-- Any --</option>
                                   <option value="Pending">PENDING</option>
                                   <option value="Completed">COMPLETED</option>
                                   <option value="Interrupted">INTERRUPTED</option>
                               </select>
                           </th>
                           <th width="20%">
                               <div class="col-md-6">
                               <input type="text" class="form-control" id="min" name="start_date" placeholder="From Date">
                               </div>
                               <div class="col-md-6">
                               <input type="text" class="form-control" id="max" name="end_date" placeholder="To Date">
                               </div>
                           </th>
                           <th width="15%">
                               <input type="submit" class="btn btn-success">
                           </th>
                       </tr>
                   </table>
               </form>
           </div>
            <table id="casesConsolidatedReport" class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Request Date</th>
                        <th>Patient</th>
                        <th>Enquirer</th>
                        <th>Schedule Type</th>
                        <th>Service</th>
                        <th>Period</th>
                        <th>Timings</th>
                        <th>Shift</th>
                        <th>Caregiver</th>
                        <th>Amount</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($schedules as $i => $c)
                          <tr>
                            <td class="text-center">{{ $i+1 }}</td>
                            <td>{{ $c->caserecord->created_at->format('d-m-Y') }}</td>
                            <td>{{ $c->patient->full_name }}</td>
                            <td>{{ $c->patient->enquirer_name }}</td>
                            <td>{{ $c->schedule_type }}</td>
                            <td>{{ $c->service->service_name }}</td>
                            <td>{{ $c->schedule_date->format('d-m-Y') }}</td>
                            <td>{{ $c->start_time ?? '-' }} to {{ $c->end_time ?? '-' }}</td>
                            <td>{{ $c->shift ?? '-' }}</td>
                            <td>{{ $c->caregiver->full_name ?? '-' }}</td>
                            <td>{{ $c->ratecard->total_amount ?? Helper::getServiceRate($c->service_required) }}</td>
                            <td>{{ $c->status }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="12" class="text-center">No record(s) found</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
@endsection


@section('page.scripts')
<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
{{-- <script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.13/filtering/row-based/range_dates.js"></script> --}}
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
<!-- Moment Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<script>
    $(document).ready(function() {
        $('#casesConsolidatedReport').DataTable( {
            "lengthMenu": [[50, 100, 200, -1], [50, 100, 200, "All"]],
            dom: 'Bflrtip',
            buttons: [
            'excel',
            {
                extend: 'pdfHtml5',
                pageSize: 'A3'
            },
            {
                extend: 'print',
                title: '<h3 style="text-align:center;">Consolidated Schedules Report</h3>',
                customize: function ( win ) {
                    $(win.document.body)
                    .css( 'font-size', '7pt' );

                    $(win.document.body).find( 'table' )
                    .addClass( 'compact' )
                    .css( 'font-size', 'inherit' );
                }
            }
            ],
            "order": [[ 0, "desc" ]]
        } );
    } );

    /* Custom filtering function which will search data in column four between two values */
    $.fn.dataTableExt.afnFiltering.push(
        function( oSettings, aData, iDataIndex ) {
            var iFini = document.getElementById('min').value;
            var iFfin = document.getElementById('max').value;
            var iStartDateCol = 1;
            var iEndDateCol = 1;

            iFini=iFini.substring(6,10) + iFini.substring(3,5)+ iFini.substring(0,2);
            iFfin=iFfin.substring(6,10) + iFfin.substring(3,5)+ iFfin.substring(0,2);

            var datofini=aData[iStartDateCol].substring(6,10) + aData[iStartDateCol].substring(3,5)+ aData[iStartDateCol].substring(0,2);
            var datoffin=aData[iEndDateCol].substring(6,10) + aData[iEndDateCol].substring(3,5)+ aData[iEndDateCol].substring(0,2);

            if ( iFini === "" && iFfin === "" )
            {
                return true;
            }
            else if ( iFini <= datofini && iFfin === "")
            {
                return true;
            }
            else if ( iFfin >= datoffin && iFini === "")
            {
                return true;
            }
            else if (iFini <= datofini && iFfin >= datoffin)
            {
                return true;
            }
            return false;
        }
        );
    $(document).ready(function() {
        var table = $('#caseReport').DataTable();
        // Event listener to the two range filtering inputs to redraw on input
        $('#min, #max').keyup( function() {
            table.draw();
        } );
    } );


    $(function(){
        $('#min').bootstrapMaterialDatePicker({
            format: 'DD-MM-YYYY',
            clearButton: true,
            time: false,
            maxDate : new Date(),
        });
        $('#max').bootstrapMaterialDatePicker({
            format: 'DD-MM-YYYY',
            clearButton: true,
            time: false,
            maxDate : new Date(),
        });
    });

</script>
@endsection
