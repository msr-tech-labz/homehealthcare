@extends('layouts.report-layout')

@section('page_title','Master Data - Leads | Reports')

@section('report-title','Master Data - Leads')

@section('report-summary','Leads master data')
<style type="text/css">
    .table-responsive{
        overflow-x: scroll !important;
    }
</style>
@section('report-filter')
    <div class="report-filter">
        <form id="filter-form" action="{{ route('leads.daily-stats') }}" class="form-horizontal" method="post">
            {{ csrf_field() }}
            <div class="col-sm-2">
                <label class="label-control">Period</label>
                <input type="text" id="period" class="form-control input-sm daterange" placeholder="Select Period"/>
                <input type="hidden" name="filter_period" />
            </div>
            <div class="col-sm-2 pull-right" style="margin-top: 1%">
                <input type="submit" class="btn btn-block btn-primary" value="Filter">
            </div>
        </form>
            <div class="col-sm-2 pull-right">
                <button class="btn btn-block btn-primary waves-effect hide" id="mailStats">Mail</button>
            </div>
            <div class="clearfix"></div>
    </div>
@endsection

@section('report-content')
<div class="body" style="height: 100%">
    <div style="min-height:310px;">
        <div class="col-md-4">
            <table class="table table-bordered table-hover table-condensed reportTable" style="width:100%;margin-bottom:30px !important;font-size: 14px !important;">
                <thead>
                    <tr>
                        <th colspan="2" class="text-center">Aggregator</th>
                    </tr>
                    <tr>
                        <th>Total Leads</th>
                        <td>{{ $data['totalLeads'] }}</td>
                    </tr>
                    <tr>
                        <th>Total Leads Pending with Apnacare</th>
                        <td>{{ $data['aggPending'] }}</td>
                    </tr>
                    <tr>
                        <th>Total Leads Converted by Apnacare</th>
                        <td>{{ $data['aggConverted'] }}</td>
                    </tr>
                    <tr>
                        <th>Total Leads Lost by Apnacare</th>
                        <td>{{ $data['aggDropped'] }}</td>
                    </tr>
                    <tr>
                        <th>Convertion Ratio</th>
                        <td>{{ $data['aggConverted'] + $data['convertedProvider'] }}:{{ $data['totalLeads'] }}</td>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="col-md-4">
            <table class="table table-bordered table-hover table-condensed reportTable" style="width:100%;margin-bottom:30px !important;font-size: 14px !important;">
                <thead>
                    <tr>
                        <th colspan="2" class="text-center">Provider</th>
                    </tr>
                    <tr>
                        <th>Total Leads Transferred to Providers</th>
                        <td>{{ $data['transProvider'] }}</td>
                    </tr>
                    <tr>
                        <th>Total Leads Pending with Providers</th>
                        <td>{{ $data['pendingProvider'] }}</td>
                    </tr>
                    <tr>
                        <th>Total Leads Converted by Providers</th>
                        <td>{{ $data['convertedProvider'] }}</td>
                    </tr>
                    <tr>
                        <th>Total Leads Dropped by Providers</th>
                        <td>{{ $data['lossProvider'] }}</td>
                    </tr>
                    <tr>
                        <th>Acceptance/Declination Ratio</th>
                        <td>{{ $data['acceptanceRatioProvider'] }}</td>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="col-md-4">
            <table class="table table-bordered table-hover table-condensed reportTable" style="width:100%;margin-bottom:30px !important;font-size: 14px !important;">
                <thead>
                    <tr>
                        <th colspan="2" class="text-center">Freelancers</th>
                    </tr>
                    <tr>
                        <th>Total Leads Transferred to Freelancers</th>
                        <td>0</td>
                    </tr>
                    <tr>
                        <th>Total Leads Pending with Freelancers</th>
                        <td>0</td>
                    </tr>
                    <tr>
                        <th>Total Leads Converted by Freelancers</th>
                        <td>0</td>
                    </tr>
                    <tr>
                        <th>Acceptance Ratio</th>
                        <td>0/0</td>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div style="min-height:310px;">
        <div class="col-md-4">
            <table class="table table-bordered table-hover table-condensed reportTable" style="width:100%;margin-bottom:30px !important;font-size: 14px !important;">
                <thead>
                    <tr>
                        <th colspan="2" class="text-center">Exotel</th>
                    </tr>
                    <tr>
                        <th>Total Leads from Exotel</th>
                        <td>{{ $data['leadExotelTotal'] }}</td>
                    </tr>
                    <tr>
                        <th>Leads Attended</th>
                        <td>{{ $data['leadExotelAttended'] }}</td>
                    </tr>
                    <tr>
                        <th>Leads Unattended</th>
                        <td>{{ $data['leadExotelUnattended'] }}</td>
                    </tr>
                    <tr>
                        <th>Leads Pending</th>
                        <td>{{ $data['leadExotelPending'] }}</td>
                    </tr>
                    <tr>
                        <th>Leads Converted</th>
                        <td>{{ $data['leadExotelConverted'] }}</td>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="col-md-4">
            <table class="table table-bordered table-hover table-condensed reportTable" style="width:100%;margin-bottom:30px !important;font-size: 14px !important;">
                <thead>
                    <tr>
                        <th colspan="2" class="text-center">Website</th>
                    </tr>
                    <tr>
                        <th>Total Leads from Website</th>
                        <td>{{ $data['leadWebsiteTotal'] }}</td>
                    </tr>
                    <tr>
                        <th>Leads Attended</th>
                        <td>{{ $data['leadWebsiteAttended'] }}</td>
                    </tr>
                    <tr>
                        <th>Leads Unattended</th>
                        <td>{{ $data['leadWebsiteUnattended'] }}</td>
                    </tr>
                    <tr>
                        <th>Leads Pending</th>
                        <td>{{ $data['leadWebsitePending'] }}</td>
                    </tr>
                    <tr>
                        <th>Leads Converted</th>
                        <td>{{ $data['leadWebsiteConverted'] }}</td>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="col-md-4">
            <table class="table table-bordered table-hover table-condensed reportTable" style="width:100%;margin-bottom:30px !important;font-size: 14px !important;">
                <thead>
                    <tr>
                        <th colspan="4" class="text-center">MCM</th>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <th>Jaya Kumari</th>
                        <th>Sheela</th>
                        <th>Bhavya</th>
                    </tr>
                    <tr>
                        <th>Pending</th>
                        <th>{{ $data['jayaPending'] }}</th>
                        <th>{{ $data['sheelaPending'] }}</th>
                        <th>{{ $data['bhavyaPending'] }}</th>
                    </tr>
                    <tr>
                        <th>Converted</th>
                        <th>{{ $data['jayaConverted'] }}</th>
                        <th>{{ $data['sheelaConverted'] }}</th>
                        <th>{{ $data['bhavyaConverted'] }}</th>
                    </tr>
                    <tr>
                        <th>Case Loss</th>
                        <th>{{ $data['jayaCaseLoss'] }}</th>
                        <th>{{ $data['sheelaCaseLoss'] }}</th>
                        <th>{{ $data['bhavyaCaseLoss'] }}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@push('report.scripts')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
    <script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
    <script type="text/javascript" src="{{ ViewHelper::ThemeJs('daterangepicker.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
    <!-- Bootstrap Notify Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-notify/bootstrap-notify.js') }}"></script>
    <script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
<script>
    var rTable;
    $(document).ready(function() {
        $('.daterange').daterangepicker({
            autoApply: true,
            autoUpdateInput: false,
            alwaysShowCalendars: true,
            locale: {
              format: 'DD-MM-YYYY',
              applyLabel: 'Apply',
              cancelLabel: 'Clear'
            },
            ranges: {
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'Last 30 Days': [moment().subtract(29, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
           }
         },
         function(start, end, label) {
             $('input[name=filter_period]').val(start.format('YYYY-MM-DD')+'_'+end.format('YYYY-MM-DD'));
             $('#period').val(start.format('DD-MM-YYYY')+' - '+end.format('DD-MM-YYYY'));
         }
        );
        if('{{$fromDate}}' && '{{$toDate}}'){
            var startsplit = '{{ $fromDate }}';
                var start = startsplit.split("-").reverse().join("-");
            var endsplit = '{{ $toDate }}';
                var end = endsplit.split("-").reverse().join("-");

            $('#period').val(start+' - '+end);
            $('input[name=filter_period]').val(startsplit+'_'+endsplit);
        }
        $('.content').on('click','#mailStats',function(){
            var filter_period = $('input[name=filter_period]').val();
            $.ajax({
                url : '{{ route('leads.daily-stats') }}',
                type: 'POST',
                data: {_token: '{{ csrf_token() }}', filter_period: filter_period, mail: true},
                success: function (data){
                    showNotification('bg-green', 'Email sent successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                },
                error: function (error){
                    console.log(error);
                }
            });

        });
    });
</script>
@endpush
