@extends('layouts.report-layout')

@section('page_title','Master Data - Leads | Reports')

@section('report-title','Master Data - Leads')

@section('report-summary','Leads master data')
<style type="text/css">
    .table-responsive{
        overflow-x: scroll !important;
    }
</style>
@section('report-filter')
    <div class="report-filter">
        <form id="filter-form" action="{{ route('sales.master-data') }}" class="form-horizontal" method="post">
            {{ csrf_field() }}
            <div class="col-sm-2">
                <label class="label-control">Status</label>
                <select class="form-control input-sm" name="filter_status" data-live-search="true">
                    <option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- Any --</option>
                    <option value="Pending">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pending</option>
                    <option value="Converted">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Converted</option>
                    <option value="Closed">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Closed</option>
                    <option value="Dropped">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dropped / Case Loss</option>
                    <option value="No Supply">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Supply</option>
                </select>
            </div>
            <div class="col-sm-2">
                <label class="label-control">Period</label>
                <input type="text" id="period" class="form-control input-sm daterange" placeholder="Select Period"/>
                <input type="hidden" name="filter_period" />
            </div>
            <div class="col-sm-2 pull-right" style="margin-top: 1%">
                <input type="submit" class="btn btn-block btn-primary" value="Filter">
            </div>
            <div class="clearfix"></div>
        </form>
    </div>
@endsection

@section('report-content')
<div class="body" style="min-height:200px">
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-condensed reportTable display nowrap" cellspacing="0" style="width:100%;margin-bottom:30px !important;font-size: 14px !important;zoom:85%;">
            <thead>
                <tr>
                    <th>Date</th>
                    @if(session('orgType') == 'Aggregator')
                    <th>Provider</th>
                    @endif
                    <th>Episode ID</th>
                    <th>Patient ID</th>
                    <th>Customer Name</th>
                    <th>Contact Number</th>
                    <th>Gender</th>
                    <th>DOB</th>
                    <th>Age</th>
                    <th>Weight</th>
                    <th>Address</th>
                    <th>Area</th>
                    <th>City</th>
                    <th>Zipcode</th>
                    <th>State</th>
                    <th>Enquirer Name</th>
                    <th>Email</th>
                    <th>Case Description</th>
                    <th>Medical Conditions</th>
                    <th>Hospital Name</th>
                    <th>Primary Doctor Name</th>
                    <th>Special Instructions</th>
                    <th>Service Category</th>
                    <th>Service Required</th>
                    <th>Duration</th>
                    <th>Gender Preference</th>
                    <th>Language Preference</th>
                    <th>Assessment Date</th>
                    <th>Assessment Time</th>
                    <th>Assessment Notes</th>
                    <th>Rate Agreed</th>
                    <th>Registration Amount</th>
                    <th>Remarks</th>
                    <th>Payment Mode</th>
                    <th>Payment Notes</th>
                    <th>Referral Category</th>
                    <th>Referral Source</th>
                    <th>Referrer name</th>
                    <th>Converted By</th>
                    <th>Status</th>
                    <th>Dropped Reason</th>
                    <th>Comments</th>
                </tr>
            </thead>
            <tbody>
            @if(count($leads))
                @foreach ($leads as $l)
                    @if(count($l))
                    @foreach($l as $la)
                    <tr>
                        <td>{{ $la->created_at->format('d-m-Y') }}</td>
                        @if(session('orgType') == 'Aggregator')
                        <td>{{ $la->profile->organization_name }}</td>
                        @endif
                        <td>{{ $la->episode_id ?? 'TEMPLD0AF'.$la->id }}</td>
                        @if(session('orgType') == 'Aggregator')
                        <td>{{ isset($la->patient)?$la->patient->patient_id:'NA' }}</td>
                        @else
                        <td>{{ isset($la->patient)?$la->patient->patient_id:'TEMPPT0AF'.$la->patient->id }}</td>
                        @endif
                        <td>{{ $la->patient->full_name ?? '' }}</td>
                        <td>{{ $la->patient->contact_number ?? '' }}</td>
                        <td>{{ $la->patient->gender ?? '' }}</td>
                        <td>{{ $la->patient->date_of_birth?$la->patient->date_of_birth->format('d-m-Y'):'' }}</td>
                        <td>{{ $la->patient->patient_age ?? '' }}</td>
                        <td>{{ $la->patient->patient_weight ?? '' }}</td>
                        <td>{{ $la->patient->street_address ?? '' }}</td>
                        <td>{{ $la->patient->area ?? '' }}</td>
                        <td>{{ $la->patient->city ?? '' }}</td>
                        <td>{{ $la->patient->zipcode ?? '' }}</td>
                        <td>{{ $la->patient->state ?? '' }}</td>
                        <td>{{ $la->patient->enquirer_name ?? '' }}</td>
                        <td>{{ $la->patient->email ?? '' }}</td>
                        <td>{{ $la->case_description ?? '' }}</td>
                        <td>{{ $la->medical_conditions ?? '' }}</td>
                        <td>{{ $la->hospital_name ?? '' }}</td>
                        <td>{{ $la->primary_doctor_name ?? '' }}</td>
                        <td>{{ $la->special_instructions ?? '' }}</td>
                        <td>{{ $la->serviceCategory->category_name ?? '' }}</td>
                        <td>{{ $la->serviceRequired->service_name ?? '' }}</td>
                        <td>{{ $la->estimated_duration ?? '' }}</td>
                        <td>{{ $la->gender_preference ?? '' }}</td>
                        <td>{{ $la->language_preference ?? '' }}</td>
                        <td>{{ $la->assessment_date ?? '' }}</td>
                        <td>{{ $la->assessment_time ?? '' }}</td>
                        <td>{{ $la->assessment_notes ?? '' }}</td>
                        <td>{{ $la->rate_agreed ?? '' }}</td>
                        <td>{{ $la->registration_amount ?? '' }}</td>
                        <td>{{ $la->remarks ?? '' }}</td>
                        <td>{{ $la->payment_mode ?? '' }}</td>
                        <td>{{ $la->payment_notes ?? '' }}</td>
                        <td>{{ $la->referralCategory->category_name ?? '' }}</td>
                        <td>{{ $la->referralSource->source_name ?? '' }}</td>
                        <td>{{ $la->referrer_name ?? '' }}</td>
                        <td>{{ $la->convertedBy->full_name ?? '' }}</td>
                        <td>{{ $la->status ?? '' }}</td>
                        <td>{{ $la->dropped_reason ?? '' }}</td>
                        <td>@foreach($la->comments()->orderBy('created_at','asc')->get() as $lac){!! $lac->created_at->format('d-m-Y h:i a').' - '.$lac->comment."\r\n" !!}@endforeach</td>
                    </tr>
                    @endforeach
                    @endif
                @endforeach
            @else
                <tr><td colspan="40" class="text-center">No records</td></tr>
            @endif
            </tbody>
        </table>
    </div>
</div>
@endsection

@push('report.scripts')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
    <script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
    <script type="text/javascript" src="{{ ViewHelper::ThemeJs('daterangepicker.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<script>
    var rTable;
    $(document).ready(function() {
        $.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});

        @if(count($leads))
        $('.reportTable').DataTable({
            scrollY: 260,
            scrollX: true,
            scrollCollapse: true,
            lengthMenu: [[50,100,200,-1],[50,100,200,"All"]],
            language: {
                processing: "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate' style='color:#ff0000'> Loading....</span>"
            },
            order: [[0,'asc']],
            // displayLength: -1,
            dom: 'Bflrpt',
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: 'Leads-Master-Data_{{ date("d-m-Y_H:i:s") }}',
                    exportOptions: {
                        format: {
                            body: function ( data, row, column ) {
                                        // Strip $ from salary column to make it numeric
                                        return (column === 40)?'"'+data.replace( /<br\s*\/?>/ig, '\r\n' )+'"':data;
                                    }
                                }
                            }
                },
                {
                    extend: 'pdfHtml5',
                    pageSize: 'A3'
                },
                {
                    extend: 'print',
                    title: '<h3 style="text-align:center;">Leads Master Data</h3>',
                    customize: function ( win ) {
                        $(win.document.body).css( 'font-size', '7pt' );

                        $(win.document.body).find( 'table' ).addClass( 'compact' ).css( 'font-size', 'inherit' );
                    }
                }
            ],
        });
        @endif

        $('.daterange').daterangepicker({
            autoApply: true,
            autoUpdateInput: false,
            alwaysShowCalendars: true,
            locale: {
              format: 'DD-MM-YYYY',
              applyLabel: 'Apply',
              cancelLabel: 'Clear'
            },
            ranges: {
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'Last 30 Days': [moment().subtract(29, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
           }
         },
         function(start, end, label) {
             $('input[name=filter_period]').val(start.format('YYYY-MM-DD')+'_'+end.format('YYYY-MM-DD'));
             $('#period').val(start.format('DD-MM-YYYY')+' - '+end.format('DD-MM-YYYY'))
         }
        );

        $('#filter-form').on('submit', function(e) {
            if ( ! $.fn.DataTable.isDataTable('.reportTable') ) {
                getData();
            }else{
                rTable.draw();
            }
            e.preventDefault();
        });
    });
</script>
@endpush
