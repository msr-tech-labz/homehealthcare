@extends('layouts.main-layout')

@section('page_title','Conveyance - Sales Reports |')

@section('page.styles')
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">

<style>
    div.dataTables_wrapper div.dataTables_filter {
    text-align: center !important;
    }
    .dt-buttons {
        position: static !important;
        float: right;
        margin-top: 15px;
    }
    table.table-bordered thead tr th, table.table-bordered tbody tr td {
        text-transform: capitalize; !important;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        font-size: 12px !important;
    }
    .report-filter {
    width: 95%;
    margin: auto 0 !important;
    background: #f6f6f6;
    border-radius: 4px;
    border: 1px solid #ccc;
    padding: 10px;
    margin-top: 10px !important;
    margin-bottom: 10px !important;
    min-height: 50px;
    margin-left: 2.5% !important;
}
</style>
@endsection

@section('content')
<div class="card">
    <div class="header clearfix">
                <a href="{{ route('reports.index') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-10">
                   Conveyance
                   <small>Distance From Caregiver To Patient Location</small>
               </h2>
           </div>
    <div class="report-filter">
        <form id="filter-form" class="form-horizontal" method="post">
            <div class="col-sm-2">
                <label class="label-control">Month</label>
                <select class="form-control input-sm" name="filter_month">
                    <option value="" disabled="" selected="">-- Choose Month --</option>
                    <option value="01">Jan</option>
                    <option value="02">Feb</option>
                    <option value="03">Mar</option>
                    <option value="04">Apr</option>
                    <option value="05">May</option>
                    <option value="06">Jun</option>
                    <option value="07">Jul</option>
                    <option value="08">Aug</option>
                    <option value="09">Sep</option>
                    <option value="10">Oct</option>
                    <option value="11">Nov</option>
                    <option value="12">Dec</option>
               </select>
            </div>
            <div class="col-sm-2">
                <label class="label-control">Year</label>
                <select class="form-control input-sm" name="filter_year">
                    <option value="" disabled="" selected="">-- Choose Year --</option>
                    <option value="2021">2021</option>
                    <option value="2020">2020</option>
                    <option value="2019">2019</option>
                    <option value="2018">2018</option>
               </select>
            </div>
            <div class="col-sm-2 pull-right" style="margin-top: 1%">
                <input type="button" class="btn btn-block btn-primary btnFilter" value="Filter">
            </div>
            <div class="clearfix"></div>
        </form>
    </div>

    <div class="body" style="min-height:200px">
        <div class="table-responsive" style="overflow-x:scroll;">
            <table class="table table-bordered table-hover table-condensed reportTable" style="width:100%">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Episode Id</th>
                        <th>Designation</th>
                        <th>Patient Name</th>
                        <th>Service Offered</th>
                        <th>Employee Id</th>
                        <th>Employee Name</th>
                        <th>Chargable</th>
                        <th>Revenue</th>
                        <th>Patient Address</th>
                        <th>Staff Address</th>
                        <th>Delivery Address</th>
                        <th>Distance</th>
                    </tr>
                </thead>
                <tbody>
                    <tr><td colspan="12" class="text-center">Select Month and Year to filter</td></tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection


@section('page.scripts')
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
    <script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>

    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>

    <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>

<script>
    var rTable;
    $(document).ready(function() {
        $('.content').on('click', '.btnFilter',function(e) {
            if ( ! $.fn.DataTable.isDataTable('.reportTable') ) {
                getData();
            }else{
                rTable.draw();
            }
            e.preventDefault();
        });
    });

    function getData(){
        rTable = $('.reportTable').DataTable({
            processing: true,
            serverSide: true,
            lengthMenu: [[15,50,100,200,-1],[15,50,100,200,"All"]],
            language: {
                processing: "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate' style='color:#ff0000'> Loading....</span>"
            },
            ajax: {
                url: '{{ route('sales.conveyance','ajax=true') }}',
                type: 'POST',
                data: function (d) {
                    d._token = '{{ csrf_token() }}';
                    d.filter_month = $('select[name=filter_month] option:selected').val();
                    d.filter_year = $('select[name=filter_year] option:selected').val();
                }
            },
            // displayLength: -1,
            dom: 'Bflrpt',
            columns: [
                {data : 'date', value : 'date', searchable: true},
                {data : 'episode_id', value : 'episode_id', searchable: true},
                {data : 'designation', value : 'designation'},
                {data : 'patient_name', value : 'patient_name', searchable: true},
                {data : 'service_offered', value : 'service_offered', searchable: true},
                {data : 'employee_id', value : 'employee_id', searchable: true},
                {data : 'employee_name', value : 'employee_name', searchable: true},
                {data : 'chargeable', value : 'chargeable', searchable: true},
                {data : 'revenue', value : 'revenue'},
                {data : 'patient_address', value : 'patient_address'},
                {data : 'staff_address', value : 'staff_address'},
                {data : 'delivery_address', value : 'delivery_address'},
                {data : 'distance', value : 'distance', searchable: true},
            ],
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: 'Conveyance_{{ date("d-m-Y_H:i:s") }}'
                }
            ],
        });
    }
</script>
@endsection