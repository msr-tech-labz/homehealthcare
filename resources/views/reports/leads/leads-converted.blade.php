@extends('layouts.report-layout')

@section('page_title','Master Data - Leads | Reports')

@section('report-title','Leads Status Report')

@section('report-summary','Summary of Leads Status Report')

@section('report-filter')
    <style>
        table.table-bordered thead tr th, table.table-bordered tbody tr td {
            text-transform: capitalize; !important;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            font-size: 12px !important;
        }
    </style>
    <div class="report-filter">
        <form id="filter-form" class="form-horizontal" method="post">
        <div class="col-sm-2">
                <label class="label-control">Status</label>
                <select class="form-control input-sm" id="filter_status" name="filter_status" data-live-search="true">
                    <option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- Any --</option>
                    <option value="Converted">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Converted</option>
                    <option value="Closed">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Closed</option>
                    <option value="Dropped">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dropped</option>
                    <option value="No Supply">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Supply</option>
                </select>
            </div>
            <div class="col-sm-2">
                <label class="label-control">Period</label>
                <input type="text" id="period" class="form-control input-sm daterange" placeholder="Select Period"/>
                <input type="hidden" name="filter_period" />
            </div>
            <div class="col-sm-2 pull-right" style="margin-top: 1%">
                <input type="button" class="btn btn-block btn-primary btnFilter" value="Filter">
            </div>
            <div class="clearfix"></div>
        </form>
    </div>
@endsection

@section('report-content')
<div class="body table-responsive" style="min-height:200px">
    <table class="table table-bordered table-hover table-condensed reportTable" id="reportTable" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>Patient Id</th>
                <th>Patient Name</th>
                <th>Status Date</th>
                <th>Comment</th>
            </tr>
        </thead>
        <tbody>
            <tr><td colspan="13" class="text-center">Select Period to filter</td></tr>
        </tbody>
    </table>
</div>
@endsection

@push('report.scripts')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
    <script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
    <script type="text/javascript" src="{{ ViewHelper::ThemeJs('daterangepicker.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<script>
    var rTable;
    $(document).ready(function() {
        $('.daterange').daterangepicker({
            autoApply: true,
            autoUpdateInput: false,
            alwaysShowCalendars: true,
            locale: {
              format: 'DD-MM-YYYY',
              applyLabel: 'Apply',
              cancelLabel: 'Clear'
            },
            ranges: {
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'Last 30 Days': [moment().subtract(29, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
           }
         },
        function(start, end, label) {
             $('input[name=filter_period]').val(start.format('YYYY-MM-DD')+'_'+end.format('YYYY-MM-DD'));
             $('#period').val(start.format('DD-MM-YYYY')+' - '+end.format('DD-MM-YYYY'))
         }
        );

        $('.content').on('click', '.btnFilter',function(e) {
            if($('#filter_status').val() == '0'){
                alert("Please select a status filter!");
                return false;
            }
            if($('input[name=filter_period]').val() == ''){
                alert("Please select a period filter!");
                return false;          
            }
            if ( ! $.fn.DataTable.isDataTable('.reportTable') ) {
                getData();
            }else{
                rTable.draw();
            }
            e.preventDefault();
        });
    });

    function getData(){
        rTable = $('.reportTable').DataTable({
            processing: true,
            serverSide: true,
            lengthMenu: [[50,100,200,-1],[50,100,200,"All"]],
            language: {
                processing: "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate' id='span' style='color:#ff0000'> Loading....</span>"
            },
            ajax: {
                url: '{{ route('sales.leads-converted','ajax=true') }}',
                type: 'POST',
                data: function (d) {
                    d._token = '{{ csrf_token() }}';
                    d.status = $('select[name=filter_status] option:selected').val();
                    d.period = $('input[name=filter_period]').val();
                }
            },
            // displayLength: -1,
            dom: 'Bflrpt',
            columns: [
                {data : 'count', name : 'count'},
                {data : 'patient_id', name : 'patient_id', searchable: true},
                {data : 'patient_name', name : 'patient_name', searchable: true},
                {data : 'date', name : 'date', searchable: true},
                {data : 'comment', name : 'comment', searchable: true},
            ],

            drawCallback: function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last = null;
            },
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: 'Report-Invoice_report_{{ date("d-m-Y_H:i:s") }}'
                },
                {
                    extend: 'print',
                    title: '<h3 style="text-align:center;">Lead Status Report</h3>',
                    customize: function ( win ) {
                        $(win.document.body).css( 'font-size', '7pt' );

                        $(win.document.body).find( 'table' ).addClass( 'compact' ).css( 'font-size', 'inherit' );
                    }
                }
            ],
        });
    }
</script>
@endpush
