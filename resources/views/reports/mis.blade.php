@extends('layouts.main-layout')

@section('page_title','MIS Report - Reports')

@section('active_reports','active')

@section('page.styles')
<style>
     .switch label .lever{
         background-color: #84c7c1 !important;
     }
     .switch label .lever:after{
         background-color: #84c7c1 !important;
     }
     .chart-table tr td.c, .chart-table tr th.c{
         text-align: center !important;
     }
     td{
        white-space: nowrap;
     }
     .tfoot th{
        text-align: center;
     }
     .dataTables_wrapper .dt-buttons a.dt-button{
        background-color: #1dd6b1 !important;
        background-image: none !important;
        color:black !important;
     }
</style>
@endsection

@section('plugin.styles')
    @parent
    <!-- Morris Css -->
    <link href="{{ ViewHelper::ThemePlugin('morrisjs/morris.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection

@section('content')
<div class="block-header">
    <div class="col-sm-2">
        <h2>MIS Report</h2>
        <small class="col-teal"><i>As on {{ date('d-m-Y',strtotime("-1 days")) }}</i></small>
    </div>
    <div class="col-sm-10 text-right">
        <a class="btn bg-blue-grey tvMode hidden-print" style="float:right;margin-bottom: 20px">Toggle TV Mode (F6)</a>
    </div>
</div>
<code>NOTE: The YTD calculations are done according to the Financial Year i.e. 01-April-XXXX to the date selected.</code>
<div class="row clearfix">
    <!-- Revenue Stats Chart -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <div class="col-sm-3">
                    <h2>Revenue</h2>
                    <small class="revenue-period"></small>
                </div>                
                <div class="col-sm-4 text-right">                    
                    <div class="input-group" style="margin-top: 0;margin-bottom: 0">
                        <span class="input-group-addon">
                            <span style="font-weight: normal;margin-right: 10px;">Select Date</span>
                            <i class="material-icons">date_range</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control date" data-id="soa" style="background-color: #eee" placeholder="Select date">
                            <input type="hidden" name="revenue_period" value=""/>
                        </div>                        
                    </div>
                </div>
                <div class="col-md-1 text-left">
                    <a class="btn bg-teal btnFilterRevenue">Submit</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="body" style="overflow:scroll !important">
                <div class="col-lg-5">
                    <div id="revenue_table" class="revenue-table" style="height:300px;display: table-cell;vertical-align: middle;display:none">
                        <table class="table table-bordered chart-table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th class="c" style="background-color: rgb(255, 153, 51)">TODATE</th>
                                    <th class="c" style="background-color: rgb(56, 133, 157)">MTD</th>
                                    <th class="c" style="background-color: rgb(60, 179, 113)">YTD</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-lg-7 col-md-7">
                    <canvas id="revenue_chart" class="revenue-graph"></canvas>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    
    <!-- Lead Stats Chart -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <div class="col-sm-3">
                    <h2>Leads</h2>
                    <small class="leads-period"></small>
                </div>                
                <div class="col-sm-4 text-right">                    
                    <div class="input-group" style="margin-top: 0;margin-bottom: 0">
                        <span class="input-group-addon">
                            <span style="font-weight: normal;margin-right: 10px;">Select Date</span>
                            <i class="material-icons">date_range</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control date" data-id="lead" style="background-color: #eee" placeholder="Select date">
                            <input type="hidden" name="leads_period" value=""/>
                        </div>                        
                    </div>
                </div>
                <div class="col-md-1 text-left">
                    <a class="btn bg-teal btnFilterLead">Submit</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="body">
                <div class="col-lg-4">
                    <div id="lead_stats_table" class="lead-table" style="height:300px;display: table-cell;vertical-align: middle;display: none">
                        <table class="table table-bordered chart-table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th class="c" style="width:20%; background-color: rgb(255, 153, 51)">TODATE</th>
                                    <th class="c" style="background-color: rgb(56, 133, 157)">MTD</th>
                                    <th class="c" style="background-color: rgb(60, 179, 113)">YTD</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-lg-8 col-md-6">
                    <canvas id="lead_trend_chart" class="services-graph"></canvas>
                </div>
                <div class="clearfix"></div><br>
            </div>
        </div>
    </div>
    
    <!-- HR Stats Chart -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <div class="col-sm-3">
                    <h2>Human Resource</h2>
                    <small class="hr-period"></small>
                </div>                
                <div class="col-sm-4 text-right">                    
                    <div class="input-group" style="margin-top: 0;margin-bottom: 0">
                        <span class="input-group-addon">
                            <span style="font-weight: normal;margin-right: 10px;">Select Date</span>
                            <i class="material-icons">date_range</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control date" data-id="soa" style="background-color: #eee" placeholder="Select date">
                            <input type="hidden" name="hr_period" value=""/>
                        </div>                        
                    </div>
                </div>
                <div class="col-md-1 text-left">
                    <a class="btn bg-teal btnFilterHr">Submit</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="body" style="height:380px !important">
                <div class="col-lg-6 col-md-6">
                    <div id="hr_stats_table" class="hr-table">
                        <table class="table table-bordered chart-table">
                            <tr style="background-color: #f6f6f6">
                                <th class="text-center">Total Staff</th>
                                <th class="text-center"></th>
                            </tr>
                            <tr>
                                <th>Available</th>
                                <td class="c"></td>
                            </tr>
                            <tr>
                                <th>On Duty</th>
                                <td class="c"></td>
                            </tr>
                            <tr>
                                <th>Training</th>
                                <td class="c"></td>
                            </tr>
                            <tr>
                                <th>Leave</th>
                                <td class="c"></td>
                            </tr>
                            <tr>
                                <th>Absent</th>
                                <td class="c"></td>
                            </tr>
                        </table>
                        <code>Total number of staffs shown belongs to the designation of category selected from Masters. </code>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div id="hr_stats_chart" class="hr-graph" style="height:250px !important"></div>
                </div>
                <div class="col-lg-12 col-md-12" id="hr_stats_legend" style="text-align: right;">
                </div>
            </div>
        </div>
    </div>
    
    <!-- Services Stats Chart -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <div class="col-sm-3">
                    <h2>Services</h2>
                    <small class="service-period"></small>
                </div>
                <div class="col-sm-4 text-right">                    
                    <div class="input-group" style="margin-top: 0;margin-bottom: 0">
                        <span class="input-group-addon">
                            <span style="font-weight: normal;margin-right: 10px;">Select Date</span>
                            <i class="material-icons">date_range</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control date" data-id="soa" style="background-color: #eee" placeholder="Select date">
                            <input type="hidden" name="service_period" value=""/>
                        </div>                        
                    </div>
                </div>
                <div class="col-md-1 text-left">
                    <a class="btn bg-teal btnFilterService">Submit</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="body">
                <div class="col-lg-12 col-md-12">
                    <div id="services_table" class="services-table" style="display: table-cell;vertical-align: middle;display:none;overflow-x: auto;">
                        <table class="table table-bordered chart-table">
                            <thead>
                                <tr>
                                    <th rowspan="2" class="c" style="vertical-align: middle;">Service Name</th>
                                    <th colspan="3" class="c" style="background-color: #ffb3ba;">Today</th>
                                    <th colspan="3" class="c" style="background-color: #baffc9;">MTD</th>
                                    <th colspan="3" class="c" style="background-color: #bae1ff;">YTD</th>
                                </tr>
                                <tr>
                                    <th class="for_today" style="background-color: #ffb3ba;">Shifts</th>
                                    <th class="for_today" style="background-color: #ffb3ba;">Revenue</th>
                                    <th class="for_today" style="background-color: #ffb3ba;">Avg Revenue</th>
                                    <th class="for_mtd" style="background-color: #baffc9;">Shifts</th>
                                    <th class="for_mtd" style="background-color: #baffc9;">Revenue</th>
                                    <th class="for_mtd" style="background-color: #baffc9;">Avg Revenue</th>
                                    <th class="for_ytd" style="background-color: #bae1ff;">Shifts</th>
                                    <th class="for_ytd" style="background-color: #bae1ff;">Revenue</th>
                                    <th class="for_ytd" style="background-color: #bae1ff;">Avg Revenue</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr class="tfoot" style="background-color: #c4dfc3;">
                                    <th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th>
                                </tr>
                            </tfoot>
                        </table>
                        <div class="row clearfix">
                        <p class="text-center" style="margin-top: 30px;"><b>MANUAL CREDIT AMOUNT</b></p>
                        <table class="creditmemo col-md-12 text-center">
                            <thead>
                                <tr class="text-align:center;">
                                    <th></th>
                                    <th style="background-color: #f6c9ee;text-align: center;">TODATE</th>
                                    <th style="background-color: #f6c9ee;text-align: center;">MTD</th>
                                    <th style="background-color: #f6c9ee;text-align: center;">YTD</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="text-align:center;" style="border-bottom: 3px solid #aabcfe;">
                                    <td>Manual Credits Raised</td>
                                    <td style="background-color: #f6c9ee;width: 22%;"></td>
                                    <td style="background-color: #f6c9ee;width: 22%;"></td>
                                    <td style="background-color: #f6c9ee;width: 22%;"></td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-md-7">
                    <canvas id="services_chart" class="services-graph"></canvas>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>    
</div>
@endsection

@section('page.scripts')
    {{-- @parent --}}
    <!-- Include Date Range Picker -->
    <script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
    <script type="text/javascript" src="{{ ViewHelper::ThemeJs('daterangepicker.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <!-- Morris Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('raphael/raphael.min.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('morrisjs/morris.js') }}"></script>
    <!-- Chart Plugins Js -->
    <script src="{{ ViewHelper::ThemePlugin('chartjs/Chart.bundle.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chartjs-plugin-zoom/0.5.0/chartjs-plugin-zoom.min.js"></script>
    <script src="{{ ViewHelper::js('leads/form.js') }}"></script>
    <!--Datatable Plugings -->
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
    <script>
        var originalSize = "";
        var leadChart;
        var revenueChart;
        var serviceChart;
        var hrDonut;
        var isRevenueRendering = false;
        var isLeadsRendering = false;
        var isServicesRendering = false;
        var isHrRendering = false;

        $(function(){
            var dateRangePickerConfig = {
                autoApply: true,
                autoUpdateInput: false,
                alwaysShowCalendars: true,
                locale: {
                  format: 'DD-MM-YYYY',
                  applyLabel: 'Apply',
                  cancelLabel: 'Clear'
                },
                singleDatePicker: true,
                showDropdowns: true,
            };
            
            $('.date').daterangepicker(dateRangePickerConfig, function(start, end, label) {
                $('input[name=revenue_period]').val(start.format('YYYY-MM-DD'));
                $('input[name=leads_period]').val(start.format('YYYY-MM-DD'));
                $('input[name=service_period]').val(start.format('YYYY-MM-DD'));
                $('input[name=hr_period]').val(start.format('YYYY-MM-DD'));
            });

            $('.date').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY'));
            });
            
            $('.content').on('click','.btnFilterRevenue', function (){
                if(!isRevenueRendering){
                    revenueStatsChart();                    
                }else{
                    alert("Please wait..Fetching revenue details in progress!");
                    return false;
                }
            });

            $('.content').on('click','.btnFilterLead', function (){
                if(!isLeadsRendering){
                    leadStatsChart();                    
                }else{
                    alert("Please wait..Fetching leads details in progress!");
                    return false;
                }
            });

            $('.content').on('click','.btnFilterService', function (){
                if(!isServicesRendering){
                    servicesStatsChart();                    
                }else{
                    alert("Please wait..Fetching service details in progress!");
                    return false;
                }
            });

            $('.content').on('click','.btnFilterHr', function (){
                if(!isHrRendering){
                    hrStatsChart();                    
                }else{
                    alert("Please wait..Fetching HR details in progress!");
                    return false;
                }
            });
            
            function fullscreen() {
                var isInFullScreen = (document.fullscreenElement && document.fullscreenElement !== null) ||
                (document.webkitFullscreenElement && document.webkitFullscreenElement !== null) ||
                (document.mozFullScreenElement && document.mozFullScreenElement !== null) ||
                (document.msFullscreenElement && document.msFullscreenElement !== null);
                
                var docElm = document.documentElement;
                if (!isInFullScreen) {
                    if (docElm.requestFullscreen) {
                        docElm.requestFullscreen();
                    } else if (docElm.mozRequestFullScreen) {
                        docElm.mozRequestFullScreen();
                    } else if (docElm.webkitRequestFullScreen) {
                        docElm.webkitRequestFullScreen();
                    } else if (docElm.msRequestFullscreen) {
                        docElm.msRequestFullscreen();
                    }
                    $('section.content').attr('style',"margin: 40px 15px 0;");
                    $('nav.navbar').hide();
                } else {
                    if (document.exitFullscreen) {
                        document.exitFullscreen();
                    } else if (document.webkitExitFullscreen) {
                        document.webkitExitFullscreen();
                    } else if (document.mozCancelFullScreen) {
                        document.mozCancelFullScreen();
                    } else if (document.msExitFullscreen) {
                        document.msExitFullscreen();
                    }
                    $('section.content').attr('style',"margin: 100px 15px 0;");
                    $('nav.navbar').show();
                }
            }
            
            if (document.addEventListener)
            {
                document.addEventListener('webkitfullscreenchange', exitHandler, false);
                document.addEventListener('mozfullscreenchange', exitHandler, false);
                document.addEventListener('fullscreenchange', exitHandler, false);
                document.addEventListener('MSFullscreenChange', exitHandler, false);
            }
            
            function exitHandler()
            {                
                //if (document.webkitIsFullScreen || document.mozFullScreen || document.msFullscreenElement !== false)
                if (!window.screenTop && !window.screenY) {
                    $('section.content').attr('style',"margin: 100px 15px 0;");
                    $('nav.navbar').show();
                }else{
                    $('section.content').attr('style',"margin: 40px 15px 0;");
                    $('nav.navbar').hide();
                }
            }
                  
            $('.content').on('click', '.tvMode', function(){
                fullscreen();
            });            
            
            $("body").keydown(function(e){
                var keyCode = e.keyCode || e.which;
                if(keyCode == 117){
                    e.preventDefault();
                    fullscreen();
                }
                
                if(keyCode == 27){
                    e.preventDefault();
                    fullscreen();
                }
            });
            
            leadStatsChart();
            hrStatsChart();
            servicesStatsChart();
            revenueStatsChart();
        });
        
        function showLoader(block){
            html = `<div class="loading-img" style="display: block;text-align:center">
                <img src="{{ asset('img/loading.gif') }}" style="width:9%;margin: auto 0 !important;"/>
            </div>`;
            $('#'+block).parent().parent().prepend(html);
        }
        
        function formatMoney(data){
            return Number(parseFloat(data)).toLocaleString('en-IN', { style: 'currency', currency: 'INR'});
        }

        function leadStatsChart(){
            isLeadsRendering = true;
            showLoader('lead_trend_chart');
            var date = $('input[name=leads_period]').val();
            $.ajax({
                url: '{{ route('report.mis-report') }}',                
                type: 'POST',
                data: {_token: '{{ csrf_token() }}', graph: 'lead' , date: date},
                success: function (d){
                    $('#lead_trend_chart').parent().parent().find('.loading-img').remove();
                    if(d.result && d.data){
                        if(d.data.graph){
                            if(leadChart){
                                leadChart.destroy();
                            }
                            leadChart = new Chart(document.getElementById("lead_trend_chart").getContext("2d"), {
                                type: 'bar',
                                data: {
                                    labels: d.data.graph.labels,
                                    datasets: d.data.graph.datasets,
                                },
                                options: {
                                    responsive: true,
                                    legend: {
                                        display: true,
                                        position: 'top'
                                    },
                                    scales: {
                                        yAxes: [{
                                            scaleLabel: {
                                                display: true,
                                                labelString: 'Count'
                                            }
                                        }]
                                    }
                                },
                                plugins: ['zeroCompensation'],
                            });
                        }
                        
                        if(d.data){
                            _row = d.data.graph.datasets;
                            html = `
                            <tr>
                                <th>Pending</th>
                                <td style="text-align:center;">`+ _row[0]['data'][0] +`</td>
                                <td style="text-align:center;">`+ _row[1]['data'][0] +`</td>
                                <td style="text-align:center;">`+ _row[2]['data'][0] +`</td>
                            </tr>
                            <tr>
                                <th>Converted</th>
                                <td style="text-align:center;">`+ _row[0]['data'][1] +`</td>
                                <td style="text-align:center;">`+ _row[1]['data'][1] +`</td>
                                <td style="text-align:center;">`+ _row[2]['data'][1] +`</td>
                            </tr>
                            <tr>
                                <th>Closed</th>
                                <td style="text-align:center;">`+ _row[0]['data'][2] +`</td>
                                <td style="text-align:center;">`+ _row[1]['data'][2] +`</td>
                                <td style="text-align:center;">`+ _row[2]['data'][2] +`</td>
                            </tr>
                            <tr>
                                <th>Dropped</th>
                                <td style="text-align:center;">`+ _row[0]['data'][3] +`</td>
                                <td style="text-align:center;">`+ _row[1]['data'][3] +`</td>
                                <td style="text-align:center;">`+ _row[2]['data'][3] +`</td>
                            </tr>`;
                            
                            $('#lead_stats_table .chart-table tbody').html(html);
                            $('#lead_stats_table').show();
                        }                        
                        
                        isLeadsRendering = false;   
                        if(date == ''){
                            $('.leads-period').html('As on '+moment().subtract(1, 'day').format('DD-MM-YYYY'));
                        }else if(date != ''){
                            $('.leads-period').html('As on '+moment(date).format('DD-MM-YYYY'));
                        }
                    }
                    isLeadsRendering = false;
                },
                error: function (err){
                    isLeadsRendering = false;
                }
            });
        }
        
        function hrStatsChart(){
            isHrRendering = true;
            showLoader('hr_stats_chart');
            $('#hr_stats_table code').hide();
            $('#hr_stats_table th:not(:nth-child(1),:nth-child(2)), #hr_stats_table td:not(:nth-child(1),:nth-child(2))').remove();
            $('#hr_stats_table .table').hide();
            $('#hr_stats_legend').hide();
            $('#hr_stats_legend').html('');
            $('#hr_stats_chart').empty();
            var date = $('input[name=leads_period]').val();

            $('#hr_stats_table .chart-table tbody tr td').html('');
            $.ajax({
                url: '{{ route('report.mis-report') }}',
                type: 'POST',
                data: {_token: '{{ csrf_token() }}', graph: 'hr', date: date },
                success: function (d){
                    $('#hr_stats_chart').parent().parent().find('.loading-img').remove();
                    if(d.result && d.data){
                         hrDonut = Morris.Donut({
                            element: 'hr_stats_chart',
                            resize: true,
                            data: [
                            {
                                label: 'Available',
                                value: d.data.available
                            },{
                                label: 'On Duty',
                                value: d.data.onduty
                            },{
                                label: 'Training',
                                value: d.data.training
                            },{
                                label: 'Leave',
                                value: d.data.leave
                            },{
                                label: 'Absent',
                                value: d.data.absent
                            }],
                            colors: ['rgb(139, 195, 74)', 'rgb(0, 150, 136)', 'rgb(0, 188, 212)', 'rgb(255, 152, 0)', 'rgba(239, 52, 52, 0.85)'],
                            formatter: function (y) {
                                if(y != 0){
                                    return y + '(' + Math.round(y/d.data.total*100) + '%)'
                                }else{
                                    return '0';
                                }
                            },
                        });
                        
                        hrDonut.options.data.forEach(function(label, i) {
                            var legendItem = $('<span></span>').text( label['label'] + " ( " +label['value'] + " )" ).prepend('<span>&nbsp;</span>');
                            legendItem.find('span')
                            .css('backgroundColor', hrDonut.options.colors[i])
                            .css('width', '20px')
                            .css('display', 'inline-block')
                            .css('margin', '5px');
                            $('#hr_stats_legend').append(legendItem)
                        });
                    }    
                    
                    // Table data
                    $('#hr_stats_table .table tr:eq(0) th:eq(1)').html(d.data.total);
                    $('#hr_stats_table .table tr:eq(1) td:eq(0)').html(parseInt(d.data.available));
                    // $('#hr_stats_table .table tr:eq(1) td:eq(1)').html(parseFloat(d.data.available/d.data.total*100).toFixed(2)+' %');
                    $('#hr_stats_table .table tr:eq(2) td:eq(0)').html(parseInt(d.data.onduty));
                    $('#hr_stats_table .table tr:eq(3) td:eq(0)').html(parseInt(d.data.training));
                    $('#hr_stats_table .table tr:eq(4) td:eq(0)').html(parseInt(d.data.leave));
                    $('#hr_stats_table .table tr:eq(5) td:eq(0)').html(parseInt(d.data.absent));

                    $.each(d.data.cadre, function (i) {
                        if(i != ''){
                            $('#hr_stats_table .table tr:eq(0)').append('<th class="text-center">'+i+'</th>');
                            $.each(d.data.cadre[i], function (key, val) {
                                $('#hr_stats_table .table tr:eq('+key+')').append('<td class="text-center">'+val+'</td');
                            });
                        }
                    });

                    $('#hr_stats_table .table').show();
                    $('#hr_stats_table code').show();
                    $('#hr_stats_legend').show();
                    
                    isHrRendering = false;
                    if(date == ''){
                        $('.hr-period').html('As on '+moment().subtract(1, 'day').format('DD-MM-YYYY'));
                    }else if(date != ''){
                        $('.hr-period').html('As on '+moment(date).format('DD-MM-YYYY'));
                    }
                },
                error: function (err){
                    $('#hr_stats_chart').parent().parent().find('.loading-img').remove();
                    isHrRendering = false;
                }
            });
        }
        
        function servicesStatsChart(){
            isServicesRendering = true;
            showLoader('services_chart');
            var date = $('input[name=service_period]').val();
            $('.creditmemo tbody tr td:not(:first-child)').html('');
            if($.fn.DataTable.isDataTable('#services_table .chart-table')){
                $('#services_table .chart-table').dataTable().fnDestroy();
                $('#services_table .chart-table tbody').html('');
                $('#services_table .chart-table tfoot tr th:not(:first-child)').html('');
            }
            $.ajax({
                url: '{{ route('report.mis-report') }}',
                type: 'POST',
                data: {_token: '{{ csrf_token() }}', graph: 'services' , date: date },
                success: function (d){
                    $('#services_chart').parent().parent().find('.loading-img').remove();
                    if(d.result && d.data){
                        if(d.data.table){
                            html = ``;
                            $(d.data.table).each(function(i){
                                _row = d.data.table[i];
                                html += `<tr>
                                    <td>`+ _row['service'] +`</td>
                                    <td class="c" style="background-color: #ffb3ba">`+ _row['todayShifts'] +`</td>
                                    <td class="c" style="background-color: #ffb3ba">`+ _row['todayRevenue'] +`</td>
                                    <td class="c" style="background-color: #ffb3ba">`+ _row['todayAverageRevenue'] +`</td>

                                    <td class="c" style="background-color: #baffc9">`+ _row['mtdShifts'] +`</td>
                                    <td class="c" style="background-color: #baffc9">`+ _row['mtdRevenue'] +`</td>
                                    <td class="c" style="background-color: #baffc9">`+ _row['mtdAverageRevenue'] +`</td>

                                    <td class="c" style="background-color: #bae1ff">`+ _row['ytdShifts'] +`</td>
                                    <td class="c" style="background-color: #bae1ff">`+ _row['ytdRevenue'] +`</td>
                                    <td class="c" style="background-color: #bae1ff">`+ _row['ytdAverageRevenue'] +`</td>

                                </tr>`;
                            });
                            html += ``;
                            
                            $('#services_table .chart-table tbody').html(html);
                            $('#services_table').show();
                            $('#services_table .chart-table').dataTable({
                                "paging": false,
                                "footerCallback": function ( row, data, start, end, display ) {
                                    var api = this.api(), data;
                                    // converting to interger to find total
                                    var intVal = function ( i ) {
                                        return typeof i === 'string' ?
                                        i.replace(/[\$,]/g, '')*1 :
                                        typeof i === 'number' ?
                                        i : 0;
                                    };

                                    // computing column Total of the complete result 
                                    var todayShiftsTotal = api
                                    .column( 1 )
                                    .data()
                                    .reduce( function (a, b) {
                                        return intVal(a) + intVal(b);
                                    }, 0 );

                                    var todayRevenueTotal = api
                                    .column( 2 )
                                    .data()
                                    .reduce( function (a, b) {
                                        return (intVal(a) + intVal(b)).toFixed(2);
                                    }, 0 );

                                    var todayAverageRevenueTotal = api
                                    .column( 3 )
                                    .data()
                                    .reduce( function (a, b) {
                                        return (intVal(a) + intVal(b)).toFixed(2);
                                    }, 0 );

                                    var mtdShiftsTotal = api
                                    .column( 4 )
                                    .data()
                                    .reduce( function (a, b) {
                                        return intVal(a) + intVal(b);
                                    }, 0 );

                                    var mtdRevenueTotal = api
                                    .column( 5 )
                                    .data()
                                    .reduce( function (a, b) {
                                        return (intVal(a) + intVal(b)).toFixed(2);
                                    }, 0 );

                                    var mtdAverageRevenueTotal = api
                                    .column( 6 )
                                    .data()
                                    .reduce( function (a, b) {
                                        return (intVal(a) + intVal(b)).toFixed(2);
                                    }, 0 );

                                    var ytdShiftsTotal = api
                                    .column( 7 )
                                    .data()
                                    .reduce( function (a, b) {
                                        return intVal(a) + intVal(b);
                                    }, 0 );

                                    var ytdRevenueTotal = api
                                    .column( 8 )
                                    .data()
                                    .reduce( function (a, b) {
                                        return (intVal(a) + intVal(b)).toFixed(2);
                                    }, 0 );

                                    var ytdAverageRevenueTotal = api
                                    .column( 9 )
                                    .data()
                                    .reduce( function (a, b) {
                                        return (intVal(a) + intVal(b)).toFixed(2);
                                    }, 0 );
                                    

                                    // Update footer by showing the total with the reference of the column index 
                                    $( api.column( 0 ).footer() ).html('Total');
                                    $( api.column( 1 ).footer() ).html(todayShiftsTotal);
                                    $( api.column( 2 ).footer() ).html(todayRevenueTotal);
                                    $( api.column( 3 ).footer() ).html(todayAverageRevenueTotal);
                                    $( api.column( 4 ).footer() ).html(mtdShiftsTotal);
                                    $( api.column( 5 ).footer() ).html(mtdRevenueTotal);
                                    $( api.column( 6 ).footer() ).html(mtdAverageRevenueTotal);
                                    $( api.column( 7 ).footer() ).html(ytdShiftsTotal);
                                    $( api.column( 8 ).footer() ).html(ytdRevenueTotal);
                                    $( api.column( 9 ).footer() ).html(ytdAverageRevenueTotal);
                                },
                                dom: 'Bflrpt',
                                buttons: [
                                    {
                                        extend: 'excelHtml5',
                                        footer: true,
                                        text: 'Export',
                                        title: 'MIS-Service-Revenue-Data_{{ date("d-m-Y_H:i:s") }}',
                                  }
                                ],
                            });

                            if(date == ''){
                                $('.service-period').html('As on '+moment().subtract(1, 'day').format('DD-MM-YYYY'));
                            }else if(date != ''){
                                $('.service-period').html('As on '+moment(date).format('DD-MM-YYYY'));
                            }
                        }
                        if(d.data.creditTable[0]){
                            _rowCM = d.data.creditTable[0];
                            $('.creditmemo tbody tr td:eq(1)').html(_rowCM['todate']);
                            $('.creditmemo tbody tr td:eq(2)').html(_rowCM['mtd']);
                            $('.creditmemo tbody tr td:eq(3)').html(_rowCM['ytd']);
                            $('#services_table').show();
                        }
                    }
                    isServicesRendering = false;
                },
                error: function (err){
                    isServicesRendering = false;
                }
            });            
        }
        
        function revenueStatsChart(){
            isRevenueRendering = true;
            showLoader('revenue_chart');
            var date = $('input[name=revenue_period]').val();
            $.ajax({
                url: '{{ route('report.mis-report') }}',                
                type: 'POST',
                data: {_token: '{{ csrf_token() }}', graph: 'revenue' , date: date},
                success: function (d){
                    $('#revenue_chart').parent().parent().find('.loading-img').remove();
                    if(d.result && d.data){
                        if(d.data.graph){
                            if(revenueChart){
                                revenueChart.destroy();
                            }
                            revenueChart = new Chart(document.getElementById("revenue_chart").getContext("2d"), {
                                type: 'bar',
                                data: {
                                    labels: d.data.graph.labels,
                                    datasets: d.data.graph.datasets,
                                },
                                options: {
                                    responsive: true,
                                    legend: {
                                        display: true,
                                        position: 'top'
                                    },
                                    scales: {
                                        yAxes: [{
                                            scaleLabel: {
                                                display: true,
                                                labelString: 'Amount'
                                            }
                                        }]
                                    }
                                },
                                plugins: ['zeroCompensation'],
                            });
                        }
                        
                        if(d.data){
                            _row = d.data.graph.datasets;
                            html = `
                            <tr>
                                <th>Gross</th>
                                <td>`+ formatMoney(_row[0]['data'][0]) +`</td>
                                <td>`+ formatMoney(_row[1]['data'][0]) +`</td>
                                <td>`+ formatMoney(_row[2]['data'][0]) +`</td>
                            </tr>
                            <tr>
                                <th>Discount</th>
                                <td>`+ formatMoney(_row[0]['data'][1]) +`</td>
                                <td>`+ formatMoney(_row[1]['data'][1]) +`</td>
                                <td>`+ formatMoney(_row[2]['data'][1]) +`</td>
                            </tr>
                            <tr>
                                <th>Revenue</th>
                                <td>`+ formatMoney(_row[0]['data'][2]) +`</td>
                                <td>`+ formatMoney(_row[1]['data'][2]) +`</td>
                                <td>`+ formatMoney(_row[2]['data'][2]) +`</td>
                            </tr>
                            <tr>
                                <th>Taxed</th>
                                <td>`+ formatMoney(_row[0]['data'][3]) +`</td>
                                <td>`+ formatMoney(_row[1]['data'][3]) +`</td>
                                <td>`+ formatMoney(_row[2]['data'][3]) +`</td>
                            </tr>
                            <tr>
                                <th>Manual Credit Raised</th>
                                <td>`+ formatMoney(_row[0]['data'][4]) +`</td>
                                <td>`+ formatMoney(_row[1]['data'][4]) +`</td>
                                <td>`+ formatMoney(_row[2]['data'][4]) +`</td>
                            </tr>`;
                            
                            $('#revenue_table .chart-table tbody').html(html);
                            $('#revenue_table').show();
                        }                        
                        
                        if(date == ''){
                            $('.revenue-period').html('As on '+moment().subtract(1,'day').format('DD-MM-YYYY'));
                        }else if(date != ''){
                            $('.revenue-period').html('As on '+moment(date).format('DD-MM-YYYY'));
                        }
                    }
                    isRevenueRendering = false;
                },
                error: function (err){
                    isRevenueRendering = false;
                }
            });            
        }
        
        const zeroCompensation = {
          renderZeroCompensation: function (chartInstance, d) {
              // get postion info from _view
              const view = d._view
              const context = chartInstance.chart.ctx

              // the view.x is the centeral point of the bar, so we need minus half width of the bar.
              const startX = view.x - view.width / 2
              // common canvas API, Check it out on MDN
              context.beginPath();
              // set line color, you can do more custom settings here.
              context.strokeStyle = '#aaaaaa';
              context.moveTo(startX, view.y);
              // draw the line!
              context.lineTo(startX + view.width, view.y);
              // bam！ you will see the lines.
              context.stroke();
          },

          afterDatasetsDraw: function (chart, easing) {
              // get data meta, we need the location info in _view property.
              const meta = chart.getDatasetMeta(0)
              // also you need get datasets to find which item is 0.
              const dataSet = chart.config.data.datasets[0].data
              meta.data.forEach((d, index) => {                  
                  // for the item which value is 0, reander a line.
                  if(dataSet[index] === 0) {
                    this.renderZeroCompensation(chart, d)
                  }
              })
          }
        };
        Chart.plugins.register(zeroCompensation);
    </script>
@endsection