@extends('layouts.main-layout')

@section('page_title','Cases - Monthly Report')

@section('active_monthly_report','active')

@section('page.styles')
<style>
    .card{
        overflow-x: scroll;
    }
    .table-bordered tbody tr td, .table-bordered tbody tr th {
        text-align: center;
    }
    .dt-buttons{
        position: static !important;
        float: right;
    }
    .paginate_button {
        cursor: pointer;
        margin:2px;
    }
    div.dataTables_wrapper div.dataTables_filter {
        text-align: center !important;
    }
    table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td {
      text-transform: uppercase; !important;
      overflow: hidden;
      text-overflow: ellipsis;
      white-space: nowrap;
    }
</style>
<!-- JQuery DataTable Css -->
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection

@section('plugin.styles')
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<style>
</style>
@endsection

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <h2 class="col-sm-9">
                   Resource Stats
                   <small>Resource Stats of all providers</small>
               </h2>
           </div>
           <div class="body">
               <div class="row clearfix">
                   <form action="{{ route('reports.resource-stats') }}" method="post">
                       {{ csrf_field() }}
                       <table class="table table-bordered">
                           <tr>
                                @if(\Helper::encryptor('decrypt',session('utype')) == 'Aggregator')
                               <th width="20%">
                                   <select class="form-control" name="filter_provider">
                                       <option value="">-- Any Provider --</option>
                                @if(isset($providers))
                                   @foreach($providers as $p)
                                        <option style="text-transform: uppercase;" value="{{ $p->tenant_id }}" @if($provider_id == $p->id){{'selected'}}@endif>{{ $p->organization_name }}</option>
                                   @endforeach
                                @endif
                                   </select>
                               </th>
                               @endif                               
                               <th width="15%">
                                   <input type="submit" class="btn btn-success">
                               </th>
                           </tr>
                       </table>
                   </form>
               </div>
            <table id="resourceReport" class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Photo</th>
                        <th>Caregiver Name</th>
                        <th>Gender</th>
                        <th>Mobile</th>
                        <th>Email</th>
                        <th>Specialization</th>
                        <th>Experience <br></th>
                        <th>Work Status</th>
                        <th>Created On</th>
                        <th>Updated On</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($caregivers as $i => $c)
                    <tr>
                        <td>{{ $i + 1 }}</td>
                        <td>
                            <div style="display: inline-block; vertical-align: top;">
                                <img src="@if($c->profile_image && file_exists(public_path('uploads/caregivers/'.$c->profile_image))){{ asset('uploads/caregivers/'.$c->profile_image)}}@else{{ asset('/img/user.png') }}@endif" width="40" height="40" class="img-circle" alt="{{ $c->first_name }}" class="img-responsive img-thumbnail pull-left" title="{{ $c->first_name }}" />
                            </div>
                        </td>
                        <td>{!! $c->first_name.'<br><small>'.$c->last_name.'</small>' !!}</td>
                        <td>{{ $c->gender }}</td>
                        <td>{!! $c->mobile_number !!}</td>
                        <td>{!! $c->email !!}</td>
                        <td>{{ $c->professional->specializations->specialization_name ?? '-' }}</td>
                        <td>{{ $c->professional->experience ?? '0' }} yr(s)</td>
                        <td>{{ $c->work_status }}</td>
                        <td>{{ $c->created_at->format('d M Y h:i A') }}</td>
                        <td>{{ $c->updated_at?$c->updated_at->format('d M Y h:i A'):'' }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
@endsection


@section('page.scripts')
<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
{{-- <script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.13/filtering/row-based/range_dates.js"></script> --}}
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
<!-- Moment Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<script>
    $(document).ready(function() {
        $('#resourceReport').DataTable( {
            "lengthMenu": [[50, 100, 200, -1], [50, 100, 200, "All"]],
            dom: 'Bflrtip',
            buttons: [
            'excel',
            {
                extend: 'pdfHtml5',
                pageSize: 'A3'
            },
            {
                extend: 'print',
                title: '<h3 style="text-align:center;">Lead Request Roster</h3>',
                customize: function ( win ) {
                    $(win.document.body)
                    .css( 'font-size', '7pt' );

                    $(win.document.body).find( 'table' )
                    .addClass( 'compact' )
                    .css( 'font-size', 'inherit' );
                }
            }
            ],
            "order": [[ 0, "desc" ]]
        } );
    } );

    /* Custom filtering function which will search data in column four between two values */
    $.fn.dataTableExt.afnFiltering.push(
        function( oSettings, aData, iDataIndex ) {
            var iFini = document.getElementById('min').value;
            var iFfin = document.getElementById('max').value;
            var iStartDateCol = 1;
            var iEndDateCol = 1;

            iFini=iFini.substring(6,10) + iFini.substring(3,5)+ iFini.substring(0,2);
            iFfin=iFfin.substring(6,10) + iFfin.substring(3,5)+ iFfin.substring(0,2);

            var datofini=aData[iStartDateCol].substring(6,10) + aData[iStartDateCol].substring(3,5)+ aData[iStartDateCol].substring(0,2);
            var datoffin=aData[iEndDateCol].substring(6,10) + aData[iEndDateCol].substring(3,5)+ aData[iEndDateCol].substring(0,2);

            if ( iFini === "" && iFfin === "" )
            {
                return true;
            }
            else if ( iFini <= datofini && iFfin === "")
            {
                return true;
            }
            else if ( iFfin >= datoffin && iFini === "")
            {
                return true;
            }
            else if (iFini <= datofini && iFfin >= datoffin)
            {
                return true;
            }
            return false;
        }
        );
    $(document).ready(function() {
        var table = $('#caseReport').DataTable();
        // Event listener to the two range filtering inputs to redraw on input
        $('#min, #max').keyup( function() {
            table.draw();
        } );
    } );


    $(function(){
        $('#min').bootstrapMaterialDatePicker({
            format: 'DD-MM-YYYY',
            clearButton: true,
            time: false,
            maxDate : new Date(),
        });
        $('#max').bootstrapMaterialDatePicker({
            format: 'DD-MM-YYYY',
            clearButton: true,
            time: false,
            maxDate : new Date(),
        });
    });

</script>
@endsection
