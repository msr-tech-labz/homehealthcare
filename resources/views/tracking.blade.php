<!DOCTYPE html>
<html>
<head>
    <title>Tracking</title>
</head>
<body>
    <div id="map-canvas" style="width: 100%;height: 100%;border: 1px solid #ccc;min-height:600px; "></div>
    <script src="https://www.gstatic.com/firebasejs/3.3.0/firebase.js"></script>
    <script>
        var isMapInitialized = false;
        // Initialize Firebase
        // TODO: Replace with your project's customized code snippet
        var config = {
            apiKey: "AIzaSyC6sEpFSvRvPxT-C1rfiD4vxJIUr6Drjqk",
            authDomain: "apnacare-portal.firebaseapp.com",
            databaseURL: "https://apnacare-portal.firebaseio.com",
            storageBucket: "apnacare-portal.appspot.com",
        };
        firebase.initializeApp(config);

        var txtField = document.getElementById("txt");

        // Get a reference to the database service
        var database = firebase.database();

        var map;
        var map_marker;
        var lat = 13.064156;
        var lng = 77.5810103;
        var lineCoordinatesArray = [];

        var trackingRef = database.ref('tracking/' + 1);    

        var dbRef = database.ref().child('tracking');
        dbRef.on('child_changed', function (snapshot) {
            var tdata = snapshot.val();
            for(var data in tdata){
                if (tdata.hasOwnProperty(data)) {
                    for(var key in tdata[data]){
                        //console.log(key + " -> " + tdata[data][key]);
                        lat = tdata[data]['lat'];
                        lng = tdata[data]['lng'];
                        
                        if(!isMapInitialized){
                            initMap();
                        }else{
                            redraw();
                        }
                    }
                }
            }
        });

        //writeUserData(1,'Nagesh H S',13.064156,77.5810103);

        function writeUserData(userId, name, lattitude, longitude) {
            firebase.database().ref('tracking/' + userId).push({
                user: name,
                lat: lattitude,
                lng : longitude
            });
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBFVRxoHdxhkIqVPiLUPe2kGDJWRmZgIS4&libraries=places&region=IN"></script>
    <script>
        function initMap(){
            map = new google.maps.Map(document.getElementById('map-canvas'), {
                zoom: 16,
                center: {lat: lat, lng : lng, alt: 0},
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                disableDefaultUI:   true,
                overviewMapControl: false,
                streetViewControl:  false,
                scaleControl:       true,
                mapTypeControl:     false,
                panControl:         true,
                panControlOptions:{
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.TOP_RIGHT
                }
            });

            map_marker = new google.maps.Marker({position: {lat: lat, lng: lng}, map: map});
            map_marker.setMap(map);
        }

        function redraw() {
            var image = 'https://lh3.ggpht.com/XAjhu-6znztoLTr9AxuwM5v0wilaKiUJJMLKEiiFMn6lGOmBmY1Km7Kt1ohildzlIdWgkwy_5g=w9-h9';

            new google.maps.Marker({
              position: {lat: lat, lng : lng},
              map: map,
              draggable: false,
              icon: image
            });

            map.setCenter({lat: lat, lng : lng, alt: 0});
            map_marker.setPosition({lat: lat, lng : lng, alt: 0});
        }

        function getNonZeroRandomNumber(){
            return Math.floor(Math.random() * 201) - 1;
        }
    </script>
</body>
</html>
