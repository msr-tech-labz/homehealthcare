<!DOCTYPE html>
<html>
<head>
	<title>Payment Gateway Response</title>
    <style>
        .border-top{
            border-top: 1px solid #333 !important;
        }
        .border-bottom{
            border-bottom: 1px solid #333 !important;
        }
        .border-right{
            border-right: 1px solid #333 !important;
        }
        .border-left{
            border-left: 1px solid #333 !important;
        }

         .no-padding, .no-padding tbody tr td, .no-padding tbody tr th{
            padding: 0 !important;
            border: 1px solid #333;
        }
        .no-border, .no-border tbody tr td, .no-border tbody tr th{
            border: 0;
        }
         .services-table, .services-table thead tr th, .services-table tfoot tr th, .services-table tfoot tr td, .services-table  tbody tr th,.services-table tbody tr td{
             padding: 1px !important;
             border: 0;
         }

         .services-table{
            margin-left: 5px;
         }

         .services-table thead tr th, .text-center{
             text-align: center !important;
         }

         .text-right{
            text-align: right !important;
         }
        .tbl-text{
            margin-top:1px !important;
            padding:2px !important;
            margin-bottom:1px !important;
        }
        @media print{
            .main{
                width: 100% !important;
            }
            .print-btn{
                display: none !important;
            }
        }
    </style>
</head>
<body style="width: 100%; margin: 0 auto; background: #e2e1e0; font-family: 'Roboto', sans-serif !important;padding-top: 10px !important">
	<div class="main" style="display:block; width:60%; margin: 0 auto; background: #fff; border-radius: 2px; box-shadow: 0 10px 20px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);margin-top:10px !important">
		<!-- Header -->
		<div style="display: block; max-width: 99%; height: 90px; border-bottom: 1px solid #E0E0E0; padding: 10px 10px 5px 10px; ">
            <div style="display: inline-block; float:left;">
                <img src="{{  asset('uploads/provider/'.Helper::encryptor('encrypt',$profile->tenant_id).'/'.$profile->organization_logo) }}" style="max-height: 80px; margin: 5px 0 0 10px" alt="{{ $profile->orgnization_name.' Logo' }}" title="{{ $profile->orgnization_name }}">
            </div>
			<div style="display: inline-block; width:80%; float:left;">
				<div style="margin-left: 2%; font-size: 12px; padding-left: 15px; font-weight: 300; height: 45px; vertical-align: middle; margin-right: 10px; letter-spacing: 1px">
                    <span style="font-size: 22px;color: #009688; ">{{ $profile->organization_name ?? '' }}</span><br>
					{!! !empty($profile->organization_address)?$profile->organization_address.', ':'' !!}
					{!! !empty($profile->organization_area)?$profile->organization_area.', ':'' !!}
					{!! !empty($profile->organization_city)?$profile->organization_city.'-':'' !!}
					{!! !empty($profile->organization_zipcode)?$profile->organization_zipcode.'<br>':'' !!}
					Tel No.: {!! !empty($profile->phone_number)?'+91-'.$profile->phone_number:'' !!}  {!! !empty($profile->landline_number)?'+91-'.$profile->landline_number:'' !!}<br>
					{!! !empty($profile->website)?'Website: '.$profile->website:'' !!}
                </div>
			</div>
		</div>

		<!-- Main Content -->
		<div style="min-height: 200px; padding: 10px 15px; font-size: 14px; line-height: 22px; text-align: justify; margin: 10px 0; border-bottom: 0px solid #E0E0E0">
            <div class="row clearfix">

				@if($response['order_status'] != 'Success')
                <p>Your payment has been received with a failure status. @if(!empty($response['failure_message'])) Reason - <code style="color:#ff0000">{{ $response['failure_message'] }}</code>.@endif <br><br>

                Please quote your transaction reference number for any queries relating to this request.</p>
                @endif
            </div>

            <table width="100%" class="table" style="border: 1px solid #eee">
                <caption style="text-align: left !important;height: 30px; font-weight: 400; font-size:18px; line-height:30px;padding: 5px;background: #eee;">Transaction Details</caption>
				<tr>
                    <td width="40%">Transaction Status</td>
                    <td><code>{{ $response['order_status'] }}</code></td>
                </tr>
                <tr>
                    <td>Tracking ID</td>
                    <td><code>{{ $response['tracking_id']?$response['tracking_id']:$response['tracking_id'] }}</code></td>
                </tr>
                <tr>
                    <td>Transaction Date and Time</td>
                    <td><code>{{ ($response['trans_date'] == 'null')?date('d-m-Y H:i:s'):$response['trans_date'] }}</code></td>
                </tr>
                <tr>
                    <td>Patient ID</td>
                    <td><code>{{ isset($invoice->patient)?$invoice->patient->patient_id:'' }}</code></td>
                </tr>
                <tr>
                    <td>Patient Name</td>
                    <td><code>{{ isset($invoice->patient)?$invoice->patient->full_name:'' }}</code></td>
                </tr>
                <tr>
                    <td>Email Address</td>
                    <td><code>{{ isset($invoice->patient)?$invoice->patient->email:'' }}</code></td>
                </tr>
                <tr>
                    <td>Transaction Amount</td>
                    <td><code>Rs. {{ $response['amount'] }}</code></td>
                </tr>
                <tr>
                    <td>Payment Mode</td>
                    <td><code>{{ ($response['payment_mode'] == 'null')?'NA':$response['payment_mode'] }}</code></td>
                </tr>
            </table>
		</div>

        <div class="print-btn" style="background:#3c860b;background-image:-ms-linear-gradient(bottom,#3c860b 0%,#6fb343 100%);background-image:-moz-linear-gradient(bottom,#3c860b 0%,#6fb343 100%);background-image:-o-linear-gradient(bottom,#3c860b 0%,#6fb343 100%);background-image:-webkit-linear-gradient(bottom,#3c860b 0%,#6fb343 100%);background-image:linear-gradient(to top,#3c860b 0%,#6fb343 100%);border-radius:5px;display:block;margin:0;padding:5px 0 0 0;height:25px;width:100px;margin-left:5%">
        <a style="color:#fff;text-align:center;display:block;font-size:16px;font-weight:500;text-decoration:none;width:100px" target="_blank" href="javascript:void(0);" onclick="window.print()">
            Print
        </a></div><br><br>

		<!-- Footer -->
		<div style="display: block; position:relative; bottom: 0; max-width: 99%; min-height: 90px; padding: 5px 10px 0px 10px; ">
			<div style="width: 50%; display: inline-block; font-size: 12px;">
				<span>Copyright &copy; {{date('Y')}} {{ $profile->organization_name }} All Rights Reserved</span><br><br>
				<span style="font-size: 12px; font-weight: 600">
					<a href="javascript:void(0);" style="color: #2899dc !important">Terms and Conditions</a> |
					<a href="javascript:void(0);" style="color: #2899dc !important">Privacy Policy</a>
				</span>
			</div>
			<div style="width: 49%; display: inline-block; text-align: right; vertical-align: top; font-size: 14px">
				<p style="margin-bottom: 10px;margin-top:0;padding-top:0">Powered by</p>
                <code><small>SmartHealthConnect v1.0.0</small></code>
			</div>
			<br><br>
		</div>
	</div>
</body>
</html>
