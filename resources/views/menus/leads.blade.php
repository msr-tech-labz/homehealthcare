<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
        <div class="header">
            <h2 style="font-size:25px; font-weight: normal">Leads</h2>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 262px;"><ul class="list" style="height: 262px; overflow: hidden; width: auto;">
            <li class="active">
                <a href="" class="toggled waves-effect waves-block">
                    <i class="material-icons">home</i>
                    <span>Home</span>
                </a>
            </li>
            <li>
                <a href="" class=" waves-effect waves-block">
                    <i class="material-icons">text_fields</i>
                    <span>Create Lead</span>
                </a>
            </li>
            <li>
                <a href="" class=" waves-effect waves-block">
                    <i class="material-icons">text_fields</i>
                    <span>Pending Leads</span>
                </a>
            </li>
            <li>
                <a href="" class=" waves-effect waves-block">
                    <i class="material-icons">text_fields</i>
                    <span>Converted Leads</span>
                </a>
            </li>
            <li class="header">Reports</li>
            <li>
                <a href="" class=" waves-effect waves-block">
                    <i class="material-icons">layers</i>
                    <span>Leads Report</span>
                </a>
            </li>
            <li>
                <a href="" class=" waves-effect waves-block">
                    <i class="material-icons">layers</i>
                    <span>Leads Analytics</span>
                </a>
            </li>
        </ul></div>
    </div>
    <!-- #Menu -->
</aside>
