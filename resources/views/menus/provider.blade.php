<div class="menu">
    <ul class="list">                    
        <li class="@yield('active_home')">
            <a href="{{ url('/') }}">
                <i class="material-icons">dashboard</i>
                <span>Dashboard</span>
            </a>
        </li>                
        <li class="@yield('active_cases')">
            <a href="{{ route('case.index') }}">
                <i class="material-icons">assignment</i>
                <span>Cases</span>
            </a>
        </li>
        <li class="@yield('active_caregivers')">
            <a href="{{ route('caregiver.index') }}">
                <i class="material-icons">group</i>
                <span>Caregivers</span>
            </a>
        </li>
        <li class="@yield('active_patients')">
            <a href="{{ route('patient.index') }}">
                <i class="material-icons">accessible</i>
                <span>Patients</span>
            </a>
        </li>
        <li class="@yield('active_billings')">
            <a href="{{ route('billing.index') }}">
                <i class="material-icons">credit_card</i>
                <span>Billings</span>
            </a>
        </li>
        <li class="@yield('active_trackings')">
            <a href="{{ route('tracking.index') }}">
                <i class="material-icons">person_pin_circle</i>
                <span>Tracking</span>
            </a>
        </li>
        <li class="@yield('active_leaves')">
            <a href="{{ route('leaves.request') }}">
                <i class="material-icons">accessibility</i>
                <span>Leaves Request</span>
            </a>
        </li>   
        <li class="header">Admin</li>
        <li class="@yield('active_administration')">
            <a href="{{ route('settings') }}">
                <i class="material-icons">settings</i>
                <span>Administration</span>
            </a>
        </li>
    </ul>
</div>