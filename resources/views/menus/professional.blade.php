<div class="menu">
    <ul class="list">                    
        <li class="@yield('active_home')">
            <a href="{{ url('/') }}">
                <i class="material-icons">dashboard</i>
                <span>Dashboard</span>
            </a>
        </li>                
        <li class="@yield('active_cases')">
            <a href="{{ route('case.index') }}">
                <i class="material-icons">assignment</i>
                <span>Cases</span>
            </a>
        </li>
        <li class="@yield('active_profilemanagement')">
            <a href="{{ route('profilemanagement') }}">
                <i class="material-icons">person_pin</i>
                <span>Profile Management</span>
            </a>
        </li>
        <li class="@yield('active_patients')">
            <a href="{{ route('patient.index') }}">
                <i class="material-icons">accessible</i>
                <span>Patients</span>
            </a>
        </li>
        <li class="@yield('active_practice')">
            <a href="{{ route('console.billing') }}">
                <i class="material-icons">event_note</i>
                <span>Practice Management</span>
            </a>
        </li>
        <li class="@yield('active_report')">
            <a href="{{ route('console.reports') }}">
                <i class="material-icons">record_voice_over</i>
                <span>Report</span>
            </a>
        </li>
        <li class="@yield('active_billing')">
            <a href="{{ route('console.reports') }}">
                <i class="material-icons">monetization_on</i>
                <span>BIlling</span>
            </a>
        </li>
        <li class="header">Admin</li>
        <li class="@yield('active_administration')">
            <a href="{{ route('settings') }}">
                <i class="material-icons">settings</i>
                <span>Administration</span>
            </a>
        </li>
    </ul>
</div>