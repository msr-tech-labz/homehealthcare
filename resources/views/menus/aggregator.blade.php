<div class="menu">
    <ul class="list">
        {{-- <li class="header">MAIN NAVIGATION</li> --}}
        <li class="@yield('active_home')">
            <a href="{{ url('/') }}">
                <i class="material-icons">dashboard</i>
                <span>Dashboard</span>
            </a>
        </li>
        @if(Helper::encryptor('decrypt',session('utype')) == 'Aggregator')
        <li class="@yield('active_leads')">
            <a href="{{ route('lead.index') }}">
                <i class="material-icons">swap_calls</i>
                <span>Leads</span>
            </a>
        </li>
        @endif
        <li>
            <a href="javascript:void(0);" class="menu-toggle sidebar-menu-toggle">
                <i class="material-icons">assignment</i>
                <span>Cases</span>
            </a>
            <ul class="ml-menu">
                <li class="@yield('active_mycases')">
                    <a href="{{ route('case.index',['viewer' => 'self']) }}">
                        <span>My Cases</span>
                    </a>
                </li>
                <li class="@yield('active_othercases')">
                    <a href="{{ route('case.index',['viewer' => 'others']) }}">
                    <span>Other Cases</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="@yield('active_caregivers')">
            <a href="{{ route('caregiver.index') }}">
                <i class="material-icons">group</i>
                <span>Caregivers</span>
            </a>
        </li>
        <li class="@yield('active_patients')">
            <a href="{{ route('patient.index') }}">
                <i class="material-icons">accessible</i>
                <span>Patients</span>
            </a>
        </li>
        @if(\Helper::getTenantID() == "90" )
        <li class="@yield('active_reports')">
            <a href="{{ route('all.reports') }}">
                <i class="material-icons">assignment_returned</i>
                <span>Reports</span>
            </a>
        </li>
        @endif
        <li class="@yield('active_leaves')">
            <a href="{{ route('leaves.request') }}">
                <i class="material-icons">accessibility</i>
                <span>Leaves Request</span>
            </a>
        </li>
    </a>
</li>
{{-- <li class="@yield('active_payroll')">
    <a href="javascript:void(0);">
        <i class="material-icons">receipt</i>
        <span>Payroll</span>
    </a>
</li> --}}
{{-- <li class="header">Accounting</li>
<li class="@yield('active_accounting')">
<a href="javascript:void(0);">
<i class="material-icons">account_balance</i>
<span>Accounting</span>
</a>
</li> --}}
{{-- <li class="header">Reports</li>
<li class="@yield('active_hr_reports')">
<a href="javascript:void(0);">
<i class="material-icons">insert_chart</i>
<span>Reports</span>
</a>
</li> --}}
@if(session('role') == 1)
<li class="header">Admin</li>
<li class="@yield('active_administration')">
    <a href="{{ route('settings') }}">
        <i class="material-icons">settings</i>
        <span>Administration</span>
    </a>
</li>
@endif
{{-- <li class="header">Stats</li>
<li>
<a href="javascript:void(0);">
<i class="material-icons col-red">donut_large</i>
<span>Important</span>
</a>
</li>
<li>
<a href="javascript:void(0);">
<i class="material-icons col-amber">donut_large</i>
<span>Warning</span>
</a>
</li>
<li>
<a href="javascript:void(0);">
<i class="material-icons col-light-blue">donut_large</i>
<span>Information</span>
</a>
</li> --}}
</ul>
</div>
