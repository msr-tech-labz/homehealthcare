@extends('layouts.main-layout')

@section('page_title','Scheduling - ')

@section('active_scheduling','active')
@section('page.styles')
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.3.1/fullcalendar.min.css"/>
    <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />
</head>
<style>
body{
    background-color: #fff !important;
}
.fc-view-container{
    background-color: white;
}
#calendar{
    background-color: white;
}
.fc-toolbar.fc-header-toolbar {
    margin-top: 1em;
}
.fc-time{
   display : none;
}

.fc td, .fc th {
    color: rgb(33, 150, 243);
}
.fc-unthemed .fc-content, .fc-unthemed .fc-divider, .fc-unthemed .fc-list-heading td, .fc-unthemed .fc-list-view, .fc-unthemed .fc-popover, .fc-unthemed .fc-row, .fc-unthemed tbody, .fc-unthemed td, .fc-unthemed th, .fc-unthemed thead {
    border-color: rgba(0, 142, 255, 0.14);
}
.dataTable td{
    line-height: 0.7 !important;
}
</style>
@endsection

@section('content')
<div class="body">
    <div class="col-md-12">
        <div class="col-md-4">
            {{-- <h4>List Of all Caregivers</h4> --}}
            <div class="col-md-12" style="background-color: #f6f6f6;height: 500px;padding: 10px;text-align: left; cursor: pointer;">
                <table class="table dataTable">
                    <thead>
                    <tr>
                        <th>Caregiver</th>
                    </tr>
                    <hr>
                    </thead>
                    <tbody>
                        @foreach($caregivers as $i => $c )
                        <tr>
                            <td style="margin:0;" class="emp" id="{{$c->id}}" >{{$c->first_name}} {{$c->middle_name}} {{$c->last_name}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-8" id='calendar'></div>
    </div>
</div>
@endsection

@section('page.scripts')
@parent
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.3.1/fullcalendar.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('.dataTable').DataTable({
        });
        $('#calendar').fullCalendar({
            header: {
                center: 'month,agendaWeek,agendaDay' // buttons for switching between views
            },
            defaultDate: '{{ date('Y-m-d') }}',
            editable: false,
            height: 500,
            defaultView: 'month',
            eventRender: function(event, element) {
                element.append(event.time);
                element.append("<br/>" + event.name);
                element.append("<br/>" + event.alert);
            },
            // eventDrop: function(event, delta, revertFunc) {
            //     alert(event.title + " was dropped on " + event.start.format());
            //     if (!confirm("Are you sure about this change?")) {
            //         revertFunc();
            //     }
            // },
            // eventResize: function(event, delta, revertFunc) {
            //     alert(event.title + " end is now " + event.end.format());
            //     if (!confirm("is this okay?")) {
            //         revertFunc();
            //     }
            // }
        });
    });

    $('.content').on('click','.emp',function(){
        $(this).css('background-color', '#2196f3');
        $("div .emp").not(this).css('background-color', '#fff');
        var caregiverID= $(this).attr('id');

        $('#calendar').fullCalendar('removeEvents');
        // $('#calendar').fullCalendar('today');

        $.ajax({
            url: '{{ route('scheduling.caregiver.fetch') }}',
            type: 'POST',
            data: {_token: '{{ csrf_token() }}', caregiver_id: caregiverID},
            success: function (data){
                if(data){
                        $('#calendar').fullCalendar('renderEvents', data, true);
                }
            },
            error: function (error){
                console.log(error);
            }
        });
    });
</script>
@endsection
