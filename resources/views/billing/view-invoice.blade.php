@extends('layouts.main-layout')

@section('page_title','View Invoice - '.$invoice->invoice_no.' | ')

@section('active_billings','active')

@section('plugin.styles')
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<style>
    .form-control-label .lbl{
        margin-bottom: 0 !important;
        text-align: left !important;
        float: left;
        font-weight: normal;
        color: #333 !important
    }
    @keyframes blink {
        50% { border-color: #ff0000; border:1px solid #ff0000; padding: 0 5px;}
    }
    .blink{
        animation: blink .9s step-end infinite alternate;
        z-index: 9999 !important;
    }
    .card{
        margin-bottom: 20px !important;
    }
    address{
        margin-bottom: 0 !important;
    }
    .card .header{
        padding: 8px 12px !important;
        background: #dadfe8;
        font-size: 14px;
        font-weight: 600;
        color: #555 !important;
    }
    .form-control-label, .form-group{
        margin-bottom: 0px !important;
    }
    .form-group > input.form-control, .form-line > input.form-control{
        border: 1px solid #ccc !important;
        box-shadow: rgba(0, 0, 0, 0.075) 0px 1px 1px inset !important;
        padding: 2px 5px !important;
        height: 30px !important;
    }
    .card .body .col-xs-8, .card .body .col-sm-8, .card .body .col-md-8, .card .body .col-lg-8{
        margin-bottom: 10px !important
    }
    .no-margin{
        margin: 0 !important;
    }
    .col-lg-12, .col-lg-6, .col-lg-4{
        margin-bottom: 0 !important
    }
    .table{
        margin-bottom: 0 !important;
    }
    .table tbody tr td, .table tbody tr th, .table thead tr th{
        padding: 5px !important;
        vertical-align: middle !important;
    }
    .table thead{
        border-top: none !important;
    }
    .itemsTable thead tr th, .itemsTable tbody tr td, .lessTable tr th, .lessTable tr td{
        font-size: 12px !important;
        vertical-align: middle !important;
        text-transform: none !important;
    }
    .statsTable tbody tr td, .statsTable tbody tr th {
        padding: 5px 15px !important;
        font-size: 15px !important;
        vertical-align: middle !important;
    }
    .plus{
        height: 20px;
        margin-top: -3px;
        margin-right: 2px;
    }
    .modal .modal-header{
        padding: 10px 20px 5px 20px !important
    }
    .loading {
        margin-left: -2% !important;
        position: absolute;
        z-index: 2;
        height: 90%;
        width: 95%;
        background: #eee;
        text-align: center !important;
    }
</style>
@endsection

@section('content')
<div class="row clearfix" style="margin:auto 0 !important">
    <div class="col-lg-12">
        <div class="card">
            <div class="body" style="padding: 5px 5px !important">
                <div class="col-md-1 text-left" style="width:5% !important; margin-bottom: 0 !important">
                    <a class="btn btn-primary btn-circle waves-effect waves-circle" href="{{ route('billing.index') }}"><i class="material-icons">arrow_back</i></a>
                </div>
                <div class="col-md-6">
                    <h4 class="card-title">View Invoice - {{ $data['inv_prefix'].$invoice->invoice_no }}</h4>
                </div>
                <div class="col-md-3"><?php if($invoice->email_invoice != null){ ?><h5 class="text-grey">Invoice Email sent on {{ $invoice->email_invoice->format('d-m-Y') }}</h5><?php } ?></div>
                <div class="col-md-1 text-right" style="margin-top:2px !important">
                    <a class="btn btn-block btn-sm btn-warning" target="_blank" href="{{ route('billing.print-invoice',[Helper::encryptor('encrypt',$invoice->id),$type]) }}" title="Print Invoice"><i class="material-icons">print</i></a>
                </div>
                <div class="col-md-1 text-right" style="margin-top:2px !important">
                    <a class="btn btn-block btn-sm btn-primary" href="{{ route('billing.email-invoice',[Helper::encryptor('encrypt',$invoice->id),$type]) }}" title="Email Invoice"><i class="material-icons">mail</i></a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12">
        <!-- Invoice Details Card -->
        <div class="card">
            <div class="header">
                Invoice Details
            </div>
            <div class="body">
                <div class="row">
                    <!-- Patient details -->
                    <div class="col-md-6 no-margin" style="padding-left: 30px">
                        <b>Customer</b><br>
                        @if(isset($invoice->patient))
                        {{ preg_replace('/ .$/', '', $invoice->patient->full_name) }}<br>
                        (<i>Patient ID: <b>{{ $invoice->patient->patient_id }}</b></i>)
                        <address>{!! $invoice->patient->street_address !!}</address>
                        Phone: {{ $invoice->patient->contact_number ?? '-' }}<br>
                        Email: {{ $invoice->patient->email ?? '-' }}<br>
                        @foreach($mailers as $mail)
                        @if($loop->first)
                        <br><b>Other active receipients for this invoice - </b><br>
                        @endif
                        <small><i>{{ $mail->email }}</i></small>
                        @if(!$loop->last)
                        ,
                        @endif
                        @if($loop->last)
                        <small>
                            <a target="_blank" href="{{ route('patient.addemailaddress',Helper::encryptor('encrypt',$invoice->patient->id)) }}">
                                <br><u><i> Go to Emails & Address</i></u>
                            </a>
                        </small>
                        @endif
                        @endforeach
                        @endif
                    </div>

                    <!-- Invoice details -->
                    <div class="col-md-6 no-margin">
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 form-control-label">
                                <label for="invoice_no">Invoice Number</label>
                            </div>
                            <div class="col-lg-4 col-md-8 col-sm-8 col-xs-7 form-control-label lbl">
                                <label class="lbl">{{ $data['inv_prefix'].''.$invoice->invoice_no }}</label>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 form-control-label">
                                <label for="invoice_date">Invoice Date</label>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-8 col-xs-7 form-control-label lbl">
                                <div class="form-group">
                                    <label class="lbl">{{ $invoice->invoice_date->format('d-m-Y') }}</label>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-5 form-control-label">
                                <label for="due_date">Due Date</label>
                            </div>
                            <div class="col-lg-4 col-md-8 col-sm-8 col-xs-7 form-control-label lbl">
                                <div class="form-group">
                                    <label class="lbl">{{ $invoice->invoice_due_date->format('d-m-Y') }}</label>
                                </div>
                            </div>
                        </div>
                        @if($type == 'Proforma')
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 form-control-label">
                                <label for="status">Status</label>
                            </div>
                            <div class="col-lg-4 col-md-8 col-sm-8 col-xs-7 form-control-label lbl">
                                <div class="form-group">
                                    <label class="lbl">{{ $invoice->status }}</label>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <!-- Services / Products  Card -->
        <div class="card">
            <div class="header">
                Services / Products
            </div>
            <div class="body">
                <div class="row clearfix">
                    <div class="col-lg-12">
                        <table class="table table-bordered table-condensed itemsTable">
                            <thead>
                                <tr>
                                    <th width="25%">Description</th>
                                    <th width="15%" class="text-center">Period</th>
                                    <th class="text-center">Duration/Qty</th>
                                    <th class="text-center">Rate/Item</th>
                                    <th class="text-center">Gross</th>
                                    <th class="text-center">Discount</th>
                                    <th class="text-center">CGST</th>
                                    <th class="text-center">SGST</th>
                                    <th class="text-right">Net Total</th>
                                </tr>
                            </thead>
                            <tbody>
                            {? $subTotal = $taxableAmount = $totalDiscount = $totalTax = $totalAmount = $less = $adjusted = 0; ?}
                            @if($invoice->getItemsArray() || count($invoice->items))
                                @foreach ($invoice->getItemsArray() as $key => $item)
                                {? $taxableAmount += ($item->rate * $item->duration - $item->discount); ?}
                                {? $subTotal += ($item->rate * $item->duration); ?}
                                {? $totalDiscount += $item->discount; ?}
                                {? $totalTax += $item->tax_amount; ?}
                                {? $totalAmount += ($item->duration * $item->rate - $item->discount + $item->tax_amount); ?}
                                <tr class="item_row">
                                    <td class="table_service_ID">{{ ucwords($item->description) }}</td>
                                    <td class="text-center">{{ $item->period ?? '-' }}</td>
                                    <td class="text-center">{{ $item->duration }}</td>
                                    <td class="text-center">{{ number_format($item->rate,2) }}</td>
                                    <td class="text-center">{{ number_format(($item->duration * $item->rate),2) }}</td>
                                    <td class="text-center">{{ number_format($item->discount,2) }}</td>
                                    <td class="text-center">{{ number_format($item->tax_amount/2,2) }}</td>
                                    <td class="text-center">{{ number_format($item->tax_amount/2,2) }}</td>
                                    <td class="text-right">{{ number_format(($item->duration * $item->rate - $item->discount + $item->tax_amount),2) }}</td>
                                </tr>
                                @endforeach

                                @if(isset($invoice->items) && count($invoice->items->where('category','Credit Memo')))
                                    <tr>
                                        <td colspan="9" style="color: #607d8b !important;font-weight:bold;font-size: 14px !important"><i>Less</i></td>
                                    </tr>

                                    {? $dataCancelledSchedulesCount = 0; ?}
                                    {? $dataCancelledSchedulesAmount = 0.00; ?}

                                    @foreach ($invoice->items->where('category','Credit Memo') as $item)
                                    @if($item->creditMemo->type == 'Others')
                                    {? $less += $item->creditMemo->amount; ?}
                                    <tr>
                                        <td class="text-left" style="padding-left: 30px !important">Credit Memo</td>
                                        <td class="text-center">{{ $item->creditMemo->date->format('d-m-Y') }}</td>
                                        <td class="text-center">{{ $item->creditMemo->comment }}</td>
                                        <td colspan="5"></td>
                                        <td class="text-right">- {{ $item->creditMemo->amount }}</td>
                                    </tr>
                                    @else
                                    {? $dataCancelledSchedulesCount += 1; ?}
                                    {? $dataCancelledSchedulesAmount += $item->creditMemo->amount; ?}
                                    @endif
                                    @endforeach
                                    @if($dataCancelledSchedulesCount)
                                    {? $less += $dataCancelledSchedulesAmount; ?}
                                    <tr>
                                        <td class="text-left" style="padding-left: 30px !important">Credit Memo</td>
                                        <td class="text-center">Multiple ({{ $dataCancelledSchedulesCount }})</td>
                                        <td class="text-center">Cancelled Schedules</td>
                                        <td colspan="5"></td>
                                        <td class="text-right">- {{ $dataCancelledSchedulesAmount }}</td>
                                    </tr>
                                    @endif
                                @endif

                                @if(isset($invoice->items) && count($invoice->items->where('category','Invoice')))
                                    <tr>
                                        <td colspan="9" style="color: #607d8b !important;font-weight:bold;font-size: 14px !important"><i>Invoice</i></td>
                                    </tr>

                                    @foreach ($invoice->items->where('category','Invoice') as $item)
                                        {? $adjusted += $item->total_amount; ?}
                                        <tr>
                                            <td class="text-left" style="padding-left: 30px !important">Invoice</td>
                                            <td class="text-center">{{ $item->itemInvoice->invoice_date->format('d-m-Y') }}</td>
                                            <td class="text-center">{{ \Helper::getSetting('proforma_invoice_inits').''.$item->itemInvoice->invoice_no }}</td>
                                            <td colspan="5"></td>
                                            <td class="text-right">{{ $item->total_amount }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            @else
                                <tr class="no_item_row">
                                    <th colspan="10" style="color:red">No Items Added!</th>
                                </tr>
                            @endif
                            </tbody>
                            <tfoot>
                                <tr style="{{ count($invoice->getItemsArray()) || count($invoice->items)?'':'display:none' }}">
                                    <td colspan="7" style="border: 0 !important"></td>
                                    <th class="text-right">Sub Total</th>
                                    <th class="text-right">{{ number_format($subTotal,2) }}</th>
                                </tr>
                                <tr style="{{ count($invoice->getItemsArray()) || count($invoice->items)?'':'display:none' }}">
                                    <td colspan="7" style="border: 0 !important"></td>
                                    <th class="text-right">Discount</th>
                                    <th class="text-right">- {{ number_format($totalDiscount,2) }}</th>
                                </tr>
                                <tr style="{{ count($invoice->getItemsArray()) || count($invoice->items)?'':'display:none' }}">
                                    <td colspan="7" style="border: 0 !important"></td>
                                    <th class="text-right">Taxed Amount</th>
                                    <th class="text-right">{{ number_format($totalTax,2) }}</th>
                                </tr>
                                <tr style="{{ count($invoice->getItemsArray()) || count($invoice->items)?'':'display:none' }}">
                                    <td colspan="7" style="border: 0 !important"></td>
                                    <th class="text-right">Net Total</th>
                                    <th class="text-right">{{ number_format($subTotal - $totalDiscount + $totalTax,2) }}</th>
                                </tr>
                                <tr style="{{ count($invoice->getItemsArray()) || count($invoice->items)?'':'display:none' }}">
                                    <td colspan="7" style="border: 0 !important"></td>
                                    <th class="text-right">Less</th>
                                    <th class="text-right">- {{ number_format($less,2) }}</th>
                                </tr>
                                <tr style="{{ count($invoice->getItemsArray()) || count($invoice->items)?'':'display:none' }}">
                                    <td colspan="7" style="border: 0 !important"></td>
                                    <th class="text-right">Previous Due</th>
                                    <th class="text-right">{{ number_format($adjusted,2) }}</th>
                                </tr>
                                <tr style="{{ count($invoice->getItemsArray()) || count($invoice->items)?'':'display:none' }}">
                                    <td colspan="7" style="border: 0 !important"></td>
                                    <th class="text-right">Grand Total</th>
                                    <th class="grand_total text-right" style="font-size:16px !important; font-weight:bold !important">{{ number_format(($taxableAmount + $totalTax - $less + $adjusted),2) }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
