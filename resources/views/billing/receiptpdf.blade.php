<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Receipt - {{ $receipt->patient->full_name.' - '.$receipt->patient->patient_id }}</title>
    <!-- Favicon-->
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <style>

    @page
    {
        size: auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }

    html, body{
        font-size: 13px;
        font-family: 'Roboto', Arial, Tahoma, sans-serif !important;
    }
    h1,h2,h3,h4,h5,h6{
        font-family: 'Roboto', Arial, Tahoma, sans-serif !important;
    }
    .border-grey{
        border: 0.1em solid #ccc !important;
    }
    </style>
</head>

<body style="background: transparent !important;">

    <div style="border: 0px solid black;background: white;padding: 50px 50px !important;">
        <br>
        <div style="display:inline-block;padding: 5px;margin-left:5px;font-size:13px;height:40px;vertical-align:middle !important">
            <h4 style="width:40%;display:inline-block;font-weight:500;font-size:25px;color:#00b0e4 !important;border-bottom:1px solid #00b0e4;margin-right:0 !important;">RECEIPT for {{ $receipt->receipt_type }}</h4>
            <div style="width:30%;display:inline-block;margin-right:5px;vertical-align:top;padding-top:0px">
            </div>
            <div style="width:20%;display:inline-block;margin-right:5px;padding-top:0px;text-align: right;">
                <img src="{{ asset('uploads/provider/'.Helper::encryptor('encrypt',$profile->tenant_id).'/'.$profile->organization_logo) }}" class="org-img" style="width: 70px;clear:both;">
            </div>
            <br>
        </div>

        <table class="table table-responsive" style="width: 100% !important;margin-bottom:10px !important;margin-top:2% !important;">
            <tr>
                <th colspan="2" class="text-center bg-blue" style="font-size:12px !important;background-color: #00b0e4 !important ;border:2px solid #00b0e4 !important;color: white; height: 20px;">Receipt Details</th>
            </tr>
            <tr class="border-grey">
                <th class="border-grey">Date</th>
                <td class="border-grey">{{ $receipt->receipt_date->format('d-m-Y') }}</td>
            </tr>
            <tr class="border-grey">
                <th class="border-grey">Receipt No.</th>
                <td class="border-grey">{{ $receipt->receipt_no }}</td>
            </tr>
            <tr class="border-grey">
                <th class="border-grey">Customer ID</th>
                <td class="border-grey">{{ $receipt->patient->patient_id }}</td>
            </tr>
            <tr class="border-grey">
                <th class="border-grey">Customer Name</th>
                <td class="border-grey">{{ $receipt->patient->full_name }}</td>
            </tr>
            <?php 
            if($receipt->receipt_type == 'Payment'){
                $number = \Helper::getSetting('proforma_invoice_inits').''.$receipt->invoice->invoice_no;
                $mode = $receipt->payment_mode;
                $refNumber = $receipt->reference_no;
            }else{
                $number = $receipt->creditMemo->credit_memo_no;
                $mode = $refNumber = 'NA';
            }
            ?>
            <tr class="border-grey">
                <th class="border-grey">Description</th>
                <td class="border-grey">Receipt Against {{ $receipt->receipt_type }} No. {{ $number }}</td>
            </tr>
            <tr class="border-grey">
                <th class="border-grey">Mode</th>
                <td class="border-grey">{{ $mode }}</td>
            </tr>
            <tr class="border-grey">
                <th class="border-grey">Reference No.</th>
                <td class="border-grey">{{ $refNumber }}</td>
            </tr>
            <tr class="border-grey">
                <th class="border-grey">Amount in Rs.</th>
                <td class="border-grey">{{ $receipt->receipt_amount }}</td>
            </tr>
            <tr class="border-grey">
                <th class="border-grey">Amount in Words</th>
                <td class="border-grey">{? $f = new \NumberFormatter("en", \NumberFormatter::SPELLOUT); ?}
                    {{ ucwords($f->format((round($receipt->receipt_amount)))).' Rupees Only' }}</td>
                </tr>
        </table>

        <div style="width:100% !important;color: #00b0e4 !important;text-align:center !important;line-height: 30px;font-size: 14px;margin-top:45px !important;">
            {{ strtoupper($profile->organization_name) }}<br>
            <div style="color: #333 !important;font-size:12px !important;line-height:18px !important">
                {{ $profile->organization_address.','.$profile->organization_area.','.$profile->organization_city }}<br>
                <b>Ph:</b> {{ $profile->phone_number }}  <b>Website:</b> {{ $profile->website }}
            </div>
        </div>

        <div style="margin-top:1%;width:100% !important;height:10px !important; background: #00b0e4 !important;text-align:center !important;line-height: 30px;font-size: 14px">
        </div>
    </div>
</body>
</html>
