@extends('layouts.main-layout')

@section('page_title','View Receipt - '.$receipt->patient->full_name.' - '.$receipt->patient->patient_id.' | ')

@section('active_billings','active')

@section('plugin.styles')
    <style>
    img.img-topbar{ margin-top: -5px !important;}

    tr, th{
        border: 2px solid #eee !important;
    }

    @media print {
        .bg-blue {
            background-color: #00b0e4 !important;
            -webkit-print-color-adjust: exact;        }
        }
    </style>
@endsection
@section('content')
<div class="body" style="background: white !important;">
    <div class="col-sm-12 text-left hidden-print" style="padding: 5px;">
        <a href= "{{ route('billing.index') }}" class="btn btn-info btn-lg backtobillings waves-effect"><span class="arrow_back"></span> GO BACK</a>
        <input type="button" class="btn btn-info btn-lg noPrint " value="Print" onclick="window.print();"></input>
        <a href="{{ route('billing.view-receipt',[Helper::encryptor('encrypt',$receipt->id),'download'=>'pdf']) }}" id="saveaspdf" class="btn btn-info btn-lg waves-effect"><span class="arrow_down"></span>Save as PDF</a>
        <a class="btn btn-info btn-lg noPrint" style="float: right;" href="{{ route('billing.view-receipt',[Helper::encryptor('encrypt',$receipt->id),'email'=>'pdf']) }}" title="Email Invoice"><span>Mail as PDF</span></a>
    </div>

    <div style="border: 0px solid black;padding: 10px 30px !important;">
        <br>
        <div style="width:75%;display:inline-block;padding: 5px;margin-left:5px;font-size:13px;height:40px;vertical-align:middle !important">
            <h4 style="display:inline-block;font-weight:500;font-size:25px;color:#00b0e4 !important;border-bottom:1px solid #00b0e4;margin-right:0 !important;padding-right: 20px !important">RECEIPT for {{ $receipt->receipt_type }}</h4>
            <span style="display:inline-block;width:5%;border-bottom: 4px solid #00b0e4;margin-left: -1% !important;padding-left: 0 !important;vertical-align:middle;margin-bottom:4px">&nbsp;</span>
        </div>
        {{-- vertical-align:top; down--}}
        <div style="width:20%;display:inline-block;margin-right:5px;padding-top:0px">
            <center><img src="{{ asset('uploads/provider/'.Helper::encryptor('encrypt',$profile->tenant_id).'/'.$profile->organization_logo) }}" class="org-img" style="width: 70px;clear:both;"></center>
        </div>
        <br>
    </div>

    <div class="bg-blue" style="margin-top:1%;width:100% !important;height:10px !important; background: #00b0e4 !important;text-align:center !important;line-height: 30px;font-size: 14px">
    </div>

    <div style="width: 80%;margin-left: 10%;">
        <table class="table table-responsive" style="width: 100% !important">
            <tr>
                <th colspan="2" class="text-center bg-blue" style="font-size:12px !important;background-color: #00b0e4 !important ;border:2px solid #00b0e4 !important;color: white">Receipt Details</th>
            </tr>
            <tr>
                <th>Date</th>
                <td>{{ $receipt->receipt_date->format('d-m-Y') }}</td>
            </tr>
            <tr>
                <th>Receipt No.</th>
                <td>{{ $receipt->receipt_no }}</td>
            </tr>
            <tr>
                <th>Customer ID</th>
                <td>{{ $receipt->patient->patient_id }}</td>
            </tr>
            <tr>
                <th>Customer Name</th>
                <td>{{ $receipt->patient->full_name }}</td>
            </tr>
            <?php 
            if($receipt->receipt_type == 'Payment'){
                $number = \Helper::getSetting('proforma_invoice_inits').''.$receipt->invoice->invoice_no;
                $mode = $receipt->payment_mode;
                $refNumber = $receipt->reference_no;
            }else{
               $number = $receipt->creditMemo->credit_memo_no;
                $mode = $refNumber = 'NA';
            }
            ?>
            <tr>
                <th>Description</th>
                <td>Receipt Against {{ $receipt->receipt_type }} No. {{ $number }}</td>
            </tr>
            <tr>
                <th>Mode</th>
                <td>{{ $mode }}</td>
            </tr>
            <tr>
                <th>Reference No.</th>
                <td>{{ $refNumber }}</td>
            </tr>
            <tr>
                <th>Amount in Rs.</th>
                <td>{{ $receipt->receipt_amount }}</td>
            </tr>
            <tr>
                <th>Amount in Words</th>
                <td>{? $f = new \NumberFormatter("en", \NumberFormatter::SPELLOUT); ?}
                    {{ ucwords($f->format((round($receipt->receipt_amount)))).' Rupees Only' }}</td>
            </tr>
        </table>
    </div>

    <div class="inv-foot-add" style="width:100% !important;color: #00b0e4 !important;text-align:center !important;line-height: 30px;font-size: 14px;margin-top:45px !important;">
        {{ strtoupper($profile->organization_name) }}<br>
        <div style="color: #333 !important;font-size:12px !important;line-height:18px !important">
            {{ $profile->organization_address.','.$profile->organization_area.','.$profile->organization_city }}<br>
            <b>Ph:</b> {{ $profile->phone_number }}  <b>Website:</b> {{ $profile->website }}
        </div>
    </div>

    <div class="bg-blue" style="margin-top:1%;width:100% !important;height:10px !important; background: #00b0e4 !important;text-align:center !important;line-height: 30px;font-size: 14px">
    </div>
@endsection
