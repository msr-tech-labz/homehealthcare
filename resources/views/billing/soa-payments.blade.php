<div class="annexure" style="padding:10px !important">
    <h4 class="text-center">Payments</h4>
    <table class="table no-padding-border payment-table" style="width:100%; margin-top:10px !important; margin-bottom: 10px !important">
        <thead>
            <tr>
                {{-- <th class="border-right border-top text-center" style="color:#333 !important;line-height: 0.8 !important">#</th> --}}
                <th class="border-right border-top text-center" style="color:#333 !important;line-height: 0.8 !important">Transaction Date</th>
                <th class="border-right border-top text-center" style="color:#333 !important;line-height: 0.8 !important">Payment Mode</th>
                <th class="border-right border-top text-center" style="color:#333 !important;line-height: 0.8 !important">Reference No.</th>
                <th class="border-right border-top text-center" style="color:#333 !important;line-height: 0.8 !important">Status</th>
                <th class="border-right border-top text-center" style="color:#333 !important;line-height: 0.8 !important">Amount</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

<script>
    var url = '{!! route('billing.get-soa-payments-ajax').'?pid='.Helper::encryptor('encrypt',$patient->id).'&tPeriod='.$to_period !!}';
    $('.payment-table').DataTable({
        processing: true,
        dom: '',
        displayLength: -1,
        serverSide: true,
        ajax: url,
        columns: [
            {data : 'created_at', name : 'created_at', searchable: true},
            {data : 'payment_mode', name : 'payment_mode', searchable: true},
            {data : 'invoice.invoice_no', name : 'invoice.invoice_no', searchable: true},
            {data : 'status', name : 'status'},
            {data : 'total_amount', name : 'total_amount'},
        ]
    });
</script>
