{? $fmt = new NumberFormatter( 'en_IN', NumberFormatter::CURRENCY ); ?}
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Statement of Account - {{ $patient->full_name.' - '.$patient->patient_id }}</title>
    <!-- Favicon-->
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{ ViewHelper::ThemePlugin('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    @yield('plugin.styles')

    <!-- Custom Css -->
    <link href="{{ ViewHelper::ThemeCss('style.css') }}" rel="stylesheet">

    <!-- You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ ViewHelper::ThemeCss('themes/all-themes.css') }}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/print.css') }}" media="print" />

    @yield('page.styles')
    <style>
    img.img-topbar{ margin-top: -5px !important;}
    .username:after{
        content: ' \25BE';
        padding-left: 10px;
        font-size: 20px;
    }
    @page
    {
        size: auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }
    div.A4 {
        background: white;
        width: 21cm;
        height: auto;
        box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
        display: block;
        margin: 0 auto;
        padding: 15px !important;
    }
    html, body{
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        font-size: 13px;
        font-family: 'Roboto', Arial, Tahoma, sans-serif !important;
        font-size: 12px !important;
        background: rgb(204,204,204);
    }
    .page{
        width: inherit;
        /*min-height: 30cm !important;*/
        margin: 0;
        padding: 0;
        /*margin-bottom: 30px !important;*/
    }

    .footer{
        position: absolute;;
        width: 100% !important;
        /*margin-top: 20px !important;*/
    }

    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-row-group }
    tfoot { display:table-footer-group }

    .break { page-break-before: always; }

    h1,h2,h3,h4,h5,h6{
        font-family: 'Roboto', Arial, Tahoma, sans-serif !important;
    }
    .border-top{
        border-top: 2px solid #666 !important;
    }
    .border-bottom{
        border-bottom: 2px solid #666 !important;
    }
    .border-right{
        border-right: 2px solid #666 !important;
    }
    .border-left{
        border-left: 2px solid #666 !important;
    }

    .no-padding, .no-padding tbody tr td, .no-padding tbody tr th{
        padding: 0 !important;
        border: 2px solid #666;
        padding-left: 2px !important;
    }
    .no-border, .no-border tbody tr td, .no-border tbody tr th{
        border: 0;
    }
    .no-padding-border, .no-padding-border tbody tr td, .no-padding-border tbody tr th{
        padding: 0 !important;
        border: 0 !important;
        font-size: 10px !important;
    }

    .annexure .no-padding-border, .annexure .no-padding-border tbody tr td, .annexure  .no-padding-border tbody tr th{
        padding: 0 !important;
        border: 1px solid #ccc !important;
        font-size: 12px !important;
    }
    .annexure .no-padding-border thead tr th{
        border: 1px solid #ccc !important;
        background: #ddd !important;
    }
    .services-table, .services-table thead tr th, .services-table tfoot tr th, .services-table tfoot tr td, .services-table  tbody tr th{
        padding: 2px !important;
        border: 0;
    }

    .services-table tbody tr td{
        padding: 8px !important;
        line-height: 2 !important;
        border: 0;
    }

    .tbl-text{
        margin-top:1px !important;
        padding:2px !important;
        margin-bottom:1px !important;
    }

    .user-info{ background: rgba(96, 125, 139, 0.10) !important; border-bottom: 0.1em solid #3F51B5 !important; padding: 10px !important;}

    @media print{
        html{
            color: #333 !important;
        }
    }
    </style>
    <!-- Jquery Core Js -->
    <script src="{{ ViewHelper::ThemePlugin('jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ ViewHelper::ThemePlugin('bootstrap/js/bootstrap.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
    <script src="/vendor/datatables/buttons.server-side.js"></script>
</head>

<body class="theme-blue" style="background: transparent !important">
    <div class="pages" style="height: auto !important;">
        <div class="col-sm-2 text-left hidden-print" style="padding: 5px;">
            <a href= "{{ route('billing.index') }}" class="btn btn-info btn-lg backtobillings waves-effect"><span class="arrow_back"></span> GO BACK</a>
            <input type="button" class="btn btn-info btn-lg noPrint " value="Print" onclick="window.print();"></input>
            <a href="{{ route('billing.statement-of-account',['soa_patient_id' => $patient->id,'download'=>'pdf']) }}" id="saveaspdf" class="btn btn-info btn-lg waves-effect hide"><span class="arrow_down"></span>Save as PDF</a>
            {{-- <a href="{{ route('billing.statement-of-account',['soa_patient_id' => $patient->id,'email'=>true]) }}" id="sendemail" class="btn btn-info btn-lg waves-effect"><span class="arrow_down"></span>Send as E-mail</a> --}}
        </div>

        <div class="A4" style="margin-top: 5px !important">
            <div style="width:78%;display:inline-block;padding: 5px;margin-left:5px;font-size:12px;height:40px;vertical-align:middle !important">
                <h4 style="display:inline-block;font-weight:500;font-size:20px;color:#666 !important;border-bottom:1px solid #666;margin-right:0 !important;padding-right: 20px !important">Statement of Account</h4>
                <span style="display:inline-block;width:5%;border-bottom: 4px solid #666;margin-left: -1% !important;padding-left: 0 !important;vertical-align:middle;margin-bottom:4px">&nbsp;</span>
            </div>
            <div style="width:18%;display:inline-block;margin-right:5px;vertical-align:top;padding-top:0px">
                <center><img src="{{  asset('uploads/provider/'.Helper::encryptor('encrypt',$profile->tenant_id).'/'.$profile->organization_logo) }}" class="org-img" style="width: 70px;clear:both;"></center>
            </div>
            <br><br>
            <table class="table hide" style="margin-top:10px !important; margin-bottom: 10px !important">
                <tr>
                    <td width="20%" style="background: #666 !important;color:#fff !important;line-height: 0.8 !important">Invoice No :</td>
                    <td style="background: #666 !important;color:#fff !important;text-align: left !important;font-weight:600 !important;line-height: 0.8 !important"></td>
                    <td width="20%" style="background: #666 !important;color:#fff !important;line-height: 0.8 !important">Invoice Date :</td>
                    <td style="background: #666 !important;color:#fff !important;text-align: left !important;font-weight:600 !important;line-height: 0.8 !important"></td>
                    <td width="20%" style="background: #666 !important;color:#fff !important;line-height: 0.8 !important">Payable By :</td>
                    <td style="background: #666 !important;color:#fff !important;text-align: left !important;font-weight:600 !important;line-height: 0.8 !important"></td>
                </tr>
            </table>

            <table class="table" style="margin-bottom: 10px !important">
                <tr>
                    <td width="20%" style="background: #eee !important;line-height: 0.8 !important">Patient Name :</td>
                    <td style="background: #eee !important;text-align: left !important;font-weight:600 !important;line-height: 0.8 !important" colspan="3">{{ $patient->full_name }}</td>
                    <td width="12%" style="background: #eee !important;line-height: 0.8 !important">Patient ID :</td>
                    <td style="background: #eee !important;text-align: left !important;font-weight:600 !important;line-height: 0.8 !important">{{ $patient->patient_id }}</td>
                </tr>
                <tr>
                    <td style="background: #eee !important;line-height: 0.8 !important">Address :</td>
                    <td style="background: #eee !important;text-align: left !important;font-weight:600 !important;line-height: 0.8 !important" colspan="5">
                    {{ !empty($patient->street_address)?$patient->street_address.',':'' }}
                    {{ !empty($patient->area)?$patient->area.',':'' }}
                    {{ !empty($patient->city)?$patient->city:'' }}
                    </td>
                </tr>
                <tr>
                    <td style="background: #eee !important;line-height: 0.8 !important">As on date :</td>
                    <td width="15%" style="background: #eee !important;text-align: left !important;font-weight:600 !important;line-height: 0.8 !important">{{ $schedules->max('schedule_date') !=null ?$schedules->max('schedule_date')->format('d-m-Y'):'' }}</td>
                    <td width="12%" style="background: #eee !important;line-height: 0.8 !important">Phone :</td>
                    <td style="background: #eee !important;text-align: left !important;font-weight:600 !important;line-height: 0.8 !important">{{ $patient->contact_number }}</td>
                    <td style="background: #eee !important;line-height: 0.8 !important">Email :</td>
                    <td style="background: #eee !important;text-align: left !important;font-weight:600 !important;line-height: 0.8 !important">{{ $patient->email }}</td>
                </tr>
            </table>

            <table class="table services-table" style="height:auto !important;margin-top: 0px !important;margin-bottom: 10px !important;vertical-align:top;">
                <thead style="font-weight:500">
                    <th class="text-center border-left border-top border-right border-bottom text-black" style="background: #eee !important;color: #333;padding: 8px !important;font-size:12px !important;font-weight:600">Description of Service</th>
                    <th width="15%" class="text-center border-top border-right border-bottom text-black" style="background: #eee !important;color: #333;padding: 8px !important;font-size:12px !important;font-weight:600">Duration</th>
                    <th width="15%" class="text-center border-top border-right border-bottom text-black" style="background: #eee !important;color: #333;padding: 8px !important;font-size:12px !important;font-weight:600">Rate</th>
                    <th width="15%" class="text-center border-top border-right border-bottom text-black" style="background: #eee !important;color: #333;padding: 8px !important;font-size:12px !important;font-weight:600">Tax amount</th>
                    <th width="20%" class="text-center border-left border-top border-right border-bottom text-black" style="background: #eee !important;color: #333;padding: 8px !important;font-size:12px !important;font-weight:600">Amount</th>
                </thead>
                <tbody>
                    {? $total = 0; ?}
                    {? $serviceCount = 0; ?}
                    @if(count($summary))
                        <tr>
                            <td class="border-left border-right" style="line-height:0.5 !important;color: #000;font-weight: 500 !important;font-size: 15px">Services</td>
                            <td class="border-left border-right"></td>
                            <td class="border-left border-right"></td>
                            <td class="border-left border-right"></td>
                            <td class="border-left border-right"></td>
                        </tr>
                        @foreach ($summary as $month => $serviceItem)
                            @if(count($serviceItem))
                                <tr>
                                    <th class="border-left border-right" style="padding-left:20px !important;line-height:1 !important;padding-top:6px !important;padding-bottom:6px !important;color: #555 !important">{{ $month }}</th>
                                    <td class="text-center border-right" style="padding-left:20px !important;line-height:1 !important"></td>
                                    <td class="text-center border-right" style="padding-left:20px !important;line-height:1 !important"></td>
                                    <td class="text-center border-right" style="padding-left:20px !important;line-height:1 !important"></td>
                                    <td class="text-right border-right" style="padding-left:20px !important;padding-right: 20px !important;line-height:0.8 !important"></td>
                                </tr>
                                @foreach ($serviceItem as $item)
                                <tr>
                                    <td class="border-left border-right" style="padding-left:40px !important;line-height:0.8 !important">{{ ucwords($item['service_name']) }} - <br><small>{{ $item['from_date'].' to '.$item['to_date'] }}</small></td>
                                    <td class="text-center border-right" style="padding-left:20px !important;line-height:0.8 !important">{{ $item['chargeable'] }}</td>
                                    <td class="text-center border-right" style="padding-left:20px !important;line-height:0.8 !important">{{ $item['rate'] }}</td>
                                    <td class="text-center border-right" style="padding-left:20px !important;line-height:0.8 !important">{{ $item['tax_amount'] }}</td>
                                    <td class="text-right border-right" style="padding-left:20px !important;padding-right: 20px !important;line-height:0.8 !important">{{ number_format($item['amount'],2) }}</td>
                                </tr>
                                {? $total += floatval($item['amount']); ?}
                                {? $serviceCount += $item['chargeable']; ?}
                                @endforeach
                            @endif
                        @endforeach
                    @endif

                    {? $billableArray = []; ?}
                    {? $totalBillables = 0; ?}
                    {? $creditTotal = 0; ?}
                    {? $debitTotal = 0; ?}
                    {? $totalReceipts = 0; ?}
                    {? $totalRefunds = 0; ?}
                    @if($to_period)
                        {? $billables = \App\Entities\CaseBillables::where('created_at','<=',$to_period)->whereIn('lead_id', \App\Entities\Lead::wherePatientId($patient->id)->pluck('id')->toArray())->get(); ?}
                    @else
                        {? $billables = \App\Entities\CaseBillables::whereIn('lead_id', \App\Entities\Lead::wherePatientId($patient->id)->pluck('id')->toArray())->get(); ?}
                    @endif
                    @if(count($billables))
                        {? $taxSlab = 0; ?}
                        @foreach ($billables as $billable)
                <?php
                            $item = $billable->item;
                            if($billable->item_id != null){
                                switch ($billable->category) {
                                    case 'Consumables': $item = $billable->consumable->consumable_name; $taxSlab = $billable->consumable->tax->tax_rate; break;
                                    case 'Equipments': $item = $billable->surgical->surgical_name; $taxSlab = $billable->surgical->tax->tax_rate; break;
                                }
                            }
                            $amount = $billable->amount;
                            $billableArray[] = [
                                'item' => $item.' - '.$billable->category,
                                'quantity' => $billable->quantity,
                                'rate' => $billable->rate,
                                'tax_amount' => $amount - ($billable->rate * $billable->quantity),
                                'amount' => $amount,
                            ];
                            $totalBillables += floatval($amount);
                ?>
                        @endforeach
                    @endif
                    @if(count($billableArray))
                        <tr>
                            <td class="border-left border-right" style="line-height:0.5 !important;color: #000;font-weight: 500 !important;font-size: 15px;padding-top:20px !important">Consumables</td>
                            <td class="border-left border-right"></td>
                            <td class="border-left border-right"></td>
                            <td class="border-left border-right"></td>
                            <td class="border-left border-right"></td>
                        </tr>
                        @foreach ($billableArray as $ba)
                            <tr>
                                <td class="border-left border-right" style="padding-left:20px !important;line-height:0.8 !important">{{ ucwords($ba['item']) }}</td>
                                <td class="text-center border-right" style="padding-left:20px !important;line-height:0.8 !important">{{ $ba['quantity'] }}</td>
                                <td class="text-center border-right" style="padding-left:20px !important;line-height:0.8 !important">{{ $ba['rate'] }}</td>
                                <td class="text-center border-right" style="padding-left:20px !important;line-height:0.8 !important">{{ $ba['tax_amount'] }}</td>
                                <td class="text-right border-right" style="padding-left:20px !important;padding-right: 20px !important;line-height:0.8 !important">{{ number_format($ba['amount'],2) }}</td>
                            </tr>
                        @endforeach
                    @endif

                    {? $totalPayments = 0; ?}
                    @if(count($payments) || count($credit_memos))
                        <tr>
                            <td class="border-left border-right" style="line-height:0.5 !important;color: #000;font-weight: 500 !important;font-size: 15px;padding-top:20px !important">Payments</td>
                            <td class="border-left border-right"></td>
                            <td class="border-left border-right"></td>
                            <td class="border-left border-right"></td>
                            <td class="border-left border-right"></td>
                        </tr>
                    @endif

                    @if($payments->sum('total_amount') > 0)
                        <tr>
                            <td class="border-left border-right" style="padding-left:20px !important;line-height:0.8 !important">Payment Received</td>
                            <td class="border-left border-right" style="line-height:0.8 !important"></td>
                            <td class="border-left border-right" style="line-height:0.8 !important"></td>
                            <td class="border-left border-right" style="line-height:0.8 !important"></td>
                            {? $totalPayments += floatVal($payments->sum('total_amount')); ?}
                            <td class="text-right border-left border-right" style="padding-right: 20px !important;line-height:0.8 !important">{{ number_format($payments->sum('total_amount'),2) }}</td>
                        </tr>
                    @endif

                    @if(count($credit_memos))
                        @foreach ($credit_memos as $credit)
                            @if($credit->type != 'Schedule')
                                <tr>
                                    <td class="border-left border-right" style="padding-left:20px !important;line-height:0.8 !important">Memo No. - {{ $credit->credit_memo_no }} on {{ $credit->date->format('d-m-Y') }} <br><br> <b>{{ $credit->comment }}</b></td>
                                    <td class="border-left border-right" style="line-height:0.8 !important"></td>
                                    <td class="border-left border-right" style="line-height:0.8 !important"></td>
                                    <td class="border-left border-right" style="line-height:0.8 !important"></td>
                                    @if(empty($credit->receipt))
                                        @if($credit->amount >= 0)
                                        {? $creditTotal += floatVal($credit->amount); ?}
                                        @else
                                        {? $debitTotal += floatVal($credit->amount); ?}
                                        @endif
                                    @endif
                                    <td class="text-right border-left border-right" style="padding-right: 20px !important;line-height:0.8 !important">{{ $credit->amount }}</td>
                                </tr>
                            @endif
                            @if($credit->type == 'Schedule' && $credit->invoices->count() == 0 && $credit->receipt)
                                {? $creditTotal -= floatVal($credit->amount); ?}
                            @endif
                        @endforeach
                    @endif
                    
                    @if(count($receipts))
                        @foreach ($receipts as $receipt)
                            <tr>
                                <td class="border-left border-right" style="padding-left:20px !important;line-height:0.8 !important">Refunds vide Memo No. - {{ $receipt->creditMemo->credit_memo_no }}-{{ $receipt->receipt_date->format('d-m-Y') }}</td>
                                <td class="border-left border-right" style="line-height:0.8 !important"></td>
                                <td class="border-left border-right" style="line-height:0.8 !important"></td>
                                <td class="border-left border-right" style="line-height:0.8 !important"></td>
                                {{-- {? $totalRefunds += floatVal($receipt->receipt_amount);?} --}}
                                <td class="text-right border-left border-right" style="padding-right: 20px !important;line-height:0.8 !important">{{ $receipt->receipt_amount }}</td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
                {{-- {{dump($total .'+'. $totalBillables .'-'. $totalPayments .'-'. $creditTotal .'+'. abs($debitTotal) .'-'. $totalRefunds)}} --}}
                <tfoot>
                    <tr>
                        <td class="border-right border-left border-top border-bottom" style="padding: 8px !important;background: #eee !important"></td>
                        <th class="text-center border-right border-top border-bottom" style="padding: 8px !important;background: #eee !important">{{ $serviceCount }}</th>
                        <td class="border-right border-top border-bottom" style="padding: 8px !important;background: #eee !important"></td>
                        <td class="border-right border-top border-bottom" style="padding: 8px !important;background: #eee !important"></td>
                        <th class="text-right border-right border-top border-bottom" style="padding: 8px !important;padding-right: 20px !important;background: #eee !important">
                        {{ $fmt->formatCurrency(($total + $totalBillables - $totalPayments - $creditTotal + abs($debitTotal) - $totalRefunds), "INR") }}</th>
                    </tr>
                </tfoot>
            </table>

            <table class="table" style="margin-bottom: 10px !important">
                <tr>
                    <td width="10%" class="text-center border-left border-right border-top border-bottom text-black" style="height: 60px;background: #eee !important;vertical-align:middle;font-size:16px;color:#333">Mode of Payment</td>
                    <td width="35%" class="text-center border-right border-top border-bottom" style="height: 60px;padding: 0 !important">
                        Online Transaction
                        <table class="no-padding-border text-left" style="margin-left:2%;margin-top:1%">
                            <tr>
                                <td>{{ $branch->bank_name }}</td>
                            </tr>
                            <tr>
                                <td>{{ $branch->bank_address }}</td>
                            </tr>
                            <tr>
                                <td><b>Account No: </b>{{ $branch->acc_num }}</td>
                            </tr>
                            <tr>
                                <td><b>IFSC Code: </b>{{ $branch->ifsc_code }}</td>
                            </tr>
                        </table>
                    </td>
                    @if($pluginEnabled && floatVal($total) - ($totalPayments) > 0)
                    <td width="18%" class="text-center border-right border-top border-bottom" style="height: 60px;padding: 0 !important;display:none">
                        Payment Gateway Link
                        <div style="border-radius:5px;display:block;margin:0 auto !important;padding:7px 0 0 0;height:40px;margin-left:5%;text-align:center !important;overflow:hidden !important">
                        <a alt="Pay Invoice" style="color:#666 !important;text-align:center;display:block;font-size:16px;font-weight:500;text-decoration:none" target="_blank" href="{{ route('payment.invoice-payment',['t_id' => session('tenant_id'),'b_id' => session('branch_id'),'pid' => Helper::encryptor('encrypt',$patient->id),'amt' => Helper::encryptor('encrypt',(floatVal($total) - ($totalPayments))),'type' => Helper::encryptor('encrypt','SOA')]) }}">
                            Click here
                        </a></div>
                    </td>
                    @endif
                    <td width="30%" class="text-center border-right border-top border-bottom" style="height: 60px;padding: 0 !important">
                        Cheque / DD (in favour of)<br>
                        <h6 style="margin-top:10px !important;padding:5px !important;font-weight:500 !important">{{ strtoupper(Helper::getSetting('invoice_payee')) }}</h6>
                    </td>
                </tr>
            </table>

            <div style="background: #eee !important;width: 58%;height: 80px !important;display:inline-block;vertical-align:top;padding: 4px !important">
                <ol style="margin-left: -5% !important;margin-top:2% !important;font-size:11px !important;line-height: 16px">
                    {{-- <li>Payment has to be made within 4 days of receipt of invoice. Delayed payment shall attract interest at 18% p.a.</li> --}}
                    <li>Company reserves the right to terminate services for any delay in payment</li>
                    <li>Any issue in the invoice to be raised within 48 hours of receipt of invoice</li>
                    {{-- <li>For queries relating to billing, please write to billdesk@onelifehealthcare.in</li> --}}
                </ol>
            </div>
            <div style="background: #eee !important;width: 40%;height: 80px !important;display:inline-block;vertical-align:top;margin-left: 10px;padding: 0px 4px !important;text-align:center !important">
                <h4 style="font-size:11px !important;font-weight: 500 !important;color:#666 !important">for {{ strtoupper($profile->organization_name) }}</h4>
                <h6 class="sig" style="font-size:10px !important;text-align: center;margin-top: 12% !important; margin-bottom: 0 !important;font-weight: 400 !important">AUTHORISED SIGNATORY</h6>
            </div>

            <br><br>

            <div style="position:relative;width:100% !important;background:#eee !important;height: 30px !important;color: #333 !important;text-align:center !important;line-height: 30px;font-size: 14px">
                PAN : {{ $branch->pan_number }} / GSTIN : {{ $branch->gstin }} / CIN : {{ $branch->cin }}
            </div>

            <div class="footers">
                <div class="inv-foot-add" style="position: relative;width:100% !important;color: #666 !important;text-align:center !important;line-height: 25px;font-size: 14px;margin-top:10px !important;">
                    {{ strtoupper($profile->organization_name) }}<br>
                    <div style="color: #333 !important;font-size:12px !important;line-height:16px !important;position: relative;">
                        {{ $branch->branch_address.','.$branch->branch_area.','.$branch->branch_city }}<br>
                        <b>Ph:</b> {{ $branch->branch_contact_number }}  <b>Website:</b> {{ $profile->website }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="break"></div>

    @if(count($schedules) && $soa_annexure)
    <div class="pagess A4" style="margin-top: 0.1in !important">
        @include('billing.soa-annexure')
    </div>
    @endif

    <div class="break"></div>

    @if(count($payments) && $soa_payments)
    <div class="pagess A4" style="margin-top: 0.1in !important">
        @include('billing.soa-payments')
    </div>
    @endif

@yield('plugin.scripts')

@yield('page.scripts')
</body>

</html>
