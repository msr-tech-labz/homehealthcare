@extends('layouts.main-layout')

@section('page_title','Create Service Invoice - Dashboard | ')

@section('active_billings','active')

@section('plugin.styles')
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<style>
    @keyframes blink {
        50% { border-color: #ff0000; border:1px solid #ff0000; padding: 0 5px;}
    }
    .blink{
        animation: blink .9s step-end infinite alternate;
        z-index: 9999 !important;
    }
    .card{
        margin-bottom: 20px !important;
    }
    address{
        margin-bottom: 0 !important;
    }
    .card .header{
        padding: 8px 12px !important;
        background: #dadfe8;
        font-size: 14px;
        font-weight: 600;
        color: #555 !important;
    }
    .form-control-label, .form-group{
        margin-bottom: 0px !important;
    }
    .form-group > input.form-control, .form-line > input.form-control{
        border: 1px solid #ccc !important;
        box-shadow: rgba(0, 0, 0, 0.075) 0px 1px 1px inset !important;
        padding: 2px 5px !important;
        height: 30px !important;
    }
    .card .body .col-xs-8, .card .body .col-sm-8, .card .body .col-md-8, .card .body .col-lg-8{
        margin-bottom: 10px !important
    }
    .no-margin{
        margin: 0 !important;
    }
    .col-lg-12, .col-lg-6, .col-lg-4{
        margin-bottom: 0 !important
    }
    .table{
        margin-bottom: 0 !important;
    }
    .table tbody tr td, .table tbody tr th, .table thead tr th{
        padding: 5px !important;
        vertical-align: middle !important;
    }
    .table thead{
        border-top: none !important;
    }
    .itemsTable thead tr th, .itemsTable tbody tr td, .lessTable tr th, .lessTable tr td{
        font-size: 12px !important;
        vertical-align: middle !important;
        text-transform: none !important;
    }
    .statsTable tbody tr td, .statsTable tbody tr th {
        padding: 5px 15px !important;
        font-size: 15px !important;
        vertical-align: middle !important;
    }
    .plus{
        height: 20px;
        margin-top: -3px;
        margin-right: 2px;
    }
    .modal .modal-header{
        padding: 10px 20px 5px 20px !important
    }
    .loading {
        margin-left: -2% !important;
        position: absolute;
        z-index: 2;
        height: 90%;
        width: 95%;
        background: #eee;
        text-align: center !important;
    }
</style>
@endsection

@section('content')
<div class="row clearfix" style="margin:auto 0 !important">
    <div class="col-lg-12">
        <div class="card">
            <div class="body" style="padding: 5px 5px !important">
                <div class="col-md-1 text-left" style="width:5% !important; margin-bottom: 0 !important">
                    <a class="btn btn-primary btn-circle waves-effect waves-circle" href="{{ route('billing.index') }}"><i class="material-icons">arrow_back</i></a>
                </div>
                <div class="col-md-8">
                    <h4 class="card-title">Create Invoice - {{ $patient->full_name.' ['.$patient->patient_id.']' }}</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12">
        <!-- Invoice Details Card -->
        <div class="card">
            <div class="header">
                Invoice Details
            </div>
            <div class="body">
                <div class="row">
                    <!-- Patient details -->
                    <div class="col-md-6 no-margin" style="padding-left: 30px">
                        <b>Customer</b><br>
                        @if(isset($patient))
                        {{ preg_replace('/ .$/', '', $patient->full_name) }}<br>
                        (<i>Patient ID: <b>{{ $patient->patient_id }}</b></i>)
                        <address>{!! $patient->street_address !!}</address>
                        Phone: {{ $patient->contact_number ?? '-' }}<br>
                        Email: {{ $patient->email ?? '-' }}<br>
                        @endif
                    </div>

                    <!-- Invoice details -->
                    <div class="col-md-6 no-margin">
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 form-control-label required">
                                <label for="invoice_no">Invoice Number</label>
                            </div>
                            <div class="col-lg-4 col-md-8 col-sm-8 col-xs-7">
                                <div class="input-group" style="margin-bottom: 0 !important">
                                    <span class="input-group-addon" style="padding-right: 5px !important">{{ $settings['invoicePrefix'] }}</span>
                                    <input type="text" style="height:30px !important;padding: 0 2px !important;border: 1px solid #ccc !important;" id="invoice_no" name="invoice_no" class="form-control" placeholder="{{ ($settings['invoiceStart'] + $settings['invoiceCounts'] + 1 ) }}" required="" value="{{ ($settings['invoiceStart'] + $settings['invoiceCounts'] + 1 ) }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 form-control-label required">
                                <label for="invoice_date">Invoice Date</label>
                            </div>
                            <div class="col-lg-4 col-md-8 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <input type="text" id="invoice_date" name="invoice_date" class="form-control datepicker" placeholder="Invoice Date" />
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 form-control-label required">
                                <label for="due_date">Due Date</label>
                            </div>
                            <div class="col-lg-4 col-md-8 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <input type="text" id="due_date" name="due_date" class="form-control datepicker" placeholder="Due Date" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Services / Products  Card -->
        <div class="card">
            <div class="header">
                Services / Products
                <div class="pull-right blink" style="margin-right: 20px;">
                    <a style="line-height: 25px;" class="btnAdd" href="#" onclick="return false;"><img class="plus" src="{{ asset('img/icons/icon_plus.png') }}"/>Add Item</a>
                </div>
            </div>
            <div class="body">
                <div class="row clearfix">
                    <div class="col-lg-12">
                        <table class="table table-bordered table-condensed itemsTable">
                            <thead>
                                <tr>
                                    <th width="25%">Description</th>
                                    <th width="15%" class="text-center">Period</th>
                                    <th class="text-center">Duration/Qty</th>
                                    <th class="text-center">Rate/Item</th>
                                    <th class="text-center">Discount</th>
                                    <th class="text-center">CGST</th>
                                    <th class="text-center">SGST</th>
                                    <th class="text-right">Total</th>
                                    <th width="2%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="no_item_row">
                                    <th colspan="10" style="color:red">No Items Added!</th>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr style="display:none">
                                    <th colspan="4" class="text-right">Total</th>
                                    <th class="text-center"></th>
                                    <th class="text-center"></th>
                                    <th class="text-center"></th>
                                    <th class="text-right"></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Invoice Totals  Card -->
        <div class="card">
            <div class="header">
                Invoice Totals
            </div>
            <div class="body">
                <div class="row clearfix">
                    <div class="col-lg-6" style="width: 50% !important">
                        <table class="table table-bordered table-condensed lessTable creditMemo">
                            <caption class="text-center"><b>Credit Memo</b></caption>
                            <thead>
                                <tr>
                                    <th width="12%"></th>
                                    <th class="text-center" width="25%">Date</th>
                                    <th>Description</th>
                                    <th class="text-right" width="20%">Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(isset($patient->unbilledCreditMemoService) && count($patient->unbilledCreditMemoService))
                                @foreach ($patient->unbilledCreditMemoService->where('type','Others') as $creditMemo)
                                <tr>
                                    <td class="text-center">
                                        <input type="checkbox" name="creditMemo" id="creditMemo_{{ $creditMemo->id }}"  class="with-gap radio-col-deep-purple credit-memo" data-type="Schedule" value="{{ $creditMemo->id }}" data-date="{{ $creditMemo->date->format('d-m-Y') }}" data-comment="{{ $creditMemo->comment }}" data-amount="{{ $creditMemo->amount }}" />
                                        <label for="creditMemo_{{ $creditMemo->id }}" style="vertical-align: middle !important;height:20px !important"></label>
                                    </td>
                                    <td class="text-center">{{ $creditMemo->date->format('d-m-Y') }}</td>
                                    <td>{{ $creditMemo->comment }}</td>
                                    <td class="text-right">{{ $creditMemo->amount }}</td>
                                </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-3" style="float: right">
                        <table class="table table-bordered statsTable">
                            <tr>
                                <th width="60%">Taxable Amount</th>
                                <td class="sub_total text-right" data-amt="0">0.00</td>
                            </tr>
                            <tr>
                                <th>Less</th>
                                <td class="less_amt text-right" data-amt="0">0.00</td>
                            </tr>
                            <tr>
                                <th>Taxed Amount</th>
                                <td class="taxed_amount text-right" data-amt="0">0.00</td>
                            </tr>
                            <tr>
                                <th>Grand Total</th>
                                <td class="grand_total text-right" data-amt="0" style="font-size:16px !important; font-weight:bold !important">0.00</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="justify-content: center;display: flex;padding-bottom: 15px;">
<button type="button" class="btn btn-success waves-effect saveInvoice">CREATE SERVICE INVOICE</button>
</div>

<!-- Add Item Modal -->
<div class="modal fade" id="addItemModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content" >
            <div class="modal-header bg-blue hidden-print">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="smallModalLabel">Add Item</h4>
            </div>
            <div class="modal-body">
                <div class="loading" style="display:none">
                    <img src="{{ asset('img/loading.gif') }}" style="vertical-align:middle !important;height: 120px !important;margin-top: 14% !important"/>
                </div>
                <div id="modalForm" class="form-horizontal">
                    <div class="row clearfix" style="margin-bottom: 8px !important">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4 form-control-label">
                            <label for="type">Category</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 text-left" style="margin-top: 0.5%">
                            <div class="form-group" style="margin-top: 1%">
                                <input name="category" type="radio" id="category_services" class="with-gap radio-col-deep-purple" value="Services"/>
                                <label class="col-md-4" for="category_services">Services</label>
                                <input name="category" type="radio" id="category_billables" class="with-gap radio-col-deep-purple" value="Billables"/>
                                <label class="col-md-4" for="category_billables">Billables</label>
                                <input name="category" type="radio" id="category_custom" class="with-gap radio-col-deep-purple" value="Custom"/>
                            </div>
                        </div>
                    </div>

                    <div id="servicesBlock" style="display:none">
                        <div class="row clearfix" style="margin-bottom: 12px !important">
                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4 form-control-label">
                                <label for="service_id">Service</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control show-tick" name="service_id" data-live-search="true">
                                            <option value="">-- Select Service --</option>
                                            @if(isset($patient->serviceOrders) && count($patient->serviceOrders))
                                            <?php
                                            $serviceOrdersGrouped = $patient->serviceOrders->groupBy('gross_rate')->transform(function($item, $k) {
                                                return $item->groupBy('tax_rate');
                                            });
                                            ?>
                                                @foreach($serviceOrdersGrouped as $group => $serviceOrdersSubGroup)
                                                    @foreach($serviceOrdersSubGroup as $group => $serviceOrder)
                                                    <?php
                                                    $datesArray = [];
                                                    $generateOption = false;
                                                    $serviceOrderIdArray = [];
                                                    $serviceOrderGroupedTaxRate = null;
                                                    $serviceOrderGroupedGrossRate = null;
                                                    $serviceOrderGroupedServiceName = null;
                                                    foreach ($serviceOrder as $group => $order) {
                                                        $status = $order->statusUnbilledService;
                                                        if($status){
                                                            $generateOption = true;
                                                            $serviceOrderIdArray[] = $order->id;
                                                            $serviceOrderGroupedTaxRate = $order->tax_rate;
                                                            $serviceOrderGroupedGrossRate = $order->gross_rate;
                                                            $serviceOrderGroupedServiceName = $order->serviceRequest->service->service_name;

                                                            if(!empty($order->from_date))
                                                                $datesArray[] = strtotime($order->from_date);

                                                            if(!empty($order->to_date))
                                                                $datesArray[] = strtotime($order->to_date);

                                                            if(!empty($order->custom_dates)){
                                                                $dates = explode(',',$order->custom_dates);
                                                                foreach ($dates as $date) {
                                                                    $datesArray[] = strtotime($date);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    $serviceOrderIdArray = implode(',',$serviceOrderIdArray);
                                                    if($datesArray)
                                                        $orderPeriod = ' | '.date('d-m-Y', min($datesArray)).' to '. date('d-m-Y', max($datesArray));
                                                    ?>
                                                        @if($generateOption)
                                                            <option style="background:aquamarine" value="{{ $serviceOrderIdArray }}" data-rate="{{ $serviceOrderGroupedGrossRate }}" data-tax-rate="{{ $serviceOrderGroupedTaxRate }}" data-schedules>{{ $serviceOrderGroupedServiceName }}{{ $orderPeriod }}</option>
                                                        @endif
                                                    @endforeach
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix" style="margin-bottom: 8px !important">
                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4 form-control-label">
                                <label for="item_type">Get Duration From</label>
                            </div>
                            <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8 text-left" style="margin-top: 0.6%">
                                <div class="form-group" style="margin-top: 1%">
                                    <input name="item_type" type="radio" id="item_completed_schedule" class="with-gap radio-col-deep-purple" value="Completed Schedule"/>
                                    <label class="col-md-5" for="item_completed_schedule">Schedules(Completed)</label>
                                </div>
                            </div>
                        </div>
                        <div id="schedulePeriodBlock" class="row clearfix" style="margin-bottom: 8px !important;display:none">
                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4 form-control-label">
                                <label for="period">Filter by Period</label>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-8">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control datepicker" name="from_date" placeholder="From Date"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-8">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control datepicker" name="to_date" placeholder="To Date"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-8 text-left">
                                <a class="btn btn-block btn-sm btn-warning btnGetSchedules">Get</a>
                            </div>
                        </div>
                        <hr style="margin:10px 0 !important;border-top:1px solid #ccc !important">
                        <div class="row clearfix" style="margin-bottom: 12px !important">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
                                <label for="services_period">Period</label>
                            </div>
                            <div class="col-lg-8 col-md-4 col-sm-4 col-xs-8" style="margin-top:1.2%">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="form-control-label services_period" style="color:#2296f4 !important">-</label>
                                        <input type="hidden" name="services_period" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix" style="margin-bottom: 12px !important">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label required">
                                <label for="services_duration">Duration</label>
                            </div>
                            <div class="col-lg-8 col-md-4 col-sm-4 col-xs-8" style="margin-top:1.2%">
                                <div class="form-group">
                                    <div class="col-md-6 servicesDurationInput" style="display:none;">
                                        <div class="form-line">
                                            <input type="number" class="form-control advanceRateCalc" name="advance_duration" style="marin-right: 2px" />
                                            <input type="hidden" name="services_duration" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="form-control-label services_duration" style="color:#2296f4 !important">-</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix" style="margin-bottom: 12px !important">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label required">
                                <label for="services_rate">Rate</label>
                            </div>
                            <div class="col-lg-8 col-md-4 col-sm-4 col-xs-8" style="margin-top:1.2%">
                                <div class="form-group">
                                    <div class="col-md-6 servicesRateInput" style="display:none;">
                                        <div class="form-line">
                                            <input type="number" class="form-control advanceRateCalc" name="advance_rate" style="marin-right: 2px" />
                                            <input type="hidden" name="services_rate" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="form-control-label services_rate" style="color:#2296f4 !important">-</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix" style="margin-bottom: 12px !important">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
                                <label for="services_amt">Actual Amount</label>
                            </div>
                            <div class="col-lg-8 col-md-4 col-sm-4 col-xs-8" style="margin-top:1.2%">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="form-control-label services_amt" style="color:#2296f4 !important">-</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix" style="margin-bottom: 12px !important">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
                                <label for="schedules_amount">Schedules Amount</label>
                            </div>
                            <div class="col-lg-8 col-md-4 col-sm-4 col-xs-8" style="margin-top:1.2%">
                                <div class="form-group">
                                    <div class="col-md-6 schedulesAmount" style="display:none;">
                                        <div class="form-line">
                                            <input type="number" class="form-control schedulesAmount" name="advance_amount" style="marin-right: 2px" />
                                            <input type="hidden" name="schedules_amount" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="form-control-label schedules_amount" style="color:#2296f4 !important">-</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix" style="margin-bottom: 12px !important">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
                                <label for="discountedAmount">Discounted Amount</label>
                            </div>
                            <div class="col-lg-8 col-md-4 col-sm-4 col-xs-8" style="margin-top:1.2%">
                                <div class="form-group">
                                    <div class="col-md-6 discountedAmount" style="display:none;">
                                        <div class="form-line">
                                            <input type="number" class="form-control discounted_amount" name="advance_discounted_amount" style="marin-right: 2px" />
                                            <input type="hidden" name="discounted_amount" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="form-control-label discounted_amount" style="color:#2296f4 !important">-</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="billablesBlock" style="display:none">
                        <div class="row clearfix" style="margin-bottom: 12px !important">
                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4 form-control-label required">
                                <label for="billable_id">Billable Item</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control show-tick" name="billable_id" data-live-search="true">
                                            <option value="">-- Select Item --</option>
                                        @if(isset($patient->billables) && count($patient->billables))
                                            @foreach ($patient->billables->where('service_status','Unbilled') as $billable)
                                            <?php
                                                switch($billable->category){
                                                    case 'Consumables':
                                                        $item = $billable->consumable->consumable_name.' [Consumable]';
                                                        $taxType = $billable->consumable->tax->type;
                                                        $taxRate = $billable->consumable->tax->tax_rate;
                                                        break;
                                                    case 'Equipments':
                                                        $item = $billable->surgical->surgical_name.' [Equipments]';
                                                        $taxType = $billable->surgical->tax->type;
                                                        $taxRate = $billable->surgical->tax->tax_rate;
                                                        break;
                                                    default:
                                                        $item = $billable->item.' [Others]';
                                                        $taxType = "";
                                                        $taxRate = "";
                                                        break;
                                                }
                                            ?>
                                                <option value="{{ $billable->id }}" data-qty="{{ $billable->quantity }}" data-rate="{{ $billable->rate }}" data-tax-type="{{ $taxType }}" data-tax-rate="{{ $taxRate }}">{{ $item }}</option>
                                            @endforeach
                                        @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix" style="margin-bottom: 12px !important">
                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4 form-control-label">
                                <label for="billable_qty">Quantity</label>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-8" style="margin-top:1.2%">
                                <div class="form-group">
                                    <label class="form-control-label billable_qty" style="color:#2296f4 !important">-</label>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix" style="margin-bottom: 12px !important">
                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4 form-control-label">
                                <label for="billable_rate">Rate</label>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-8" style="margin-top:1.2%">
                                <div class="form-group">
                                    <label class="form-control-label billable_rate" style="color:#2296f4 !important">-</label>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix" style="margin-bottom: 12px !important">
                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4 form-control-label">
                                <label for="billable_amt">Amount</label>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-8" style="margin-top:1.2%">
                                <div class="form-group">
                                    <label class="form-control-label billable_amt" style="color:#2296f4 !important">-</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="modal-footer" style="border-top: 1px solid #ccc">
                <button type="button" class="btn btn-success waves-effect btnAddItem">Add Item</button>
                <button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page.scripts')
<!-- Select Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.4.3/plugins/confirmDate/confirmDate.js"></script>
<script type="text/javascript">
    // Warning before leaving the page (back button, or outgoinglink)
    window.onbeforeunload = function() {
       return "Do you really want to leave this page?";
       //if we return nothing here (just calling return;) then there will be no pop-up question at all
       //return;
    };
    var invoiceItems = [];
    var subTotal = 0;
    let fpDate = null;
    $(function(){
        fpDate = $('.datepicker').flatpickr({
            altInput: true,
            altFormat: "d-m-Y",
            dateFormat: "Y-m-d",
            allowInput: false,
            ignoredFocusElements: [window.document.body],
            plugins: [new confirmDatePlugin({})],
            onReady: function (selectedDates, dateStr, instance){
                if(instance.element.name == 'invoice_date'){
                    instance.setDate(new Date());
                }
                if(instance.element.name == 'due_date'){
                    instance.set('minDate',new Date($('#invoice_date').val()));
                    instance.setDate(new Date(new Date($('#invoice_date').val()).getTime()+({{ $settings['paymentDueDate'] }}*24*60*60*1000)));
                }
            },
            onChange: function(selectedDates, dateStr, instance) {
                if(instance.element.id == 'invoice_date'){
                    fpDate[1].set('minDate',new Date($('#invoice_date').val()));
                    fpDate[1].setDate(new Date(new Date($('#invoice_date').val()).getTime()+({{ $settings['paymentDueDate'] }}*24*60*60*1000)));
                }

                if(instance.element.name == 'from_date'){
                    fpDate[3].set('minDate',instance.element.value);
                }

                if(instance.element.name == 'to_date'){
                    calculateServiceRate('Service');
                }

                if(instance.element.name == 'custom_from_date'){
                    fpDate[5].set('minDate',instance.element.value);
                }
            },
            onClose: function(selectedDates, dateStr, instance){
                if(instance.element.name == 'from_date'){
                    if($('#addItemModal #servicesBlock input[name=item_type]:checked').val() == 'Service')
                        fpDate[3].toggle();
                }

                if(instance.element.name == 'custom_from_date'){
                    if($('#addItemModal input[name=category]:checked').val() == 'Custom')
                        fpDate[5].toggle();
                }
            }
        });

        $('.content').on('click','.btnAdd', function(){
            $('#addItemModal input[type=radio][name=category][value=Services]').prop('checked', false);
            $('#addItemModal input[type=radio][name=category][value=Billables]').prop('checked', false);
            $('#addItemModal #servicesBlock').hide();
            $('#addItemModal #billablesBlock').hide();
            clearServiceBlockValues();
            clearBillableBlockValues();

            $('#addItemModal').modal();
        });

        $('.content').on('change','input[type=radio][name=category]', function(){
            if($(this).val() == 'Services'){
                $('#addItemModal #servicesBlock').show();
                $('#addItemModal #billablesBlock').hide();
                clearBillableBlockValues();
            }

            if($(this).val() == 'Billables'){
                $('#addItemModal #servicesBlock').hide();
                $('#addItemModal #billablesBlock').show();
                $('#addItemModal #customBlock').hide();
                clearServiceBlockValues();
            }
        });

        $('.content').on('change','select[name=service_id]', function(){
            calculateServiceRate($('#addItemModal #servicesBlock input[name=item_type]:checked').val());
        });

        $('.content').on('change','input[type=radio][name=item_type]', function(e){
            block = '#addItemModal #servicesBlock';
            if($(block+' select[name=service_id] option:selected').val() != '' && $('#addItemModal input[name=category]:checked').val() == 'Services'){
                if(typeof fpDate != 'undefined' && typeof fpDate[2] != 'undefined') fpDate[2].clear();
                if(typeof fpDate != 'undefined' && typeof fpDate[3] != 'undefined') fpDate[3].clear();
                clearServiceRate();

                if($(this).val() == 'Completed Schedule'){
                    $(block+' #serviceOrderBlock').hide();
                    $(block+' #schedulePeriodBlock').show();

                    $(block+' .servicesDurationInput').hide();
                    $(block+' .servicesRateInput').hide();
                    $(block+' .services_duration').show();
                    $(block+' .services_rate').show();
                }

                return true;
            }else{
                $(block+' input[name=item_type][value="Completed Schedule"]').prop('checked', false);
                alert("Please select service!");
                e.preventDefault();
                e.stopPropagation();
                return false;
            }
        });

        $('.content').on('click', '.btnGetSchedules', function(){
            block = '#addItemModal #servicesBlock';
            srID = $(block+' select[name=service_id] option:selected');
            fromDate = $(block+' #schedulePeriodBlock input[name=from_date]').val();
            toDate = $(block+' #schedulePeriodBlock input[name=to_date]').val();
            pID = {{$patient->id}};

            $.ajax({
                url: '{{ route('ajax.get-schedules-by-status') }}',
                type: 'POST',
                data: {_token: '{{ csrf_token() }}', sr_id: srID.val().split(','), pt_id: pID, status: 'Completed', from_date: fromDate, to_date: toDate},
                success: function (d){
                    if(typeof d.length == 'undefined' || d.length > 0){
                        $(block+' input[name=services_period]').val(d.from+'_'+d.to);
                        $(block+' input[name=services_duration]').val(d.duration);
                        $(block+' input[name=services_rate]').val(srID.data('rate'));
                        $(block+' input[name=services_rate]').data('rate',srID.data('rate'));

                        $(block+' input[name=schedules_amount]').val(d.schedule_total_amount);
                        $(block+' .schedules_amount').html('Rs. '+formatCurrency(d.schedule_total_amount));

                        calculateServiceRate('Completed Schedule');
                    }else{
                        alert("No services scheduled for this period!");
                        return false;
                    }
                },
                error: function (err){
                    console.log(err);
                }
            });
        });

        $('.content').on('change','select[name=billable_id]', function(){
            calculateBillableRate(false);
        });

        /* Add Item Modal button save click handler */
        $('.content').on('click','.btnAddItem', function(){
            category = $('#addItemModal input[name=category]:checked').val();
            switch (category) {
                case 'Services': addServiceItem(); break;
                case 'Billables': addBillableItem(); break;
                default: break;
            }
        });

        /* Delete Item button click handler */
        $('.content').on('click','.btnDeleteItem', function(){
            if(confirm("Are you sure?")){
                rowID = $(this).data('id');
                item = $(this).data('item');
                type = item.split('_')[0];
                typeId = item.split('_')[1];
                if(type == 'service'){
                    block = '#addItemModal #servicesBlock';
                }else if(type == 'billable'){
                    block = '#addItemModal #billablesBlock';
                }
                $(block+' select[name='+ type +'_id] option[value="'+typeId+'"]').prop('disabled', false);
                $(block+' select[name='+ type +'_id]').selectpicker('refresh');

                delete invoiceItems[rowID];
                $('.itemsTable tbody tr[data-id='+rowID+']').remove();

                refreshTable();

                calculateTotal();
            }
        });

        $('.content').on('change','.credit-memo', function(){
            lessAmt = parseFloat($('td.less_amt').attr('data-amt'));
            if($(this).is(':checked')){
                lessAmt -= parseFloat($(this).data('amount'));
            }else{
                lessAmt += parseFloat($(this).data('amount'));
            }
            $('td.less_amt').attr('data-amt',lessAmt);
            if(lessAmt > 0)
                $('td.less_amt').html(formatCurrency(lessAmt));
            else
                $('td.less_amt').html(formatCurrency(lessAmt));

            calculateTotal();
        });
    });

    function formatCurrency(n){
        return parseFloat(n).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    }

    function uniqueID(n){
        if(n > 0){
            characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            charactersLength = characters.length;
            randomString = '';
            for (i = 0; i < n; i++) {
                randomString += characters[Math.floor(Math.random() * ((charactersLength - 1) - 0 + 1)) + 0];
            }
            return randomString;
        }
    }

    function showLoading(){
        $('#addItemModal .loading').show();
    }

    function hideLoading(){
        $('#addItemModal .loading').hide();
    }

    function calculateTotal(){
        discountTotal = 0;
        taxTotal = 0;
        subTotal = 0;

        if(Object.keys(invoiceItems).length >= 0){
            Object.keys(invoiceItems).forEach(function(key) {
                taxTotal += parseFloat(invoiceItems[key].tax_amount);
                subTotal += parseFloat(invoiceItems[key].amount);
                discountTotal += parseFloat(invoiceItems[key].discount);
            });

            $('td.sub_total').attr('data-amt',subTotal - (taxTotal*2));
            $('td.sub_total').html(formatCurrency(subTotal - (taxTotal*2)));

            $('td.taxed_amount').attr('data-amt',taxTotal*2);
            $('td.taxed_amount').html(formatCurrency(taxTotal*2));

            // Update table footer stats
            $('.itemsTable tfoot tr:eq(0) th:eq(1)').html(formatCurrency(discountTotal));
            $('.itemsTable tfoot tr:eq(0) th:eq(2)').html(formatCurrency(taxTotal));
            $('.itemsTable tfoot tr:eq(0) th:eq(3)').html(formatCurrency(taxTotal));
            $('.itemsTable tfoot tr:eq(0) th:eq(4)').html(formatCurrency(subTotal));

            lessAmt = parseFloat($('td.less_amt').attr('data-amt'));

            if((subTotal - lessAmt) >= 0){
                $('td.grand_total').attr('data-amt',(subTotal + lessAmt));
                $('td.grand_total').html(formatCurrency(subTotal + lessAmt));
                $('.saveInvoice').attr('disabled',false);
            }else{
                $('.saveInvoice').attr('disabled','disabled');
            }
        }else{
            $('td.sub_total').attr('data-amt',0);
            $('td.sub_total').html(formatCurrency(0));
            $('td.taxed_amount').attr('data-amt',0);
            $('td.taxed_amount').html(formatCurrency(0));
            $('td.grand_total').attr('data-amt',0);
            $('td.grand_total').html(formatCurrency(0));
            $('.saveInvoice').attr('disabled','disabled');
        }
    }

    function refreshTable(){
        if($('.itemsTable tbody tr.item_row').length > 0){
            $('.itemsTable tbody tr.no_item_row').hide();
            $('.itemsTable tfoot tr').show();
        }else{
            $('.itemsTable tbody tr.no_item_row').show();
            $('.itemsTable tfoot tr').hide();
        }
    }

    /*--------------------------------*/
    /* Service Item functions START   */
    /*--------------------------------*/
    //2
    function calculateServiceRate(type, returnValues=false){
        var data = [];
        block = '#addItemModal #servicesBlock';
        service = $(block+' select[name=service_id] option:selected');

        if(service.val() != '' && typeof type != 'undefined'){
            days = 0;
            rate = 0;
            amount = 0;
            period = '';
            if(type == 'Advance'){
                days = $(block+' input[name=advance_duration]').val();
                rate = $(block+' input[name=advance_rate]').val();
                itemDiscountAmount = 0;
            }

            if(type == 'Completed Schedule'){
                days = $(block+' input[name=services_duration]').val();
                rate = $(block+' input[name=services_rate]').val();
                period = $(block+' input[name=services_period]').val();
                itemDiscountAmount = $('input[name=discounted_amount]').val().replace(',', '');
            }

            amount = (days * rate);

            if(days > 0 && rate > 0 && amount > 0){
                if(returnValues){
                    data = { days: days, rate: rate, amount: amount ,itemDiscountAmount: itemDiscountAmount };
                }
                if(period)
                    $(block+' .services_period').html(period.replace('_',' - '));

                $(block+' .services_duration').html(days+' (days)');
                $(block+' .services_rate').html(rate);
                $(block+' .services_amt').html('Rs. '+formatCurrency(amount));
                if($('input[name=schedules_amount]').val() != ''){
                    $(block+' .discounted_amount').html('Rs. '+formatCurrency(amount - $('input[name=schedules_amount]').val()));
                    $(block+' input[name=discounted_amount]').val(formatCurrency(amount - $('input[name=schedules_amount]').val()));
                }else{
                    $(block+' .discounted_amount').html('Rs. '+formatCurrency(0));
                    $(block+' input[name=discounted_amount]').val(formatCurrency(0));
                }
            }
        }

        return data;
    }

    function clearServiceRate(){
        block = '#addItemModal #servicesBlock';
        $(block+' input[name=services_duration]').val('');
        $(block+' input[name=services_period]').val('');
        $(block+' input[name=schedules_amount]').val('');
        $(block+' input[name=discounted_amount]').val('');
        $(block+' input[name=services_rate]').val($(block+' input[name=services_rate]').data('rate'));
        $(block+' .services_period').html('-');
        $(block+' .services_duration').html('-');
        $(block+' .services_rate').html('-');
        $(block+' .services_amt').html('-');
        $(block+' .schedules_amount').html('-');
        $(block+' .discounted_amount').html('-');
    }

    function clearServiceBlockValues(){
        if(typeof fpDate != 'undefined' && typeof fpDate[2] != 'undefined') fpDate[2].clear();
        if(typeof fpDate != 'undefined' && typeof fpDate[3] != 'undefined') fpDate[3].clear();
        $('#addItemModal #servicesBlock input[name=item_type][value="Unbilled Schedule"]').prop('checked', false);
        $('#addItemModal #servicesBlock input[name=item_type][value="Completed Schedule"]').prop('checked', false);
        $('#addItemModal #servicesBlock select[name=service_id]').selectpicker('val','');
        clearServiceRate();
    }

    function addServiceItem(){
        showLoading();
        block = '#addItemModal #servicesBlock';
        service = $(block+' select[name=service_id] option:selected');
        if(service.val() != ''){
            $(block+' select[name=service_id] option[value="'+service.val()+'"]').prop('disabled', true);
            $(block+' select[name=service_id]').selectpicker('refresh');
            periodTxt = '-';
            period = null;
            serviceId = service.val();
            itemType = $(block+' input[name=item_type]:checked').val();
            description = $(block+' select[name=service_id] option:selected').text();
            serviceRate = calculateServiceRate(itemType, true);
            //1
            if(Object.keys(serviceRate).length >= 3){
                period = $(block+' input[name=services_period]').val();
                if(period){
                    periodTxt = period.replace('_',' to ');
                }

                taxAmount = 0;
                taxData = service.data();

                if(serviceRate.itemDiscountAmount != ''){
                        taxAmount = (parseFloat(serviceRate.amount)-parseFloat(itemDiscountAmount))*((taxData.taxRate/2)/100);
                        taxTxt = formatCurrency(taxAmount) + ' @ ' + (taxData.taxRate/2) + '%';
                        itemFinalAmount = (parseFloat(serviceRate.amount)-parseFloat(itemDiscountAmount)) + (taxAmount*2);
                }else{
                        taxAmount = (serviceRate.amount)*((taxData.taxRate/2)/100);
                        taxTxt = formatCurrency(taxAmount) + ' @ ' + (taxData.taxRate/2) + '%';
                        itemFinalAmount = (serviceRate.amount) + (taxAmount*2);
                }
                rowID = uniqueID(6);
                invoiceItems[rowID] = {
                    'category': 'Services',
                    'item_id': serviceId,
                    'type': itemType,
                    'discount': parseFloat(itemDiscountAmount),
                    'period': period,
                    'tax_amount': taxAmount,
                    'duration': serviceRate.days,
                    'rate': serviceRate.rate,
                    'amount': itemFinalAmount
                };


                html = `<tr class="item_row" data-id="`+ rowID +`">
                    <td class="table_service_ID" data-service-id="`+ serviceId+`" data-service-period="`+ periodTxt +`" data-service-duration="`+ serviceRate.days +`" data-service-rate="`+ serviceRate.rate +`" data-service-tax="`+ taxData.taxRate +`" data-item-type="`+ itemType +`">`+ description +`</td>
                    <td class="text-center">`+ periodTxt +`</td>
                    <td class="text-center">`+ serviceRate.days +`</td>
                    <td class="text-center">`+ serviceRate.rate +`</td>
                    <td class="text-center">`+ $('input[name=discounted_amount]').val() +`</td>
                    <td class="text-center">`+ taxTxt +`</td>
                    <td class="text-center">`+ taxTxt +`</td>
                    <td class="text-right">`+ itemFinalAmount +`</td>
                    <td class="text-center"><a style="line-height: 25px;" class="btnDeleteItem" href="#" onclick="return false;" data-item="service_`+ serviceId +`" data-id="`+ rowID +`"><img class="plus" src="{{ asset('img/icons/icon_minus.jpg') }}"></a></td>
                </tr>`;

                $('.itemsTable tbody').append(html);
                refreshTable();

                $('#addItemModal').modal('hide');
                hideLoading();
                calculateTotal();
            }else{
                alert("Please fill all mandatory(*) fields!");
                hideLoading();
                return false;
            }
        }else{
            alert("Please select service!");
            hideLoading();
            return false;
        }
    }
    /* Service Item functions END */

    /*--------------------------------*/
    /* Billable Item functions START  */
    /*--------------------------------*/
    function calculateBillableRate(returnValues=false){
        var data = [];
        block = '#addItemModal #billablesBlock';
        billable = $(block+' select[name=billable_id] option:selected');

        if(billable.val() > 0){
            qty = 0;
            rate = 0;
            amount = 0;

            billableRate = billable.data();
            if(billableRate){
                qty = billableRate.qty;
                rate = billableRate.rate;
            }

            amount = (qty * rate);

            if(qty > 0 && rate > 0 && amount > 0){
                if(returnValues){
                    data = {qty: qty, rate: rate, amount: amount, itemDiscountAmount: 0};
                }
                $(block+' .billable_qty').html(qty);
                $(block+' .billable_rate').html(rate);
                $(block+' .billable_amt').html('Rs. '+formatCurrency(amount));
            }
        }

        return data;
    }

    function clearBillableRate(){
        block = '#addItemModal #billablesBlock';
        $(block+' .billable_qty').html('-');
        $(block+' .billable_rate').html('-');
        $(block+' .billable_amt').html('-');
    }

    function clearBillableBlockValues(){
        $('#addItemModal #billablesBlock select[name=billable_id]').selectpicker('val','');
        clearBillableRate();
    }

    function addBillableItem(){
        showLoading();
        block = '#addItemModal #billablesBlock';
        billable = $(block+' select[name=billable_id] option:selected');
        if(billable.val() > 0){
            $(block+' select[name=billable_id] option[value='+billable.val()+']').prop('disabled', true);
            $(block+' select[name=billable_id]').selectpicker('refresh');
            taxTxt = '-';
            billableId = billable.val();
            description = billable.text();
            billableRate = calculateBillableRate(true);

            if(Object.keys(billableRate).length >= 3){
                taxAmount = 0;
                taxData = billable.data();
                if(taxData.taxType == 'Percentage'){
                    taxAmount = (amount * ((taxData.taxRate /2)/ 100));
                    taxTxt = formatCurrency(taxAmount) + ' @ ' + (taxData.taxRate/2) + '%';
                    itemFinalAmount = amount+(taxAmount*2);
                }else if(taxData.taxType == 'Fixed'){
                    taxAmount = amount + taxData.taxRate;
                    taxTxt = formatCurrency(taxAmount) + ' @ ' + (taxData.taxRate/2);
                }else{
                    itemFinalAmount = amount;
                }
                rowID = uniqueID(6);
                invoiceItems[rowID] = {
                    'category': 'Billable',
                    'item_id': billableId,
                    'period': null,
                    'discount': parseFloat(billableRate.itemDiscountAmount),
                    'tax_amount': taxAmount,
                    'duration': billableRate.qty,
                    'rate': billableRate.rate,
                    'amount': itemFinalAmount
                };

                html = `<tr class="item_row" data-id="`+ rowID +`">
                    <td class="table_billable_ID" data-billable-id="`+ billableId+`" data-billable-quantity="`+ billableRate.qty +`" data-billable-rate="`+ billableRate.rate +`" data-billable-tax="`+ taxData.taxRate +`">`+ description +`</td>
                    <td class="text-center">-</td>
                    <td class="text-center">`+ billableRate.qty +`</td>
                    <td class="text-center">`+ billableRate.rate +`</td>
                    <td class="text-center">0</td>
                    <td class="text-center">`+ taxTxt +`</td>
                    <td class="text-center">`+ taxTxt +`</td>
                    <td class="text-right">`+ formatCurrency(itemFinalAmount) +`</td>
                    <td class="text-center"><a style="line-height: 25px;" class="btnDeleteItem" href="#" onclick="return false;" data-item="billable_`+ billableId +`" data-id="`+ rowID +`"><img class="plus" src="{{ asset('img/icons/icon_minus.jpg') }}"></a></td>
                </tr>`;

                $('.itemsTable tbody').append(html);
                refreshTable();

                $('#addItemModal').modal('hide');
                hideLoading();
                calculateTotal();
            }else{
                alert("Please fill all mandatory(*) fields!");
                hideLoading();
                return false;
            }
        }else{
            alert("Please select billable item  !");
            hideLoading();
            return false;
        }
    }
    /* Billable Item functions END */

    $('.content').on('click','.saveInvoice',function(){
        var serviceList = [];
        var creditMemoList = [];
        var billableList = [];

        var caseInvoice = {
            'tenant_id': '{{ \Helper::getTenantID() }}',
            'user_id': '{{ \Helper::getUserID() }}',
            'patient_id': '{{ $patient->id }}',
            'invoice_no': $('#invoice_no').val(),
            'invoice_date': $('#invoice_date').val(),
            'invoice_due_date': $('#due_date').val(),
            'taxable_amount':$('.sub_total').attr('data-amt'),
            'less_amount':$('.less_amt').attr('data-amt'),
            'taxed_amount':$('.taxed_amount').attr('data-amt'),
            'total_amount':$('.grand_total').attr('data-amt'),
        };

        $("tr.item_row").each(function() {
            var serviceId = $(this).children(".table_service_ID").data('service-id');
            var servicePeriod = $(this).children(".table_service_ID").data('service-period');
            var serviceDuration = $(this).children(".table_service_ID").data('service-duration');
            var serviceRate = $(this).children(".table_service_ID").data('service-rate');
            var serviceTax = $(this).children(".table_service_ID").data('service-tax');
            var itemType = $(this).children(".table_service_ID").data('item-type');

            serviceList.push({service_id:serviceId, services_period:servicePeriod, service_duration: serviceDuration, service_rate: serviceRate, service_tax: serviceTax, item_type: itemType});
        });

        $("input:checkbox[name=creditMemo]:checked").each(function(){
            if($(this).data('type') == 'Schedule'){
                var ids = $(this).val().replace(/[\[\]']+/g,"").split(',');
                $.each(ids, function(key,val) {
                    creditMemoList.push(val);
                });
            }else{
                creditMemoList.push($(this).val());
            }
        });

        $("tr.item_row").each(function() {
            var billableId = $(this).children(".table_billable_ID").data('billable-id');
            var billableQuantity = $(this).children(".table_billable_ID").data('billable-quantity');
            var billableRate = $(this).children(".table_billable_ID").data('billable-rate');
            var billableTax = $(this).children(".table_billable_ID").data('billable-tax');

            billableList.push({billable_id:billableId, billable_quantity: billableQuantity, billable_rate: billableRate, billable_tax: billableTax});
        });

        if($('#invoice_no').val() != '' && $('#due_date').val() != '' && (serviceList.length != 0 || billableList.length != 0 || creditMemoList.length != 0)){
            if(parseFloat($('.grand_total').attr('data-amt')) >= parseFloat($('.less_amt').attr('data-amt'))){
                $(this).attr('disabled', true);
                $.ajax({
                    url: '{{ route('billing.save-service-invoice') }}',
                    type: 'POST',
                    data: {caseInvoice: caseInvoice, serviceList: serviceList, creditMemoList: creditMemoList, billableList: billableList,_token: '{{ csrf_token() }}'},
                    success: function (){
                        showNotification('bg-green', 'Service Invoice Raised successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                        window.setTimeout(function(){
                            window.onbeforeunload = null;
                            window.location.href = '/billing';
                        },1500);
                    },
                    error: function (error){
                        console.log(error);
                    }
                });
            }else{
                alert('Grand Total cannot be zero or a negative value. Please readjust the Less amounts.');
                $(this).attr('disabled', false);
            }
        }else{
            alert('Please Enter Service Inovice Number, Due Date, and at least one Item.');
        }
    })
</script>
@endsection