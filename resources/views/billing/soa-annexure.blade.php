<div class="annexure" style="padding:10px !important">
    <h4 class="text-center">Annexure</h4>
    <table class="table no-padding-border annexure-table" style="width:100%; margin-top:10px !important; margin-bottom: 10px !important">
        <thead>
            <tr>
                {{-- <th class="border-right border-top text-center" style="color:#333 !important;line-height: 0.8 !important">#</th> --}}
                <th class="border-right border-top text-center" style="color:#333 !important;line-height: 0.8 !important">Service Visit</th>
                <th class="border-right border-top text-center" style="color:#333 !important;line-height: 0.8 !important">Date</th>
                <th class="border-right border-top text-center" style="color:#333 !important;line-height: 0.8 !important">Employee ID</th>
                <th class="border-right border-top text-center" style="color:#333 !important;line-height: 0.8 !important">Employee Name</th>
                <th class="border-right border-top text-center" style="color:#333 !important;line-height: 0.8 !important">Net Rate</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="5" class="border-right border-top text-center" style="font-size: 18px !important;color: #ff0000 !important">Loading data...</th>
            </tr>
        </tbody>
    </table>
</div>

<script>
    var url = '{!! route('billing.get-soa-schedules-ajax').'?pid='.Helper::encryptor('encrypt',$patient->id).'&tPeriod='.$to_period !!}';
    $('.annexure-table').DataTable({
        processing: true,
        dom: '',
        displayLength: -1,
        serverSide: true,
        ajax: url,
        columns: [
            {data : 'service_required', name : 'service_required', searchable: true},
            {data : 'schedule_date', name : 'schedule_date', searchable: true},
            {data : 'caregiver.employee_id', name : 'caregiver.employee_id', searchable: true},
            {data : 'caregiver.first_name', name : 'caregiver.first_name', searchable: true},
            {data : 'rate', name : 'rate'},
        ],
    });
</script>
