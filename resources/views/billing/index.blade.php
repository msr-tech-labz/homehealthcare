@extends('layouts.main-layout')

@section('page_title','Billing - Dashboard | ')

@section('active_billings','active')

@section('plugin.styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
<style>
    .loader {
        border: 5px solid #f3f3f3; /* Light grey */
        border-top: 5px solid #3498db; /* Blue */
        border-radius: 50%;
        margin: 0 auto;
        width: 35px;
        height: 35px;
        margin-top: 5px !important;
        animation: spin 2s linear infinite;
    }

    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
    .plus:after{
        content: ' + ';
        font-size: 18px;
        font-weight: bold;
    }
    .wizard .content{
        min-height: auto !important;
    }
    .wizard > .steps a{
        text-align: left !important;
        border: 1px solid #ccc;
    }
    .nav-justified li a{
        font-size: 16px;
    }
    .nav-justified li:hover{
        background: #eee !important;
    }

    .daterangepicker td.highlight, .daterangepicker option.highlight{
        color: #ff9800 !important;
    }
    .serviceExpiry:hover{
        cursor: pointer;
    }
    .unbilledServiceOrders:hover{
        cursor: pointer;
    }
    .modal-body .table {
        margin-bottom: 0 !important;
    }
    .modal-body .table tr th, .modal-body .table tr td {
        font-size: 12px !important;
        padding: 7px !important;
    }
    .dataTables_length{
        float:left;
    }
</style>
@endsection

@section('content')
<style>
    .tile{
        width: 25% !important;
        padding-right: 5px !important;
        padding-left: 5px !important;
    }
    .info-box .icon{
        padding-top: 5px;
    }
    .info-box .content{
        margin: 1px 5px !important;
    }
    .info-box .content .text{
        margin-top: 5px !important;
    }
    .info-box .content .number{
        font-size: 18px !important;
        margin-top: 0 !important;
    }
    .select2{
        width: 100% !important;
    }
</style>

@ability('admin','billing-stats')
<div class="row clearfix" style="margin-bottom:30px;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hide">
        <small class="pull-right hide" style="font-size: 14px;color: white;">
            For the period <b>01-{{ date('M-Y') }}</b> to <b>{{ cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y')) }}-{{ date('M-Y') }}</b> (All figures are in Rs.)
        </small>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12 tile">
        <div class="info-box bg-pink hover-expand-effect outstandingAmount">
            <div class="icon">
                <i class="material-icons">payment</i>
            </div>
            <div class="content">
                <div class="text">Pending Payments - {{ date('M') }} {{ date('Y') }}</div>
                <div class="number count-to loaders">&#8377; {{ number_format($stats->pending_payments,2) }} </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12 tile">
        <div class="info-box bg-cyan hover-expand-effect statsPaymentsReceived">
            <div class="icon">
                <i class="material-icons">account_balance_wallet</i>
            </div>
            <div class="content">
                <div class="text">Payments Received - {{ date('M') }} {{ date('Y') }}</div>
                <div class="number count-to" data-value="0">&#8377; {{ number_format($stats->payments_received,2) }}</div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12 tile">
        <div class="info-box bg-pink hover-expand-effect serviceExpiry" data-toggle="modal" data-target="#serviceExpiryModal">
            <div class="icon">
                <i class="material-icons">watch_later</i>
            </div>
            <div class="content">
                <div class="text">Next Service Expiry</div>
                <div class="number count-to loaders">0</div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12 tile">
        <div class="info-box bg-red hover-expand-effect unbilledServiceOrders" data-toggle="modal" data-target="#unbilledServiceOrdersModal">
            <div class="icon">
                <i class="material-icons">assignment</i>
            </div>
            <div class="content">
                <div class="text">Unbilled Service Orders</div>
                <div class="number count-to loaders">0</div>
            </div>
        </div>
    </div>
</div>
@endability

<div class="row clearfix">
    <div class="col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('dashboard') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-md-5">
                    Billing
                    <small>Create and manage invoice, payments, statement of account and credit memo</small>
                </h2>
                <div class="col-md-6">
                    @ability('admin','billing-invoice-create')
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#selectPatientModal" class="btn btn-success pull-right" title="Create New Invoice"><i class="material-icons">add</i> New Invoice</a>
                    @endability
                </div>
            </div>
            <div class="body">
                <div class="">
                    <a href="#" data-toggle="modal" data-target="#creditMemoModal" class="btn btn-default"><i class="material-icons">receipt</i> New Credit Memo</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="#" data-toggle="modal" data-target="#creditMemoLogModal" class="btn btn-default"><i class="material-icons">receipt</i>Credit Memo Log</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="#" data-toggle="modal" data-target="#creditMemoRefundModal" class="btn btn-default"><i class="material-icons">receipt</i>Credit Memo Refund</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="#" data-toggle="modal" data-target="#statementOfAccountModal" class="btn btn-default"><i class="material-icons">receipt</i> Statement  of Account</a>&nbsp;&nbsp;&nbsp;&nbsp;
                </div>
                <br>
                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                    <li role="presentation" class="active"><a href="#proforma" data-toggle="tab">PROFORMA</a></li>
                    <li role="presentation"><a href="#service" data-toggle="tab">SERVICE</a></li>
                    <li role="presentation"><a href="#receipt" data-toggle="tab">RECEIPT</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="proforma">
                        <div class="col-lg-12" style="margin-bottom: 0">
                            <div class="col-md-1" style="margin-bottom: 0"><label style="padding-top:5px">Filter by</label></div>
                            <div class="col-md-3" style="margin-bottom: 0">
                                <select class="form-control show-tick" id="filter_patient_proforma" name="filter_patient_proforma" data-live-search="true">
                                    <option value="">All Patient</option>
                                    @if(isset($patients) && count($patients))
                                    @foreach ($patients as $p)
                                    <option value="{{ $p->id }}">{{ $p->full_name.' - '.$p->patient_id }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="col-md-3" style="margin-bottom:0">
                                <div class="input-group">
                                    <span class="input-group-addon">{{ Helper::getSetting('proforma_invoice_inits') }}</span>
                                    <div class="form-line">
                                        <input type="number" class="form-control" id="filter_invoice_no_proforma" placeholder="Invoice No.">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2" style="margin-bottom: 0">
                                <select class="form-control show-tick" id="filter_status_proforma" name="filter_status_proforma" data-live-search="true">
                                    <option value="">All Status</option>
                                    <option value="Pending">Pending</option>
                                    <option value="Partial">Partial</option>
                                    <option value="Paid">Paid</option>
                                    <option value="Adjusted">Adjusted</option>
                                    <option value="Cancelled">Cancelled</option>
                                </select>
                            </div>
                            <div class="col-md-1" style="margin-bottom: 0">
                                <input type="button" class="btn btn-block bg-teal btnFilterProforma" value="Filter">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        @ability('admin','billing-invoice-list')
                        <div id="tableProforma">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered proformaInvoiceTable" style="zoom:85%;">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Patient ID</th>
                                            <th>Patient</th>
                                            <th>Invoice No.</th>
                                            <th>Invoice Date</th>
                                            <th>Amount</th>
                                            <th>Due Date</th>
                                            <th>Status</th>
                                            <th width="10%">Payment</th>
                                            <th>Aging</th>
                                            <th class="text-center" style="width: 11% !important;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($proformaInvoices) && count($proformaInvoices))
                                        @foreach($proformaInvoices as $i => $invoice)
                                        <tr <?php if($invoice->email_invoice != null){ ?>style="background-color:#a0c8ce;"<?php } ?>>
                                            <td class="text-center">{{ $i+1 }}</td>
                                            <td>{{ $invoice->patient->patient_id }}</td>
                                            <td>
                                                {{ $invoice->patient->full_name }}<br>
                                                <small>Enquirer: {{ $invoice->patient->enquirer_name }}</small>
                                            </td>
                                            <td>{{ Helper::getBranchSettings('proforma_invoice_inits',$invoice->patient_id).''.$invoice->invoice_no }}</td>
                                            <td>{{ $invoice->invoice_date->format('d-m-Y') }}</td>
                                            <td>Rs. {{ number_format($invoice->total_amount,2) }}</td>
                                            <td>{{ $invoice->invoice_due_date->format('d-m-Y') }}</td>
                                            <td>{{ $invoice->status }}</td>
                                            <td>
                                                @ability('admin','billing-payments-update')
                                                <a class="btn btn-info btnUpdatePayment" style="margin-left: 2%" data-invoice-id="{{ Helper::encryptor('encrypt',$invoice->id) }}" data-invoice-no="{{ Helper::getBranchSettings('proforma_invoice_inits',$invoice->patient_id).''.$invoice->invoice_no }}" data-invoice-date="{{ $invoice->invoice_date->format('d-m-Y') }}" data-invoice-amount="{{ floatVal($invoice->total_amount) }}" data-amount-received="{{ floatVal($invoice->amount_paid) }}" data-patient-id="{{ isset($invoice->patient)?Helper::encryptor('encrypt',$invoice->patient->id):'' }}" data-pat-id="{{ isset($invoice->patient)?$invoice->patient->patient_id:'' }}" data-status="{{ $invoice->status }}" data-patient="{{ isset($invoice->patient)?$invoice->patient->full_name:'' }}" data-customer="{{ isset($invoice->patient)?$invoice->patient->enquirer_name:'' }}">View / Update</a>
                                                @endability
                                            </td>
                                            <td>
                                                <small>{{ date_diff(new DateTime(), $invoice->created_at)->format("%a day(s) %h hr(s) %i min(s)") }}</small>
                                            </td>
                                            <td class="text-left">
                                                @ability('admin','billing-invoice-view')
                                                @if($invoice->status != 'Cancelled')
                                                    <a href="{{ route('billing.view-invoice',[Helper::encryptor('encrypt',$invoice->id),'Proforma']) }}" class="btn btn-md btn-primary" title="View Invoice"><i class="material-icons">remove_red_eye</i></a>

                                                    <a href="{{ route('billing.invoicepdf',['invoice_id' => $invoice->id,'email'=>true,'type'=>'Proforma']) }}" class="btn btn-md btn-info hide" title="Email Invoice"><i class="material-icons">email</i></a>
                                                    @if(!($invoice->amount_paid > 0))
                                                        @if($invoice->status != 'Adjusted' && $invoice->status != 'Paid')
                                                        <a href="{{ route('billing.delete-invoice',['invoice_id' => Helper::encryptor('encrypt',$invoice->id),'type'=>'Proforma']) }}" class="btn btn-md btn-danger" title="Cancel Invoice" onclick="return confirm_click();"><i class="material-icons">cancel</i></a>
                                                        @endif
                                                    @endif
                                                @else
                                                    Cancelled
                                                @endif
                                                @endability
                                            </td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="12" class="text-center">No invoice(s) found</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                                {{ $proformaInvoices->links() }}
                                <p class="text-right" style="margin-bottom:0;">
                                    {{ 'Showing '.$proformaInvoices->firstItem().' to '.$proformaInvoices->lastItem().' out of Total '.$proformaInvoices->total() }}
                                </p>
                            </div>
                        </div>
                        @else
                        <div class="text-center restricted-access">Access to this information is restricted</div>
                        @endability
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="service">
                        <div class="col-lg-12" style="margin-bottom: 0">
                            <div class="col-md-1" style="margin-bottom: 0"><label style="padding-top:5px">Filter by</label></div>
                            <div class="col-md-3" style="margin-bottom: 0">
                                <select class="form-control show-tick" id="filter_patient_service" data-live-search="true">
                                    <option value="">All Patient</option>
                                    @if(isset($patients) && count($patients))
                                    @foreach ($patients as $p)
                                    <option value="{{ $p->id }}">{{ $p->full_name.' - '.$p->patient_id }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="col-md-3" style="margin-bottom:0">
                                <div class="input-group">
                                    <span class="input-group-addon">{{ Helper::getSetting('invoice_inits') }}</span>
                                    <div class="form-line">
                                        <input type="number" class="form-control" id="filter_invoice_no_service" placeholder="Invoice No.">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1" style="margin-bottom: 0">
                                <a class="btn btn-block bg-teal btnFilterService">Filter</a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        @ability('admin','billing-invoice-list')
                        <div id="tableService">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered serviceInvoiceTable" style="zoom:85%;">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Patient ID</th>
                                            <th>Patient</th>
                                            <th>Invoice No.</th>
                                            <th>Invoice Date</th>
                                            <th>Amount</th>
                                            <th>Due Date</th>
                                            <th>Aging</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($serviceInvoices) && count($serviceInvoices))
                                        @foreach($serviceInvoices as $i => $invoice)
                                        <tr>
                                            <td class="text-center">{{ $i+1 }}</td>
                                            <td>{{ $invoice->patient->patient_id }}</td>
                                            <td>
                                                {{ $invoice->patient->full_name }}<br>
                                                <small>Enquirer: {{ $invoice->patient->enquirer_name }}</small>
                                            </td>
                                            <td>{{ Helper::getBranchSettings('invoice_inits',$invoice->patient_id).''.$invoice->invoice_no }}</td>
                                            <td>{{ $invoice->invoice_date->format('d-m-Y') }}</td>
                                            <td>Rs. {{ number_format($invoice->total_amount,2) }}</td>
                                            <td>{{ $invoice->invoice_due_date->format('d-m-Y') }}</td>
                                            <td>
                                                <small>{{ date_diff(new DateTime(), $invoice->created_at)->format("%a day(s) %h hr(s) %i min(s)") }}</small>
                                            </td>
                                            <td class="text-left">
                                                @ability('admin','billing-invoice-view')
                                                @if($invoice->status != 'Cancelled')
                                                    <a href="{{ route('billing.view-invoice',[Helper::encryptor('encrypt',$invoice->id),'Service']) }}" class="btn btn-md btn-primary" title="View Invoice"><i class="material-icons">remove_red_eye</i></a>

                                                    <a href="{{ route('billing.invoicepdf',['invoice_id' => $invoice->id,'email'=>true,'type'=>'Service']) }}" class="btn btn-md btn-info hide" title="Email Invoice"><i class="material-icons">email</i></a>

                                                    <a href="{{ route('billing.delete-invoice',['invoice_id' => Helper::encryptor('encrypt',$invoice->id),'type'=>'Service']) }}" class="btn btn-md btn-danger" title="Cancel Invoice" onclick="return confirm_click();"><i class="material-icons">cancel</i></a>
                                                @else
                                                    Cancelled
                                                @endif
                                                @endability
                                            </td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="10" class="text-center">No invoice(s) found</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                                {{ $serviceInvoices->links() }}
                                <p class="text-right" style="margin-bottom:0;">
                                    {{ 'Showing '.$serviceInvoices->firstItem().' to '.$serviceInvoices->lastItem().' out of Total '.$serviceInvoices->total() }}
                                </p>
                            </div>
                        </div>
                        @else
                        <div class="text-center restricted-access">Access to this information is restricted</div>
                        @endability
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="receipt">
                        <div class="col-lg-12" style="margin-bottom: 0">
                            <div class="col-md-1" style="margin-bottom: 0"><label style="padding-top:5px">Filter by</label></div>
                            <div class="col-md-3" style="margin-bottom: 0">
                                <select class="form-control show-tick" id="filter_patient_receipt" data-live-search="true">
                                    <option value="">All Patient</option>
                                    @if(isset($patients) && count($patients))
                                    @foreach ($patients as $p)
                                    <option value="{{ $p->id }}">{{ $p->full_name.' - '.$p->patient_id }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>

                            <div class="col-md-3" style="margin-bottom:0">
                                <div class="input-group">
                                    <span class="input-group-addon">{{ Helper::getSetting('receipt_inits') }}</span>
                                    <div class="form-line">
                                        <input type="number" class="form-control" id="filter_invoice_no_receipt" placeholder="Receipt No.">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2" style="margin-bottom: 0">
                                <select class="form-control show-tick" id="filter_status_receipt" data-live-search="true">
                                    <option value="">All Status</option>
                                    <option value="Payment">PAYMENT</option>
                                    <option value="Memo">MEMO</option>
                                </select>
                            </div>
                            <div class="col-md-1" style="margin-bottom: 0">
                                <a class="btn btn-block bg-teal btnFilterReceipt">Filter</a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        @ability('admin','billing-invoice-list')
                        <div id="tableReceipt">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered receiptTable" style="zoom:85%;">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Patient ID</th>
                                            <th>Patient</th>
                                            <th>Receipt Type</th>
                                            <th>Receipt No.</th>
                                            <th>Receipt Date</th>
                                            <th>Amount</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($receipts) && count($receipts))
                                        @foreach($receipts as $i => $receipt)
                                        <tr>
                                            <td class="text-center">{{ $i+1 }}</td>
                                            <td>{{ $receipt->patient->patient_id }}</td>
                                            <td>
                                                {{ $receipt->patient->full_name }}<br>
                                                <small>Enquirer: {{ $receipt->patient->enquirer_name }}</small>
                                            </td>
                                            <td>{{ $receipt->receipt_type }}</td>
                                            <td>{{ $receipt->receipt_no }}</td>
                                            <td>{{ $receipt->receipt_date->format('d-m-Y') }}</td>
                                            <td>Rs. {{ number_format($receipt->receipt_amount,2) }}</td>
                                            <td class="text-left">
                                                @ability('admin','billing-invoice-view')
                                                    <a href="{{ route('billing.view-receipt',[Helper::encryptor('encrypt',$receipt->id)]) }}" class="btn btn-md btn-primary" title="View Receipt"><i class="material-icons">remove_red_eye</i></a>
                                                @endability
                                            </td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="10" class="text-center">No receipt(s) found</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                                {{ $receipts->links() }}
                                <p class="text-right" style="margin-bottom:0;">
                                    {{ 'Showing '.$receipts->firstItem().' to '.$receipts->lastItem().' out of Total '.$receipts->total() }}
                                </p>
                            </div>
                        </div>
                        @else
                        <div class="text-center restricted-access">Access to this information is restricted</div>
                        @endability
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="selectPatientModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content" >
            <form id="patientForm">
                <div class="modal-header bg-blue hidden-print">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="smallModalLabel">Select Patient</h4>
                </div>
                <div class="modal-body text-center">
                    <div class="form-horizontal">
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
                                <label for="patient">Select Patient</label>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-left">
                                <div class="form-group" style="margin-top: 1%">
                                    <div class="">
                                        <select class="form-control show-tick" required="" data-live-search="true" name="patient_id">
                                            <option value="">-- Select Patient --</option>
                                            @if(isset($patients) && count($patients))
                                            @foreach ($patients as $patient)
                                            <option value="{{ $patient->id }}" data-id="{{ Helper::encryptor('encrypt',$patient->id) }}">{{ $patient->full_name.' - '.$patient->patient_id }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div><br>
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
                                <label for="invoice_type">Select Invoice Type</label>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-left">
                                <div class="form-group" style="margin-top: 1%">
                                    <div class="">
                                        <select class="form-control show-tick" required="" data-live-search="true" name="invoice_type">
                                            <option value="">-- Select Type --</option>
                                            <option value="proforma">Proforma</option>
                                            <option value="service">Service</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    {{ csrf_field() }}
                    <button type="button" class="btn btn-success waves-effect btnContinue">Continue</button>
                    <button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="creditMemoModal" data-keyboard="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content" >
            <form id="creditMemoForm" action="{{ route('billing.add-credit-memo') }}" method="POST">
                <div class="modal-header bg-blue hidden-print">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="smallModalLabel">Credit Memo - New</h4>
                </div>
                <div class="modal-body text-center">
                    <div class="form-horizontal">
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label required">
                                <label for="patient">Select Patient</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 text-left">
                                <div class="form-group" style="margin-top: 1%">
                                    <div class="form-line">
                                        <select class="form-control show-tick" data-live-search="true" name="credit_memo_patient_id" required>
                                            <option value="0">-- Select Patient --</option>
                                            @if(isset($patients) && count($patients))
                                            @foreach ($patients as $l)
                                            <option value="{{ $l->id }}">{{ $l->full_name.' - '.$l->patient_id }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div><br>
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label required">
                                <label for="patient">Credit Type</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 text-left">
                                <div class="form-group" style="margin-top: 1%">
                                    <div class="form-line">
                                        <select class="form-control show-tick" data-live-search="true" name="credit_memo_type" required>
                                            <option value="">-- Select Credit Type --</option>
                                            <option value="Others">Others</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div><br>
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label required">
                                <label for="date">Date</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="date" name="date" class="form-control datepicker" placeholder="Eg. {{ date('d-m-Y') }}">
                                    </div>
                                </div>
                            </div>
                        </div><br>
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label required">
                                <label for="amount">Amount</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="number" id="amount" name="amount" class="form-control" placeholder="Eg. 15000" required="">
                                    </div>
                                </div>
                            </div>
                        </div><br>
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label required">
                                <label for="comment">Comment</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <textarea id="comment" name="comment" class="form-control" placeholder="Comment / Remarks / Notes" required=""></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="border-top: 1px solid #eee">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-success waves-effect" onclick="javascript: return confirm('Are you sure?')">Submit</button>
                    <button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="creditMemoLogModal" data-keyboard="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
                <div class="modal-header bg-blue hidden-print">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="smallModalLabel">Credit Memo - Logs</h4>
                </div>
                <div class="modal-body text-center">
                    <div class="form-horizontal">
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-4 form-control-label required">
                                <label for="patient">Select Patient</label>
                            </div>
                            <div class="col-lg-4 col-md-8 col-sm-8 col-xs-8 text-left">
                                <div class="form-group" style="margin-top: 1%">
                                    <div class="form-line">
                                        <select class="form-control show-tick" data-live-search="true" name="credit_memo_patient_id_log" required="">
                                            <option value="">-- Select Patient --</option>
                                            @if(isset($patients) && count($patients))
                                            @foreach ($patients as $l)
                                            <option value="{{ $l->id }}">{{ $l->full_name.' - '.$l->patient_id }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 text-left">
                                <button type="submit" class="btn btn-success waves-effect checkcreditlog">Check</button>
                            </div>
                        </div><br>
                        <table class="table table-striped table-bordered creditMemoLogList">
                            <thead>
                                <th style="text-align:center;">#</th>
                                <th style="text-align:center;">Credit Memo No.</th>
                                <th style="text-align:center;">Credit Memo Date</th>
                                <th style="text-align:center;">Credit Memo Type</th>
                                <th style="text-align:center;">Amount</th>
                                <th style="text-align:center;">Comment</th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
                </div>
        </div>
    </div>
</div>

<div class="modal fade" id="creditMemoRefundModal" data-keyboard="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
                <div class="modal-header bg-blue hidden-print">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="smallModalLabel">Credit Memo - Refunds</h4>
                </div>
                <div class="modal-body text-center">
                    <div class="form-horizontal">
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-4 form-control-label required">
                                <label for="patient">Select Patient</label>
                            </div>
                            <div class="col-lg-4 col-md-8 col-sm-8 col-xs-8 text-left">
                                <div class="form-group" style="margin-top: 1%">
                                    <div class="form-line">
                                        <select class="form-control show-tick" data-live-search="true" name="credit_memo_patient_id_refund_log" required="">
                                            <option value="">-- Select Patient --</option>
                                            @if(isset($patients) && count($patients))
                                            @foreach ($patients as $l)
                                            <option value="{{ $l->id }}">{{ $l->full_name.' - '.$l->patient_id }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 text-left">
                                <button type="button" class="btn btn-success waves-effect checkCreditRefundLog">Check</button>
                            </div>
                        </div><br>
                        <table class="table table-striped table-bordered creditMemoRefundLogList">
                            <thead>
                                <th width="5%" class="creditmemorefund-sel"></th>
                                <th style="text-align:center;">#</th>
                                <th style="text-align:center;">Credit Memo No.</th>
                                <th style="text-align:center;">Credit Memo Date</th>
                                <th style="text-align:center;">Credit Memo Type</th>
                                <th style="text-align:center;">Amount</th>
                                <th style="text-align:center;">Comment</th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success waves-effect creditRefundRaiseReceipt">Raise Receipt</button>
                    <button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
                </div>
        </div>
    </div>
</div>

<div class="modal fade" id="updatePaymentModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" >
            <form id="updatePaymentForm" action="{{ route('billing.update-invoice-payment') }}" method="POST">
                <div class="modal-header bg-blue hidden-print">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Update Payment</h4>
                </div>
                <div class="modal-body text-center">
                    <ul class="nav nav-tabs" role="tablist" style="border: 2px dotted #eee">
                        <li role="presentation" class="active" style="min-width: 30%;text-align:center">
                            <a href="#update_payment" data-toggle="tab">
                                <i class="material-icons">payment</i> Update Payment
                            </a>
                        </li>
                        <li role="presentation" style="min-width: 30%;text-align:center">
                            <a href="#payment_history" data-toggle="tab">
                                <i class="material-icons">assignment</i> Payment History
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content" style="border: 2px dotted #eee;padding: 10px;margin-bottom: 20px">
                        <div role="tabpanel" class="tab-pane fade in active" id="update_payment">
                            <div class="form-horizontal">
                                <div class="col-sm-5 invoiceDetailsTable">
                                    <table class="table table-striped table-bordered">
                                        <caption class="text-center"><b>Invoice Details</b></caption>
                                        <tr>
                                            <th width="38%">Invoice No.</th>
                                            <td class="invoice-no text-left"></td>
                                        </tr>
                                        <tr>
                                            <th>Invoice Date</th>
                                            <td class="invoice-date text-left"></td>
                                        </tr>
                                        <tr>
                                            <th>Patient ID</th>
                                            <td class="patient-id text-left"></td>
                                        </tr>
                                        <tr>
                                            <th>Patient</th>
                                            <td class="patient text-left"></td>
                                        </tr>
                                        <tr>
                                            <th>Enquirer</th>
                                            <td class="customer text-left"></td>
                                        </tr>
                                        <tr>
                                            <th>Invoice Amount</th>
                                            <td class="invoice-amount text-left"></td>
                                        </tr>
                                        <tr>
                                            <th>Amount Received</th>
                                            <td class="amount-received text-left"></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-sm-6 paymentDetailsTable">
                                    <table class="table table-striped table-bordered">
                                        <caption class="text-center"><b>Payment Details</b></caption>
                                        <tr>
                                            <th width="30%" class="required">Amount</th>
                                            <td class="text-left">
                                                <input type="number" id="amount_paid" name="amount_paid" min="0" step="0.001" class="form-control input-sm" value="" placeholder="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <th width="30%" class="required">Payment Mode</th>
                                            <td class="text-left">
                                                <select id="payment_mode" name="payment_mode" class="form-control" required="">
                                                    <option value="">-</option>
                                                    <option value="Cash">Cash</option>
                                                    <option value="Credit/Debit Card">Credit / Debit Card</option>
                                                    <option value="Cheque/DD">Cheque / DD</option>
                                                    <option value="Bank Transfer">Bank Transfer</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Reference No.</th>
                                            <td class="text-left">
                                                <input type="text" id="reference_no" name="reference_no" class="form-control input-sm" placeholder="Reference No.">
                                            </td>
                                        </tr>
                                        <tr class="chequeDDBlock hide">
                                            <th>Cheque/DD No.</th>
                                            <td class="text-left">
                                                <input type="text" id="cheque_dd_no" name="cheque_dd_no" class="form-control input-sm" placeholder="Cheque/DD No.">
                                            </td>
                                        </tr>
                                        <tr class="chequeDDBlock hide">
                                            <th>Cheque/DD Date</th>
                                            <td class="text-left">
                                                <input type="text" id="cheque_dd_date" name="cheque_dd_date" class="form-control input-sm datepicker" placeholder="Cheque/DD Date">
                                            </td>
                                        </tr>
                                        <tr class="bankBlock hide">
                                            <th>Bank Name</th>
                                            <td class="text-left">
                                                <input type="text" id="bank_name" name="bank_name" class="form-control input-sm" placeholder="Bank Name">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="payment_history">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-condensed table-bordered invoicePaymentHistoryTable">
                                        <thead>
                                            <tr>
                                                <th class="text-center">#</th>
                                                <th class="text-center">Date</th>
                                                <th class="text-center">Amount</th>
                                                <th class="text-center">Payment Mode</th>
                                                <th class="text-center">Reference No.</th>
                                                <th class="text-center">Other details</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="6" class="text-center">No record(s)</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer" style="border-top: 1px solid #ccc">
                    {{ csrf_field() }}
                    <input type="hidden" name="invoice_id" value="">
                    <input type="hidden" name="patient_id" value="">
                    <input type="hidden" name="invoice_amount" value="">
                    <button type="button" class="btn btn-danger waves-effect btnRaiseCreditMemo hide" data-patient data-amount data-invoice title="Raises a credit memo and settles the invoice.">Raise Credit Memo And Settle Invoice</button>
                    <button type="button" class="btn btn-success waves-effect btnSavePayment">Update</button>
                    <button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="serviceExpiryModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document" style="width: 80%;margin-top: 1%;">
        <div class="modal-content modal-col-blue">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" id="largeModalLabel">Upcoming Service Expiry List</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive" style="margin-bottom: 0;max-height:480px;overflow: none;">
                    <table class="table table-bordered table-condensed table-hover serviceExpiryTable" style="width:99% !important">
                        <thead>
                            <tr>
                                <th width="3%">#</th>
                                <th>Episode ID</th>
                                <th>Service Name</th>
                                <th class="hide">Patient</th>
                                <th>Enquirer</th>
                                <th>Phone</th>
                                <th>Expiry Date</th>
                                <th>Link</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div><br>
            </div>
            <div class="modal-footer" style="border-top: 1px solid #ccc">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="unbilledServiceOrdersModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document" style="width: 80%;margin-top: 1%;">
        <div class="modal-content modal-col-blue" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" id="largeModalLabel">Unbilled Service Orders List</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive" style="margin-bottom: 0;max-height:480px;overflow: none;">
                    <table class="table table-bordered table-condensed table-hover unbilledServiceOrdersTable" style="width: 99% !important">
                        <thead>
                            <tr>
                                <th width="3%">#</th>
                                <th>Episode ID</th>
                                <th>Service Name</th>
                                <th>Period</th>
                                <th class="hide">Patient</th>
                                <th>Enquirer</th>
                                <th>Phone</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div><br>
            </div>
            <div class="modal-footer" style="border-top: 1px solid #ccc">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="statementOfAccountModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" >
            <form action="{{ route('billing.statement-of-account') }}" method="POST" target="_blank">
                <div class="modal-header bg-blue hidden-print">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="smallModalLabel">Statement of Account</h4>
                </div>
                <div class="modal-body text-center">
                    <div class="form-horizontal">
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
                                <label for="patient">Select Patient</label>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-left">
                                <div class="form-group" style="margin-top: 1%">
                                    <div class="">
                                        <select class="form-control show-tick" required="" data-live-search="true" name="soa_patient_id">
                                            <option value="">-- Select Patient --</option>
                                            @if(isset($patients) && count($patients))
                                            @foreach ($patients as $l)
                                            <option value="{{ $l->id }}">{{ $l->full_name.' - '.$l->patient_id }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div><br>
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
                                <label for="soa_period">Period Till</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-5 text-left">
                                <div class="form-group" style="margin-top: 1%">
                                    <input name="soa_period" type="radio" id="soa_period_till_date" class="with-gap radio-col-deep-purple" value="Till Date" checked=""/>
                                    <label class="col-sm-3" for="soa_period_till_date">Current Date</label>
                                    <input name="soa_period" type="radio" id="soa_period_custom" class="with-gap radio-col-deep-purple" value="Custom"/>
                                    <label class="col-sm-6" for="soa_period_custom">
                                        <div class="input-group" style="margin-top: -3%">
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" class="form-control datepickersoa" data-id="soa" disabled="" style="background-color: #eee" placeholder="Ex: 30/07/2016">
                                                <input type="hidden" name="soa_period_to" value=""/>
                                            </div>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div><br>
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
                                <label for="options">Show</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 text-left">
                                <div class="form-group" style="margin-top: 1%">
                                    <input name="soa_option_annexure" type="checkbox" id="soa_option_annexure" class="filled-in"/>
                                    <label class="col-sm-3" for="soa_option_annexure">Annexure</label>
                                    <input name="soa_option_payments" type="checkbox" id="soa_option_payments" class="filled-in"/>
                                    <label class="col-sm-4" for="soa_option_payments">Payments</label>
                                </div>
                            </div>
                        </div><br>

                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-success waves-effect">Continue</button>
                    <button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('page.scripts')
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.show-tick').select2();
        $.fn.modal.Constructor.prototype.enforceFocus = function() {};
    });
    let fpDate = null;
    let rTable;
    let paymentHistory = [];
    $(function(){
        $('.datepickersoa').flatpickr({
            altInput: true,
            altFormat: "d-m-Y",
            dateFormat: "Y-m-d",
            maxDate: new Date(),
            allowInput: true,
            onChange: function(selectedDates, dateStr, instance) {
                console.log(moment(dateStr).format("YYYY-MM-DD"));
                $('input[name=soa_period_to]').val(moment(dateStr).format("YYYY-MM-DD"));
            }
        });

        fpDate = $('.datepicker').flatpickr({
            altInput: true,
            altFormat: "d-m-Y",
            dateFormat: "Y-m-d",
            maxDate: new Date(),
            allowInput: false,
        });

        $('.content').on('click','.btnContinue', function(){
            invoiceType = $('#selectPatientModal #patientForm select[name=invoice_type] option:selected').val();

            if(invoiceType == 'proforma')
                var route = '{{ route('billing.create-proforma-invoice') }}';
            else
                var route = '{{ route('billing.create-service-invoice') }}';

            patientID = $('#selectPatientModal #patientForm select[name=patient_id] option:selected').val();
            if(patientID > 0){
                route += '/'+$('#selectPatientModal #patientForm select[name=patient_id] option:selected').data('id');
                window.location.href = route;
            }else{
                alert('Please Select a Patient.');
            }
        });

        $('body').on('click','.btnUpdatePayment', function(){
            var e = $(this);
            var invoiceId = e.data('invoice-id');
            var invoiceNo = e.data('invoice-no');
            var invoiceDate = e.data('invoice-date');
            var invoiceAmount = e.data('invoice-amount');
            var amountReceived = e.data('amount-received');
            var patient = e.data('patient');
            var patID = e.data('pat-id');
            var patientID = e.data('patient-id');
            var customer = e.data('customer');
            var status = e.data('status');

            $('#updatePaymentModal .invoicePaymentHistoryTable tfoot').show();
            $('#updatePaymentModal .invoicePaymentHistoryTable tbody').html('');

            if(paymentHistory.hasOwnProperty(invoiceId)){
                console.log("Log already exists!");
                showInvoicePaymentHistory(paymentHistory[invoiceId]);
            }else{
                $.ajax({
                    url: '{{ route('billing.invoice-payment-history') }}',
                    type: 'POST',
                    data: {'_token': '{{ csrf_token() }}', invoice_id: invoiceId},
                    success: function (d){
                        if(typeof d.length == 'undefined' || d.length > 0){
                            if(typeof paymentHistory[invoiceId] == 'undefined'){
                                paymentHistory[invoiceId] = [];
                            }
                            $.each(d, function(i){
                                paymentHistory[invoiceId].push(d[i]);
                            });
                        }
                        console.log("Log fetched from server!");
                        showInvoicePaymentHistory(paymentHistory[invoiceId]);
                    },
                    error: function (err){
                        console.log(err);
                    }
                });
            }


            var modal = '#updatePaymentModal';
            $(modal+' .modal-title').html('Invoice Payment - '+invoiceNo);
            $(modal+' input[name=invoice_id]').val(invoiceId);
            $(modal+' input[name=patient_id]').val(patientID);
            $(modal+' input[name=invoice_amount]').val(invoiceAmount);
            $(modal+' .invoice-no').html(invoiceNo);
            $(modal+' .invoice-date').html(invoiceDate);
            $(modal+' .invoice-amount').html('&#8377; '+formatCurrency(invoiceAmount));
            $(modal+' .amount-received').html('&#8377; '+formatCurrency(amountReceived));
            $(modal+' .patient-id').html(patID);
            $(modal+' .patient').html(patient);
            $(modal+' .customer').html(customer);
            $(modal+' #amount_paid').attr('max',parseFloat(invoiceAmount - amountReceived).toFixed(2));
            $(modal+' #amount_paid').data('amount',parseFloat(invoiceAmount - amountReceived).toFixed(2));
            $(modal+' #amount_paid').val(parseFloat(invoiceAmount - amountReceived).toFixed(2));

            $(modal+' .btnRaiseCreditMemo').attr('data-amount',parseFloat(invoiceAmount - amountReceived).toFixed(2));
            $(modal+' .btnRaiseCreditMemo').attr('data-patient',patientID);
            $(modal+' .btnRaiseCreditMemo').attr('data-invoice',invoiceId);

            if(((invoiceAmount - amountReceived) <= 0) || status == 'Settled' || status == 'Cancelled' || status == 'Adjusted'){
                $(modal+' .invoiceDetailsTable').removeClass('col-sm-5');
                $(modal+' .invoiceDetailsTable').addClass('col-sm-12');
                $(modal+' .paymentDetailsTable').removeClass('show');
                $(modal+' .paymentDetailsTable').addClass('hide');
                $(modal+' .btnSavePayment').hide();
                $(modal+' .btnRaiseCreditMemo').hide();
            }else{
                $(modal+' .invoiceDetailsTable').removeClass('col-sm-12');
                $(modal+' .invoiceDetailsTable').addClass('col-sm-5');
                $(modal+' .paymentDetailsTable').removeClass('col-sm-12');
                $(modal+' .paymentDetailsTable').addClass('col-sm-6');
                $(modal+' .paymentDetailsTable').addClass('show');
                $(modal+' .btnSavePayment').show();
                $(modal+' .btnRaiseCreditMemo').show();
            }

            $(modal).modal();
        });

        $('body').on('change','#payment_mode', function(){
            if($(this).val() == 'Cheque/DD'){
                $('.chequeDDBlock').removeClass('hide');
            }else{
                $('.chequeDDBlock').addClass('hide');
            }

            if($(this).val() == 'Bank Transfer'){
                $('.bankBlock').removeClass('hide');
            }else{
                $('.bankBlock').addClass('hide');
            }
        });

        $('body').on('click','.btnSavePayment',function(e){
            e.preventDefault();
            let isValid = true;

            if(confirm('Are you sure to proceed?')){
                if(parseFloat($('#updatePaymentForm #amount_paid').val()) > parseFloat($('#updatePaymentForm #amount_paid').data('amount'))){
                    $('#updatePaymentForm #amount_paid').focus();
                    alert("Amount should be less than or equal to invoice amount!");
                    isValid =  false;
                    return;
                }

                if($('#updatePaymentForm #payment_mode option:selected').val() == ''){
                    $('#updatePaymentForm #payment_mode').focus();
                    alert("Please select payment mode!");
                    isValid =  false;
                    return;
                }

                if(isValid){
                    $('#updatePaymentForm').submit();
                }
            }
        });

        $('body').on('click','.btnRaiseCreditMemo',function(e){
            if (confirm('Are you sure to settle off the invoice by creating a credit memo of the Outstanding Amount ?')) {
                var patient = $(this).data('patient');
                var amount = $(this).data('amount');
                var invoice = $(this).data('invoice');

                if(patient && amount && invoice){
                    $(this).attr('disabled', true);
                    $.ajax({
                        url: '{{ route('billing.settle-off-invoice') }}',
                        type: 'POST',
                        data: {pid: patient, amount: amount, invoice: invoice, _token: '{{ csrf_token() }}' },
                        success: function (d){
                            if(d){
                                showNotification('bg-green', 'Credit Memo raised successfully and Invoice is Settled.', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                                setTimeout(function(){
                                    window.location.reload();
                                },1500);
                            }
                        },
                        error: function (err){
                            $(this).attr('disabled', false);
                        }
                    })
                }
            }
        })
    });

    function showInvoicePaymentHistory(data){
        html = ``;
        if(typeof data != 'undefined' && data.length > 0){
            $.each(data, function(i){
                otherDetails = ``;
                if(data[i].payment_mode == 'Cheque/DD'){
                    otherDetails = `<b>Cheque/DD No. </b>: `+data[i].cheque_dd_no+`<br><b>Cheque/DD Date </b>: `+data[i].cheque_dd_date;
                }
                if(data[i].payment_mode == 'Bank Transfer'){
                    otherDetails = `<b>Bank Name </b>: `+data[i].bank_name;
                }

                html += `<tr>
                    <td class="text-center">`+(i+1)+`</td>
                    <td>`+ data[i].date +`</td>
                    <td>`+ formatCurrency(data[i].amount) +`</td>
                    <td>`+ data[i].payment_mode +`</td>
                    <td class="text-center">`+ data[i].reference_no +`</td>
                    <td class="text-left">`+ otherDetails +`</td>
                </tr>`;
            });

            $('#updatePaymentModal .invoicePaymentHistoryTable tfoot').hide();
            $('#updatePaymentModal .invoicePaymentHistoryTable tbody').html(html);
        }else{
            $('#updatePaymentModal .invoicePaymentHistoryTable tfoot').show();
        }
    }

    function formatCurrency(n){
        return parseFloat(n).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    }

    function filterColumn ( i, searchString, block ) {
        if(typeof rTable != 'undefined'){
            if(searchString != '')
                $(block).DataTable().column( i ).search("^" + searchString + "$", true, false).draw();
            else
                $(block).DataTable().column( i ).search("").draw();
        }
    }

    var navigationFn = {
        goToSection: function(id) {
            $('html, body').animate({
                scrollTop: $(id).offset().top
            }, 1000);
        }
    }

    getServiceExpiry();

    getUnbilledServices();

    function getServiceExpiry(){
        $.ajax({
            url: '{{ route('billing.get-upcoming-service-expiry-ajax') }}',
            type: 'POST',
            data: {_token: '{{ csrf_token() }}'},
            success: function (d){
                if(d.length){
                    html = ``;
                    link = '{{ url('leads/view') }}';
                    $.each(d, function(i){
                        html += `<tr>
                            <td>`+ (i + 1) +`</td>
                            <td>`+ d[i]['episode_id'] +`</td>
                            <td>`+ d[i]['service_name'] +`</td>
                            <td class="hide">`+ d[i]['patient'] +`</td>
                            <td>`+ d[i]['enquirer'] +`</td>
                            <td>`+ d[i]['phone'] +`</td>
                            <td>`+ d[i]['date'] +`</td>
                            <td class="text-center"><a target="_blank" href="`+ link + `/` + d[i]['id'] +`" class="btn btn-xs btn-success">View</a></td>
                        </tr>`;
                    });
                    $('.serviceExpiry .count-to').html(d.length);
                    $('.serviceExpiry .count-to').removeClass('loader');
                }else{
                    html = `<tr>
                        <td colspan="8" class="text-center">
                            No record(s) found
                        </td>
                    </tr>`;
                    $('.serviceExpiry .count-to').html('0');
                    $('.serviceExpiry .count-to').removeClass('loader');
                }
                    $('.serviceExpiryTable tbody').html(html);
                    if(d.length){
                        $('.serviceExpiryTable').DataTable();
                    }
            },
            error: function (err){
                console.log(err);
            }
        });
    }

    function getUnbilledServices(){
        $.ajax({
            url: '{{ route('billing.get-unbilled-services-ajax') }}',
            type: 'POST',
            data: {_token: '{{ csrf_token() }}'},
            success: function (d){
                if(d.length){
                    html = ``;
                    link = '{{ url('billing/create-proforma-invoice') }}';
                    $.each(d, function(i){
                        html += `<tr>
                            <td>`+ (i + 1) +`</td>
                            <td>`+ d[i]['episode_id'] +`</a></td>
                            <td><b>`+ d[i]['service_name'] +`</b></td>
                            <td><b>`+ d[i]['period'] +`</b></td>
                            <td class="hide">`+ d[i]['patient'] +`</td>
                            <td>`+ d[i]['enquirer'] +`</td>
                            <td>`+ d[i]['phone'] +`</td>
                            <td>`+ d[i]['status'] +`</td>
                            <td class="text-center"><a target="_blank" href="`+ link + `/` + d[i]['patient_id'] +`" class="btn btn-xs btn-primary" style="text-transform: none !important">Generate Invoice</a></td>
                        </tr>`;
                    });
                    $('.unbilledServiceOrders .count-to').html(d.length);
                    $('.unbilledServiceOrders .count-to').removeClass('loader');
                }else{
                    html = `<tr>
                        <td colspan="8" class="text-center">
                            No record(s) found.
                        </td>
                    </tr>`;
                    $('.unbilledServiceOrders .count-to').html('0');
                    $('.unbilledServiceOrders .count-to').removeClass('loader');
                }
                $('.unbilledServiceOrdersTable tbody').html(html);
                if(d.length){
                    var table = $('.unbilledServiceOrdersTable').DataTable({
                        "order": [[ 0, 'asc' ]],
                        "displayLength": 25,
                        "drawCallback": function ( settings ) {
                            var api = this.api();
                            var rows = api.rows( {page:'current'} ).nodes();
                            var last=null;

                            api.column(4, {page:'current'} ).data().each( function ( group, i ) {
                                if ( last !== group ) {
                                    $(rows).eq( i ).before(
                                        '<tr class="group" style="text-align:left;font-size:15px;font-weight:bold;"><td colspan="9">'+group+'</td></tr>'
                                        );

                                    last = group;
                                }
                            } );
                        },
                        dom: 'Bflrpt',
                        buttons: [
                            {
                                extend: 'excelHtml5',
                                title: 'Unbilled-Service-Orders-till_{{ date("d-m-Y_H:i:s") }}',
                                className: 'btn btn-primary',
                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                        ],
                    });
                    // Order by the grouping
                    $('.unbilledServiceOrdersTable tbody').on( 'click', 'tr.group', function () {
                        var currentOrder = table.order()[0];
                        if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
                            table.order( [ 4, 'desc' ] ).draw();
                        }
                        else {
                            table.order( [ 4, 'asc' ] ).draw();
                        }
                    } );
                }
            },
            error: function (err){
                console.log(err);
            }
        });
    }

    $('.content').on('click', '.checkcreditlog', function(){
        var patient_id = $('select[name=credit_memo_patient_id_log] option:selected').val();
        if(patient_id > 0){
            $.ajax({
                url: '{{ route('billing.get-credit-memo-log-ajax') }}',
                type: 'POST',
                data: { patient_id: patient_id, _token: '{{ csrf_token() }}'},
                success: function (d){
                    if(d.length){
                        html = ``;
                        $.each(d, function(i){
                            html += `<tr>
                                <td style="text-align:center;">`+ (i + 1) +`</td>
                                <td style="text-align:center;">`+ d[i]['number'] +`</td>
                                <td style="text-align:center;">`+ d[i]['date'] +`</td>
                                <td style="text-align:center;">`+ d[i]['type'] +`</td>
                                <td style="text-align:center;">`+ d[i]['amount'] +`</td>
                                <td style="text-align:center;">`+ d[i]['comment'] +`</td>
                            </tr>`;
                        });
                    }else{
                        html = `<tr>
                            <td colspan="5" class="text-center">
                                No record(s) found.
                            </td>
                        </tr>`;
                    }
                    $('.creditMemoLogList tbody').html(html);
                    if(d.length){
                        $('.creditMemoLogList').DataTable();
                    }
                },
                error: function (err){
                    console.log(err);
                }
            });
        }else{
            alert('Select a Patient')
        }
    });

    $('.content').on('click', '.checkCreditRefundLog', function(){
        var patient_id = $('select[name=credit_memo_patient_id_refund_log] option:selected').val();
        if(patient_id > 0){
            $.ajax({
                url: '{{ route('billing.get-credit-memo-refund-log-ajax') }}',
                type: 'POST',
                data: { patient_id: patient_id, _token: '{{ csrf_token() }}'},
                success: function (d){
                    if(d.length){
                        html = ``;
                        $.each(d, function(i){
                            html += `<tr>
                                <td class="text-center creditmemorefund-sel">
                                    <input id="creditmemorefund_`+d[i]['id']+`" name="choosen_creditmemo" type="checkbox" class="with-gap chk-col-blue filled-in creditmemorefund-sel-chk" value="`+d[i]['id']+`"/><label for="creditmemorefund_`+d[i]['id']+`">&nbsp;</label>
                                </td>
                                <td style="text-align:center;">`+ (i + 1) +`</td>
                                <td style="text-align:center;">`+ d[i]['number'] +`</td>
                                <td style="text-align:center;">`+ d[i]['date'] +`</td>
                                <td style="text-align:center;">`+ d[i]['type'] +`</td>
                                <td style="text-align:center;">`+ d[i]['amount'] +`</td>
                                <td style="text-align:center;">`+ d[i]['comment'] +`</td>
                            </tr>`;
                        });
                    }else{
                        html = `<tr>
                            <td colspan="6" class="text-center">
                                No record(s) found.
                            </td>
                        </tr>`;
                    }
                    $('.creditMemoRefundLogList tbody').html(html);
                    if(d.length){
                        $('.creditMemoRefundLogList').DataTable();
                    }
                },
                error: function (err){
                    console.log(err);
                }
            });
        }else{
            alert('Select a Patient')
        }
    });

    $('.content').on('click','.creditRefundRaiseReceipt', function(){
        var items = [];
        var patientID = $('select[name=credit_memo_patient_id_refund_log] option:selected').val();

        $('.creditmemorefund-sel-chk').each(function(i){
            if($(this).is(':checked')){
                items.push($(this).val());
            }
        });

        if(items.length <= 0){
            alert("Please select items!");
            return false;
        }

        if(patientID > 0 && items.length > 0){
            $(this).attr('disabled', true);
            $.ajax({
                url: '{{ route('billing.raise-credit-memo-receipt') }}',
                type: 'POST',
                data: {patient_id: patientID, item: items, _token: '{{ csrf_token() }}' },
                success: function (d){
                    if(d){
                        showNotification('bg-green', 'Receipt raised successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                        setTimeout(function(){
                            window.location.reload();
                        },1000);
                    }
                },
                error: function (err){
                    console.log(err);
                    $(this).attr('disabled', false);
                }
            })
        }
    });

    $('.content').on('change','#statementOfAccountModal input[name=soa_period]', function(){
        if($(this).val() == 'Till Date'){
            $('#statementOfAccountModal .daterange[data-id=soa]').prop('disabled',true);
            $('#statementOfAccountModal .daterange[data-id=soa]').css('background-color','#eee');
            $('#statementOfAccountModal input[name=soa_period_to]').val('');
        }else{
            $('#statementOfAccountModal .datepickersoa[data-id=soa]').prop('disabled',false);
            $('.datepickersoa.flatpickr-input').removeAttr('disabled');
            $('#statementOfAccountModal .datepicker[data-id=soa]').css('background-color','transparent');
        }
    });
</script>
<script type="text/javascript">
    function confirm_click(){
        if (confirm('Are you sure to delete the Invoice?'))
            return true;
        else
            return false;
    }
</script>

<script type="text/javascript">
// proforma table
$('.content').on('click','.btnFilterProforma', function(){
    var patientId = $('#filter_patient_proforma').val();
    var invoiceNo = $('#filter_invoice_no_proforma').val();
    var statusProforma = $('#filter_status_proforma').val();
    $.ajax({
        url: '{{ route('billing.proforma-paginator') }}',
        type: 'POST',
        data: {patientId:patientId, invoiceNo:invoiceNo, status:statusProforma, _token:'{{ csrf_token() }}'},
    }).done(function(data){
        $('#tableProforma').html(data);
    });
});
$(document).on('click','#proforma .pagination a', function(e){
    e.preventDefault();
    var url = $(this).attr('href').split('page=')[1];
    var patientId = $('#filter_patient_proforma').val();
    var invoiceNo = $('#filter_invoice_no_proforma').val();
    var statusProforma = $('#filter_status_proforma').val();
    $.ajax({
        url: '{{ route('billing.proforma-paginator') }}?page='+url,
        type: 'POST',
        data: {patientId:patientId, invoiceNo:invoiceNo, status:statusProforma, _token:'{{ csrf_token() }}'},
    }).done(function(data){
        $('#tableProforma').html(data);
    });
});

// service table    
$('.content').on('click','.btnFilterService', function(){
    console.log('btnFilterService working');
    var patientId = $('#filter_patient_service').val();
    var invoiceNo = $('#filter_invoice_no_service').val();
    $.ajax({
        url: '{{ route('billing.service-paginator') }}',
        type: 'POST',
        data: {patientId:patientId, invoiceNo:invoiceNo, _token:'{{ csrf_token() }}'},
    }).done(function(data){
        $('#tableService').html(data);
    });
});
$(document).on('click','#service .pagination a', function(e){
    e.preventDefault();
    var url = $(this).attr('href').split('page=')[1];
    console.log(url);
    var patientId = $('#filter_patient_service').val();
    var invoiceNo = $('#filter_invoice_no_service').val();
    $.ajax({
        url:'{{ route('billing.service-paginator') }}?page='+url,
        type: 'POST',
        data: {patientId:patientId, invoiceNo:invoiceNo, _token:'{{ csrf_token() }}'},
    }).done(function(data){
        $('#tableService').html(data);
    });
});

// receipt table
$('.content').on('click','.btnFilterReceipt', function(){
    var patientId = $('#filter_patient_receipt').val();
    var receiptNo = $('#filter_invoice_no_receipt').val();
    var statusReceipt = $('#filter_status_receipt').val();
    $.ajax({
        url: '{{ route('billing.receipt-paginator') }}',
        type: 'POST',
        data: {patientId:patientId, receiptNo:receiptNo, status:statusReceipt, _token:'{{ csrf_token() }}'},
    }).done(function(data){
        $('#tableReceipt').html(data);
    });
});
$(document).on('click','#receipt .pagination a', function(e){
    var patientId = $('#filter_patient_receipt').val();
    var receiptNo = $('#filter_invoice_no_receipt').val();
    var statusReceipt = $('#filter_status_receipt').val();
    e.preventDefault();
    var url = $(this).attr('href').split('page=')[1];
    $.ajax({
        url:'{{ route('billing.receipt-paginator') }}?page='+url,
        type: 'POST',
        data: {patientId:patientId, receiptNo:receiptNo, status:statusReceipt, _token:'{{ csrf_token() }}'},
    }).done(function(data){
        $('#tableReceipt').html(data);
    });
});
</script>
@endsection