<html>
<head>
    <title>Print Invoice - Smart Health Connect</title>
    <!-- Bootstrap Core Css -->
    <link href="{{ ViewHelper::ThemePlugin('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Custom Css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/print.css') }}" media="print" />
    <style>
       img.img-topbar{ margin-top: -5px !important;}
      .username:after{
        content: ' \25BE';
        padding-left: 10px;
        font-size: 20px;
      }
      .v-middle{
          vertical-align: middle !important;
      }
      body{
        font-size: 12px !important;
        background: rgb(204,204,204);
      }
      @page
      {
        size: A4;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
      }
      div.A4 {
          background: white;
          @if(Request::segment(3) == 'print')
          width: 21cm;
          height: 29.7cm;
          box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
          @else
          width: 793.707px;
          height: 1122.52px;
          box-shadow: 0 0 5px rgba(0,0,0,0.5);
          @endif
          display: block;
          margin: 0 auto;
      }
      .border-top{
        border-top: 1px solid #333 !important;
      }
      .border-bottom{
        border-bottom: 1px solid #333 !important;
      }
      .border-right{
        border-right: 1px solid #333 !important;
      }
      .border-left{
        border-left: 1px solid #333 !important;
      }
      .no-padding, .no-padding tbody tr td, .no-padding tbody tr th{
        padding: 0 !important;
        border: 1px solid #333;
      }
      .no-border, .no-border tbody tr td, .no-border tbody tr th{
        border: 0;
      }
      .services-table, .services-table thead tr th, .services-table tfoot tr th, .services-table tfoot tr td, .services-table  tbody tr th,.services-table tbody tr td{
          padding: 1px !important;
          border: 0;
      }
      .tbl-text{
          margin-top:1px !important;
          padding:2px !important;
          margin-bottom:1px !important;
      }
      h6{
          font-weight: bold;
      }
      .user-info{ background: rgba(96, 125, 139, 0.10) !important; border-bottom: 0.1em solid #3F51B5 !important; padding: 10px !important;}
    </style>
</head>
<body onload="javascript: window.print();">
    <div class="A4">
        <div class="col-lg-12 invoicereport" style="border: 0px solid black;background: white;margin-bottom: 0">
            <br>
            <table class="table table-bordered no-padding" style="margin-bottom: 0 !important">
                <!-- Heading -->
                <tr>
                    <th colspan="3" class="text-center">
                        <h6 style="font-size:20px; margin-top:2px !important;margin-bottom:2px !important; padding:0px !important">
                            {{ $type == 'Proforma'?'PROFORMA INVOICE':'BILL OF SUPPLY' }}
                        </h6>
                    </th>
                </tr>
                <!-- Logo, address and payment mode information -->
                <tr>
                    <td width="44%">
                        <table class="no-border" style="margin-bottom:0 !important">
                            <tr>
                                <td style="vertical-align: top !important;border: 0 !important">
                                    <center><img src="{{ asset('uploads/provider/'.Helper::encryptor('encrypt',$profile->tenant_id).'/'.$profile->organization_logo) }}" class="org-img" style="width: 60px;margin-top: 15px;clear:both;border-right:0 !important"></center>
                                </td>
                                <td style="border: 0 !important">
                                    <div style="width:98%;display:inline-block;padding: 5px;margin-left:5px;font-size:12px;padding-top:10px;border-left:0 !important;vertical-align: top !important;font-size: 12px !important">
                                        <span style="font-size: 12px !important;font-weight: bold">{{ $profile->organization_name }}</span><br>
                                        {!! !empty($branch->branch_address)?$branch->branch_address.'<br>':'' !!}
                                        {!! !empty($branch->branch_area)?$branch->branch_area.'<br>':'' !!}
                                        {!! !empty($branch->branch_city)?$branch->branch_city:'' !!}
                                        {!! !empty($branch->branch_zipcode)?'-'.$branch->branch_zipcode.'<br>':'<br>' !!}
                                        Tel No.: {!! !empty($branch->branch_contact_number)?'+91-'.$branch->branch_contact_number:'' !!}  {!! !empty($branch->branch_contact_number)?'+91-'.$branch->branch_contact_number:'' !!}
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td width="21.5%" style="border-left: 0 !important">
                        <table class="table no-border no-padding no-bottom" style="margin-bottom: 5px !important">
                            <tr>
                                <td class="border-bottom no-bottom">
                                    <h6 style="display:inline-block;margin-top:0px !important;padding:5px !important;margin-bottom:0px !important">INVOICE NO</h6>
                                    <h6 style="display:block;margin-top:-1.5% !important;margin-right:5px;font-weight:normal;padding:5px !important;margin-bottom:0px !important">{{ isset($data)?$data['inv_prefix'].$invoice->invoice_no:$inv_prefix.$invoice->invoice_no }}</h6>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-bottom" style="border:0 !important;border-top: 1px solid #333 !important">
                                    <h6 style="margin-top:2px !important;padding:5px !important;margin-bottom: 2px !important">MODE OF PAYMENT</h6>
                                    <div class="col-md-12" style="margin-top:2px !important;margin-left:0px;padding-left:5px !important;border: 0 !important">
                                        <table class="no-border" border="0" style="border: 0 !important;font-size: 12px !important;">
                                            <tr>
                                                <td>Cheque</td>
                                                <td style="padding-left: 20px !important;">DD</td>
                                            </tr>
                                            <tr>
                                                <td>Bank Transfer</td>
                                                <td style="padding-left: 20px !important;">Cash</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">Other</td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border-left: 0 !important">
                        <table class="table no-border no-padding" style="margin-bottom: 5px !important">
                            <tr style="height: 44px !important;">
                                <td class="border-bottom border-right">
                                    <h6 style="margin-top:2px !important;padding:5px !important;">DATE</h6>
                                    <h6 style="margin-top:2px !important;padding:5px !important;">DUE BY</h6>
                                </td>
                                <td>
                                  <h6 style="margin-top:2px !important;padding:5px !important;">{{ $invoice->invoice_date->format('d-M-Y') }}</h6>
                                  <h6 style="margin-top:2px !important;padding:5px !important;">{{ $invoice->invoice_due_date->format('d-M-Y') }}</h6>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="padding: 5px !important;font-size: 12px !important;border-top: 1px solid #333 !important">
                                    Immediate by cheque / DD favouring<br>
                                    <h6 style="margin-top:2px !important;padding:5px !important">{{ $branch->invoice_payee }}</h6>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- Customer and bank information -->
                <tr>
                    <td width="40%" style="border: 0 !important">
                        <div style="padding: 1px 4px !important;margin-bottom: 0px !important;vertical-align: top !important">
                            <h6 style="font-size:13px;margin-top:2px !important;margin-bottom: 1px !important;">{{ $invoice->patient->full_name }}</h6>
                            <div style="font-weight: normal !important;font-size: 12px !important;margin-bottom: 2px !important;font-style: italic">Patient ID: <b>{{ $invoice->patient->patient_id }}</b></div>
                            <address style="font-size: 12px !important;font-weight:500;margin-bottom: 0px !important;vertical-align: top !important">
                                {!! !empty($invoice->patient->street_address)?$invoice->patient->street_address.'<br>':'' !!}
                                {!! !empty($invoice->patient->area)?$invoice->patient->area.'<br>':'' !!}
                                {!! $invoice->patient->city.' - '.$invoice->patient->zipcode.', '.$invoice->patient->state.'<br>' !!}
                                Phone: {{ $invoice->patient->contact_number.','.$invoice->patient->alternate_number }}<br>
                                Email: {{ strtolower($invoice->patient->email) }}
                                @if($invoice->patient->enquirer_name != '') <br><i>Enquirer</i>: <b>{{ $invoice->patient->enquirer_name }}</b> @endif
                            </address>
                        </div>
                    </td>

                    <td colspan="2" style="border: 0 !important;">
                        <table class="table no-padding " style="border: 0 !important;margin-bottom: 0px !important;font-size: 11px !important;min-height: 123px">
                            <tr>
                                <th width="38.4%" class="border-right border-bottom" style="border-top: 0 !important"><h6 class="tbl-text" style="font-size: 11px !important;">BANK NAME</h6></th>
                                <td class="border-bottom" style="padding-left: 5px !important;border-top: 0 !important;border-right: 0 !important">{{ $branch->bank_name }}</td>
                            </tr>
                            <tr>
                                <th class="border-right border-bottom"><h6 class="tbl-text" style="font-size: 11px !important;">ADDRESS</h6></th>
                                <td class="border-bottom" style="padding-left: 5px !important;padding-top:2px !important">{{ $branch->bank_address }}</td>
                            </tr>
                            <tr>
                                <th class="border-right border-bottom"><h6 class="tbl-text" style="font-size: 11px !important;">BANK A/C NO</h6></th>
                                <td class="border-bottom" style="padding-left: 5px !important;padding-top:2px !important"> @if(Helper::getTenantId() == 269) {{-- if aajicare --}} AAJI15{{ $invoice->patient->patient_id }} @else {{ $branch->acc_num }} @endif </td>
                            </tr>
                            <tr>
                                <th class="border-right" style="border-bottom: 0 !important"><h6 class="tbl-text" style="font-size: 11px !important;">IFSC CODE</h6></th>
                                <td style="padding-left: 5px !important;padding-top:2px !important;border-bottom: 0 !important">{{ $branch->ifsc_code }}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <table class="table services-table" style="font-size:12px !important;margin-top: -1px !important;margin-bottom: 0px !important">
                <thead>
                    <tr>
                        <th class="text-center border-left border-top border-right border-bottom v-middle" style="font-size:10px !important">DESCRIPTION</th>
                        <th class="text-center border-left border-top border-right border-bottom v-middle" style="font-size:10px !important">HSN</th>
                        <th width="8%" class="text-center border-top border-right border-bottom v-middle" style="font-size:10px !important">DURATION / QTY</th>
                        <th width="8%" class="text-center border-top border-right border-bottom v-middle" style="font-size:10px !important">RATE / ITEM</th>
                        <th width="9%" class="text-center border-top border-right border-bottom v-middle" style="font-size:10px !important">TOTAL</th>
                        <th width="8%" class="text-center border-top border-right border-bottom v-middle" style="font-size:10px !important">DISCOUNT</th>
                        <th width="9%" class="text-center border-top border-right border-bottom v-middle" style="font-size:10px !important">TAXABLE VALUE</th>
                        <th width="10%" colspan="2" class="text-center border-top border-right border-bottom v-middle" style="font-size:10px !important">CGST</th>
                        <th width="10%" colspan="2" class="text-center border-top border-right border-bottom v-middle" style="font-size:10px !important">SGST</th>
                        <th width="9%" class="text-center border-top border-right border-bottom v-middle" style="font-size:10px !important">AMOUNT</th>
                    </tr>
                </thead>
                <tbody>
                    {? $subTotal = $taxableAmount = $totalDiscount = $totalTax = $totalAmount = $less = $adjusted = 0; ?}
                    @if($invoice->getItemsArray())
                        @foreach ($invoice->getItemsArray() as $key => $item)
                            {? $taxableAmount += ($item->rate * $item->duration - $item->discount); ?}
                            {? $subTotal += ($item->rate * $item->duration); ?}
                            {? $totalDiscount += $item->discount; ?}
                            {? $totalTax += $item->tax_amount; ?}
                            {? $totalAmount += ($item->duration * $item->rate - $item->discount + $item->tax_amount); ?}
                            <tr class="item_row">
                                <td class="table_service_ID border-left border-right border-bottom" style="padding-left: 5px !important">
                                    {{ ucwords($item->description) }}
                                    {!! (!empty($item->period) && $item->period != '-')?'<br><small><i>'.$item->period.'</i></small>':'' !!}
                                </td>
                                <td class="text-center border-right border-bottom v-middle">{{ isset($item->hsn)?strtoupper($item->hsn):'NA' }}</td>
                                <td class="text-center border-right border-bottom v-middle">{{ $item->duration }}</td>
                                <td class="text-center border-right border-bottom v-middle">{{ number_format($item->rate,2) }}</td>
                                <td class="text-center border-right border-bottom v-middle">{{ number_format($item->duration * $item->rate,2) }}</td>
                                <td class="text-center border-right border-bottom v-middle">{{ number_format($item->discount,2) }}</td>
                                <td class="text-center border-right border-bottom v-middle">{{ number_format($item->duration * $item->rate - $item->discount,0) }}</td>
                                <td class="text-center border-right border-bottom v-middle">{{ number_format($item->tax_percentage/2,2).'%' }}</td>
                                <td class="text-center border-right border-bottom v-middle">{{ number_format($item->tax_amount/2,2) }}</td>
                                <td class="text-center border-right border-bottom v-middle">{{ number_format($item->tax_percentage/2,2).'%' }}</td>
                                <td class="text-center border-right border-bottom v-middle">{{ number_format($item->tax_amount/2,2) }}</td>
                                <td class="text-right border-right border-bottom v-middle" style="padding-right: 6px !important;">{{ number_format(($item->duration * $item->rate - $item->discount + $item->tax_amount),2) }}</td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
                <tfoot>
                    <tr class="{{ count($invoice->getItemsArray())?'':'display:none' }}" style="background: #ddd !important">
                        <th colspan="5" class="text-right border-left border-right" style="padding-right: 10px !important;">Total</th>
                        <th class="text-center border-right border-top">{{ number_format($totalDiscount,2) }}</th>
                        <th class="text-center border-right border-top">{{ number_format($taxableAmount,2) }}</th>
                        <th class="text-center border-right border-top"></th>
                        <th class="text-center border-right border-top">{{ number_format($totalTax/2,2) }}</th>
                        <th class="text-center border-right border-top"></th>
                        <th class="text-center border-right border-top">{{ number_format($totalTax/2,2) }}</th>
                        <th class="text-right border-right border-top" style="padding-right: 10px !important;">{{ number_format($totalAmount,2) }}</th>
                    </tr>
                    @if(isset($invoice->items) && count($invoice->items->where('category','Credit Memo')))
                        {? $creditMemoTxt = ""; ?}
                        @foreach ($invoice->items->where('category','Credit Memo') as $item)
                            {? $less += (float) $item->creditMemo->amount; ?}
                            {? $creditMemoTxt .= $item->creditMemo->date->format('d-m-Y').", "; ?}
                        @endforeach
                        <tr style="background: #ddd !important">
                            <td colspan="5" class="text-right border-left border-top border-right border-bottom" style="padding-right: 10px !important"><span style="color: #607d8b !important;font-weight:bold;font-size: 14px !important">Less </span>Credit Memo ({{ rtrim($creditMemoTxt,', ') }})</td>
                            <td colspan="6" class="text-left border-left border-top border-right border-bottom"></td>
                            <th class="text-right border-top border-left border-right border-bottom" style="padding-right: 10px !important;">- {{ number_format($less,2) }}</th>
                        </tr>
                    @endif
                    @if(isset($invoice->items) && count($invoice->items->where('category','Invoice')))
                        <tr>
                            <td colspan="10" style="color: #607d8b !important;font-weight:bold;font-size: 14px !important"><i>Invoice</i></td>
                        </tr>

                        @foreach ($invoice->items->where('category','Invoice') as $item)
                            {? $adjusted += $item->total_amount; ?}
                            <tr>
                                <td colspan="5" class="text-right border-left border-top border-right border-bottom" style="padding-right: 10px !important"><span style="color: #607d8b !important;font-weight:bold;font-size: 14px !important">Invoice ({{ \Helper::getSetting('proforma_invoice_inits').''.$item->itemInvoice->invoice_no.' - '.$item->itemInvoice->invoice_date->format('d-m-Y') }})</td>
                                <td colspan="6" class="text-left border-left border-top border-right border-bottom"></td>
                                <td class="text-right border-top border-left border-right border-bottom" style="padding-right: 10px !important;">{{ $item->total_amount }}</td>
                            </tr>
                        @endforeach
                    @endif
                    <tr class="{{ count($invoice->getItemsArray())?'':'display:none' }}" style="background: #ddd !important">
                        <th colspan="5" class="text-right border-left border-right" style="padding-right: 10px !important;">Total Invoice Value</th>
                        <td colspan="6" class="text-left border-left border-top border-right"></td>
                        <th class="text-right border-right border-top" style="padding-right: 10px !important;">{{ number_format(($totalAmount - $less + $adjusted),2) }}</th>
                    </tr>
                </tfoot>
            </table>

            <table class="table" style="font-size:13px !important;margin-bottom: 2px !important;position:relative;bottom:0">
                <tr>
                    <td colspan="2" class="border-left border-top border-right">
                        AMOUNT CHARGEABLE IN WORDS:
                        {? $f = new \NumberFormatter("en", \NumberFormatter::SPELLOUT); ?}
                        <b>{{ ucwords($f->format(($invoice->total_amount))).' Rupees Only' }}</b><br>
                          <div style="color: #ff0303 !important;font-size: 12px !important" class="col-md-9">
                           @if(Helper::getTenantId() == 269)
                              *Late payment fee of 100 Rs per day will be applicable if the payment is done after 10th of month.
                            @endif
                          </div>
                          <div class="col-md-3">
                            <h6 class="tbl-text text-right">E. &amp; O.E</h6><br>
                          </div>
                    </td>
                </tr>
                <tr>
                    <td class="border-left border-top border-bottom border-right" width="50%">
                        <div style="font-size: 11px !important;padding-left:0 !important">PAN : {{ $branch->pan_number }}</div>
                        <div style="font-size: 11px !important;padding-left:0 !important">GSTIN : {{ $branch->gstin }}</div>
                        <div style="font-size: 11px !important;padding-left:0 !important">CIN : {{ $branch->cin }}</div>
                        <div style="font-size: 11px !important;margin-top:5px">
                            <small style="font-weight:500">Note: Payment has to be made immediately.<br> Delayed payment shall attract interest at 18% p.a. <br><br> We declare that this invoice shows the actual price of the goods/ service described and that all particulars are true and correct</small>
                        </div>
                    </td>
                    <td class="no-padding border-left border-bottom border-right">
                        <h6 class="tbl-text text-right" style="font-size:13px;margin-top:5px !important; margin-right: 8% !important;">for {{ strtoupper($profile->organization_name) }}</h6>
                        <h6 class="sig" style="text-align: right;margin-top: 10%; margin-bottom: 0 !important; margin-right: 20%">AUTHORISED SIGNATORY</h6>
                    </td>
                </tr>
            </table>
            <center><b>SUBJECT TO {{ strtoupper($branch->branch_city) }} JURISDICTION</b></center>
            <span style="font-size:13px;margin-left:20px;">This is a computer generated invoice and does not require a signature.</span><br>
        </div>
    </div>
</body>
</html>
