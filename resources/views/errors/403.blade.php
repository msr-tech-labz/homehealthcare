@extends('layouts.main-layout')

@section('page_title','Permission Denied - ')

@section('content')
    <div class="row clearfix">
        <div class="col-sm-12">
            <div class="card">
                <div class="header">
                    <h2 class="col-red">
                        <i class="material-icons">warning</i> Permission Denied
                    </h2>
                </div>
                <div class="body text-center">
                    <div class="entry text-left">
                        <p>Sorry. You are trying to access a web page without proper authorization.</p>
                        <p>You are currently logged in as <b>{{ session('email') }}</b>, and this account doesn't have permission to access the information.</p>
                        <p>If you believe this is a mistake, try <a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">logging out</a> and then
                            <a href="{{ url('/login') }}">logging in</a> again.</p>
                        <p>After logging in with an authorized account, revisit the desired web page.</p>
                    </div>
                    <a href="javascript: window.history.back();" class="btn btn-primary waves-effect">Go Back</a>
                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
