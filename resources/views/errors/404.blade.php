<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Page not found - Smart Health Connect</title>
    <!-- Favicon-->
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <meta name="robots" content="noindex" />
    <meta name="robots" content="nofollow" />
    <meta name="robots" content="noarchive" />
    <meta name="googlebot" content="nosnippet" />
    <meta name="robots" content="noodp" />
    <meta name="slurp" content="noydir">
    <meta name="robots" content="noimageindex,nomediaindex" />

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{ ViewHelper::ThemePlugin('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom Css -->
    <link href="{{ ViewHelper::ThemeCss('style.css') }}" rel="stylesheet">
</head>
<body>
    <div class="content" style="padding: 40px">
        <div class="row clearfix">
            <div class="col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2 class="text-center" style="font-size: 25px !important;color: #1a68b7 !important">
                            Smart Health Connect
                        </h2>
                    </div>
                    <div class="body text-center">                        
                        <div class="col-md-8 col-md-offset-2 text-center">
                            <div class="text-center">
                                <h3 style="color:#ff0000; font-size: 62px !important;line-height: 40px !important">Oops!</h3>
                            </div><br>
                            <p style="font-size: 28px;line-height: 40px">We can't seem to find the <br>page you're looking for</p>
                        </div>                        
                        <div class="clearfix"></div>
                        <a href="{{ url('/') }}" class="btn btn-primary waves-effect">Home</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
