@extends('layouts.main-layout')

@section('page_title','Something went wrong - ')

@section('page.styles')
<style>
    .powered-by{
        display: none !important;
    }
</style>
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-sm-12">
            <div class="card">
                <div class="col-lg-12" style="background-color: #1dd6af;font-family: cursive;">
                    <p class="topmsg" style="text-align: center;font-size: 40px;color: white;">OOPS!!!</p>
                    <p class="topmsg" style="text-align: center;font-size: 20px;color: white;">Please reach our admins.</p>
                </div>
                <div class="body text-center" style="font-family: cursive;">
                    <div class="entry text-left">
                        <div class="content" style="margin: 0;padding: 30px;">
                            <div class="title" style="text-align: center;font-size: 20px;">Something went wrong.</div>

                            @if(app()->bound('sentry') && !empty(Sentry::getLastEventID()))
                                <div class="subtitle" style="text-align: center;font-size: 20px;">Error ID: {{ Sentry::getLastEventID() }}</div>

                                <!-- Sentry JS SDK 2.1.+ required -->
                                <script src="https://cdn.ravenjs.com/3.3.0/raven.min.js"></script>

                                <script>
                                    Raven.showReportDialog({
                                        eventId: '{{ Sentry::getLastEventID() }}',
                                        // use the public DSN (dont include your secret!)
                                        dsn: 'https://68c3fe39da7f4700902ba134e510472d@sentry.io/1411529',
                                        user: {
                                            'name': 'Jane Doe',
                                            'email': 'jane.doe@example.com',
                                        }
                                    });
                                </script>
                            @endif
                        </div>
                    </div>
                    <a href="javascript: window.history.back();" class="btn btn-primary waves-effect">Go Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
