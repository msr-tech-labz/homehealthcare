@extends('layouts.main-layout')

@section('page_title','Tele Consultation - ')


@section('plugin.styles')
    <link rel="stylesheet" href="https://cdn.plyr.io/2.0.11/plyr.css">
    <link rel="stylesheet" href="https://av.healthkon.com/public/video.css">
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header clearfix">
                    <a href="{{ url('/') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                        <i class="material-icons">arrow_back</i>
                    </a>
                    <h2 class="col-sm-6">
                        Tele Consultation
                        <small>&nbsp;</small>
                    </h2>
                    <div class="col-sm-5 text-right">
                        {{-- @ability('admin','employees-create')
                        <a href="{{ route('employee.create') }}" class="btn-group" role="group">
                        <button type="button" class="btn btn-success waves-effect"><i class="material-icons">add_circle</i></button>
                        <button type="button" class="btn btn-success waves-effect" style="line-height: 1.9;padding-left:0">New Employee</button>
                    </a>&nbsp;&nbsp;
                    <a href="{{ route('employee.import') }}" class="btn-group hide" role="group">
                    <button type="button" class="btn btn-primary waves-effect"><i class="material-icons">cloud_upload</i></button>
                    <button type="button" class="btn btn-primary waves-effect" style="line-height: 1.9;padding-left:0">Bulk Import</button>
                </a>
                @endability --}}
            </div>
        </div>
        <div class="container ">
            <div class="row" style="background-color:#f3f3f3;height:auto;padding:8px;margin-right:0px;margin-left:0px;" >
                <div class="col-md-7">
                    <div class="col-md-12" style="border:1px solid #e4e5e0;margin-top:8px;padding:10px;">
                        @if(Auth::user()->roleid ==1)
                        <style>
                        #preview {
                            max-width: 610px;
                            max-height: 650px;
                            border: none;
                        }
                        #remote-media {
                            min-height: 350px;
                            display: flex;
                            flex-wrap: wrap;
                            justify-content: center;
                        }
                        #remote-media > div {
                            width: 50%;
                        }
                        #remote-media > div:only-child {
                            width: 100%;
                        }
                        #remote-media .plyr__video-wrapper {
                            background-color: #f2f2f2;
                        }
                        video.remote-video {
                            max-height: 350px;
                        }
                        #local-media {
                            background-color: rgba(0, 0, 0, 0.18);
                            min-width: 313px;
                            text-align: center;
                        }
                        #local-media .plyr {
                            min-width: 150px;
                        }
                        #local-media > div {
                            display: inline-block;
                            margin: 0 auto;
                            max-width: 150px;
                        }
                        </style>
                        <div id="preview">
                            <div id="hk-video-poster" style="margin-bottom:-100px">
                                <img src="assets/assets/img/logo1-default.png" alt="Healthkon Video">
                            </div>
                            <div id="presenter-media" style="display:none;"></div>
                            <div id="remote-media">
                            </div>
                        </div>
                        <div id="local-media"></div>
                    @endif
                    @if(Auth::user()->roleid ==2)
                    <style>
                    #preview {
                        min-width: 100%;
                        min-height: 350px;
                        border: none;
                    }
                    #local-media {
                        background-color: rgba(0, 0, 0, 0.18);
                        min-width: 313px;
                    }
                    #local-media .plyr {
                        min-width: 150px;
                    }
                    #local-media > div {
                        display: inline-block;
                        margin: 0 auto;
                        max-width: 150px;
                    }
                    </style>
                    <div id="preview">
                        <div id="hk-video-poster" style="margin-bottom:-100px">
                            <img src="assets/assets/img/logo1-default.png" alt="Healthkon Video">
                        </div>
                        <div id="presenter-media">

                        </div>
                    </div>
                    <div id="local-media">

                    </div>
                @endif
            </div>
            <div class="col-md-12" style="background-color:black;margin-top:8px;padding:6px;">
                @if(Auth::user()->roleid ==1)
                    <div class="col-md-5" id="log">
                        <p><span id="log-content" class="color-green" style="font-weight:bold;">Welcome...</span></p>
                    </div>
                    <div class="col-md-3">
                        <span style="color:white;font-size:16px;" id="countdown">00:00</span>
                    </div>
                    <div class="col-md-4 pull-right">
                        <button type="button" class="btn btn-u" id='start' style="padding:4px;" onclick='startDrVideo();'>Start</button>
                        <button type="button" class="btn btn-u" id='stop' style="padding:4px;">Stop</button>
                    </div>
                @endif
                @if(Auth::user()->roleid ==2)
                    <div class="col-md-8" id="log">
                        <p><span id="log-content" class="color-green" style="font-weight:bold;">Welcome...</span></p>
                    </div>
                    <div class="col-md-4 pull-right">
                        <span style="color:white;font-size:16px;" id="countdown">00:00</span>
                    </div>
                @endif
            </div>
        </div>

        <div class="col-md-5">
            <div align="right" id="campaignParticipantsDiv">
                @if(Auth::user()->roleid ==1)
                    <button type="button" class="btn btn-u" onclick="showParticipants('loadParticipantsDetails');" data-toggle="modal" data-target="#campaignParticipantsModal">Participants</button>

                @endif
                <!-- Naveen Changes start -->
                <a href="#"><button type="button" class="btn btn-u" onclick="clearFields();" id="addParticipant" data-toggle="modal" data-target="#addParticipantSession" style="padding:5px 15px;">Add a Participant</button></a>
                <!-- Naveen Changes end -->
            </div>
            <div class="tab-v1">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#chatarea" data-toggle="tab"><img width="50px" height="50px" src="assets/assets/img/icons/flat/chat.png"></a></li>
                    <li><a href="#notes" data-toggle="tab"><img width="50px" height="50px" src="assets/assets/img/icons/flat/notes.png" onclick="return getParticipantData()"></a></li>
                    @if(Auth::user()->roleid ==1)
                        <li><a href="#prescription" data-toggle="tab"><img width="50px" height="50px" src="assets/assets/img/icons/flat/pres.png"></a></li>
                        <li><a href="#labtest" data-toggle="tab"><img width="50px" height="50px" src="assets/assets/img/icons/flat/lab.png"></a></li>
                    @endif
                    @if(Auth::user()->roleid === 2)
                        <li id="campaignPracLi"><a href="#campaignParticipantList" data-toggle="tab" onclick="showParticipants('campaignParticipantList')"><img width="50px" height="50px" src="assets/assets/img/icons/flat/profile.png"></a></li>
                    @endif
                </ul>
                <div class="tab-content" style="margin-top:10px;">
                    <div class="tab-pane fade in" id="notes">
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset>
                                    <section>
                                        <form id="uploadForm" action="" method="post" enctype="multipart/form-data">

                                            <label class="textarea textarea-expandable">
                                                <div id='apointmentDetails'>Loading Patient Details...</div>
                                                <div id='reportsDetails'></div>
                                                <input type='hidden' id='vitalsFetched' name='vitalsFetched' value='0' />
                                                <input type='hidden' id='bookingId' name='bookingId' value='' />
                                                <input type='hidden' id='doctorsId' name='doctors_id' value='<?php Auth::user()->id ?>' />
                                                <input type='hidden' id='details' name='details' value='hello' />
                                                <input type='hidden' id='patientsId' name='patientsId' value='1' />
                                                <input type='hidden' id='activeParticipantHealthkonId' name='activeParticipant' value='0' />
                                                <input type='hidden' id='activeParticipantName' name='activeParticipantName' value='0' />
                                                <input type='hidden' id='activeParticipantId' name='activeParticipantId' value='0' />

                                                @if(Auth::user()->roleid ==2)
                                                    <hr id="line">
                                                    <div id="selectImage">
                                                        <label>Upload Report</label><br/>

                                                        <input type="file" name="file_up" id="file_up" required />
                                                        <input type="submit" value="Upload" class="submit" />
                                                    </div>
                                                </br>
                                                <h4 id='loading' >Uploading..</h4>
                                                <h4 id='deleting' >Deleting..</h4>
                                            @endif
                                        </label>
                                    </form>
                                </section>
                            </div>
                            @if(Auth::user()->roleid ==1)
                                <div class="col-md-12">
                                    <section>
                                        <label class="textarea textarea-expandable">
                                            <textarea rows="5" cols="67" class="form-control" id='notesData' name='notesData'></textarea>
                                        </label>
                                    </section>
                                </div>
                            @endif
                        </fieldset>

                    </div>
                </div>
                @if(Auth::user()->roleid ==1)
                    <div class="tab-pane fade in" id="prescription">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="textarea textarea-expandable">
                                            <textarea rows="6" cols="67" class="form-control" id="prescriptionarea"></textarea>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-9">
                                        <input id="medicines" class="form-control" placeholder="Type Medicine" autofocus>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="button" onclick="addnewmedicine();" class="btn btn-u" style="width:100%;padding:5px;">ADD</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div id="container1" class="col-md-9">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <br/>
                                        <ul id="presitems" class="presitemslist">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="tab-pane fade in active" id="chatarea">
                    <div class="row">
                        <div class="col-md-12">
                            <section>
                                <div id="messages" style="background-color:white;width:100%;height:250px;overflow-y:scroll;"></div><br>
                                <input  id="chat-input" type="text" class="form-control" placeholder="Type Here and Press Enter" autofocus>
                            </section>
                        </div>
                    </div>
                </div>
                @if(Auth::user()->roleid ==1)
                    <div class="tab-pane fade in" id="labtest">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="textarea textarea-expandable">
                                            <textarea rows="6" cols="67" class="form-control" id="labtestarea"></textarea>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-9">
                                        <input id="labtests" class="form-control" placeholder="Type Lab Tests" autofocus>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="button" onclick="addnewlabtest();" class="btn btn-u" style="width:100%;padding:5px;">ADD</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div id="container" class="col-md-9">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <br/>
                                        <ul id="labtestitems" class="presitemslist">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                @if(Auth::user()->roleid === 2)
                    <div class="tab-pane fade in active" id="campaignParticipantList">

                    </div>
                @endif
            </div>
        </div>
        <div align="right">
            @if(Auth::user()->roleid ==1)
                <button type="button" class="btn btn-u" id="saveBtn" onclick="saveConversation('save');">Save</button>&nbsp;&nbsp;&nbsp;
                <button type="button" class="btn btn-u" id="submitBtn" onclick="saveConversation('submit');">Consultation Done</button>
            @endif
            @if(Auth::user()->roleid ==2)
                <button type="button" class="btn btn-u" id="printChatBtn" onclick="PrintElem('#messages')">Print Chat</button>&nbsp;&nbsp;&nbsp;
                <button type="button" class="btn btn-u" id="backBtn" onclick="return gotoProfile()">Back</button>
            @endif
        </div>
    </div>
    <!-- End Tab v1 -->
    <div id="allDisconnectedDiv"></div>
</div>
</div>
</div>
</div>
</div>
@endsection

@section('page.scripts')
    {{-- <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script> --}}
    <script src="https://av.healthkon.com/js/app.js"></script>
    <script src="https://av.healthkon.com/public/sdk/video.1.1.min.js"></script>
    <script>
    //var apiKey = "11IcZSHfzR7qQ4EiNQiH3PI5xJcF0N3DYuB18aOeKNrhG6gGSIbe1tXFqTUM"; // ApnaCare
    var apiKey = "XgRN5amGqICrv6ZV1yOpJIqCXtBXl9c06PPrHGMlIlKBbNSo5Yw2JmRDnbW3";
    var url = 'https://av.healthkon.com/api/authorize?api_token='+apiKey;
    var accessTokenFromServer = "";

    $(document).ready(function() {
        identity = 'test@apnacare.in';
        room = '';
        var video = new Video({
            identity: identity,
            room: room,
            localVideoContainer:     'local-video-container',
            remoteVideoContainer:    'remote-video-container',
            presenterIdentity:       'presenter@healthkon.com',
            presenterVideoContainer: 'presenter-video-container'
        });
        video.presenterInitiation(false);

        $('.content').on('click','#connect-button', function(){
            room = $('#room-name').val();
            console.log(room);

            $.ajax({
                url: url,
                type: 'GET',
                crossDomain: true,
                datatype: 'jsonp',
                jsonp:'jsonp',
                success: function(response) {
                    //console.log(response.token);
                    accessTokenFromServer = response.token;

                    initVideo(accessTokenFromServer);
                },
                error: function (jqXHR, exception) {
                    var msg = '';
                    if (jqXHR.status === 0) {
                        msg = 'Not connect.\n Verify Network.';
                    } else if (jqXHR.status == 404) {
                        msg = 'Requested page not found. [404]';
                    } else if (jqXHR.status == 500) {
                        msg = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                        msg = 'Requested JSON parse failed.';
                    } else if (exception === 'timeout') {
                        msg = 'Time out error.';
                    } else if (exception === 'abort') {
                        msg = 'Ajax request aborted.';
                    } else {
                        msg = 'Uncaught Error.\n' + jqXHR.responseText;
                    }
                    alert('Error: '+msg+'\nstatusText: ' + jqXHR.statusText);
                },
            });
        });

        function initVideo(accessTokenFromServer){
            video.authorize(accessTokenFromServer).then(callback);
        }

        function callback(){
            //console.log(video);
            video.connect().then(function (room){
                // `room` is the video conference room
                var joined = video.joinRoom(room);
                console.log(joined);
                // if participant is joined in the room
                if (joined.status){
                    // if chat is required
                    var chat = video.withChat({
                        messagesContainer: 'messages', // div id where the chat messages appear
                        messageInput: 'chat-input' // div id to type in the chat
                    }).then(function (chatClient){
                        var channelFound = chatClient.getChannelByUniqueName(video.room);
                        video.pushChatInfo('Connecting..'); // you can broadcast info to the others with the ‘pushChatInfo’ method
                        channelFound.then(function (channel){
                            // `channel` is the chat channel
                            // if channel is already found
                            video.pushChatInfo('Connected');
                            video.setupChatConversation(channel);
                            channel.sendMessage('has joined'); // trigger messages from the participant with the `sendMessage` method
                        }, function (error){
                            if (error.status == 404){
                                // create channel if not found
                                chatClient.createChannel({
                                    uniqueName: video.room, friendlyName: 'General Channel'
                                }).then(function (channel){
                                    video.chatChannel = channel;
                                    video.pushChatInfo('Connected');
                                    video.setupChatConversation(channel);
                                    channel.sendMessage('has joined');
                                });
                            }
                        });
                    });
                }
            });
        }
    });

    function startDrVideo() {
        $('#stop').removeAttr('disabled');
        $('#start').attr('disabled', 'disabled');

        // var ary = '{{ Auth::user()->email }}'.split("@", 2);
        // var identity = ary[0];
        var identity = '{{ Auth::user()->email }}';

        // var ary = '{{ $practitioner->email }}'.split("@", 2);
        // var practitioner = ary[0];
        var practitioner = '{{ $practitioner->email }}';

        var video = new Video({
            identity: identity,
            room: '{{ $_GET['booking'] }}',
            localVideoContainer: 'local-media',
            remoteVideoContainer: 'remote-media',
            presenterIdentity: practitioner,
            presenterVideoContainer: 'presenter-media'
        });

        video.presenterInitiation(true);
        video.setConferenceTimeout('{{ $conference_time }}');

        var accessTokenFromServer = "";
        url = "{{action('Conversation\VideoController@getAccessToken')}}";
        $.get(url, {userName:identity  }, function(data) {
            accessTokenFromServer = data;

            video.authorize(accessTokenFromServer).then(connected);

            function connected() {
                video.connect().then(function (room) {

                    //toggleHkPoster();

                    var joined = video.joinRoom(room);
                    if (joined.status) {
                        var chat = video.withChat({
                            messagesContainer: 'messages',
                            messageInput: 'chat-input'
                        }).then(function (chatClient) {
                            var channelFound = chatClient.getChannelByUniqueName(video.room);
                            video.pushChatInfo('Connecting..');

                            channelFound.then(function (channel) {
                                video.pushChatInfo('Connected');
                                video.setupChatConversation(channel);
                                window.hkChatChannel = channel;
                                window.hkVideoCon = video;
                                channel.sendMessage('has joined');
                                $('#stop').on('click', function () {
                                    channel.leave();
                                    delete video;
                                    window.hkChatChannel = '';
                                    window.hkVideoCon = '';
                                });
                            }, function (error) {
                                if (error.status == 404) {
                                    chatClient.createChannel({
                                        uniqueName: video.room,
                                        friendlyName: 'General Channel'
                                    }).then(function (channel) {
                                        video.chatChannel = channel;
                                        video.pushChatInfo('Connected');
                                        video.setupChatConversation(channel);
                                        window.hkChatChannel = channel;
                                        window.hkVideoCon = video;
                                        channel.sendMessage('has joined');
                                        $('#stop').on('click', function () {
                                            channel.leave();
                                            delete video;
                                            window.hkChatChannel = '';
                                            window.hkVideoCon = '';
                                        });
                                    });
                                }
                            });
                        });                        

                        log('Waiting for patient..');
                        // startTimer();
                        var d = new Date();
                        var curTimezoneOffset = d.getTimezoneOffset();
                        conferenceTime="REJECTED";
                        var confurl = "{{action('Conversation\VideoController@checkConversationsById')}}";
                        $.get(confurl, {bookingId:<?= request('booking') ?>,currentTimezoneOffset: curTimezoneOffset}, function(data) {
                            conferenceTime = data;
                            if(conferenceTime!="REJECTED")
                            {
                                var time = conferenceTime.split(":", 2);
                                countdown( "countdown", parseInt(time[0]),parseInt(time[1]));
                            }
                            else
                            {
                                $('#stop').trigger("click");
                                alert("Conversation is yet to start or has expired.");
                                //	url1 = "{{URL::action('Conversation\VideoController@viewConversations')}}";
                                if (role == 2)
                                url1 = "{{url('/patientsProfile')}}";
                                else if (role == 1)
                                url1 = "{{url('/practitionersProfile')}}";
                                else url1 = "{{url('/')}}";
                                window.location = url1;
                            }
                        });
                    }
                    room.on('participantConnected', function (participant) {
                        toggleHkPoster();
                        log('Connected.');
                    });
                    room.on('participantDisconnected', function (participant) {
                        if (participant.identity != practitioner) {
                            //room.disconnect();
                            toggleHkPoster();
                            log('Waiting for Patient...');
                            //allDisconnected();
                        }
                    });
                    $('#allDisconnectedDiv').on('click', function (){
                        room.disconnect();
                        allDisconnected();
                    });
                    $('#stop').click(function () {
                        room.disconnect();
                        log('Disconnected');
                        allDisconnected();
                        toggleHkPoster();
                        $('#stop').attr("disabled","disabled");
                        $('#start').removeAttr("disabled");
                    });
                });
            }
        });
    }
    </script>
@endsection
