<html>
<head>
  <title>Print Patient Report</title>
    <link href="{{ ViewHelper::ThemePlugin('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/print.css') }}" media="print" />
    <style>
        img.img-topbar{ margin-top: -5px !important;}
        .username:after{
          content: ' \25BE';
          padding-left: 10px;
          font-size: 20px;
        }
        .v-middle{
            vertical-align: middle !important;
        }
        body{
          font-size: 12px !important;
          background: rgb(204,204,204);
        }
        @page
        {
          size: A4;   /* auto is the initial value */
          margin: 0mm;  /* this affects the margin in the printer settings */
        }
        div.A4 {
            background: white;
            @if(Request::segment(3) == 'print')
            width: 21cm;
            height: auto;
            box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
            @else
            width: 793.707px;
            height: 1122.52px;
            box-shadow: 0 0 5px rgba(0,0,0,0.5);
            @endif
            display: block;
            margin: 0 auto;
        }
        .border-top{
          border-top: 1px solid #333 !important;
        }
        .border-bottom{
          border-bottom: 1px solid #333 !important;
        }
        .border-right{
          border-right: 1px solid #333 !important;
        }
        .border-left{
          border-left: 1px solid #333 !important;
        }
        .no-padding, .no-padding tbody tr td, .no-padding tbody tr th{
          padding: 0 !important;
          border: 1px solid #333;
        }
        .no-border, .no-border tbody tr td, .no-border tbody tr th{
          border: 0;
        }
        .services-table, .services-table thead tr th, .services-table tfoot tr th, .services-table tfoot tr td, .services-table  tbody tr th,.services-table tbody tr td{
          padding: 1px !important;
            border: 0;
        }
        .tbl-text{
            margin-top:1px !important;
            padding:2px !important;
            margin-bottom:1px !important;
        }
        h6{
          font-weight: bold;
        }
        .user-info{ background: rgba(96, 125, 139, 0.10) !important; border-bottom: 0.1em solid #3F51B5 !important; padding: 10px !important;}
    </style>
</head>
<body onload="javascript: window.print();">
  <div class="A4">
    <div class="col-lg-12 invoicereport" style="border: 0px solid black;background: white;margin: 15 0 0 0;">
          <br>
          <table class="table table-bordered no-padding" style="margin-bottom: 0 !important">
              <tr>
                  <th colspan="2" class="text-center">
                      <h4 style="font-size:40px; margin-top:4px !important;margin-bottom:2px !important; padding:0px !important">
                        <b>{{ $subscriber->org_code  }}</b>
                      </h4>
                  </th>
                  <th colspan="4" class="text-center">
                    <h6 style="font-size:40px; margin-top:4px !important;margin-bottom:2px !important; padding:0px !important">
                        Active Users: {{ count($caregivers) }}
                      </h6> 
                  </th>
                 
              </tr> <div class="col-lg-12 invoicereport" style="border: 0px solid black;background: white;margin: 15 0 0 0;">
            <br>
          </table>
          <table class="table table-bordered no-padding" style="margin-bottom: 0 !important">
            <thead>
              <tr>
                <th width="1%">#</th>
                <th width="4%">Employe Id</th>
                <th width="3%">First Name</th>
                <th width="3%">Middle Name</th>
                <th width="3%">Last Name</th>
              </tr>
            </thead>
            <tbody>
            @if(count($caregivers))
                  @foreach($caregivers as $index => $caregiver)
                      <tr>
                          <td>{{ $index+ 1 }}</td>
                          <td>{{ $caregiver->caregiver->employee_id }}</td>
                          <td>{{ $caregiver->caregiver->first_name }}</td>
                          <td>{{ $caregiver->caregiver->middle_name }}</td>
                          <td>{{ $caregiver->caregiver->last_name }}</td>
                      </tr>
                  @endforeach
              @else    
              <td>No Users</td>
              @endif
            </tbody>
          </table>
        </div>
  </div>
</body>
</html>