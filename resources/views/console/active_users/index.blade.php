@extends('layouts.console-layout')

@section('page_title','Billing - ')

@section('active_users','active')

@section('page.styles')
    <style>
    .expandClass{
        margin: 0 !important;
        margin-top: 6% !important;
    }
    p{
        margin-top: 5px !important;
    }
    </style>
@endsection

@section('plugin.styles')
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet"/>
<!-- JQuery DataTable Css -->
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
                <button type="button" class="btn btn-info waves-effect expandPage"><i class="material-icons">apps</i></button>
            <div class="header clearfix">
                <h2 class="col-sm-9">
                    Active Users
                    <small>Smart Health Connect Software Active Users History</small>
                </h2>
            </div>
            <br>
            <div class="clearfix">
                <form action="{{ route('console.active_users') }}" id="getScheduledStaffCountSubmit" method="post">
                    {{ csrf_field() }}
                   <div class="col-lg-3">
                    <div class="form-group">
                        <div class="form-line">
                            <select class="form-control show-tick" name="corporate_office" id="corporate_office" required>
                                <option value>-- Select Corporate --</option>
                                @foreach($corporates as $corporate)
                                <option value="{{$corporate->id}}">{{ $corporate->organization }} - {{ $corporate->type }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                   </div>
                   <div class="col-lg-3">
                    <div class="form-group">
                        <div class="form-line">
                            <select class="form-control show-tick" name="corporate_branch" id="corporate_branch" required disabled>
                                <option value>-- Select Corporate First --</option>
                            </select>
                        </div>
                    </div>
                   </div>
                    <div class="col-md-3">
                        <input type="text" class="form-control flatpickr" id="usage_duration" name="usage_duration" placeholder="Usage Duration">
                    </div>
                    <button type="button" class="btn btn-success btn-md waves-effect" id="getScheduledStaffCount">Check Scheduled Staffs</button>
                </form>
            </div>
            <div class="body">
                <table class="table table-bordered table-hover table-condensed reportTable display nowrap" id="user_list_table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th width="5%">Employee Id</th>
                            <th>First Name</th>
                            <th>Middle Name</th>
                            <th>Last Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($caregivers))
                            @foreach($caregivers as $index => $caregiver)
                                <tr>
                                    <td>{{ $index+ 1 }}</td>
                                    <td>{{ $caregiver->caregiver->employee_id }}</td>
                                    <td>{{ $caregiver->caregiver->first_name }}</td>
                                    <td>{{ $caregiver->caregiver->middle_name }}</td>
                                    <td>{{ $caregiver->caregiver->last_name }}</td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
@section('page.scripts')
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script>
    $(function(){
        if(localStorage.getItem('expandMode') == 'true'){
            $('.content').toggleClass('expandClass');
            $('.sidebar').toggleClass('hide');
        }

        $(".flatpickr").flatpickr({
            dateFormat: "d-m-Y",
            mode: "range",
            locale: { rangeSeparator: ' - ' }
        });

        var branchList = [];
        @if(isset($branches))
        @foreach ($branches as $branch)
        branchList.push({id: '{{ $branch->id }}', key: '{{ $branch->corp_id }}', value:'{{ $branch->org_code }}', rpu:'{{ $branch->charge_per_user }}'});
        @endforeach
        @endif

        $('.content').on('change','#corporate_office', function(){
            var val = $(this).val();
            var branchesHtml = '<option value="">-- Select Branch --</option>';
            var branches = branchList.filter(function(item){ return item.key === val; });
            if(branches.length > 0){
                for(key in branches){
                    if(branches.hasOwnProperty(key)){
                        branchesHtml += '<option value="'+branches[key].id+'">'+branches[key].value+' @ '+branches[key].rpu+'</option>';
                    }
                }
                $('#corporate_branch').prop('disabled',false);
            }else{
                $('#corporate_branch').prop('disabled',true);
            }

            $('#corporate_branch').html(branchesHtml);
            $('#corporate_branch').selectpicker('refresh');
        });

        $('.content').on('change','#corporate_office_new', function(){
            var val = $(this).val();
            var branchesHtml = '<option value="">-- Select Branch --</option>';
            var branches = branchList.filter(function(item){ return item.key === val; });
            if(branches.length > 0){
                for(key in branches){
                    if(branches.hasOwnProperty(key)){
                        branchesHtml += '<option value="'+branches[key].id+'">'+branches[key].value+' @ '+branches[key].rpu+'</option>';
                    }
                }
                $('#corporate_branch_new').prop('disabled',false);
            }else{
                $('#corporate_branch_new').prop('disabled',true);
            }

            $('#corporate_branch_new').html(branchesHtml);
            $('#corporate_branch_new').selectpicker('refresh');
        });

        $('#user_list_table').DataTable( {
            scrollY: 260,
            scrollX: true,
            scrollCollapse: true,
            lengthMenu: [[25,50,100,-1],[25,50,100,"All"]],
            language: {
                processing: "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate' style='color:#ff0000'> Loading....</span>"
            },
            order: [[0,'asc']],
            // displayLength: -1,
            dom: 'Bflrpt',
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: '{{ $data[0]['user'] }}_active_users_{{ $data[0]['period'] }}',
                    exportOptions: {
                        format: {
                            body: function ( data, row, column ) {
                                // Strip $ from salary column to make it numeric
                                return (column === 40)?'"'+data.replace( /<br\s*\/?>/ig, '\r\n' )+'"':data;
                            }
                        }
                    }
                }
            ],
        } );
    });

    $('.content').on('click','#getScheduledStaffCount',function(){
        var corpID = $('#corporate_office').val();
        var branchID = $('#corporate_branch').val();
        var range = $('#usage_duration').val();
        if(corpID == '' || branchID == '' || range == ''){
            alert('Please select all the fields');
            return false;
        }else{
          $( "#getScheduledStaffCountSubmit" ).submit();
        }
    })

    $('.content').on('click','.expandPage',function(){
        $('.content').toggleClass('expandClass');
        $('.sidebar').toggleClass('hide');
        if( $('.content').hasClass('expandClass') && $('.sidebar').hasClass('hide') ){
            localStorage.setItem('expandMode', true);
        }else{
            localStorage.removeItem('expandMode');
        }
    });

    $('.table').on('click', '.btnEditPayment', function(){
        organization = $(this).data('organization');
        orgCode = $(this).data('orgcode');
        monthYear = $(this).data('monthyear');
        usageDuration = $(this).data('usageduration');
        invoiceNo = $(this).data('invoiceno');
        invoiceAmount = $(this).data('invoiceamount');
        amountPaid = $(this).data('amountpaid');
        status = $(this).data('status');

        $('.action_editPayment').html('Update Invoice - '+invoiceNo);
        $('.editPaymentForm').attr('action',$(this).data('url'));
        $('.editPaymentForm input[name=_token]').val('{{ csrf_token() }}');

        $('.editPaymentForm #corporate_office_edit').html(organization);
        $('.editPaymentForm #corporate_branch_edit').html(orgCode);
        $('.editPaymentForm #usage_duration_edit').html(monthYear);
        $('.editPaymentForm #invoice_monthYear_edit').html(usageDuration);
        $('.editPaymentForm #invoice_amount_edit').html(invoiceAmount);
        $('.editPaymentForm #invoice_amount_received').html(amountPaid);
        if(invoiceAmount == amountPaid){
            $('.editPaymentForm #invoice_amount_received_update').attr('disabled',true);
            $('.editPaymentForm #invoice_amount_received_update').prop('placeholder','');
        }else{
            $('.editPaymentForm #invoice_amount_received_update').attr('disabled',false);
            $('.editPaymentForm #invoice_amount_received_update').prop('max',invoiceAmount-amountPaid);
            $('.editPaymentForm #invoice_amount_received_update').prop('placeholder','Remaining amount : Rs '+(invoiceAmount-amountPaid));
        }
        $('.editPaymentForm select[name=status]').selectpicker('val',status);


        $('#editPayment').modal('show');
    });
</script>
@endsection