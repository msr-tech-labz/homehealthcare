@extends('layouts.console-layout')

@section('page_title','Dashboard - ')

@section('active_home', 'active')

@section('content')
    <style>
        .text-red td, .text-red{
            color: #ff0000 !important;
        }
    </style>
    <div class="block-header">
        <h2>DASHBOARD</h2>
    </div>
    
    <div class="row clearfix">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-green hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">domain</i>
                </div>
                <div class="content">
                    <div class="text">ACTIVE ACCOUNTS</div>
                    <div class="number">{{ $stats['paid_accounts'] }}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-indigo hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">people</i>
                </div>
                <div class="content">
                    <div class="text">LICENCED USERS</div>
                    <div class="number">{{ $stats['paid_users'] }}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-pink hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">account_box</i>
                </div>
                <div class="content">
                    <div class="text">TRIAL ACCOUNTS</div>
                    <div class="number">{{ $stats['trial_accounts'] }}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-orange hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">person_add</i>
                </div>
                <div class="content">
                    <div class="text">TRIAL LICENCES</div>
                    <div class="number">{{ $stats['trial_users'] }}</div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row clearfix">
        <!-- Trail Accounts -->
        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
            <div class="card">
                <div class="header">
                    <h2>TRIAL ACCOUNTS</h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-hover table-condensed ashboard-task-infos">
                            <thead>
                                <tr>
                                    <th>Account</th>
                                    <th>Licenses</th>
                                    <th>Expiry Date & Info</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($trialAccounts as $account)
                                    <tr class="@if($account['days_remaining'] <= 5){{ 'text-red' }}@endif">
                                        <td>{{ $account['account_name'] }}</td>
                                        <td>{{ $account['licenses'] }}</td>
                                        <td>{{ $account['trial_end_date'] }} : {{ $account['days_remaining'] }} day(s)</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4" class="text-center">NO TRIAL ACCOUNTS FOUND</td>
                                    </tr>
                                @endforelse                        
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Trail Accounts -->
                
        <!-- Top 5 Accounts -->
        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
            <div class="card">
                <div class="body bg-teal">
                    <div class="font-bold m-b--35">TOP 5 PAID ACCOUNTS</div>
                    <ul class="dashboard-stat-list">
                @if(count($topAccounts))
                    @foreach ($topAccounts as $account)
                        <li>
                            {{ $account['account_name'] }}
                            <span class="pull-right"><b>{{ $account['licenses'] }}</b> <small>USERS</small></span>
                        </li>
                    @endforeach
                @endif                     
                    </ul>
                </div>
            </div>
        </div>
        <!-- #END# Top 5 Accounts -->
    </div>
@endsection
