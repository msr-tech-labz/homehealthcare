@extends('layouts.console-layout')

@section('page_title','Billing - ')

@section('active_administration','active')

@section('page.styles')
    <style>
    .expandClass{
        margin: 0 !important;
        margin-top: 6% !important;
    }
    p{
        margin-top: 5px !important;
    }
    </style>
@endsection

@section('plugin.styles')
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet"/>
<!-- JQuery DataTable Css -->
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
                <button type="button" class="btn btn-info waves-effect expandPage"><i class="material-icons">apps</i></button>
            <div class="header clearfix">
                <h2 class="col-sm-9">
                    Subscribers List
                    <small>Smart Health Connect Software Active Subscribers</small>
                </h2>
            </div>
            <br>
            <div class="body">
                <table class="table table-bordered table-hover table-condensed reportTable display nowrap" id="user_list_table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Org Code</th>
                            <th>Database Name</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($subscribers))
                            @foreach($subscribers as $index => $subscriber)
                                <tr>
                                    <td>{{ $index+ 1 }}</td>
                                    <td>{{ $subscriber->org_code }}</td>
                                    <td>{{ $subscriber->database_name }}</td>
                                    <td>{{ $subscriber->status }}</td>
                                    <td>
                                        <div class="switch">
                                            <label><input type="checkbox" name="status" data-mid="{{ Helper::encryptor('encrypt',$subscriber->id) }}" {{ $subscriber->status=='Active'?'checked':'' }}><span class="lever"></span></label>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
@section('page.scripts')
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script>
    $(function(){
        if(localStorage.getItem('expandMode') == 'true'){
            $('.content').toggleClass('expandClass');
            $('.sidebar').toggleClass('hide');
        }

        $(".flatpickr").flatpickr({
            dateFormat: "d-m-Y",
            mode: "range",
            locale: { rangeSeparator: ' - ' }
        });

        $('.content').on('click','.expandPage',function(){
            $('.content').toggleClass('expandClass');
            $('.sidebar').toggleClass('hide');
            if( $('.content').hasClass('expandClass') && $('.sidebar').hasClass('hide') ){
                localStorage.setItem('expandMode', true);
            }else{
                localStorage.removeItem('expandMode');
            }
        });

        $('.content').on('change','input[name=status]',function(){
            var id = $(this).data('mid');
            var status = $(this).is(':checked')?1:0;
            $.ajax({
                url: '{{ route('console.utilities.activeusersstatus') }}',
                type: "POST",  
                data: {_token:'{{ csrf_token() }}', id:id,status:status},
                success: function (state){
                    window.setTimeout(function(){location.reload()},100);
                },
                error: function (error){
                    console.log(error);
                }
            });
        });

    });
</script>
@endsection