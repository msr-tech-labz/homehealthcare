@extends('layouts.console-layout')

@section('page_title','Subscribers - ')

@section('active_subscribers','active')

@section('plugin.styles')
<link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />
@endsection

@section('page.styles')
<style>
div#sp_inputsql textarea {
    margin: 0;
    padding: 0;
    width: 99%;
    white-space: pre;
    word-wrap: normal;
}
.executesql{
    position: absolute !important;
    top: 35% !important;
    right: 5% !important;
}
</style>
@endsection

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <h2 class="col-sm-12">
                    SQL Executor<br>
                    <code style="font-size: 70%;">Execute Commands to Add,Update,Alter,or Delete Operations in the databases.</code>
                </h2>
            </div>
            <div class="body">
                <textarea style="font-family: monospace !important;" id="inputsql" name="inputsql" cols="100" rows="15" wrap="off" onfocus="this.value='';this.onfocus=null;">Enter your sql code here...
                </textarea>
                <button class="btn bg-teal executesql waves-effect">Execute SQL</button>
                <hr>
                <div class="table-responsive">
                    <table class="table table-bordered condensed providerTable">
                        <thead>
                            <tr>
                                <th width="10%" class="provider-sel text-center"><input name="select_all" type="checkbox" id="select_all" class="with-gap chk-col-blue filled-in" value="All" title="Select All"/><label for="select_all"></label></th>
                                <th width="20%">Provider</th>
                                <th width="30%">Database Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($provider) && count($provider))
                            @foreach ($provider as $index => $cb)
                            <tr data-id="{{ \Helper::encryptor('encrypt',$cb['tenant_id']) }}">
                                <th class="text-center provider-sel">
                                    <input id="provider_{{ $cb['tenant_id'] }}" type="checkbox" class="with-gap chk-col-blue filled-in provider-sel-chk" value="{{ $cb['tenant_id'] }}"/><label for="provider_{{ $cb['tenant_id'] }}">&nbsp;</label>
                                </th>
                                <td>{{ $cb['profile'] }}</td>
                                <td>{{ $cb['database_name'] }}</td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No Provider(s) Found</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('plugin.scripts')
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="{{ ViewHelper::ThemePlugin('bootstrap-notify/bootstrap-notify.js') }}"></script>
<script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
<script>
        $('.content').on('click','.executesql', function(){
            var items = [];
            var sql = $('#inputsql').val();

            $('.provider-sel-chk').each(function(i){
                if($(this).is(':checked')){
                    items.push($(this).val());
                }
            });

            if(items.length <= 0){
                alert("Please select provider(s)!");
                return false;
            }

            if(sql != '' && items.length > 0){
                $.ajax({
                    url: '{{ route('console.utilities.runGlobalSqlQuery') }}',
                    type: 'POST',
                    data: {sql: sql, items: items, _token: '{{ csrf_token() }}' },
                    success: function (d){
                        if(d.error == true){
                            showNotification('bg-red',d.msg, 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                        }else{
                            showNotification('bg-green', 'Sql Successfully Executed', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                        }
                        setTimeout(function(){
                            window.location.reload();
                        },1000);
                    },
                    error: function (err){
                        console.log(err);
                        showNotification('bg-red', 'Oops!! Sql Successfully Not Executed', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                    }
                })
            }
        });

        $('.content').on('click','#select_all', function(){
            if($(this).is(':checked')){
                $('.provider-sel-chk').each(function(i){
                    $(this).prop('checked',true);
                });
            }else{
                $('.provider-sel-chk').each(function(i){
                    $(this).prop('checked',false);
                });
            }
        });
    </script>
    @endsection
