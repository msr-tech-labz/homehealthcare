@extends('layouts.console-layout')

@section('page_title','Administration - ')

@section('active_administration','active')

@section('page.styles')
<style>
</style>
@endsection

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="card">
                <div class="body">
                    <div>
                        <a href="{{ route('console.utilities.sqlExecutor') }}">
                            <i class="material-icons col-red" style="font-size: 45px;">code</i>
                            <div style="font-size: 30px;text-align: right;margin-top: -50px;">SQL Executor</div>
                            <small style="float: right;" class="col-pink">Execute Sql Queries on Databases</small><br>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="card">
                <div class="body">
                    <div>
                        <a href="{{ route('console.utilities.activeusers') }}">
                            <i class="material-icons col-red" style="font-size: 45px;">people</i>
                            <div style="font-size: 30px;text-align: right;margin-top: -50px;">Active Subscribers</div>
                            <small style="float: right;" class="col-pink">Active Subscribers</small><br>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
