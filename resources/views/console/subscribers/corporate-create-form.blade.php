@extends('layouts.console-layout')

@section('page_title', 'New Subscriber - Corporate')

@section('active_subscribers','active')

@section('plugin.styles')
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet"/>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section('content')

@if (count($errors) > 0)
<div class="row clearfix">
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif
<div class="loader"></div>
<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card">
			<form id="corporateSaveForm" action="{{ route('console.subscribers.createCorporate') }}" method="POST">
				{{ csrf_field() }}
				<div class="header clearfix">
					<a href="{{ route('console.corporates') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
						<i class="material-icons">arrow_back</i>
					</a>

					<h2 class="col-sm-8">
						New Corporate Head Office
						<small>Subscriber Type - Corporate</small>
					</h2>
					<button type="submit" class="btn btn-success btn-lg waves-effect btnCorpCreate hide" style="float: right;">Create</button>
				</div>
				<div class="body">
					<div class="row clearfix">
						<div class="col-sm-6 form-horizontal">
							<h2 class="card-inside-title">
								Basic Details
								<small>Organization Name, Address</small>
							</h2>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="corporate_id">Corporate ID</label>
								</div>
								<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
									<div class="form-group">
										<div class="form-line">
											<input type="text" pattern="[a-zA-Z]+"  title="a-z and A-Z are allowed. Spaces,numbers,and special characters are not allowed" onkeypress="return avoidSpace(event)" id="corporate_id" name="corporate_id" class="form-control col-teal" required>
										</div>
									</div>
								</div>
								<a type="button" class="btn btn-primary waves-effect checkCorpDB">Check Availability</a>
								<i class="material-icons available hide" style="color: green;font-size: 20px;display: inline;">check_circle</i>
								<i class="material-icons unavailable hide" style="color: red;font-size: 20px;display: inline;">cancel</i>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="corporate_name">Corp Name</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="corporate_name" name="corporate_name" class="form-control" required>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="corporate_type">Billing Type</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<select class="form-control show-tick" name="corporate_type" required>
												<option value="User Based" selected >User Based</option>
												<option value="Lump Sum">Lump Sum</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="corporate_type">Billing Expiry</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="corporate_expiry" name="corporate_expiry" class="form-control" value="NA" required>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-sm-6 form-horizontal">
							<h2 class="card-inside-title">
								Location Details
								<small>Email, Phone Number, Address</small>
							</h2>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="corporate_email">Email</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="email" id="corporate_email" name="corporate_email" class="form-control" required>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="corporate_address">Address</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="corporate_address" name="corporate_address" class="form-control" required>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="corporate_state">State</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<select class="form-control show-tick" id="corporate_state" name="corporate_state" data-live-search="true" required>
											<option selected value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
											<option value="Andhra Pradesh">Andhra Pradesh</option>
											<option value="Arunachal Pradesh">Arunachal Pradesh</option>
											<option value="Assam">Assam</option>
											<option value="Bihar">Bihar</option>
											<option value="Chandigarh">Chandigarh</option>
											<option value="Chhattisgarh">Chhattisgarh</option>
											<option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
											<option value="Daman and Diu">Daman and Diu</option>
											<option value="Delhi">Delhi</option>
											<option value="Goa">Goa</option>
											<option value="Gujarat">Gujarat</option>
											<option value="Haryana">Haryana</option>
											<option value="Himachal Pradesh">Himachal Pradesh</option>
											<option value="Jammu and Kashmir">Jammu and Kashmir</option>
											<option value="Jharkhand">Jharkhand</option>
											<option value="Karnataka">Karnataka</option>
											<option value="Kerala">Kerala</option>
											<option value="Lakshadweep">Lakshadweep</option>
											<option value="Madhya Pradesh">Madhya Pradesh</option>
											<option value="Maharashtra">Maharashtra</option>
											<option value="Manipur">Manipur</option>
											<option value="Meghalaya">Meghalaya</option>
											<option value="Mizoram">Mizoram</option>
											<option value="Nagaland">Nagaland</option>
											<option value="Odisha">Odisha</option>
											<option value="Pondicherry">Pondicherry</option>
											<option value="Punjab">Punjab</option>
											<option value="Rajasthan">Rajasthan</option>
											<option value="Sikkim">Sikkim</option>
											<option value="Tamil Nadu">Tamil Nadu</option>
											<option value="Telangana">Telangana</option>
											<option value="Tripura">Tripura</option>
											<option value="Uttaranchal">Uttaranchal</option>
											<option value="Uttar Pradesh">Uttar Pradesh</option>
											<option value="West Bengal">West Bengal</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="corporate_landline">Landline</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="number" id="corporate_landline" name="corporate_landline" class="form-control" required>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-sm-12 form-horizontal">
							<h2 class="card-inside-title">
								Initial User
								<small>Set Initial User Credentials</small>
							</h2>
							<div class="row clearfix">
								<div class="col-lg-1 form-control-label">
									<label for="corporate_profile_full_name">Name</label>
								</div>
								<div class="col-lg-5">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="corporate_profile_full_name" name="corporate_profile_full_name" class="form-control" required>
										</div>
									</div>
								</div>

								<div class="col-lg-1 form-control-label">
									<label for="corporate_profile_email">Email</label>
								</div>
								<div class="col-lg-5">
									<div class="form-group">
										<div class="form-line">
											<input type="email" id="corporate_profile_email" name="corporate_profile_email" class="form-control" required>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-1 form-control-label">
									<label for="corporate_profile_password">Password</label>
								</div>
								<div class="col-lg-5">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="corporate_profile_password" name="corporate_profile_password" class="form-control" required>
										</div>
									</div>
								</div>

								<div class="col-lg-1 form-control-label">
									<label for="corporate_status">Status</label>
								</div>
								<div class="col-lg-5">
									<div class="form-group">
										<div class="form-line">
											<select class="form-control show-tick" name="corporate_status" required>
												<option value="Active" selected >Active</option>
												<option value="Inactive">Inactive</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('plugin.scripts')
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
@endsection

@section('page.scripts')
<script>
	function avoidSpace(event) {
		var k = event ? event.which : window.event.keyCode;
		if((k >= 97 && k <= 122) || (k >= 65 && k <= 90))
			return true;
		else
			return false;
	}

	$(function(){
		$("#corporate_expiry").flatpickr({
			minDate: "today",
			dateFormat: "d-m-Y",
			mode: "single",
			onReady: function ( dateObj, dateStr, instance ) {
				const $clear = $( '<div class="flatpickr-clear"><button class="btn btn-info flatpickr-clear-button">Clear</button></div>' )
				.on( 'click', () => {
					$("#corporate_expiry").val('NA');
					instance.close();
				})
				.appendTo( $( instance.calendarContainer ) );
			}
		});
	});

	$('.content').on('keyup', '#corporate_id', function(){
		if (!$('.btnCorpCreate').hasClass('hide')){
			$('.btnCorpCreate, .available, .unavailable').addClass('hide');
		}
	});

	$('.content').on('click','.checkCorpDB',function(){
		var corpid = $('#corporate_id').val();
		if(corpid == ''){
			alert('Head Office ID must be present');
			return false;
		}

		$.ajax({
			url: '{{route('console.subscribers.check-corp-id')}}',
			type: 'POST',
			data: { corpID: corpid ,_token: '{{ csrf_token() }}'},
			success: function (state){
				if(state == "Available"){
					$('.available').removeClass('hide');
					$('.unavailable').addClass('hide');
					$('.btnCorpCreate').removeClass('hide');
				}
				if(state == "Unavailable"){
					$('.available').addClass('hide');
					$('.unavailable').removeClass('hide');
					$('.btnCorpCreate').addClass('hide');
				}
			},
			error: function(e){
				console.log(e);
			}
		});
	});

	$('#corporateSaveForm').submit(function() {
	       $(".loader").html(`<div class="page-loader-wrapper">
	       	<div class="loader">
	       	<div class="md-preloader pl-size-md">
	       	<svg viewbox="0 0 75 75">
	       	<circle cx="37.5" cy="37.5" r="33.5" class="pl-red" stroke-width="4" />
	       	</svg>
	       	</div>
	       	<p>Please wait...</p>
	       	</div>
	       	</div>`);
	});
</script>
@endsection
