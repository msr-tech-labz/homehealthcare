@extends('layouts.console-layout')

@section('page_title','New Subscriber - Branch')

@section('active_subscribers','active')

@section('plugin.styles')
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section('content')

@if (count($errors) > 0)
<div class="row clearfix">
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif
<div class="loader"></div>
<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card">
			<div class="header clearfix">
				<a href="{{ url()->previous() }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
					<i class="material-icons">arrow_back</i>
				</a>
				<h2 class="col-sm-8">
					New Branch
					<small>Subscriber Type - Branch</small>
				</h2>
				<div class="col-sm-3 text-right">
					<b>Corporate ID: <span style="font-size: 18px">{{ $corporate->corporate_id }}</span></b>
				</div>
			</div>
			<div class="body">
				<form id="branchSaveForm" action="{{ route('console.subscribers.createBranch') }}" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active">
							<a href="#profile_tab" data-toggle="tab" aria-expanded="false">
								<i class="material-icons">location_city</i> Branch Profile
							</a>
						</li>
						<button type="submit" class="btn btn-success btn-lg waves-effect btnBranchCreate hide" style="float: right;">Create Branch</button>
					</ul><br>
					<input type="hidden" name="corp_id" value="{{ Helper::encryptor('encrypt',$corporate->id) }}">
					<div class="row clearfix">
						<div class="col-sm-6 form-horizontal">
							<h2 class="card-inside-title">
								Basic Details
								<small>Organization Name, Address</small>
							</h2>
							<div class="row clearfix">
								<div class="col-lg-3 form-control-label">
									<label for="org_code" class="required">Branch ID</label>
								</div>
								<div class="col-lg-5">
									<div class="form-group">
										<div class="form-line">
											<input type="text" pattern="[a-zA-Z]+"  title="a-z and A-Z are allowed. Spaces,numbers,and special characters are not allowed " onkeypress="return avoidSpace(event)" id="org_code" name="org_code" class="form-control col-teal" required>
										</div>
									</div>
								</div>
								<a type="button" class="btn btn-primary waves-effect checkBranchAvailability">Check Availability</a>
								<i class="material-icons available hide" style="color: green;font-size: 20px;display: inline;">check_circle</i>
								<i class="material-icons unavailable hide" style="color: red;font-size: 20px;display: inline;">cancel</i>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="organization_name" class="required">Org Name</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="organization_name" name="organization_name" class="form-control" placeholder="Ex. Healthcare Pvt Ltd" required>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="organization_address" class="required">Address</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="organization_address" name="organization_address" class="form-control" placeholder="Address" required>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="organization_area" class="required">Area</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="organization_area" name="organization_area" class="form-control" placeholder="Ex. Sahakar Nagar" required>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="organization_city" class="required">City</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="organization_city" name="organization_city" class="form-control" placeholder="Ex. Bengaluru" required>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="organization_zipcode" class="required">Zip Code</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="organization_zipcode" name="organization_zipcode" class="form-control" placeholder="Ex. 560001" required>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="organization_state" class="required">State</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<select class="form-control show-tick" id="organization_state" name="organization_state" data-live-search="true" required>
												<option selected value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
												<option value="Andhra Pradesh">Andhra Pradesh</option>
												<option value="Arunachal Pradesh">Arunachal Pradesh</option>
												<option value="Assam">Assam</option>
												<option value="Bihar">Bihar</option>
												<option value="Chandigarh">Chandigarh</option>
												<option value="Chhattisgarh">Chhattisgarh</option>
												<option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
												<option value="Daman and Diu">Daman and Diu</option>
												<option value="Delhi">Delhi</option>
												<option value="Goa">Goa</option>
												<option value="Gujarat">Gujarat</option>
												<option value="Haryana">Haryana</option>
												<option value="Himachal Pradesh">Himachal Pradesh</option>
												<option value="Jammu and Kashmir">Jammu and Kashmir</option>
												<option value="Jharkhand">Jharkhand</option>
												<option value="Karnataka">Karnataka</option>
												<option value="Kerala">Kerala</option>
												<option value="Lakshadweep">Lakshadweep</option>
												<option value="Madhya Pradesh">Madhya Pradesh</option>
												<option value="Maharashtra">Maharashtra</option>
												<option value="Manipur">Manipur</option>
												<option value="Meghalaya">Meghalaya</option>
												<option value="Mizoram">Mizoram</option>
												<option value="Nagaland">Nagaland</option>
												<option value="Odisha">Odisha</option>
												<option value="Pondicherry">Pondicherry</option>
												<option value="Punjab">Punjab</option>
												<option value="Rajasthan">Rajasthan</option>
												<option value="Sikkim">Sikkim</option>
												<option value="Tamil Nadu">Tamil Nadu</option>
												<option value="Telangana">Telangana</option>
												<option value="Tripura">Tripura</option>
												<option value="Uttaranchal">Uttaranchal</option>
												<option value="Uttar Pradesh">Uttar Pradesh</option>
												<option value="West Bengal">West Bengal</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="organization_country" class="required">Country</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="organization_country" name="organization_country" class="form-control" placeholder="Ex. India" required>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-sm-6 form-horizontal">
							<h2 class="card-inside-title">
								Communication Details
								<small>Phone Number, Email</small>
							</h2>
							<div class="row clearfix">
								<div class="col-lg-4 form-control-label">
									<label for="contact_person" class="required">Contact Person</label>
								</div>
								<div class="col-lg-8">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="contact_person" name="contact_person" class="form-control" placeholder="Contact Person" required>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-4 form-control-label">
									<label for="phone_number" class="required">Phone Number</label>
								</div>
								<div class="col-lg-8">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="phone_number" name="phone_number" class="form-control" placeholder="Phone Number" required>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-4 form-control-label">
									<label for="landline_number">Landline Number</label>
								</div>
								<div class="col-lg-8">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="landline_number" name="landline_number" class="form-control" placeholder="Landline Number">
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-4 form-control-label">
									<label for="email" class="required">Accounts Email</label>
								</div>
								<div class="col-lg-8">
									<div class="form-group">
										<div class="form-line">
											<input type="email" id="email" name="email" class="form-control searchCol" placeholder="Email address" required>
										</div>
									</div>
								</div>
							</div>
							<h2 class="card-inside-title">
								Logo
								<small>Organization Logo</small>
							</h2>
							<div class="row clearfix">
								<div class="col-sm-1"></div>
								<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 profile-img-block">
									<img src="{{ asset('img/default-avatar.jpg') }}" style="max-width: 360px; max-height: 160px; padding: 5px;" class="img img-thumbnail img-profile"/>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">
									<br>
									<input type="file" id="organization_logo" name="organization_logo" class="form-control profile-picture"><br><br>
								</div>
							</div>
						</div>

						<div class="col-sm-6 form-horizontal">
							<h2 class="card-inside-title">
								Branch Admin
								<small>Admin user for the branch account</small>
							</h2>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="admin_name" class="required">Admin Name</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="admin_name" name="admin_name" class="form-control" required>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="admin_email" class="required">Admin Email</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="admin_email" name="admin_email" class="form-control" required>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="password" class="required">Password</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="admin_password" name="admin_password" class="form-control" required>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row clearfix form-horizontal">
							<div class="col-sm-6">
								<h2 class="card-inside-title">
									Subscription Details
									<small>Users Limit, Rate per User</small>
								</h2>
								<div class="row clearfix">
									<div class="col-lg-2 form-control-label">
										<label for="users_limit">Users</label>
									</div>
									<div class="col-lg-3">
										<div class="form-group">
											<div class="form-line">
												<input type="number" min="0" id="users_limit" name="users_limit" class="form-control">
											</div>
										</div>
									</div>
									<div class="col-lg-3 form-control-label">
										<label for="charge_per_user">Charge/User</label>
									</div>
									<div class="col-lg-4">
										<div class="form-group">
											<div class="form-line">
												<input type="number" min="0.00" id="charge_per_user" name="charge_per_user" class="form-control">
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-sm-6">
								<h2 class="card-inside-title">
									Instance Details
									<small>Instance type, Trial end date</small>
								</h2>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
										<label for="instance_type">Instance Type</label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<div class="form-group">
											<input type="radio" name="instance_type" id="instance_type_paid" class="with-gap radio-col-indigo" value="Paid" checked="">
											<label for="instance_type_paid">Paid</label>
											<input type="radio" name="instance_type" id="instance_type_trial" class="with-gap radio-col-indigo" value="Trial">
											<label for="instance_type_trial">Trial</label>
										</div>
									</div>
								</div>
								<div class="row clearfix end-date-block" style="display:none;">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
										<label for="trial_end_date">Trial end date</label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" name="trial_end_date" id="trial_end_date" class="form-control">
											</div>
										</div>
									</div>
								</div>                              
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('plugin.scripts')
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
@endsection

@section('page.scripts')
<script>
	function avoidSpace(event) {
		var k = event ? event.which : window.event.keyCode;
		if((k >= 97 && k <= 122) || (k >= 65 && k <= 90))
			return true;
		else
			return false;
	}
    $(function(){
		$('.content').on('change','input[name=instance_type]', function(){
			if($(this).val() == 'Paid'){
				$('.end-date-block').hide();
				$("#trial_end_date").val('');
			}else{
				$('.end-date-block').show();
			}
		});

		$("#trial_end_date").flatpickr({
			minDate: "today",
			dateFormat: "d-m-Y",
			mode: "single",
			onReady: function ( dateObj, dateStr, instance ) {
				const $clear = $( '<div class="flatpickr-clear"><button class="btn btn-info flatpickr-clear-button">Clear</button></div>' )
				.on( 'click', () => {
					$("#trial_end_date").val('');
					$('.end-date-block').hide();
					$('input[name=instance_type][value=Paid]').prop('checked','checked')
					instance.close();
				})
				.appendTo( $( instance.calendarContainer ) );
			}
		});
    });

    function handleFileSelect(evt) {
		var files = evt.target.files; // FileList object

		// Loop through the FileList and render image files as thumbnails.
		for (var i = 0, f; f = files[i]; i++) {
			// Only process image files.
			if (!f.type.match('image.*')) {
				continue;
			}
			var reader = new FileReader();

			// Closure to capture the file information.
			reader.onload = (function(theFile) {
				return function(e) {
					// Render thumbnail.
					var span = document.createElement('span');
					span.innerHTML = ['<img style="width:160px;height: 160px;" class="img-responsive img-thumbnail" src="', e.target.result,
					'" title="', escape(theFile.name), '"/>'].join('');
					$('.profile-img-block').html(span);
				};
			})(f);

			// Read in the image file as a data URL.
			reader.readAsDataURL(f);
		}
	}

	document.getElementById('organization_logo').addEventListener('change', handleFileSelect, false);

	$('.content').on('keyup', '#org_code', function(){
		if (!$('.btnBranchCreate').hasClass('hide')){
			$('.btnBranchCreate, .available, .unavailable').addClass('hide');
		}
	});

	$('.content').on('click','.checkBranchAvailability',function(){
		var subscriber_id = $('#org_code').val();
		if(subscriber_id == ''){
			alert('Branch ID must be present');
			return false;
		}

		$.ajax({
			url: '{{route('console.corporate.checkBranchId')}}',
			type: 'POST',
			data: { subscriberId: subscriber_id ,_token: '{{ csrf_token() }}'},
			success: function (state){
				if(state == "Available"){
					$('.available').removeClass('hide');
					$('.unavailable').addClass('hide');
					$('.btnBranchCreate').removeClass('hide');
				}
				if(state == "Unavailable"){
					$('.available').addClass('hide');
					$('.unavailable').removeClass('hide');
					$('.btnBranchCreate').addClass('hide');
				}
			},
			error: function(e){
				console.log(e);
			}
		});
	});

	$('#branchSaveForm').submit(function() {
	       $(".loader").html(`<div class="page-loader-wrapper">
	       	<div class="loader">
	       	<div class="md-preloader pl-size-md">
	       	<svg viewbox="0 0 75 75">
	       	<circle cx="37.5" cy="37.5" r="33.5" class="pl-red" stroke-width="4" />
	       	</svg>
	       	</div>
	       	<p>Please wait...</p>
	       	</div>
	       	</div>`);
	});
</script>
@endsection
