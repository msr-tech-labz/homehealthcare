@extends('layouts.console-layout')

@section('page_title','Subscribers - ')

@section('active_subscribers','active')

@section('plugin.styles')
<link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />
@endsection

@section('page.styles')
<style>
    .plus:after{
		content: ' + ';
		font-size: 18px;
		font-weight: bold;
	}
    .dataTables_filter{
        float: right;
    }
    tbody{
        white-space: nowrap;
    }
    .paginate_button, .paginate_button a{
        cursor: pointer;
    }
</style>
@endsection

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <h2 class="col-sm-9">
                    Corporates
                    <small>List of all Corporate Offices</small>
                </h2>
                <div class="col-sm-3">
                    <a href="{{ route('console.subscribers.create',['type' => 'corporate']) }}" class="btn btn-primary work-status waves-effect waves-block">Create New Corporate</a>
                </div>
            </div>
            <div class="body">
                <table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable sublist">
                    <thead>
                        <tr>
                            <th width="15%">Subscriber ID</th>
                            <th width="12%">Type</th>
                            <th width="30%">Name</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Subscribed on</th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse ($subscribers as $s)
                        <tr>
                            <td>
                                <a href="{{ route('console.subscribers.view',Helper::encryptor('encrypt',$s->id)) }}"><i class="material-icons" style="vertical-align:middle;font-size: 18px;">description</i> {{ $s->subscriber_id }}</a>
                            </td>
                            <td>{{ $s->user_type }}</td>
                            <td>{{ $s->name }}</td>
                            <td>{{ $s->email }}</td>
                            <td>{{ $s->status }}</td>
                            <td>{{ $s->created_at }}</td>
                        </tr>
                    @empty
                        <tr>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('plugin.scripts')
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.sublist').DataTable();
        } );
    </script>
@endsection
