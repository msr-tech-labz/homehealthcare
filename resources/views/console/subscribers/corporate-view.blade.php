@extends('layouts.console-layout')

@section('page_title','View Corporate |')

@section('active_subscribers','active')

@section('plugin.styles')
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section('content')

@if (count($errors) > 0)
<div class="row clearfix">
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif

<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card">
			<div class="header clearfix">
				<a href="{{ route('console.corporates') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
					<i class="material-icons">arrow_back</i>
				</a>
				<h2 class="col-sm-8">
					View Corporate
					<small>Subscriber Type - Corporate</small>
				</h2>
			</div>
			<div class="body">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active">
						<a href="#profile_tab" data-toggle="tab" aria-expanded="false">
							<i class="material-icons">location_city</i> Head Office Profile
						</a>
					</li>
					<li role="presentation" class="">
						<a href="#branches" data-toggle="tab" aria-expanded="true">
							<i class="material-icons">storage</i> Branches
						</a>
					</li>
				    <a href="{{ route('console.subscribers.viewBranchForm',['id' => \Helper::encryptor('encrypt',$corporate->id)]) }}" class="btn btn-primary work-status waves-effect waves-block" style="float: right;margin-top: 15px;">Create New Branch</a>
				</ul>
				<br>

				<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane fade active in" id="profile_tab">
						<form id="corporateSaveForm" action="{{ route('console.subscribers.updateCorporate') }}" method="POST">
							{{ csrf_field() }}
							<div class="row clearfix">
								<div class="col-sm-6 form-horizontal">
									<h2 class="card-inside-title">
										Basic Details
										<small>Organization Name, Address</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="corporate_id">Corporate ID</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="corporate_id" class="form-control col-teal" value="{{ $corporate->corporate_id }}" disabled="true" readonly="true">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="corporate_name">Corp Name</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="corporate_name" name="corporate_name" class="form-control" value="{{ $corporate->organization }}" >
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="corporate_type">Billing Type</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<select class="form-control show-tick" name="corporate_type">
														<option value="User Based" {{ $corporate->type =='User Based'?'selected':'' }}>User Based</option>
														<option value="Lump Sum" {{ $corporate->type =='Lump Sum'?'selected':'' }}>Lump Sum</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="corporate_type">Billing Expiry</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="corporate_expiry" name="corporate_expiry" class="form-control" value="{{ isset($corporate->expiry)?$corporate->expiry->format('d-m-Y'):'NA' }}" >
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="col-sm-6 form-horizontal">
									<h2 class="card-inside-title">
										Location Details
										<small>Email, Phone Number, Address</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="corporate_email">Email</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="email" id="corporate_email" name="corporate_email" class="form-control" value="{{ $corpProfile->email }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="corporate_address">Address</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="corporate_address" name="corporate_address" class="form-control" value="{{ $corpProfile->address }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="corporate_state">State</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select class="form-control show-tick" id="corporate_state" name="corporate_state" data-live-search="true">
													<option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
													<option value="Andhra Pradesh">Andhra Pradesh</option>
													<option value="Arunachal Pradesh">Arunachal Pradesh</option>
													<option value="Assam">Assam</option>
													<option value="Bihar">Bihar</option>
													<option value="Chandigarh">Chandigarh</option>
													<option value="Chhattisgarh">Chhattisgarh</option>
													<option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
													<option value="Daman and Diu">Daman and Diu</option>
													<option value="Delhi">Delhi</option>
													<option value="Goa">Goa</option>
													<option value="Gujarat">Gujarat</option>
													<option value="Haryana">Haryana</option>
													<option value="Himachal Pradesh">Himachal Pradesh</option>
													<option value="Jammu and Kashmir">Jammu and Kashmir</option>
													<option value="Jharkhand">Jharkhand</option>
													<option value="Karnataka">Karnataka</option>
													<option value="Kerala">Kerala</option>
													<option value="Lakshadweep">Lakshadweep</option>
													<option value="Madhya Pradesh">Madhya Pradesh</option>
													<option value="Maharashtra">Maharashtra</option>
													<option value="Manipur">Manipur</option>
													<option value="Meghalaya">Meghalaya</option>
													<option value="Mizoram">Mizoram</option>
													<option value="Nagaland">Nagaland</option>
													<option value="Odisha">Odisha</option>
													<option value="Pondicherry">Pondicherry</option>
													<option value="Punjab">Punjab</option>
													<option value="Rajasthan">Rajasthan</option>
													<option value="Sikkim">Sikkim</option>
													<option value="Tamil Nadu">Tamil Nadu</option>
													<option value="Telangana">Telangana</option>
													<option value="Tripura">Tripura</option>
													<option value="Uttaranchal">Uttaranchal</option>
													<option value="Uttar Pradesh">Uttar Pradesh</option>
													<option value="West Bengal">West Bengal</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="corporate_landline">Landline</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="corporate_landline" name="corporate_landline" class="form-control" value="{{ $corpProfile->landline_number }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="corporate_status">Status</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<select class="form-control show-tick" name="corporate_status" required>
														<option value="Active" {{ $corporate->status =='Active'?'selected':'' }} >Active</option>
														<option value="Inactive" {{ $corporate->status =='Inactive'?'selected':'' }}>Inactive</option>
													</select>
												</div>
											</div>
										</div>
									</div>

									<button type="submit" class="btn btn-success btn-lg waves-effect" style="float: right;">Update</button>
									<input type="hidden" name="corp_id" value="{{ Helper::encryptor('encrypt',$corporate->id) }}">
								</div>
							</div>
						</form>
					</div>

					<div role="tabpanel" class="tab-pane fade in" id="branches">
						<div class="row clearfix">
							<div class="col-md-12">
								<h2 class="card-inside-title">
									Associated Branches
									<small>Branch List</small>
								</h2>
								<table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable branchlist">
									<thead>
										<tr>
											<th>#</th>
											<th>Branch Code</th>
											<th>Branch City</th>
											<th>Branch State</th>
											<th>Users Limit</th>
											<th>Charge per User</th>
											<th>Total Users</th>
											<th>Active Users</th>
											<th>Status</th>
											<th>Edit</th>
										</tr>
									</thead>
									<tbody>
										@forelse ($assoBranches as $index => $branch)
										<tr>
											<td>{{ $index+1 }}</td>
											<td>{{ $branch->org_code }}</td>
											<td>{{ $branch->city }}</td>
											<td>{{ $branch->state }}</td>
											<td>{{ $branch->users_limit }}</td>
											<td>{{ $branch->charge_per_user }}</td>
											<td>{{ $branch->total_users }}</td>
											<td>{{ $branch->active_users }}</td>
											<td>
												<div class="switch">
													<label><input id="branch_{{ Helper::encryptor('encrypt',$branch->id) }}" type="checkbox" name="branch_status" data-status="{{ $branch->status }}" data-id="{{ Helper::encryptor('encrypt',$branch->id) }}" {{ $branch->status=="Active"?'checked':'' }}><span class="lever"></span></label>
												</div>
											</td>
											<td>
												<button type="button" data-id="{{ Helper::encryptor('encrypt',$branch->id) }}" data-branchcity="{{ $branch->city }}" data-userslimit="{{ $branch->users_limit }}" data-chargeperuser="{{ $branch->charge_per_user }}" data-url="{{ route('console.subscribers.branchUpdate', ['branch_id' => Helper::encryptor('encrypt',$branch->id)]) }}" class="btn btn-success btn-sm waves-effect btnEditBranch"><i class="material-icons small">edit</i></button>
											</td>
										</tr>
										@empty
										<tr>
										</tr>
										@endforelse
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Email Modal -->
<div class="modal fade" id="editBranch" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
        <form class="form-horizontal editBranchForm" method="POST" action="">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header bg-blue">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title action_branch">Edit Branch</h4>
                </div>
                <div class="modal-body">
                	<div class="row clearfix">
                		<div class="col-lg-5 form-control-label">
                			<label for="users_limit">Users Limit</label>
                		</div>
                		<div class="col-lg-7">
                			<div class="form-group">
                				<div class="form-line">
                					<input type="number" id="users_limit" name="users_limit" class="form-control" >
                				</div>
                			</div>
                		</div>
                	</div>
                	<div class="row clearfix">
                		<div class="col-lg-5 form-control-label">
                			<label for="charge_per_user">Charge/User</label>
                		</div>
                		<div class="col-lg-7">
                			<div class="form-group">
                				<div class="form-line">
                					<input type="number" id="charge_per_user" name="charge_per_user" class="form-control">
                				</div>
                			</div>
                		</div>
                	</div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success waves-effect">Save</button>
                    <button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Cancel</button>
                </div>
			</div>
        </form>
	</div>
</div>

@endsection

@section('plugin.scripts')
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('bootstrap-notify/bootstrap-notify.js') }}"></script>
<script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
@endsection

@section('page.scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
    $(function(){
        $('#corporate_state').val('{{ $corpProfile->state }}').change();

        $("#corporate_expiry").flatpickr({
        	minDate: "today",
        	dateFormat: "d-m-Y",
        	mode: "single",
        	onReady: function ( dateObj, dateStr, instance ) {
        		const $clear = $( '<div class="flatpickr-clear"><button class="btn btn-info flatpickr-clear-button">Clear</button></div>' )
        		.on( 'click', () => {
        			$("#corporate_expiry").val('NA');
        			instance.close();
        		})
        		.appendTo( $( instance.calendarContainer ) );
        	}
        });

        $('.content').on('change','input[name=branch_status]', function(){
        	if(!confirm("Do you really want to change the branch status ?")){
        		$(this).prop("checked", $(this).data('status')=='Active'?'checked':'');
        		return false;
        	}
            var branch_id = $(this).data('id');
            var status = $(this).is(':checked')?'Active':'Inactive';
            $.ajax({
                url: '{{ route('console.update-branch-status')}}',
                type: 'POST',
                data: {branchID: branch_id, status: status, _token: '{{ csrf_token() }}'},
                success: function (state){
                    if(state == "statusChanged"){
                    	$('#branch_'+branch_id).data('status',status);
		                showNotification('bg-green', 'Status changed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
		            }
                },
                error: function (error){
                    showNotification('bg-red', 'Something went Wrong !', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                }
            });
        });

        $('.table').on('click', '.btnEditBranch', function(){
        	id = $(this).data('id');
        	branchcity = $(this).data('branchcity');
        	userslimit = $(this).data('userslimit');
        	chargeperuser = $(this).data('chargeperuser');

        	$('.action_branch').html('Edit Branch - '+branchcity);
        	$('.editBranchForm').attr('action',$(this).data('url'));

        	$('.editBranchForm input[name=users_limit]').val(userslimit);
        	$('.editBranchForm input[name=charge_per_user]').val(chargeperuser);

        	$('#editBranch').modal('show');
        });

        $('#editBranch').on('hidden.bs.modal', function (e) {
            $('.action_branch').html('Edit Branch');
            $('.editBranchForm').attr('action','');
            $('.editBranchForm input[name=users_limit]').val('');
            $('.editBranchForm input[name=charge_per_user]').val('');

        });
    });
    $(document).ready(function() {
        $('.branchlist').DataTable({
            dom: 'Bflrtip',
            order: [[ 1, "asc" ]]
        });
    } );
</script>
@endsection
