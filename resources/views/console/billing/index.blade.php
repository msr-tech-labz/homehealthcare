@extends('layouts.console-layout')

@section('page_title','Billing - ')

@section('active_billing','active')

@section('page.styles')
    <style>
    .expandClass{
        margin: 0 !important;
        margin-top: 6% !important;
    }
    p{
        margin-top: 5px !important;
    }
    </style>
@endsection

@section('plugin.styles')
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet"/>
<link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
                <button type="button" class="btn btn-info waves-effect expandPage"><i class="material-icons">apps</i></button>
            <div class="header clearfix">
                <h2 class="col-sm-9">
                    Billings and Payments
                    <small>Smart Health Connect Software Billing History</small>
                </h2>
                <button class="btn btn-info waves-effect payment" style="float: right;" data-toggle="modal" data-target="#addPayment">New Invoice</button>
            </div>
            <br>
            <div class="clearfix">
                <form action="{{ route('console.billing') }}" method="post">
                    {{ csrf_field() }}
                   <div class="col-lg-3">
                    <div class="form-group">
                        <div class="form-line">
                            <select class="form-control show-tick" name="corporate_office" id="corporate_office" required>
                                <option value>-- Select Corporate --</option>
                                @foreach($corporates as $corporate)
                                <option value="{{$corporate->id}}">{{ $corporate->organization }} - {{ $corporate->type }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                   </div>
                   <div class="col-lg-3">
                    <div class="form-group">
                        <div class="form-line">
                            <select class="form-control show-tick" name="corporate_branch" id="corporate_branch" required disabled>
                                <option value>-- Select Corporate First --</option>
                            </select>
                        </div>
                    </div>
                   </div>
                    <div class="col-md-3">
                        <input type="text" class="form-control flatpickr" id="usage_duration" name="usage_duration" placeholder="Usage Duration">
                    </div>
                    <button type="button" class="btn btn-success btn-md waves-effect" id="getScheduledStaffCount">Check Scheduled Staffs</button>
                </form>
            </div>
            <div class="body">
                <table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable">
                    <thead>
                        <tr>
                            <th>Invoice no</th>
                            <th>Orgname</th>
                            <th>Branch</th>
                            <th>Month/Year</th>
                            <th>Duration</th>
                            <th>Total Amount</th>
                            <th>Amount Received</th>
                            <th>Status</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($subInvs as $index => $subInv)
                            <tr style="background-color: {{ $subInv->status }}">
                                <td>{{ $subInv->invoice_no }}</td>
                                <td>{{ $subInv->organization->organization }}</td>
                                <td>{{ $subInv->subcriber->org_code }}</td>
                                <td>{{ $subInv->invoice_month.' - '.$subInv->invoice_year }}</td>
                                <td>{{ $subInv->usage_duration }}</td>
                                <td>{{ $subInv->invoice_amount }}</td>
                                <td>{{ $subInv->amount_paid }}</td>
                                <td>{{ $subInv->getAttributes()['status'] }}</td>
                                <td>{{ $subInv->created_at->format('d-m-Y') }}</td>
                                <td>{{ $subInv->updated_at->format('d-m-Y') }}</td>
                                <td>
                                    <button type="button" data-organization="{{ $subInv->organization->organization }}" data-orgCode="{{ $subInv->subcriber->org_code }}" data-monthYear="{{ $subInv->invoice_month.' - '.$subInv->invoice_year }}" data-usageDuration="{{ $subInv->usage_duration }}" data-invoiceNo="{{ $subInv->invoice_no }}" data-invoiceAmount="{{ $subInv->invoice_amount }}" data-amountPaid="{{ $subInv->amount_paid }}" data-status="{{ $subInv->getOriginal('status') }}" data-url="{{ route('console.subscribers.updateInvoice', ['id' => Helper::encryptor('encrypt',$subInv->id)]) }}" class="btn btn-success btn-sm waves-effect btnEditPayment"><i class="material-icons small">edit</i></button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Add Payment Modal -->
<div class="modal fade" id="addPayment" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <form class="form-horizontal addPaymentForm" method="POST" action="{{ route('console.subscribers.addInvoice') }}">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header bg-blue">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title action_email">New Invoice</h4>
                </div>
                <div class="modal-body">
                    <div class="row clearfix">
                        <div class="col-sm-12 form-horizontal">
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="corporate_office_new">Corporate</label>
                                </div>
                                <div class="col-lg-9 col-md-9">
                                 <div class="form-group">
                                     <div class="form-line">
                                         <select class="form-control show-tick" name="corporate_office_new" id="corporate_office_new" required>
                                             <option value>-- Select Corporate --</option>
                                             @foreach($corporates as $corporate)
                                             <option value="{{$corporate->id}}">{{ $corporate->organization }} - {{ $corporate->type }}</option>
                                             @endforeach
                                         </select>
                                     </div>
                                 </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="corporate_branch_new">Branch</label>
                                </div>
                                <div class="col-lg-9 col-md-9">
                                    <div class="form-group">
                                        <select class="form-control show-tick" name="corporate_branch_new" id="corporate_branch_new" required disabled>
                                            <option value>-- Select Corporate First --</option>
                                        </select>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="usage_duration_new">Usage Duration</label>
                                </div>
                                <div class="col-lg-9 col-md-9">
                                        <input type="text" class="form-control flatpickr" id="usage_duration_new" name="usage_duration_new" placeholder="Usage Duration">
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="invoice_no_new">Invoice Number</label>
                                </div>
                                <div class="col-lg-9 col-md-9">
                                        <input type="text" class="form-control" id="invoice_no_new" name="invoice_no_new" placeholder="Last Invoice Raised : {{ $subInvs->first()['invoice_no'] ?? 'None' }}">
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="invoice_month_new">Invoice Month</label>
                                </div>
                                <div class="col-lg-9 col-md-9">
                                    <div class="form-group">
                                        <select class="form-control show-tick" name="invoice_month_new" id="invoice_month_new" required>
                                            <option value="January">January</option>
                                            <option value="February">February</option>
                                            <option value="March">March</option>
                                            <option value="April">April</option>
                                            <option value="May">May</option>
                                            <option value="June">June</option>
                                            <option value="July">July</option>
                                            <option value="August">August</option>
                                            <option value="September">September</option>
                                            <option value="October">October</option>
                                            <option value="November">November</option>
                                            <option value="December">December</option>
                                        </select>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="invoice_year">Invoice Year</label>
                                </div>
                                <div class="col-lg-9 col-md-9">
                                    <div class="form-group">
                                        <select class="form-control show-tick" name="invoice_year" id="invoice_year" required>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                        </select>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="invoice_amount_new">Invoice Amount</label>
                                </div>
                                <div class="col-lg-9 col-md-9">
                                    <div class="form-group">
                                        <div class="form-line">
                                        <input type="number" min="0" name="invoice_amount_new" id="invoice_amount_new" class="form-control" placeholder="50000" required="required">
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success waves-effect">Save</button>
                    <button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Edit Payment Modal -->
<div class="modal fade" id="editPayment" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <form class="form-horizontal editPaymentForm" method="POST" action="">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header bg-blue">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title action_editPayment">Edit Invoice</h4>
                </div>
                <div class="modal-body">
                    <div class="row clearfix">
                        <div class="col-sm-12 form-horizontal">
                            <div class="row clearfix">
                                <div class="col-lg-4 form-control-label">
                                    <label for="corporate_office_edit">Corporate</label>
                                </div>
                                <div class="col-lg-8">
                                    <p id="corporate_office_edit"></p>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-4 form-control-label">
                                    <label for="corporate_branch_edit">Branch</label>
                                </div>
                                <div class="col-lg-8">
                                    <p id="corporate_branch_edit"></p>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-4 form-control-label">
                                    <label for="usage_duration_edit">Usage Duration</label>
                                </div>
                                <div class="col-lg-8">
                                    <p id="usage_duration_edit"></p>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-4 form-control-label">
                                    <label for="invoice_monthYear_edit">Invoice Month-Year</label>
                                </div>
                                <div class="col-lg-8">
                                    <p id="invoice_monthYear_edit"></p>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-4 form-control-label">
                                    <label for="invoice_amount_edit">Invoice Amount</label>
                                </div>
                                <div class="col-lg-8">
                                    <p id="invoice_amount_edit"></p>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-4 form-control-label">
                                    <label for="invoice_amount_received">Amount Received</label>
                                </div>
                                <div class="col-lg-8">
                                    <p id="invoice_amount_received"></p>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-4 form-control-label">
                                    <label for="invoice_amount_received_update">Update Amount</label>
                                </div>
                                <div class="col-lg-8">
                                    <input class="form-control" type="number" min="0.00" max="" name="invoice_amount_received_update" id="invoice_amount_received_update">
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-4 form-control-label">
                                    <label for="status">Invoice Amount</label>
                                </div>
                                <div class="col-lg-8">
                                    <select class="form-control show-tick" name="status" id="status" required>
                                        <option value="Pending">Pending</option>
                                        <option value="Partial">Partial</option>
                                        <option value="Paid">Paid</option>
                                    </select>
                                </div>
                            </div><br>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success waves-effect">Update</button>
                    <button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection
@section('page.scripts')
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script>
    $(function(){
        if(localStorage.getItem('expandMode') == 'true'){
            $('.content').toggleClass('expandClass');
            $('.sidebar').toggleClass('hide');
        }

        $(".flatpickr").flatpickr({
            dateFormat: "d-m-Y",
            mode: "range",
            locale: { rangeSeparator: ' - ' }
        });

        var branchList = [];
        @if(isset($branches))
        @foreach ($branches as $branch)
        branchList.push({id: '{{ $branch->id }}', key: '{{ $branch->corp_id }}', value:'{{ $branch->org_code }}', rpu:'{{ $branch->charge_per_user }}'});
        @endforeach
        @endif

        $('.content').on('change','#corporate_office', function(){
            var val = $(this).val();
            var branchesHtml = '<option value="">-- Select Branch --</option>';
            var branches = branchList.filter(function(item){ return item.key === val; });
            if(branches.length > 0){
                for(key in branches){
                    if(branches.hasOwnProperty(key)){
                        branchesHtml += '<option value="'+branches[key].id+'">'+branches[key].value+' @ '+branches[key].rpu+'</option>';
                    }
                }
                $('#corporate_branch').prop('disabled',false);
            }else{
                $('#corporate_branch').prop('disabled',true);
            }

            $('#corporate_branch').html(branchesHtml);
            $('#corporate_branch').selectpicker('refresh');
        });

        $('.content').on('change','#corporate_office_new', function(){
            var val = $(this).val();
            var branchesHtml = '<option value="">-- Select Branch --</option>';
            var branches = branchList.filter(function(item){ return item.key === val; });
            if(branches.length > 0){
                for(key in branches){
                    if(branches.hasOwnProperty(key)){
                        branchesHtml += '<option value="'+branches[key].id+'">'+branches[key].value+' @ '+branches[key].rpu+'</option>';
                    }
                }
                $('#corporate_branch_new').prop('disabled',false);
            }else{
                $('#corporate_branch_new').prop('disabled',true);
            }

            $('#corporate_branch_new').html(branchesHtml);
            $('#corporate_branch_new').selectpicker('refresh');
        });
    });

    $('.dataTable').DataTable({
        "order": [[ 0, 'asc' ]],
        "displayLength": 25,
    });

    $('.content').on('click','#getScheduledStaffCount',function(){
        var corpID = $('#corporate_office').val();
        var branchID = $('#corporate_branch').val();
        var range = $('#usage_duration').val();
        if(corpID == '' || branchID == '' || range == ''){
            alert('Please select all the fields');
            return false;
        }else{
            $.ajax({
                url: '{{route('console.subscribers.getScheduledStaffCount')}}',
                type: 'POST',
                data: { branchID: branchID, range: range, _token: '{{ csrf_token() }}'},
                success: function (count){
                    if(count != 0)
                        alert('You need to bill for '+count+' user(s) for the duration '+range+' for the selected Branch.');
                    else
                        alert('No users were scheduled for the duration '+range+' for the selected Branch.');
                },
                error: function(e){
                    console.log(e);
                }
            });
        }
    })

    $('.content').on('click','.expandPage',function(){
        $('.content').toggleClass('expandClass');
        $('.sidebar').toggleClass('hide');
        if( $('.content').hasClass('expandClass') && $('.sidebar').hasClass('hide') ){
            localStorage.setItem('expandMode', true);
        }else{
            localStorage.removeItem('expandMode');
        }
    });

    $('.table').on('click', '.btnEditPayment', function(){
        organization = $(this).data('organization');
        orgCode = $(this).data('orgcode');
        monthYear = $(this).data('monthyear');
        usageDuration = $(this).data('usageduration');
        invoiceNo = $(this).data('invoiceno');
        invoiceAmount = $(this).data('invoiceamount');
        amountPaid = $(this).data('amountpaid');
        status = $(this).data('status');

        $('.action_editPayment').html('Update Invoice - '+invoiceNo);
        $('.editPaymentForm').attr('action',$(this).data('url'));
        $('.editPaymentForm input[name=_token]').val('{{ csrf_token() }}');

        $('.editPaymentForm #corporate_office_edit').html(organization);
        $('.editPaymentForm #corporate_branch_edit').html(orgCode);
        $('.editPaymentForm #usage_duration_edit').html(monthYear);
        $('.editPaymentForm #invoice_monthYear_edit').html(usageDuration);
        $('.editPaymentForm #invoice_amount_edit').html(invoiceAmount);
        $('.editPaymentForm #invoice_amount_received').html(amountPaid);
        if(invoiceAmount == amountPaid){
            $('.editPaymentForm #invoice_amount_received_update').attr('disabled',true);
            $('.editPaymentForm #invoice_amount_received_update').prop('placeholder','');
        }else{
            $('.editPaymentForm #invoice_amount_received_update').attr('disabled',false);
            $('.editPaymentForm #invoice_amount_received_update').prop('max',invoiceAmount-amountPaid);
            $('.editPaymentForm #invoice_amount_received_update').prop('placeholder','Remaining amount : Rs '+(invoiceAmount-amountPaid));
        }
        $('.editPaymentForm select[name=status]').selectpicker('val',status);


        $('#editPayment').modal('show');
    });
</script>
@endsection