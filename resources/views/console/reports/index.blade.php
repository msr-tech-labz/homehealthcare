@extends('layouts.console-layout')

@section('page_title','Reports - ')

@section('active_reports','active')

@section('page.styles')
<style>
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
</style>
@endsection
@section('plugin.styles')
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />
@endsection
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <h2 class="col-sm-9">
                    Providers Credit History
                    <small>List of all Credit Purchase History</small>
                </h2>
            </div>

            <div class="clearfix">
                <form action="{{ route('console.reports') }}" method="post">
                    {{ csrf_field() }}
                    <table class="table table-bordered">
                        <tr>
                            <th>
                                <select class="form-control" name="filter_status">
                                    <option value="">-- Any --</option>
                                    <option value="Pending">PENDING</option>
                                    <option value="SUCCESS">SUCCESS</option>
                                    <option value="FAILED">FAILED</option>
                                </select>
                            </th>
                            <th>
                                <div class="col-md-6">
                                <input type="text" class="form-control" id="min" name="start_date" placeholder="From Date">
                                </div>
                                <div class="col-md-6">
                                <input type="text" class="form-control" id="max" name="end_date" placeholder="To Date">
                                </div>
                            </th>
                            <th>
                                <input type="submit" class="btn btn-success">
                            </th>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="body">
                <table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Date</th>
                            <th>OrgCode</th>
                            <th>Provider</th>
                            <th>Credits</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($paymentsList as $i => $p)
                        <tr>
                            <td>{{ $i + 1 }}</td>
                            <td>{{ \Carbon\Carbon::parse($p->transaction_date)->format('d-m-Y') }}</td>
                            <td>{{ $p->orgCode }}</td>
                            <td>{{ $p->provider }}</td>
                            <td>{{ $p->credits }}</td>
                            <td>{{ $p->status }}</td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="6" class="text-center">
                                No provider record(s) found.
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page.scripts')
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<script>
if('{{ count($paymentsList) > 0 }}'){
    var table = $('.dataTable').DataTable({
        "order": [[ 0, 'asc' ]],
        "displayLength": 25,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;

            api.column(3, {page:'current'} ).data().each( function ( group, i ) {
                if(group != null){
                    if ( last !== group.substring(0, 10) ) {
                        $(rows).eq( i ).before(
                            '<tr class="group"><td colspan="10"><b>'+group+'</b></td></tr>'
                            );

                        last = group.substring(0, 10);
                    }
                }
            } );
        }
    });
}

$(function(){
    $('#max').bootstrapMaterialDatePicker({
        format: 'DD-MM-YYYY',
        clearButton: true,
        time: false,
        maxDate : new Date(),
    });

    $('#min').bootstrapMaterialDatePicker({
        format: 'DD-MM-YYYY',
        clearButton: true,
        time: false,
        maxDate : new Date(),
    }).on('change', function(e, date){
        $('#max').bootstrapMaterialDatePicker('setMinDate', date);
    });
});
</script>
@endsection