@extends('layouts.main-layout')

@section('page_title','New Case - Cases |')

@section('active_cases','active')

@section('plugin.styles')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<!-- Bootstrap Tagsinput Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://clin-table-search.lhc.nlm.nih.gov/autocomplete-lhc-versions/13.0.2/autocomplete-lhc_jQueryUI.min.css"/>
@endsection

@section('page.styles')
<style>
	.autocomp_selected {
		width: 100%;
	}
	.card .header .header-dropdown{
		top: 15px;
	}
	hr{
		margin-top: 10px;
		margin-bottom: 10px;
	}
	.bootstrap-tagsinput{
		min-height: 40px;
		vertical-align: top;
		padding: 4px 0;
		width: 100%;
	}
	.bootstrap-tagsinput input{
		padding-left: 0;
	}
	.plus:after{
		content: ' + ';
		font-size: 18px;
		font-weight: bold;
	}
	.care-plan-card .header{
		padding: 15px 20px;
	}
	.modal-lg{
		width: 70%;
	}
	.modal .modal-header{
		padding: 20px 25px 15px 25px;
	}
	.modal-footer{
		border-top: 1px solid #ddd !important;
	}

	/* Map Styles */
	#infowindow-content .title {
		font-weight: bold;
	}

	#infowindow-content {
		display: none;
	}

	#map #infowindow-content {
		display: inline;
	}

	.pac-card {
		margin: 10px 10px 0 0;
		border-radius: 2px 0 0 2px;
		box-sizing: border-box;
		-moz-box-sizing: border-box;
		outline: none;
		box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
		background-color: #fff;
		font-family: Roboto;
	}

	#pac-container {
		padding-bottom: 12px;
		margin-right: 12px;
	}

	.pac-controls {
		display: inline-block;
		padding: 5px 11px;
	}

	.pac-controls label {
		font-family: Roboto;
		font-size: 13px;
		font-weight: 300;
	}

	#pac-input {
		background-color: #fff;
		font-family: Roboto;
		font-size: 15px;
		font-weight: 300;
		margin-left: 12px;
		padding: 0 11px 0 13px;
		text-overflow: ellipsis;
		width: 400px;
	}

	#pac-input:focus {
		border-color: #4d90fe;
	}

	#title {
		color: #fff;
		background-color: #4d90fe;
		font-size: 20px;
		font-weight: 500;
		padding: 6px 12px;
	}
	.dataTables_filter{
		float: right !important;
	}
	.dataTables_wrapper {
		margin-bottom: -40px !important;
	}
	td {
		font-size: 12px !important;
	}
</style>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="row clearfix">
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif

<div class="row">
	<form id="caseForm" name="caseForm">
		{{ csrf_field() }}
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<a href="{{ route('lead.index') }}" class="pull-left btn btn-info clearLocalStore btn-circle waves-effect waves-circle waves-float">
						<i class="material-icons">arrow_back</i>
					</a>
					<h2 class="col-sm-3">
						{{ isset($e)?'Edit':'New'}} Case
						<small>Case {{ isset($e)?'Updation':'Creation'}} Form</small>
					</h2>

					@if(isset($l) && $l->source == 'Exotel')
					<div class="col-sm-6" style="width:auto;border:1px solid #eee;background:#f8f8f8">
						<span style="line-height: 38px;vertical-align:top;color:#666;font-weight:500;padding-right:10px;border-right:1px solid #ccc;">Recording</span>
						<audio style="opacity:.85" controls controlsList="nodownload" preload="metadata" src="https://s3-ap-southeast-1.amazonaws.com/exotelrecordings/apnacare/{{ $l->exotel_sid }}.mp3">
							<source src="" type="audio/mpeg">
						</audio>
					</div>
					@endif
					<ul class="header-dropdown m-r--5">
						<div class="btn-group" role="group">
							<button type="button" class="btn btn-success m-l-15 waves-effect"><i class="material-icons" style="color: #fff">save</i></button>
							<button type="button" onclick="javascript: return validator();" class="btn btn-success waves-effect btnSaveAggLead" style="line-height: 1.9;padding-left:0">Save Case</button>
						</div>&nbsp;&nbsp;
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="body" style="padding-top: 0">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active">
							<a href="#patient_details_tab" data-toggle="tab" aria-expanded="false">
								<i class="material-icons">person</i> Patient &amp; Enquirer Details
							</a>
						</li>
						<li role="presentation" class="">
							<a href="#care_plan_tab" data-toggle="tab" aria-expanded="true">
								<i class="material-icons">child_friendly</i> Case Details
							</a>
						</li>
						<li role="presentation" class="">
							<a href="#provider_tab" data-toggle="tab" aria-expanded="true">
								<i class="material-icons">business</i> Provider Details
							</a>
						</li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content tab-menu">
						<div role="tabpanel" class="tab-pane fade active in" id="patient_details_tab">
							<div class="row clearfix">
								<div class="col-sm-6 form-horizontal">
									<h2 class="card-inside-title">
										Patient Details
										<small>Name,Age</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="first_name">First Name</label>
										</div>
										<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="first_name" name="first_name" class="form-control searchCol" placeholder="First Name" value="{{isset($l->patient)?$l->patient->first_name:''}}" required>
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="last_name" name="last_name" class="form-control searchCol" placeholder="Last Name" value="{{isset($l->patient)?$l->patient->last_name:''}}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="gender">Gender</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select class="form-control show-tick" id="gender" name="gender" required>
													<option value="">-- Please select--</option>
													<option value="Male" {{ (isset($l->patient) && $l->patient->gender == 'Male')?'selected':'' }}>Male</option>
													<option value="Female" {{ (isset($l->patient) && $l->patient->gender == 'Female')?'selected':'' }}>Female</option>
													<option value="Other" {{ (isset($l->patient) && $l->patient->gender == 'Other')?'selected':'' }}>Other</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="patient_age">Patient Age</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="patient_age" name="patient_age" class="form-control" placeholder="Patient Age"value="{{ isset($l->patient->patient_age)?$l->patient->patient_age:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="patient_weight">Patient Weight</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="patient_weight" name="patient_weight" class="form-control" placeholder="Patient Weight"value="{{ isset($l->patient->patient_weight)?$l->patient->patient_weight:'' }}">
												</div>
											</div>
										</div>
									</div>
									<h2 class="card-inside-title">
										Patient Address
										<small>Address / Area, City and State</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="street_address">Flat / Building</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="street_address" name="street_address" class="form-control" placeholder="Street Address" value="{{ isset($l->patient)?$l->patient->street_address:'' }}" autocomplete="off"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="area">Area</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="area" name="area" class="form-control" placeholder="area / Area" value="{{ isset($l->patient)?$l->patient->area:'' }}" autocomplete="off"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="city">City</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="city" name="city" class="form-control" placeholder="City" value="{{ isset($l->patient)?$l->patient->city:(isset($l->city)?$l->city:'') }}" autocomplete="off"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="zipcode">Zip Code</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="zipcode" name="zipcode" class="form-control" placeholder="Zip Code" value="{{ isset($l->patient)?$l->patient->zipcode:'' }}" autocomplete="off"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="state">State</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="state" name="state" class="form-control" value="{{ isset($l->patient)?$l->patient->state:'' }}" placeholder="State" autocomplete="off"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="country">Country</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="country" name="country" class="form-control" value="{{ isset($l->patient)?$l->patient->country:'' }}" placeholder="Country" autocomplete="off"/>
												</div>
											</div>
										</div>
									</div>

									<h2 class="card-inside-title">
										Location Co-ordinates
										<small>Lattitude and Longitude</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="latitude">Latitude</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="latitude" name="latitude" class="form-control" placeholder="Latitude" value="{{ isset($l->patient)?$l->patient->latitude:'' }}"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="longitude">Longitude</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="longitude" name="longitude" class="form-control" placeholder="Longitude" value="{{ isset($l->patient)?$l->patient->longitude:'' }}"/>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="col-sm-6 form-horizontal">
									<h2 class="card-inside-title">
										Enquirer Details
										<small>Enquirer Name, Phone, Email, Relationship</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="enquirer_name">Enquirer Name</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="enquirer_name" name="enquirer_name" class="form-control" placeholder="Enquirer Name" value="{{ isset($l->patient)?$l->patient->enquirer_name:(isset($l->name)?$l->name:'') }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="contact_number">Enquirer Phone</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="number" id="contact_number" name="contact_number" class="form-control" placeholder="Enquirer Phone" value="{{ isset($l->patient)?$l->patient->contact_number:(isset($l->phone_number)?$l->phone_number:'') }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="alternate_number">Alternate Number</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="number" id="alternate_number" name="alternate_number" class="form-control searchCol" placeholder="Alternate Number" value="{{ isset($l->patient)?$l->patient->alternate_number:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="email">Enquirer Email</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="email" id="email" name="email" class="form-control" placeholder="Enquirer Email" value="{{ isset($l->patient)?$l->patient->email:(isset($l->email)?$l->email:'') }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="relationship_with_patient">Relationship with Patient</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select id="relationship_with_patient" name="relationship_with_patient" class="form-control show-tick" data-live-search="true" placeholder="Relationship with Patient">
													<option value=""> -- Select Relationship -- </option>
													<option value="BROTHER">BROTHER</option>
													<option value="BROTHER-IN-LAW">BROTHER-IN-LAW</option>
													<option value="DAUGHTER">DAUGHTER</option>
													<option value="FATHER">FATHER</option>
													<option value="FATHER-IN-LAW">FATHER-IN-LAW</option>
													<option value="FRIEND">FRIEND</option>
													<option value="GRANDFATHER">GRANDFATHER</option>
													<option value="GRANDMOTHER">GRANDMOTHER</option>
													<option value="HUSBAND">HUSBAND</option>
													<option value="MOTHER">MOTHER</option>
													<option value="MOTHER-IN-LAW">MOTHER-IN-LAW</option>
													<option value="SELF">SELF</option>
													<option value="SISTER">SISTER</option>
													<option value="SON">SON</option>
													<option value="WIFE">WIFE</option>
													<option value="OTHER">OTHER</option>
												</select>
											</div>
										</div>
									</div>

									<h2 class="card-inside-title">
										Location Map
									</h2>
									<div class="pac-card" id="pac-card">
										<div>
											<div id="title">
												Location search
											</div>
											<div id="type-selector" class="pac-controls">
												<input type="radio" name="type" id="changetype-all" checked="checked">
												<label for="changetype-all">All</label>

												<input type="radio" name="type" id="changetype-establishment">
												<label for="changetype-establishment">Establishments</label>

												<input type="radio" name="type" id="changetype-address">
												<label for="changetype-address">Addresses</label>

												<input type="radio" name="type" id="changetype-geocode">
												<label for="changetype-geocode">Geocodes</label>
											</div>
											<div id="strict-bounds-selector" class="pac-controls">
												<input type="checkbox" id="use-strict-bounds" value="">
												<label for="use-strict-bounds">Strict Bounds</label>
											</div>
										</div>
										<div id="pac-container">
											<input id="pac-input" type="text"
											placeholder="Enter a location">
										</div>
									</div>
									<div id="map" style="width: 98%; height:500px;"></div>
									<div id="infowindow-content">
										<img src="" width="16" height="16" id="place-icon">
										<span id="place-name"  class="title"></span><br>
										<span id="place-address"></span>
									</div>
								</div>
							</div>
						</div>

						<div role="tabpanel" class="tab-pane fade" id="care_plan_tab">
							<div class="row clearfix form-horizontal">
								<div class="col-sm-6">
									<h2 class="card-inside-title">
										Case Details
										<small>Medical Condition, Medication, Notes</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="case_description">Case Description</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<textarea id="case_description" name="case_description" class="form-control" placeholder="Describe the patient's condition">{{ isset($l)?$l->case_description:'' }}</textarea>
												</div>
											</div>
										</div>
									</div>
									<div style="background: #b6dbf9; padding:10px;border-radius: 4px">
										<div style="text-align: center;"><b>Medical History of Patient</b></div>
										<hr>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="medical_conditions">Medical Conditons <br>/ Current Diagnosis</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group">
														<input type="text" id="conditions" name="conditions" placeholder="Type here to search.(e.g. Abdominal Pain,Oxygen Toxicity)">
														<small>Please enter unlisted entries manually below.</small>
														<textarea id="medical_conditions" name="medical_conditions" class="form-control" >{{ isset($l)?$l->medical_conditions:'' }}</textarea>
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="medications">Medications</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group">
														<input type="text" id="medications_list" name="medications_list" placeholder="Type here to search.(e.g. Aspirin,Clotrimazole)">
														<small>Please enter unlisted entries manually below.</small>
														<textarea id="medications" name="medications" class="form-control">{{ isset($l)?$l->medications:'' }}</textarea>
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="procedures">Surgeries/Procedures</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group">
														<input type="text" id="procedures_list" name="procedures_list" placeholder="Type here to search.(e.g. Open Heart Surgery,Knee Arthoplasty)">
														<small>Please enter unlisted entries manually below.</small>
														<textarea id="procedures" name="procedures" class="form-control">{{ isset($l)?$l->surgeries:'' }}</textarea>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="designation">Service Required</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select class="form-control show-tick" data-live-search="true" id="service_id" name="service_id">
													<option value="">-- Please select --</option>
												@if(isset($services) && count($services))
													@foreach($services as $s)
													<option value="{{ $s->id }}" @if(isset($l) && $l->service_required == $s->id){ selected }@endif>{{ ucwords($s->service_name) }}</option>
													@endforeach
												@endif
												</select>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="no_of_hours">No. of hours</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="number" id="no_of_hours" name="no_of_hours" class="form-control" value="{{ isset($l)?$l->estimated_duration:'' }}" placeholder="No. of hours">
												</div>
											</div>
										</div>
									</div>

									<h2 class="card-inside-title">
										Referral Details
										<small>Referrer Name, Case Source</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="source">Source</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select class="form-control show-tick" data-live-search="true" id="source" name="source">
													<option value="">--Select--</option>
													<option value="Google Search">Google Search</option>
													<option value="Website">Website</option>
													<option value="Exotel">Exotel</option>
													<option value="Referral from Service Partner">Referral from Service Partner</option>
													<option value="Online Classifieds">Online Classifieds</option>
													<option value="Email Campaign">Email Campaign</option>
													<option value="Facebook">Facebook</option>
													<option value="Twitter">Twitter</option>
													<option value="Google Plus">Google Plus</option>
													<option value="Events">Events</option>
													<option value="Exhibitions">Exhibitions</option>
													<option value="Flyer">Flyer</option>
													<option value="Print Classifieds">Print Classifieds</option>
													<option value="Radio">Radio</option>
													<option value="Television">Television</option>
													<option value="Print Ads">Print Ads</option>
													<option value="News/Media">News/Media</option>
													<option value="Other">Other</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<h2 class="card-inside-title">
										Caregiver Preference
										<small>&nbsp;</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="gender_preference">Gender Preference</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group" style="margin-top: 1%">
												<input name="gender_preference" type="radio" id="gender_preference_any" class="with-gap radio-col-deep-purple" value="Any" checked />
												<label for="gender_preference_any">Any</label>
												<input name="gender_preference" type="radio" id="gender_preference_male" class="with-gap radio-col-deep-purple" value="Male" />
												<label for="gender_preference_male">Male</label>
												<input name="gender_preference" type="radio" id="gender_preference_female" class="with-gap radio-col-deep-purple" value="Female"/>
												<label for="gender_preference_female">Female</label>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="language_preference">Language Preference</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group" style="margin-top: 2.5%">
												<div class="demo-checkbox language-preference">
											@if(isset($languages) && count($languages))
												@foreach($languages as $ln)
													<input type="checkbox" id="languages_{{ strtolower($ln->language_name) }}" name="language_preference[]" class="filled-in chk-col-light-blue" value="{{ $ln->language_name }}" {{ (isset($l) && in_array($ln->language_name, explode(",",$l->language_preference)))?'checked':'' }}/>
													<label for="languages_{{ strtolower($ln->language_name) }}">{{ $ln->language_name }}</label>
												@endforeach
											@endif
												</div>
											</div>
										</div>
									</div>

									<h2 class="card-inside-title">
										Charges
										<small>&nbsp;</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="rate_agreed">Rate Agreed</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="number" id="rate_agreed" name="rate_agreed" class="form-control" value="@if(isset($l) && $l->aggregator_rate != null){{$l->aggregator_rate}}@elseif(isset($l) && $l->rate_agreed != null){{$l->rate_agreed}}@else '' @endif" placeholder="Rate Agreed">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="gender_preference">Negotiable</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group" style="margin-top: 1%">
												<input name="rate_negotiable" type="radio" id="negotiable_yes" class="with-gap radio-col-deep-purple" value="Yes" checked />
												<label for="negotiable_yes">Negotiable</label>
												<input name="rate_negotiable" type="radio" id="negotiable_no" class="with-gap radio-col-deep-purple" value="No" />
												<label for="negotiable_no">Non-Negotiable</label>
											</div>
										</div>
									</div>
									<input type="hidden" id="lead_id" value="{{ isset($lead)?$lead->id:0 }}">
									<input type="hidden" id="previous_provider_id" value="{{ isset($l)?\Helper::encryptor('encrypt',$l->tenant_id):'' }}">
									<input type="hidden" id="previous_lead_id" value="{{ isset($l)?\Helper::encryptor('encrypt',$l->id):'' }}">
								</div>
							</div>
							<div class="clearfix"></div>
						</div>

						<div role="tabpanel" class="tab-pane fade" id="provider_tab">
							<div class="row clearfix" style="margin-bottom:5px !important;border-bottom: 1px solid #ddd !important">
								<div class="col-lg-3 col-md-4 col-sm-5 col-xs-5 form-control-label">
									<label for="provider">Selected Provider</label>
								</div>
								<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
									<div class="form-group">
										<input type="hidden" id="provider_id" value="" />
										<input type="hidden" id="branch_id" value="" />
										<div style="margin-top: 8px;" id="selectedProviderName" data-visit-id="1">-</div>
									</div>
								</div>
							</div><br>
							<div class="col-sm-12">
								<div id="providerSearchForm" class="col-sm-12">
									<div class="col-sm-3">
										<div class="form-group">
											<label for="filter_service">Service Required</label>
											<div class="form-line">
												<select class="form-control show-tick" data-live-search="true" id="filter_service">
													<option value="0">-- Please select --</option>
												@if(isset($services) && count($services))
													@foreach($services as $s)
													<option value="{{ $s->id }}">{{ $s->service_name }}</option>
													@endforeach
												@endif
												</select>
											</div>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group">
											<label for="filter_gender">Gender Preference</label>
											<div class="form-line">
												<select class="form-control show-tick" id="filter_gender">
													<option value="0">-- Any --</option>
													<option value="Male">Male</option>
													<option value="Female">Female</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group">
											<label for="filter_city">City</label>
											<div class="form-line">
												<input class="form-control" id="filter_city" placeholder="City" value="{{ isset($l->patient)?$l->patient->city:'' }}">
											</div>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group">
											<label for="filter_area">Area</label>
											<div class="form-line">
												<input class="form-control" id="filter_area" placeholder="Area">
											</div>
										</div>
									</div>
									<div class="col-sm-3 col-sm-offset-5">
										<br>
										<input type="hidden" name="filter_latitude" />
										<input type="hidden" name="filter_longitude" />
										<a class="btn btn-primary waves-effect btnFilterProvider">Search</a>&nbsp;&nbsp;
										<button type="reset" class="btn btn-danger waves-effect btnReset">Reset</button>
									</div>
								</div>
								<div class="col-sm-12" style="overflow:auto;">
									<div class="page-loader-wrapper">
										<div class="loader">
											<div class="md-preloader pl-size-md">
												<svg viewbox="0 0 75 75">
													<circle cx="37.5" cy="37.5" r="33.5" class="pl-blue" stroke-width="4" />
												</svg>
											</div>
											<p>Please wait...</p>
										</div>
									</div>
									<table class="table table-bordered table-striped providerSearchResults">
										<thead>
											<tr>
												<th>Provider</th>
												<th>Location</th>
												<th>Contact</th>
												<th width="20%">Availability</th>
												<th width="10%" class="text-center">Staff</th>
												<th width="10%" class="text-center">Distance</th>
												<th width="10%" class="text-center">Rating</th>
												<th width="5%">Select</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="text-center" colspan="8">Click 'Search' to get results</td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div><br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection

@section('plugin.scripts')
<!-- Select Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<!-- Input Mask Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>
<!-- Moment Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<!-- Bootstrap Tags Input Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/typeahead.min.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('bootstrap-3-typeahead/bootstrap3-typeahead.js') }}"></script>

<!-- Bootstrap Notify Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-notify/bootstrap-notify.js') }}"></script>
<script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>

<script src="{{ asset('js/autocomplete.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2-azeUdXbeAqQrvGtLZrbsXIFcofE_B8&libraries=places&region=IN"></script>
<script src="https://clin-table-search.lhc.nlm.nih.gov/autocomplete-lhc-versions/13.0.2/autocomplete-lhc_jQueryUI.min.js"></script>
<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
@endsection

@section('page.scripts')
<script type="text/javascript">
	$(function(){
			var conditionsArray = [];
			new Def.Autocompleter.Search('conditions', conditionsArray, {maxSelect: '*'});
			var searchConditions = new Def.Autocompleter.Search('conditions','https://clin-table-search.lhc.nlm.nih.gov/api/conditions/v3/search');

			var medicationsArray = [];
			new Def.Autocompleter.Search('medications_list', medicationsArray, {maxSelect: '*'});
			var searchMedications = new Def.Autocompleter.Search('medications_list','https://clin-table-search.lhc.nlm.nih.gov/api/rxterms/v3/search?ef=STRENGTHS_AND_FORMS');

			var proceduresArray = [];
			new Def.Autocompleter.Search('procedures_list', proceduresArray, {maxSelect: '*'});
			var searchProcedures = new Def.Autocompleter.Search('procedures_list','https://clin-table-search.lhc.nlm.nih.gov/api/procedures/v3/search');

			searchConditions.onMouseDownListener = function(d){
				$('#medical_conditions').tagsinput('add',d.currentTarget.innerHTML);
			}

			searchMedications.onMouseDownListener = function(d){
				$('#medications').tagsinput('add',d.currentTarget.innerHTML);
			}

			searchProcedures.onMouseDownListener = function(d){
				$('#procedures').tagsinput('add',d.currentTarget.innerHTML);
			}

			$('#medical_conditions').tagsinput({
				typeahead: {
					minLength: 0,
					showHintOnFocus:"all",
					allowDuplicates: false,
				}
			});

			$('#medications').tagsinput({
				typeahead: {
					minLength: 0,
					showHintOnFocus:"all",
					allowDuplicates: false,
				}
			});

			$('#procedures').tagsinput({
				typeahead: {
					minLength: 0,
					showHintOnFocus:"all",
					allowDuplicates: false,
				}
			});

			$('.autocomp_selected ul').hide();
	});
</script>
<script type="text/javascript">
	$(function(){
		$('.content').on('click','.clearLocalStore', function() {
			return localStorage.removeItem('viewLastTab');
		});

		// for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
		$('.nav-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			 $('html, body').stop().animate({
				'scrollTop': $('.tab-menu').offset().top-20
			}, 700, 'swing', function () {
				//window.location.hash = $('.tab-menu');
			});
			// save the latest tab; use cookies if you like 'em better:
			localStorage.setItem('viewLastTab', $(this).attr('href'));
		});

		// go to the latest tab, if it exists:
		var viewLastTab = localStorage.getItem('viewLastTab');
		if (viewLastTab) {
			$('.nav-tabs [href="' + viewLastTab + '"]').tab('show');
		}
	});

    window.addEventListener('keydown', function(e) {
        if (e.keyIdentifier == 'U+000A' || e.keyIdentifier == 'Enter' || e.keyCode == 13) {
            if (e.target.nodeName == 'INPUT' && e.target.type == 'text' && !$(e.path[1]).hasClass('bootstrap-tagsinput')) {
                e.preventDefault();
                return false;
            }
        }
    }, true);

	initMap();

	var componentForm = {
		sublocality_level_2: 'short_name',
		street_number: 'short_name',
		route: 'long_name',
		locality: 'long_name',
		administrative_area_level_1: 'long_name',
		country: 'long_name',
		postal_code: 'short_name'
	};

	var componentIds = {
		sublocality_level_2: 'area',
		locality: 'city',
		administrative_area_level_1: 'state',
		country: 'country',
		postal_code: 'zipcode'
	};

	function handleLocationError(browserHasGeolocation, infoWindow, pos) {
	    // infoWindow.setPosition(pos);
	    // infoWindow.setContent(browserHasGeolocation ?
	    //                       'Error: The Geolocation service failed.' :
	    //                       'Error: Your browser doesn\'t support geolocation.');
	}

	function initMap() {
		var map = new google.maps.Map(document.getElementById('map'), {
			center: {lat: 16.28004385258969, lng: 78.10594640625004},
			zoom: 6
		});
		var card = document.getElementById('pac-card');
		var input = document.getElementById('pac-input');
		var types = document.getElementById('type-selector');
		var strictBounds = document.getElementById('strict-bounds-selector');

		map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

		var autocomplete = new google.maps.places.Autocomplete(input);

		// Bind the map's bounds (viewport) property to the autocomplete object,
		// so that the autocomplete requests use the current map bounds for the
		// bounds option in the request.
		autocomplete.bindTo('bounds', map);

		var infowindow = new google.maps.InfoWindow();
		var infowindowContent = document.getElementById('infowindow-content');
		infowindow.setContent(infowindowContent);
		var marker = new google.maps.Marker({
			map: map,
			animation: google.maps.Animation.DROP,
			draggable: true,
			anchorPoint: new google.maps.Point(0, -29)
		});

		// Try HTML5 geolocation.
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
				var pos = {
					lat: position.coords.latitude,
					lng: position.coords.longitude
				};
				map.setCenter(pos);
			}, function() {
				handleLocationError(true, infowindow, map.getCenter());
			});
		} else {
		    // Browser doesn't support Geolocation
		    handleLocationError(false, infowindow, map.getCenter());
		}

		google.maps.event.addListener(
			marker,
			'drag',
			function(event) {
				infowindow.close();
				document.getElementById('latitude').value = this.position.lat();
				document.getElementById('longitude').value = this.position.lng();
		        //alert('drag');
		    });


		google.maps.event.addListener(marker,'dragend',function(event) {
			infowindow.close();
			document.getElementById('latitude').value = this.position.lat();
			document.getElementById('longitude').value = this.position.lng();
		        //alert('Drag end');
		    });

		autocomplete.addListener('place_changed', function() {
			infowindow.close();
			marker.setVisible(false);
			var place = autocomplete.getPlace();
			if (!place.geometry) {
				// User entered the name of a Place that was not suggested and
				// pressed the Enter key, or the Place Details request failed.
				window.alert("No details available for input: '" + place.name + "'");
				return;
			}

			// If the place has a geometry, then present it on a map.
			if (place.geometry.viewport) {
				map.fitBounds(place.geometry.viewport);
			} else {
				map.setCenter(place.geometry.location);
				map.setZoom(17);  // Why 17? Because it looks good.
			}
			marker.setPosition(place.geometry.location);
			marker.setVisible(true);

			// Update Location Co-ordinates Values
			$('#latitude').val(place.geometry.location.lat());
			$('#longitude').val(place.geometry.location.lng());

			var address = '';
			if (place.address_components) {
				address = [
				(place.address_components[0] && place.address_components[0].short_name || ''),
				(place.address_components[1] && place.address_components[1].short_name || ''),
				(place.address_components[2] && place.address_components[2].short_name || '')
				].join(' ');

				// Get each component of the address from the place details
		        // and fill the corresponding field on the form.
		        for (var i = 0; i < place.address_components.length; i++) {
		        	var addressType = place.address_components[i].types[0];
		        	if (componentForm[addressType]) {
		        		var val = place.address_components[i][componentForm[addressType]];
		        		if(typeof componentIds[addressType] != 'undefined'){
		        			document.getElementById(componentIds[addressType]).value = val;
							$('#'+componentIds[addressType]).trigger("change");
		        		}
		        	}
		        }
		    }

		    infowindowContent.children['place-icon'].src = place.icon;
		    infowindowContent.children['place-name'].textContent = place.name;
		    infowindowContent.children['place-address'].textContent = address;
		    infowindow.open(map, marker);
		});

		// Sets a listener on a radio button to change the filter type on Places
		// Autocomplete.
		function setupClickListener(id, types) {
			var radioButton = document.getElementById(id);
			radioButton.addEventListener('click', function() {
				autocomplete.setTypes(types);
			});
		}

		setupClickListener('changetype-all', []);
		setupClickListener('changetype-address', ['address']);
		setupClickListener('changetype-establishment', ['establishment']);
		setupClickListener('changetype-geocode', ['geocode']);

		document.getElementById('use-strict-bounds')
		.addEventListener('click', function() {
			console.log('Checkbox clicked! New state=' + this.checked);
			autocomplete.setOptions({strictBounds: this.checked});
		});
	}
</script>
<script>
	$(function(){
		initBlock();

		initAutoComplete({
			'route': 'area',
			'locality': 'city',
			'postal_code': 'zipcode',
			'administrative_area_level_1': 'state',
			'country': 'country'
		});

		var referralNames = new Bloodhound({
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			local:  ["(A)labama","Alaska","Arizona","Arkansas","Arkansas2","Barkansas"]
		});
		referralNames.initialize();

		$('.referrer-name').tagsinput({
			typeaheadjs: {
				name: 'referralNames',
				source: referralNames.ttAdapter()
			}
		});

		$('.content').on('click', '#checkForPreviousCarePlans', function(){
			checkIfExistingCustomer();
		});

		$('.content').on('click','.btnChooseProvider', function(){
			var id = $(this).data('visit-id');

			$('#providerSearchForm #filter_city').val($('#patient_details_tab #city').val());
			$('#providerSearchModal .btnSelectProvider').attr('data-visit-id',id);
			$('#providerSearchModal').modal();
		});

		$('.content').on('click', '.btnSelectProvider', function(){
			var id = $(this).attr('data-visit-id');

			var branch = $('input[name=choosen_provider]:checked').val();
			var provider = $('input[name=choosen_provider]:checked').data('provider');
			var name = $('input[name=choosen_provider]:checked').data('name');
			var location = $('input[name=choosen_provider]:checked').data('location');
			var phone = $('input[name=choosen_provider]:checked').data('phone');
			var logo = $('input[name=choosen_provider]:checked').data('logo');

			 if(typeof provider != 'undefined' && provider != ''){
				 $('#providerSearchModal').modal('hide');
				 $('#branch_id').val(branch);
				 $('#provider_id').val(provider);
				 $('#selectedProviderName').html('<div class="col-sm-3"><img src="{{ asset('uploads/provider') }}/'+provider+'/'+logo+'" class="img-responsive" style="width: 100px"></div><div class="col-sm-8"><b>'+name+'</b><br>'+location+'<br>Phone: '+phone+'</div>');
			 }else{
				 alert("Please select provider !");
			 }
		});

		$('.content').on('change','#city',function(e){
			$('#filter_city').val($(this).val());
		});

		$('.content').on('change','input[name=choosen_provider]',function(){
			var branch = $('input[name=choosen_provider]:checked').val();
			var provider = $('input[name=choosen_provider]:checked').data('provider');
			var name = $('input[name=choosen_provider]:checked').data('name');
			var location = $('input[name=choosen_provider]:checked').data('location');
			var phone = $('input[name=choosen_provider]:checked').data('phone');
			var logo = $('input[name=choosen_provider]:checked').data('logo');

			 if(typeof provider != 'undefined' && provider != ''){
				 $('#providerSearchModal').modal('hide');
				 $('#branch_id').val(branch);
				 $('#provider_id').val(provider);
				 $('#selectedProviderName').html('<div class="col-sm-2" style="width: auto"><img class="img-responsive provider-img" style="width: 80px !important"></div><div class="col-sm-8"><b>'+name+'</b><br>'+location+'<br>Phone: '+phone+'</div>');

				 if(logo != null && logo != '' && typeof logo != 'undefined'){
					 $('#selectedProviderName img').attr("src",'{{ asset('uploads/provider') }}/'+provider+'/'+logo);
				 }else{
					 $('#selectedProviderName img').attr('src','{{ asset('img/provider.png') }}');
					 $('#selectedProviderName img').attr('style',"width: 60px");
				 }
			 }else{
				 alert("Please select provider !");
			 }
		});

		$('.content').on('click','.btnFilterProvider', function(){
			gender = $('#filter_gender option:selected').val();
			city = $('#filter_city').val();
			area = $('#filter_area').val();
			lat = $('#filter_latitude').val();
			lng = $('#filter_longitude').val();
			patLat = $('#latitude').val();
			patLng = $('#longitude').val();

			$.ajax({
				url: '{{ route('ajax.provider.filter') }}',
				type: 'POST',
				data: {_token: '{{ csrf_token() }}', g: gender, c: city, a: area, lat: lat, lng: lng},
				success: function (data){
					html = '';
					count = 0;
					if(data.length){
						$(data).each(function(i){
							var details = data[i].details;
							var caregivers = data[i].caregivers;
							var rates = data[i].rates;

							if(caregivers.length >= 0){
								count++;
								html += `<tr data-id="`+details.id+`" class="provider-details">`;

								if(details){
									html += `<td title="`+details.organization_name+`">`+details.organization_short_name+`</td>`;
									html += `<td>`+details.city+`<br><small>`+details.area+`</small></td>`;
									html += `<td>`+details.contact_person+`<br><small>`+details.phone_number+`</small></td>`;
									html += `<td>Male: `+details.availability.m+`<br/> Female: `+details.availability.f+`</td>`;
									if(caregivers.length)
										html += `<td class="text-center"><a href="javascript: void(0);" class="toggle-row-view" data-row="caregivers_`+details.id+`">View</a></td>`;
									else
										html += `<td class="text-center">-</td>`;

									distanceTxt = calculateDistance(lat, lng, patLat, patLng);
									html += `<td class="text-center">`+distanceTxt+`</td>`;

									html += `<td class="text-center"></td>`;

									// if(rates.length)
									// 	html += `<td class="text-center"><a href="javascript: void(0);" class="toggle-row-view" data-row="rates_`+details.id+`">View</a></td>`;
									// else
									// 	html += `<td class="text-center">-</td>`;

									html += `<td class="text-center"><input name="choosen_provider" type="radio" id="choosen_provider_`+details.id +`_`+details.branch_id+`" class="with-gap radio-col-blue" data-name="`+details.organization_name+`" data-location="`+details.city+`" data-provider="`+details.id+`" value="`+details.branch_id+`" data-phone="`+details.phone_number+`" data-logo="`+details.logo+`" /><label for="choosen_provider_`+details.id +`_`+details.branch_id+`"></label></td>`;
								}

								html += `</tr>`;

								if(caregivers.length){
									html += `<tr style="display: none; background: #fbe7ad; max-height: 200px !important; overflow-y:scroll" id="caregivers_`+details.id+`"><td colspan="8">
												<table class="table table-striped table-condensed caregiverTable" style="margin:0;max-height: 200px !important; overflow-y:scroll">
													<thead class="bg-cyan">
														<tr>
															<th>Caregiver Name</th>
															<th>Gender</th>
															<th>Age</th>
															<th>Specialization</th>
															<th>Qualification</th>
															<th>Experience</th>
															<th>Languages Known</th>
														</tr>
													</thead>
													<tbody>`;
									$(caregivers).each(function(j){
										experience = caregivers[j].caregiver_experience == null?'-':caregivers[j].caregiver_experience;
										specialization = caregivers[j].caregiver_specialization == null?'-':caregivers[j].caregiver_specialization;
										qualification = caregivers[j].caregiver_qualification == null?'-':caregivers[j].caregiver_qualification;
										html += `<tr>`;
										html += `<td>`+caregivers[j].caregiver_name+`</td>` +
												`<td>`+caregivers[j].caregiver_gender+`</td>` +
												`<td>`+caregivers[j].caregiver_age+`</td>` +
												`<td>`+specialization+`</td>` +
												`<td>`+qualification+`</td>` +
												`<td>`+experience+`</td>` +
												`<td>`+caregivers[j].caregiver_languages_known+`</td>`;
										html += `</tr>`;
									});

									html += `</tbody></table></td></tr>`;
								}
								
								// if(rates.length){
								// 	html += `<tr style="display: none; background: #fbe7ad" id="rates_`+details.id+`"><td colspan="7">
								// 				<table class="table table-striped table-condensed" style="margin:0">
								// 					<thead class="bg-teal">
								// 						<tr>
								// 							<th>Servie Name</th>
								// 							<th>Rate</th>
								// 						</tr>
								// 					</thead>
								// 					<tbody>`;
								// 	$(rates).each(function(k){
								// 		html += `<tr>`;
								// 		html += `<td>`+rates[k].service_name+`</td>` +
								// 				`<td>`+rates[k].rate+`</td>`;
								// 		html += `</tr>`;
								// 	});
								
								// 	html += `</tbody></table></td></tr>`;
								// }
							}
						});
					}

					if(count > 0){
						$('.providerSearchResults tbody').html(html);
						$('.providerSearchResults').dataTable();
					}else{
						$('.providerSearchResults tbody').html('<tr><td colspan="8" class="text-center">No matching provider(s) found</td></t>');
					}

				},
				error: function (error){
					console.log(error);
				}
			})
		});

		$('.content').on('click','.toggle-row-view',function(){
			id = $(this).data('row');
			$('.providerSearchResults tbody tr#'+id).toggle();
			$('.caregiverTable').dataTable();
		})

		$('.content').on('input','.amtCal', function(){
			var id = $(this).data('visit-id');
			calculateTotal(id);
		});

		$('.content').on('click','.btnReset', function(){
			clearSearchForm();
		});
	});

	function initBlock(){
		$('.date').inputmask('dd-mm-yyyy', { placeholder: '__-__-____' });
	}

	function calculateDistance(lat1, lon1, lat2, lon2, unit) {
		var radlat1 = Math.PI * lat1/180;
		var radlat2 = Math.PI * lat2/180;
		var theta = lon1-lon2;
		var radtheta = Math.PI * theta/180;
		var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
		dist = Math.acos(dist);
		dist = dist * 180/Math.PI;
		dist = dist * 60 * 1.1515;
		if (unit=="K") { dist = dist * 1.609344; }
		if (unit=="N") { dist = dist * 0.8684; }

		//console.log('calculateDistance: '+dist);

		return dist?dist.toFixed(2)+' Km(s)':'-';
	}

	function checkIfExistingCustomer()
	{
		var fname = $('#first_name').val();
		var lname = $('#last_name').val();
		var mobile = $('#mobile').val();
		var email = $('#email').val();
		var dob = $('#date_of_birth').val();

		if(fname != '' || lname != '' || mobile != '' || email != '' || dob != ''){
			$.ajax({
				url: '{{ route('case.check-for-existing-case') }}',
				type: 'POST',
				data: {f: fname, l: lname, m: mobile, e: email, d: dob, _token: '{{ csrf_token() }}'},
				success: function (data){
					console.log(data);
				},
				error: function (error){
					console.log(error);
				}
			});
		}
	}

	function clearSearchForm()
	{
		$('#providerSearchModal #providerSearchForm #reset').trigger('click');
		$('#providerSearchModal #providerSearchForm #filter_gender').selectpicker('val','0');
		$('#providerSearchModal .providerSearchResults tbody').html('<tr><td class="text-center" colspan="7">Click \'Search\' to getresults</td></tr>');
	}

	var visitListArray = [];

	function saveCase()
	{
		// Parse Patient Details
		var patientDetails = [];
		var fname = $('#first_name').val();
		var lname = $('#last_name').val();
		var patientAge = $('#patient_age').val();
		var patientWeight = $('#patient_weight').val();
		var gender = $('#gender option:selected').val();

		var address = $('#street_address').val();
		var area = $('#area').val();
		var city = $('#city').val();
		var zip = $('#zipcode').val();
		var state = $('#state').val();
		var country = $('#country').val();

		// Parse Enquirer Details
		var enquirerName = $('#enquirer_name').val();
		var contactNumber = $('#contact_number').val();
		var alternateNumber = $('#alternate_number').val();
		var email = $('#email').val();
		var relationship = $('#relationship_with_patient option:selected').val();

		patientDetails = {
			'first_name': fname,
			'last_name': lname,
			'gender': gender,
			'patient_age': patientAge,
			'patient_weight': patientWeight,
			'street_address': address,
			'area': area,
			'city': city,
			'zipcode': zip,
			'state': state,
			'country': country,
			'enquirer_name': enquirerName,
			'contact_number': contactNumber,
			'alternate_number': alternateNumber,
			'email': email,
			'relationship_with_patient': relationship
		};

		// Parse Care Plan Details
		var carePlanDetails = [];
		var medicalConditions = $('#medical_conditions').val();
		var medications = $('#medications').val();
		var procedures = $('#procedures').val();

		var caseDescription = $('#case_description').val();
		var serviceRequired = $('#service_id option:selected').val();
		var serviceName = $('#service_id option:selected').text();
		var noOfHours = $('#no_of_hours').val();
		var genderPreference = $('input[name=gender_preference]:checked').val();
		var languagePreference = [];
		$(".language-preference input[type=checkbox]:checked").map(function(){
			languagePreference.push($(this).val());
		});
		var rateAgreed = $('#rate_agreed').val();
		var negotiable = $('input[name=rate_negotiable]:checked').val();
		var lead = $('#lead_id').val();
		var source = $('#source').val();
		var referrerName = $('#referrer_name').val();

		var providerID = $('#provider_id').val();
		var branchID = $('#branch_id').val();
		var providerNotes = $('#provider_notes').val();

		var previousProviderId = $('#previous_provider_id').val();
		var previousLeadId = $('#previous_lead_id').val();

		carePlanDetails = {
			'lead_id': lead,
			'provider_notes': providerNotes,
			'case_description': caseDescription,
			'medical_conditions': medicalConditions,
			'medications': medications,
			'procedures': procedures,
			'service_id': serviceRequired,
			'service_name': serviceName,
			'no_of_hours': noOfHours,
			'rate_agreed': rateAgreed,
			'rate_negotiable': negotiable,
			'gender_preference': genderPreference,
			'language_preference': languagePreference.toString(),
			'source': source,
			'referrer_name': referrerName
		};

		var previousAssignedCaseDetails = [];

		previousAssignedCaseDetails = {
			'previous_provider_id': previousProviderId,
			'previous_lead_id' :previousLeadId
		};
		
		var lReq = {{ $leadRequestId }};

		if(patientDetails && carePlanDetails){
			$('.btnSaveAggLead, .btn-circle').prop('disabled',true);
			$.ajax({
				url: '{{ route('case.save-case-aggregator') }}',
				type: 'POST',
				data: {_token:'{{ csrf_token() }}',pid: providerID, bid: branchID, patient: patientDetails, careplan: carePlanDetails, pacd: previousAssignedCaseDetails,lReq: lReq},
				success: function (data){
					alert("Case Created Successfully");
					window.location.href = '{{ route('case.index') }}';
				},
				error: function (error){
					// console.log(error);
					$('.btnSaveAggLead, .btn-circle').prop('disabled',false);
				}
			});
		}else{
			showNotification('bg-red', 'Please enter case details!', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
		}

		return false;
	}
	$(function(){
		@if(isset($l))
			@if(isset($l->patient))
			$('#relationship_with_patient').selectpicker('val','{{ $l->patient->relationship_with_patient }}');
			$('#relationship_with_patient').selectpicker('refresh');
			@endif

			@if(isset($l->requirement))
			$('#service_id').selectpicker('val','{{ $l->requirement }}');
			$('#service_id').selectpicker('refresh');
			@endif

			@if(isset($l->source))
			$('#source').selectpicker('val','{{ $l->source }}');
			$('#source').selectpicker('refresh');
			@endif

			@if($l->medical_conditions)
			$('#medical_conditions').removeAttr('placeholder');
			@foreach(explode(",",$l->medical_conditions) as $mc)
			$('#medical_conditions').tagsinput('add','{{ $mc }}');
			@endforeach
			@endif

			@if($l->medications)
			$('#medications').removeAttr('placeholder');
			@foreach(explode(",",$l->medications) as $m)
			$('#medications').tagsinput('add','{{ $m }}');
			@endforeach
			@endif

			@if($l->procedures)
			$('#procedures').removeAttr('placeholder');
			@foreach(explode(",",$l->procedures) as $p)
			$('#procedures').tagsinput('add','{{ $p }}');
			@endforeach
			@endif

			@if($l->status)
			$('#status').selectpicker('val','{{ $l->status }}');
			$('#status').selectpicker('refresh');
			$('#status').trigger("change");
			@endif
		@endif
	});
	function validator() {
		var fname = $('#first_name').val();
		var gender = $('#gender option:selected').val();
		var patientAge = $('#patient_age').val();
		var address = $('#street_address').val();
		var city = $('#city').val();
		var state = $('#state').val();
		var enquirerName = $('#enquirer_name').val();
		var enquirerPhone = $('#enquirer_phone').val();
		var medicalConditions = $('#medical_conditions').val();
		var medications = $('#medications').val();
		var procedures = $('#procedures').val();
		var serviceRequired = $('#service_id option:selected').val();
		var noOfHours = $('#no_of_hours').val();

		if (fname == ""){
			alert("Please Enter First Name");
			$("label[for='"+$('#first_name').attr('id')+"']").addClass('required');
			document.getElementById("first_name").focus();

			return false;
		}
		if (serviceRequired == ""){
			alert("Please Enter Service Required");
			$("label[for='"+$('#service_id').attr('id')+"']").addClass('required');
			document.getElementById("service_id").focus();

			return false;
		}
		if (gender == ""){
			alert("Please Enter Gender");
			$("label[for='"+$('#gender').attr('id')+"']").addClass('required');
			document.getElementById("gender").focus();

			return false;
		}
		if (patientAge == ""){
			alert("Please Enter Patient's Age");
			$("label[for='"+$('#patient_age').attr('id')+"']").addClass('required');
			document.getElementById("patient_age").focus();

			return false;
		}
		if (address == ""){
			alert("Please Enter Address");
			$("label[for='"+$('#address').attr('id')+"']").addClass('required');
			document.getElementById("address").focus();

			return false;
		}
		if (city == ""){
			alert("Please Enter City");
			$("label[for='"+$('#city').attr('id')+"']").addClass('required');
			document.getElementById("city").focus();

			return false;
		}
		if (state == ""){
			alert("Please Enter State");
			$("label[for='"+$('#state').attr('id')+"']").addClass('required');
			document.getElementById("state").focus();

			return false;
		}
		if (enquirerName == ""){
			alert("Please Enter Enquirer Name");
			$("label[for='"+$('#enquirer_name').attr('id')+"']").addClass('required');
			document.getElementById("enquirer_name").focus();

			return false;
		}
		if (enquirerPhone == ""){
			alert("Please Enter Enquirer Phone");
			$("label[for='"+$('#enquirer_phone').attr('id')+"']").addClass('required');
			document.getElementById("enquirer_phone").focus();

			return false;
		}
		if (medicalConditions == ""){
			alert("Please Enter Medical Condition");
			$("label[for='"+$('#medical_conditions').attr('id')+"']").addClass('required');
			document.getElementById("medical_conditions").focus();

			return false;
		}
		if (medications == ""){
			alert("Please Enter Medications");
			$("label[for='"+$('#medications').attr('id')+"']").addClass('required');
			document.getElementById("medications").focus();

			return false;
		}
	    if (noOfHours == ""){
	    	alert("Please Enter no. of Hours");
	    	$("label[for='"+$('#no_of_hours').attr('id')+"']").addClass('required');
			document.getElementById("no_of_hours").focus();

	    	return false;
	    }
	    saveCase();
	}
</script>
@endsection
