<!DOCTYPE html>
<html>
<head>
	<title>Service Records</title>
	<!-- Bootstrap Core Css -->
	<link href="{{ ViewHelper::ThemePlugin('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/print.css') }}" media="print" />
	<style>
		body {font-family: 'Roboto', Arial, Tahoma, sans-serif;}
		.service_details {padding-left:5em}
	</style>
</head>
<body>
	<div class="container-fluid">
		<h2 class="text-center">{{ session('organization_name') }}</h2>
		<table style="width:100%;">
			<tr>
				<td style="width:33%;">
					<h4>
					{{ $patient->first_name.' '.$patient->last_name.' - '.$patient->patient_id }}
					</h4>
				</td>
				<td style="width:33%;">
					<h4 class="text-center">Service Record History</h4>
				</td>
				<td style="width:33%;">
					<p class="text-right"><button onclick="window.print()" class="btn btn-default btn-sm noPrint"><span class="glyphicon glyphicon-print"></span> Print</button></p>
				</td>
			</tr>
		</table>
		<hr>
		<div class="col-md-10">
			<h3 class="text-center"><u>Service List</u></h3>
			@foreach ($services as $i => $service)
				<h3>{{ $i }}</h3>
				@foreach($service as $s)
					<?php $caregivers = $s->schedules->unique('caregiver_id')->where('caregiver_id','!=',null)->where('caregiver_id','!=',0)->pluck('caregiver_id') ?>
					@if(isset($s->schedules) && count($s->schedules) && count($caregivers))
			            <p class="service_details">
			            	- <b><i>[{{ $s->lead->episode_id }}]</i> {{ $s->service->service_name }}</b> 
			            	service added on <b>{{ $s->created_at->format('d-m-Y') }}</b> dated 
			            	 from <b>{{ \App\Entities\Schedule::getFirstScheduleDate($s->id) }}</b> to <b>{{ \App\Entities\Schedule::getLastScheduleDate($s->id) }}</b>
			            	@foreach($caregivers as $c)
			            		@if ($loop->first) attended by
			            		@else ,
			            		@endif
			            		{{ \App\Entities\Caregiver\Caregiver::whereId($c)->value('first_name').' '.
			            		\App\Entities\Caregiver\Caregiver::whereId($c)->value('middle_name').' '.
			            		\App\Entities\Caregiver\Caregiver::whereId($c)->value('last_name') }}
			            		@if ($loop->last). @endif
			            	@endforeach
			            </p>
		            @endif
				@endforeach
			@endforeach
		</div>
		<div class="col-md-2" style="border-left: 2px solid darkgrey;">
			<h3 class="text-center"><u>Episode List</u></h3>
			@foreach($patient->lead->where('episode_id','!=',null) as $lead)
				<h5>{{ $loop->iteration }} - <a target="_blank" href="{{ route('lead.view',Helper::encryptor('encrypt',$lead->id)) }}" title="Visit episode" >{{ $lead->episode_id }}</a></h5>
			@endforeach
		</div>
	</div>
</body>
</html>