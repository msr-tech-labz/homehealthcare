@extends('layouts.main-layout')

@section('page_title','Patient Incidents |')

@section('active_cases','active')

@section('plugin.styles')
	<!-- Bootstrap Select Css -->
	<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
	<!-- Pepper Grinder Css for Multi Datespicker-->
	<link href="https://code.jquery.com/ui/1.12.1/themes/pepper-grinder/jquery-ui.css" rel="stylesheet" />
	<!-- Multiple Dates Picker Css-->
	<link href="https://cdn.rawgit.com/dubrox/Multiple-Dates-Picker-for-jQuery-UI/master/jquery-ui.multidatespicker.css" rel="stylesheet" />
	<!-- Bootstrap Tagsinput Css -->
	<link href="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
	<!-- NoUiSlider Css -->
	<link href="{{ ViewHelper::ThemePlugin('nouislider/nouislider.min.css') }}" />
	<!-- Font Awesome Css -->
	<link href="{{ ViewHelper::ThemePlugin('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
	<!-- Bootstrap datetimepicker Css -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">

	<!-- Med Autocomplete Css -->
	<link rel="stylesheet" href="https://clin-table-search.lhc.nlm.nih.gov/autocomplete-lhc-versions/13.0.2/autocomplete-lhc_jQueryUI.min.css"/>
	<!-- Print Css -->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/print.css') }}" media="print" />
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section('page.styles')
@endsection

@section('content')

	@if (count($errors) > 0)
	<div class="row clearfix">
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	</div>
	@endif

<div class="row clearfix">
    <div class="col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('lead.operations') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-md-5">
                    Patient Incidents
                    <small>Incident tracking</small>
                </h2>
                <div class="col-md-6">
                    @ability('admin','leads-create')
                    <div class="col-sm-3 text-right pull-right">
                        <button type="button" class="btn btn-primary waves-effect" data-toggle="modal" data-target="#incidentModal">Add Incident</button>
                    </div>
                    @endability
                </div>
            </div>
            <div class="body clearfix">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#pendingTab" data-toggle="tab">
                            <i class="material-icons notranslate">announcement</i> PENDING
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#completedTab" data-toggle="tab">
                            <i class="material-icons notranslate">done</i> COMPLETED
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="pendingTab">
                        <div class="body">
                            <div id="tablePendingLeaves">
                                <div class="table-responsive">
                                    <table style="zoom:85%;" class="table table-bordered table-striped table-hover table-condensed js-basic-example">
                                       	<thead>
                                           	<tr>
                                               	<th>#</th>
												<th>Patient</th>
												<th>Location</th>
												<th>Complaint Date</th>
												<th>Complaint Type</th>
												<th>Manager</th>
												<th>Details</th>
												<th>Action Taken</th>
												<th>Complaint For</th>
												<th>Warning</th>
												<th>Action</th>
                                           	</tr>
                                       	</thead>
                                       	<tbody>
                                       	@if(count($pendingIncident))
	                                       	@foreach($pendingIncident as $pi)
	                                       		<tr>
	                                       			<td>{{ $loop->iteration }}</td>
	                                       			<td>{{ $pi->patient->full_name }}</td>
	                                       			<td>{{ $pi->patient->street_address??'' }} {{ $pi->patient->area??'' }}</td>
	                                       			<td>{{ $pi->created_at->format('d-m-Y') }}</td>
	                                       			<td>{{ $pi->complain_type }}</td>
	                                       			<td>@if($pi->manager==1) Admin 
	                                       				@else {{ $pi->user->caregiver->full_name }} @endif</td>
	                                       			<td>{{ $pi->complain_details??'-' }}</td>
	                                       			<td>{{ $pi->action_taken??'-' }}</td>
	                                       			<td>{{ isset($pi->caregiver)?$pi->caregiver->full_name:'-' }}</td>
	                                       			<td>{{ isset($pi->caregiver)?$pi->caregiver->professional->warning:'-' }}</td>
	                                       			<td><a class="btn btn-info" onclick="editIncident('{{$pi->id}}','{{ $pi->patient->full_name }} - {{ $pi->patient->patient_id }}','{{$pi->patient_id}}','{{$pi->complain_type}}','{{$pi->complain_details}}','{{$pi->complain_for_caregiver_id??''}}','{{$pi->action_taken??''}}','{{$pi->status}}')" data-toggle="modal" data-target="#incidentModal"><span class="view"></span> Edit</a></td>
	                                       		</tr>
	                                       	@endforeach
										@else
											<tr>
												<td colspan="11" class="text-center">No record(s)</td>
											</tr>
										@endif
                                       	</tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="completedTab">
                        <div class="body">
                            <div id="tableApprovedLeaves">
                                <div class="table-responsive">
                                    <table style="zoom:85%;" class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTableApprove">
                                       	<thead>
                                           <tr>
                                               	<th>#</th>
												<th>Patient</th>
												<th>Location</th>
												<th>Complaint Date</th>
												<th>Complaint Type</th>
												<th>Manager</th>
												<th>Details</th>
												<th>Action Taken</th>
												<th>Complaint For</th>
												<th>Warning</th>
												<th>Action</th>
                                           </tr>
                                       	</thead>
                                       	<tbody>
                                       	@if(count($completedIncident))
	                                       	@foreach($completedIncident as $ci)
	                                       		<tr>
	                                       			<td>{{ $loop->iteration }}</td>
	                                       			<td>{{ isset($ci->patient)?$ci->patient->full_name:'-' }}</td>
	                                       			<td>{{ $ci->patient->street_address??'' }} {{ $ci->patient->area??'' }}</td>
	                                       			<td>{{ $ci->created_at->format('d-m-Y') }}</td>
	                                       			<td>{{ $ci->complain_type }}</td>
	                                       			<td>@if($ci->manager==1) Admin 
	                                       				@else {{ $ci->user->caregiver->full_name }} @endif</td>
	                                       			<td>{{ $ci->complain_details??'-' }}</td>
	                                       			<td>{{ $ci->action_taken??'-' }}</td>
	                                       			<td>{{ isset($ci->caregiver)?$ci->caregiver->full_name:'-' }}</td>
	                                       			<td>{{ isset($ci->caregiver)?$ci->caregiver->professional->warning:'-' }}</td>
	                                       			<td><a class="btn btn-info" onclick="editIncident('{{$ci->id}}','{{ $ci->patient->full_name }} - {{ $ci->patient->patient_id }}','{{$ci->patient_id}}','{{$ci->complain_type}}','{{$ci->complain_details}}','{{$ci->complain_for_caregiver_id??''}}','{{$ci->action_taken??''}}','{{$ci->status}}')" data-toggle="modal" data-target="#incidentModal"><span class="view"></span> Edit</a></td>
	                                       		</tr>
	                                       	@endforeach
										@else
											<tr>
												<td colspan="11" class="text-center">No record(s)</td>
											</tr>
										@endif
                                       	</tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- New Incident Request Modal -->
<div class="modal fade" id="incidentModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
   	<div class="modal-dialog modal-lg" role="document">
       	<form action="{{ route('lead.add-incident') }}" method="POST" id="clear_form1">
           	{{  csrf_field() }}
           	<div class="modal-content">
               	<div class="modal-header bg-blue">
                   	<button type="button" class="close" data-dismiss="modal">&times;</button>
                   	<h4 class="modal-title" id="largeModalLabel">Add Incident</h4>
               	</div>
               	<div class="modal-body">
               		<div class="row form-horizontal">
                        <div class="col-md-6">
                        	<div class="form-group">
                                <label class="col-md-12 required" for="patient_id">Patient</label>
                                <div class="col-md-12 changablePatient">
									<select class="form-control show-tick" data-live-search="true" id="patient_id" name="patient_id" required>
										<option value="">-- Please select a Patient --</option>
										@foreach($patients as $i => $p)
											<option value="{{ $p->id }}">{{ $p->full_name }} - {{ $p->patient_id }}</option>
										@endforeach
									</select>
								</div>
                                <div class="col-md-12 nonChangablePatient" style="display:none;">
									<div class="form-line">
										<input class="form-control" id="patient_view_only" readonly>
										<input type="hidden" id="patient_id_view_only" name="patient_id_view_only">
										<input type="hidden" id="incident_id" name="incident_id">
									</div>
								</div>
							</div>
							<br>
                        	<div class="form-group">
                                <label class="col-md-12 required" for="complain_type">Complaint Type</label>
                                <div class="col-md-12">
									<select class="form-control show-tick" data-live-search="true" id="complain_type" name="complain_type" required>
										<option value="">-- Please select Complaint Type --</option>
										<option value="Late comings">Late comings</option>
										<option value="Miscommunication">Miscommunication</option>
										<option value="No Service">No Service</option>
										<option value="Others">Others</option>
										<option value="Patient related injury">Patient related injury</option>
										<option value="Replacement issues">Replacement issues</option>
										<option value="Rude behaviour">Rude behaviour</option>
										<option value="Staff Misfit">Staff Misfit</option>
										<option value="Theft">Theft</option>
										<option value="Unprofessional behaviour">Unprofessional behaviour</option>
									</select>
								</div>
							</div>
							<br>
                        	<div class="form-group">
                                <label class="col-md-12 required" for="complain_details">Details</label>
                                <div class="col-md-12">
									<div class="form-line">
										<textarea class="form-control" id="complain_details" name="complain_details" rows="3" placeholder="Details of the incident" required></textarea>
									</div>
								</div>
							</div>
                        </div>
                        <div class="col-md-6">
                        	<div class="form-group">
                                <label class="col-md-12" for="complain_for_caregiver_id">Complaint For</label>
                                <div class="col-md-12">
									<select class="form-control show-tick" data-live-search="true" id="complain_for_caregiver_id" name="complain_for_caregiver_id">
										<option value="">-- Please select a Caregiver --</option>
										@foreach($caregivers as $i => $c)
											<option value="{{$c->id}}">{{ $c->full_name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<br>
                        	<div class="form-group">
                                <label class="col-md-12" for="action_taken">Action Taken</label>
                                <div class="col-md-12">
									<div class="form-line">
										<textarea class="form-control" id="action_taken" name="action_taken" rows="3" placeholder="What action has been taken"></textarea>
									</div>
								</div>
							</div>
							<br>
							<div class="form-group nonChangablePatient" style="display:none;">
                                <label class="col-md-12" for="status">Status</label>
                                <div class="col-md-12">
                                    <input name="status" type="radio" id="statusPending" class="with-gap radio-col-deep-purple" value="Pending">
                                    <label for="statusPending">Pending</label>
                                    <input name="status" type="radio" id="statusComplete" class="with-gap radio-col-deep-purple" value="Completed">
                                    <label for="statusComplete">Completed</label>
                                </div>
                            </div>
                        </div>
                    </div>
               	</div>
				<div class="modal-footer" style="border-top:none !important;">
					{{ csrf_field() }}
					<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-success waves-effect pull-right btnSaveIncident">Save</button>
				</div>
           </div>
       </form>
   </div>
</div>
@endsection

@section('plugin.scripts')
	<!-- Select Plugin Js -->
	<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>

	<!-- Multiple Dates Picker -->
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
	<script src="https://cdn.rawgit.com/dubrox/Multiple-Dates-Picker-for-jQuery-UI/master/jquery-ui.multidatespicker.js"></script>

	<!-- Input Mask Plugin Js -->
	<script src="{{ ViewHelper::ThemePlugin('jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>
	<!-- Moment Plugin Js -->
	<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
	<!-- Bootstrap Material Datetime Picker Plugin Js -->
	<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>

	<!-- Bootstrap Tags Input Plugin Js -->
	<script src="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
	<script src="{{ ViewHelper::ThemePlugin('bootstrap-3-typeahead/bootstrap3-typeahead.js') }}"></script>

	<!-- Bootstrap Notify Plugin Js -->
	<script src="{{ ViewHelper::ThemePlugin('bootstrap-notify/bootstrap-notify.js') }}"></script>
	<script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
	<!-- noUiSlider-->
	<script src="{{ asset('themes/default/plugins/nouislider/nouislider.js') }}"></script>
	<script src="{{ asset('js/autocomplete.js')}}"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2-azeUdXbeAqQrvGtLZrbsXIFcofE_B8&libraries=places&region=IN"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.js"></script>
	<script src="https://clin-table-search.lhc.nlm.nih.gov/autocomplete-lhc-versions/13.0.2/autocomplete-lhc_jQueryUI.min.js"></script>
	<!--  Sweetalert -->
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="../../themes/default/js/pages/ui/tooltips-popovers.js"></script>
@endsection

@section('page.scripts')
	<script src="{{ ViewHelper::js('leads/form.js') }}"></script>
	<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
	<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
	<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
	<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
	<script src="/vendor/datatables/buttons.server-side.js"></script>
	<script>
	$(function(){
		$('.content').on('click','.clearLocalStore', function() {
		 	return localStorage.removeItem('incidentLastTab');
		});

      	// for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            // save the latest tab; use cookies if you like 'em better:
            localStorage.setItem('incidentLastTab', $(this).attr('href'));
        });

        // go to the latest tab, if it exists:
        var lastTab = localStorage.getItem('incidentLastTab');
        if (lastTab) {
            $('[href="' + lastTab + '"]').tab('show');
        }
    });

	function editIncident(incident_id,patient,patient_id,complain_type,complain_details,complain_for_caregiver_id,action_taken,status){
		$('#incident_id').val(incident_id);
		$('#patient_view_only').val(patient);
		$('#patient_id_view_only').val(patient_id);
		$('#complain_type').selectpicker('val',complain_type);
		$('textarea[name="complain_details"]').val(complain_details);
		$('#complain_for_caregiver_id').selectpicker('val',complain_for_caregiver_id);
		$('#complain_type').selectpicker('val',complain_type);		
		$('textarea[name="action_taken"]').val(action_taken);
		$('input[name="status"][value="'+status+'"]').prop("checked",true);

		$('#patient_id').prop('required',false);
		$('#incidentModal #largeModalLabel').html('Edit Incident');
		$('#incidentModal .btnSaveIncident').html('Update');
		$('.changablePatient').hide();
		$('.nonChangablePatient').show();

		$('#incidentModal').on('hidden.bs.modal', function(e){
			document.getElementById("clear_form1").reset();
			$('#patient_id').prop('required',true);
			$('#incident_id').val('');
			$('#complain_type').selectpicker('val','');
			$('#complain_for_caregiver_id').selectpicker('val','');
			$('#incidentModal #largeModalLabel').html('Add Incident');
			$('#incidentModal .btnSaveIncident').html('Save');
			$('.changablePatient').show();
			$('.nonChangablePatient').hide();
		});
	}
	</script>
@endsection
