@extends('layouts.main-layout')

@section('page_title','Leads - ')

@section('active_leads','active')

@section('page.styles')
<style>
.plus:after{
    content: ' + ';
    font-size: 18px;
    font-weight: bold;
}
</style>
@endsection

@section('plugin.styles')
<style>
.tab-nav-right > li > a{
    min-width: 180px !important;
    text-align: center;
}
.nav-tabs > li{
    margin-bottom:2px !important;
}
.nav-tabs > li.active{
    border-left: 1px solid #aabcfe;
    border-right: 1px solid #aabcfe;
    border-top: 4px solid #aabcfe;
    margin-bottom: 2px;
}
.nav-tabs > li.active > a{
    font-weight: bold;
    color: #2196F3 !important;
}
.nav-tabs > li > a:before{
    border-bottom: 0 !important;
}
table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td {
    /*text-transform: uppercase; !important;*/
}
.allocations:hover{
    cursor: pointer;
}

.followups:hover{
    cursor: pointer;
}

.serviceExpiry:hover{
    cursor: pointer;    
}

.distributionsTable, .serviceExpiryTable {
    margin-bottom: 0 !important;
}

.modal-body .table tr th, .modal-body .table  tr td {
    font-size: 12px !important;
    padding: 7px !important;
}

.filter{
    display: inline-block;
}
.filter-div > label{
    margin-right: 10px !important;
}
.bgagglead{
    background-color: lightblue !important;
}
</style>
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section('side-menu')
    @include('menus.leads')
@endsection

@section('content')    
    <style>
        .tile{
            /*width: 20% !important;*/
            padding-right: 5px !important;
            padding-left: 5px !important;
        }
        .info-box .icon{
            padding-top: 5px;
        }
        .info-box .content{
            margin: 1px 5px !important;
        }
        .info-box .content .text{
            margin-top: 5px !important;
        }
        .info-box .content .number {
            font-size: 18px !important;
        }
    </style>

    <div class="block-header col-sm-12">
        <div class="col-sm-3"><h2>Leads Dashboard</h2></div>
        <form action="{{ route('lead.index') }}" method="post">
            {{ csrf_field() }}
            <div class="col-sm-2" style="left: 5%">
                    <input type="text" id="from_date" name="from_date" class="form-control datepicker" style="background-color: #fff !important;" placeholder="From Date" value="{{ isset($filterFromDate)?\Carbon\Carbon::parse($filterFromDate)->format('d-m-Y'):date('01-m-Y') }}" />
            </div>
            <div class="col-sm-2" style="left: 5%">
                    <input type="text" id="to_date" name="to_date" class="form-control datepicker" style="background-color: #fff !important;" placeholder="To Date" value="{{ isset($filterToDate)?\Carbon\Carbon::parse($filterToDate)->format('d-m-Y'):date('t-m-Y') }}" />
            </div>
            <div class="col-sm-2" style="left: 5%">
                    <button type="submit" class="btn btn-primary waves-effect">FILTER</button>
            </div>
        </form>
        @ability('admin','leads-create')
        <div class="col-sm-3 text-right" style="float: right;">
            <a href="{{ route('lead.create') }}" class="btn btn-primary waves-effect">Create Lead</a>
        </div>
        @endability
    </div>
        <div class="clearfix"></div>
    @ability('admin','leads-index')
    <div class="row clearfix" style="margin-bottom: 30px;">
        <div class="col-lg-offset-2 col-lg-4 tile">
            <div class="info-box bg-red hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">people</i>
                </div>
                <div class="content">
                    <div class="text">Pending</div>
                    <div class="number count-to">{{ count($leads->whereIn('status',['Pending','Follow-Up','Assessment Pending','Assessment Completed'])) }}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 tile">
            <div class="info-box bg-pink hover-expand-effect followups" data-toggle="modal" data-target="#followupsModal">
                <div class="icon">
                    <i class="material-icons">settings_phone</i>
                </div>
                <div class="content">
                    <div class="text">Follow Up</div>
                    <div class="number count-to">{{ count($followupArray) }}</div>
                </div>
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="card">
            <div class="body">
                <div class="filter-div" style="margin-top: 10px;">
                    <b>Filter by Status : &nbsp;&nbsp;</b>
                    <input type="checkbox" name="filter_status" id="filter_pending" value="Pending" class="filled-in chk-col-blue filter">
                    <label for="filter_pending">Pending</label>
                    <input type="checkbox" name="filter_status" id="filter_followup" value="Follow Up" class="filled-in chk-col-indigo filter">
                    <label for="filter_followup">Follow Up</label>
                </div>
                <div class="clearfix"></div><br>                
                {!! $dataTable->table(['style="width:100%"']) !!}
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="modal fade" id="viewAggregatorLeadModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content" >
                <div class="modal-header bg-blue">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="largeModalLabel">Lead Details</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <tr>
                                <th>Patient Name</th>
                                <td class="aggLead_patient"></td>
                            </tr>
                            <tr>
                                <th>Age, Gender &amp; Weight</th>
                                <td class="aggLead_gender_age_weight"></td>
                            </tr>
                            <tr>
                                <th>Location</th>
                                <td class="aggLead_location"></td>
                            </tr>
                            <tr>
                                <th>Case Description</th>
                                <td class="aggLead_case"></td>
                            </tr>
                            <tr>
                                <th>Medical Condition</th>
                                <td class="aggLead_condition"></td>
                            </tr>
                            <tr>
                                <th>Medications</th>
                                <td class="aggLead_medication"></td>
                            </tr>
                            <tr>
                                <th>Surgeries Undergone/Continuing</th>
                                <td class="aggLead_procedures"></td>
                            </tr>
                            <tr>
                                <th>Service Required</th>
                                <td class="aggLead_service"></td>
                            </tr>
                            <tr>
                                <th>Language Preference</th>
                                <td class="aggLead_lang_pref"></td>
                            </tr>
                            <tr>
                                <th>Gender Preference</th>
                                <td class="aggLead_gender_pref"></td>
                            </tr>
                            <tr>
                                <th>Rate Agreed</th>
                                <td class="aggLead_rate"></td>
                            </tr>
                            <tr>
                                <th>Rate Negotiable</th>
                                <td class="aggLead_negotiable"></td>
                            </tr>
                        </table>
                        <form action="{{ route('lead.aggregator-lead-status') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="lead_id" value="" />
                            <input type="hidden" name="patient_id" value="" />
                            <div class="col-sm-12 text-center">
                                <div class="col-sm-6 text-center">
                                    <button type="submit" onclick="javascript: return confirm('Are you sure?')" name="leadStatus" value="1" class="btn btn-block btn-success">Accept, I have staff</button>
                                </div>
                                <div class="col-sm-6 text-center">
                                    <button type="submit" onclick="javascript: return confirm('Are you sure?')" name="leadStatus" value="0" class="btn btn-block btn-warning">Decline, I do not have staff</button>
                                </div>
                            </div><br><br>
                        </form>
                        <code style="font-size: 10px;line-height:1">
                            <i class="material-icons" style="font-size:16px">info_outline</i></small> By accepting the case, you acknowledge the availability of staff to render the service to the patient.
                        </code>
                    </div>
                    <div class="clearfix"></div><br>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="followupsModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-col-blue" >
                <div class="modal-header">                    
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <a class="btn btn-sm bg-teal pull-right" style="margin-right: 20px;margin-top:-1%" onclick="javascript: printData('.followUpTable');">Print</a>
                    <h4 class="modal-title" id="largeModalLabel">Required Follow Up List</h4>
                </div>
                <div class="modal-body">
                    
                    <div class="clearfix"></div><br>
                    <div class="table-responsive followUpTable" style="margin-bottom: 0;max-height:600px;overflow: auto;">
                        <table class="table table-bordered table-condensed table-hover followupsTable" style="margin-bottom: 0">
                            <thead>
                                <tr>
                                    <th width="3%">#</th>
                                    <th>Patient</th>
                                    <th>Enquirer</th>
                                    <th>Date to follow up</th>
                                    <th>Time</th>
                                    <th>Comment</th>
                                    <th>Phone</th>
                                    <th class="hidden-print">Link</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(isset($followupArray) && count($followupArray))
                                @foreach($followupArray as $i => $f)
                                    <tr>
                                        <td>{{ $i + 1 }}</td>
                                        <td>{{ $f['patient'] }}</td>
                                        <td>{{ $f['enquirer'] }}</td>
                                        <td>{{ $f['date'] }}</td>
                                        <td>{{ $f['time'] }}</td>
                                        <td>{{ $f['comment'] }}</td>
                                        <td>{{ $f['phone'] }}</td>
                                        <td class="hidden-print"><a rel="noreferrer noopener" target="_blank" href="{{ route('lead.view',$f['id']) }}" class="btn btn-xs btn-success">View</a></td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="8" class="text-center">
                                        No Required Follow Up(s) found.
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div><br>
                </div>
                <div class="modal-footer" style="border-top: 1px solid #ccc">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="allocationsModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-col-blue" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title" id="largeModalLabel">Required Allocations List</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive" style="margin-bottom: 0;max-height:600px;overflow: auto;">
                        <table class="table table-bordered table-condensed table-hover distributionsTable" style="margin-bottom: 0">
                            <thead>
                                <tr>
                                    <th width="3%">#</th>
                                    <th>Patient</th>
                                    <th>Enquirer</th>
                                    <th>Phone</th>
                                    <th>Status</th>
                                    <th>Link</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(isset($allocationArray) && count($allocationArray))
                                @foreach($allocationArray as $i => $a)
                                    <tr>
                                        <td>{{ $i + 1 }}</td>
                                        <td>{{ $a['patient'] }}</td>
                                        <td>{{ $a['enquirer'] }}</td>
                                        <td>{{ $a['phone'] }}</td>
                                        <td>{{ $a['status'] }}</td>
                                        <td><a rel="noreferrer noopener" target="_blank" href="{{ route('lead.view',$a['id']) }}" class="btn btn-xs btn-success">View</a></td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="text-center">
                                        No Required Allocation(s) found.
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div><br>
                </div>
                <div class="modal-footer" style="border-top: 1px solid #ccc">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>    
    
    <div class="modal fade" id="serviceExpiryModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-col-blue">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title" id="largeModalLabel">Upcoming Service Expiry List</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive" style="margin-bottom: 0;max-height:600px;overflow: auto;">
                        <table class="table table-bordered table-condensed table-hover serviceExpiryTable">
                            <thead>
                                <tr>
                                    <th width="3%">#</th>
                                    <th>Episode ID</th>
                                    <th>Service Name</th>
                                    <th>Patient</th>
                                    <th>Enquirer</th>
                                    <th>Phone</th>
                                    <th width="10%">Expiry Date</th>
                                    <th>Link</th>
                                </tr>
                            </thead>
                            <tbody>                            
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div><br>
                </div>
                <div class="modal-footer" style="border-top: 1px solid #ccc">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
    @endability
@endsection

@push('page.scripts')
    
@endpush

@section('page.scripts')
    @parent    
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css" /> 
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>   
    {!! $dataTable->scripts() !!}
    <script type="text/javascript">
        $(function(){
            $(".datepicker").flatpickr({
                dateFormat: "d-m-Y",
                mode: "single",
            });

            $('.content').on('click','.clearLocalStore', function() {
                return localStorage.removeItem('leadListLastTab');
            });

            // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // save the latest tab; use cookies if you like 'em better:
                localStorage.setItem('leadListLastTab', $(this).attr('href'));
            });

            // go to the latest tab, if it exists:
            var lastTab = localStorage.getItem('leadListLastTab');
            if (lastTab) {
                $('[href="' + lastTab + '"]').tab('show');
            }
        });
    </script>    
    <script>
    $(function(){    
        $('.dataTables_filter input').unbind();
        $('.dataTables_filter input').bind('keyup', function(e){
            if(e.keyCode == 13){
                window.LaravelDataTables["dataTableBuilder"].search(this.value).draw();
            }
        });

        $('.content').on('click','input[name=filter_status]',function(){
            var searchString = "";
            // Get all checked status
            $('input[name=filter_status]:checked').each(function(i){
                searchString += $(this).val() + "|";
            });

            if(searchString.length > 0 && typeof window.LaravelDataTables["dataTableBuilder"] != 'undefined'){
                searchString = searchString.substring(0, searchString.length-1);
                window.LaravelDataTables["dataTableBuilder"].column(8).search(searchString, true, false, true).draw();
            }else{
                window.LaravelDataTables["dataTableBuilder"].column(8).search("").draw();
            }
        });

        $('.content').on('click','.btnViewLead',function(){
            var id = $(this).data('id');
            var patientID = $(this).data('patient-id');
            var patient = JSON.parse($(this).data('patient').replace(/\'/g, '\"'));
            var data = JSON.parse($(this).data('case').replace(/\'/g, '\"'));
            
            modal = '#viewAggregatorLeadModal';

            $(modal+' input[name=lead_id]').val(id);
            $(modal+' input[name=patient_id]').val(patientID);
            $(modal+' .aggLead_patient').html(patient.first_name+' '+patient.last_name);
            $(modal+' .aggLead_gender_age_weight').html(patient.patient_age+', '+patient.gender+', '+patient.patient_weight);
            $(modal+' .aggLead_location').html(patient.area+','+patient.street_address+', '+patient.city);
            $(modal+' .aggLead_case').html(data.case_description);
            $(modal+' .aggLead_condition').html(data.medical_conditions);
            $(modal+' .aggLead_medication').html(data.medications);
            $(modal+' .aggLead_procedures').html(data.procedures);
            $(modal+' .aggLead_service').html(data.aggregator_service);
            $(modal+' .aggLead_lang_pref').html(data.language_preference);
            $(modal+' .aggLead_gender_pref').html(data.gender_preference);
            $(modal+' .aggLead_rate').html(data.aggregator_rate);
            $(modal+' .aggLead_negotiable').html(data.aggregator_rate_negotiable);

            $(modal).modal();
        });

        $('.content').on('click','.btnLeadType', function(){
            $('#leadTypeModal').modal();
            $('#leadTypeModal .btnOtherRequest').attr('data-id',$(this).data('id'));

            $('#leadTypeModal .openMedicalCase').attr('data-id',$(this).data('id'));
            $('#leadTypeModal .openMedicalCase').attr('href','');
            url = $('#leadTypeModal .openMedicalCase').data('href');
            url += '?ref='+$(this).data('id');
            $('#leadTypeModal .openMedicalCase').attr('href',url);
        });

        $('.content').on('click','.btnOtherRequest', function(){
            $('#leadTypeModal').modal('hide');
            id = $(this).attr('data-id');
            block = '#leadsTable tbody tr[data-id='+id+']';

            $('#otherRequestForm input[name=name]').val($(block+' td:eq(3)').html().replace(/<\/?[^>]+(>|$)/g, "").trim());
            $('#otherRequestForm input[name=phone_number]').val($(block+' td:eq(4)').html().replace(/<\/?[^>]+(>|$)/g, "").trim());
            $('#otherRequestForm input[name=email]').val($(block+' td:eq(5)').html().replace(/<\/?[^>]+(>|$)/g, "").trim());
            $('#otherRequestForm input[name=city]').val(removeTags($(block+' td:eq(6)').text()));

            // console.log($(block+' td:eq(6)').html()+"  :: "+removeTags($(block+' td:eq(6)').text()));

            $('#otherRequestForm #lead_id').val(id);

            $('#otherRequestsModal').modal();
        });

        $('.content').on('click', '.markLeadAsChecked', function(){
            id = $(this).data('id');
            if(confirm("Are you sure?")){
                $.ajax({
                    url: '{{ route('ajax.mark-lead-as-checked') }}',
                    type: 'POST',
                    data: {_token:'{{ csrf_token() }}',id: id},
                    success: function(data){
                        console.log(data);
                        $('#leadsTable tbody tr[data-id='+id+']')
                        .children('td, th')
                        .animate({ padding: 0 })
                        .wrapInner('<div />')
                        .children()
                        .slideUp(function() { $(this).closest('tr').remove(); });

                        //toastr.success("Lead Status has been updated");
                        alert("Lead Status has been updated");
                    },
                    error: function(err){
                        console.log(err);
                    }
                });
            }
        });
    });

    function removeTags(txt){
        var rex = /(<([^>]+)>)/ig;
        //return txt.replace(rex , ",").replace(/\s/g, '').replace(/,\s*$/, '');
        return txt.replace(/\s+/g,' ')
        .replace(/^\s+|\s+$/,'');
    }
    
    getServiceExpiry();
    
    function getServiceExpiry(){
        $.ajax({
            url: '{{ route('leads.get-upcoming-service-expiry-ajax') }}',
            type: 'POST',
            data: {_token: '{{ csrf_token() }}'},
            success: function (d){                    
                if(d.length){
                    html = ``;
                    link = '{{ url('leads/view') }}';
                    $.each(d, function(i){
                        html += `<tr>
                            <td>`+ (i + 1) +`</td>
                            <td>`+ d[i]['episode_id'] +`</td>
                            <td>`+ d[i]['service_name'] +`</td>
                            <td>`+ d[i]['patient'] +`</td>
                            <td>`+ d[i]['enquirer'] +`</td>
                            <td>`+ d[i]['phone'] +`</td>
                            <td>`+ d[i]['date'] +`</td>
                            <td><a target="_blank" href="`+ link + `/` + d[i]['id'] +`" class="btn btn-xs btn-success">View</a></td>
                        </tr>`;
                    });
                    $('.serviceExpiry .count-to').html(d.length);
                    $('.serviceExpiry .count-to').removeClass('loader');
                }else{
                    html = `<tr>
                        <td colspan="8" class="text-center">
                            No record(s) found
                        </td>
                    </tr>`;
                    $('.serviceExpiry .count-to').html('0');
                    $('.serviceExpiry .count-to').removeClass('loader');
                }
                $('.serviceExpiryTable tbody').html(html);
            },
            error: function (err){
                console.log(err);
            }
        });
    }

    function printData(elem) {
        data = $(elem).html();
        var myWindow = window.open('', 'my div', 'height=600,width=800');
        myWindow.document.write('<html><head><title>Follow-Up List</title>');
        myWindow.document.write('<link rel="stylesheet" href="{{ ViewHelper::ThemePlugin('bootstrap/css/bootstrap.min.css') }}" type="text/css" />');
        myWindow.document.write('</head><body >');
        myWindow.document.write(data);
        myWindow.document.write('</body></html>');
        myWindow.document.close(); // necessary for IE >= 10
        
        myWindow.onload = function(){ // necessary if the div contain images
            myWindow.focus(); // necessary for IE >= 10
            myWindow.print();
            myWindow.close();
        };
    }
    </script>
@endsection
