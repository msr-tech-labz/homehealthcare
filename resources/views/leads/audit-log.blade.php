<!DOCTYPE html>
<html>
<head>
	<title>Audit Log</title>
	<!-- Bootstrap Core Css -->
	<link href="{{ ViewHelper::ThemePlugin('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/print.css') }}" media="print" />
	<style>
		body {font-family: 'Roboto', Arial, Tahoma, sans-serif;}
		.card, .info-box {border: 1px solid #1dc8cd;}
        .comment-card { margin-bottom: 15px;}
		.comment-card .header { padding: 5px 10px;}
		.card .header {
		    color: #555;
		    position: relative;
		    border-bottom: 1px solid rgba(204, 204, 204, 0.35);
		}
		.col-teal {
    color: #009688 !important;
}
.card .header .header-dropdown {
    position: absolute;
    top: 20px;
    right: 15px;
    list-style: none;
}
		.col-pink {color:#E91E63; font-size:13px;}
		.comment-card .body { padding: 10px;}
		.comment-card .header h2 {font-size: 16px}
		.card .header h2 {
    margin: 0;
    font-weight: normal;
}
		.comment-card .header .header-dropdown li > span {font-size: 13px}
		.comment-card th{ background: #ccc}
		.comment-card .table{ margin-bottom: 0}
	</style>
</head>
<body>
	<div class="container-fluid">
		<h2 class="text-center">{{ session('organization_name') }}</h2>
		<table style="width:100%;">
			<tr>
				<td style="width:33%;">
					<h4>
					{{ $lead->patient->full_name }}
					@if(isset($lead->patient->patient_id)) - {{ $lead->patient->patient_id }} @endif
					</h4>
				</td>
				<td style="width:33%;">
					<h4 class="text-center">
						Audit Log 
						@if(isset($lead->episode_id)) - Episode Id {{ $lead->episode_id }} @endif</h4>
				</td>
				<td style="width:33%;">
					
				</td>
			</tr>
		</table>
		<hr>
		{!! AuditHelper::getCaseHistory($lead->id); !!}
	</div>
</body>
</html>