<html>
	<head>
		<title>Laravel Signature Pad Tutorial Example-nicesnippets.com </title>
    	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.css">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
    	<link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/south-street/jquery-ui.css" rel="stylesheet"> 
    	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    	<script type="text/javascript" src="http://keith-wood.name/js/jquery.signature.js"></script>
    	<link rel="stylesheet" type="text/css" href="http://keith-wood.name/css/jquery.signature.css">
    	<link rel="stylesheet" href="{{ asset('css/assessment.css') }}" />
    	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" crossorigin="anonymous">
    	<style>
    		.kbw-signature { width: 70%; height: 100px;}
	        #sig canvas{
	            width: 100% !important;
	            height: auto;
	        }
	        body {
			  font-family: Arial, Helvetica, sans-serif;
			  background-color: black;
			}

			* {
			  box-sizing: border-box;
			}

			/* Add padding to containers */
			.container-fluid {
			  padding: 16px;
			  background-color: white;
			}

			/* Full-width input fields */
			input[type=text], input[type=password] {
			  width: 100%;
			  padding: 15px;
			  margin: 5px 0 22px 0;
			  display: inline-block;
			  border: none;
			  background: #f1f1f1;
			}

			input[type=text]:focus, input[type=password]:focus {
			  background-color: #ddd;
			  outline: none;
			}

			/* Overwrite default styles of hr */
			hr {
			  border: 1px solid #f1f1f1;
			  margin-bottom: 25px;
			}

			/* Set a style for the submit button */
			.registerbtn {
			  background-color: #4CAF50;
			  color: white;
			  padding: 16px 20px;
			  margin: 8px 0;
			  border: none;
			  cursor: pointer;
			  width: 100%;
			  opacity: 0.9;
			}

			.registerbtn:hover {
			  opacity: 1;
			}

			/* Add a blue text color to links */
			a {
			  color: dodgerblue;
			}

			/* Set a grey background color and center the text of the "sign in" section */
			.signin {
			  background-color: #f1f1f1;
			  text-align: center;
			}
				.content {
			    max-width: 450px;
			    margin: auto;
			}

			.collection .collection-item.avatar {
			    padding: 5px;
			}

			.material-icons {
			    font-size: 50px;
			    color: #26a69a;
			}

			.collapsible-header  {
			    min-height: 4rem;
			    line-height: 4rem;
			    font-size: 0.9rem;
			}

			.collapsible-header  i {
			    width: 3rem;
			    line-height: 4rem;
			    margin-right: -10px;
			}

			.collapsible li.active i {
			  -ms-transform: rotate(180deg); /* IE 9 */
			  -webkit-transform: rotate(180deg); /* Chrome, Safari, Opera */
			  transform: rotate(180deg);
			}


			.collapsible-body {
			    background-color: #d0d0d0;
			}
	    </style>
	</head>
</html>
<body class="bg-white">
	<div class="container-fluid">
		<form action="{{ route('termsandcond.save') }}" method="POST" enctype="multipart/form-data">
			@csrf
			<div class="col-lg-12">
				<h2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u><b>Terms & Conditions for Client Registration</b></u></h2><br>
				<h5><b>Dear,<br>
				Client Name: <input acknowledgement  name="acknowledgement" required></b></h5>
				<input type="hidden" name="lead_id" value="{{ $lead->id }}">
				<div class="row" style="margin-top: 10px">
		            <div class="col-sm-12 header">
		                <div class="checkbox-inline"><label><b>SAFETY, SECURITY & HYGINE</b></label></div>
		            </div>
		            <div class="col-sm-12"><b>We will share background verification and document details of Caregivers working with you and at the same time we will require you one address proof of current residence submited to us.<br> We request you to do bag check of Care Assistant at entrance and exit from your home.<br>We suggest you to put CCTV in yoyr house for better monitoring.<br>We strictly request <u>NOT to keep any jewelry or hign value item with patient</u> and incase of any theft company will not be responsible for it.</br> In case of any search incient we request you to lodge police complaint on immediate basis and we will provide all require support for the same.</br> In case of any issues we request not to send back any female Care Assistant back from duty during night shift for security reasons.<br> Client need to arrange for diapers, medicines, gloves, hand sanitizer, tissue papers and other necessary medical and surgical items as per individual needs in timely manner, Care Assistant will inform you about stoke situation.</br></b></p>
		            </div>
				</div>
				<div class="row" style="margin-top: 10px">
		            <div class="col-sm-12 header">
		                <div class="checkbox-inline"><label><b>DO's</b></label></div>
		            </div>
		            <div class="col-sm-12"><b>Do treat our Care Assistants fairly with dignity to get the best out of them for your family.<br>Please provide tea and breakfast to our Day Care Assistant and tea for Night Care Assistant.</br>You will need to provide accommodation and food for 24Hr stay in service by one care giver.<br>Ask for Id cards and uniform to the Care Assistant.</br>We request you to keep randomly checking the activity log book and attendance sheet maintain by the Care Assistant, If you find any issues then please inform the Service Managers through email/call.</b></p>
		            </div>
				</div>
				<div class="row" style="margin-top: 10px">
		            <div class="col-sm-12 header">
		                <div class="checkbox-inline"><label><b>DON'T's</b></label></div>
		            </div>
		            <div class="col-sm-12"><b>Please do not perform any finacial transactions with our Care Assistant without informing the Aaji Care management hence will not be responsible for the same. </br> Do Not offer privite duty to our Care Assistants to avoid complications.</b></p>
		            </div>
				</div>
				<div class="row" style="margin-top: 10px">
		            <div class="col-sm-12 header">
		                <div class="checkbox-inline"><label><b>BILLING & DISCONTINUATION OF SERVICE</b></label></div>
		            </div>
		            <div class="col-sm-12"><b>If you want to stop service any time please provide us 3 day notice period else it will be added to your bill amount.<br>This rule will be also applicable to us incase we want to stop service for any reason.<br>Aaji Care or Client as right to stop service on immediate basis in case of any sort of abuse identified from either party.<br>Invoice will sent by 1st of every month and we requiest you to do payment with in 5 days of receiving the invoice else there will 500Rs fine charged per month.<br><br><h5>I acknowledge that i have read and agree to above Terms & Conditions.</h5></b></p>
		            </div>
				</div>
				<div class="row">
			       	<div class="col-md-6  ">
			           <div class="card">
			               <div class="card-header">
			                   <h5>Client Signature Pad</h5>
			               </div>
			               <div class="card-body">
		                        <div class="col-md-12">
		                            <label class="" for="">Client Signature:</label>
		                            <br/>
		                            <div id="sig" ></div>
		                            <br/><br>
		                            <button id="clear" class="btn btn-danger btn-sm">Clear Signature</button>
		                            <textarea id="signature64" name="signed" style="display: none"></textarea>
		                        </div>
			               </div>
			           </div>
			       </div>
			   </div>
			<button type="submit" onclick="javascript: return validator();" class="registerbtn">SAVE</button>
			</div>
		</form>
	</div>
	<script type="text/javascript">
		var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
	   	$('#clear').click(function(e) {
	        e.preventDefault();
	        sig.signature('clear');
	        $("#signature64").val('');
	    });

	  	$('input[type=checkbox]').each(function(i){
            if(!$(this).hasClass('block-sel')){
                $(this).attr('value',$(this).parent().text().trim());
            }
        });

        $('input, select, textarea').each(function(){
            if(!$(this).hasClass('block-sel')){
                block = $(this).closest('.row').find('.block-sel').attr('id');
                if(typeof block != 'undefined'){
                    $(this).attr('name',block+'_'+$(this).attr('name'));
                }
            }
        });

        $(document).on('click','.block-sel', function(){
            $('.'+$(this).attr('id')).toggle();
        });

        function validator() {
		  
	        var acknowledgement = $('#acknowledgement').val();
	        var signature64 = $('#signature64').val();
		    
		    if(!acknowledgement){  
		        alert("Client name is needed");
		        return false;  
    		}

		    if(!signature64){  
		        alert("Signature is needed");
		        return false;  
    		}

	        return true;
	    }
	</script>
</body>
