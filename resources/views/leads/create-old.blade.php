@extends('layouts.main-layout')

@section('page_title',isset($l)?'Edit Lead - Leads |':'New Lead - Leads')

@section('active_cases','active')

@section('plugin.styles')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.css">
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<!-- Bootstrap Tagsinput Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
<!-- Intl Tel Input Css -->
<link href="{{ ViewHelper::ThemePlugin('intl-tel-input/intlTelInput.css') }}" rel="stylesheet">

<link href="{{ ViewHelper::ThemePlugin('jquery-ui/jquery-ui.css') }}" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('jquery-ui/jquery-ui.structure.css') }}" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('jquery-ui/jquery-ui.theme.css') }}" rel="stylesheet" />
<!-- Multiple Dates Picker Css-->
<link href="{{ ViewHelper::ThemePlugin('multiple-dates-picker/jquery-ui.multidatespicker.css') }}" rel="stylesheet" />

<link rel="stylesheet" href="https://clin-table-search.lhc.nlm.nih.gov/autocomplete-lhc-versions/13.0.2/autocomplete-lhc_jQueryUI.min.css"/>
@endsection

@section('page.styles')
<style>
.autocomp_selected {
	width: 100%;
}

.bootstrap-datetimepicker-widget table td.disabled{
	color: red !important;
}
.bootstrap-tagsinput, .bootstrap-tagsinput>input{
    width: 100%;
}
div.savestatus{ /* Style for the "Saving Form Contents" DIV that is shown at the top of the form */
	width:200px;
	padding:2px 5px;
	border:1px solid gray;
	background:#fff6e5;
	-webkit-box-shadow: 0 0 8px #818181;
	box-shadow: 0 0 8px #818181;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	border-radius:5px;
	color:red;
	position: fixed;
	z-index: 1;
	top: 50%;
	left: 42%;
}
</style>
<style>
	.card .header .header-dropdown{
		top: 15px;
	}
	hr{
		margin-top: 10px;
		margin-bottom: 10px;
	}
	.bootstrap-tagsinput{
		min-height: 60px;
		vertical-align: top;
		padding: 4px 0;
	}
	.bootstrap-tagsinput input{
		padding-left: 0;
	}
	.plus:after{
		content: ' + ';
		font-size: 18px;
		font-weight: bold;
	}
	.care-plan-card .header{
		padding: 15px 20px;
	}
	.modal-lg{
		width: 70%;
	}
	.modal .modal-header{
		padding: 20px 25px 15px 25px;
	}
	.modal-footer{
		border-top: 1px solid #ddd !important;
	}

	.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control, .bootstrap-select.btn-group.disabled, .bootstrap-select.btn-group > .disabled{
		background: #eee !important;
	}

	/* Map Styles */
	#infowindow-content .title {
		font-weight: bold;
	}

	#infowindow-content {
		display: none;
	}

	#map #infowindow-content {
		display: inline;
	}

	.pac-card {
		margin: 10px 10px 0 0;
		border-radius: 2px 0 0 2px;
		box-sizing: border-box;
		-moz-box-sizing: border-box;
		outline: none;
		box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
		background-color: #fff;
		font-family: Roboto;
	}

	#pac-container {
		padding-bottom: 12px;
		margin-right: 12px;
	}

	.pac-controls {
		display: inline-block;
		padding: 5px 11px;
	}

	.pac-controls label {
		font-family: Roboto;
		font-size: 13px;
		font-weight: 300;
	}

	#pac-input {
		background-color: #fff;
		font-family: Roboto;
		font-size: 15px;
		font-weight: 300;
		margin-left: 12px;
		padding: 0 11px 0 13px;
		text-overflow: ellipsis;
		width: 400px;
	}

	#pac-input:focus {
		border-color: #4d90fe;
	}

	#title {
		color: #fff;
		background-color: #4d90fe;
		font-size: 20px;
		font-weight: 500;
		padding: 6px 12px;
	}
</style>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="row clearfix">
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif

<div class="row">
<div class="savestatus" style="visibility:hidden"></div>
	<form id="caseForm" action="{{ route('lead.save-lead') }}" method="POST">
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{ isset($l)?Helper::encryptor('encrypt',$l->id):0 }}" />
		<input type="hidden" name="patient_id" value="{{ isset($l)?Helper::encryptor('encrypt',$l->patient_id):0 }}" />
		<input type="hidden" name="mode" value="{{ Helper::encryptor('encrypt','PL') }}" />
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<a href="{{ route('lead.index') }}" class="pull-left btn btn-info btn-circle clearLocalStore waves-effect waves-circle waves-float">
						<i class="material-icons">arrow_back</i>
					</a>
					<h2 class="col-sm-8">
						{{ isset($l)?'Edit':'New'}} Lead
						<small>Lead {{ isset($l)?'Updation':'Creation'}} Form</small>
					</h2>
					<div class="col-sm-2">
						<a class="btn btn-info btnShowSearchForm" style="float: right;"><i class="material-icons">search</i> Search Patient</a>
					</div>
					<div class="btn-group" role="group">
						<button type="button" class="btn btn-success m-l-15 waves-effect"><i class="material-icons" style="color: #fff">save</i></button>
						<button type="submit" class="btn btn-success waves-effect clearLocalStore" style="line-height: 1.9;padding-left:0">Save Lead</button>
					</div>&nbsp;&nbsp;
					<div class="clearfix"></div>
				</div>
				<div class="body" style="padding-top: 0">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active">
							<a href="#patient_details_tab" data-toggle="tab" aria-expanded="false">
								<i class="material-icons">person</i> Patient &amp; Enquirer Details
							</a>
						</li>
						<li role="presentation" class="">
							<a href="#requirement_tab" data-toggle="tab" aria-expanded="true">
								<i class="material-icons">child_friendly</i> Requirement Details
							</a>
						</li>
						<li role="presentation" class="">
							<a href="#status_tab" data-toggle="tab" aria-expanded="true">
								<i class="material-icons">done_all</i> Status
							</a>
						</li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane fade active in" id="patient_details_tab">
							<div class="row clearfix">
								<div class="col-sm-6 form-horizontal">
									<h2 class="card-inside-title">
										Basic Details
										<small>Name, Gender, Age, Weight</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
											<label for="first_name">First Name</label>
										</div>
										<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
											<div class="form-group form-float">
												<div class="form-line">
													<input type="text" pattern="[A-Za-z-]+" id="first_name" name="first_name" class="form-control searchCol" placeholder="Ashok" value="{{ isset($l->patient)?$l->patient->first_name:'' }}" title="Only Characters Allowed" required>
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<div class="form-line">
													<input type="text" pattern="[A-Za-z-]+" id="last_name" name="last_name" class="form-control searchCol" placeholder="Kumar" value="{{ isset($l->patient)?$l->patient->last_name:'' }}" title="Only Characters Allowed">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="date_of_birth">Date of Birth</label>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="date_of_birth" name="date_of_birth" class="form-control bdate" placeholder="20-03-1992" value="{{ (isset($l->patient) && $l->patient != null && isset($l->patient->date_of_birth))?$l->patient->date_of_birth->format('d-m-Y'):'' }}">
												</div>
											</div>
										</div>
										<div class="col-sm-1" style="line-height: 3"><b> or </b></div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<div class="form-line">
													<input type="number" min="24" id="patient_age" name="patient_age" class="form-control" placeholder="25" value="{{ (isset($l->patient) && isset($l->patient->patient_age))?$l->patient->patient_age:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="weight">Patient Weight</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="number" min="2" id="patient_weight" name="patient_weight" class="form-control" placeholder="70" value="{{ isset($l->patient->patient_weight)?$l->patient->patient_weight:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
											<label for="gender">Gender</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select class="form-control show-tick" id="gender" name="gender" required>
													<option value="">-- Please select--</option>
													<option value="Male" {{ (isset($l->patient) && $l->patient->gender == 'Male')?'selected':'' }}>Male</option>
													<option value="Female" {{ (isset($l->patient) && $l->patient->gender == 'Female')?'selected':'' }}>Female</option>
													<option value="Other" {{ (isset($l->patient) && $l->patient->gender == 'Other')?'selected':'' }}>Other</option>
												</select>
											</div>
										</div>
									</div>
								</div>

								<div class="col-sm-6 form-horizontal">
									<h2 class="card-inside-title">
										Communication Details
										<small>Enquirer Name, Phone, Email, Relationship</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
											<label for="contact_number">Contact Number</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="contact_number" name="contact_number" onkeyup="checknumber();" class="form-control intl" value="{{ isset($l->patient)?$l->patient->contact_number:'' }}" required>
												</div>
												<span id="number_status" style="font-weight: bold;"></span>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="alternate_number">Alternate Number</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="tel" id="alternate_number" name="alternate_number" class="form-control intl" placeholder="Alternate Number" value="{{ isset($l->patient)?$l->patient->alternate_number:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="email">Email</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="email" name="email" class="form-control searchCol" placeholder="someone@example.com"value="{{ isset($l->patient)?$l->patient->email:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="enquirer_name">Enquirer Name</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="enquirer_name" name="enquirer_name" class="form-control" placeholder="Nitish Kumar" value="{{ isset($l->patient)?$l->patient->enquirer_name:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
											<label for="relationship_with_patient">Relationship with Patient</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<select id="relationship_with_patient" name="relationship_with_patient" class="form-control show-tick" required>
														<option value="">-- Please select --</option>
														<option value="FATHER">FATHER</option>
														<option value="MOTHER">MOTHER</option>
														<option value="BROTHER">BROTHER</option>
														<option value="SISTER">SISTER</option>
														<option value="HUSBAND">HUSBAND</option>
														<option value="WIFE">WIFE</option>
														<option value="DAUGHTER">DAUGHTER</option>
														<option value="SON">SON</option>
														<option value="MOTHER-IN-LAW">MOTHER-IN-LAW</option>
														<option value="FATHER-IN-LAW">FATHER-IN-LAW</option>
														<option value="BROTHER-IN-LAW">BROTHER-IN-LAW</option>
														<option value="GRANDFATHER">GRANDFATHER</option>
														<option value="GRANDMOTHER">GRANDMOTHER</option>
														<option value="UNCLE">UNCLE</option>
														<option value="AUNT">AUNT</option>
														<option value="FRIEND">FRIEND</option>
														<option value="SELF">SELF</option>
														<option value="OTHER">OTHER</option>
													</select>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-sm-5 form-horizontal">
									<h2 class="card-inside-title">
										Patient Address
										<small>Address / Area, City and State</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
											<label for="street_address">Address</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="street_address" name="street_address" class="form-control" placeholder="ApnaCare India Private Limited" value="{{ isset($l->patient)?$l->patient->street_address:'' }}" required >
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="area">Area</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="area" name="area" class="form-control" placeholder="Sahakar Nagar" value="{{ isset($l->patient)?$l->patient->area:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
											<label for="city">City</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="city" name="city" class="form-control" placeholder="Bengaluru" value="{{ isset($l->patient)?$l->patient->city:'' }}" required >
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="zipcode">Zip Code</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="zipcode" name="zipcode" class="form-control" placeholder="560092" value="{{ isset($l->patient)?$l->patient->zipcode:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
											<label for="state">State</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="state" name="state" class="form-control" placeholder="Karnataka" value="{{ isset($l->patient)?$l->patient->state:'' }}" required >
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
											<label for="country">Country</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="country" name="country" class="form-control" placeholder="India" value="{{ isset($l->patient)?$l->patient->country:'' }}" required >
												</div>
											</div>
										</div>
									</div>
									<h2 class="card-inside-title">
										Location Co-ordinates
										<small>Lattitude and Longitude</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="latitude">Latitude</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="latitude" name="latitude" class="form-control" placeholder="Latitude" value="{{ isset($l->patient)?$l->patient->latitude:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="longitude">Longitude</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="longitude" name="longitude" class="form-control" placeholder="Longitude" value="{{ isset($l->patient)?$l->patient->longitude:'' }}">
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="col-sm-7 form-horizontal">
									<h2 class="card-inside-title">
										Location Map
									</h2>
									<div class="pac-card" id="pac-card">
										<div>
											<div id="title">
												Location search
											</div>
											<div id="type-selector" class="pac-controls">
												<input type="radio" name="type" id="changetype-all" checked="checked">
												<label for="changetype-all">All</label>

												<input type="radio" name="type" id="changetype-establishment">
												<label for="changetype-establishment">Establishments</label>

												<input type="radio" name="type" id="changetype-address">
												<label for="changetype-address">Addresses</label>

												<input type="radio" name="type" id="changetype-geocode">
												<label for="changetype-geocode">Geocodes</label>
											</div>
											<div id="strict-bounds-selector" class="pac-controls">
												<input type="checkbox" id="use-strict-bounds" value="">
												<label for="use-strict-bounds">Strict Bounds</label>
											</div>
										</div>
										<div id="pac-container">
											<input id="pac-input" type="text"
											placeholder="Enter a location">
										</div>
									</div>
									<div id="map" style="width: 98%; height:500px;"></div>
									<div id="infowindow-content">
										<img src="" width="16" height="16" id="place-icon">
										<span id="place-name"  class="title"></span><br>
										<span id="place-address"></span>
									</div>
									{{-- <div id="map-canvas" style="width: 98%; height:500px;"></div> --}}
								</div>
							</div>
						</div>

						<div role="tabpanel" class="tab-pane fade" id="requirement_tab">
							<div class="row clearfix form-horizontal">
								<div class="col-sm-6">
									<h2 class="card-inside-title">
										Case Requirement
										<small>Notes, Medical Conditions, Service Type, Special Instructions</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="case_description">Case Description</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<textarea id="case_description" name="case_description" class="form-control" placeholder="Describe the patient's condition">{{ isset($l)?$l->case_description:'' }}</textarea>
												</div>
											</div>
										</div>
									</div>
									<div style="background: #b6dbf9; padding:10px;border-radius: 4px">
										<div style="text-align: center;"><b>Medical History of Patient</b></div>
										<hr>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="medical_conditions">Medical Conditons <br>/ Current Diagnosis</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group">
														<input type="text" id="conditions" name="conditions" placeholder="Type here to search.(e.g. Abdominal Pain,Oxygen Toxicity)">
														<small>Please enter unlisted entries manually below.</small>
														<textarea id="medical_conditions" name="medical_conditions" class="form-control" >{{ isset($l)?$l->medical_conditions:'' }}</textarea>
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="medications">Medications</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group">
														<input type="text" id="medications_list" name="medications_list" placeholder="Type here to search.(e.g. Aspirin,Clotrimazole)">
														<small>Please enter unlisted entries manually below.</small>
														<textarea id="medications" name="medications" class="form-control">{{ isset($l)?$l->medications:'' }}</textarea>
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="procedures">Surgeries/Procedures</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group">
														<input type="text" id="procedures_list" name="procedures_list" placeholder="Type here to search.(e.g. Open Heart Surgery,Knee Arthoplasty)">
														<small>Please enter unlisted entries manually below.</small>
														<textarea id="procedures" name="procedures" class="form-control">{{ isset($l)?$l->surgeries:'' }}</textarea>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="hospital_name">Hospital Name</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="hospital_name" name="hospital_name" class="form-control" placeholder="Apollo, Bangalore" value="{{ isset($l)?$l->hospital_name:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="primary_doctor_name">Primary Doctor Name</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="primary_doctor_name" name="primary_doctor_name" class="form-control" placeholder="Dr. Sunil Awasthi (Critical Care)" value="{{ isset($l)?$l->primary_doctor_name:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div style="background: #f8c8c8; padding:10px;border-radius: 4px">
										<h2 class="card-inside-title">
											Special Instructions
											<small>Any Important details about patient for staff</small>
										</h2>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="special_instructions"></label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group">
													<div class="form-line">
														<textarea rows="3" id="special_instructions" name="special_instructions" class="form-control" placeholder="Referral from CM's office" style="padding: 5px;">{{ isset($l)?$l->special_instructions:'' }}</textarea>
													</div>
												</div>
											</div>
										</div>
									</div>

									<h2 class="card-inside-title">
										Service Details
										<small>Service Type, Duration, Preference, Care Taker</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
											<label for="service_category">Service Category</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select class="form-control show-tick"  data-live-search="true" id="service_category" name="service_category" required>
													<option value="">-- Please select --</option>
													@if(isset($servicecategories) && count($servicecategories))
													@foreach($servicecategories as $scat)
													<option value="{{ $scat->id }}" @if(isset($l) && $l->service_category == $scat->id){{ 'selected' }}@endif>{{ $scat->category_name }}</option>
													@endforeach
													@endif
												</select>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
											<label for="service_required">Service Required</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select class="form-control show-tick"  data-live-search="true" id="service_required" name="service_required" required>
													<option value="">-- Please select --</option>
													@if(isset($services) && count($services))
													@foreach($services as $s)
													<option value="{{ $s->id }}" @if(isset($l) && $l->service_required == $s->id){{ 'selected' }}@endif>{{ $s->service_name }}</option>
													@endforeach
													@endif
												</select>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="estimated_duration">Estimated Duration</label>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
											<div class="form-group">
												<div class="form-line">
													<input type="number" min="1" id="estimated_duration" name="estimated_duration" class="form-control" placeholder="15" value="{{ isset($l)?$l->estimated_duration:'' }}">
												</div>
											</div>
										</div>
										<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="line-height: 3; text-align: left">
											<b>day(s)</b>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="gender_preference">Gender Preference</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group" style="margin-top: 1%">
												<input name="gender_preference" type="radio" id="gender_preference_any" class="with-gap radio-col-deep-purple" value="Any" checked />
												<label for="gender_preference_any">Any</label>
												<input name="gender_preference" type="radio" id="gender_preference_male" class="with-gap radio-col-deep-purple" value="Male" {{ (isset($l) && $l->gender_preference == 'Male')?'checked':'' }}/>
												<label for="gender_preference_male">Male</label>
												<input name="gender_preference" type="radio" id="gender_preference_female" class="with-gap radio-col-deep-purple" value="Female" {{ (isset($l) && $l->gender_preference == 'Female')?'checked':'' }}/>
												<label for="gender_preference_female">Female</label>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="language_preference[]">Language Preference</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group" style="margin-top: 2.5%">
												<div class="demo-checkbox language-preference">
													@if(isset($languages) && count($languages))
													@foreach($languages as $language)
													<input type="checkbox" id="languages_{{ strtolower($language->language_name) }}" name="language_preference[]" class="filled-in chk-col-light-blue" value="{{ $language->language_name }}" {{ (isset($l) && in_array($language->language_name, explode(",",$l->language_preference)))?'checked':'' }} />
													<label for="languages_{{ strtolower($language->language_name) }}">{{ $language->language_name }}</label>
													@endforeach
													@endif
												</div>
											</div>
										</div>
									</div>
									@include('partials.branch-input')
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="manager_id">Manager</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select class="form-control show-tick"  data-live-search="true" id="manager_id" name="manager_id">
													<option value="0">-- Please select --</option>
													@if(isset($managers) && count($managers))
													@foreach($managers as $manager)
													<option value="{{ $manager->id }}" @if(isset($l) && $l->manager_id == $manager->id){{ 'selected' }}@endif>{{ $manager->full_name }}</option>
													@endforeach
													@endif
												</select>
											</div>
										</div>
									</div>
								</div>

								<div class="col-sm-6">
									<h2 class="card-inside-title">
										Assessment Schedule
										<small>Assessment Date and Time, Notes</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
											<label for="assessment_required">Assessment Required</label>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
											<div class="form-group">
												<div class="switch" style="margin-top:5px;">
													<label>No<input type="checkbox" id="assessment_required" checked=""><span class="lever switch-col-green"></span>Yes</label>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
											<label for="assessment_date">Date</label>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="assessment_date" name="assessment_date" class="form-control date" placeholder="19-07-2017" value="{{ isset($l)?Carbon\Carbon::parse($l->assessment_date)->format('d-m-Y'):'' }}">
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="assessment_time" name="assessment_time" class="form-control time" placeholder="09:00 AM" value="{{ isset($l)?Carbon\Carbon::parse($l->assessment_time)->format('h:i A'):'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
											<label for="assessment_notes">Assessment Notes</label>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
											<div class="form-group">
												<div class="form-line">
													<textarea id="assessment_notes" name="assessment_notes" class="form-control" placeholder="Pre Assessment Notes">{{ isset($l)?$l->assessment_notes:'' }}</textarea>
												</div>
											</div>
										</div>
									</div>

									<h2 class="card-inside-title">
										Payment Details
										<small>Rate agreed, Advance, Payment Mode, Notes</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
											<label for="rate_agreed">Rate Agreed</label>
										</div>
										<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="line-height: 3;font-weight: normal">
											Rs.
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
											<div class="form-group">
												<div class="form-line">
													<input type="number" min="0" id="rate_agreed" name="rate_agreed" class="form-control" placeholder="800" value="{{ isset($l)?$l->rate_agreed:'' }}" required>
												</div>
											</div>
										</div>
										<label> /- per day</label>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="advance_amount">Advance Amount</label>
										</div>
										<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="line-height: 3;font-weight: normal">
											Rs.
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
											<div class="form-group">
												<div class="form-line">
													<input type="number" min="0" id="advance_amount" name="advance_amount" class="form-control" placeholder="1600" value="{{ isset($l)?$l->advance_amount:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
											<label for="payment_mode">Payment Mode</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select class="form-control show-tick" id="payment_mode" name="payment_mode" required>
													<option value="">-- Please select --</option>
													<option value="Cash" {{ (isset($l) && $l->payment_mode == 'Cash')?'selected':'' }}>Cash</option>
													<option value="Credit Card" {{ (isset($l) && $l->payment_mode == 'Credit Card')?'selected':'' }}>Credit Card</option>
													<option value="Cheque/DD" {{ (isset($l) && $l->payment_mode == 'Cheque/DD')?'selected':'' }}>Cheque/DD</option>
													<option value="Online/NEFT" {{ (isset($l) && $l->payment_mode == 'Online/NEFT')?'selected':'' }}>Online/NEFT</option>
													<option value="Paytm" {{ (isset($l) && $l->payment_mode == 'Paytm')?'selected':'' }}>Paytm</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="payment_notes">Payment Notes</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<textarea id="payment_notes" name="payment_notes" class="form-control" placeholder="Advance amount to be collected prior service.">{{ isset($l)?$l->payment_notes:'' }}</textarea>
												</div>
											</div>
										</div>
									</div>

									<h2 class="card-inside-title">
										Referral Details
										<small>Referrer Category, Referral Source, Referrer Name</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="referral_category">Category</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select class="form-control show-tick" data-live-search="true" id="referral_category" name="referral_category">
													<option value="">--Select--</option>
													@if(isset($categories))
													@foreach ($categories as $cat)
													<option value="{{ $cat->id }}" {{ (isset($l) && $l->referral_category == $cat->id)?'selected':'' }}>{{ $cat->category_name }}</option>
													@endforeach
													@endif
												</select>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="referral_source">Source</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select class="form-control show-tick" data-live-search="true" id="referral_source" name="referral_source" disabled="">
													<option value="">-- Select Category First --</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="referrer_name">Referrer Name (If any)</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="referrer_name" name="referrer_name" class="form-control referrer-name" placeholder="Suhas PR" value="{{ isset($l)?$l->referrer_name:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="converted_by">Converted By</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<select id="converted_by" name="converted_by" class="form-control show-tick" data-live-search="true">
														<option value="">-- Select Staff --</option>
												@if(isset($staffs) && count($staffs))
													@foreach ($staffs as $staff)
														<option value="{{ $staff->id }}" {{ (isset($l) && $l->converted_by == $staff->id)?'selected':'' }}>{{ $staff->full_name.' - '.$staff->employee_id }}</option>
													@endforeach
												@endif
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="referral_amount_applicable">Referral amount applicable</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="switch" style="margin-top:5px;">
													<label>No<input type="checkbox" id="referral_amount_applicable"><span class="lever switch-col-green"></span>Yes</label>
												</div>
											</div>
										</div>
									</div>
									<input type="hidden" name="referral_value" id="referral_value" value="{{isset($l->referral_value)?$l->referral_value:'0'}}">
									<input type="hidden" name="referral_type" id="referral_type" value="{{isset($l->referral_type)?$l->referral_type:'0'}}">
								</div>
							</div>
							<div class="clearfix"></div>
						</div>

						<div role="tabpanel" class="tab-pane fade" id="status_tab">
							<div class="row clearfix form-horizontal">
								<div class="col-sm-6">
									<br>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="status">Status</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<select class="form-control show-tick" id="status" name="status">
														<option value="Pending">Pending</option>
														<option value="Converted">Converted</option>
														<option value="Dropped">Dropped</option>
														<option value="Follow Up">Follow Up</option>
														<option value="Assessment Pending">Assessment to be done</option>
														<option value="Assessment Completed">Assessment Completed</option>
														<option value="Closed">Closed</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="conversionStatusBlock hide" style="display:none;">
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="service_offered">Service Offered</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group">
													<div class="form-line">
														<select class="form-control show-tick"  data-live-search="true" id="service_offered" name="service_offered">
															<option value="0">-- Please select --</option>
															@if(isset($services) && count($services))
															@foreach($services as $s)
															<option value="{{ $s->id }}">{{ $s->service_name }}</option>
															@endforeach
															@endif
														</select>
													</div>
												</div>
											</div>
											<div class="row clearfix">
												<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
													<label for="current_rack_rate">Current Rack Rate</label>
												</div>
												<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
													<div class="form-group">
														<div class="form-line">
															<input type="text" class="form-control" id="current_rack_rate" name="current_rack_rate">
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="row clearfix droppedReasonBlock" style="display:none">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="status">Dropped Reason</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<br>
												<input type="checkbox" id="reason_1" name="dropped_reason[]" value="Phone ringing no response" class="filled-in chk-col-amber" {{ isset($dropped_reason)?(in_array('Phone ringing no response',$dropped_reason))?'checked':'':''}}>
												<label for="reason_1">Phone ringing no response</label><br>
												<input type="checkbox" id="reason_2" name="dropped_reason[]" value="PCA Not available" class="filled-in chk-col-amber" {{ isset($dropped_reason)?(in_array('PCA Not available',$dropped_reason))?'checked':'':''}}>
												<label for="reason_2">PCA Not available</label><br>
												<input type="checkbox" id="reason_3" name="dropped_reason[]" value="RN not available" class="filled-in chk-col-amber" {{ isset($dropped_reason)?(in_array('RN not available',$dropped_reason))?'checked':'':''}}>
												<label for="reason_3">RN not available</label><br>
												<input type="checkbox" id="reason_4" name="dropped_reason[]" value="Distance out of reach" class="filled-in chk-col-amber" {{ isset($dropped_reason)?(in_array('Distance out of reach',$dropped_reason))?'checked':'':''}}>
												<label for="reason_4">Distance out of reach</label><br>
												<input type="checkbox" id="reason_5" name="dropped_reason[]" value="Clinically not competent" class="filled-in chk-col-amber" {{ isset($dropped_reason)?(in_array('Clinically not competent',$dropped_reason))?'checked':'':''}}>
												<label for="reason_5">Clinically not competent</label><br>
												<input type="checkbox" id="reason_oth" name="dropped_reason[]" value="Other" class="filled-in chk-col-amber" {{ isset($dropped_reason)?(in_array('Other',$dropped_reason))?'checked':'':''}}>
												<label for="reason_oth">Other</label><br>
												<div class="form-line">
													<textarea class="form-control" id="dropped_reason_other" name="dropped_reason_other" placeholder="If Other, Please specify" disabled="">{{isset($l->dropped_reason_other)?$l->dropped_reason_other:''}}</textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>

<!-- Patient Search by Phone Number -->
<div class="modal fade" id="patientSearchModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<form method="POST">
			<div class="modal-content">
				<div class="modal-header bg-blue">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="largeModalLabel">Search Patient</h4>
				</div>
				<div class="modal-body">
					<div class="row clearfix">
						<div class="col-sm-3">
							<label class="">Search By</label>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
							<div class="form-group" style="margin-top: 1%">
								<input name="search_by" type="radio" id="search_by_id" class="with-gap radio-col-deep-purple" value="ID" checked="">
								<label for="search_by_id">Patient ID</label>
								<input name="search_by" type="radio" id="search_by_mobile" class="with-gap radio-col-deep-purple" value="Mobile">
								<label for="search_by_mobile">Mobile Number</label>
								<input name="search_by" type="radio" id="search_by_email" class="with-gap radio-col-deep-purple" value="Email">
								<label for="search_by_email">Email</label>
							</div>
						</div>
					</div>
					<div class="row clearfix">
						<div class="col-sm-3">
							<label class="">Search Text</label>
						</div>
						<div class="col-sm-5">
							<div class="form-group">
								<div class="form-line">
									<input type="text" class="form-control" id="search_text" autocomplete="off" placeholder="Search text here"/>
								</div>
							</div>
						</div>
						<div class="col-sm-2">
							<a class="btn bg-cyan waves-effect btnSearchPatient">Search</a>
						</div>
					</div>
					<div class="patientSearchResult" style="display:none">
						<hr>
						<table class="table table-bordered patientsResultTable">
							<thead>
								<tr>
									<th>ID</th>
									<th>Name</th>
									<th>Gender</th>
									<th>Age</th>
									<th>Contact Number</th>
									<th>Enquirer</th>
									<th>Location</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-success waves-effect pull-right btnSelectPatient">Select</button>
				</div>
			</div>
		</form>
	</div>
</div>

@endsection

@section('plugin.scripts')
<!-- Select Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<!-- Moment Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<!-- Bootstrap Tags Input Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('bootstrap-3-typeahead/bootstrap3-typeahead.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('intl-tel-input/intlTelInput.js') }}"></script>

<script src="{{ ViewHelper::ThemePlugin('jquery-ui/jquery-ui.js') }}"></script>
<!-- Multiple Dates Picker -->
<script src="{{ ViewHelper::ThemePlugin('multiple-dates-picker/jquery-ui.multidatespicker.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.js"></script>
<script src="https://clin-table-search.lhc.nlm.nih.gov/autocomplete-lhc-versions/13.0.2/autocomplete-lhc_jQueryUI.min.js"></script>
@endsection

@section('page.scripts')
	<!-- Save As You Type Plugin Js -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>

	<script src="{{ ViewHelper::ThemeJs('sayt.min.jquery.js') }}"></script>
	<script type="text/javascript">
		$(function(){
			$('#caseForm').sayt();
			var conditionsArray = [];
			new Def.Autocompleter.Search('conditions', conditionsArray, {maxSelect: '*'});
			var searchConditions = new Def.Autocompleter.Search('conditions','https://clin-table-search.lhc.nlm.nih.gov/api/conditions/v3/search');

			var medicationsArray = [];
			new Def.Autocompleter.Search('medications_list', medicationsArray, {maxSelect: '*'});
			var searchMedications = new Def.Autocompleter.Search('medications_list','https://clin-table-search.lhc.nlm.nih.gov/api/rxterms/v3/search?ef=STRENGTHS_AND_FORMS');

			var proceduresArray = [];
			new Def.Autocompleter.Search('procedures_list', proceduresArray, {maxSelect: '*'});
			var searchProcedures = new Def.Autocompleter.Search('procedures_list','https://clin-table-search.lhc.nlm.nih.gov/api/procedures/v3/search');

			searchConditions.onMouseDownListener = function(d){
				$('#medical_conditions').tagsinput('add',d.currentTarget.innerHTML);
			}

			searchMedications.onMouseDownListener = function(d){
				$('#medications').tagsinput('add',d.currentTarget.innerHTML);
			}

			searchProcedures.onMouseDownListener = function(d){
				$('#procedures').tagsinput('add',d.currentTarget.innerHTML);
			}

			$('.autocomp_selected ul').hide();


			$('.content').on('click','.clearLocalStore', function() {
				$('#caseForm').sayt({'erase': true});
			    return localStorage.removeItem('leadFormLastTab');
			});

			// for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
		    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		        // save the latest tab; use cookies if you like 'em better:
		        localStorage.setItem('leadFormLastTab', $(this).attr('href'));
		    });

		    // go to the latest tab, if it exists:
		    var lastTab = localStorage.getItem('leadFormLastTab');
		    if (lastTab) {
		        $('[href="' + lastTab + '"]').tab('show');
		    }

			$('select').selectpicker('refresh');
		});
	</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBFVRxoHdxhkIqVPiLUPe2kGDJWRmZgIS4&libraries=places&region=IN"></script>
<script>
	$(function(){
		@if(isset($l))
		$('input[name=id]').val('{{ Helper::encryptor('encrypt',$l->id) }}');
		$('input[name=patient_id]').val('{{ Helper::encryptor('encrypt',$l->patient_id) }}');
		@endif

		initMap();

		$('#medical_conditions').tagsinput({
			typeahead: {
				minLength: 0,
				showHintOnFocus:"all",
				allowDuplicates: true,
			}
		});

		$('#medications').tagsinput({
			typeahead: {
				minLength: 0,
				showHintOnFocus:"all",
				allowDuplicates: true,
			}
		});

		$('#procedures').tagsinput({
			typeahead: {
				minLength: 0,
				showHintOnFocus:"all",
				allowDuplicates: true,
			}
		});

		$('.form-line .bootstrap-tagsinput').click(function() {
			$(".bootstrap-tagsinput input").val('');
			$(".bootstrap-tagsinput input").trigger('keyup');
		});

		$('.content').on('change','#assessment_required',function(){
			if(!$(this).is(':checked')){
				$('#assessment_date').prop('disabled',true);
				$('#assessment_time').prop('disabled',true);
				$('#assessment_notes').prop('disabled',true);
			}else{
				$('#assessment_date').prop('disabled',false);
				$('#assessment_time').prop('disabled',false);
				$('#assessment_notes').prop('disabled',false);
			}
		});

		$('.content').on('change','#referral_amount_applicable',function(){
			if(!$(this).is(':checked')){
				$('#referral_amount').prop('disabled',true);
				$('#referral_amount').selectpicker('refresh');
			}else{
				$('#referral_amount').prop('disabled',false);
				$('#referral_amount').selectpicker('refresh');
			}
		});

		$('.content').on('change','#status',function(){
			if($(this).val() == 'Dropped'){
				$('.droppedReasonBlock').show();
			}else{
				$('.droppedReasonBlock').hide();
			}

			if($(this).val() == 'Converted'){
				$('.conversionStatusBlock').show();
			}else{
				$('.conversionStatusBlock').hide();
			}
		});

		$('.content').on('change','#reason_oth',function(){
			if($(this).is(':checked')){
				$('#dropped_reason_other').prop('disabled',false);
			}else{
				$('#dropped_reason_other').val('');
				$('#dropped_reason_other').prop('disabled',true);
			}
		});
	});

	var componentForm = {
		sublocality_level_2: 'short_name',
		street_number: 'short_name',
		route: 'long_name',
		locality: 'long_name',
		administrative_area_level_1: 'long_name',
		country: 'long_name',
		postal_code: 'short_name'
	};

	var componentIds = {
		sublocality_level_2: 'area',
		locality: 'city',
		administrative_area_level_1: 'state',
		country: 'country',
		postal_code: 'zipcode'
	};

	function handleLocationError(browserHasGeolocation, infoWindow, pos) {
	    // infoWindow.setPosition(pos);
	    // infoWindow.setContent(browserHasGeolocation ?
	    //                       'Error: The Geolocation service failed.' :
	    //                       'Error: Your browser doesn\'t support geolocation.');
	}

	function initMap() {
		var map = new google.maps.Map(document.getElementById('map'), {
			center: {lat: 16.28004385258969, lng: 78.10594640625004},
			zoom: 6
		});
		var card = document.getElementById('pac-card');
		var input = document.getElementById('pac-input');
		var types = document.getElementById('type-selector');
		var strictBounds = document.getElementById('strict-bounds-selector');

		map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

		var autocomplete = new google.maps.places.Autocomplete(input);

		// Bind the map's bounds (viewport) property to the autocomplete object,
		// so that the autocomplete requests use the current map bounds for the
		// bounds option in the request.
		autocomplete.bindTo('bounds', map);

		var infowindow = new google.maps.InfoWindow();
		var infowindowContent = document.getElementById('infowindow-content');
		infowindow.setContent(infowindowContent);
		var marker = new google.maps.Marker({
			map: map,
			animation: google.maps.Animation.DROP,
			draggable: true,
			anchorPoint: new google.maps.Point(0, -29)
		});

		// Try HTML5 geolocation.
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
				var pos = {
					lat: position.coords.latitude,
					lng: position.coords.longitude
				};
				map.setCenter(pos);
			}, function() {
				handleLocationError(true, infowindow, map.getCenter());
			});
		} else {
		    // Browser doesn't support Geolocation
		    handleLocationError(false, infowindow, map.getCenter());
		}

		google.maps.event.addListener(
			marker,
			'drag',
			function(event) {
				infowindow.close();
				document.getElementById('latitude').value = this.position.lat();
				document.getElementById('longitude').value = this.position.lng();
		        //alert('drag');
		    });


		google.maps.event.addListener(marker,'dragend',function(event) {
			infowindow.close();
			document.getElementById('latitude').value = this.position.lat();
			document.getElementById('longitude').value = this.position.lng();
		        //alert('Drag end');
		    });

		autocomplete.addListener('place_changed', function() {
			infowindow.close();
			marker.setVisible(false);
			var place = autocomplete.getPlace();
			if (!place.geometry) {
				// User entered the name of a Place that was not suggested and
				// pressed the Enter key, or the Place Details request failed.
				window.alert("No details available for input: '" + place.name + "'");
				return;
			}

			// If the place has a geometry, then present it on a map.
			if (place.geometry.viewport) {
				map.fitBounds(place.geometry.viewport);
			} else {
				map.setCenter(place.geometry.location);
				map.setZoom(17);  // Why 17? Because it looks good.
			}
			marker.setPosition(place.geometry.location);
			marker.setVisible(true);

			// Update Location Co-ordinates Values
			$('#latitude').val(place.geometry.location.lat());
			$('#longitude').val(place.geometry.location.lng());

			var address = '';
			if (place.address_components) {
				address = [
				(place.address_components[0] && place.address_components[0].short_name || ''),
				(place.address_components[1] && place.address_components[1].short_name || ''),
				(place.address_components[2] && place.address_components[2].short_name || '')
				].join(' ');

				// Get each component of the address from the place details
		        // and fill the corresponding field on the form.
		        for (var i = 0; i < place.address_components.length; i++) {
		        	var addressType = place.address_components[i].types[0];
		        	if (componentForm[addressType]) {
		        		var val = place.address_components[i][componentForm[addressType]];
		        		if(typeof componentIds[addressType] != 'undefined'){
		        			document.getElementById(componentIds[addressType]).value = val;
		        		}
		        	}
		        }
		    }

		    infowindowContent.children['place-icon'].src = place.icon;
		    infowindowContent.children['place-name'].textContent = place.name;
		    infowindowContent.children['place-address'].textContent = address;
		    infowindow.open(map, marker);
		});

		// Sets a listener on a radio button to change the filter type on Places
		// Autocomplete.
		function setupClickListener(id, types) {
			var radioButton = document.getElementById(id);
			radioButton.addEventListener('click', function() {
				autocomplete.setTypes(types);
			});
		}

		setupClickListener('changetype-all', []);
		setupClickListener('changetype-address', ['address']);
		setupClickListener('changetype-establishment', ['establishment']);
		setupClickListener('changetype-geocode', ['geocode']);

		document.getElementById('use-strict-bounds')
		.addEventListener('click', function() {
			console.log('Checkbox clicked! New state=' + this.checked);
			autocomplete.setOptions({strictBounds: this.checked});
		});
	}
</script>
<script>
	$(function(){
		initBlock();

		$('.content').on('change','#referral_category', function(){
			var val = $(this).val();
			var sourcesHtml = '<option value="">-- Select --</option>';
			var sources = sourcesList.filter(function(item){ return item.key === val; });

			if(sources.length > 0){
				for(key in sources){
					if(sources.hasOwnProperty(key)){
						sourcesHtml += '<option value="'+sources[key].id+'">'+sources[key].value+' - '+ sources[key].phone+'</option>';
					}
				}
				$('#referral_source').html(sourcesHtml);
				$('#referral_source').prop('disabled',false);
				$('#referral_source').selectpicker('refresh');
			}
		});

		var sourcesList = [];
		var percentagesList = [];
		@if(isset($sources))
		@foreach ($sources as $s)
		sourcesList.push({id: '{{ $s->id }}', key: '{{ $s->category_id }}', value:'{{ $s->source_name }}', phone: '{{ $s->phone_number }}', percentage: '{{ $s->referral_value }}'});
		percentagesList.push({id: '{{ $s->id }}', percentage: '{{ $s->referral_value }}', type: '{{ $s->charge_type }}' });
		@endforeach
		@endif

		$('.content').on('change','#referral_source', function(){
			var val = $(this).val();
			var result = $.grep(percentagesList, function(e){ return e.id == val; });
			$('#referral_value').val(result[0].percentage);
			$('#referral_type').val(result[0].type);
		});
	});

	function initBlock()
	{
		$('.bdate').bootstrapMaterialDatePicker({
			format: 'DD-MM-YYYY',
			time: false,
			clearButton: true,
			maxDate : new Date(),
		});
		var holidayListRev = ["<?php echo ($holidaysr); ?>"];
		var date =new Date();

		$('.date').datetimepicker({
			format: 'DD-MM-YYYY',
			disabledDates: holidayListRev,
			daysOfWeekDisabled:[0],
			showClose: true,
			showClear: true,
			minDate: date,
			useCurrent: false,
			keepInvalid:true
		});
		$('.time').bootstrapMaterialDatePicker({
			format: 'hh:mm A',
			time: true,
			date:false,
			clearButton: true,
			shortTime: true,
		});
		$(".intl").intlTelInput({
			initialCountry: "IN",
			utilsScript: "{{ ViewHelper::ThemePlugin('intl-tel-input/utils.js') }}",
			nationalMode : false,
			preferredCountries :["in","us"]
		});
	}

		$('.content').on('change', '#date_of_birth', function(){
			var dob = moment($('#date_of_birth').val(),'DD-MM-YYYY').format('YYYY-MM-DD');
			var years = moment().diff(dob, 'years');

			$('#patient_age').val(years);
		});

		$('.content').on('click', '.btnShowSearchForm', function(){
			$('#patientSearchModal input[name=search_by][value=ID]').prop('checked',true);
			$('#patientSearchModal #search_text').val('');
			$('#patientSearchModal .patientSearchResult').hide();
			$('#patientSearchModal .patientsResultTable tbody').html('<tr><td colspan="8" class="text-center">No record(s) found!</th></tr>');

			$('#patientSearchModal').modal();
		});

		$('.content').on('click', '.btnSearchPatient', function(){
			$('#patientSearchModal .patientSearchResult').hide();
			searchCol = $('#patientSearchModal input[name=search_by]:checked').val();
			searchText = $('#patientSearchModal #search_text').val();

			if(searchCol && searchText){
				searchPatient(searchCol, searchText);
			}else {
				alert("Please enter search text!");
				return;
			}
		});

		function searchPatient(searchCol, searchText)
		{
			if(searchText != ''){
				$.ajax({
					url: '{{ route('lead.search-patient') }}',
					type: 'POST',
					data: {c: searchCol, t: searchText, _token: '{{ csrf_token() }}'},
					success: function (data){
						html = ``;
						if(data.length){
							$(data).each(function(i){
								html += `<tr>
								<td>`+ data[i].patient_id +`</td>
								<td>`+ data[i].first_name + ` ` + data[i].last_name +`</td>
								<td>`+ data[i].gender +`</td>
								<td>`+ data[i].patient_age +`</td>
								<td>`+ data[i].contact_number +`</td>
								<td>`+ data[i].enquirer_name +`</td>
								<td>`+ data[i].area + `,` + data[i].city + `<br>` + data[i].state +`</td>
								<td class="text-center"><input name="select_patient" type="radio" id="select_patient_`+data[i].id+`" data-patient='`+JSON.stringify(data[i]).replace(/'/g, "\\'")+`' class="with-gap radio-col-deep-purple" value="`+data[i].id+`">
									<label for="select_patient_`+data[i].id+`""></label></td>
								</tr>`;
							});
						}else{
							html += `<tr><td colspan="8" class="text-center">No record(s) found!</th></tr>`;
						}

						$('#patientSearchModal .patientsResultTable tbody').html(html);
						$('#patientSearchModal .patientSearchResult').show();
					},
					error: function (error){
						console.log(error);
					}
				});
			}
		}

		$('.content').on('click', '.btnSelectPatient', function(){
			var selRow = $('#patientSearchModal input[name=select_patient]:checked');
			var patient = selRow.data('patient');

			if(selRow.val() && typeof selRow.val() != undefined){
				// Fill Patient Details
				$('#first_name').val(patient.first_name);
				$('#last_name').val(patient.last_name);
				$('#contact_number').val(patient.contact_number);
				$('#email').val(patient.email);
				if(patient.date_of_birth){
					dob = moment(patient.date_of_birth,'YYYY-MM-DD hh:mm:ss')
					$('#date_of_birth').val(dob.format('DD-MM-YYYY'));
				}
				$('#patient_age').val(patient.patient_age);
				$('#patient_weight').val(patient.patient_weight);
				$('#gender').selectpicker('val',patient.gender);
				$('#gender').selectpicker('refresh');

				$('#address').val(patient.street_address);
				$('#area').val(patient.area);
				$('#city').val(patient.city);
				$('#zipcode').val(patient.zipcode);
				$('#state').val(patient.state);
				$('#country').val(patient.country);
				$('#latitude').val(patient.latitude);
				$('#longitude').val(patient.longitude);

				$('#enquirer_name').val(patient.enquirer_name);
				$('#alternate_number').val(patient.alternate_number);
				$('#relationship_with_patient').selectpicker('val',patient.relationship_with_patient);
				$('#relationship_with_patient').selectpicker('refresh');

				$('input[name=patient_id]').val(patient.id);

				$('#patientSearchModal').modal('hide');
			}else{
				alert("Please select patient record!");
				return;
			}

		});

		var visitListArray = [];

		function saveCase()
		{
			// Parse Patient Details
			var patientDetails = [];
			var fname = $('#first_name').val();
			var lname = $('#last_name').val();
			var mobile = $('#contact_number').val();
			var email = $('#email').val();
			var dob = $('#date_of_birth').val();
			var gender = $('#gender option:selected').val();

			var address = $('#address').val();
			var city = $('#city').val();
			var zip = $('#zipcode').val();
			var state = $('#state').val();
			var country = $('#country').val();

			// Parse Enquirer Details
			var enquirerName = $('#enquirer_name').val();
			var enquirerPhone = $('#enquirer_phone').val();
			var enquirerEmail = $('#enquirer_email').val();
			var relationship = $('#relationship_with_patient option:selected').val();

			patientDetails = {
				'first_name': fname,
				'last_name': lname,
				'mobile_number': mobile,
				'email': email,
				'date_of_birth': dob,
				'gender': gender,
				'address': address,
				'city': city,
				'zipcode': zip,
				'state': state,
				'country': country,
				'enquirer_name': enquirerName,
				'enquirer_phone': enquirerPhone,
				'enquirer_email': enquirerEmail,
				'relationship_with_patient': relationship
			};

			// Parse Care Plan Details
			var carePlanDetails = [];
			var careplanName = $('#careplan_name').val();
			var careplanDescription = $('#careplan_description').val();
			var medicalConditions = $('#medical_conditions').val();
			var medications = $('#medications').val();
			var procedures = $('#procedures').val();

			var serviceRequired = $('#service_id option:selected').val();
			var noOfHours = $('#no_of_hours').val();
			var genderPreference = $('input[name=gender_preference]:checked').val();
			var languagePreference = [];
			$(".language-preference input[type=checkbox]:checked").map(function(){
				languagePreference.push($(this).val());
			});
			var source = $('#source option:selected').val();
			var referrerName = $('#referrer_name').val();

			carePlanDetails = {
				'careplan_name': careplanName,
				'careplan_description': careplanDescription,
				'medical_conditions': medicalConditions,
				'medications': medications,
				'procedures': procedures,
				'service_id': serviceRequired,
				'no_of_hours': noOfHours,
				'gender_preference': genderPreference,
				'language_preference': languagePreference.toString(),
				'source': source,
				'referrer_name': referrerName
			};


			if(patientDetails && carePlanDetails && visitListArray){
				$.ajax({
					url: '{{ route('case.save-case') }}',
					type: 'POST',
					data: {patient: patientDetails, careplan: carePlanDetails, visits: visitListArray},
					success: function (data){
						console.log(data);
						window.location.href = '{{ route('case.index') }}';
					},
					error: function (error){
						console.log(error);
					}
				});
			}

			return false;
		}

		$(function(){
			@if(isset($l))
			$('#relationship_with_patient').selectpicker('val','{{ $l->patient->relationship_with_patient }}');
			$('#relationship_with_patient').selectpicker('refresh');

			$('#referral_category').selectpicker('val','{{ $l->referral_category }}');
			$('#referral_category').selectpicker('refresh');
			$('#referral_category').trigger("change");

			$('#referral_source').selectpicker('val','{{ $l->referral_source }}');
			$('#referral_source').selectpicker('refresh');

			$('#assessment_required').prop('checked','{{ (!empty($l->assessment_date) || $l->assessment_date != null)?true:false }}').trigger("change");

			@if($l->medical_conditions)
			$('#medical_conditions').removeAttr('placeholder');
			@foreach(explode(",",$l->medical_conditions) as $mc)
			$('#medical_conditions').tagsinput('add','{{ $mc }}');
			@endforeach
			@endif

			@if($l->medications)
			$('#medications').removeAttr('placeholder');
			@foreach(explode(",",$l->medications) as $m)
			$('#medications').tagsinput('add','{{ $m }}');
			@endforeach
			@endif

			@if($l->procedures)
			$('#procedures').removeAttr('placeholder');
			@foreach(explode(",",$l->procedures) as $p)
			$('#procedures').tagsinput('add','{{ $p }}');
			@endforeach
			@endif

			$('#status').selectpicker('val','{{ $l->status }}');
			$('#status').selectpicker('refresh');
			$('#status').trigger("change");
			@endif
		});
		@if(!isset($l))
		function checknumber(){
			var number=document.getElementById("contact_number").value;
			//number = number.replace(/^\+[0-9]{2}?|^[0]|\s+/g,''); // remove ^+, ^91,^0 and spaces
			//Removes dialCode, + sign , spaces ,and initial 0 if exists
			number = number.replace($('#contact_number').intlTelInput("getSelectedCountryData").dialCode,'');
			number = number.replace(/^\+?|\s+/g,'');
			number = number.replace(/^0?/,'');
			if(number.length >= 10){
				$.ajax({
					type: 'post',
					url: '{{ route('patient.checknumber') }}',
					data: { patient_number:number, _token: '{{ csrf_token() }}'},
					success: function (response){
						if(response=="New Patient Entry. Please Continue."){
							$( '#number_status' ).html(response);
							$('#number_status').css('color','green');

							return true;
						}else if(response == 'Patient Already Exists'){
							$('#number_status').css('color','red');
							$('#search_by_mobile').prop('checked','checked');
							$('#search_text').val(number);
							$('.btnSearchPatient').trigger('click');
							setTimeout(function(){
							 $( '#number_status' ).html('Patient record exists. <a onclick="$(\'#patientSearchModal\').modal(\'show\');">View</a>');
							}, 2000);

							return false;
						}
					}
				});
			}else{
				$( '#number_status' ).html("");
				return false;
			}
		}
		@endif
	</script>
	@endsection
