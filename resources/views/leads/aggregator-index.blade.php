@extends('layouts.main-layout')

@section('page_title','Leads - ')

@section('active_leads','active')

@section('page.styles')
<style>
.plus:after{
    content: ' + ';
    font-size: 18px;
    font-weight: bold;
}
</style>
@endsection

@section('plugin.styles')
<style>
.info-box .icon{
    width: 80px !important;
}
.info-box .content{
    padding: 0px 10px !important;
}
.tab-nav-right > li > a{
    min-width: 180px !important;
    text-align: center;
}
.nav-tabs > li{
    margin-bottom:2px !important;
}
.nav-tabs > li.active{
    border-left: 1px solid #aabcfe;
    border-right: 1px solid #aabcfe;
    border-top: 4px solid #aabcfe;
    margin-bottom: 2px;
}
.nav-tabs > li.active > a{
    font-weight: bold;
    color: #2196F3 !important;
}
.nav-tabs > li > a:before{
    border-bottom: 0 !important;
}
table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td {
    line-height: 14px;
}
</style>
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection

@section('side-menu')
    @include('menus.leads')
@endsection

@section('content')
    <div class="block-header">
        <h2 class="col-sm-2">Leads</h2>
        @ability('admin','leads-create')
        <div class="col-sm-1" style="float: right;">
            <a href="{{ route('lead.create') }}" class="btn btn-primary waves-effect">Create Lead</a>
        </div>
        <div class="col-sm-2" style="float: right;">
            <button type="button" data-toggle="modal" data-target="#distributionsModal" class="btn btn-primary waves-effect distributions">View Distributions</a>
        </div>
        @endability
        <div class="clearfix"></div>
    </div>

    <div class="row clearfix" style="margin-bottom: 30px;">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-pink hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">people</i>
                </div>
                <div class="content">
                    <div class="text">New Leads</div>
                    <div class="number count-to">{{ $requests->count() }}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-cyan hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">receipt</i>
                </div>
                <div class="content">
                    <div class="text">Pending <small>(Apnacare | SP)</div>
                    <div class="number count-to"><small>{{ count($leads->whereIn('status',['Pending','Follow Up','Assessment Pending','Assessment Completed'])) }} | {{ count($cases->whereIn('status',['Pending','Accepted','Declined'])) }}</small></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-light-green hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">list</i>
                </div>
                <div class="content">
                    <div class="text">Converted <small>(Aggregator | Provider)</small></div>
                    <div class="number count-to">{{ count($leads->where('status','Converted')) }} | {{ count($cases->where('status','Converted')) }}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-orange hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">account_balance_wallet</i>
                </div>
                <div class="content">
                    <div class="text">Closed <small>(Aggregator | Provider)</div>
                    <div class="number count-to">{{ count($leads->whereIn('status',['Closed','Dropped'])) }} | {{ count($cases->whereIn('status',['Closed','Dropped']))  }}</div>
                </div>
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="card">
            <div class="body">
                <div class="filter-div" style="margin-top: 10px;">
                    <b>Filter by Status : &nbsp;&nbsp;</b>
                    <input type="checkbox" name="filter_status" id="filter_newleadrequest" value="New Lead Request" class="filled-in chk-col-blue filter">
                    <label for="filter_newleadrequest">New Lead Request</label>
                    <input type="checkbox" name="filter_status" id="filter_pendingwithsp" value="Pending With SP" class="filled-in chk-col-indigo filter">
                    <label for="filter_pendingwithsp">Pending With SP</label>
                    <input type="checkbox" name="filter_status" id="filter_pendingwithapnacare" value="Pending with Apnacare" class="filled-in chk-col-green filter">
                    <label for="filter_pendingwithapnacare">Pending with Apnacare</label>
                    <input type="checkbox" name="filter_status" id="filter_convertedbyprovider" value="Converted by Provider" class="filled-in chk-col-orange filter">
                    <label for="filter_convertedbyprovider">Converted by Provider</label>
                    <input type="checkbox" name="filter_status" id="filter_convertedbyapnacare" value="Converted by Apnacare" class="filled-in chk-col-red filter">
                    <label for="filter_convertedbyapnacare">Converted by Apnacare</label>
                    <input type="checkbox" name="filter_status" id="filter_closedbyprovider" value="Closed by Provider" class="filled-in chk-col-red filter">
                    <label for="filter_closedbyprovider">Closed by Provider</label>
                    <input type="checkbox" name="filter_status" id="filter_closedbyaggregator" value="Closed by Aggregator" class="filled-in chk-col-red filter">
                    <label for="filter_closedbyaggregator">Closed by Aggregator</label>
                </div>
                <div class="clearfix"></div><br>
                <!-- Simplified Tabs -->
                <table class="table table-bordered table-hover table-condensed table-striped" id="leadsTable" style="zoom:83%;white-space: nowrap;">
                    <thead>
                        <tr>
                            <th width="5%">Code Status</th>
                            <th width="10%">MCM</th>
                            <th width="15%">Provider</th>
                            <th width="5%">Source</th>
                            <th width="15%">Name</th>
                            <th width="5%">Phone</th>
                            <th width="5%">Email</th>
                            <th width="5%">Location</th>
                            <th width="10%">Service Required</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($requests->count())
                            @foreach($requests as $i => $r)
                                <tr data-id="{{ Helper::encryptor('encrypt',$r->id) }}">
                                    <td>
                                        <span class="label bg-green">New Lead Request</span><br><br>
                                        {{ $r->created_at->format('d-m-Y') }}
                                        <small>{{ $r->created_at->format('h:i A') }}</small>
                                    </td>
                                    <td> - </td>
                                    <td> - </td>
                                    <td>{{ $r->source ?? '' }}</td>
                                    <td>{{ $r->name ?? '' }}</td>
                                    <td>{{ $r->phone_number ?? '' }}</td>
                                    <td>{{ $r->email ?? '-' }}</td>
                                    <td>{{ $r->city ?? '-' }}</td>
                                    <td>{{ isset($r->requirement)?\Helper::getServiceName($r->requirement):'' }}</td>
                                    <td>{{ $r->status }}</td>
                                    <td>
                                        <a href="javascript:void(0);" class="btn btn-md btn-primary btnLeadType" data-id="{{ Helper::encryptor('encrypt',$r->id) }}" title="Create Case"><i class="material-icons">control_point</i></a>
                                        <a href="javascript: void(0);" data-id="{{ Helper::encryptor('encrypt',$r->id) }}" class="btn btn-md btn-warning markLeadAsDuplicate" title="Mark as Duplicate"><i class="material-icons">filter_2</i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        @if(isset($cases) && count($cases->whereIn('status',['Pending','Follow Up','Accepted','Declined','Assessment Pending','Assessment Completed'])))
                            @foreach($cases->whereIn('status',['Pending','Follow Up','Accepted','Declined','Assessment Pending','Assessment Completed']) as $i => $c)
                                <tr data-id="{{ Helper::encryptor('encrypt',$c->provider_id) }}">
                                    <td>
                                        <span class="label bg-deep-orange">Pending with SP</span><br><br>
                                        {{ $c->created_at->format('d-m-Y') }}
                                        <small>{{ $c->created_at->format('h:i A') }}</small>
                                    </td>
                                    <td>{{ $c->aggregator_manager }}</td>
                                    <td>
                                        <span title="{{ $c->provider_name }}">{{ $c->provider_name }}</span><br>
                                        <small>{{ $c->branch }}</small>
                                    </td>
                                    <td> - </td>
                                    <td>
                                        {{ $c->patient_name }}<br>
                                        {!! $c->patient_enquirer?'<small>Enquirer: '.$c->patient_enquirer.'</small>':'' !!}
                                    </td>
                                    <td>
                                        {{ $c->patient_phone }}<br>
                                        {!! $c->patient_alternate_number?'<small>Enquirer: '.$c->patient_alternate_number.'</small>':'' !!}
                                    </td>
                                    <td>
                                        {{ $c->patient_email ?? '-' }}
                                    </td>
                                    <td>
                                        {{ $c->patient_city ?? '-' }}<br>
                                        <small>{{ $c->patient_area ?? '-' }}</small>
                                    </td>
                                    <td> {{ $c->aggregator_service ?? '-' }} </td>
                                    <td>{{ $c->status ?? '-' }}</td>
                                    <td>
                                        @if($c->status == 'Declined')
                                            <a style="width: 84%;margin-bottom: 15px;" href="{{ route('case.edit',['id' => Helper::encryptor('encrypt',$c->provider_id), 'lead_id' => Helper::encryptor('encrypt',$c->lead_id)]) }}" class="btn btn-md bg-deep-orange" data-id="{{ Helper::encryptor('encrypt',$c->id) }}" title="Create Lead">Change SP</a>
                                        @elseif($c->status == 'Pending')
                                            <a style="margin-top: 15px;" onclick="return confirm('Are you sure to retract the Lead?')" href="{{ route('lead.aggregator-retractLead',['lead_id' => Helper::encryptor('encrypt',$c->lead_id), 'patient_id' => Helper::encryptor('encrypt',$c->patient_id), 'provider_id' => Helper::encryptor('encrypt',$c->provider_id),]) }}" class="btn btn-md bg-deep-orange" data-id="{{ Helper::encryptor('encrypt',$c->id) }}" title="Retract Lead">Retract Lead</a>
                                            <a style="margin-top: 15px;" href="{{ route('case.view',['id' => Helper::encryptor('encrypt',$c->provider_id), 'lead_id' => Helper::encryptor('encrypt',$c->lead_id)]) }}" class="btn btn-md bg-deep-orange float-right" data-id="{{ Helper::encryptor('encrypt',$c->provider_id) }}" title="View Lead">View Comments</a>
                                        @else
                                            <a href="{{ route('case.view',['id' => Helper::encryptor('encrypt',$c->provider_id), 'lead_id' => Helper::encryptor('encrypt',$c->lead_id)]) }}" class="btn btn-md bg-deep-orange float-right" data-id="{{ Helper::encryptor('encrypt',$c->provider_id) }}" title="View Lead">View Lead</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        @if(count($leads->whereIn('status',['Pending','Follow Up','Assessment Pending','Assessment Completed'])))
                            @foreach($leads->whereIn('status',['Pending','Follow Up','Assessment Pending','Assessment Completed']) as $i => $l)
                            <tr data-id="{{ Helper::encryptor('encrypt',$l->id) }}">
                                <td>
                                    <span class="label bg-orange">Pending with Apnacare</span><br><br>
                                    {{ $l->created_at->format('d-m-Y') }}
                                    <small>{{ $l->created_at->format('h:i A') }}</small>
                                </td>
                                <td>{{ $l->aggregatorManager->full_name }}</td>
                                <td>Apnacare</td>
                                <td>{{ $l->source ?? '' }}</td>
                                <td>
                                    {{ isset($l->patient)?$l->patient->first_name.' '.$l->patient->last_name:'-' }}<br>
                                    {!! (isset($l->patient) && $l->patient->enquirer_name)?'<small>Enquirer: '.$l->patient->enquirer_name.'</small>':'-' !!}
                                </td>
                                <td>
                                    {{ isset($l->patient)?$l->patient->contact_number:'-' }}<br>
                                    {!! (isset($l->patient) && $l->patient->alternate_number)?'<small>Enquirer: '.$l->patient->alternate_number.'</small>':'-' !!}
                                </td>
                                <td>{{ isset($l->patient)?$l->patient->email:'-' }}</td>
                                <td>{{ isset($l->patient)?$l->patient->city:'-' }}</td>
                                <td>{{ Helper::getServiceName($l->service_required) }}</td>
                                <td>{{ $l->status ?? '-' }}</td>
                                <td>
                                    @ability('admin', 'leads-edit')
                                    <a style="margin-bottom: 15px;" href="{{ route('case.edit',['id' => Helper::encryptor('encrypt',$l->tenant_id), 'lead_id' => Helper::encryptor('encrypt',$l->id)]) }}" class="btn btn-md bg-orange" }}" title="Change Provider">Change SP</a>
                                    @endability

                                    @ability('admin', 'leads-edit')
                                    <a href="{{ route('lead.view',Helper::encryptor('encrypt',$l->id)) }}" class="btn btn-md bg-orange" data-id="{{ Helper::encryptor('encrypt',$l->id) }}" title="View Lead">View Lead</a>
                                    @endability
                                </td>
                            </tr>
                            @endforeach
                        @endif
                        @if(isset($cases) && count($cases->where('status','Converted')))
                            @foreach($cases->where('status','Converted') as $i => $c)
                                <tr data-id="{{ Helper::encryptor('encrypt',$c->provider_id) }}">
                                    <td>
                                        <span class="label bg-green">Converted by Provider</span><br><br>
                                        {{ $c->created_at->format('d-m-Y') }}
                                        <small>{{ $c->created_at->format('h:i A') }}</small>
                                    </td>
                                    <td>{{ $c->aggregator_manager }}</td>
                                    <td>
                                        <span title="{{ $c->provider_name }}">{{ $c->provider_name }}</span><br>
                                        <small>{{ $c->branch }}</small>
                                    </td>
                                    <td> - </td>
                                    <td>
                                        {{ $c->patient_name }}<br>
                                        {!! $c->patient_enquirer?'<small>Enquirer: '.$c->patient_enquirer.'</small>':'' !!}
                                    </td>
                                    <td>
                                        {{ $c->patient_phone }}<br>
                                        {!! $c->patient_alternate_number?'<small>Enquirer: '.$c->patient_alternate_number.'</small>':'' !!}
                                    </td>
                                    <td>
                                        {{ $c->patient_email ?? '-' }}
                                    </td>
                                    <td>
                                        {{ $c->patient_city ?? '-' }}<br>
                                        <small>{{ $c->patient_area ?? '-' }}</small>
                                    </td>
                                    <td> {{ $c->aggregator_service ?? '-' }} </td>
                                    <td>{{ $c->status ?? '-' }}</td>
                                    <td>
                                        @ability('admin', 'cases-view')
                                        <a href="{{ route('case.view',['id' => Helper::encryptor('encrypt',$c->provider_id), 'lead_id' => Helper::encryptor('encrypt',$c->lead_id)]) }}" class="btn btn-md bg-green" data-id="{{ Helper::encryptor('encrypt',$c->provider_id) }}" title="View Lead">View Lead</a>
                                        <a style="margin-top: 15px;" onclick="return confirm('Are you sure to retract the Lead?')" href="{{ route('lead.aggregator-retractLead',['lead_id' => Helper::encryptor('encrypt',$c->lead_id), 'patient_id' => Helper::encryptor('encrypt',$c->patient_id), 'provider_id' => Helper::encryptor('encrypt',$c->provider_id),]) }}" class="btn btn-md bg-deep-orange" data-id="{{ Helper::encryptor('encrypt',$c->id) }}" title="Retract Lead">Retract Lead</a>
                                        @endability
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        @if(isset($leads) && count($leads->where('status','Converted')))
                            @foreach($leads->where('status','Converted') as $i => $l)
                                <tr data-id="{{ Helper::encryptor('encrypt',$l->id) }}">
                                    <td>
                                        <span class="label bg-light-green">Converted by Apnacare</span><br><br>
                                        {{ $l->created_at->format('d-m-Y') }}
                                        <small>{{ $l->created_at->format('h:i A') }}</small>
                                    </td>
                                    <td>{{ $l->aggregatorManager->full_name }}</td>
                                    <td>Apnacare</td>
                                    <td>{{ $l->source }}</td>
                                    <td>{{ $l->patient->first_name.' '.$l->patient->last_name }}</td>
                                    <td>{{ $l->patient->contact_number ?? '' }}</td>
                                    <td>{{ $l->patient->email ?? '-' }}</td>
                                    <td>{{ $l->patient->city ?? '-' }}</td>
                                    <td>{{ Helper::getServiceName($l->service_required) }}</td>
                                    <td>{{ $l->status ?? '-' }}</td>
                                    <td>
                                        @ability('admin', 'leads-edit')
                                            <a href="{{ route('lead.view',Helper::encryptor('encrypt',$l->id)) }}" class="btn btn-md bg-light-green" data-id="{{ Helper::encryptor('encrypt',$l->id) }}" title="View Lead">View Lead</a>
                                        @endability
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        @if(isset($cases) && count($cases->whereIn('status',['Closed','Dropped'])))
                            @foreach($cases->whereIn('status',['Closed','Dropped']) as $i => $c)
                                <tr data-id="{{ Helper::encryptor('encrypt',$c->provider_id) }}">
                                    <td>
                                        <span class="label bg-red">Closed by Provider</span><br><br>
                                        {{ $c->created_at->format('d-m-Y') }}
                                        <small>{{ $c->created_at->format('h:i A') }}</small>
                                    </td>
                                    <td>{{ $c->aggregator_manager }}</td>
                                    <td>
                                        <span title="{{ $c->provider_name }}">{{ $c->provider_name }}</span><br>
                                        <small>{{ $c->branch }}</small>
                                    </td>
                                    <td> - </td>
                                    <td>
                                        {{ $c->patient_name }}<br>
                                        {!! $c->patient_enquirer?'<small>Enquirer: '.$c->patient_enquirer.'</small>':'' !!}
                                    </td>
                                    <td>
                                        {{ $c->patient_phone }}<br>
                                        {!! $c->patient_alternate_number?'<small>Enquirer: '.$c->patient_alternate_number.'</small>':'' !!}
                                    </td>
                                    <td>
                                        {{ $c->patient_email ?? '-' }}
                                    </td>
                                    <td>
                                        {{ $c->patient_city ?? '-' }}<br>
                                        <small>{{ $c->patient_area ?? '-' }}</small>
                                    </td>
                                    <td> {{ $c->aggregator_service ?? '-' }} </td>
                                    <td>{{ $c->status ?? '-' }}</td>
                                    <td>
                                        @ability('admin', 'cases-view')
                                        <a href="{{ route('case.view',['id' => Helper::encryptor('encrypt',$c->provider_id), 'lead_id' => Helper::encryptor('encrypt',$c->lead_id)]) }}" class="btn btn-md bg-red" data-id="{{ Helper::encryptor('encrypt',$c->provider_id) }}" title="View Lead">View Lead</a>
                                        @endability
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        @if(isset($leads) && count($leads->whereIn('status',['Closed','Dropped'])))
                            @foreach($leads->whereIn('status',['Closed','Dropped']) as $i => $c)
                                <tr>
                                    <td>
                                        <span class="label bg-pink">Closed by Aggregator</span><br><br>
                                        {{ $c->created_at->format('d-m-Y') }}
                                        <small>{{ $c->created_at->format('h:i A') }}</small>
                                    </td>
                                    <td>{{ $c->aggregatorManager->full_name }}</td>
                                    <td>Apnacare</td>
                                    <td>{{ $c->source }}</td>
                                    <td>
                                        {{ $c->patient->full_name }}<br>
                                        {!! $c->patient->enquirer_name?'<small>Enquirer: '.$c->patient->enquirer_name.'</small>':'' !!}
                                    </td>
                                    <td>
                                        {{ $c->patient->contact_number }}<br>
                                        {!! $c->patient->alternate_number?'<small>Enquirer: '.$c->patient->alternate_number.'</small>':'' !!}
                                    </td>
                                    <td>
                                        {{ $c->patient->email ?? '-' }}
                                    </td>
                                    <td>
                                        {{ $c->patient->city ?? '-' }}<br>
                                        <small>{{ $c->patient->area ?? '-' }}</small>
                                    </td>
                                    <td>{{ Helper::getServiceName($l->service_required) }}</td>
                                    <td>{{ $c->status ?? '-' }}</td>
                                    <td>
                                        @ability('admin', 'cases-view')
                                        <a href="{{ route('lead.view',Helper::encryptor('encrypt',$c->id)) }}" class="btn btn-md bg-pink" data-id="{{ Helper::encryptor('encrypt',$c->id) }}" title="View Lead">View Lead</a>
                                        @endability
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="modal fade" id="distributionsModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-col-grey" >
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalLabel">Lead Distribution List</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-condensed table-hover distributionsTable">
                            <thead>
                                <tr>
                                    <th width="3%">#</th>
                                    <th>Provider</th>
                                    <th>Patient</th>
                                    <th>Phone</th>
                                    <th>Patient Email</th>
                                    <th>Location</th>
                                    <th>Requirement</th>
                                    <th>Status</th>
                                    <th width="53px">Status Date</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(isset($distributions) && count($distributions))
                                @foreach($distributions as $i => $d)
                                    <tr @if($d->status == 'Declined') style="background-color: #f99696;" @else style="background-color: #acf5ac;" @endif>
                                        <td>{{ $i + 1 }}</td>
                                        <td>{{ $d->branch_name }}</td>
                                        <td>{{ $d->patient_name }}</td>
                                        <td>{{ $d->patient_number }}</td>
                                        <td>{{ $d->email }}</td>
                                        <td>{{ $d->city }}</td>
                                        <td>{{ $d->requirement }}</td>
                                        <td>{{ $d->status }}</td>
                                        <td>{{ $d->status_date->format('d-m-Y h:i A') }}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="10" class="text-center">
                                        No Distribution(s) found.
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div><br>
                </div>
                <div class="modal-footer" style="background: rgba(0, 0, 0, 0.12) !important">
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="leadTypeModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="col-md-6 text-center">
                        <a class="btn btn-block btn-primary btnOtherRequest" data-id="0">Job / Partnership Request</a>
                    </div>
                    <div class="col-md-6 text-center">
                        <a class="btn btn-block btn-success openMedicalCase" data-id="" target="_blank" data-href="{{ route('lead.create') }}">Medical Case</a>
                    </div>
                    <div class="clearfix"></div><br>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="otherRequestsModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" >
                <form id="otherRequestForm" method="POST" action="{{ route('lead.otherRequest') }}" class="col-sm-12 form-horizontal">
                    <div class="modal-header bg-blue hidden-print">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="largeModalLabel">Other Request Form</h4>
                    </div>
                    <div class="modal-body">
                        <div class="col-sm-12">
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="full_name">Full Name</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="name" placeholder="Ex. Ashok Kumar"/>
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="phone_number">Phone Number</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="phone_number" placeholder="Ex. 9845098450"/>
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="email">Email</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="email" placeholder="Ex. ashok@gmail.com"/>
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="city">City</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="city" placeholder="Ex. Bengaluru"/>
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="requirement">Requirement Type</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="form-group">
                                        <div class="requirement_type">
                                            <input name="requirement_type" type="radio" id="JOB" value="JOB" checked />
                                            <label for="JOB">JOB</label>
                                            <input name="requirement_type" type="radio" id="PARTNERSHIP" value="PARTNERSHIP"/>
                                            <label for="PARTNERSHIP">PARTNERSHIP</label>
                                            <input name="requirement_type" type="radio" id="ENQUIRY" value="ENQUIRY"/>
                                            <label for="ENQUIRY">ENQUIRY</label>
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="requirement">Comment</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea class="form-control" name="requirement" placeholder="Ex. Job Requirement for nurse"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer" style="border-top: 1px solid #ccc !important;">
                        <input type="hidden" id="lead_request_id" name="lead_request_id" value="" />
                        <button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-success" onclick="javascript: return validate();" value="Save"></input>
                        {{ csrf_field() }}
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('page.scripts')
@parent
<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
<script>
    $(function(){
        $('.content').on('click','.clearLocalStore', function() {
            return localStorage.removeItem('aggLeadsIndexLastTab');
        });

        // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
        $('.nav-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
             $('html, body').stop().animate({
                'scrollTop': $('.tab-menu').offset().top-150
            }, 700, 'swing', function () {
                //window.location.hash = $('.tab-menu');
            });
            // save the latest tab; use cookies if you like 'em better:
            localStorage.setItem('aggLeadsIndexLastTab', $(this).attr('href'));
        });

        // go to the latest tab, if it exists:
        var viewLastTab = localStorage.getItem('aggLeadsIndexLastTab');
        if (viewLastTab) {
            $('.nav-tabs [href="' + viewLastTab + '"]').tab('show');
        }

        var table = $('#leadsTable').DataTable({
                "order": [[ 1, "desc" ]]
        });
        
        $('.content').on('click','input[name=filter_status]',function(){
            var searchString = "";
            // Get all checked status
            $('input[name=filter_status]:checked').each(function(i){
                searchString += $(this).val() + "|";
            });

            if(searchString.length > 0 && typeof table != 'undefined'){
                searchString = searchString.substring(0, searchString.length-1);
                table.search(searchString, true, false).draw();
            }else{
                table.search("").draw();
            }
        });
        
        @if(isset($distributions) && count($distributions))
        $('.distributionsTable').DataTable({
            "order": [[ 1, "desc" ]]
        });
        @endif

        $('.content').on('click','.btnLeadType', function(){
            $('#leadTypeModal').modal();
            $('#leadTypeModal .btnOtherRequest').attr('data-id',$(this).data('id'));
            $('#leadTypeModal .openMedicalCase').attr('data-id',$(this).data('id'));
            $('#leadTypeModal .openMedicalCase').attr('href','');
            var url = $('#leadTypeModal .openMedicalCase').data('href');
            url += '?req_id='+$(this).data('id');
            $('#leadTypeModal .openMedicalCase').attr('href',url);
        });

        $('.content').on('click','.btnOtherRequest', function(){
            $('#leadTypeModal').modal('hide');
            id = $(this).attr('data-id');
            block = '#leadsTable tbody tr[data-id='+id+']';

            $('#otherRequestForm input[name=name]').val($(block+' td:eq(5)').html().replace(/<\/?[^>]+(>|$)|\-/g, "").trim());
            $('#otherRequestForm input[name=phone_number]').val($(block+' td:eq(6)').html().replace(/<\/?[^>]+(>|$)|\-/g, "").trim());
            $('#otherRequestForm input[name=email]').val($(block+' td:eq(7)').html().replace(/<\/?[^>]+(>|$)|\-/g, "").trim());
            $('#otherRequestForm input[name=city]').val(removeTags($(block+' td:eq(8)').html().replace(/<\/?[^>]+(>|$)|\-/g, "").trim()));

            $('#otherRequestForm #lead_request_id').val(id);

            $('#otherRequestsModal').modal();
        });

        $('.content').on('click', '.markLeadAsDuplicate', function(){
            id = $(this).data('id');
            if(confirm("Are you sure?")){
                $.ajax({
                    url: '{{ route('lead.markDuplicate') }}',
                    type: 'POST',
                    data: {_token:'{{ csrf_token() }}',id: id},
                    success: function(data){
                        $('#leadsTable tbody tr[data-id='+id+']')
                        .children('td, th')
                        .animate({ padding: 0 })
                        .wrapInner('<div />')
                        .children()
                        .slideUp(function() { $(this).closest('tr').remove(); });
                        alert("Lead Status has been updated");
                    },
                    error: function(err){
                        console.log(err);
                    }
                });
            }
        });
    });

    function removeTags(txt){
        var rex = /(<([^>]+)>)/ig;
        return txt.replace(/\s+/g,' ')
        .replace(/^\s+|\s+$/,'');
    }

</script>
@endsection
