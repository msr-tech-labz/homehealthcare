<html>
	<head>
		<title>Laravel Signature Pad Tutorial Example-nicesnippets.com </title>
    	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.css">
    	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
    	<link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/south-street/jquery-ui.css" rel="stylesheet"> 
    	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    	<script type="text/javascript" src="http://keith-wood.name/js/jquery.signature.js"></script>
    	<link rel="stylesheet" type="text/css" href="http://keith-wood.name/css/jquery.signature.css">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    	<link rel="stylesheet" href="{{ asset('css/assessment.css') }}" />
    	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" crossorigin="anonymous">
    	<style>
	        .kbw-signature { width: 70%; height: 100px;}
	        #sig canvas{
	            width: 100% !important;
	            height: auto;
	        }
	        body {
			  font-family: Arial, Helvetica, sans-serif;
			  background-color: black;
			}

			* {
			  box-sizing: border-box;
			}

			/* Add padding to containers */
			.container-fluid {
			  padding: 16px;
			  background-color: white;
			}

			/* Full-width input fields */
			input[type=text], input[type=password] {
			  width: 100%;
			  padding: 15px;
			  margin: 5px 0 22px 0;
			  display: inline-block;
			  border: none;
			  background: #f1f1f1;
			}

			input[type=text]:focus, input[type=password]:focus {
			  background-color: #ddd;
			  outline: none;
			}

			/* Overwrite default styles of hr */
			hr {
			  border: 1px solid #f1f1f1;
			  margin-bottom: 25px;
			}

			/* Set a style for the submit button */
			.registerbtn {
			  background-color: #4CAF50;
			  color: white;
			  padding: 16px 20px;
			  margin: 8px 0;
			  border: none;
			  cursor: pointer;
			  width: 100%;
			  opacity: 0.9;
			}

			.registerbtn:hover {
			  opacity: 1;
			}

			/* Add a blue text color to links */
			a {
			  color: dodgerblue;
			}

			/* Set a grey background color and center the text of the "sign in" section */
			.signin {
			  background-color: #f1f1f1;
			  text-align: center;
			}
				.content {
			    max-width: 450px;
			    margin: auto;
			}

			.collection .collection-item.avatar {
			    padding: 5px;
			}

			.material-icons {
			    font-size: 50px;
			    color: #26a69a;
			}

			.collapsible-header  {
			    min-height: 4rem;
			    line-height: 4rem;
			    font-size: 0.9rem;
			}

			.collapsible-header  i {
			    width: 3rem;
			    line-height: 4rem;
			    margin-right: -10px;
			}

			.collapsible li.active i {
			  -ms-transform: rotate(180deg); /* IE 9 */
			  -webkit-transform: rotate(180deg); /* Chrome, Safari, Opera */
			  transform: rotate(180deg);
			}


			.collapsible-body {
			    background-color: #d0d0d0;
			}
	    </style>
	</head>
</html>
<body class="bg-white">
	<div class="container-fluid">
		<form action="{{ route('signaturepad.upload') }}" method="POST" enctype="multipart/form-data">
			@csrf
			<div class="col-lg-12">
				<h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u><b>CLIENT REGISTRATION FORM</b></u></h1>
				<div class="col-sm-12 header">
	                <div class="checkbox-inline"><label class="no-margin"><input type="checkbox" class="block-sel" id="block_1" /> CLIENT DETAILS</label></div>
	            </div><br>
				<div class="row block_1" style="display:none">
					<div class="col-lg-4">
					    <label for="email"><b>Client Name</b></label>
					    <input type="text" value="{{ $patient->enquirer_name }}" placeholder="Enter full name" id=client_full_name name="client_full_name" required>
					    <input type="hidden" value="{{ $patient->id }}" name="patient_id" id="patient_id">
					     <input type="hidden" value="{{ $lead->id }}" name="lead_id" id="lead_id">
					    <label for="psw"><b>Contact Number</b></label>
					    <input type="text" value="{{ $patient->contact_number }}" placeholder="Enter contact number" id="contact_number" name="contact_number" required>

					    <label for="psw-repeat"><b>Mobile  Number</b></label>
					    <input type="text" value="{{ $patient->alternate_number }}" placeholder="Enter Alternate number" id="alternate_number" name="alternate_number">

					    <label for="email"><b>Relationship with patient</b></label>
					    <select id="relationship_with_patient" name="relationship_with_patient" class="form-control show-tick" data-live-search="true">
							<option value="">-- Please select --</option>
							<option value="FATHER">FATHER</option>
							<option value="MOTHER">MOTHER</option>
							<option value="BROTHER">BROTHER</option>
							<option value="SISTER">SISTER</option>
							<option value="HUSBAND">HUSBAND</option>
							<option value="WIFE">WIFE</option>
							<option value="DAUGHTER">DAUGHTER</option>
							<option value="SON">SON</option>
							<option value="MOTHER-IN-LAW">MOTHER-IN-LAW</option>
							<option value="FATHER-IN-LAW">FATHER-IN-LAW</option>
							<option value="BROTHER-IN-LAW">BROTHER-IN-LAW</option>
							<option value="GRANDFATHER">GRANDFATHER</option>
							<option value="GRANDMOTHER">GRANDMOTHER</option>
							<option value="UNCLE">UNCLE</option>
							<option value="AUNT">AUNT</option>
							<option value="FRIEND">FRIEND</option>
							<option value="SELF">SELF</option>
							<option value="OTHER">OTHER</option>
						</select>
					</div>
					<div class="col-lg-4">
					    <label for="email"><b>Email Id</b></label>
					    <input type="text" value="{{ $patient->email }}" placeholder="Enter Email" id="email" name="email">

					    <label for="psw"><b>Company/Profession</b></label>
					    <input type="text" placeholder="Enter Company/Profession" name="company">

					    <label for="psw-repeat"><b>Address</b></label>
					    <input type="text" value="{{ $patient->street_address }}"  placeholder="Repeat street address" name="street_address">

					    <label for="email"><b>Area</b></label>
					    <input type="text" value="{{ $patient->area }}"  placeholder="Enter area" name="area">
					</div>
					<div class="col-lg-4">
					    <label for="email"><b>City</b></label>
					    <input type="text" value="{{ $patient->city }}"  placeholder="Enter city" name="city">

					    <label for="psw"><b>Zip code</b></label>
					    <input type="text" value="{{ $patient->zip_code }}"  placeholder="Enter zip code" name="zip_code">

					    <label for="psw-repeat"><b>State/ Country</b></label>
					    <input type="text" value="{{ $patient->state }}"  placeholder="Repeat state" name="state">

					    <label for=""><b>Upload client picture</b></label>
					    <input type="file" placeholder="Enter Email" name="client_picture">
					</div>
				</div>
				<div class="col-sm-12 header">
	                <div class="checkbox-inline"><label class="no-margin"><input type="checkbox" class="block-sel" id="block_2" /> PATIENT DETAILS</label></div>
	            </div><br>
				<div class="row block_2" style="display:none">
					<div class="col-lg-4">
					    <label for="email"><b>Patient Name</b></label>
					    <input type="text" value="{{ $patient->full_name }}" placeholder="Enter patient name" id="patient_full_name" name="patient_full_name" required>
					   
					    <label for="psw"><b>Age</b></label>
					    <input type="text" value="{{ $patient->age }}" placeholder="Enter patient_age" id="patient_age" name="patient_age">

					    <label for="psw-repeat"><b>Gender</b></label>
					    <select id="gender" name="gender" class="form-control show-tick" data-live-search="true">
							<option value="">-- Please select --</option>
							<option value="MALE">MALE</option>
							<option value="FEMALE">FEMALE</option>				
							<option value="OTHER">OTHER</option>
						</select>
					</div>
					<div class="col-lg-4">
					    <label for="email"><b>Mobility</b></label>
					    <input type="text" placeholder="Enter patient mobility" name="mobility">

					    <label for="psw"><b>Existing medical condition</b></label>
					    <input type="text" value="{{ $lead->medical_conditions }}" placeholder="Enter Medical Conditions" name="medical_conditions">

					    <label for="psw-repeat"><b>Family doctor name</b></label>
					    <input type="text" value="{{ $lead->primary_doctor_name }}" name="primary_doctor_name">
					</div>
					<div class="col-lg-4">
					    <label for="email"><b>Dr. Contact number</b></label>
					    <input type="text"  placeholder="Enter Dr. Contact number" id="dr_contact_number" name="dr_contact_number">

					    <label for="psw"><b>Doctor Address</b></label>
					    <input type="text" placeholder="Enter Doctor Address" name="doctor_address">

					    <label for=""><b>Upload patient picture</b></label>
					    <input type="file" id="patient_picture" accept="image/*" name="patient_picture">
					</div>
				</div>
				<div class="col-sm-12 header">
	                <div class="checkbox-inline"><label class="no-margin"><input type="checkbox" class="block-sel" id="block_3" /> REQUIREMENT DETAILS</label></div>
	            </div><br>
				<div class="row block_3" style="display:none">
					<div class="col-lg-6">
					    <label for="email"><b>Expected service time line</b></label>
					    <input type="text" placeholder="Enter Expected service time line" name="expected_time_line">
					   
					    <label for="psw"><b>Tenure</b></label>
					    <input type="text"  placeholder="Tenure" name="tenure">
					</div>
					<div class="col-lg-6">
					    <label for="email"><b>Expected service details</b></label>
					    <input type="text" placeholder="Expected service details" name="expected_service_details">

					    <label for="psw"><b>Advance</b></label>
					    <input type="text" placeholder="Advance" id="advance_amount" name="advance_amount">
					</div>
				</div>
				<div class="row" style="margin-top: 10px">
		            <div class="col-sm-12 header">
		                <div class="checkbox-inline"><label>ACKNOWLEDGEMENT</label></div>
		            </div>
		            <div class="col-sm-12">
		                <p style="padding: 10px;text-align:justify">I, <input  name="acknowledgement" id="acknowledgement" required> hereby agree the acknowledgement that i shall be bound by the terms and conditions of service agreement mentioned at the back of this client registration form and also take full responsibility to make the advance payment towards the services rendered by Aaji Care Home Heealth Service Pvt. Ltd. on for the above-mentioned Patient.</p>
		            </div>
				</div>
				<div class="row">
			       	<div class="col-md-6  ">
			           <div class="card">
			               <div class="card-header">
			                   <h5>Client Signature Pad</h5>
			               </div>
			               <div class="card-body">
		                        <div class="col-md-12">
		                            <label class="" for="">Client Signature:</label>
		                            <br/>
		                            <div id="sig" ></div>
		                            <br/><br>
		                            <button id="clear" class="btn btn-danger btn-sm">Clear Signature</button>
		                            <textarea id="signature64" name="signed" style="display: none"></textarea>
		                        </div>
			               </div>
			           </div>
			       </div>
			   </div>
			<button type="submit" onclick="javascript: return validator();" class="registerbtn">SAVE</button>
			</div>
		</form>
	</div>
	<script type="text/javascript">
    	var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
	   	$('#clear').click(function(e) {
	        e.preventDefault();
	        sig.signature('clear');
	        $("#signature64").val('');
	    });

	  	$('input[type=checkbox]').each(function(i){
            if(!$(this).hasClass('block-sel')){
                $(this).attr('value',$(this).parent().text().trim());
            }
        });

        $('input, select, textarea').each(function(){
            if(!$(this).hasClass('block-sel')){
                block = $(this).closest('.row').find('.block-sel').attr('id');
                if(typeof block != 'undefined'){
                    $(this).attr('name',block+'_'+$(this).attr('name'));
                }
            }
        });

        $(document).on('click','.block-sel', function(){
            $('.'+$(this).attr('id')).toggle();
        });
        $('#relationship_with_patient').val('{{ $patient->relationship_with_patient }}').change();
        $('#gender').val('{{ $patient->gender }}').change();

        function validator() {
	        var alternate_number = $('#alternate_number').val();
	        var contact_number = $('#contact_number').val();
	        var client_name = $('#client_full_name').val();
	        var email = $('#email').val();
	        var patient_full_name = $('#patient_full_name').val();
	        var patient_age = $('#patient_age').val();
	        var dr_contact_number = $('#dr_contact_number').val();
	        var gender = $('#gender').val();
	        var advance = $('#advance_amount').val();
	        var acknowledgement = $('#acknowledgement').val();
	        var signature64 = $('#signature64').val();

	        var reg = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;

    		var intRegex = /[0-9 -()+]+$/;

	
	        if(alternate_number){
		        if (!intRegex.test(alternate_number)){
		           	alert("Please enter correct mobile number!");
		            return false;
		        }

		        if(alternate_number.length != 10){ 
			       	alert("Please enter 10 digits mobile number!");
			        return false;
			    }
		    }

		   
	        if (!intRegex.test(contact_number)){
	           	alert("Please enter correct contact number!");
	            return false;
	        }

	        if(contact_number.length != 10){ 
		       	alert("Please enter 10 digits contact number!");
		        return false;
		    }

		    if(email){
		        if (!email.match(reg)){
		            alert("Enter poper email!");
		            return false;
		        }
    		}

    		if(!patient_full_name){  
		        alert("Enter patient name");
		        return false;  
    		}

    		if(patient_age){
		        if (!intRegex.test(patient_age)){
		           	alert("Please enter age!");
		            return false;
		        }

		        if(patient_age < 115){ 
			       	alert("Please enter correct age!");
			        return false;
			    }
		    }

		    if(dr_contact_number){
		        if (!intRegex.test(dr_contact_number)){
		           	alert("Please enter correct dr mobile number!");
		            return false;
		        }

		        if(dr_contact_number.length != 10){ 
			       	alert("Please enter 10 digits  mobile number!");
			        return false;
			    }
		    }

		    if (gender == ''){
		        alert("Please select gender!");
		        return false;
		    }

		    if (advance){
		        if (!intRegex.test(advance)){
		           	alert("Please enter correct advance amount!");
		            return false;
		        }
		    }
		    
		    if(!acknowledgement){  
		        alert("Acknowledgement is needed");
		        return false;  
    		}

		    if(!signature64){  
		        alert("Signature is needed");
		        return false;  
    		}

	        return false;
	    }
	</script>
</body>
