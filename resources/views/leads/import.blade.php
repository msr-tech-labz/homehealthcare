@extends('layouts.main-layout')

@section('page_title','Import Leads - ')
@section('active_caregivers','active')

@section('page.styles')
    <style>
        .card .header{
            padding: 10px;
        }
        .table tr th{
            text-align: center;
            vertical-align: middle !important;
        }
    </style>
@endsection

@section('content')
<div class="row clearfix">
    <div class="col-sm-6" style="padding-left: 0">
        <div class="card">
            <div class="header clearfix">
                <h2 class="col-sm-6">
                    Sample Excel File
                </h2>
            </div>
            <div class="body text-center" style="height: 120px;">
                <a class="btn bg-teal" href="{{ asset('documents/samples/Sample_Lead_CSV.csv') }}">Download File</a>
                <div class="clearfix"></div><br>
                <span>
                    <b>Note:</b> <i class="text-red">Do not modify/alter columns, keep the fields blank if not applicable</i>
                </span>
            </div>
        </div>
    </div>

    <div class="col-sm-6" style="padding-right: 0">
        <div class="card">
            <div class="header clearfix">
                <h2 class="col-sm-6">
                    Upload Excel File
                </h2>
            </div>
            <div class="body text-center" style="height: 120px;">
                <form id="import-form" action="{{ route('leads.bulk-import') }}" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
                    <div class="col-md-6 text-center">
                        <input type="file" name="excel_file" class="form-control" required="">
                    </div>
                    <div class="col-md-4 text-center">
                        <button type="submit" class="btn btn-block btn-primary" href="">Upload File</button>
                    </div>
                    <div class="clearfix"></div>
                    <span>
                        <b>Note:</b> <i class="text-red">Do not modify/alter columns, keep the fields blank if not applicable</i>
                    </span>
				</form>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-sm-6" style="padding-left: 0">
        <div class="card">
            <div class="header clearfix">
                <h2 class="col-sm-12">
                    Things to do before uploading the excel file
                </h2>
            </div>
            <div class="body text-left" style="height: 250px;">
                <div class="clearfix"></div>
                <span>
                    <i class="text-red">
                        1- Download the sample excel file.<br>
                        2- Add the required field details as given in the sample file.<br>
                        3- Remove the top sample example record(s) in the file before uploading the file.<br>
                        4- Date Format that has to be used is DD-MMM-YYYY (eg. 20-Mar-1992).<br>
                        5- If a field has to be left empty,please keep it blank,do not add - , NA, Not Available,etc.<br>
                        6- If there is any field which demands an input of years, then only numeric value has to be given (eg. Experience(in years) field should have 9 as a value, and not 9 years or 9 yrs).<br>
                        7- Save the file.<br>
                        8- Choose File and Click Upload.<br>
                        9- The Leads will be in Pending Status by default.<br>
                    </i>
                </span><br>
            </div>
        </div>
    </div>

    <div class="col-sm-6" style="padding-right: 0">
        <div class="card">
            <div class="header clearfix">
                <h2 class="col-sm-12">
                    Things to do after uploading the excel file
                </h2>
            </div>
            <div class="body text-left" style="height: 250px;">
            <div class="clearfix"></div>
            <span>
                <i class="text-red">
                    1- Go into the required case.<br>
                    2- Click on the Edit Button in Leads.<br>
                    3- Choose Service Details,Referral Details(if any), and any other left out fields.<br>
                    4- Change Case Disposition(If Required [Default - PENDING] ) along with the Service Date started and required comments.<br>
                </i>
            </span><br>
            </div>
        </div>
    </div>
</div>
@endsection
