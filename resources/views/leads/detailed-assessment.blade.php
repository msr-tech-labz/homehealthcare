<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Detailed Assessment</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="{{ asset('css/assessment.css') }}" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" crossorigin="anonymous">
</head>
<body>
    <div class="text-center">
        <button style="float:left; margin-top: 5px;margin-left:5px;" onclick="window.close();">Back</button>
        <h4>Detailed Assessment</h4>
    </div>
    <form id="assessment_form">
        <div class="row">
            <div class="col-sm-12 header">
                <div class="checkbox-inline"><label class="no-margin"><input type="checkbox" class="block-sel" id="block_1" /> PHYSICIAN'S DETAILS</label></div>
            </div>
            <div class="col-sm-12 block_1" style="display:none">
                <table class="table table-bordered">
                    <tr>
                        <th width="20%">PRIMARY PHYSICIAN NAME</th>
                        <td><input type="text" name="primary_physician_name" class="form-control" /></td>
                    </tr>
                    <tr>
                        <th>PHONE</th>
                        <td>
                            <table class="table">
                                <tr>
                                    <th width="20%">MOBILE</th>
                                    <td><input type="number" name="mobile_number" class="form-control" /></td>
                                </tr>
                                <tr>
                                    <th width="20%">LANDLINE</th>
                                    <td><input type="number" name="landline" class="form-control" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <th>EMAIL</th>
                        <td><input type="email" name="email" class="form-control" /></td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 header">
                <div class="checkbox-inline"><label><input type="checkbox" class="block-sel" id="block_2" /> RELEVANT MEDICAL / SURGICAL INFORMATION</label></div>
            </div>
            <div class="col-sm-12 block_2" style="display:none">
                <table class="table table-bordered">
                    <tr>
                        <th width="20%">PRIMARY DIAGNOSIS</th>
                        <td><textarea class="form-control" name="primary_diagnosis"></textarea></td>
                    </tr>
                    <tr>
                        <th>SECONDARY DIAGNOSIS</th>
                        <td><textarea class="form-control" name="secondary_diagnosis"></textarea></td>
                    </tr>
                    <tr>
                        <th>DIABETIC</th>
                        <td>
                            <table class="table">
                                <tr>
                                    <th width="30%">
                                        <div class="checkbox-inline"><label><input type="radio" name="diabetic" value="Yes" /> YES</label></div>
                                        <div class="checkbox-inline"><label><input type="radio" name="diabetic" value="No" /> NO</label></div>
                                    </th>
                                    <td>
                                        <b>IF YES => </b>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <div class="checkbox-inline"><label><input type="checkbox" name="diabetic_insulin" /> INSULIN</label></div>
                                        <div class="checkbox-inline"><label><input type="checkbox" name="diabetic_oral_hypoglycemic" /> ORAL HYPOGLYCEMIC</label></div>
                                        <div class="checkbox-inline"><label><input type="checkbox" name="diabetic_diet_controlled" /> DIET CONTROLLED</label></div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <th width="20%">HYPERTENSIVE</th>
                        <td>
                            <table class="table">
                                <tr>
                                    <th width="30%">
                                        <div class="checkbox-inline"><label><input type="radio" name="hypertensive" value="Yes"/> YES</label></div>
                                        <div class="checkbox-inline"><label><input type="radio" name="hypertensive" value="No" /> NO</label></div>
                                    </th>
                                    <td>
                                        <b>IF YES => </b>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <div class="checkbox-inline"><label><input type="checkbox" name="hypertensive_oral_hypoglycemic"/> ORAL HYPERTENSIVE DRUGS</label></div>
                                        <div class="checkbox-inline"><label><input type="checkbox" name="hypertensive_diet_controlled" /> DIET CONTROLLED</label></div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <th>MAJOR ILLNESSES IN THE PAST</th>
                        <td><textarea class="form-control" name="major_illnesses_in_the_past"></textarea></td>
                    </tr>
                    <tr>
                        <th>MAJOR SURGERIES IN THE PAST</th>
                        <td><textarea class="form-control"name="major_surgeries_in_the_past"></textarea></td>
                    </tr>
                    <tr>
                        <th>VITAL SIGNS AT THE TIME OF ASSESSMENT</th>
                        <td>
                            <table class="table">
                                <tr>
                                    <th width="10%">BP: </th>
                                    <td><input type="number" step="0.01" class="form-control" name="vital_signs_bp"/></td>
                                    <th width="10%">Pulse: </th>
                                    <td><input type="number" step="0.01" class="form-control" name="vital_signs_pulse"/></td>
                                </tr>
                                <tr>
                                    <th width="10%">Respiration: </th>
                                    <td><input type="number" step="0.01" class="form-control" name="vital_signs_respiration"/></td>
                                    <th width="10%">Temp: </th>
                                    <td><input type="number" step="0.01" class="form-control" name="vital_signs_temp"/></td>
                                </tr>
                                <tr>
                                    <th width="10%">BS: </th>
                                    <td><input type="number" step="0.01" class="form-control" name="vital_signs_bs"/></td>
                                    <th width="10%">SpO2: </th>
                                    <td><input type="number" step="0.01" class="form-control" name="vital_signs_spo2"/></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 header">
                <div class="checkbox-inline"><label class="no-margin"><input type="checkbox" class="block-sel" id="block_3" /> PREVIOUS HOSPITALIZATION DETAILS</label></div>
            </div>
            <div class="col-sm-12 block_3" style="display:none">
                <table class="table table-bordered">
                    <tr>
                        <th width="15%" class="text-center">DATE</th>
                        <th class="text-center">CONDITION</th>
                        <th class="text-center">DETAILS OF TREATMENT / HOSPITALIZATION</th>
                    </tr>
                    <tr height="40px">
                        <td><input type="date" class="form-control" name="hospitalization_details_date1"/></td>
                        <td><textarea class="form-control" name="hospitalization_details_condition1"></textarea></td>
                        <td><textarea class="form-control" name="hospitalization_details_details1"></textarea></td>
                    </tr>
                    <tr height="40px">
                        <td><input type="date" class="form-control" name="hospitalization_details_date2"/></td>
                        <td><textarea class="form-control" name="hospitalization_details_condition2"></textarea></td>
                        <td><textarea class="form-control" name="hospitalization_details_details2"></textarea></td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 header">
                <div class="checkbox-inline"><label><input type="checkbox" class="block-sel" id="block_4" /> RELEVANT MEDICAL / SURGICAL INFORMATION</label></div>
            </div>
            <div class="col-sm-12 block_4" style="display:none">
                <table class="table table-bordered">
                    <tr height="30px">
                        <th>ORIENTATION</th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="orientation_time"> TIME</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="orientation_place"> PLACE</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="orientation_progessive_disorientation"> PROGRESSIVE DISORIENTATION</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="orientation_transient_disorientation"> TRANSIENT DISORIENTATION</label></div>
                        </td>
                    </tr>
                    <tr height="30px">
                        <th>COGNITION</th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="cognition_normal"> NORMAL</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="cognition_mild_impairment"> MILD IMPAIRMENT</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="cognition_moderate_impairment"> MODERATE IMPAIRMENT</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="cognition_severe_impairment"> SEVERE IMPAIRMENT</label></div>
                        </td>
                    </tr>
                    <tr height="30px">
                        <th>BEHAVIOUR</th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="behaiour_compliant_to_care"> COMPLIANT TO CARE</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="behaiour_anxious"> ANXIOUS</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="behaiour_restless"> RESTLESS</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="behaiour_agigated"> AGITATED</label></div>
                        </td>
                    </tr>
                    <tr height="30px">
                        <th>AGGRESSION</th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="aggression_verbal"> VERBAL</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="aggression_physical"> PHYSICAL</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="aggression_sexual"> SEXUAL</label></div>
                        </td>
                    </tr>
                    <tr height="30px">
                        <th>INAPPROPRIATE BEHAVIOUR</th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="inappropriate_behaviour_verbal"> VERBAL</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="inappropriate_behaviour_social"> SOCIAL</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="inappropriate_behaviour_sexual"> SEXUAL</label></div>
                        </td>
                    </tr>
                    <tr height="30px">
                        <th>RISKS</th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="risks_elopement"> ELOPEMENT</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="risks_falls"> FALLS</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="risks_aggression"> AGGRESSION</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="risks_choking"> CHOKING</label></div>
                        </td>
                    </tr>
                    <tr height="30px">
                        <th>ABUSE</th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="abuse_history_of_being_abused"> HISTORY OF BEING ABUSED</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="abuse_history_of_being_abusive"> HISTORY OF BEING ABUSIVE</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="abuse_substance_abuse"> SUBSTANCE ABUSE</label></div>
                        </td>
                    </tr>
                    <tr height="30px">
                        <th>ASSESSOR'S NOTES 1 <br><small>(ON MENTAL STATUS)</small></th>
                        <td>
                            <textarea rows="" cols="" class="form-control" name="assessor_notes1"></textarea>
                        </td>
                    </tr>
                    <tr height="30px">
                        <th>ALCOHOL INTAKE</th>
                        <td>
                            <textarea rows="" cols="" class="form-control" name="alcohol_intake"></textarea>
                        </td>
                    </tr>
                    <tr height="30px">
                        <th>SMOKING HISTORY</th>
                        <td>
                            <textarea rows="" cols="" class="form-control" name="smoking_history"></textarea>
                        </td>
                    </tr>
                    <tr height="30px">
                        <th>ASSESSOR'S NOTES 2 <br><small>(ON MENTAL STATUS)</small></th>
                        <td>
                            <textarea rows="" cols="" class="form-control" name="assessor_notes2"></textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 header">
                <div class="checkbox-inline"><label><input type="checkbox" class="block-sel" id="block_5" /> SENSORY PERCEPTION</label></div>
            </div>
            <div class="col-sm-12 block_5" style="display:none">
                <table class="table table-bordered">
                    <tr height="30px">
                        <th>SKIN CONDITION</th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="skin_condition_intact"> INTACT</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="skin_condition_redness"> REDNESS</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="skin_condition_decubitus_ulcer"> DECUBITUS ULCER</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="skin_condition_excoriation"> EXCORIATION</label></div>
                        </td>
                    </tr>
                    <tr height="30px">
                        <th>VISION</th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="vision_normal"> NORMAL</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="vision_impaired"> IMPAIRED</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="vision_blind"> BLIND (RIGHT/LEFT)</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="vision_cataracts"> CATARACTS</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="vision_peripheral_vision"> PERIPHERAL VISION</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="vision_contacts"> CONTACTS</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="vision_glass"> GLASS</label></div>
                        </td>
                    </tr>
                    <tr height="30px">
                        <th>HEARING</th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="hearing_normal"> NORMAL</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="hearing_impaired"> IMPAIRED - RIGHT /LEFT</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="hearing_deaf"> DEAF - RIGHT/LEFT</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="hearing_hearing"> HEARING AID</label></div>
                        </td>
                    </tr>
                    <tr height="30px">
                        <th>SPEECH</th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="speech_normal"> NORMAL</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="speech_impaired"> IMPAIRED ( MILD/ MEDIUM / SEVERE )</label></div>
                        </td>
                    </tr>
                    <tr height="30px">
                        <th>UNDERSTANDING</th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="understanding_normal"> NORMAL</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="understanding_impaired"> IMPAIRED ( MILD/ MEDIUM / SEVERE )</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="understanding_non_responsive"> NON-RESPONSIVE</label></div>
                        </td>
                    </tr>
                    <tr height="30px">
                        <th>MODE OF COMMUNICATION</th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="mode_of_communication_speech"> SPEECH</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="mode_of_communication_writing"> WRITING</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="mode_of_communication_reading"> LIP READING</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="mode_of_communication_language"> SIGN LANGUAGE</label></div>
                        </td>
                    </tr>
                    <tr height="30px">
                        <th>ASSESSOR'S NOTES 3</th>
                        <td><textarea class="form-control" name="assessor_notes3"></textarea></td>
                    </tr>

                </table>
            </div>
        </div>

		<div class="row">
            <div class="col-sm-12 header">
                <div class="checkbox-inline"><label><input type="checkbox" class="block-sel" id="block_6" /> FUNCTIONAL STATUS </label></div>
            </div>
            <div class="col-sm-12 block_6"style="display:none">
                <table class="table table-bordered">
                    <tr height="30px">
                        <th width="20%">TRANSFERRING</th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="transferring_self"> SELF</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="transferring_assist"> ASSIST</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="transferring_total_care"> TOTAL CARE</label></div>
                        </td>
                    </tr>
                    <tr height="30px">
						<th width="20%">BATHING</th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="bathing_self"> SELF</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="bathing_assist"> ASSIST</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="bathing_total_care"> TOTAL CARE</label></div>
                        </td>
                    </tr>
                    <tr height="30px">
						<th>DRESSING</th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="dressing_self"> SELF</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="dressing_assist"> ASSIST</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="dressing_total_care"> TOTAL CARE</label></div>
						</td>
                    </tr>
                    <tr height="30px">
						<th>MEAL PREPN</th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="meal_prepn_self"> SELF</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="meal_prepn_assist"> ASSIST</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="meal_prepn_total_care"> TOTAL CARE</label></div>
                        </td>
                    </tr>
                    <tr height="30px">
						<th>FEEDING</th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="feeding_self"> SELF</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="feeding_assist" > ASSIST</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="feeding_total_care"> TOTAL CARE</label></div>
                        </td>
                    </tr>
					<tr height="30px">
						<th>ORAL CARE</th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="oral_care_self"> SELF</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="oral_care_assist"> ASSIST</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="oral_care_total_care"> TOTAL CARE</label></div>
                        </td>
                    </tr>
					<tr height="30px">
						<th>PERSONAL CHORES</th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="personal_chores_self"> SELF</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="personal_chores_assist"> ASSIST</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="personal_chores_total_care"> TOTAL CARE</label></div>
                        </td>
                    </tr>
					<tr height="30px">
						<th>TOILETTING</th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="toiletting_self"> SELF</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="toiletting_assist"> ASSIST</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="toiletting_total_care"> TOTAL CARE</label></div>
                        </td>
                    </tr>
					<tr height="30px">
                        <th>ASSESSOR'S NOTES 4</th>
                        <td><textarea class="form-control" name="assessors_notes"></textarea></td>
                    </tr>

                </table>
            </div>
        </div>

		<div class="row">
            <div class="col-sm-12 header">
                <div class="checkbox-inline"><label><input type="checkbox" class="block-sel" id="block_7" /> ACTIVITY </label></div>
            </div>
            <div class="col-sm-12 block_7" style="display:none">
                <table class="table table-bordered">
                    <tr height="30px">
                        <th>MOBILITY</th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox"  name="mobility_independent"> INDEPENDENT</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox"  name="mobility_bedridden"> BEDRIDDEN</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="mobility_assistence_required"> ASSISTENCE REQUIRED 1 2 </label></div>
                        </td>
                    </tr>
                    <tr height="30px">
                        <th>ASSISTIVE DEVICE</th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="assistive_device_mechanical_lifs"> MECHANICAL LIFS</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="assistive_device_walker"> WALKER</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="assistive_device_cane"> CANE</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="assistive_device_crutches"> CRUTCHES</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="assistive_device_wheel_chair"> WHEEL CHAIR</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="assistive_device_others"> OTHERS</label><u><textarea class="form-control" name="assistive_device_others"></textarea></u></div>
							<div class="checkbox-inline"><label><input type="checkbox" name="assistive_device_prosthetics"> PROSTHETICS</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="assistive_device_leg_brace"> LEG BRACE</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="assistive_device_neck_brace"> NECK BRACE</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="assistive_device_hearing_aid"> HEARING AID</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="assistive_device_others"> OTHERS</label><u><textarea class="form-control"name="assistive_device_others"></textarea></u></div>
						</td>
                    </tr>
                    <tr height="30px">
                        <th>UPPER LIMBS</th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="upper_limbs_normal"> NORMAL</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="upper_limbs_impairment_rl"> IMPAIRMENT(R/L)</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="upper_limbs_tremor_rl"> TREMOR(R/L)</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="upper_limbs_amputation_rl"> AMPUTATION(R/L)</label></div>
							<div class="checkbox-inline"><label><input type="checkbox" name="upper_limbs_prosthesis"> PROSTHESIS</label></div>
						</td>
                    </tr>
                    <tr height="30px">
                        <th>LOWER LIMBS</th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="lower_limbs_normal"> NORMAL</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="lower_limbs_impairment_rl"> IMPAIRMENT(R/L)</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox"  name="lower_limbs_tremor_rl"> TREMOR(R/L)</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="lower_limbs_amputation_rl"> AMPUTATION(R/L)</label></div>
							<div class="checkbox-inline"><label><input type="checkbox" name="lower_limbs_prosthesis"> PROSTHESIS</label></div>
						</td>
                    </tr>
                    <tr height="30px">
                        <th>ASSESSOR'S NOTES 5</th>
                        <td><textarea class="form-control" name="assessors_notes_5"></textarea></td>
                    </tr>
                </table>
            </div>
        </div>

		<div class="row">
            <div class="col-sm-12 header">
                <div class="checkbox-inline"><label><input type="checkbox" class="block-sel" id="block_8"/> NUTRITIONAL /FLUID INTAKE STATUS </label></div>
            </div>
            <div class="col-sm-12 block_8" style="display:none">
                <table class="table table-bordered">
                    <tr height="30px">
                        <th>BODY MASS</th>
                        <td colspan="8">
                            <div class="checkbox-inline"><label><input type="checkbox" name="body_mass_emaciated"> EMACIATED</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="body_mass_thin"> THIN</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox"  name="body_mass_normal">  NORMAL</label></div>
                            <div class="clearfix"></div>
							<div class="checkbox-inline"><label><input type="checkbox" name="body_mass_overweight"> OVERWEIGHT</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="body_mass_obese"> OBESE</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="body_mass_resent_weight_loss"> RESENT WEIGHT LOSS</label></div>
						</td>
                    </tr>
                    <tr height="30px">
						<th>ORAL STATUS</th>
                        <td colspan="8">
                            <div class="checkbox-inline"><label><input type="checkbox" name="oral_status_own_teeth"> OWN TEETH</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="oral_status_dentures"> DENTURES</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="oral_status_partials_plate"> PARTIALS/PLATE</label></div>
                            <div class="clearfix"></div>
							<div class="checkbox-inline"><label><input type="checkbox" name="oral_status_tooth_decay"> TOOTH DECAY</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="oral_status_swallowing_problems"> SWALLOWING PROBLEMS</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="oral_status_choking_risk"> CHOKING RISK</label></div>
                            <div class="clearfix"></div>
							<div class="checkbox-inline"><label><input type="checkbox" name="oral_status_dry_mouth"> DRY MOUTH</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="oral_status_excessive_saliva"> EXCESSIVE SALIVA</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="oral_status_mouth_ulcers_infection"> MOUTH ULCERS/INFECTION</label></div>
						</td>
                    </tr>
                    <tr height="30px">
						<th>DIET</th>
                        <td colspan="2">
                            <div class="checkbox-inline"><label><input type="checkbox" name="diet_regular"> REGULAR</label></div>
							<div class="checkbox-inline"><label><input type="checkbox" name="diet_semi_fluid"> SEMI-FLUID</label></div>
						</td>
						<td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="diet_soft"> SOFT</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="diet_fluid"> FLUID</label></div>
						</td>
                    </tr>
                    <tr height="30px">
						<th>PROTEIN</th>
						<td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="protien_high"> HIGH</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="protien_low"> LOW</label></div>
						</td>
                    </tr>
                    <tr height="30px">
						<th>CALORIE</th>
						<td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="calorie_high"> HIGH</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="calorie_low"> LOW</label></div>
						</td>
						<td>
						<div class="checkbox-inline"><label><input type="checkbox" name="calorie_suppliments"> SUPPLIMENTS</label></div>
						</td>
                    </tr>
                    <tr height="30px">
                        <th>APPETITE</th>
                        <td colspan="8">
                            <div class="checkbox-inline"><label><input type="checkbox" name="appetite_good"> GOOD</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox"  name="appetite_fair"> FAIR</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="appetite_poor"> POOR</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="appetite_recent_change"> RECENT CHANGE</label></div>
						</td>
                    </tr>

					<tr height="30px">
                        <th>ASSESSOR'S NOTES 6</th>
                        <td colspan="8"><textarea class="form-control" name="assessors_notes6"></textarea></td>
                    </tr>

                </table>
            </div>
        </div>

		<div class="row">
            <div class="col-sm-12 header">
                <div class="checkbox-inline"><label><input type="checkbox" class="block-sel" id="block_9" /> ELIMINATION</label></div>
            </div>
            <div class="col-sm-12 block_9" style="display:none">
                <table class="table table-bordered">
                    <tr height="30px">
                        <th rowspan="5">BLADDER </th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="bladder_contitnent"> CONTITNENT</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="bladder_incontitnent"> INCONTINENT</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="bladder_nocturial"> NOCTURIA</label></div>
						</td>
					</tr>
					<tr height="30px">
						<td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="bladder_indwelling_catheter"> INDWELLING CATHETER</label></div> AND
                            <div class="checkbox-inline"><label>TYPE&SIZE</label><input class="checkbox-inline" type="text" name="bladder_ic_type_size"> </div>
						</td>
					</tr>
					<tr height="30px">
						<td>
                            <div class="checkbox-inline"><label>IN & OUT CATHETERIZATION</label><input class="checkbox-inline" type="text" name="bladder_in_and_out_catheterization"></div>
						</td>
					</tr>
					<tr height="30px">
						<td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="bladder_ileoconduit"> ILEOCONDUIT, </label></div>
                            <div class="checkbox-inline"><label>APPLIANCE LAST CHANGED ON</label><input class="checkbox-inline" type="text" name="bladder_appliance_ast_changed_on"></div>
                        </td>
					</tr>
					<tr height="30px">
						<td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="condom_drainage"> CONDOM DRAINAGE</label></div>
                        </td>

                    </tr>
                    <tr height="30px">
						<th>BOWELS</th>
                        <td>
                            <div class="checkbox-inline"><label><input type="checkbox" name="bowels_self_care"> SELF CARE</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="bowels_assist"> ASSIST</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="bowels_total_care"> TOTAL CARE</label></div>
							<div class="checkbox-inline"><label><input type="checkbox" name="bowels_c_difficile"> C. DIFFICILE</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="bowels_continent"> CONTINENT</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="bowels_incontinent"> INCONTINENT</label></div>
							<div class="checkbox-inline"><label><input type="checkbox" name="bowels_constipation"> CONSTIPATION</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="bowels_diarrehea"> DIARRHEA</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="bowels_ostomy_care_ostomy_type"> OSTOMY CARE / OSTOMY TYPE</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="bowels_mushroom_catheter" > WASHROOM CATHETER</label></div>
						</td>
                    </tr>
					<tr height="30px">
                        <th>ASSESSOR'S NOTES 7</th>
                        <td><textarea class="form-control" name="assessors_note7"></textarea></td>
                    </tr>

                </table>
            </div>
        </div>

		<div class="row">
            <div class="col-sm-12 header">
                <div class="checkbox-inline"><label><input type="checkbox" class="block-sel" id="block_10"/> PAIN MANAGEMENT</label></div>
            </div>
            <div class="col-sm-12 block_10" style="display:none">
                <table class="table table-bordered">
                    <tr height="30px">
                        <th width="20%">RATE YOUR PAIN</th>
                        <td><textarea class="form-control" name="rate_your_pain"></textarea>
						</td>
                    </tr>
                    <tr height="30px">
                        <th>PAIN HISTORY</th>
                        <td><textarea class="form-control" name="pain_history"></textarea>
						</td>
                    </tr>
					<tr height="30px">
                        <th>DESCRIPTION</th>
                        <td><textarea class="form-control" name="pain_description"></textarea>
                        </td>
                    </tr>
                    <tr height="30px">
						<th>LOCATION/S</th>
                        <td><textarea class="form-control" name="pain_location"></textarea>
                        </td>
                    </tr>
					<tr height="30px">
                        <th>WHAT CAUSES /INCREASES THE PAIN</th>
                        <td>
						<textarea rows="" cols="" class="form-control" name="what_cause_increases_the_pain"></textarea>
                        </td>
                    </tr>
					<tr height="30px">
                        <th>WHAT DECREASES / HELPS TO REDUCE PAIN/th>
                        <td>
						<div class="checkbox-inline"><label><input type="checkbox" name="what_decrease_helps_to_reduce_pain_medication"> MEDICATION</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox"  name="what_decrease_helps_to_reduce_pain_heat_pad"> HEAT PAD</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="what_decrease_helps_to_reduce_pain_change_of_position"> CHANGE OF POSITION</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="what_decrease_helps_to_reduce_pain_any_other_method"> ANY OTHER METHOD</label><textarea class="form-control"></textarea></div>
                        </td>

                    </tr>
					<tr height="30px">
                        <th>PAIN EXPRESSION / CLIENT GOALS FOR SELF RELIEF</th>
                        <td>
						<textarea rows="" cols="" class="form-control" name="pain_express_client_goals_for_self_relife"></textarea>
                        </td>

                    </tr>

					<tr height="30px">
                        <th>ASSESSOR'S NOTES 8</th>
                        <td><textarea class="form-control" name="assessor_notes8"></textarea></td>
                    </tr>

                </table>
            </div>
        </div>

		<div class="row">
            <div class="col-sm-12 header">
                <div class="checkbox-inline"><label><input type="checkbox" class="block-sel" id="block_11" /> FALLS ASSESSMENT</label></div>
            </div>
            <div class="col-sm-12 block_11" style="display:none">
                <table class="table table-bordered">
                    <tr height="30px">
                        <th width="30%">HAVE YOU HAD A FALL IN LAST 12 MONTHS?</th>
                        <td>
						<div class="checkbox-inline"><label><input type="radio" name="have_you_had_a_fail_in_last_12months" value="Yes"> YES</label></div>
                        <div class="checkbox-inline"><label><input type="radio" name="have_you_had_a_fail_in_last_12months" value="No"> NO</label></div>
                        </td>
                    </tr>
                    <tr height="30px">
                        <th>ANY HISTORY OF FRACTURES OR INJURIES?</th>
                        <td>
						<div class="checkbox-inline"><label><input type="radio" name="any_history_of_fractures_or_injuries" value="Yes"> YES</label></div>
                        <div class="checkbox-inline"><label><input type="radio" name="any_history_of_fractures_or_injuries" value="No"> NO</label></div>
                        </td>
                    </tr>
					<tr height="30px">
                        <th>ARE YOU ON 4 ORMORE MEDICATIONS A DAY?</th>
                        <td>
						<div class="checkbox-inline"><label><input type="radio" name="are_you_on_4_ormore_medications_a_day" value="Yes"> YES</label></div>
                        <div class="checkbox-inline"><label><input type="radio" name="are_you_on_4_ormore_medications_a_day" value="No"> NO</label></div>
                        </td>
                    </tr>
                    <tr height="30px">
						<th>DO YOU HAVE PARKINSONS DISEASE OR HAVE YOU HAD A STROKE?</th>
                        <td>
						<div class="checkbox-inline"><label><input type="radio" name="do_you_have_parkinsons_disese_or_have_you_had_a_stroke" value="Yes"> YES</label></div>
                        <div class="checkbox-inline"><label><input type="radio" name="do_you_have_parkinsons_disese_or_have_you_had_a_stroke" value="No"> NO</label></div>
                        </td>
                    </tr>
					<tr height="30px">
                        <th>DO YOU FEEL UNSTEADY OR HAVE PROBLEMS WITH BALANCE</th>
                        <td>
						<textarea rows="" cols="" class="form-control" name="do_you_feel_unsteady_or_have_problems_with_balance"></textarea>
                        </td>

                    </tr>

					<tr height="30px">
                        <th>ASSESSOR'S NOTES 9</th>
                        <td><textarea class="form-control" name="assessor_notes9"></textarea></td>
                    </tr>

                </table>
            </div>
        </div>

		<div class="row">
            <div class="col-sm-12 header">
                <div class="checkbox-inline"><label><input type="checkbox" class="block-sel" id="block_12"/> MEDICATION REGIMEN</label></div>
            </div>
            <div class="col-sm-12 block_12" style="display:none">
                <table class="table table-bordered">
                    <tr height="30px">
                        <th>SL NO</th>
						<th>MEDICATION</th>
						<th>DOSAGE</th>
						<th>FREQUENCY</th>
						<th>COMMENTS</th>
					<tr height="30px">
                        <td>
						<textarea class="form-control" name="medication_regimen_sl_no_1"></textarea>
                        </td>
						<td>
						<textarea class="form-control" name="medication_regimen_medication_1"></textarea>
                        </td>
						<td>
						<textarea class="form-control"  name="medication_regimen_dosage_1"></textarea>
                        </td>
						<td>
						<textarea class="form-control" name="medication_regimen_frequency_1"></textarea>
                        </td>
						<td>
						<textarea class="form-control" name="medication_regimen_comments_1"></textarea>
                        </td>
                    </tr>
                    <tr height="30px">
                        <td>
                            <textarea class="form-control" name="medication_regimen_sl_no_2"></textarea>
                        </td>
                        <td>
                            <textarea class="form-control" name="medication_regimen_medication_2"></textarea>
                        </td>
                        <td>
                            <textarea class="form-control"  name="medication_regimen_dosage_2"></textarea>
                        </td>
                        <td>
                            <textarea class="form-control" name="medication_regimen_frequency_2"></textarea>
                        </td>
                        <td>
                            <textarea class="form-control" name="medication_regimen_comments_2"></textarea>
                        </td>
                    </tr>
                    <tr height="30px">
                        <td>
                            <textarea class="form-control" name="medication_regimen_sl_no_3"></textarea>
                        </td>
                        <td>
                            <textarea class="form-control" name="medication_regimen_medication_3"></textarea>
                        </td>
                        <td>
                            <textarea class="form-control"  name="medication_regimen_dosage_3"></textarea>
                        </td>
                        <td>
                            <textarea class="form-control" name="medication_regimen_frequency_3"></textarea>
                        </td>
                        <td>
                            <textarea class="form-control" name="medication_regimen_comments_3"></textarea>
                        </td>
                    </tr>
                    <tr height="30px">
                        <td>
                        <textarea class="form-control" name="medication_regimen_sl_no_4"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control" name="medication_regimen_medication_4"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control"  name="medication_regimen_dosage_4"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control" name="medication_regimen_frequency_4"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control" name="medication_regimen_comments_4"></textarea>
                        </td>
                    </tr>
                    <tr height="30px">
                        <td>
                        <textarea class="form-control" name="medication_regimen_sl_no_5"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control" name="medication_regimen_medication_5"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control"  name="medication_regimen_dosage_5"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control" name="medication_regimen_frequency_5"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control" name="medication_regimen_comments_5"></textarea>
                        </td>
                    </tr>
                    <tr height="30px">
                        <td>
                        <textarea class="form-control" name="medication_regimen_sl_no_6"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control" name="medication_regimen_medication_6"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control"  name="medication_regimen_dosage_6"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control" name="medication_regimen_frequency_6"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control" name="medication_regimen_comments_6"></textarea>
                        </td>
                    </tr>
                    <tr height="30px">
                        <td>
                        <textarea class="form-control" name="medication_regimen_sl_no_7"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control" name="medication_regimen_medication_7"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control"  name="medication_regimen_dosage_7"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control" name="medication_regimen_frequency_7"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control" name="medication_regimen_comments_7"></textarea>
                        </td>
                    </tr>
                    <tr height="30px">
                        <td>
                        <textarea class="form-control" name="medication_regimen_sl_no_8"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control" name="medication_regimen_medication_8"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control"  name="medication_regimen_dosage_8"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control" name="medication_regimen_frequency_8"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control" name="medication_regimen_comments_8"></textarea>
                        </td>
                    </tr>
                    <tr height="30px">
                        <td>
                        <textarea class="form-control" name="medication_regimen_sl_no_9"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control" name="medication_regimen_medication_9"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control"  name="medication_regimen_dosage_9"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control" name="medication_regimen_frequency_9"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control" name="medication_regimen_comments_9"></textarea>
                        </td>
                    </tr>
                    <tr height="30px">
                        <td>
                        <textarea class="form-control" name="medication_regimen_sl_no_10"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control" name="medication_regimen_medication_10"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control"  name="medication_regimen_dosage_10"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control" name="medication_regimen_frequency_10"></textarea>
                        </td>
                        <td>
                        <textarea class="form-control" name="medication_regimen_comments_10"></textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

		<div class="row">
            <div class="col-sm-12 header">
                <div class="checkbox-inline"><label><input type="checkbox" class="block-sel" id="block_13"/> PHYSICAL PROBLEMS AND SUPPORTIVE DEVICES</label></div>
            </div>
            <div class="col-sm-12 block_13" style="display:none">
                <table class="table table-bordered">
                    <tr height="30px">
                        <th>PHYSICAL PROBLEMS</th>
                        <td>
						<div class="checkbox-inline"><label><input type="checkbox" name="physical_problems_difficulty_in_hearing"> DIFFICULTY IN HEARING</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="physical_problems_difficulty_in_walking"> DIFFICULTY IN WALKING</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="physical_problems_difficulty_in_breathing"> DIFFICULTY IN BREATHING</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="physical_problems_eyesight"> EYESIGHT</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox"  name="physical_problems_fails"> FALLS</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="physical_problems_paralysis"> PARALYSIS</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="physical_problems_arthritis"> ARTHRITIS</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="physical_problems_osteoporosis"> OSTEOPOROSIS</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="physical_problems_chronic_pain"> CHRONIC PAIN</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="physical_problems_dementia"> DEMENTIA</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="physical_problems_alzheimer"> ALZHEIMERS</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="physical_problems_stroke"> STROKE</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="physical_problems_diabetes"> DIABETES</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="physical_problems_high_blood_pressure"> HIGH BLOOD PRESSURE</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="physical_problems_kidney_urinary"> KIDNEY / URINARY</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="physical_problems_emphysema"> EMPHYSEMA</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox"> OTHER:</label><textarea class="form-control" name="physical_problems_other"></textarea></div>
						</td>
                    </tr>
                    <tr height="30px">
                        <th>SUPPORTIVE DEVICES USED CURRENTLY</th>
                        <td>
						<div class="checkbox-inline"><label><input type="checkbox" name="supportive_devices_used_currently_heading_aids"> HEARING AIDS</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox"  name="supportive_devices_used_currently_cane"> CANE </label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="supportive_devices_used_currently_walker"> WALKER </label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="supportive_devices_used_currently_wheelchair"> WHEELCHAIR </label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="supportive_devices_used_currently_dentures"> DENTURES</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="supportive_devices_used_currently_prosthetic"> PROSTHETICS</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="supportive_devices_used_currently_leg_brace"> LEG BRACE</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="supportive_devices_used_currently_neck_brace"> NECK BRACE</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="supportive_devices_used_currently_implants"> IMPLANTS</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="supportive_devices_used_currently_glasses"> GLASSES</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="supportive_devices_used_currently_fixator_external_internal"> FIXATOR(EXTERNAL / INTERNAL)</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox"> OTHER :</label><textarea class="form-control" name="supportive_devices_used_currently_other"></textarea></div>
                        </td>
                    </tr>
					<tr height="30px">
                        <th>ASSESSOR NOTES 10</th>
                        <td><textarea class="form-control" name="assessor_notes10"></textarea></td>
                    </tr>
                </table>
            </div>
        </div>

		<div class="row">
            <div class="col-sm-12 header">
                <div class="checkbox-inline"><label><input type="checkbox" class="block-sel" id="block_14"/> DIETARY PROFILE - DETAILED (OPTIONAL)</label></div>
            </div>
            <div class="col-sm-12 block_14" style="display:none">
                <table class="table table-bordered">
                    <tr height="30px">
                        <th>FOOD ALLERGIES</th>
                        <td colspan="1">
						<div class="checkbox-inline"><label><input type="radio" name="food_allergies" value="No"> NO</label></div>
                        <div class="checkbox-inline"><label><input type="radio" name="food_allergies" value="Yes"> YES</label></div>
						</td>
						<td colspan="3">
                        <div class="checkbox-inline"><label><input type="checkbox" name="food_allergies_peanuts"> PEANUTS</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="food_allergies_shellfish"> SHELLFISH</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="food_allergies_dairy_produce"> DAIRY PRODUCE</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="food_allergies_eggs"> EGGS</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="food_allergies_other"> OTHER</label><textarea class="form-control"></textarea></div>
						</td>
                    </tr>
                    <tr height="30px">
                        <th>EATING & DRINKING</th>
                        <td colspan="4">
						<div class="checkbox-inline"><label><input type="checkbox" name="eating_drinking_self"> SELF</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="eating_drinking_supervision"> SUPERVISION </label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="eating_drinking_assist"> ASSIST </label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="eating_drinking_total_care"> TOTAL CARE </label></div>
                        </td>
                    </tr>
					<tr hight="30px">
                        <th>FLUID INTAKE</th>
                        <td colspan="4">
						<div class="checkbox-inline"><label><input type="checkbox" name="fluid_intake_oral"> ORAL</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="fluid_intake_IV"> IV</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="fluid_intake_ryles_tube_feeding"> RYLES TUBE FEEDING </label></div>
                        </td>
                    </tr>
					<tr>
						<th></th>
						<th>BREAKFAST</th>
						<th>LUNCH</th>
						<th>SNACKS</th>
						<th>DINNER</th>
					</tr>
					<tr>
						<td>USUAL TIME</td>
						<td><textarea class="form-control" name="usual_time_break_fast"></textarea></td>
						<td><textarea class="form-control" name="usual_time_lunch"></textarea></td>
						<td><textarea class="form-control" name="usual_time_snacks"></textarea></td>
						<td><textarea class="form-control" name="usual_time_dinner"></textarea></td>
					</tr>
					<tr>
						<td>FAVOURITE FOOD</td>
						<td><textarea class="form-control" name="favourite_food_breakfast"></textarea></td>
						<td><textarea class="form-control" name="favourite_food_lunch"></textarea></td>
						<td><textarea class="form-control" name="favourite_food_snacks"></textarea></td>
						<td><textarea class="form-control" name="favourite_food_dinner"></textarea></td>
					</tr>
					<tr>
						<td>DISLIKE</td>
						<td><textarea class="form-control" name="dislike_break_fast"></textarea></td>
						<td><textarea class="form-control" name="dislike_lunch"></textarea></td>
						<td><textarea class="form-control" name="dislike_snacks"></textarea></td>
						<td><textarea class="form-control" name="dislike_dinner"></textarea></td>
					</tr>
					<tr height="30px">
                        <th>ASSESSORS NOTES 11</th>
                        <td colspan="4"><textarea class="form-control" name="assessor_notes11"></textarea></td>
                    </tr>


                </table>
            </div>
        </div>

		<div class="row">
            <div class="col-sm-12 header">
                <div class="checkbox-inline"><label><input type="checkbox" class="block-sel" id="block_15" name="daily_routinr_optional" /> DAILY ROUTINE (OPTIONAL)</label></div>
            </div>
            <div class="col-sm-12 block_15" style="display:none">
                <table class="table table-bordered">
                    <tr height="30px">
                        <th>TIME</th>
						<th>MON</th>
						<th>TUE</th>
						<th>WED</th>
						<th>THU</th>
						<th>FRI</th>
						<th>SAT</th>
						<th>SUN</th>
					</tr>
					<tr>
						<td>6 AM</td>
						<td><textarea class="form-control"  name="6am_mon"></textarea></td>
						<td><textarea class="form-control" name="6am_tue"></textarea></td>
						<td><textarea class="form-control" name="6am_wed"></textarea></td>
						<td><textarea class="form-control" name="6am_thu"></textarea></td>
						<td><textarea class="form-control" name="6am_fri"></textarea></td>
						<td><textarea class="form-control" name="6am_sat"></textarea></td>
						<td><textarea class="form-control" name="6am_sun"></textarea></td>
					</tr>
					<tr>
						<td>7 AM</td>
						<td><textarea class="form-control"  name="7am_mon"></textarea></td>
						<td><textarea class="form-control" name="7am_tue"></textarea></td>
						<td><textarea class="form-control" name="7am_wed"></textarea></td>
						<td><textarea class="form-control" name="7am_thu"></textarea></td>
						<td><textarea class="form-control" name="7am_fri"></textarea></td>
						<td><textarea class="form-control" name="7am_sat"></textarea></td>
						<td><textarea class="form-control" name="7am_sun"></textarea></td>
					</tr>
					<tr>
						<td>8 AM</td>
						<td><textarea class="form-control"  name="8am_mon"></textarea></td>
						<td><textarea class="form-control" name="8am_tue"></textarea></td>
						<td><textarea class="form-control" name="8am_wed"></textarea></td>
						<td><textarea class="form-control" name="8am_thu"></textarea></td>
						<td><textarea class="form-control" name="8am_fri"></textarea></td>
						<td><textarea class="form-control" name="8am_sat"></textarea></td>
						<td><textarea class="form-control" name="8am_sun"></textarea></td>
					</tr>
					<tr>
						<td>9 AM</td>
						<td><textarea class="form-control"  name="9am_mon"></textarea></td>
						<td><textarea class="form-control" name="9am_tue"></textarea></td>
						<td><textarea class="form-control" name="9am_wed"></textarea></td>
						<td><textarea class="form-control" name="9am_thu"></textarea></td>
						<td><textarea class="form-control" name="9am_fri"></textarea></td>
						<td><textarea class="form-control" name="9am_sat"></textarea></td>
						<td><textarea class="form-control" name="9am_sun"></textarea></td>
					</tr>
					<tr>
						<td>10 AM</td>
						<td><textarea class="form-control"  name="10am_mon"></textarea></td>
						<td><textarea class="form-control" name="10am_tue"></textarea></td>
						<td><textarea class="form-control" name="10am_wed"></textarea></td>
						<td><textarea class="form-control" name="10am_thu"></textarea></td>
						<td><textarea class="form-control" name="10am_fri"></textarea></td>
						<td><textarea class="form-control" name="10am_sat"></textarea></td>
						<td><textarea class="form-control" name="10am_sun"></textarea></td>
					</tr>
					<tr>
						<td>11 AM</td>
						<td><textarea class="form-control"  name="11am_mon"></textarea></td>
						<td><textarea class="form-control" name="11am_tue"></textarea></td>
						<td><textarea class="form-control" name="11am_wed"></textarea></td>
						<td><textarea class="form-control" name="11am_thu"></textarea></td>
						<td><textarea class="form-control" name="11am_fri"></textarea></td>
						<td><textarea class="form-control" name="11am_sat"></textarea></td>
						<td><textarea class="form-control" name="11am_sun"></textarea></td>
					</tr>
					<tr>
						<td>12 NOON</td>
						<td><textarea class="form-control"  name="12noon_mon"></textarea></td>
						<td><textarea class="form-control" name="12noon_tue"></textarea></td>
						<td><textarea class="form-control" name="12noon_wed"></textarea></td>
						<td><textarea class="form-control" name="12noon_thu"></textarea></td>
						<td><textarea class="form-control" name="12noon_fri"></textarea></td>
						<td><textarea class="form-control" name="12noon_sat"></textarea></td>
						<td><textarea class="form-control" name="12noon_sun"></textarea></td>
					</tr>
					<tr>
						<td>1 PM</td>
						<td><textarea class="form-control"  name="1pm_mon"></textarea></td>
						<td><textarea class="form-control" name="1pm_tue"></textarea></td>
						<td><textarea class="form-control" name="1pm_wed"></textarea></td>
						<td><textarea class="form-control" name="1pm_thu"></textarea></td>
						<td><textarea class="form-control" name="1pm_fri"></textarea></td>
						<td><textarea class="form-control" name="1pm_sat"></textarea></td>
						<td><textarea class="form-control" name="1pm_sun"></textarea></td>
					</tr>
					<tr>
						<td>2 PM</td>
						<td><textarea class="form-control"  name="2pm_mon"></textarea></td>
						<td><textarea class="form-control" name="2pm_tue"></textarea></td>
						<td><textarea class="form-control" name="2pm_wed"></textarea></td>
						<td><textarea class="form-control" name="2pm_thu"></textarea></td>
						<td><textarea class="form-control" name="2pm_fri"></textarea></td>
						<td><textarea class="form-control" name="2pm_sat"></textarea></td>
						<td><textarea class="form-control" name="2pm_sun"></textarea></td>
					</tr>
					<tr>
						<td>3 PM</td>
						<td><textarea class="form-control"  name="3pm_mon"></textarea></td>
						<td><textarea class="form-control" name="3pm_tue"></textarea></td>
						<td><textarea class="form-control" name="3pm_wed"></textarea></td>
						<td><textarea class="form-control" name="3pm_thu"></textarea></td>
						<td><textarea class="form-control" name="3pm_fri"></textarea></td>
						<td><textarea class="form-control" name="3pm_sat"></textarea></td>
						<td><textarea class="form-control" name="3pm_sun"></textarea></td>
					</tr>
					<tr>
						<td>4 PM</td>
						<td><textarea class="form-control"  name="4pm_mon"></textarea></td>
						<td><textarea class="form-control" name="4pm_tue"></textarea></td>
						<td><textarea class="form-control" name="4pm_wed"></textarea></td>
						<td><textarea class="form-control" name="4pm_thu"></textarea></td>
						<td><textarea class="form-control" name="4pm_fri"></textarea></td>
						<td><textarea class="form-control" name="4pm_sat"></textarea></td>
						<td><textarea class="form-control" name="4pm_sun"></textarea></td>
					</tr>
					<tr>
						<td>5 PM</td>
						<td><textarea class="form-control"  name="5pm_mon"></textarea></td>
						<td><textarea class="form-control" name="5pm_tue"></textarea></td>
						<td><textarea class="form-control" name="5pm_wed"></textarea></td>
						<td><textarea class="form-control" name="5pm_thu"></textarea></td>
						<td><textarea class="form-control" name="5pm_fri"></textarea></td>
						<td><textarea class="form-control" name="5pm_sat"></textarea></td>
						<td><textarea class="form-control" name="5pm_sun"></textarea></td>
					</tr>
					<tr>
						<td>6 PM</td>
						<td><textarea class="form-control"  name="6pm_mon"></textarea></td>
						<td><textarea class="form-control" name="6pm_tue"></textarea></td>
						<td><textarea class="form-control" name="6pm_wed"></textarea></td>
						<td><textarea class="form-control" name="6pm_thu"></textarea></td>
						<td><textarea class="form-control" name="6pm_fri"></textarea></td>
						<td><textarea class="form-control" name="6pm_sat"></textarea></td>
						<td><textarea class="form-control" name="6pm_sun"></textarea></td>
					</tr>
					<tr>
						<td>7 PM</td>
						<td><textarea class="form-control"  name="7pm_mon"></textarea></td>
						<td><textarea class="form-control" name="7pm_tue"></textarea></td>
						<td><textarea class="form-control" name="7pm_wed"></textarea></td>
						<td><textarea class="form-control" name="7pm_thu"></textarea></td>
						<td><textarea class="form-control" name="7pm_fri"></textarea></td>
						<td><textarea class="form-control" name="7pm_sat"></textarea></td>
						<td><textarea class="form-control" name="7pm_sun"></textarea></td>
					</tr>
					<tr>
						<td>8 PM</td>
						<td><textarea class="form-control"  name="8pm_mon"></textarea></td>
						<td><textarea class="form-control" name="8pm_tue"></textarea></td>
						<td><textarea class="form-control" name="8pm_wed"></textarea></td>
						<td><textarea class="form-control" name="8pm_thu"></textarea></td>
						<td><textarea class="form-control" name="8pm_fri"></textarea></td>
						<td><textarea class="form-control" name="8pm_sat"></textarea></td>
						<td><textarea class="form-control" name="8pm_sun"></textarea></td>
					</tr>
					<tr>
						<td>9 PM</td>
						<td><textarea class="form-control"  name="9pm_mon"></textarea></td>
						<td><textarea class="form-control" name="9pm_tue"></textarea></td>
						<td><textarea class="form-control" name="9pm_wed"></textarea></td>
						<td><textarea class="form-control" name="9pm_thu"></textarea></td>
						<td><textarea class="form-control" name="9pm_fri"></textarea></td>
						<td><textarea class="form-control" name="9pm_sat"></textarea></td>
						<td><textarea class="form-control" name="9pm_sun"></textarea></td>
					</tr>
					<tr>
						<td>10 PM-5 AM</td>
						<td><textarea class="form-control"  name="10pm_5am_mon"></textarea></td>
						<td><textarea class="form-control" name="10pm_5am_tue"></textarea></td>
						<td><textarea class="form-control" name="10pm_5am_wed"></textarea></td>
						<td><textarea class="form-control" name="10pm_5am_thu"></textarea></td>
						<td><textarea class="form-control" name="10pm_5am_fri"></textarea></td>
						<td><textarea class="form-control" name="10pm_5am_sat"></textarea></td>
						<td><textarea class="form-control" name="10pm_5am_sun"></textarea></td>
					</tr>
				</table>
            </div>
        </div>

		<div class="row">
            <div class="col-sm-12 header">
                <div class="checkbox-inline"><label><input type="checkbox" class="block-sel" id="block_16" /> MONTHLY ROUTINE (OPTIONAL)</label></div>
            </div>
            <div class="col-sm-12 block_16" style="display:none">
                <table class="table table-bordered">
                    <tr height="30px">
                        <th>WEEK</th>
						<th>1</th>
						<th>2</th>
						<th>3</th>
						<th>4</th>
					</tr>
					<tr>
						<td>ACTIVITY</td>
						<td><textarea class="form-control"  name="week_1"></textarea></td>
						<td><textarea class="form-control" name="week_2"></textarea></td>
						<td><textarea class="form-control" name="week_3"></textarea></td>
						<td><textarea class="form-control" name="week_4"></textarea></td>
					</tr>
				</table>
            </div>
        </div>

		<div class="row">
            <div class="col-sm-12 header">
                <div class="checkbox-inline"><label><input type="checkbox" class="block-sel" id="block_17" /> ENVIRONMENTAL ASSESSMENT</label></div>
            </div>
            <div class="col-sm-12 block_17" style="display:none">
                <table class="table table-bordered">
                    <tr height="30px">
                        <th width="25%">FAMILY MEMBERS</th>
						<td><textarea class="form-control" name="environmental_assessment_family_members"></textarea></td>
					</tr>
					<tr>
						<th>HOME/HOSPITAL/GUEST HOUSE</th>
						<td><textarea class="form-control" name="environmental_assessment_home_hospital_guest_house"></textarea></td>
					</tr>
				    <tr height="30px">
                        <th>BATHROOM</th>
						<td><textarea class="form-control" name="environmental_assessment_bathroom"></textarea></td>
					</tr>
					<tr>
						<th>PETS</th>
						<td><textarea class="form-control" name="environmental_assessment_pets"></textarea></td>
					</tr>
					<tr height="30px">
                        <th>ASSESSORS NOTES 12</th>
                        <td><textarea class="form-control" name="environmental_assessment_assesors_note12"></textarea></td>
                    </tr>
				</table>
            </div>
        </div>

		<div class="row">
            <div class="col-sm-12 header">
                <div class="checkbox-inline"><label><input type="checkbox" class="block-sel" id="block_18" /> FACILITIES FOR CAREGIVER</label></div>
            </div>
            <div class="col-sm-12 block_18" style="display:none">
                <table class="table table-bordered">
                    <tr height="30px">
                        <th width="25%">ACCOMMODATION</th>
						<td><textarea class="form-control" name="facilities_for_caregivers_accomodation"></textarea></td>
					</tr>
					<tr>
						<th>FOOD</td>
						<td><textarea class="form-control" name="facilities_for_caregivers_food"></textarea></td>
					</tr>
				    <tr height="30px">
                        <th>BATHROOM</th>
						<td><textarea class="form-control" name="facilities_for_caregivers_bathroom"></textarea></td>
					</tr>
					<tr>
						<th>WASHING FACILITIES</td>
						<td><textarea class="form-control" name="facilities_for_caregivers_washing_facilities"></textarea></td>
					</tr>
					<tr height="30px">
                        <th>ASSESSORS NOTES 13</th>
                        <td><textarea class="form-control" name="facilities_for_caregivers_assessors_note13"></textarea></td>
                    </tr>
				</table>
            </div>
        </div>

		<div class="row">
            <div class="col-sm-12 header">
                <div class="checkbox-inline"><label><input type="checkbox" class="block-sel" id="block_19" /> SERVICE REQUIREMENT</label></div>
            </div>
            <div class="col-sm-12 block_19" style="display:none">
                <table class="table table-bordered">
                    <tr height="30px">
						<th>SERVICES REQUIRED</th>
                        <td colspan="8">
						<div class="checkbox-inline"><label><input type="checkbox" name="sevice_required_live_in_care"> LIVE-IN-CARE</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox"  name="service_required_personal_care"> PERSONAL CARE</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="service_required_skilled_nurshing"> SKILLED NURSING</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="service_required_companion_care"> COMPANION CARE</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="service_required_counselling"> COUNSELING</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="service_required_physiotherapy"> PHYSIOTHERAPY</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="service_required_new_mother_child_care_education"> NEW MOTHER & CHILD CARE & EDUCATION</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="service_required_other(specify)"> OTHER (SPECIFY)</label><textarea class="form-control"></textarea></div>
                        </td>
                    </tr>
					<tr height="30px">
						<th>FREQUENCY</th>
                        <td colspan="2"><input type="text" name="service_frequency" class="form-control"></td>
						<th colspan="2">EXPECTED DURATION</th>
						<td colspan="4"><input type="text" name="service_expected_duration" class="form-control"></td>
					</tr>
					<tr height="30px">
						<th>GOALS & OBJECTIVES OF SERVICE</th>
                        <td colspan="8"><input type="text" name="goals_objective_service" class="form-control"></td>

					</tr>
					<tr height="30px">
						<th rowspan="3">SCHEDULE</th>
                        <td colspan="8">
						<div class="checkbox-inline"><label><input type="checkbox" name="schedule_on_call"> ON CALL</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="schedule_24_7"> 24/7</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="schedule_everyday"> EVERYDAY</label></div>
                        <div class="checkbox-inline"><label><input type="checkbox" name="schedule_designed_days"> DESIGNATED DAYS</label></div>
                        </td>
					</tr>
					<tr>
						<th>Days</th>
							<td>
								<div class="checkbox-inline"><label><input type="checkbox" name="days_mon"> MON</label></div>
							</td>
							<td>
								<div class="checkbox-inline"><label><input type="checkbox" name="days_tue"> Tue</label></div>
							</td>
							<td>
								<div class="checkbox-inline"><label><input type="checkbox" name="days_wed"> WED</label></div>
							</td>
							<td>
								<div class="checkbox-inline"><label><input type="checkbox" name="days_thu"> THU</label></div>
							</td>
							<td>
								<div class="checkbox-inline"><label><input type="checkbox" name="days_fri"> Fri</label></div>
							</td>
							<td>
								<div class="checkbox-inline"><label><input type="checkbox"  name="days_sat"> Sat</label></div>
							</td>
							<td>
								<div class="checkbox-inline"><label><input type="checkbox" name="days_sun"> Sun</label></div>
							</td>
                    </tr>
					<tr height="30px">
						<th>Time</th>
                        <td><textarea class="form-control" name="time1"></textarea></td>
                        <td><textarea class="form-control" name="time2"></textarea></td>
                        <td><textarea class="form-control" name="time3"></textarea></td>
                        <td><textarea class="form-control" name="time4"></textarea></td>
                        <td><textarea class="form-control"  name="time5"></textarea></td>
                        <td><textarea class="form-control" name="time6"></textarea></td>
                        <td><textarea class="form-control" name="time7"></textarea></td>
					</tr>
					<tr height="30px">
						<th>SERVICE START</th>

						<td colspan="2"><textarea class="form-control" name="service_start"></textarea>
						</td>
						<th colspan="2"> SERVICE DURATION </th>
						<td colspan="4">
							<div class="checkbox-inline"><label><input type="checkbox" name="service_duration_long_term"> Long Term</label></div>
                            <div class="checkbox-inline"><label><input type="checkbox" name="service_duration_short_term"> Short Term</label></div>
						</td>
                    </tr>
				</table>
            </div>
        </div>

		<div class="row">
            <div class="col-sm-12 header">
                <div class="checkbox-inline"><label><input type="checkbox" class="block-sel" id="block_20" /> ADDITIONAL INFORMATION / TREATMENTS / NOTES</label></div>
            </div>
            <div class="col-sm-12 block_20" style="display:none">

				<textarea rows="" cols="" class="form-control" name="additional_information_treatments_notes"></textarea>
            </div>
        </div>

		<table class="table table-bordered" style="margin-top: 5px">
			<tr height="30px">
				<th>Assessors Name</th>
                <td><input type="text" class="form-control" name="assesors_name" /></td>
            </tr>
            <tr height="30px">
                <th>Designation</th>
                <td><input type="text" class="form-control" name="designation" /></td>
            </tr>
            <tr height="30px">
                <th>Mobile</th>
                <td><input type="text" class="form-control"   name="assessor_mobile" /></td>
            </tr>
            <tr height="20px"><td colspan="2">&nbsp;</td></tr>
            <tr height="30px">
				<th>Client/ Guardians Name</th>
                <td><input type="text"  class="form-control" name="client_guardian_name" /></td>
			</tr>
            <tr height="30px">
				<th>Relationship</th>
                <td><input type="text" class="form-control" name="relationship" /></td>
			</tr>
            <tr height="30px">
				<th>Mobile</th>
                <td><input type="text" class="form-control" name="client_mobile" /></td>
			</tr>
		</table>

        @if(isset($form))
        <div class="row authorization" style="margin-top: 10px">
            <div class="col-sm-12 header">
                <div class="checkbox-inline"><label>CLIENT OR GUARDIAN AUTHORIZATION</label></div>
            </div>
            <div class="col-sm-12">
                <p style="padding: 5px;text-align:justify">The information contained within this document is not shared with any third parties unless requested in writing by the client or guardian. The information is kept in the company's client file for as long as services are being rendered. Upon termination of services the document is destroyed in a timely manner or retained if required by law. The document is used as a guide and reference to essential client care information. The Client or Legal Guardian, by signing this document, gives the company consent to collect the information contained herein and to use it for the specified purpose.</p>
            </div>
        </div>

        <div class="signature-block" style="width: 95%;margin: 0 auto; display:none;margin-top:10px">
            <b>Signature (Client / Guardian)</b><br>
            <img id="signature_image" style="width: 150px;" src=""/>
        </div>
        @endif
        @if(!isset($form))
        <div class="row" style="margin-top: 10px; margin-bottom: 10px; padding: 10px;">
            <a class="btn btn-success btn-block btnProceed" style="margin: 5px 15%; color: #fff;">Save Assessment</a>
        </div>
        @endif

        @if(isset($case_forms_id))
        <div class="row" style="margin-top: 10px; margin-bottom: 10px; padding: 10px;">
            <a class="btn btn-success btn-block btnUpdate" style="margin: 5px 15%; color: #fff;">Update Assessment</a>
        </div>
        @endif
    </form>
    <input type="hidden" name="lead_id" value="{{ isset($id)?$id:Helper::encryptor('encrypt',0) }}" />
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

        $('input, select, textarea').each(function(){
            if(!$(this).hasClass('block-sel')){
                $(this).attr('tabindex',tabCount);
                tabCount++;
            }
        });

        $(document).keypress(function(e) {
            if(e.which == 13) {
                // Do something here if the popup is open
                var index = parseInt($(e.target).attr('tabindex')) + 1;
                $('*[tabindex='+index+']').focus();
            }
        });

        $('input[type=checkbox]').each(function(i){
            if(!$(this).hasClass('block-sel')){
                $(this).attr('value',$(this).parent().text().trim());
            }
        });

        $('input, select, textarea').each(function(){
            if(!$(this).hasClass('block-sel')){
                block = $(this).closest('.row').find('.block-sel').attr('id');
                if(typeof block != 'undefined'){
                    $(this).attr('name',block+'_'+$(this).attr('name'));
                }
            }
        });

        $(document).on('click','.block-sel', function(){
            $('.'+$(this).attr('id')).toggle();
        });

        $(document).on('click','.btnProceed', function(){
            formData = JSON.stringify(getFormData($('#assessment_form')));
            if(formData){
                $('.btnProceed').attr('disabled',true);
                $.ajax({
                    url: '{{ route('lead.save-detailed-assessment') }}',
                    type: 'POST',
                    data: {_token: '{{ csrf_token() }}','lead_id': $('input[name=lead_id]').val(), 'form_data': formData },
                    success: function (data){
                        console.log(data.res);
                        if(data.res == true){
                            alert("Assessment saved successfully");
                            window.opener.location.reload(true);
                            window.close();
                        }else{
                            alert("Could not save assessment!");
                            $('.btnProceed').attr('disabled',false);
                            return;
                        }
                    },
                    error: function (err){
                        console.log(err);
                    }
                })
            }
        });

        @if(isset($case_forms_id))
            $(document).on('click','.btnUpdate', function(){
                formData = JSON.stringify(getFormData($('#assessment_form'))).replace(/'/g, "");
                if(formData){
                    $('.btnUpdate').attr('disabled',true);
                    $.ajax({
                        url: '{{ route('lead.update-detailed-assessment') }}',
                        type: 'POST',
                        data: {_token: '{{ csrf_token() }}','case_forms_id': '{{$case_forms_id}}', 'form_data': formData },
                        success: function (data){
                            console.log(data.res);
                            if(data.res == true){
                                alert("Assessment updated successfully");
                                window.opener.location.reload(true);
                                window.close();
                            }else{
                                alert("Could not update assessment!");
                                $('.btnUpdate').attr('disabled',false);
                                return;
                            }
                        },
                        error: function (err){
                            console.log(err);
                        }
                    })
                }
            });
        @endif

        Array.prototype.remove = function() {
            var what, a = arguments, L = a.length, ax;
            while (L && this.length) {
                what = a[--L];
                while ((ax = this.indexOf(what)) !== -1) {
                    this.splice(ax, 1);
                }
            }
            return this;
        };

        function getFormData($form){
            var unindexed_array = $form.serializeArray();
            var indexed_array = {};

            $.map(unindexed_array, function(n, i){
                if(n['value'] != ''){
                    idArray = n['name'].split("_");
                    temp = n['name'].split("_");
                    temp.splice(0, 2);
                    var key = temp.join("_");
                    if(idArray.length > 2 && idArray[0] == 'block'){
                        if(typeof indexed_array[idArray[0]+'_'+idArray[1]] == 'undefined'){
                            indexed_array[idArray[0]+'_'+idArray[1]] = {};
                        }
                        indexed_array[idArray[0]+'_'+idArray[1]][key] = n['value'];
                    }else{
                        if(typeof indexed_array['others'] == 'undefined'){
                            indexed_array['others'] = {};
                        }
                        indexed_array['others'][n['name']] = n['value'];
                    }
                }
            });

            return indexed_array;
        }

        var tabCount = 1;
        var formData = [];

        var json = `{!! isset($form)?$form->form_data:'' !!}`;
        if(json) getFilledFormData(json);

        function getFilledFormData(json){
            obj = JSON.parse(json);
            // console.log(obj);
            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                    var val = obj[key];
                    $('input[type=checkbox][id='+key+']').prop('checked',true);
                    $('.'+key).show();
                    for (var iKey in val){
                        if(val.hasOwnProperty(iKey)){
                            if(key == 'others'){
                                var target = $('[name='+iKey+']');
                            }else{
                                var target = $('.'+key+' *[name='+key+'_'+iKey+']');    
                            }
                            // console.log('.'+key+' *[name='+key+'_'+iKey+']');
                            if(typeof target != 'undefined' && target.length){
                                if(target.is('input[type=checkbox]')){
                                    $('.'+key+' input[name='+key+'_'+iKey+']').prop('checked',true);
                                }
                                
                                if(target.is('input[type=text]')){
                                    target.val(val[iKey]);
                                }
                                
                                if(target.is('input[type=email]')){
                                    target.val(val[iKey]);
                                }
                                
                                if(target.is('textarea')){
                                    target.text(val[iKey]);
                                }

                                if(target.is('input[type=number]')){
                                    target.val(val[iKey]);
                                }

                                if(target.is('input[type=radio]')){
                                    $('.'+key+' input[name='+key+'_'+iKey+'][value="'+val[iKey]+'"]').prop('checked',true);
                                }
                                // console.log(iKey+' -> '+val[iKey]);
                            }
                        }
                    }
                }
            }

            @if(!isset($case_forms_id))
                $('.block-sel').each(function(){
                    if(!$(this).is(':checked')){
                        $(this).closest('.row').hide();
                    }
                });
            @endif

            // Set Signature Image
            var imagePath = '{!! isset($form)?$form->signature_file:'' !!}';
            if(imagePath != '' && imagePath != null){
                $('.signature-block').show();
                $('.signature-block #signature_image').attr('src',imagePath);
            }

            @if(!isset($case_forms_id))
                $('input, select, textarea').prop('disabled',true);
                $('input, select, textarea').prop('readonly',true);
                $('.btnProceed').hide();
            @endif
        
            $('.authorization').hide();
            $('tr td').css('color','#00bcd4');
        }
    });
</script>
</html>
