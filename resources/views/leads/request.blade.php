@extends('layouts.main-layout')

@section('page_title','View Lead - Requests |')

@section('active_lead_requests','active')
@section('plugin.styles')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<!-- Bootstrap Tagsinput Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
@endsection

@section('page.styles')
<style>
	.table-bordered tbody tr td, .table-bordered tbody tr th {
		text-align: center;
	}
    .dt-buttons{
        position: static !important;
        float: right;
    }
    .paginate_button {
        cursor: pointer;
        margin:2px;
    }
    @page{size:auto; margin:5mm;}
</style>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="row clearfix">
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <h2 class="col-sm-9">
                    Lead Requests
                    <small>List of all lead reqests</small>
                </h2>
            </div>
            <div class="body">
                <table id="leadRequests" class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable">
                    <thead>
                        <th width="10%">#</th>
                        <th width="30%">Date</th>
                        <th width="10%">Enquirer Name</th>
                        <th width="10%">Phone</th>
                        <th width="20%">Email</th>
                        <th width="10%">Location</th>
                        <th width="10%">Requirement</th>
                    </thead>
                    <tbody>
                        @forelse($leadRequests as $i => $lr)
                        <tr>
                            <td>{{ $i + 1 }}</td>
                            <td>
                                {{ $lr->created_at->format('d-m-Y') }}<br>
                                <small>{{ $lr->created_at->format('h:i A') }}</small>
                            </td>
                            <td>
                                {{ $lr->name }}<br>
                            </td>
                            <td>
                                {{ $lr->phone_number }}<br>
                            </td>
                            <td>
                                {{ $lr->email ?? '-' }}
                            </td>
                            <td>
                                {{ $lr->city ?? '-' }}
                            </td>
                            <td>{{ $lr->requirement }}
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="9" class="text-center">
                                No lead(s) found.
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@endsection

@section('plugin.scripts')
<!-- Select Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<!-- Input Mask Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>
<!-- Moment Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<!-- Bootstrap Tags Input Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>

@endsection

@section('page.scripts')
<script>
$(document).ready(function() {
    $('#leadRequests').DataTable( {
        dom: 'Bfrtip',
        // buttons: [
        //     'excel', 'pdf', 'print'
        // ]
        buttons: [
        'excel',
        {
            extend: 'pdfHtml5',
            pageSize: 'A3'
        },
        {
                        extend: 'print',
                        title: '<h3 style="text-align:center;">Lead Request Roster</h3>',
                        customize: function ( win ) {
                            $(win.document.body)
                                .css( 'font-size', '7pt' );
         
                            $(win.document.body).find( 'table' )
                                .addClass( 'compact' )
                                .css( 'font-size', 'inherit' );
                        }
                    }
        ]
    } );
} );
</script>
@endsection