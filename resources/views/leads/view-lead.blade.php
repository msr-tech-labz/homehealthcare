@extends('layouts.main-layout')

@section('page_title','View Case - Case |')

@section('active_cases','active')

@section('plugin.styles')
	<!-- Bootstrap Select Css -->
	<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
	<!-- Pepper Grinder Css for Multi Datespicker-->
	<link href="https://code.jquery.com/ui/1.12.1/themes/pepper-grinder/jquery-ui.css" rel="stylesheet" />
	<!-- Multiple Dates Picker Css-->
	<link href="https://cdn.rawgit.com/dubrox/Multiple-Dates-Picker-for-jQuery-UI/master/jquery-ui.multidatespicker.css" rel="stylesheet" />
	<!-- Bootstrap Tagsinput Css -->
	<link href="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
	<!-- NoUiSlider Css -->
	<link href="{{ ViewHelper::ThemePlugin('nouislider/nouislider.min.css') }}" />
	<!-- Font Awesome Css -->
	<link href="{{ ViewHelper::ThemePlugin('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
	<!-- Bootstrap datetimepicker Css -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">

	<!-- Med Autocomplete Css -->
	<link rel="stylesheet" href="https://clin-table-search.lhc.nlm.nih.gov/autocomplete-lhc-versions/13.0.2/autocomplete-lhc_jQueryUI.min.css"/>
	<!-- Print Css -->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/print.css') }}" media="print" />
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section('page.styles')
	<link rel="stylesheet" href="{{ ViewHelper::css('vtabs.css') }}" />
	<style>
		.card{
			box-shadow: none !important;
		}
		.flatpickr-calendar{
			left: 294.109px !important;
		}
		.daterangepicker{
			top: 22% !important;
		}
		#searchResults{
			z-index: 9999 !important;
			left:40% !important;
			left: 36% !important;
			width: 17%;
		}
		.autocomp_selected {
			width: 100% !important;
		}
		.jcrop-active{
			width: 300px !important;
			height: 300px !important;
		}
		.modal-body .row{
			margin-bottom: 5px !important;
		}
		.modal-body{
			padding-top: 2px !important;
		}
		.modal-body h4.card-inside-title{
			font-size: 14px;
			color: #009688 !important;
		}
		.bootstrap-datetimepicker-widget{
			width: 300px !important;
		}
		.bootstrap-datetimepicker-widget table td.disabled{
			color: red !important;
		}
		.bootstrap-notify-container{
			z-index: 99999 !important;
		}
		.ui-datepicker .ui-datepicker-calendar .ui-state-highlight a{
			background: #4caf50 none !important;
			color: #fff !important;
		}

		.daterangepicker td.highlight, .daterangepicker option.highlight{
		    color: #ff9800 !important;
		}
		.multi-col{
			-webkit-column-count: 4; /* Chrome, Safari, Opera */
			-moz-column-count: 4; /* Firefox */
			column-count: 4;
			-webkit-column-gap: 20px; /* Chrome, Safari, Opera */
			-moz-column-gap: 20px; /* Firefox */
			column-gap: 20px;
			-webkit-column-rule: 1px solid lightblue; /* Chrome, Safari, Opera */
			-moz-column-rule: 1px solid lightblue; /* Firefox */
			column-rule: 1px solid lightblue;
		}
		.multi-col-worklog{
			-webkit-column-count: 2; /* Chrome, Safari, Opera */
			-moz-column-count: 2; /* Firefox */
			column-count: 2;
			-webkit-column-gap: 20px; /* Chrome, Safari, Opera */
			-moz-column-gap: 20px; /* Firefox */
			column-gap: 20px;
			-webkit-column-rule: 1px solid lightblue; /* Chrome, Safari, Opera */
			-moz-column-rule: 1px solid lightblue; /* Firefox */
			column-rule: 1px solid lightblue;
		}
		@page
		{
			size: auto;   /* auto is the initial value */
			margin: 0mm;  /* this affects the margin in the printer settings */
		}

		.light-red{
			background-color: rgba(239, 152, 152, 0.46) !important;
		}
		.light-blue{
			background-color: rgba(51, 153, 255, 0.46) !important;
		}
		.form-control-label{
			text-align: left;
		}
		.form-group .form-line, .form-group{
			text-transform: uppercase; !important;
		}
		.form-group label{
			text-transform: capitalize !important;
		}
		.pac-container {
			z-index: 9999 !important;
		}
		.nav-tabs > li{
			/*min-width: 12%;*/
		}
		.card .header .header-dropdown{
			top: 15px;
		}
		hr{
			margin-top: 10px;
			margin-bottom: 10px;
		}
		.staffSearchResults thead tr th{
			font-size: 12px !important;
		}
		.table-patient tbody tr td, .table-patient tbody tr th{
			padding: 5px !important;
			vertical-align: middle;
			height: 20px !important;
			border: 1px solid #f6f6f6;
			color: #777;
			font-size: 0.9em;
			font-weight: 600;
			white-space: normal;
			width: 150px;
		}
		.table-patient tbody tr td{
			font-weight: 500;
			color: #333;
			letter-spacing: 0.05em;
			padding-left: 8px !important;
		}
		.table-schedules thead tr th, .table-schedules tbody tr td{
			font-size: 12px !important;
		}
		.condensed thead tr th{
			padding: 5px 10px;
		}
		.table-borderless tbody tr td, .table-borderless tbody tr th{
			border: none;
			padding: 6px;
		}
		.table-borderless tbody tr td{
			min-width: 150px;
		}
		.routines tbody tr td{
			white-space: nowrap;
			width: 25% !important;
		}
		.table-borderless tbody tr td:before{
			content: ' :';
			padding-right: 10px;
			font-weight: bold;
		}
		.bootstrap-tagsinput{
			min-height: 40px;
			vertical-align: top;
			padding: 4px 0;
			width: 100%;
		}
		.bootstrap-tagsinput input{
			padding-left: 0;
		}
		.plus:after{
			content: ' + ';
			font-size: 18px;
			font-weight: bold;
		}
		.care-plan-card .header{
			padding: 15px 20px;
		}
		.modal-lg{
			width: 70%;
		}
		.modal .modal-header{
			padding: 20px 25px 15px 25px;
		}
		.modal-footer{
			border-top: 1px solid #ddd !important;
		}
		.details-pane .details{
			margin-top: 6px;
		}
		.details-pane .details .form-line{
			border-bottom: none;
		}
		.details-pane .details .form-line .form-control, .details-pane .details .form-line .form-control:focus{
			outline: 0;
			border-bottom: 1px solid #ff5722 !important;
		}

		.card .body .col-lg-9, .card .body .col-lg-3{
			margin-bottom: 8px !important;
		}
		.careplan-status button.waves-effect{
			width: 160px !important;
		}
		.comment-card { margin-bottom: 15px;}
		.comment-card .header { padding: 5px 10px;}
		.comment-card .body { padding: 10px;}
		.comment-card .header h2 {font-size: 16px}
		.comment-card .header .header-dropdown li > span {font-size: 13px}
		.comment-card th{ background: #ccc}
		.comment-card .table{ margin-bottom: 0}

		.details-pane div.col-lg-8, .details-pane div.col-lg-4{ margin-bottom: 8px !important}

		.noUi-horizontal .noUi-handle{
			background-color: #ff0000;
		}
		.text-muted {
			color: #777;
			position: absolute;
			right: 10%;
		}
		.search-table-outter {border:2px solid #b5b5b5;}
		.search-table, td, th{border-collapse:collapse; border:none;}
		th{padding:7px 7px; font-size:1em; font-weight: 600; color:#555; background:#f6f6f6;}
		td{padding:7px 10px; height:35px; font-size:1em; font-weight: 500; color:#333;}
		.search-table-outter {
			overflow-x: scroll;
		}

		.card-inside-title{
			color: #444 !important;
		}

		.condensed tbody tr td, .condensed tbody tr th{
			padding: 3px 10px !important;
			line-height: 1.5 !important;
			font-size: 12px;
		}

		.dataTables_length .bootstrap-select{
			display: inline !important;
		}
		.dataTables_empty{
			text-align: center !important;
		}

		#ui-datepicker-div{
			top:640px !important;
		}
		/* Map Styles */
		#infowindow-content .title {
			font-weight: bold;
		}

		#infowindow-content {
			display: none;
		}

		#map #infowindow-content {
			display: inline;
		}

		.pac-card {
			margin: 10px 10px 0 0;
			border-radius: 2px 0 0 2px;
			box-sizing: border-box;
			-moz-box-sizing: border-box;
			outline: none;
			box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
			background-color: #fff;
			font-family: Roboto;
			right: -1px;
			width: 75%;
		}

		#pac-container {
			padding-bottom: 12px;
			margin-right: 12px;
		}

		#pac-input {
			width: 99%;
			background-color: #fff;
			font-family: Roboto;
			font-size: 15px;
			font-weight: 300;
			margin-left: 12px;
			margin-top: 5px;
			padding: 0 11px 0 13px;
			text-overflow: ellipsis;
			/*width: 400px;*/
		}

		#pac-input:focus {
			border-color: #4d90fe;
		}

		#title {
			color: #fff;
			background-color: #4d90fe;
			font-size: 20px;
			font-weight: 500;
			padding: 6px 12px;
		}

		#viewSchedulesModal .dataTables_wrapper > .top > div{
			min-width: 25% !important;
			display: inline-block !important;
			margin: 0 10px !important;
			margin-bottom: 20px;
		}

		#viewSchedulesModal{
			left: -15%;
			width: 130%;
		}

		#viewSchedulesModal .dataTables_paginate{
			text-align: center !important;
			vertical-align: middle !important;
		}

		#viewSchedulesModal .dataTables_paginate > .pagination{
			margin: 0 !important;

		}

		#viewSchedulesModal .dataTables_filter{
			float: right !important;
		}

		.demo-checkbox label, .demo-radio-button label {
			min-width: 0;
		}

		.btn:not(.btn-link):not(.btn-circle){
			box-shadow: none !important;
		}

		.current-schedule-chargeable:hover{
			color: #fff !important;
		}
		.chargeability{
			display: unset !important;
		}
		.schedule-sel[type="checkbox"] + label {
		    margin-bottom: 0px !important;
		    margin-top: 8px;
		}
		.caret-right{
			border-left: 4px solid white;
			    border-right: 0;
			    border-top: 4px solid transparent;
			    border-bottom: 4px solid transparent;
		}
		#createSchedulesForm .dataTables_scrollBody{
			overflow: inherit !important;
		}
		.dataTables_scrollHeadInner{
			padding-left:0 !important; 
		}
		.swal-button--notdone {
		  background-color: red;
		  border: 1px solid red;
		}
		.swal-button--done {
		  background-color: green;
		  border: 1px solid green;
		}
		.popover .popover-content {
		    font-size: 13px !important;
		    color: #000 !important;
		}
	</style>
@endsection

@section('content')

	@if (count($errors) > 0)
	<div class="row clearfix">
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	</div>
	@endif

	<div class="loader2"></div>
	<div class="row hidden-print">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card" style="min-height: 138px;">
				<div class="header" style="padding-bottom:5px">
					<?php if($l->status == 'Pending' || $l->status =='Follow Up'){$backPage='index';}else{$backPage='operations';} ?>
					<a href="{{ route('lead.'.$backPage) }}" class="pull-left btn btn-info btn-circle clearLocalStore waves-effect waves-circle waves-float">
						<i class="material-icons">arrow_back</i>
					</a>
					<h2 class="col-sm-8">
						{{ isset($l)?'View':'New'}}
						{{ isset($l) && in_array($l->status,["Pending","Follow Up"])?'Lead':'Episode'}} - {{ $l->episode_id ?? 'TEMPLD0AF'.$l->id }}

						<small>Patient Case File</small>
					</h2>
					
					<a href="{{ route('lead.prevservicerecords',Helper::encryptor('encrypt',$l->patient->id)) }}" target="_blank" class="pull-right btn btn-primary"><span>Previous Records</span></a>
					<div class="col-sm-2 pull-right" style="width:20%">
						<span class="pull-left">Patient ID: <b>{{ isset($l->patient->patient_id)?$l->patient->patient_id:'TEMPPT0AF'.$l->patient->id }}</b></span><br>
						<span class="pull-left">Status: <b>{{ $l->status }}</b>&nbsp;&nbsp;
						@ability('admin','leads-disposition-add')
							@caseNotClosed($l->status)
							<button type="button" class="btn bg-cyan btn-circle waves-effect waves-circle waves-float btnChangeLeadStatus" style="height:30px;width:30px;line-height:1">
								<i class="material-icons" style="left: -4px !important;font-size:15px">edit</i>
							</button>
							@endCaseNotClosed
						@endability
						</span>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="body" style="padding: 10px 8px;">
					<div class="row">
						<div class="@if(empty($l->special_instructions)){{ 'col-lg-12 col-md-12 col-sm-12 col-xs-12' }}@else{{ 'col-lg-9 col-md-9 col-sm-9 col-xs-12' }}@endif" style="margin-bottom:0px !important;padding-right:0;@if(empty($l->special_instructions)){{ 'width:98%' }}@endif">
							<div class="card" style="margin-bottom:10px !important;margin-left:15px">
								<div class="body">
									<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style="margin-top: 5px;margin-bottom:7px;padding-left:10px;text-align: center;">
										@if(file_exists(public_path('uploads/provider/'.session('tenant_id').'/case_documents/'.Helper::encryptor('encrypt',$l->id))))
											{? $path = url('uploads/provider/'.session('tenant_id').'/case_documents/'.Helper::encryptor('encrypt',$l->id).'/'.Helper::encryptor('encrypt',$l->patient_id).'.jpg'); ?}
										@else
											{? $path = '/img/'; ?}
											{? $path .= in_array($l->patient->gender,['Male','Female'])?'icon_'.strtolower($l->patient->gender).'.jpg':'default-avatar.jpg'; ?}
										@endif
										<img class="img-responsive img-thumbnail" style="width:120px;height:120px" src="{{ $path }}" />
										<center><a class="btn btn-xs btn-warning text-center" data-toggle="modal" data-target="#patientPictureModal"><i style="font-size:15px" class="material-icons">edit</i> Change Picture</a></center>
										<br>
										{{-- <a href="{{ route('patient.addemailaddress',Helper::encryptor('encrypt',$l->patient->id)) }}" target="_blank" class="btn btn-primary"><span>Email - Address</span></a> --}}
										<a href="{{ route('patient.addemailaddress',Helper::encryptor('encrypt',$l->patient->id)) }}" target="_blank">
										<button type="button" class="btn btn-sm btn-primary waves-effect">
											<i class="material-icons">add_circle_outline</i>
											<span style="position: relative;top: -2px;margin-left: 3px;">Email - Address</span>
										</button>
										</a>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" style="padding-left:0;height:inherit;margin-bottom:-1px !important;">
										<table class="table table-patient condensed">
											<tr>
												<th>Patient Name</th>
												<td colspan="3">{{ $l->patient->full_name ?? '' }}</td>
												<th>Patient ID</th>
												<td>{{ $l->patient->patient_id ?? '' }}</td>
											</tr>
											<tr>
												<th>Gender</th>
												<td>{{ $l->patient->gender ?? '' }}</td>
												<th>Age</th>
												<td>{{ $l->patient->patient_age ?? '' }}</td>
												<th>Weight (kg)</th>
												<td>{{ $l->patient->patient_weight ?? '' }}</td>
											</tr>
											<tr>
												<th>Contact Number</th>
												<td><a href="tel:{{ $l->patient->contact_number ?? '' }}">{{ $l->patient->contact_number ?? '' }}</a></td>
												<th>Email</th>
												<td colspan="3">{{ $l->patient->email ?? '' }}</td>
											</tr>
											<tr>
												<th>Address</th>
												<td colspan="5">
													{{ !empty($l->patient->street_address)?($l->patient->street_address.','):'' }}
													{{ !empty($l->patient->area)?($l->patient->area.','):'' }}
													{{ !empty($l->patient->city)?($l->patient->city.' - '):''}}
													{{ !empty($l->patient->zipcode)?($l->patient->zipcode.','):'' }}
													{{ !empty($l->patient->state)?$l->patient->state:'' }}
													@if(!empty($l->patient->area) || (!empty($l->patient->latitude) && !empty($l->patient->longitude)))
													| <a href="javascript:void(0);" data-target="#patientLocationModal" data-toggle="modal">View Map</a>

													<div class="modal fade" id="patientLocationModal" aria-labelledby="modalLabel" role="dialog" tabindex="-1">
														<div class="modal-dialog" role="document" style="width:60%">
															<div class="modal-content">
																<div class="modal-header bg-cyan">
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																	<h5 class="modal-title" id="modalLabel">Patient Location</h5>
																</div>
																<div class="modal-body">
																@if(!empty($l->patient->latitude) && !empty($l->patient->longitude))
																	{? $url = $l->patient->latitude.",".$l->patient->longitude; ?}
																@else
																	<br><span><strong class="col-red">Warning!</strong> Map shown below may not have patient's exact location</span><br><br>
																	{? $url = $l->patient->area.','.$l->patient->city.','.$l->patient->state; ?}
																@endif
																	<iframe src="https://maps.google.com/maps?q={{ $url }}&z=14&amp;output=embed" width="770" height="400" frameborder="0" style="border:0"></iframe>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
																</div>
															</div>
														</div>
													</div>
													@endif
												</td>
											</tr>
											<tr>
												<th>Enquirer</th>
												<td>{{ $l->patient->enquirer_name ?? '' }}</td>
												<th>Contact Number</th>
												<td colspan="3"><a href="tel:{{ $l->patient->alternate_number ?? '' }}">{{ $l->patient->alternate_number ?? '' }}</a></td>
											</tr>
										</table>
										<button type="button" data-toggle="modal" data-target="#checklistFormModal" class="pull-right btn btn-warning btnEditCase waves-effect">
											<i class="material-icons">add</i>Checklist form
										</button>
									</div>
									@ability('admin','leads-patient-details-add,leads-patient-details-edit')
									<div class="col-lg-2">
										<button type="button" data-toggle="modal" title="Edit Patient" class="btn bg-teal btn-circle pull-right waves-effect waves-circle waves-float editPatient">
											<i class="material-icons">edit</i>
										</button>
									</div>
									@endability
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						@if(!empty($l->special_instructions))
							<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="width:23.8%;">
								<div class="card" style="margin-bottom: 0">
									<div class="header bg-orange" style="padding: 5px">
										<h2 style="font-size:16px;">
											Special Instructions
										</h2>
									</div>
									<div class="body" style="padding: 5px;height: 138px;overflow-y:auto;font-size: 15px">
										{{ $l->special_instructions }}
									</div>
								</div>
							</div>
						@endif
						<div class="clearfix"></div>
					</div>

					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 vertical-tab-container" style="margin-bottom: 0 !important">
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 vertical-tab-menu">
							<div class="list-group">
								<a href="#" class="list-group-item active text-center">
									<h4 class="material-icons">assignment</h4><br/>Case Activity
								</a>
								<a href="#" class="list-group-item text-center">
									<h4 class="material-icons">info</h4><br/>Case &amp; Assessment
								</a>
								<a href="#" class="list-group-item text-center">
									<h4 class="material-icons">receipt</h4><br/>Services &amp; Schedules
								</a>
								<a href="#" class="list-group-item text-center">
									<h4 class="material-icons">attach_money</h4><br/>Billables
								</a>
								<a href="#" class="list-group-item text-center">
									<h4 class="material-icons">cloud_upload</h4><br/>Documents
								</a>
								<a href="#" class="list-group-item text-center">
									<h4 class="material-icons">view_list</h4><br/>Tasks
								</a>
							</div>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 vertical-tab">
							<!-- case history section -->
							<div class="vertical-tab-content active">
								<div class="row">
									@ability('admin','check-audit-log')
										<a href="{{ route('lead.auditlog',Helper::encryptor('encrypt',$l->id)) }}" target="_blank" class="btn btn-primary"><span>Check Audit Log</span></a>&nbsp;
									@endability
									@ability('admin','leads-comments-add')
										<a class="btn btn-primary pull-right" data-toggle="modal" data-target="#commentModal">+ Add Comment</a>
									@endability
								</div>
								<div class="clearfix"></div>
								<hr>
								<div class="row">
									@ability('admin','leads-comments-view')
									<div class="col-md-6" style="overflow-y: scroll;max-height: 500px;min-height:500px;">
										<h2 class="card-inside-title">Case History</h2>
										{{-- {!! AuditHelper::getCaseHistory($l->id); !!} --}}
										@if(isset($l->dispositions) && count($l->dispositions))
										@foreach ($l->dispositions as $disposition)
										<div class="card comment-card">
											<div class="header bg-light-blue">
												<h2>Disposition: </h2>
												<small>{{ $disposition->status }} on @if($disposition->status =="Follow Up") {{ $disposition->follow_up_datetime->format('d M Y, h:i A') }} @else {{ $disposition->created_at->format('d M Y, h:i A') }} @endif @if(isset($disposition->user)){!! ' - <b>'.$disposition->user->full_name.'</b>' !!}@endif</small>
											</div>
											<div class="body">
												{!! $disposition->comment !!}
											</div>
										</div>
										@endforeach
										@endif
									</div>
									@endability
									@ability('admin','leads-disposition-view')
									<div class="col-md-6" style="border-left: 1px solid #ccc;min-height:500px;overflow-y: scroll;max-height: 500px;">
										<h2 class="card-inside-title">Comments</h2>
										@if(isset($l->comments) && count($l->comments))
										@foreach ($l->comments as $comment)
										<div class="card comment-card">
											<div class="header bg-cyan">
												<h2>{{ $comment->user_name }}</h2>
												<ul class="header-dropdown m-r--5" style="top: 5px;">
													<li><small>{{ $comment->created_at->format('d M Y, h:i A') }}</small></li>
												</ul>
											</div>
											<div class="body">
												{!! $comment->comment !!}
											</div>
										</div>
										@endforeach
										@endif
									</div>
									@endability
								</div>
							</div>

							<!-- case and assessment section -->
							<div class="vertical-tab-content">
								<h2 class="col-lg-6 col-md-6 col-sm-6 col-xs-6  card-inside-title">
									Case Details
									<small>Case Description and assessment details</small>
								</h2>
								@ability('admin','leads-case-details-edit')
								<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 pull-right" style="margin-bottom:0;margin-top:0">
									<button type="button" data-toggle="modal" data-target="#editCaseModal" class="pull-right btn btn-warning btnEditCase waves-effect">
										<i class="material-icons">edit</i>
										<span>Edit Case details</span>
									</button>
								</div>
								@endability
								@ability('admin','leads-case-details-view')
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-left:5px;padding-left:0;height:inherit;margin-bottom:7px;padding-left:0px;">
							    	<table class="table table-patient condensed">
										<tr>
											<th>Case Description</th>
											<td>{{ $l->case_description ?? '' }}</td>
											<th>Medical Conditions</th>
											<td>{{ $l->medical_conditions ?? '' }}</td>
										</tr>
										<tr>
											<th>Hospital Name</th>
											<td>{{ $l->hospital_name ?? '' }}</td>
											<th>Medications</th>
											<td>{{ $l->medications ?? '' }}</td>
										</tr>
										<tr>
											<th>Primary Doctor</th>
											<td>{{ $l->primary_doctor_name ?? '' }}</td>
											<th>Procedures</th>
											<td>{{ $l->procedures ?? '' }}</td>
										</tr>
										<tr>
											<th>Service Requested</th>
											<td>{{ $l->serviceRequired->service_name ?? '' }}</td>
											<th>Estimated Duration</th>
											<td>{{ $l->estimated_duration ?? '' }}</td>
										</tr>
										<tr>
											<th>Gender Preference</th>
											<td>{{ $l->gender_preference ?? '' }}</td>
											<th>Language Preference</th>
											<td>{{ $l->language_preference ?? '' }}</td>
										</tr>
										<tr>
											<th>Status</th>
											<td>
											@ability('admin','leads-disposition-add')
												@caseNotClosed($l->status)
												<div class="btn-group">
													<button type="button" class="btn btn-info waves-effect current-lead-status" data-id="">{{ $l->status }}</button>
													<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
														<span class="caret"></span>
														<span class="sr-only">Toggle Dropdown</span>
													</button>
													<ul class="dropdown-menu">
														<li><a href="javascript:void(0);" data-status="Pending" data-id="{{ Helper::encryptor('encrypt',$l->id) }}" class="lead-status waves-effect waves-block">Pending</a></li>
														<li><a href="javascript:void(0);" data-status="Follow Up" data-id="{{ Helper::encryptor('encrypt',$l->id) }}" class="lead-status waves-effect waves-block">Follow Up</a></li>
														<li><a href="javascript:void(0);" data-status="Assessment Pending" data-id="{{ Helper::encryptor('encrypt',$l->id) }}" class="lead-status waves-effect waves-block">Assessment Pending</a></li>
														<li><a href="javascript:void(0);" data-status="Assessment Completed" data-id="{{ Helper::encryptor('encrypt',$l->id) }}" class="lead-status waves-effect waves-block">Assessment Completed</a></li>
														<li><a href="javascript:void(0);" data-status="Converted" data-id="{{ Helper::encryptor('encrypt',$l->id) }}" class="lead-status waves-effect waves-block">Converted</a></li>
														<li><a href="javascript:void(0);" data-status="Closed" data-id="{{ Helper::encryptor('encrypt',$l->id) }}" class="lead-status waves-effect waves-block">Closed</a></li>
														<li><a href="javascript:void(0);" data-status="Dropped" data-id="{{ Helper::encryptor('encrypt',$l->id) }}" class="lead-status waves-effect waves-block">Dropped / Case Loss</a></li>
														<li><a href="javascript:void(0);" data-status="No Supply" data-id="{{ Helper::encryptor('encrypt',$l->id) }}" class="lead-status waves-effect waves-block">No Supply</a></li>
													</ul>
												</div>
												@endCaseNotClosed
											@endability
											</td>
											<th>Case Manager(s)</th>
											<td>
												{? $managerStr = "-"; ?}
												@if(isset($l->manager_id) && !empty($l->manager_id))
													{? $mArr = explode(",",$l->manager_id); ?}
													@if(count($mArr))
														{? $managerStr = ""; ?}
														@foreach ($mArr as $mgr)
															{? $managerStr .= Helper::getStaffName($mgr).", "; ?}
														@endforeach
													@endif
												@endif
												{{ rtrim($managerStr,', ') }}
											</td>
										</tr>
										<tr>
											<th>Remarks</th>
											<td colspan="3">{{ $l->remarks ?? '' }}</td>
										</tr>
									</table>
									<hr>
									<ul class="nav nav-tabs tab-nav-right" role="tablist">
									    <li role="presentation" class="active"><a href="#assessmentTab" data-toggle="tab">ASSESSMENT</a></li>
									    <li role="presentation"><a href="#followupTab" data-toggle="tab">FOLLOW-UP</a></li>
									</ul>
									<div class="tab-content">
									    <div role="tabpanel" class="tab-pane fade in active" id="assessmentTab">
											@ability('admin','leads-assessment-create')
											<button type="button" class="btn btn-primary btn-sm pull-right btnAddAssessment waves-effect" data-toggle="modal" data-target="#assessmentSelectionModal" style="margin-bottom:5px;">Add Assessment</button>
											@endability
											<table class="table table-bordered condensed table-assessment">
												<thead>
													<tr>
														<th>Date</th>
														<th>Service Required</th>
														<th>Suggested Staff</th>
														<th>Manager</th>
														<th>Type</th>
														<th>View</th>
														<th>Edit</th>
														<th>Delete</th>
														<th>Status</th>
													</tr>
												</thead>
												<tbody>
													@if(count($l->assessment))
														@foreach($l->assessment()->orderBy('created_at','Desc')->get() as $a)
															@if($a->form_type == "Assessment" && $a->type != 'Detailed')
																{? $formData = json_decode($a->form_data,true); ?}
																<tr data-id="{{ Helper::encryptor('encrypt',$a->id) }}">
																	<td>{{ $formData['assessment_date'] }}</td>
																	<td>{{ $a->form_type }}</td>
																	<td>{{ isset($formData['suggested_professional'])?$formData['suggested_professional']:'NA' }}</td>
																	<td>{{ $formData['assessor_name'] }}</td>
																	<td>{{ $a->type }}</td>
																	<td><a class="btn btn-info btnViewAssessment assessmentId_{{$a->id}}" data-toggle="modal" data-target="#viewassessmentModal" data-form-data="{{$a->form_data}}" onclick="viewAssessment({{$a->id}});"><span class="view"></span> View</a></td>
																	<td><a class="btn btn-info btnViewAssessment" onclick="editassessment('{{$a->id}}','{{$a->status}}')" data-toggle="modal" data-target="#editSimpleAssessmentModal"><span class="view"></span> Edit</a></td>		
																	<td><a href="{{ route('lead.delete-assessment',Helper::encryptor('encrypt',$a->id)) }}" class="btn btn-info btnViewAssessment"><span class="view"></span> Delete</a></td>
																	<td>
																		<div class="btn-group">
																			<button type="button" class="btn btn-info waves-effect current-assessment-status">{{ $a->status }}</button>
																			<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
																				<span class="caret"></span>
																				<span class="sr-only">Toggle Dropdown</span>
																			</button>
																			<ul class="dropdown-menu">
																				<li><a href="javascript:void(0);" data-status="Pending" data-lead="{{ Helper::encryptor('encrypt',$a->lead_id) }}" data-id="{{ Helper::encryptor('encrypt',$a->id) }}" class="assessment-status waves-effect waves-block">Pending</a></li>
																				<li><a href="javascript:void(0);" data-status="Fit" data-lead="{{ Helper::encryptor('encrypt',$a->lead_id) }}" data-id="{{ Helper::encryptor('encrypt',$a->id) }}" class="assessment-status waves-effect waves-block">Fit</a></li>
																				<li><a href="javascript:void(0);" data-status="Unfit" data-lead="{{ Helper::encryptor('encrypt',$a->lead_id) }}" data-id="{{ Helper::encryptor('encrypt',$a->id) }}" class="assessment-status waves-effect waves-block">Unfit</a></li>
																			</ul>
																		</div>
																	</td>
																</tr>
															@endif

															@if($a->form_type == "Assessment" && $a->type == 'Detailed')
																{? $formData = json_decode($a->form_data,true); ?}
																<tr>
																	<td>{{ $a->created_at->format('d-m-Y') }}</td>
																	<td>{{ $a->form_type }}</td>
																	<td>NA</td>
																	<td>{{ $a->user->full_name ?? '' }}</td>
																	<td>{{ $a->type }}</td>
																	<td><a href="{{ route('lead.detailed-assessment',Helper::encryptor('encrypt',$a->id)) }}" class="btn btn-info assessmentId_{{$a->id}}" target="_blank"><span class="view"></span> View</a></td>
																	<td><a href="{{ route('lead.view-detailed-assessment-edit',Helper::encryptor('encrypt',$a->id)) }}" class="btn btn-info assessmentId_{{$a->id}}" target="_blank"><span class="view"></span> Edit</a></td>
																	<td><a href="{{ route('lead.delete-assessment',Helper::encryptor('encrypt',$a->id)) }}" class="btn btn-info btnViewAssessment"><span class="view"></span> Delete</a></td>
																	<td>
																		<div class="btn-group">
																			<button type="button" class="btn btn-info waves-effect current-assessment-status">{{ $a->status ?? '-' }}</button>
																			<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
																				<span class="caret"></span>
																				<span class="sr-only">Toggle Dropdown</span>
																			</button>
																			<ul class="dropdown-menu">
																				<li><a href="javascript:void(0);" data-status="Pending" data-lead="{{ Helper::encryptor('encrypt',$a->lead_id) }}" data-id="{{ Helper::encryptor('encrypt',$a->id) }}" class="assessment-status waves-effect waves-block">Pending</a></li>
																				<li><a href="javascript:void(0);" data-status="Fit" data-lead="{{ Helper::encryptor('encrypt',$a->lead_id) }}" data-id="{{ Helper::encryptor('encrypt',$a->id) }}" class="assessment-status waves-effect waves-block">Fit</a></li>
																				<li><a href="javascript:void(0);" data-status="Unfit" data-lead="{{ Helper::encryptor('encrypt',$a->lead_id) }}" data-id="{{ Helper::encryptor('encrypt',$a->id) }}" class="assessment-status waves-effect waves-block">Unfit</a></li>
																			</ul>
																		</div>
																	</td>
																</tr>
															@endif

															@if($a->form_type == "PhysioAssessment")
																<tr>
																	<td>{{ $a->created_at->format('d-m-Y h:i A') }}</td>
																	<td>{{ $a->form_type }}</td>
																	<td></td>
																	<td>{{ isset($a->user)?$a->user->full_name:'' }}</td>
																	<td><a class="btn btn-info btnViewLabRequirement requirementId_{{$a->id}}" data-toggle="modal" data-target="#viewrequirementModal" data-form-data="{{ $a->form_data }}" onclick="viewRequirement({{ $a->id }});"><span class="view"></span> View</a></td>
																	<td></td>
																</tr>
															@endif
														@endforeach
													@else
														<tr>
															<td colspan="9" class="text-center">No record(s)</td>
														</tr>
													@endif
												</tbody>
											</table>
									    </div>
									    <div role="tabpanel" class="tab-pane fade" id="followupTab">
									    	<a class="btn btn-primary pull-right" data-toggle="modal" data-target="#followupModal" style="margin-bottom:5px;">Add Follow-Up</a>
									    	<table class="table table-bordered condensed table-followUp">
												<thead>
													<tr>
														<th>Date</th>
														<th>Manager</th>
														<th>View</th>
														<th>Edit</th>
														<th>Delete</th>
													</tr>
												</thead>
												<tbody>
													@if(count($l->followUp))
														@foreach($l->followUp()->orderBy('created_at','Desc')->get() as $a)
															{? $formData = json_decode($a->form_data,true); ?}
													<tr data-id="{{ Helper::encryptor('encrypt',$a->id) }}">
														<td>{{ $a->followup_date->format('d-m-Y')??'' }}</td>
														<td>{{ $formData['followup_manager']??'-' }}</td>
														<td><a class="btn btn-info assessmentId_{{$a->id}}" data-toggle="modal" data-target="#viewassessmentModal" data-form-data="{{$a->form_data}}" onclick="viewAssessment({{$a->id}});"><span class="view"></span> View</a></td>
														<td><a class="btn btn-info editFollowup" onclick="editFollowup('{{$a->id}}','{{ $a->followup_date->format('d-m-Y')??'' }}')" data-toggle="modal" data-target="#followupModal"><span class="view"></span> Edit</a></td>		
														<td><a href="{{ route('lead.delete-followup',Helper::encryptor('encrypt',$a->id)) }}" class="btn btn-info"><span class="view"></span> Delete</a></td>
													</tr>
														@endforeach
													@else
														<tr>
															<td colspan="5" class="text-center">No record(s)</td>
														</tr>
													@endif
												</tbody>
											</table>
									    </div>
									</div>
								</div>
								@endability
							</div>

							<!-- service requests section -->
							<div class="vertical-tab-content">
								<h2 class="col-lg-6 col-md-6 col-sm-6 col-xs-6 card-inside-title" style="margin-left:0;padding-left:0">
									Services and Schedules
									<small>Manage services offered, schedules</small>
								</h2>
								@ability('admin','leads-service-order-add')
								<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 pull-right">
									@if($l->status == 'Converted')
										<button type="button" class="pull-right btn btn-primary btnAddService waves-effect">
											<i class="material-icons">add</i>
											<span>Add Service</span>
										</button>
									@endif
								</div>
								@endability
								<div class="clearfix"></div>

								@ability('admin','leads-service-order-add')
								<div id="service-request-block" class="row clearfix" style="display:none">
									<div class="card">
										<div class="header bg-cyan" style="padding:10px 12px;border-radius:5px 5px 0 0 !important">
											<h2 class="card-inside-title">
												New Service Request
												<a href="javascript:void(0);" class="btnCloseServiceRequestBlock pull-right" style="color: #fff;text-decoration:none">×</a>
											</h2>
										</div>
										<div class="body" style="padding-top:20px;padding-bottom:0">
											<form id="serviceRequestForm" action="{{ route('lead.save-case-services') }}" method="post" class="form-horizontal">
												{{ csrf_field() }}
												<div class="col-sm-6" style="margin-bottom:0">
													<div class="row clearfix">
														<div class="col-sm-3">
															<label class="required" for="service_requested_id">Service</label>
														</div>
														<div class="col-sm-8">
															<div class="form-group">
																<select class="form-control show-tick" id="service_requested_id" name="service_requested_id" data-live-search="true" required>
																	<option value="">-- Select Service --</option>
																	@if(isset($services) && count($services))
																		@foreach ($services as $service)
																			<option {{!isset($service->ratecard['amount'])?"disabled":''}} value="{{ $service->id }}" data-rate={{ $service->ratecard['amount'] ?? 0.00 }}>{{ strtoupper($service->service_name) }}</option>
																		@endforeach
																	@endif
																</select><br>
																<code style="text-transform:none">Rate: <span id="rate_lbl"></span></code>
															</div>
														</div>
													</div>
													<div class="row clearfix">
														<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 form-control-label">
															<label class="required" for="period_type">Period Type</label>
														</div>
														<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
															<div class="form-group" style="margin-top: 1%">
																<input name="period_type" type="radio" id="period_type_daterange" class="with-gap radio-col-deep-purple" value="Period" checked="">
																<label for="period_type_daterange">Date Range</label>
																<input name="period_type" type="radio" id="period_type_custom" class="with-gap radio-col-deep-purple" value="Custom">
																<label for="period_type_custom">Custom Dates</label>
															</div>
														</div>
													</div>
													<div class="row clearfix" id="daterange-block">
														<div class="col-sm-3">
															<label class="">Period</label>
														</div>
														<div class="col-sm-3">
															<div class="form-group">
																<div class="form-line">
																	<input autocomplete="off" type="text" class="form-control date" id="service_request_from_date" name="service_request_from_date" placeholder="From Date"/>
																</div>
															</div>
														</div>
														<div class="col-sm-3">
															<div class="form-group">
																<div class="form-line">
																	<input autocomplete="off" type="text" class="form-control date" id="service_request_to_date" name="service_request_to_date" placeholder="To Date"/>
																</div>
															</div>
														</div>
													</div>
													<div class="row clearfix" id="custom-dates-block" style="display:none">
														<div class="col-sm-3" style="margin-top: 3px;">
															<label class="">Custom Dates</label>
														</div>
														<div class="col-sm-8">
															<div class="form-group">
																<div class="form-line mdate">
																	<input type="text" class="form-control hide" id="mdates" placeholder="Multiple Dates" readonly=""/>
																	<input type="hidden" id="custom_dates" name="custom_dates"/>
																</div>
															</div>
														</div>
													</div>
													<div class="row clearfix">
														<div class="col-sm-3">
															<label class="">Interval</label>
														</div>
														<div class="col-sm-4">
															<div class="form-group">
																<div class="form-line">
																	<input style="font-size: 18px;" type="time" class="form-control time" id="service_request_start_time" name="service_request_start_time" placeholder="From Time" title="06:30 AM"/>
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group">
																<div class="form-line">
																	<input style="font-size: 18px;" type="time" class="form-control time" id="service_request_end_time" name="service_request_end_time" placeholder="To Time" title="06:30 PM"/>
																</div>
															</div>
														</div>
													</div>
													<div class="row clearfix hide">
														<div class="col-sm-3">
															<label class="">Frequency</label>
														</div>
														<div class="col-sm-3">
															<div class="form-group">
																<div class="form-line">
																	<input type="number" class="form-control" id="frequency_period" name="frequency_period" min="1" value="1" required=""/>
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group">
																<select class="form-control show-tick" id="frequency" name="frequency">
																	<option value="Hourly">Hourly</option>
																	<option value="Daily">Daily</option>
																	<option value="Weekly">Weekly</option>
																	<option value="Monthly">Monthly</option>
																</select>
															</div>
														</div>
													</div>
													<div class="row clearfix">
														<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 form-control-label">
															<label for="discount">Discount</label>
														</div>
														<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
															<div class="form-group" style="margin-top: 1%">
																<input name="discount_applicable" type="radio" id="discount_applicable_yes" class="with-gap radio-col-deep-purple" value="Y" disabled="">
																<label for="discount_applicable_yes">Yes</label>
																<input name="discount_applicable" type="radio" id="discount_applicable_no" class="with-gap radio-col-deep-purple" value="N" checked="" disabled="">
																<label for="discount_applicable_no">No</label>
															</div>
														</div>
													</div>
													<div id="discount_block" style="display:none">
														<div class="row clearfix">
															<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 form-control-label">
																<label for="no_of_hours">Discount Type</label>
															</div>
															<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
																<div class="form-group" style="margin-top: 1%">
																	<input name="discount_type" type="radio" id="discount_type_percentage" class="with-gap radio-col-deep-purple discount_type rateCardInput" value="P" checked="" disabled="">
																	<label for="discount_type_percentage">Percentage (%)</label>
																	<input name="discount_type" type="radio" id="discount_type_fixed" class="with-gap radio-col-deep-purple discount_type rateCardInput" value="F" disabled="">
																	<label for="discount_type_fixed">Fixed Value (Rs.)</label>
																</div>
															</div>
														</div>
														<div class="row clearfix">
															<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 form-control-label">
																<label for="no_of_hours">Discount Value</label>
															</div>
															<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
																<div class="form-group">
																	<div class="form-line">
																		<input type="number" min="0" step="0.01" id="discount_value" name="discount_value" class="form-control amtCal rateCardInput" placeholder="Ex. 5% or 100" autocomplete="off" disabled="">
																	</div>
																</div>
															</div>
														</div>
														<div class="row clearfix">
															<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 form-control-label">
																<label for="total_amount">Discounted Rate</label>
															</div>
															<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
																<div class="form-group">
																	<div class="form-line">
																		<input type="number" id="discounted_amount" name="discounted_amount" class="form-control amtCal" placeholder="Ex. 200.00" autocomplete="off" disabled="">
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="row clearfix">
														<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 form-control-label">
															<label for="net_rate">Total Amount</label>
														</div>
														<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															<div class="form-group" style="margin-top: 2%">
																<span id="service_request_amount" style="display: inline-block !important"></span>
																<input type="number" style="width:40%;display: inline-block !important" id="net_rate" name="net_rate" class="form-control amtCal" placeholder="-" autocomplete="off" readonly="true">
																<input type="hidden" id="gross_rate" name="gross_rate" class="form-control amtCal" placeholder="-" autocomplete="off" readonly="true">
															</div>
														</div>
														<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
														</div>
													</div>
												</div>

												<div class="col-sm-6" style="margin-bottom:0">
													<div>
														<div class="row clearfix">
															<div class="col-lg-4 col-md-3 col-sm-3 col-xs-12 form-control-label">
																<label for="delivery_address">Delivery Address</label>
															</div>
															<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
																<div class="form-group" style="margin-top: 1%">
																	<input name="delivery_address" type="radio" id="delivery_address_default" class="with-gap radio-col-deep-purple" value="Default" checked="checked">
																	<label for="delivery_address_default">Default</label>
																	@foreach($extraAddress as $address)
																	<input name="delivery_address" type="radio" id="delivery_address_{{ $address->id }}" class="with-gap radio-col-deep-purple" value="{{ $address->id }}">
																	<label for="delivery_address_{{ $address->id }}">{{ $address->type }}</label>
																	@endforeach
																</div>
																<input type="hidden" id="delivery_address_value" value="Default">
															</div>
														</div>
													</div>
													<div id="scheduleBlock">
														<div class="row clearfix">
															<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 form-control-label">
																<label for="create_schedules">Allocation</label>
															</div>
															<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
																<button type="button" class="btn bg-blue-grey btn-block waves-effect btnChooseStaff" data-block=".serviceRequestCaregiverBlock" data-profile="#selectedStaffName" data-caregiver="#caregiver_id"  onclick="javascript: return clearSearchForm();" disabled>Choose Staff</button>
															</div>
														</div>
														<input type="hidden" id="employeeLatitide">
														<input type="hidden" id="employeeLongitude">
														<input type="hidden" id="patientLatitide">
														<input type="hidden" id="patientLongitude">
														<input type="hidden" name="road_distance" class="road_distance">
													</div>
													<div class="row clearfix serviceRequestCaregiverBlock" style="display:none;">
														<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
															<label for="selectedStaffName">Caregiver</label>
														</div>
														<div class="col-lg-9 col-md-9 col-sm-6 col-xs-6">
															<div class="form-group">
																<div style="margin-top: 8px;" id="selectedStaffName">-</div>
																<input type="hidden" id="caregiver_id" name="caregiver_id" value=""/>
															</div>
														</div>
													</div><br>
													<hr>
													<div id="scheduleTasksBlock">
														<div class="row clearfix">
															<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 form-control-label">
																<label for="schedule_tasks">Tasks</label>
															</div>
															<div class="col-lg-9 col-md-9 col-sm-5 col-xs-5">
																<input name="assign_schedule_tasks" type="checkbox" id="assign_schedule_tasks" class="filled-in chk-col-indigo" value="1" checked="" onchange="javascript: $('.task-button-block').toggle();">
																<label for="assign_schedule_tasks">Assign Tasks</label>

																<div class="task-button-block">
																	<a class="btn bg-indigo waves-effect btnAssignScheduleTasks" style="width: 60%" data-toggle="modal" data-target="#assignTasksModal" onclick="javascript: return clearSearchForm();">Choose Task(s)</a><br><br>
																	<code><small>The selected task(s) will be assigned for all the day(s)</small></code><br>
																	<div style="margin-top: 8px;" id="selectedTasks">-</div>
																</div>
																<input type="hidden" name="assigned_tasks" value=""/>
															</div>
														</div>
													</div>
												</div>

												<div class="col-sm-12 outstanding-msg-block" style="display:none">
													<div class="alert alert-warning" style="background-color:#ff960073 !important;color: #ff0000 !important;font-size: 16px !important;">
													</div>
												</div>

												<div class="col-sm-12 text-center" style="margin-top:0;border-top: 1px solid #c6c6c6;padding-top:8px;margin:auto 0">
													<div class="col-sm-2 col-md-offset-4">
														<button type="reset" class="btn btn-block btn-danger btnResetServiceRequest waves-effect">Cancel</button>
													</div>
													<div class="col-sm-2">
														<input type="hidden" name="id" value="0"/>
														<input type="hidden" name="ivrs_type" value="ivrs_visit_based_creation_from_service">
														<button type="button" class="btn btn-block btn-success waves-effect btnSaveServiceRequest">Save</button>
													</div>
												</div>
												<div class="clearfix"></div>
											</form>
										</div>
									</div>
								</div>
								@endability

								<div class="row clearfix">
									@ability('admin','leads-service-order-edit,leads-service-order-view,leads-schedules-add,leads-schedules-edit,leads-schedules-view')
									<table class="table table-bordered condensed table-services" style="zoom:80%;">
										<thead>
											<tr>
												<th>#</th>
												<th>Date</th>
												<th>User</th>
												<th>Service</th>
												<th>Period</th>
												<th>Accumulated Price</th>
												<th class="text-center" colspan="1">Schedules</th>
												<th width="2%">Discount</th>
											</tr>
										</thead>
										<tbody>
											@if(isset($l->serviceRequests) && count($l->serviceRequests))
												@foreach ($l->serviceRequests as $index => $serviceRequest)
													<tr data-id="{{ Helper::encryptor('encrypt',$serviceRequest->id) }}" @if($serviceRequest->status == 'Cancelled') class="light-red" @endif>
														<td>{{ $index + 1 }}</td>
														<td>{{ $serviceRequest->created_at->format('d-m-Y') }}</td>
														<td>{{ isset($serviceRequest->user)?$serviceRequest->user->full_name:'' }}</td>
														<td>{{ $serviceRequest->service->service_name ?? '-' }}</td>
														<td>
															@if($serviceRequest->custom_dates == null)
																{{ $serviceRequest->from_date->format('d-m-Y') }}
																@if(isset($serviceRequest->to_date) && $serviceRequest->to_date != null)
																	{{ ' to '.$serviceRequest->to_date->format('d-m-Y') }}
																@endif
															@else
																{? $dates = explode(",",trim($serviceRequest->custom_dates)); ?}
																{{ $dates[0].' to '.end($dates) }}
															@endif
														</td>
														<td>{{ $serviceRequest->schedules->where('chargeable',1)->where('status','!=','Cancelled')->sum('amount') }}</td>
														<td width="8%" class="text-center">
															@ability('admin','leads-schedules-add,leads-schedules-edit,leads-schedules-view')
																<a class="btn btn-primary btnViewSchedules" data-lead-id="{{ Helper::encryptor('encrypt',$l->id) }}" data-service-request-id="{{ Helper::encryptor('encrypt',$serviceRequest->id) }}" data-service-required="{{ Helper::encryptor('encrypt',$serviceRequest->service_id) }}" data-service-rate="{{ $serviceRequest->net_rate }}" data-service-name="{{ $serviceRequest->service->service_name ?? '-' }}" data-status="{{ $serviceRequest->status }}">Schedules</a>
															@endability
														</td>
														<td>
															@if(Entrust::can('leads-discounts-add') || Entrust::hasRole('admin'))
																<?php $scheduledDates = [];
																foreach($serviceRequest->schedules->where('status','Pending') as $schedule){
																	$scheduledDates[] = $schedule->schedule_date->format('Y-m-d');
																}
																$scheduledDates = implode(',',$scheduledDates);
																$noOfDays = count($serviceRequest->schedules);
																?>
																<button type="button" class="btn bg-indigo btn-circle waves-effect waves-circle waves-float addDiscount" data-service-id="{{ $serviceRequest->id }}" data-service-rate="{{ $serviceRequest->service->ratecard->amount }}" >
																<i class="material-icons">add</i>
																</button>
															@else
																<button type="button" class="btn bg-grey btn-circle waves-effect waves-circle waves-float" data-toggle="popover" data-placement="left" data-trigger="focus" data-original-title="No Permission to Add Discount" data-content="Please connect to the Administrator !">
																	<i class="material-icons">add</i>
																</button>
															@endif
														</td>
													</tr>
													<tr class="showDiscountRow_{{ $serviceRequest->id }}">
														<td class="row_{{ $serviceRequest->id }}" colspan="11" style="border:1px solid #1f91f3;height: 100px; display: none;">
															<div class="col-md-12" style="margin-top: 20px;margin-bottom: 0;">
															<form action="{{ route('lead.add-discount') }}" method="POST">
																{{ csrf_field() }}
																<div class="col-md-6" style="margin-bottom: 0px;">
																	<div class="col-md-12" style="margin-bottom: 0px;">
																		<div class="col-md-10">
																			<div class="row clearfix">
																				<div class="col-lg-4 col-md-3 col-sm-3 col-xs-12 form-control-label">
																					<label class="service_order">Service Period</label>
																				</div>
																				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
																					<select class="form-control selectpicker servicePeriod" data-service-id="{{ $serviceRequest->id }}" id="servicePeriod_{{ $serviceRequest->id }}" name="service_order_{{ $serviceRequest->id }}" required></select>
																				</div>
																				<div class="col-lg-4 col-md-3 col-sm-3 col-xs-12 form-control-label">
																					<label class="apply_discount">Discount Period</label>
																				</div>
																				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
																					<input type="text" class="form-control apply_discount_{{ $serviceRequest->id }}" name="apply_discount" placeholder="Discount Period" data-service-id={{ $serviceRequest->id }} required="required">
																				</div>
																			</div>
																			<div id="discount_block_after">
																			<div class="row clearfix">
																				<div class="col-lg-4 col-md-3 col-sm-3 col-xs-12 form-control-label" style="margin-bottom: 0px;">
																					<label for="discount_type_after">Discount Type</label>
																				</div>
																				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="margin-bottom: 0px;">
																					<div class="form-group" style="margin-top: 1%">
																						<input name="discount_type_after_{{ $serviceRequest->id }}" type="radio" id="discount_type_percentage_after_{{ $serviceRequest->id }}" class="with-gap radio-col-deep-purple discount_type_after rateCardInput_after_{{ $serviceRequest->id }}" value="P" checked="">
																						<label for="discount_type_percentage_after_{{ $serviceRequest->id }}">Percentage (%)</label>
																						<input name="discount_type_after_{{ $serviceRequest->id }}" type="radio" id="discount_type_fixed_after_{{ $serviceRequest->id }}" class="with-gap radio-col-deep-purple discount_type_after rateCardInput_after_{{ $serviceRequest->id }}" value="F">
																						<label for="discount_type_fixed_after_{{ $serviceRequest->id }}">Fixed Value (Rs.)</label>
																					</div>
																				</div>
																			</div>
																			<div class="row clearfix">
																				<div class="col-lg-4 col-md-3 col-sm-3 col-xs-12 form-control-label" style="margin-bottom: 0px;">
																					<label for="discount_value_after_{{ $serviceRequest->id }}">Discount Value</label>
																				</div>
																				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="margin-bottom: 0px;">
																					<div class="form-group">
																						<div class="form-line">
																							<input type="number" min="0" step="0.01" id="discount_value_after_{{ $serviceRequest->id }}" name="discount_value_after_{{ $serviceRequest->id }}" class="form-control amtCal_after rateCardInput_after_{{ $serviceRequest->id }}" placeholder="Ex. 5% or 100" autocomplete="off" data-service-id={{ $serviceRequest->id }} required="required">
																						</div>
																					</div>
																				</div>
																			</div>
																			</div>
																			<div class="row clearfix">
																			<div class="col-lg-4 col-md-4 col-sm-3 col-xs-12 form-control-label" style="margin-bottom: 0px;">
																				<label for="net_rate_after_{{ $serviceRequest->id }}">Amount Per Day</label>
																			</div>
																			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-bottom: 0px;">
																				<div class="form-group" style="margin-top: 2%">
																					<span id="service_request_amount_after_{{ $serviceRequest->id }}" style="display: inline-block !important"></span>
																					<input type="number" style="width:40%;display: inline-block !important" id="net_rate_after_{{ $serviceRequest->id }}" name="net_rate_after_{{ $serviceRequest->id }}" class="form-control amtCal_after" placeholder="" autocomplete="off" readonly="true">
																					<input type="hidden" id="gross_rate_after_{{ $serviceRequest->id }}" name="gross_rate_after_{{ $serviceRequest->id }}" class="form-control amtCal_after" placeholder="" autocomplete="off" readonly="true">
																				</div>
																			</div>
																			</div>
																			<div class="row clearfix">
																				<div class="col-md-2">
																					<button type="submit" class="btn bg-indigo waves-effect waves-float discountSubmit_{{ $serviceRequest->id }}">Submit</button>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<input type="hidden" name="service_id" value={{ $serviceRequest->id }}>
															</form>
																<div class="col-md-6" style="border-left: 1px solid grey;min-height: 30px;">
																	<div><u>Discount Log</u><b style="float: right;">P - PERCENTAGE, F - FIXED AMOUNT</b></div>
																	<ul style="font-size: 14px;max-height: 200px;overflow-y: auto;">
																		@forelse($serviceRequest->discountLog as $dl)
																		<li>
																			@if($dl->from_date == null && $dl->to_date == null)
																			<b style="color:#ff9800">{{ $dl->serviceRequest->custom_dates }}</b>
																			@else
																			<b style="color:#ff9800">{{  \Carbon\Carbon::parse($dl->from_date)->format('d-m-Y') }}</b>
																			TO
																			<b style="color:#ff9800">{{ \Carbon\Carbon::parse($dl->to_date)->format('d-m-Y') }}</b>
																			@endif
																			@
																			<b>{{ $dl->discount_value }}</b>
																			<b>{{ $dl->discount_type }}</b>
																			FOR
																			<b>{{ $dl->amount }}</b>
																			/- PER DAY BY
																			<b>{{ $dl->user->full_name }}</b>
																		</li>
																		@empty
																		<li><i>No Discount Log(s) Found</i></li>
																		@endforelse
																	</ul>
																</div>
															</div>
														</td>
													</tr>
												@endforeach
											@else
												<tr>
													<td colspan="11" class="text-center">No service(s) found</td>
												</tr>
											@endif
										</tbody>
									</table>
									@endability
								</div>
							</div>

							<!-- billables section -->
							<div class="vertical-tab-content">
								<h2 class="card-inside-title">
									Case Billables
									<small>Consumables, Equipments and Pharmaceuticals</small>
								</h2>
								@if(session('billable_alert'))
								<div class="alert alert-info alert-dismissible" role="alert" style="padding:10px">
									<button type="button" style="padding-right:20px" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<strong>Success!</strong> {{ session('billable_alert') }}.
								</div>
								@endif
								<ul class="nav nav-tabs" role="tablist" style="border: 2px dotted #eee">
									<li role="presentation" class="active" style="min-width: 30%;text-align:center">
										<a href="#case_billables" data-toggle="tab">
											<i class="material-icons">attach_money</i> Billables
										</a>
									</li>
								</ul>

								<div class="tab-content" style="border: 2px dotted #eee;padding: 10px;margin-bottom: 20px">
									<div role="tabpanel" class="tab-pane fade in active" id="case_billables">
										@ability('admin','leads-billables-create')
										<div class="col-md-12 col-lg-12 col-sm-12 clearfix" style="border:1px solid #ccc;border-radius:6px;padding-top:15px;">
											<form class="form-inline" action="{{ route('lead.add-billables') }}" method="POST" style="display: inline">
												{{ csrf_field() }}
												<input type="hidden" name="patient_id" value="{{ isset($l)?Helper::encryptor('encrypt',$l->patient_id):0 }}"/>
												<table class="table table-bordered condensed">
													<thead>
														<tr>
															<th width="25%" class="text-center">Category</th>
															<th width="40%" class="text-center">Item</th>
															<th class="text-center">Rate</th>
															<th class="text-center">Quantity</th>
															<th class="text-center">Tax Rate</th>
															<th class="text-center">Amount</th>
															<th class="text-center"></th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>
																<select class="form-control show-tick" id="billable_category" name="billable_category" data-live-search="true" required>
																	<option value="">-- Select --</option>
																	<option value="Consumables">Consumables</option>
																	<option value="Equipments">Equipments</option>
																	<option value="Laboratory">Laboratory Investigation</option>
																	<option value="Pharmaceuticals">Pharmaceuticals</option>
																	<option value="Others">Others</option>
																</select>
															</td>
															<td>
																<select class="form-control show-tick" id="billable_item" name="item" data-live-search="true" disabled="" required>
																	<option value="">-- Select Item --</option>
																	<optgroup label="Consumables">
																		@if(isset($consumables) && count($consumables))
																			@foreach($consumables as $c)
																				<option value="{{ $c->id }}" data-rate="{{ $c->consumable_price }}" data-tax="{{ $c->tax->tax_rate }}" >{{ $c->consumable_name }}</option>
																			@endforeach
																		@endif
																	</optgroup>
																	<optgroup label="Equipments">
																		@if(isset($surgicals) && count($surgicals))
																			@foreach($surgicals as $c)
																				<option value="{{ $c->id }}" data-rate="{{ $c->surgical_price }}" data-tax="{{ $c->tax->tax_rate }}">{{ $c->surgical_name }}</option>
																			@endforeach
																		@endif
																	</optgroup>
																	<optgroup label="Laboratory">
																		@if(isset($tests) && count($tests))
																			@foreach($tests as $c)
																				<option value="{{ $c->id }}" data-rate="{{ $c->test_price }}" data-tax="{{ $c->tax->tax_rate }}">{{ $c->test_name }}</option>
																			@endforeach
																		@endif
																	</optgroup>
																</select>
																<input type="text" class="form-control input-sm" style="width:99%;display:none"  id="medicine_name" name="item_name" size="50" value="" placeholder="Medicine name"/>
																<input type="text" class="form-control input-sm" style="width:99%;display:none" id="other_name" name="other_name" size="50" value="" placeholder="Charge description"/>
															</td>
															<td class="text-center">
																<input type="number" style="width:150px" class="form-control input-sm" min="1" id="billable_rate" step="0.01" name="rate" data-live-search="true" readonly="" required/>
															</td>
															<td class="text-center">
																<input type="number" style="width:80px" class="form-control input-sm" min="1" id="billable_quantity" name="quantity" data-live-search="true" value="1" required />
															</td>
															<td class="text-center">
																<input type="number" style="width:80px" class="form-control input-sm" min="0" id="billable_tax" name="billable_tax" data-live-search="true" value="0.00" required disabled="" />
															</td>
															<td class="text-center">
																<input type="number" step="0.01" style="width:150px" class="form-control input-sm" min="1" id="billable_amount" name="amount" data-live-search="true" required />
															</td>
															<td class="text-center">
																@if($l->status == 'Converted')
																	<button type="submit" class="btn btn-primary">Add</button>
																@endif
															</td>
														</tr>
													</tbody>
												</table>
											</form>
										</div>
										@endability
										@ability('admin','leads-billables-create,leads-billables-edit')
										<div class="col-sm-12" style="margin-bottom:10px;border: 1px solid #ccc;border-radius:4px;padding-top:10px">
											<div class="col-sm-2  form-control-label" style="margin-bottom:0">
												<label>Choose Vendor</label>
											</div>
											<div class="col-sm-6" style="margin-bottom:0">
												<div class="form-group" style="margin-top: 1%">
													<select class="form-control show-tick" required="" data-live-search="true" name="vendor_id">
														<option value="0">-- Vendor --</option>
														@if(isset($vendors) && count($vendors))
															@foreach ($vendors as $vendor)
																<option value="{{ $vendor->id }}">{{ $vendor->name }}</option>
															@endforeach
														@endif
													</select>
												</div>
											</div>
											<div class="col-sm-4" style="margin-bottom:0">
												<a class="btn bg-teal btnSendToVendor">Send to Vendor</a>
											</div>
											<div class="col-sm-12" style="margin-bottom:10px">
												<code>Select the items from the below list to send an email to vendor</code>
											</div>
										</div>
										@endability
										<div class="clearfix"></div>
										@ability('admin','leads-billables-edit')
										<div class="table-responsive">
											<table class="table table-bordered condensed billablesTable">
												<thead>
													<tr>
														{{-- <th width="5%"></th> --}}
														<th width="5%" class="billable-sel"></th>
														<th>#</th>
														<th>Date</th>
														<th>Category</th>
														<th>Item</th>
														<th class="text-center">Rate</th>
														<th width="8%" class="text-center">Quantity</th>
														<th width="8%" class="text-center">Tax</th>
														<th class="text-right">Amount</th>
													</tr>
												</thead>
												<tbody>
													@if(isset($l->billables) && count($l->billables))
														{? $total = 0; $count = 0; ?}
														@foreach ($l->billables as $index => $cb)
															{? $count += $cb->quantity; $tax = 0.00; ?}
															<tr data-id="{{ Helper::encryptor('encrypt',$cb->id) }}">
																{{-- <td>
																	<a href="javascript:void(0);" data-id="{{ Helper::encryptor('encrypt',$cb->id) }}" class="btn btn-xs btn-danger btnRemoveBillableItem" title="Remove Item"><i class="material-icons">delete</i></a>
																</td> --}}
																<th class="text-center billable-sel">
																	<input id="billable_{{ $cb->id }}" type="checkbox" class="with-gap chk-col-blue filled-in billable-sel-chk" value="{{ $cb->id }}"/><label for="billable_{{ $cb->id }}">&nbsp;</label>
																</th>
																<td>{{ $index + 1 }}</td>
																<td>{{ $cb->created_at->format('d-m-Y') }}</td>
																<td>{{ $cb->category }}</td>
																<td>
																	@if($cb->category == 'Consumables')
																		{{ $cb->consumable->consumable_name }}
																		{? $tax = $cb->consumable->tax->tax_rate; ?}
																	@endif
																	@if($cb->category == 'Equipments')
																		{{ $cb->surgical->surgical_name }}
																		{? $tax = $cb->surgical->tax->tax_rate; ?}
																	@endif
																	@if($cb->category == 'Laboratory')
																		{{ $cb->tests->test_name }}
																		{? $tax = $cb->tests->tax->tax_rate; ?}
																	@endif
																	@if($cb->category == 'Pharmaceuticals' || $cb->category == 'Others')
																		{{ $cb->item }}
																	@endif
																</td>
																<td class="text-right">{{ number_format($cb->rate,2) }}</td>
																<td class="text-center rowDataSd">{{ $cb->quantity }}</td>
																<td class="text-center rowDataSd">{{ $tax }}</td>
																<td class="text-right rowDataSd">{{ number_format($cb->amount,2) }}</td>
																{? $total += $cb->amount; ?}
															</tr>
														@endforeach
														<tr id="bill_sum">
															<th class="text-right" colspan="7">Total</th>
															<th class="text-center totalCol">{{ $count }}</th>
															<th class="text-right totalCol" colspan="2">{{ number_format($total,2) }}</th>
														</tr>
													@else
														<tr>
															<td colspan="8" class="text-center">No billable item(s)</td>
														</tr>
													@endif
												</tbody>
											</table>
										</div>
										@endability
									</div>
								</div>
							</div>

							<!-- documents section-->
							<div class="vertical-tab-content">
								<h2 class="card-inside-title">
									Case Documents
									<small>Case related documents</small>
								</h2>
								<div class="row clearfix">
									@caseNotClosed($l->status)
									@ability('admin','leads-document-add')
									<div class="col-sm-12 form-horizontal">
										<form id="documentForm" action="{{ route('lead.upload-case-document') }}" method="post" enctype="multipart/form-data">
											{{ csrf_field() }}
											<table class="table table-bordered condensed">
												<thead>
													<tr>
														<th class="text-center">Document Name</th>
														<th class="text-center">Service Request</th>
														<th class="text-center" width="25%">Document</th>
														<th width="10%"></th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>
															<input type="text" class="form-control" id="document_name" name="document_name" placeholder="Document Name" required="">
														</td>
														<td>
															<select class="form-control" id="service_request_id" name="service_request_id">
																<option>- Select -</option>
														@if(isset($l->serviceRequests) && !empty($l->serviceRequests))
															@foreach ($l->serviceRequests as $sr)
																<option value="{{ $sr->id }}">{{ $sr->service->service_name ?? '-' }}</option>
															@endforeach
														@endif
															</select>
														</td>
														<td>
															<input type="file" class="form-control" id="case_document" name="case_document" required=""/>
														</td>
														<td>
															<input type="hidden" name="lead_id" value="{{ Helper::encryptor('encrypt',$l->id) }}" />
															<button type="submit" class="btn btn-success">Upload Document</button>
														</td>
													</tr>
												</tbody>
											</table>
										</form>
									</div>
									@endability
									@endCaseNotClosed

									@ability('admin','leads-document-view')
									<div class="col-sm-12">
										<table class="table table-bordered condensed">
											<thead>
												<tr>
													<th>#</th>
													<th>Document Name</th>
													<th>Document</th>
													<th>Service</th>
													<th>Uploaded By</th>
													<th>Upload Date</th>
													<th>Remove</th>
												</tr>
											</thead>
											<tbody>
												@if(isset($l->documents) && count($l->documents))
												@foreach ($l->documents as $index => $document)
												<tr>
													<td>{{ $index + 1 }}</td>
													<td>{{ $document->document_name }}</td>
													<td>
														<a href="{{ $document->document_path }}" target="_blank">View Document</a>
													</td>
													<td>{{ isset($document->serviceRequest)?$document->serviceRequest->service->service_name:'' }}</td>
													<td>{{ isset($document)?$document->user->full_name:'' }}</td>
													<td>{{ $document->created_at->format('d-m-Y') }}</td>
													<td>
														<form class="form-inline" action="{{ route('lead.remove-case-document') }}" method="POST" style="display: inline">
															{{ method_field('DELETE') }}
															{{ csrf_field() }}
															<input type="hidden" name="id" value="{{ Helper::encryptor('encrypt',$document->id) }}"/>
															<button type="submit" onclick="return confirm('Are you sure?');" class="btn btn-danger btn-xs waves-effect btnRemove"><i class="material-icons small">delete</i></button>
														</form>
													</td>
												</tr>
												@endforeach
												@else
												<tr>
													<td colspan="6" class="text-center">No document(s) found</td>
												</tr>
												@endif
											</tbody>
										</table>
									</div>
									{{-- <div class="text-center restricted-access">Access to this information is restricted</div> --}}
									@endability
								</div>
							</div>

							<!-- tasks section -->
							<div class="vertical-tab-content">
								<h2 class="col-lg-8 card-inside-title">
									Daily Tasks
									<small>Daily tasks &amp; routines</small>
								</h2>
								@ability('admin','leads-worklog-create')
								<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 pull-right hide">
									<button type="button" class="pull-right btn btn-primary btnAddWorklog waves-effect">
										<i class="material-icons notranslate">add</i>
										<span>Update Worklog</span>
									</button>
								</div>
								@endability
								@ability('admin','leads-worklog-view')
								<div class="row clearfix">
									<table class="table table-bordered condensed">
										<thead>
											<tr>
												<th>#</th>
												<th>Type</th>
												<th>Date</th>
												<th>Caregiver</th>
												<th>Service</th>
												<th>Customer Comment</th>
												<th>View</th>
											</tr>
										</thead>
										<tbody>
											@forelse ($l->worklog as $index => $w)
											<tr>
												<td>{{ ($index + 1) }}</td>
												<td>{{ isset($w->schedule)?$w->schedule->service->service_name:'-' }}</td>
												<td>{{ $w->worklog_date }}</td>
												<td>{{ isset($w->caregiver)?$w->caregiver->first_name.' '.$w->caregiver->last_name:'-' }}</td>
												<td>
													@if(isset($w->feedback->rating))
														@if (floor($w->feedback->rating) == $w->feedback->rating && $w->feedback->rating != '0')
															<?php echo str_repeat('<i class="fa fa-star" style="color:royalblue;"></i> ',$w->feedback->rating); ?>
														@elseif ($w->feedback->rating == '0')
															<?php echo str_repeat('<i class="fa fa-star-o" style="color:grey;"></i> ',5); ?>
														@else
															<?php echo str_repeat('<i class="fa fa-star" style="color:royalblue;"></i> ',$w->feedback->rating); ?>
															<?php echo is_float($w->feedback->rating) ? '<i class="fa fa-star-half" style="color:royalblue;"></i>' : ''; ?>
														@endif
													@else
														<?php echo str_repeat('<i class="fa fa-star-o" style="color:grey;"></i> ',5); ?>
													@endif
												</td>
												<td>{{ $w->feedback->comment ?? '-' }}</td>
												<td><a class="btn btn-info btnViewWorklog worklogId_{{ $w->id }}" data-toggle="modal" data-target="#viewWorklogModal" data-vitals="{{ $w->vitals }}" data-routines="{{ $w->routines }}" onclick="viewWorklog(this);"><span class="view"></span> View</a></td>
											</tr>
											@empty
											<tr>
												<td colspan="7" class="text-center">No record(s)</td>
											</tr>
											@endforelse
										</tbody>
									</table>
								</div>
								@endability
							</div>

						</div>
					</div>

					<div class="clearfix"></div><br>
				</div>
			</div>
		</div>
	</div>

	<!-- Patient Image -->
	<div class="modal fade" id="patientPictureModal" aria-labelledby="modalLabel" role="dialog" tabindex="-1">
		<div class="modal-dialog" role="document">
			<form action="{{ route('patient.upload-picture') }}" method="POST" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="modal-content">
					<div class="modal-header bg-cyan">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h5 class="modal-title" id="modalLabel">Patient Photo</h5>
					</div>
					<div class="modal-body">
						<div class="row clearfix" style="margin-top:5px">
							<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
								<label for="lead_status">Choose File</label>
							</div>
							<div class="col-lg-7 col-md-7 col-sm-5 col-xs-7">
								<div class="form-group">
									<input type="file" class="form-control" id="pic_upload" name="pic_upload" accept="image/*" required=""/>
								</div>
							</div>
						</div>
						<div class="row clearfix">
							<center>
								<div class="profile-img-block"></div>
							</center>
						</div>
					</div>
					<div class="modal-footer">
						<input type="hidden" name="crop_values" value="" />
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-success">Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<!-- Case Status Modal -->
	<div class="modal fade" id="leadStatusModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" style="z-index: 1052 !important">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header bg-blue">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="largeModalLabel">Case Status</h4>
				</div>
				<div class="modal-body" style="padding-top:15px !important">
					<div class="row clearfix">
						<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
							<label for="lead_status">Status</label>
						</div>
						<div class="col-lg-7 col-md-7 col-sm-5 col-xs-7">
							<div class="form-group">
								<select class="form-control show-tick" id="lead_status" name="checkState" onchange="checkCaseLoss(this.value);">
									<option value="Pending" {{ $l->status=='Pending'?'selected':'' }}>Pending</option>
									<option value="Converted" {{ $l->status=='Converted'?'selected':'' }}>Converted</option>
									<option value="Follow Up" {{ $l->status=='Follow Up'?'selected':'' }}>Follow Up</option>
									<option value="Assessment Pending" {{ $l->status=='Assessment Pending'?'selected':'' }}>Assessment Pending</option>
									<option value="Assessment Completed" {{ $l->status=='Assessment Completed'?'selected':'' }}>Assessment Completed</option>
									<option value="Closed" {{ $l->status=='Closed'?'selected':'' }}>Closed</option>
									<option value="Dropped" {{ $l->status=='Dropped'?'selected':'' }}>Dropped / Case Loss</option>
									<option value="No Supply" {{ $l->status=='No Supply'?'selected':'' }}>No Supply</option>
								</select>
							</div>
						</div>
					</div>

					<div class="row clearfix" id="dropped" style="display:none;border-radius: 5px;background: rgba(162, 18, 18, 0.06); padding: 15px;margin-bottom: 10px;">
						<div class="col-lg-12 form-control-label" style="text-align: center !important;">
							<label for="dropped_reason">Dropped / Case Loss Reason</label>
						</div>
						<div class="col-lg-12 text-left">
							<div class="form-group" style="margin-top: 1%">
								<input name="dropped_reason[]" type="checkbox" id="0" class="with-gap filled-in chk-col-amber loss" value="Emergency Service"/>
								<label for="0">Emergency Service</label><br>

								<input name="dropped_reason[]" type="checkbox" id="1" class="with-gap filled-in chk-col-amber loss" value="No Response" />
								<label for="1">No Response</label><br>

								<input name="dropped_reason[]" type="checkbox" id="2" class="with-gap filled-in chk-col-amber loss" value="Prices are High" />
								<label for="2">Prices are High</label><br>

								<input name="dropped_reason[]" type="checkbox" id="3" class="with-gap filled-in chk-col-amber loss" value="Household Helper" />
								<label for="3">Household Helper</label><br>

								<input name="dropped_reason[]" type="checkbox" id="4" class="with-gap filled-in chk-col-amber loss" value="Patient is hospitaized" />
								<label for="4">Patient is hospitaized</label><br>

								<input name="dropped_reason[]" type="checkbox" id="5" class="with-gap filled-in chk-col-amber loss" value="No Staff/Out of servic area" />
								<label for="5">No Staff/Out of servic area</label><br>

								<input name="dropped_reason[]" type="checkbox" id="6" class="with-gap filled-in chk-col-amber loss" value="Service taken from others" />
								<label for="6">Service taken from others</label><br>

								<input name="dropped_reason[]" type="checkbox" id="7" class="with-gap filled-in chk-col-amber loss" value="Patient Passed away" />
								<label for="7">Patient Passed away</label><br>

								<input name="dropped_reason[]" type="checkbox" id="8" class="with-gap filled-in chk-col-amber loss" value="General Inquiry about services and charges" />
								<label for="8">General Inquiry about our services and charges</label><br>

								<input name="dropped_reason[]" type="checkbox" id="9" class="with-gap filled-in chk-col-amber loss" value="Not willing to share any details" />
								<label for="9">Not willing to share any details</label><br>

								<input name="dropped_reason[]" type="checkbox" id="10" class="with-gap filled-in chk-col-amber loss" value="Bad Lead" />
								<label for="10">Bad Lead</label><br>

								<input name="dropped_reason[]" type="checkbox" id="11" class="with-gap filled-in chk-col-amber loss" value="Advance Payment" />
								<label for="11">Advance Payment</label><br>

								<input name="dropped_reason[]" type="checkbox" id="12" class="with-gap filled-in chk-col-amber loss" value="Duplicate Lead" />
								<label for="12">Duplicate Lead</label><br>
							</div>
						</div>
					</div>

					<div class="row clearfix" id="followup" style="display:none;border-radius: 5px;background: rgba(162, 18, 18, 0.06); padding: 15px;margin-bottom: 10px;">
						<div class="col-lg-12 text-left">
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label">
									<label for="follow_up_date">Follow Up Date</label>
								</div>
								<div class="col-lg-5 col-md-5 col-sm-5 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" class="form-control datetime" id="follow_up_date">
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label">
									<label for="follow_up_comment">Follow Up Comment</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<textarea class="form-control" id="follow_up_comment"></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row clearfix">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label">
							<label for="lead_disposition_date">Disposition Date</label>
						</div>
						<div class="col-lg-5 col-md-5 col-sm-5 col-xs-7">
							<div class="form-group">
								<div class="form-line">
									<input type="text" class="form-control date" id="lead_disposition_date" placeholder="Date">
								</div>
							</div>
						</div>
					</div>
					<div class="row clearfix">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label">
							<label for="lead_comment">Comment</label>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line">
									<textarea class="form-control" id="lead_comment" placeholder="Comments / Reason / Notes (if any)"></textarea>
								</div>
							</div>
						</div>
					</div><br>
					<div class="row clearfix">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="form-group text-center">
								<a class="btn btn-warning btn-lg waves-effect updateLeadStatus" data-lead-id="{{ Helper::encryptor('encrypt',$l->id) }}">Update Status</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Staff Search Modal -->
	<div class="modal fade" id="staffSearchModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" style="z-index: 1052 !important">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header bg-blue" style="padding: 10px 25px 10px 25px !important">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="largeModalLabel">Staff Search Form</h4>
				</div>
				<div class="modal-body">
					<div class="row clearfix">
						<button type="button" class="btn bg-green btn-circle waves-effect waves-circle waves-float pull-right" style="display:none;margin-right: 10px;" title="Show Filters">
							<i class="material-icons">filter_list</i>
						</button>
						<form id="staffSearchForm">
							<fieldset class="col-sm-12" style="background:#eee; border:1px solid #ccc; border-radius:5px; padding:5px;">
								<div class="col-sm-12 hide" style="margin-bottom:10px; border-bottom:1px solid #ccc; padding-bottom:10px">
									<div class="form-group">
										<div class="col-sm-1">
											<label for="period" style="line-height: 2;">Date</label>
										</div>
										<div class="col-sm-2">
											<div class="form-line">
												<input type="text" class="form-control date" id="filter_from_date" placeholder="From Date">
											</div>
										</div>
										<div class="col-sm-1 filter_period" style="width: 5%;line-height: 2;">to</div>
										<div class="col-sm-2 filter_period">
											<div class="form-line">
												<input type="text" class="form-control date" id="filter_to_date" placeholder="To Date">
											</div>
										</div>
										<div class="col-sm-2 text-right">
											<label for="period" class="text-right" style="line-height: 2;">Time</label>
										</div>
										<div class="col-sm-2" style="width: 14%">
											<div class="form-line">
												<input type="text" class="form-control time" id="filter_from_time" placeholder="From Time">
											</div>
										</div>
										<div class="col-sm-1" style="width: 5%;line-height: 2;">to</div>
										<div class="col-sm-2" style="width: 14%">
											<div class="form-line">
												<input type="text" class="form-control time" id="filter_to_time" placeholder="To Time">
											</div>
										</div>
									</div>
								</div>
								<div class="row" style="margin-bottom:0">
									<div class="col-sm-3">
										<div class="form-group" style="margin-bottom:0;">
											<label for="specialization">Specialization</label>
											<div class="form-line">
												<select class="form-control input-sm show-tick" id="filter_specialization">
													<option value="0">Any</option>
													@if(isset($specializations) && count($specializations))
														@foreach($specializations as $s)
															<option value="{{ $s->id }}">{{ $s->specialization_name }}</option>
														@endforeach
													@endif
												</select>
											</div>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group" style="margin-bottom:0;">
											<label for="experience">Experience</label>
											<div class="form-line">
												<select class="form-control input-sm show-tick" id="filter_experience">
													<option value="0">Any</option>
													<option value="0_5">0-5 yr(s)</option>
													<option value="5_10">5-10 yr(s)</option>
													<option value="10_15">10-15 yr(s)</option>
													<option value="15_35">Above 15 yrs</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group" style="margin-bottom:0;">
											<label for="language">Language</label>
											<div class="form-line">
												<select class="form-control input-sm show-tick" id="filter_language" data-live-search="true" data-actions-box="true" multiple>
													<option value="0">Any</option>
													@foreach($languages as $language)
														<option value="{{ $language->id }}">{{ $language->language_name }}</option>
													@endforeach
												</select>
											</div>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group" style="margin-bottom:0;">
											<label for="skill">Skill</label>
											<div class="form-line">
												<select class="form-control input-sm show-tick" id="filter_skill" data-live-search="true" data-actions-box="true" multiple>
													<option value="0">Any</option>
													@foreach($skills as $skill)
														<option value="{{ $skill->id }}">{{ $skill->skill_name }}</option>
													@endforeach
												</select>
											</div>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group" style="margin-bottom:0;">
											<label for="experience_level">Staff Level</label>
											<div class="form-line">
												<select class="form-control input-sm show-tick" id="filter_experience_level">
													<option value="0">Any</option>
													<option value="1">Level 1</option>
													<option value="2">Level 2</option>
													<option value="3">Level 3</option>
													<option value="4">Level 4</option>
												</select>
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="row" style="margin-bottom:0;">
									<div class="col-sm-4">
										<div class="form-group" style="margin-bottom:0;">
											<label for="staff_gender_preference">Gender Preference</label>
											<div class="form-line">
												<select class="form-control input-sm show-tick" id="staff_gender_preference">
													<option value="Any">Any</option>
													<option value="Male">Male</option>
													<option value="Female">Female</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-sm-4 text-center" style="display:inline-block; margin-top:10px;">
										<input type="checkbox" id="include_onduty_staff" class="filled-in chk-col-orange" style="position:relative;">
										<label for="include_onduty_staff" style="float: left">Include On-Duty</label>
									</div>
									<div class="col-sm-4 text-center" style="display:inline-block; text-align:right;  margin-top:10px;">
										<input type="hidden" id="replacement" value="0" />
										<a class="btn btn-primary waves-effect btnFilterStaff">Search</a>&nbsp;&nbsp;
										<button type="reset" class="btn btn-danger waves-effect btnReset">Reset</button>
									</div>
								</div>
								<div class="clearfix"></div>
							</fieldset>
						</form>
						<div class="col-sm-12">
							<div class="page-loader-wrapper">
								<div class="loader">
									<div class="md-preloader pl-size-md">
										<svg viewbox="0 0 75 75">
											<circle cx="37.5" cy="37.5" r="33.5" class="pl-blue" stroke-width="4" />
										</svg>
									</div>
									<p>Please wait...</p>
								</div>
							</div>
							<table class="table table-bordered table-striped staffSearchResults" style="width:100% !important; margin-bottom: 5px !important">
								<thead>
									<tr>
										<th>ID</th>
										<th>Name</th>
										<th>Designation</th>
										<th>Experience</th>
										<th>Level</th>
										<th>Distance in KM</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody style="height: 230px;overflow-y: auto;">
									<tr>
										<td class="text-center" colspan="8">Click 'Search' to get results</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success waves-effect btnSelectStaff" data-visit-id="0">Continue</button>
					<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Comment Modal -->
	<div class="modal fade" id="commentModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<form action="{{ route('lead.save-comment') }}" method="POST">
				{{ csrf_field() }}
				<div class="modal-content">
					<div class="modal-header bg-blue">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="largeModalLabel">Add Comment</h4>
					</div>
					<div class="modal-body">
						<div class="row clearfix">
							<div class="col-sm-12">
								<div class="form-group">
									<div class="form-line">
										<textarea class="form-control" id="comment" name="comment" rows="3" placeholder="Enter Comment here" required></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<input type="hidden" name="lead_id" value="{{ Helper::encryptor('encrypt',$l->id) }}" />
						{{ csrf_field() }}
						<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-success waves-effect pull-right" onclick="this.disabled=true;this.form.submit();">Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<!-- Edit Patient Modal -->
	<div class="modal fade" id="editPatientModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" role="document" style="width:85%">
			<form id="editPatientForm" method="POST" action="{{ route('patient.save-patient') }}">
				<div class="modal-content">
					<div class="modal-header bg-blue">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="largeModalLabel">Edit Patient <span class="crn_no"></span></h4>
					</div>
					<div class="modal-body">
						<div class="row clearfix">
							<div class="col-sm-6 form-horizontal">
								<h4 class="card-inside-title col-teal">
									Basic Details
								</h4>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
										<label for="first_name">First Name</label>
									</div>
									<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
										<div class="form-group form-float">
											<div class="form-line">
												<input type="text" id="first_name" name="first_name" class="form-control searchCol" placeholder="Ashok" value="{{ isset($l->patient)?$l->patient->first_name:'' }}" title="Only Characters Allowed" required>
											</div>
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
										<div class="form-group">
											<div class="form-line">
												<input type="text" id="last_name" name="last_name" class="form-control searchCol" placeholder="Kumar" value="{{ isset($l->patient)?$l->patient->last_name:'' }}" title="Only Characters Allowed">
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
										<label for="date_of_birth">Date of Birth</label>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
										<div class="form-group">
											<div class="form-line">
												<input type="text" id="dob" name="dob" class="form-control bdate" placeholder="20-03-1992" value="{{ (isset($l->patient) && $l->patient != null && isset($l->patient->date_of_birth))?$l->patient->date_of_birth->format('d-m-Y'):'' }}">
											</div>
										</div>
									</div>
									<div class="col-sm-1" style="line-height: 3"><b> or </b></div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
										<div class="form-group">
											<div class="form-line">
												<input type="number" min="5" id="patient_age" name="patient_age" class="form-control" placeholder="25" value="{{ (isset($l->patient) && isset($l->patient->patient_age))?$l->patient->patient_age:'' }}">
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
										<label for="weight">Patient Weight</label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="number" min="2" id="patient_weight" name="patient_weight" class="form-control" placeholder="70" value="{{ isset($l->patient->patient_weight)?$l->patient->patient_weight:'' }}">
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
										<label for="gender">Gender</label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<div class="form-group">
											<select class="form-control show-tick" id="gender" name="gender" required>
												<option value="">-- Please select--</option>
												<option value="Male" {{ (isset($l->patient) && $l->patient->gender == 'Male')?'selected':'' }}>Male</option>
												<option value="Female" {{ (isset($l->patient) && $l->patient->gender == 'Female')?'selected':'' }}>Female</option>
												<option value="Other" {{ (isset($l->patient) && $l->patient->gender == 'Other')?'selected':'' }}>Other</option>
											</select>
										</div>
									</div>
								</div>

								<h4 class="card-inside-title col-teal">
									Patient Address
								</h4>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label ">
										<label for="street_address">Address</label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" id="street_address" name="street_address" class="form-control" placeholder="Smart Health Global Private Limited" value="{{ isset($l->patient)?$l->patient->street_address:'' }}" >
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
										<label for="area">Area</label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" id="area" name="area" class="form-control" placeholder="Sahakar Nagar" value="{{ isset($l->patient)?$l->patient->area:'' }}">
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
										<label for="city">City</label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" id="city" name="city" class="form-control" placeholder="Bengaluru" value="{{ isset($l->patient)?$l->patient->city:'' }}" >
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
										<label for="zipcode">Zip Code</label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" id="zipcode" name="zipcode" class="form-control" placeholder="560092" value="{{ isset($l->patient)?$l->patient->zipcode:'' }}">
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
										<label for="state">State</label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" id="state" name="state" class="form-control" placeholder="Karnataka" value="{{ isset($l->patient)?$l->patient->state:'' }}" >
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
										<label for="country">Country</label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" id="country" name="country" class="form-control" placeholder="India" value="{{ isset($l->patient)?$l->patient->country:'' }}" >
											</div>
										</div>
									</div>
								</div>
								<h4 class="card-inside-title">
									Location Co-ordinates
									<small>Lattitude and Longitude</small>
								</h4>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
										<label for="latitude">Latitude</label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" id="latitude" name="latitude" class="form-control" placeholder="Latitude" value="{{ isset($l->patient)?$l->patient->latitude:'' }}">
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
										<label for="longitude">Longitude</label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" id="longitude" name="longitude" class="form-control" placeholder="Longitude" value="{{ isset($l->patient)?$l->patient->longitude:'' }}">
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-sm-6 form-horizontal">
								<h4 class="card-inside-title col-teal">
									Communication Details
								</h4>
								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label required">
										<label for="contact_number">Contact Number</label>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" id="contact_number" name="contact_number" onkeyup="checknumber();" class="form-control intl" value="{{ isset($l->patient)?$l->patient->contact_number:'' }}" required>
											</div>
											<span id="number_status" style="font-weight: bold;"></span>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
										<label for="alternate_number">Alternate Number</label>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="tel" id="alternate_number" name="alternate_number" class="form-control intl" placeholder="Alternate Number" value="{{ isset($l->patient)?$l->patient->alternate_number:'' }}">
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
										<label for="email">Email</label>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" id="email" name="email" class="form-control searchCol" placeholder="someone@example.com"value="{{ isset($l->patient)?$l->patient->email:'' }}">
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
										<label for="enquirer_name">Enquirer Name</label>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" id="enquirer_name" name="enquirer_name" class="form-control" placeholder="Nitish Kumar" value="{{ isset($l->patient)?$l->patient->enquirer_name:'' }}">
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
										<label for="relationship_with_patient">Relationship with Patient</label>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<select id="relationship_with_patient" name="relationship_with_patient" class="form-control show-tick" data-live-search="true">
													<option value="">-- Please select --</option>
													<option value="FATHER">FATHER</option>
													<option value="MOTHER">MOTHER</option>
													<option value="BROTHER">BROTHER</option>
													<option value="SISTER">SISTER</option>
													<option value="HUSBAND">HUSBAND</option>
													<option value="WIFE">WIFE</option>
													<option value="DAUGHTER">DAUGHTER</option>
													<option value="SON">SON</option>
													<option value="MOTHER-IN-LAW">MOTHER-IN-LAW</option>
													<option value="FATHER-IN-LAW">FATHER-IN-LAW</option>
													<option value="BROTHER-IN-LAW">BROTHER-IN-LAW</option>
													<option value="GRANDFATHER">GRANDFATHER</option>
													<option value="GRANDMOTHER">GRANDMOTHER</option>
													<option value="UNCLE">UNCLE</option>
													<option value="AUNT">AUNT</option>
													<option value="FRIEND">FRIEND</option>
													<option value="SELF">SELF</option>
													<option value="OTHER">OTHER</option>
												</select>
											</div>
										</div>
									</div>
								</div>

								<h4 class="card-inside-title">
									Location Map
								</h4>
								<div class="pac-card" id="pac-card">
									<div>
										<div id="title">
											Location search
										</div>
									</div>
									<div id="pac-container">
										<input type="text" id="pac-input" placeholder="Type here to search" />
									</div>
								</div>
								<div id="map" style="width: 500px; height:300px;"></div>
								<div id="infowindow-content">
									<img src="" width="16" height="16" id="place-icon">
									<span id="place-name"  class="title"></span><br>
									<span id="place-address"></span>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						{{ csrf_field() }}
						<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-success waves-effect pull-right btnSavePatientDetails">Save</button>
						<input type="hidden" name="patient_id" value="{{ $l->patient->id }}">
					</div>
				</div>
			</form>
		</div>
	</div>

	<!-- Edit Case Modal -->
	<div class="modal fade" id="editCaseModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" role="document">
			<form id="editCaseForm" id="caseForm" action="{{ route('lead.save-lead') }}" method="POST">
				{{ csrf_field() }}
				<input type="hidden" name="id" value="{{ isset($l)?Helper::encryptor('encrypt',$l->id):0 }}" />
				<input type="hidden" name="patient_id" value="{{ isset($l)?Helper::encryptor('encrypt',$l->patient_id):0 }}" />
				<input type="hidden" name="mode" value="{{ Helper::encryptor('encrypt','L') }}" />
				<input type="hidden" name="status" value="{{ $l->status }}" />

				<div class="modal-content">
					<div class="modal-header bg-blue">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="largeModalLabel">Edit Case Details <span class="crn_no"></span></h4>
					</div>
					<div class="modal-body">
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active">
								<a href="#case_details_tab" data-toggle="tab" aria-expanded="false">
									<i class="material-icons">assignment</i>Case Details
								</a>
							</li>
							<li role="presentation" class="">
								<a href="#services_tab" data-toggle="tab" aria-expanded="true">
									<i class="material-icons">child_friendly</i> Service Details
								</a>
							</li>
							<li role="presentation" class="">
								<a href="#other_tab" data-toggle="tab" aria-expanded="true">
									<i class="material-icons">done_all</i> Other Details
								</a>
							</li>
						</ul>
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane fade active in" id="case_details_tab">
								<div class="row clearfix form-horizontal" style="padding: 2px 15px; 5px 15px;">
									<div class="col-lg-8">
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="case_description">Case Description</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group">
													<div class="form-line">
														<textarea id="case_description" name="case_description" class="form-control" placeholder="Describe the patient's condition">{{ isset($l)?$l->case_description:'' }}</textarea>
													</div>
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="case_type">Type of Case</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group">
													<div class="form-line">
														<select data-live-search="true" id="case_type" name="case_type" class="form-control">
															<option value="">- Select -</option>
															<option value="Neurology" {{ (isset($l) && $l->case_type == 'Neurology')?'selected':'' }}>Neurology</option>
															<option value="Ortho" {{ (isset($l) && $l->case_type == 'Ortho')?'selected':'' }}>Ortho</option>
															<option value="Oncology" {{ (isset($l) && $l->case_type == 'Oncology')?'selected':'' }}>Oncology</option>
															<option value="Cardiology" {{ (isset($l) && $l->case_type == 'Cardiology')?'selected':'' }}>Cardiology</option>
															<option value="General Surgery" {{ (isset($l) && $l->case_type == 'General Surgery')?'selected':'' }}>General Surgery</option>
															<option value="Other" {{ (isset($l) && $l->case_type == 'Other')?'selected':'' }}>Other</option>
														</select>
													</div>
												</div>
											</div>
										</div>
										<div style="background: #b6dbf9; padding:10px;border-radius: 4px">
											<div style="text-align: center;"><b>Medical History of Patient</b></div>
											<hr>
											<div class="row clearfix">
												<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
													<label for="medical_conditions">Medical Conditons <br>/ Current Diagnosis</label>
												</div>
												<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
													<div class="form-group">
														<input type="text" class="form-control input-sm" size="50" id="conditions" name="conditions" placeholder="Type here to search.(e.g. Abdominal Pain,Oxygen Toxicity)"><br>
														<small>Please enter unlisted entries manually below.</small>
														<textarea id="medical_conditions" name="medical_conditions" class="form-control" >{{ isset($l)?$l->medical_conditions:'' }}</textarea>
													</div>
												</div>
											</div>
											<div class="row clearfix">
												<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
													<label for="medications">Medications</label>
												</div>
												<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
													<div class="form-group">
														<input type="text" class="form-control input-sm" size="50" id="medications_list" name="medications_list" placeholder="Type here to search.(e.g. Aspirin,Clotrimazole)"><br>
														<small>Please enter unlisted entries manually below.</small>
														<textarea id="medications" name="medications" class="form-control">{{ isset($l)?$l->medications:'' }}</textarea>
													</div>
												</div>
											</div>
											<div class="row clearfix">
												<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
													<label for="procedures">Surgeries/Procedures</label>
												</div>
												<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
													<div class="form-group">
														<input type="text" class="form-control input-sm" size="50" id="procedures_list" name="procedures_list" placeholder="Type here to search.(e.g. Open Heart Surgery,Knee Arthoplasty)"><br>
														<small>Please enter unlisted entries manually below.</small>
														<textarea id="procedures" name="procedures" class="form-control">{{ isset($l)?$l->surgeries:'' }}</textarea>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-4">
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="hospital_name">Hospital Name</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group">
													<div class="form-line">
														<input type="text" id="hospital_name" name="hospital_name" class="form-control" placeholder="Apollo, Bangalore" value="{{ isset($l)?$l->hospital_name:'' }}">
													</div>
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="primary_doctor_name">Primary Doctor Name</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group">
													<div class="form-line">
														<input type="text" id="primary_doctor_name" name="primary_doctor_name" class="form-control" placeholder="Dr. Sunil Awasthi (Critical Care)" value="{{ isset($l)?$l->primary_doctor_name:'' }}">
													</div>
												</div>
											</div>
										</div>
										<div style="background: #f8c8c8; padding:10px;border-radius: 4px">
											<h4 class="card-inside-title">
												Special Instructions
												<small>Any Important details about patient for staff</small>
											</h4>
											<div class="row clearfix">
												<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
													<label for="special_instructions"></label>
												</div>
												<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
													<div class="form-group">
														<div class="form-line">
															<textarea rows="3" id="special_instructions" name="special_instructions" class="form-control" placeholder="Referral from CM's office" style="padding: 5px;">{{ isset($l)?$l->special_instructions:'' }}</textarea>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div role="tabpanel" class="tab-pane fade in" id="services_tab">
								<div class="row clearfix form-horizontal" style="padding: 2px 15px; 5px 15px;">
									<div class="col-md-6">
										<div class="row clearfix hide">
											<div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
												<label for="service_category">Service Category</label>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
												<div class="form-group">
													<select class="form-control show-tick"  data-live-search="true" id="service_category" name="service_category">
														<option value="">-- Please select --</option>
													@if(isset($servicecategories) && count($servicecategories))
														@foreach($servicecategories as $scat)
															<option value="{{ $scat->id }}" @if(isset($l) && $l->service_category == $scat->id){{ 'selected' }}@endif>{{ $scat->category_name }}</option>
															@endforeach
														@endif
													</select>
												</div>
											</div>
										</div><br>
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
												<label for="service_required">Service Required</label>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
												<div class="form-group">
													<select class="form-control show-tick"  data-live-search="true" id="service_required" name="service_required">
														<option value="">-- Please select --</option>
													@if(isset($services) && count($services))
														@foreach($services as $s)
															<option value="{{ $s->id }}" @if(isset($l) && $l->service_required == $s->id){{ 'selected' }}@endif>{{ $s->service_name }}</option>
															@endforeach
														@endif
													</select>
												</div>
											</div>
										</div><br>
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
												<label for="estimated_duration">Estimated Duration</label>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="form-group">
													<div class="form-line">
														<input type="number" id="estimated_duration" name="estimated_duration" class="form-control" placeholder="15" value="{{ isset($l)?$l->estimated_duration:'' }}">
													</div>
												</div>
											</div>
											<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="line-height: 3; text-align: left">
												<b>day(s)</b>
											</div>
										</div><br>
										@include('partials.branch-input')<br>
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
												<label for="manager_id">Manager</label>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
												<div class="form-group">
													<select class="form-control show-tick" data-live-search="true" id="manager_id" data-multiple-separator=", " data-live-search-placeholder="Select Manager(s)" multiple>
														@if(isset($managers) && count($managers))
															@foreach($managers as $manager)
																<option value="{{ $manager->id }}" @if(isset($l) && in_array($manager->id, explode(",",$l->manager_id))){{ 'selected' }}@endif>{{ trim($manager->full_name).' ['.$manager->employee_id.']' }}</option>
															@endforeach
														@endif
													</select>
													<input type="hidden" name="manager_id">
												</div>
											</div>
										</div>
									</div>

									<div class="col-md-6">
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
												<label for="gender_preference">Gender Preference</label>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
												<div class="form-group" style="margin-top: 1%">
													<input name="gender_preference" type="radio" id="gender_preference_any" class="with-gap radio-col-deep-purple" value="Any" checked />
													<label for="gender_preference_any">Any</label>
													<input name="gender_preference" type="radio" id="gender_preference_male" class="with-gap radio-col-deep-purple" value="Male" {{ (isset($l) && $l->gender_preference == 'Male')?'checked':'' }}/>
													<label for="gender_preference_male">Male</label>
													<input name="gender_preference" type="radio" id="gender_preference_female" class="with-gap radio-col-deep-purple" value="Female" {{ (isset($l) && $l->gender_preference == 'Female')?'checked':'' }}/>
													<label for="gender_preference_female">Female</label>
												</div>
											</div>
										</div><br>
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
												<label for="language_preference[]">Language Preference</label>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
												<div class="form-group" style="margin-top: 2.5%">
													<div class="demo-checkbox language-preference">
														@if(isset($languages) && count($languages))
															@foreach($languages as $language)
																<input type="checkbox" id="languages_{{ strtolower($language->language_name) }}" name="language_preference[]" class="filled-in chk-col-light-blue" value="{{ $language->language_name }}" {{ (isset($l) && in_array($language->language_name, explode(",",$l->language_preference)))?'checked':'' }} />
																<label style="width: 49%;" for="languages_{{ strtolower($language->language_name) }}">{{ $language->language_name }}</label>
															@endforeach
														@endif
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div role="tabpanel" class="tab-pane fade in" id="other_tab">
								<div class="row clearfix form-horizontal" style="padding: 2px 15px; 5px 15px;">
									<div class="col-md-6">
										<h4 class="card-inside-title">
											Assessment Schedule
											<small>Assessment Date and Time, Notes</small>
										</h4>
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
												<label for="assessment_required">Required</label>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
												<div class="form-group">
													<div class="switch" style="margin-top:5px;">
														<label>No<input type="checkbox" id="assessment_required" checked=""><span class="lever switch-col-green"></span>Yes</label>
													</div>
												</div>
											</div>
										</div><br>
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
												<label for="assessment_date">Date</label>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="form-group">
													<div class="form-line">
														<input type="text" id="assessment_date" name="assessment_date" class="form-control date" placeholder="19-07-2017" value="{{ isset($l)?Carbon\Carbon::parse($l->assessment_date)->format('d-m-Y'):'' }}">
													</div>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="form-group">
													<div class="form-line">
														<input type="text" id="assessment_time" name="assessment_time" class="form-control time" placeholder="09:00 AM" value="{{ isset($l)?Carbon\Carbon::parse($l->assessment_time)->format('h:i A'):'' }}">
													</div>
												</div>
											</div>
										</div><br>
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
												<label for="assessment_notes">Assessment Notes</label>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
												<div class="form-group">
													<div class="form-line">
														<textarea id="assessment_notes" name="assessment_notes" class="form-control" placeholder="Pre Assessment Notes">{{ isset($l)?$l->assessment_notes:'' }}</textarea>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="col-md-6">
										<h4 class="card-inside-title">
											Payment Details
											<small>Rate agreed, Advance, Payment Mode, Notes</small>
										</h4>
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
												<label for="rate_agreed">Rate Agreed</label>
											</div>
											<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="line-height: 3;font-weight: normal">
												Rs.
											</div>
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="form-group">
													<div class="form-line">
														<input type="number" min="0" id="rate_agreed" name="rate_agreed" class="form-control" placeholder="800" value="{{ isset($l)?$l->rate_agreed:'' }}">
													</div>
												</div>
											</div>
											<label> /- per day</label>
										</div><br>
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
												<label for="registration_amount">Registration Amount</label>
											</div>
											<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="line-height: 3;font-weight: normal">
												Rs.
											</div>
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="form-group">
													<div class="form-line">
														<input type="number" min="0" id="registration_amount" name="registration_amount" class="form-control" placeholder="1600" value="{{ isset($l)?$l->registration_amount:'' }}">
													</div>
												</div>
											</div>
										</div><br>
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
												<label for="payment_mode">Payment Mode</label>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
												<div class="form-group">
													<select class="form-control show-tick" id="payment_mode" name="payment_mode">
														<option value="">-- Please select --</option>
														<option value="Cash" {{ (isset($l) && $l->payment_mode == 'Cash')?'selected':'' }}>Cash</option>
														<option value="Credit Card" {{ (isset($l) && $l->payment_mode == 'Credit Card')?'selected':'' }}>Credit Card</option>
														<option value="Cheque/DD" {{ (isset($l) && $l->payment_mode == 'Cheque/DD')?'selected':'' }}>Cheque/DD</option>
														<option value="Online/NEFT" {{ (isset($l) && $l->payment_mode == 'Online/NEFT')?'selected':'' }}>Online/NEFT</option>
														<option value="Paytm" {{ (isset($l) && $l->payment_mode == 'Paytm')?'selected':'' }}>Paytm</option>
													</select>
												</div>
											</div>
										</div><br>
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
												<label for="payment_notes">Payment Notes</label>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
												<div class="form-group">
													<div class="form-line">
														<textarea id="payment_notes" name="payment_notes" class="form-control" placeholder="Advance amount to be collected prior service.">{{ isset($l)?$l->payment_notes:'' }}</textarea>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
									<h4 class="card-inside-title">
										Referral Details
										<small>Referrer Category, Referral Source, Referrer Name</small>
									</h4>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="referral_category">Category</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select class="form-control show-tick" data-live-search="true" id="referral_category" name="referral_category">
													<option value="">--Select--</option>
													@if(isset($categories))
														@foreach ($categories as $cat)
															<option value="{{ $cat->id }}" {{ (isset($l) && $l->referral_category == $cat->id)?'selected':'' }}>{{ $cat->category_name }}</option>
														@endforeach
													@endif
												</select>
											</div>
										</div>
									</div><br>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="referral_source">Source</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select class="form-control show-tick" data-live-search="true" id="referral_source" name="referral_source" disabled="">
													<option value="">-- Select Category First --</option>
												</select>
											</div>
										</div>
									</div><br>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="referrer_name">Referrer Name (If any)</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="referrer_name" name="referrer_name" class="form-control referrer-name" placeholder="Suhas PR" value="{{ isset($l)?$l->referrer_name:'' }}">
												</div>
											</div>
										</div>
									</div><br>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="converted_by">Converted By</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<select id="converted_by" name="converted_by" class="form-control show-tick" data-live-search="true">
														<option value="">-- Select Staff --</option>
														@if(isset($persons) && count($persons))
															@foreach ($persons as $staff)
																<option value="{{ $staff->id }}" {{ (isset($l) && $l->converted_by == $staff->id)?'selected':'' }}>{{ $staff->full_name.' - '.$staff->employee_id }}</option>
															@endforeach
														@endif
													</select>
												</div>
											</div>
										</div>
									</div><br>
									</div>

									<div class="col-md-6">
										<h4 class="card-inside-title">
											Remarks
										</h4>
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
												<label for="remarks">Remark</label>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
												<div class="form-group">
													<div class="form-line">
														<textarea id="remarks" name="remarks" class="form-control" placeholder="Assessment Required.">{{ isset($l)?$l->remarks:'' }}</textarea>
													</div>
												</div>
											</div>
										</div><br>
									</div>
									<input type="hidden" name="referral_value" id="referral_value" value="{{isset($l->referral_value)?$l->referral_value:'0'}}">
									<input type="hidden" name="referral_type" id="referral_type" value="{{isset($l->referral_type)?$l->referral_type:'0'}}">
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-success waves-effect pull-right" onclick="if(validator()) this.form.submit();">Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<!-- Check List Form Modal -->
	<div class="modal fade" id="checklistFormModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" role="document">
			<form id="checklistForm" id="checklistForm" action="{{ route('lead.storechecklist') }}" method="POST">
				{{ csrf_field() }}
				<input type="hidden" name="checklist_id" value="{{ isset($cl->id)?$cl->id:0 }}" />
				<input type="hidden" name="patient_id" value="{{ isset($l)?Helper::encryptor('encrypt',$l->patient_id):0 }}" />

				<div class="modal-content">
					<div class="modal-header bg-blue">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="largeModalLabel">Checklist Form <span class="crn_no"></span></h4>
					</div>
					<div class="modal-body">
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active">
								<a href="#part_one_tab" data-toggle="tab" aria-expanded="false">
									<i class="material-icons">assignment</i>SECTION 1
								</a>
							</li>
							<li role="presentation">
								<a href="#part_two_tab" data-toggle="tab" aria-expanded="false">
									<i class="material-icons">assignment</i>SECTION 2
								</a>
							</li>
							<li role="presentation">
								<a href="#part_three_tab" data-toggle="tab" aria-expanded="false">
									<i class="material-icons">assignment</i>SECTION 3
								</a>
							</li>
							<li role="presentation">
								<a href="#part_four_tab" data-toggle="tab" aria-expanded="false">
									<i class="material-icons">assignment</i>SECTION 4
								</a>
							</li>
						</ul>
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane fade active in" id="part_one_tab">
								<div class="col-lg-12">
									<div style="background: #b6dbf9; padding:10px;border-radius: 4px">
										<div style="text-align: center;"><b>Introduction</b></div>
										<hr>
										<div class="row clearfix">
											<div class="col-lg-12 col-md-12 col-sm-8 col-xs-7">
												<div class="form-group">
													<small>Company/Service manager/Care attendant introduction</small>
													<input type="checkbox" id="care_attendant_intro_check" name="care_attendant_intro_check" class="filled-in chk-col-light-blue" value="Yes"  {{ isset($cl->care_attendant_intro_check) && $cl->care_attendant_intro_check=="Yes"?'checked':'' }}/>
													<label style="width: 49%;" for="care_attendant_intro_check">.</label>
													<input type="text" id="care_attendant_intro" name="care_attendant_intro" class="form-control" value="{{ isset($cl->care_attendant_intro)?$cl->care_attendant_intro:'' }}">
												</div>
											</div>
										</div>
									</div><br><br>
									<div style="background: #b6dbf9; padding:10px;border-radius: 4px">
										<div style="text-align: center;"><b>Patient Details</b></div>
										<hr>
										<div class="row clearfix">
											<div class="col-lg-12 col-md-12 col-sm-8 col-xs-7">
												<div class="form-group">
													<small>Understand Patients Medical History And Expectations From us</small>
													<input type="checkbox" id="understand_medical_history_check" name="understand_medical_history_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->understand_medical_history_check) && $cl->understand_medical_history_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="understand_medical_history_check">.</label>
													<input type="text" id="understand_medical_history" name="understand_medical_history" class="form-control" value="{{ isset($cl->understand_medical_history)?$cl->understand_medical_history:'' }}">
													<small>Medicine Handover</small>
													<input type="checkbox" id="medicine_handover_check" name="medicine_handover_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->medicine_handover_check) && $cl->medicine_handover_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="medicine_handover_check">.</label>
													<input type="text" id="medicine_handover" name="medicine_handover" value="{{ isset($cl->medicine_handover)?$cl->medicine_handover:'' }}" class="form-control">
													<small>Suggesting value&service-Hospital bed,anti slipary mat,washroom handle,commod chair,air bed,rubber sheet,CCTV setup</small>
													<input type="checkbox" id="suggestingvalue_check" name="suggestingvalue_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->suggestingvalue_check) && $cl->suggestingvalue_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="suggestingvalue_check">.</label>
													<input type="text" id="suggestingvalue" name="suggestingvalue" value="{{ isset($cl->suggestingvalue)?$cl->suggestingvalue:'' }}" class="form-control">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="part_two_tab">
								<div class="col-lg-12">
									<div style="background: #b6dbf9; padding:10px;border-radius: 4px">
										<div style="text-align: center;"><b>Our Process</b></div>
										<hr>
										<div class="row clearfix">
											<div class="col-lg-12 col-md-12 col-sm-8 col-xs-7">
												<div class="form-group">
													<small>Office shift timings and Office timings for communication</small>
													<input type="checkbox" id="officeshifttimings_check" name="officeshifttimings_check" class="filled-in chk-col-light-blue" value="Yes" {{isset($cl->officeshifttimings_check) && $cl->officeshifttimings_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="officeshifttimings_check">.</label>
													<input type="text" id="officeshifttimings" name="officeshifttimings" value="{{ isset($cl->officeshifttimings)?$cl->officeshifttimings:'' }}" class="form-control">
													<small>On Sunday/holiday 1 service manager will be working and it will intimated to you on previous day</small>
													<input type="checkbox" id="serviceworking_check" name="serviceworking_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->serviceworking_check) && $cl->serviceworking_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="serviceworking_check">.</label>
													<input type="text" id="serviceworking" name="serviceworking" value="{{ isset($cl->serviceworking)?$cl->serviceworking:'' }}" class="form-control">
													<small>Monthly one planned follow visit by service manager</small>
													<input type="checkbox" id="plannedfollow_check" name="plannedfollow_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->plannedfollow_check) && $cl->plannedfollow_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="plannedfollow_check">.</label>
													<input type="text" id="plannedfollow" name="plannedfollow" value="{{ isset($cl->plannedfollow)?$cl->plannedfollow:'' }}" class="form-control">
													<small>Oue payment policy and modes of payment</small>
													<input type="checkbox" id="paymentpolicy_check" name="paymentpolicy_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->paymentpolicy_check) && $cl->paymentpolicy_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="paymentpolicy_check">.</label>
													<input type="text" id="paymentpolicy" name="paymentpolicy" value="{{ isset($cl->paymentpolicy)?$cl->paymentpolicy:'' }}" class="form-control">
													<small>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a)Minimum service trial period of 5 days only</small>
													<input type="checkbox" id="trailperiod_check" name="trailperiod_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->trailperiod_check) && $cl->trailperiod_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="trailperiod_check">.</label>
													<input type="text" id="trailperiod" name="trailperiod" value="{{ isset($cl->trailperiod)?$cl->trailperiod:'' }}" class="form-control">
													<small>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b)Service will be started only on the basis advance payment</small>
													<input type="checkbox" id="servicestarted_check" name="servicestarted_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->servicestarted_check) && $cl->servicestarted_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="servicestarted_check">.</label>
													<input type="text" id="servicestarted" name="servicestarted" value="{{ isset($cl->servicestarted)?$cl->servicestarted:'' }}" class="form-control">
													<small>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c)If you need service out of the station,charges will be applicable as below,Upto 7 days-1800 per day/Else for long term &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1500 per day</small>
													<input type="checkbox" id="servicestation_check" name="servicestation_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->servicestation_check) && $cl->servicestation_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="servicestation_check">.</label>
													<input type="text" id="servicestation" name="servicestation" value="{{ isset($cl->servicestation)?$cl->servicestation:'' }}" class="form-control">
													<small>Attendants uniform status</small>
													<input type="checkbox" id="uniformstatus_check" name="uniformstatus_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->uniformstatus_check) && $cl->uniformstatus_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="uniformstatus_check">.</label>
													<input type="text" id="uniformstatus" name="uniformstatus" value="{{ isset($cl->uniformstatus)?$cl->uniformstatus:'' }}" class="form-control">
													<small>Logbook process and details of parameters mentioned insheet</small>
													<input type="checkbox" id="logbook_check" name="logbook_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->logbook_check) && $cl->logbook_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="logbook_check">.</label>
													<input type="text" id="logbook" name="logbook" value="{{ isset($cl->logbook)?$cl->logbook:'' }}" class="form-control">
													<small>Clarify our terms and expectations from client</small>
													<input type="checkbox" id="clarifyterms_check" name="clarifyterms_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->clarifyterms_check) && $cl->clarifyterms_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="clarifyterms_check">.</label>
													<input type="text" id="clarifyterms" name="clarifyterms" value="{{ isset($cl->clarifyterms)?$cl->clarifyterms:'' }}" class="form-control">
													<small>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a)Providing breakfast and Tea to attendant is appreciated</small>
													<input type="checkbox" id="providingtea_check" name="providingtea_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->providingtea_check) && $cl->providingtea_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="providingtea_check">.</label>
													<input type="text" id="providingtea" name="providingtea" value="{{ isset($cl->providingtea)?$cl->providingtea:'' }}" class="form-control">
													<small>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b)Please avoid direct communication with attendant regarding leaves or changes in shift timing</small>
													<input type="checkbox" id="directcommunication_check" name="directcommunication_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->directcommunication_check) && $cl->directcommunication_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="directcommunication_check">.</label>
													<input type="text" id="directcommunication" name="directcommunication" value="{{ isset($cl->directcommunication)?$cl->directcommunication:'' }}" class="form-control">
													<small>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c)No housekeeping work will be done by attendants</small>
													<input type="checkbox" id="housekeping_check" name="housekeping_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->housekeping_check) && $cl->housekeping_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="housekeping_check">.</label>
													<input type="text" id="housekeping" name="housekeping" value="{{ isset($cl->housekeping)?$cl->housekeping:'' }}" class="form-control">
													<small>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;d)Intimation by email regarding outstation visits with attendants</small>
													<input type="checkbox" id="intimationemail_check" name="intimationemail_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->intimationemail_check) && $cl->intimationemail_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="intimationemail_check">.</label>
													<input type="text" id="intimationemail" name="intimationemail" value="{{ isset($cl->intimationemail)?$cl->intimationemail:'' }}" class="form-control">
													<small>Process of Night shift reporting through calls</small>
													<input type="checkbox" id="nightshift_check" name="nightshift_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->nightshift_check) && $cl->nightshift_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="nightshift_check">.</label>
													<input type="text" id="nightshift" name="nightshift" value="{{ isset($cl->nightshift)?$cl->nightshift:'' }}" class="form-control">
													<small>Replacement staff policy</small>
													<input type="checkbox" id="staffpolicy_check" name="staffpolicy_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->staffpolicy_check) && $cl->staffpolicy_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="staffpolicy_check">.</label>
													<input type="text" id="staffpolicy" name="staffpolicy" value="{{ isset($cl->staffpolicy)?$cl->staffpolicy:'' }}" class="form-control">
													<small>Incase of any issues we request not to send back any female care assistant back from duty during night shift for security reasons</small>
													<input type="checkbox" id="femaleshift_check" name="femaleshift_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->femaleshift_check) && $cl->femaleshift_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="femaleshift_check">.</label>
													<input type="text" id="femaleshift" name="femaleshift" value="{{ isset($cl->femaleshift)?$cl->femaleshift:'' }}" class="form-control">		
												</div>
											</div>
										</div>
									</div>								
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="part_three_tab">
								<div class="col-lg-12">
									<div style="background: #b6dbf9; padding:10px;border-radius: 4px">
										<div style="text-align: center;"><b>Set up</b></div>
										<hr>
										<div class="row clearfix">
											<div class="col-lg-12 col-md-12 col-sm-8 col-xs-7">
												<div class="form-group">
													<small>Define care plan as per patients medical condition and as prescribed</small>
													<input type="checkbox" id="medicalcondition_check" name="medicalcondition_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->medicalcondition_check) && $cl->medicalcondition_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="medicalcondition_check">.</label>
													<input type="text" id="medicalcondition" name="medicalcondition" value="{{ isset($cl->medicalcondition)?$cl->medicalcondition:'' }}" class="form-control">
													<small>Check available stock of medicines</small>
													<input type="checkbox" id="stockmedicine_check" name="stockmedicine_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->stockmedicine_check) && $cl->stockmedicine_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="stockmedicine_check">.</label>
													<input type="text" id="stockmedicine" name="stockmedicine" value="{{ isset($cl->stockmedicine)?$cl->stockmedicine:'' }}" class="form-control">
													<small>Explain care plan with Care Asst.</small>
													<input type="checkbox" id="careplan_check" name="careplan_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->careplan_check) && $cl->careplan_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="careplan_check">.</label>
													<input type="text" id="careplan" name="careplan" value="{{ isset($cl->careplan)?$cl->careplan:'' }}" class="form-control">
													<small>Do safety audit and provide suggestions</small>
													<input type="checkbox" id="safetyaudit_check" name="safetyaudit_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->safetyaudit_check) && $cl->safetyaudit_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="safetyaudit_check">.</label>
													<input type="text" id="safetyaudit" name="safetyaudit" value="{{ isset($cl->safetyaudit)?$cl->safetyaudit:'' }}" class="form-control">
												</div>
											</div>
										</div>
									</div>			
									<div style="background: #b6dbf9; padding:10px;border-radius: 4px">
										<div style="text-align: center;"><b>Grievance Redressal</b></div>
										<hr>
										<div class="row clearfix">
											<div class="col-lg-12 col-md-12 col-sm-8 col-xs-7">
												<div class="form-group">
													<small>Explain Client the Escalation Metrics of their issues</small>
													<input type="checkbox" id="escalation_check" name="escalation_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->escalation_check) && $cl->escalation_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="escalation_check">.</label>
													<input type="text" id="escalation" name="escalation" value="{{ isset($cl->escalation)?$cl->escalation:'' }}" class="form-control">
													<small>Mode of ecalation of their problems through Email,phone,WhatsApp</small>
													<input type="checkbox" id="problems_check" name="problems_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->problems_check) && $cl->problems_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="problems_check">.</label>
													<input type="text" id="problems" name="problems" value="{{ isset($cl->problems)?$cl->problems:'' }}" class="form-control">
													<small>Provide Operations Manager No. and company both the numbers</small>
													<input type="checkbox" id="providemanagerno_check" name="providemanagerno_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->providemanagerno_check) && $cl->providemanagerno_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="providemanagerno_check">.</label>
													<input type="text" id="providemanagerno" name="providemanagerno" value="{{ isset($cl->providemanagerno)?$cl->providemanagerno:'' }}" class="form-control">
													<small>Service Managers availability on Phone between 8am to 6pm</small>
													<input type="checkbox" id="servicemanagerphone_check" name="servicemanagerphone_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->servicemanagerphone_check) && $cl->servicemanagerphone_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="servicemanagerphone_check">.</label>
													<input type="text" id="servicemanagerphone" name="servicemanagerphone" value="{{ isset($cl->servicemanagerphone)?$cl->servicemanagerphone:'' }}" class="form-control">
												</div>
											</div>
										</div>
									</div>				
									<div style="background: #b6dbf9; padding:10px;border-radius: 4px">
										<div style="text-align: center;"><b>Awareness About Our Other Services</b></div>
										<hr>
										<div class="row clearfix">
											<div class="col-lg-12 col-md-12 col-sm-8 col-xs-7">
												<div class="form-group">
													<small>Our other services-Elder care centres,versova,thane</small>
													<br>
													<small>Companionship</small>
													<input type="checkbox" id="companionship_check" name="companionship_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->companionship_check) && $cl->companionship_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="companionship_check">.</label>
													<input type="text" id="companionship" name="companionship" value="{{ isset($cl->companionship)?$cl->companionship:'' }}" class="form-control">
													<small>Complementary Services-1 Physio and 1 activity session yearly</small>
													<input type="checkbox" id="omplementaryservices_check" name="omplementaryservices_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->omplementaryservices_check) && $cl->omplementaryservices_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="omplementaryservices_check">.</label>
													<input type="text" id="omplementaryservices" name="omplementaryservices" value="{{ isset($cl->omplementaryservices)?$cl->omplementaryservices:'' }}" class="form-control">
													<small>Mobile App and the use of it</small>
													<input type="checkbox" id="mobileapp_check" name="mobileapp_check" class="filled-in chk-col-light-blue" value="Yes" {{ isset($cl->mobileapp_check) && $cl->mobileapp_check=="Yes"?'checked':'' }} />
													<label style="width: 49%;" for="mobileapp_check">.</label>
													<input type="text" id="mobileapp" name="mobileapp" value="{{ isset($cl->mobileapp)?$cl->mobileapp:'' }}" class="form-control">
												</div>
											</div>
										</div>
									</div>								
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="part_four_tab">
								<div class="col-lg-12">
									<div class="row">
										<div style="background: #b6dbf9; padding:10px;border-radius: 4px">
											<div class="row clearfix">
												<div class="col-lg-12 col-md-12 col-sm-8 col-xs-7">
													<div class="form-group">
														<small>Key family memeber names for the daily coordination</small>
														<input type="text" id="keyfamily" name="keyfamily" value="{{ isset($cl->keyfamily)?$cl->keyfamily:'' }}" class="form-control">
														<small>Identify family member who will do invoice payment</small>
														<input type="text" id="identifyinvoice" name="identifyinvoice" value="{{ isset($cl->identifyinvoice)?$cl->identifyinvoice:'' }}" class="form-control">
														<small>Key family member name and signature</small>
														<input type="text" id="nameandsig" name="nameandsig" value="{{ isset($cl->nameandsig)?$cl->nameandsig:'' }}" class="form-control">
														<small>Service Manager name</small>
														<input type="text" id="smname" name="smname" value="{{ isset($cl->smname)?$cl->smname:'' }}" class="form-control">
													</div>
												</div>
											</div>
										</div><br><br>
										<div class="col-md-6">
											<div style="background: #b6dbf9; padding:10px;border-radius: 4px">
												<div class="row clearfix">
													<div class="col-lg-12 col-md-12 col-sm-8 col-xs-7">
														<div class="form-group">
															<small>Date/Time</small>
															<input type="text" id="date_time" name="date_time" value="{{ isset($cl->date_time)?$cl->date_time->format('Y-m-d h:i A'):'' }}" class="form-control datetimepicker">
														</div>
													</div>
												</div>
											</div>							
										</div>
										<div class="col-md-6">
											<div style="background: #b6dbf9; padding:10px;border-radius: 4px">
												<div class="row clearfix">
													<div class="col-lg-12 col-md-12 col-sm-8 col-xs-7">
														<div class="form-group">
															<small>Service Managers Sign</small>
															<input type="text" id="manager_sig" name="managersig" value="{{ isset($cl->managersig)?$cl->managersig:'' }}" class="form-control">
														</div>
													</div>
												</div>
											</div>							
										</div>
									</div>
								</div>
							</div>		
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-success waves-effect pull-right">@if(isset($cl->id)) Update @else Save @endif</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<!-- Assessment Selection Modal -->
	<div class="modal fade" id="assessmentSelectionModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header bg-blue">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="largeModalLabel">Assessment Type</h4>
				</div>
				<div class="modal-body">
					<br>
					<div class="col-sm-6 text-center">
						<a class="btn btn-block btn-primary" data-toggle="modal" data-target="#assessmentModal" onclick="$('#assessmentSelectionModal').modal('hide');">Simple</a>
					</div>
					<div class="col-sm-6 text-center">
						<a class="btn btn-block btn-warning" target="_blank" href="{{ route('lead.add-detailed-assessment',Helper::encryptor('encrypt',$l->id)) }}">Detailed</a>
					</div>
					<br><br>
				</div>
				<div class="modal-footer" style="border-top: none !important;">
				</div>
			</div>
		</div>
	</div>

	<!-- Add Form Modal -->
	<div class="modal fade" id="assessmentModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog" style="overflow-y: auto;">
		<div class="modal-dialog modal-lg" style="width: 80% !important;" role="document">
			<form action="{{ route('lead.save-form') }}" method="POST" id="clear_form1" name="clear_form1">
				<div class="modal-content">
					<div class="modal-header bg-blue">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="largeModalLabel">Add Form</h4>
					</div>
					<div class="modal-body">
						<div class="col-lg-4">
							<select class="form-control show-tick" data-live-search="true" id="schedule_details_type" name="schedule_details_type">
								<option selected disabled value="">-- Please select Form--</option>
								<optgroup label="Assessment">
									{{-- <option data-category="Assessment" data-specialization="physiotherapy">Physiotherapy</option> --}}
									<option data-category="Assessment" data-specialization="nursing">Nursing</option>
								</optgroup>
								<optgroup label="Lab Test" class="hide">
									<option data-category="LabTest" data-specialization="labtest">Lab Test</option>
								</optgroup>
							</select>
						</div>
						<div class="col-lg-8 labhide">
							<select class="form-control show-tick" data-live-search="true" id="schedule_details" name="schedule_details">
								<option value="">-- Please select a Manger for Assessment--</option>
								@foreach($persons as $i => $p)
									<option data-user-id="{{$p->id}}">{{ $p->full_name }}</option>
								@endforeach
							</select>
						</div>
						<div class="forms" style="margin-top: 30px;">
							{{-- include various forms here--}}
						</div>
					</div>
					<div class="modal-footer labhide" style="border-top: none !important;">
						<input type="hidden" name="lead_id" value="{{Helper::encryptor('encrypt',$l->id)}}"/>
						<input type="hidden" name="user_id" id="listed_user_id" value="0" />
						<input type="hidden" name="form_type" id="listed_form_type" value="0" />
						{{ csrf_field() }}
						<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-success waves-effect pull-right" onclick="return checkSimpleAssessment()">Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<!-- View Assessment Modal -->
	<div class="modal fade" id="viewassessmentModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content" >
				<div id="assessment-report">
					<div class="modal-header bg-blue hidden-print">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="largeModalLabel">REPORT</h4>
					</div>
					<div class="modal-body" id="view-assessment-data">
						<div id="print-content">
							<table border="1" class="table table-bordered condensed" id="viewAssessmentTable">
							</table>
						</div>
					</div>
					<div class="modal-footer" style="border-top: none !important;">
						<input type="hidden" name="lead_id" value="{{Helper::encryptor('encrypt',$l->id)}}"/>
						<input type="hidden" name="episode_id" value="{{$l->episode_id}}" />
						<input type="hidden" name="caregiver_id" id="listed_caregiver_id" value="0" />
						<input type="hidden" name="patient_id" value="{{$l->patient_id}}" />
						<button type="button" class="btn btn-danger waves-effect pull-left noPrint " data-dismiss="modal">Close</button>
						<input type="button" class="btn btn-info noPrint " value="Print" onclick="window.print();"></input>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Edit Assessment Modal -->
	<div class="modal fade" id="editSimpleAssessmentModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog" style="overflow-y: auto;">
		<div class="modal-dialog modal-lg" style="width: 80% !important;" role="document">
			<form action="{{ route('lead.save-form') }}" method="POST" id="clear_form2">
				<div class="modal-content">
					<div class="modal-header bg-blue">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="largeModalLabel">Edit Assessment Form</h4>
					</div>
					<div class="modal-body">
						<div class="form-horizontal">
							<div class="form-group">
					    		<label for="schedule_details" class="control-label col-lg-6">This Assessment was done by</label>
					    		<div class="col-lg-6">
					    			<input type="text" class="form-control" name="schedule_details" id="assessor_name" readonly="true">
					    		</div>
					  		</div>
					  	</div>
						<div class="forms_editassessment">
							{{-- include various forms here--}}
						</div>
					</div>
					<div class="modal-footer labhide" style="border-top: none !important;">
						<input type="hidden" name="lead_id" value="{{Helper::encryptor('encrypt',$l->id)}}"/>
						<input type="hidden" name="case_forms_id" id="case_forms_id">
						<input type="hidden" name="form_type" value="nursing">
						{{ csrf_field() }}
						<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-success waves-effect pull-right">Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<!-- Add Followup Modal -->
	<div class="modal fade" id="followupModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog" style="overflow-y:auto;">
		<div class="modal-dialog modal-lg" style="width:80% !important;" role="document">
			<form action="{{ route('lead.save-follow-up-form') }}" method="POST" id="clear_form3" name="clear_form3">
				<div class="modal-content">
					<div class="modal-header bg-blue">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="largeModalLabel">Add Follow-Up</h4>
					</div>
					<div class="modal-body" style="margin-top:5px;">
						<div class="row">
	                        <div class="col-md-6">
	                        	<div class="form-group">
                                    <label class="control-label col-md-4 required" for="followup_manager">Manager</label>
                                    <div class="col-md-8 changableManager">
										<select class="form-control show-tick" data-live-search="true" id="followup_manager" name="followup_manager" required>
											<option value="">-- Please select a Manger --</option>
											@foreach($persons as $i => $p)
												<option data-user-id="{{$p->id}}">{{ $p->full_name }}</option>
											@endforeach
										</select>
									</div>
                                    <div class="col-md-8 nonChangableManager" style="display:none;">
										<div class="form-line">
											<input class="form-control" id="followup_manager_view_only" name="followup_manager_view_only" readonly>
											<input type="hidden" id="followup_id" name="followup_id">
										</div>
									</div>
								</div>
	                        </div>
	                        <div class="col-md-6">
	                        	<div class="form-group">
									<label class="control-label col-md-4 required" for="followup_date">Follow-Up Date</label>
									<div class="col-md-8">
										<div class="form-line">
											<input type="text" id="followup_date" name="followup_date" class="form-control" placeholder="Follow-Up Date" required autocomplete="off" style="cursor:pointer;">
										</div>
									</div>
								</div>
	                        </div>
	                    </div>
	                    <h4 class="card-inside-title text-center">Care Assistant Personal</h4>
						<div class="row">
	                        <div class="col-md-6">
	                        	<div class="form-group">
                                    <label class="control-label col-md-8" for="nails_clean_and_short">Nails Clean and short</label>
                                    <div class="col-md-4">
                                        <input name="nails_clean_and_short" type="radio" id="nails_clean_and_short_yes" class="with-gap radio-col-deep-purple" value="Yes">
                                        <label for="nails_clean_and_short_yes">Yes</label>
                                        <input name="nails_clean_and_short" type="radio" id="nails_clean_and_short_no" class="with-gap radio-col-deep-purple" value="No">
                                        <label for="nails_clean_and_short_no">No</label>
                                    </div>
                                </div>
	                        	<div class="form-group">
                                    <label class="control-label col-md-8" for="wearing_hair_band">Wearing Hair band</label>
                                    <div class="col-md-4">
                                        <input name="wearing_hair_band" type="radio" id="wearing_hair_band_yes" class="with-gap radio-col-deep-purple" value="Yes">
                                        <label for="wearing_hair_band_yes">Yes</label>
                                        <input name="wearing_hair_band" type="radio" id="wearing_hair_band_no" class="with-gap radio-col-deep-purple" value="No">
                                        <label for="wearing_hair_band_no">No</label>
                                    </div>
                                </div>
	                        	<div class="form-group">
                                    <label class="control-label col-md-8" for="clean_cloth">Clean cloth</label>
                                    <div class="col-md-4">
                                        <input name="clean_cloth" type="radio" id="clean_cloth_yes" class="with-gap radio-col-deep-purple" value="Yes">
                                        <label for="clean_cloth_yes">Yes</label>
                                        <input name="clean_cloth" type="radio" id="clean_cloth_no" class="with-gap radio-col-deep-purple" value="No">
                                        <label for="clean_cloth_no">No</label>
                                    </div>
                                </div>
	                        </div>
	                        <div class="col-md-6">
	                        	<div class="form-group">
                                    <label class="control-label col-md-8" for="odor">Odor</label>
                                    <div class="col-md-4">
                                        <input name="odor" type="radio" id="odor_yes" class="with-gap radio-col-deep-purple" value="Yes">
                                        <label for="odor_yes">Yes</label>
                                        <input name="odor" type="radio" id="odor_no" class="with-gap radio-col-deep-purple" value="No">
                                        <label for="odor_no">No</label>
                                    </div>
                                </div>
	                        	<div class="form-group">
                                    <label class="control-label col-md-8" for="toilet_usage_and_sanitary_mgmt">Toilet usage and sanitary mgmt</label>
                                    <div class="col-md-4">
                                        <input name="toilet_usage_and_sanitary_mgmt" type="radio" id="toilet_usage_and_sanitary_mgmt_yes" class="with-gap radio-col-deep-purple" value="Yes">
                                        <label for="toilet_usage_and_sanitary_mgmt_yes">Yes</label>
                                        <input name="toilet_usage_and_sanitary_mgmt" type="radio" id="toilet_usage_and_sanitary_mgmt_no" class="with-gap radio-col-deep-purple" value="No">
                                        <label for="toilet_usage_and_sanitary_mgmt_no">No</label>
                                    </div>
                                </div>
	                        </div>
	                    </div>
	                    <h4 class="card-inside-title text-center">Patient Management</h4>
						<div class="row">
	                        <div class="col-md-6">
	                        	<div class="form-group">
                                    <label class="control-label col-md-8" for="bed_making_done_properly">Bed making done properly</label>
                                    <div class="col-md-4">
                                        <input name="bed_making_done_properly" type="radio" id="bed_making_done_properly_yes" class="with-gap radio-col-deep-purple" value="Yes">
                                        <label for="bed_making_done_properly_yes">Yes</label>
                                        <input name="bed_making_done_properly" type="radio" id="bed_making_done_properly_no" class="with-gap radio-col-deep-purple" value="No">
                                        <label for="bed_making_done_properly_no">No</label>
                                    </div>
                                </div>
	                        	<div class="form-group">
                                    <label class="control-label col-md-8" for="room_hygiene_maintained">Room hygiene maintained</label>
                                    <div class="col-md-4">
                                        <input name="room_hygiene_maintained" type="radio" id="room_hygiene_maintained_yes" class="with-gap radio-col-deep-purple" value="Yes">
                                        <label for="room_hygiene_maintained_yes">Yes</label>
                                        <input name="room_hygiene_maintained" type="radio" id="room_hygiene_maintained_no" class="with-gap radio-col-deep-purple" value="No">
                                        <label for="room_hygiene_maintained_no">No</label>
                                    </div>
                                </div>
	                        	<div class="form-group">
                                    <label class="control-label col-md-8" for="oral_care_done_properly">Oral care done properly</label>
                                    <div class="col-md-4">
                                        <input name="oral_care_done_properly" type="radio" id="oral_care_done_properly_yes" class="with-gap radio-col-deep-purple" value="Yes">
                                        <label for="oral_care_done_properly_yes">Yes</label>
                                        <input name="oral_care_done_properly" type="radio" id="oral_care_done_properly_no" class="with-gap radio-col-deep-purple" value="No">
                                        <label for="oral_care_done_properly_no">No</label>
                                    </div>
                                </div>
	                        	<div class="form-group">
                                    <label class="control-label col-md-8" for="back_care_done_properly">Back care done properly</label>
                                    <div class="col-md-4">
                                        <input name="back_care_done_properly" type="radio" id="back_care_done_properly_yes" class="with-gap radio-col-deep-purple" value="Yes">
                                        <label for="back_care_done_properly_yes">Yes</label>
                                        <input name="back_care_done_properly" type="radio" id="back_care_done_properly_no" class="with-gap radio-col-deep-purple" value="No">
                                        <label for="back_care_done_properly_no">No</label>
                                    </div>
                                </div>
	                        	<div class="form-group">
                                    <label class="control-label col-md-8" for="bed_sores_monitoring_or_check_observation">Bed sores monitoring/ check observation</label>
                                    <div class="col-md-4">
                                        <input name="bed_sores_monitoring_or_check_observation" type="radio" id="bed_sores_monitoring_or_check_observation_yes" class="with-gap radio-col-deep-purple" value="Yes">
                                        <label for="bed_sores_monitoring_or_check_observation_yes">Yes</label>
                                        <input name="bed_sores_monitoring_or_check_observation" type="radio" id="bed_sores_monitoring_or_check_observation_no" class="with-gap radio-col-deep-purple" value="No">
                                        <label for="bed_sores_monitoring_or_check_observation_no">No</label>
                                    </div>
                                </div>
	                        	<div class="form-group">
                                    <label class="control-label col-md-8" for="catheter_care_done_properly">Catheter care done properly</label>
                                    <div class="col-md-4">
                                        <input name="catheter_care_done_properly" type="radio" id="catheter_care_done_properly_yes" class="with-gap radio-col-deep-purple" value="Yes">
                                        <label for="catheter_care_done_properly_yes">Yes</label>
                                        <input name="catheter_care_done_properly" type="radio" id="catheter_care_done_properly_no" class="with-gap radio-col-deep-purple" value="No">
                                        <label for="catheter_care_done_properly_no">No</label>
                                    </div>
                                </div>
	                        	<div class="form-group">
                                    <label class="control-label col-md-8" for="rt_and_peg_care_done_properly">R.T & P. E.G. care  done properly</label>
                                    <div class="col-md-4">
                                        <input name="rt_and_peg_care_done_properly" type="radio" id="rt_and_peg_care_done_properly_yes" class="with-gap radio-col-deep-purple" value="Yes">
                                        <label for="rt_and_peg_care_done_properly_yes">Yes</label>
                                        <input name="rt_and_peg_care_done_properly" type="radio" id="rt_and_peg_care_done_properly_no" class="with-gap radio-col-deep-purple" value="No">
                                        <label for="rt_and_peg_care_done_properly_no">No</label>
                                    </div>
                                </div>
	                        </div>
	                        <div class="col-md-6">
	                        	<div class="form-group">
                                    <label class="control-label col-md-8" for="hair_wash_done_properly">Hair wash done properly</label>
                                    <div class="col-md-4">
                                        <input name="hair_wash_done_properly" type="radio" id="hair_wash_done_properly_yes" class="with-gap radio-col-deep-purple" value="Yes">
                                        <label for="hair_wash_done_properly_yes">Yes</label>
                                        <input name="hair_wash_done_properly" type="radio" id="hair_wash_done_properly_no" class="with-gap radio-col-deep-purple" value="No">
                                        <label for="hair_wash_done_properly_no">No</label>
                                    </div>
                                </div>
	                        	<div class="form-group">
                                    <label class="control-label col-md-8" for="nail_care_observation">Nail care observation</label>
                                    <div class="col-md-4">
                                        <input name="nail_care_observation" type="radio" id="nail_care_observation_yes" class="with-gap radio-col-deep-purple" value="Yes">
                                        <label for="nail_care_observation_yes">Yes</label>
                                        <input name="nail_care_observation" type="radio" id="nail_care_observation_no" class="with-gap radio-col-deep-purple" value="No">
                                        <label for="nail_care_observation_no">No</label>
                                    </div>
                                </div>
	                        	<div class="form-group">
                                    <label class="control-label col-md-8" for="bp_and_temperature_checking_done_properly">B.P.& Temperature checking done properly</label>
                                    <div class="col-md-4">
                                        <input name="bp_and_temperature_checking_done_properly" type="radio" id="bp_and_temperature_checking_done_properly_yes" class="with-gap radio-col-deep-purple" value="Yes">
                                        <label for="bp_and_temperature_checking_done_properly_yes">Yes</label>
                                        <input name="bp_and_temperature_checking_done_properly" type="radio" id="bp_and_temperature_checking_done_properly_no" class="with-gap radio-col-deep-purple" value="No">
                                        <label for="bp_and_temperature_checking_done_properly_no">No</label>
                                    </div>
                                </div>
	                        	<div class="form-group">
                                    <label class="control-label col-md-8" for="exercise_done_properly">Exercise done properly</label>
                                    <div class="col-md-4">
                                        <input name="exercise_done_properly" type="radio" id="exercise_done_properly_yes" class="with-gap radio-col-deep-purple" value="Yes">
                                        <label for="exercise_done_properly_yes">Yes</label>
                                        <input name="exercise_done_properly" type="radio" id="exercise_done_properly_no" class="with-gap radio-col-deep-purple" value="No">
                                        <label for="exercise_done_properly_no">No</label>
                                    </div>
                                </div>
	                        	<div class="form-group">
                                    <label class="control-label col-md-8" for="patient_work_schedule_checks">Patient work schedule checks</label>
                                    <div class="col-md-4">
                                        <input name="patient_work_schedule_checks" type="radio" id="patient_work_schedule_checks_yes" class="with-gap radio-col-deep-purple" value="Yes">
                                        <label for="patient_work_schedule_checks_yes">Yes</label>
                                        <input name="patient_work_schedule_checks" type="radio" id="patient_work_schedule_checks_no" class="with-gap radio-col-deep-purple" value="No">
                                        <label for="patient_work_schedule_checks_no">No</label>
                                    </div>
                                </div>
	                        	<div class="form-group">
                                    <label class="control-label col-md-8" for="patient_hand_over_to_next_shift_person_done">Patient hand over to next shift person done</label>
                                    <div class="col-md-4">
                                        <input name="patient_hand_over_to_next_shift_person_done" type="radio" id="patient_hand_over_to_next_shift_person_done_yes" class="with-gap radio-col-deep-purple" value="Yes">
                                        <label for="patient_hand_over_to_next_shift_person_done_yes">Yes</label>
                                        <input name="patient_hand_over_to_next_shift_person_done" type="radio" id="patient_hand_over_to_next_shift_person_done_no" class="with-gap radio-col-deep-purple" value="No">
                                        <label for="patient_hand_over_to_next_shift_person_done_no">No</label>
                                    </div>
                                </div>
	                        	<div class="form-group">
                                    <label class="control-label col-md-8" for="end_of_day_patient_feedback_to_client">End of day patient feedback to Client</label>
                                    <div class="col-md-4">
                                        <input name="end_of_day_patient_feedback_to_client" type="radio" id="end_of_day_patient_feedback_to_client_yes" class="with-gap radio-col-deep-purple" value="Yes">
                                        <label for="end_of_day_patient_feedback_to_client_yes">Yes</label>
                                        <input name="end_of_day_patient_feedback_to_client" type="radio" id="end_of_day_patient_feedback_to_client_no" class="with-gap radio-col-deep-purple" value="No">
                                        <label for="end_of_day_patient_feedback_to_client_no">No</label>
                                    </div>
                                </div>
	                        </div>
	                    </div>
	                    <h4 class="card-inside-title text-center">Attendance</h4>
	                    <div class="row">
	                    	<div class="form-group">
	                            <label class="control-label col-md-5" for="miss_call_in_time_and_out_time_done_by_staff_and_client">Miss call in time & out time done by staff & client</label>
	                            <div class="col-md-7">
	                                <input name="miss_call_in_time_and_out_time_done_by_staff_and_client" type="radio" id="miss_call_in_time_and_out_time_done_by_staff_and_client_yes" class="with-gap radio-col-deep-purple" value="Yes">
	                                <label for="miss_call_in_time_and_out_time_done_by_staff_and_client_yes">Yes</label>
	                                <input name="miss_call_in_time_and_out_time_done_by_staff_and_client" type="radio" id="miss_call_in_time_and_out_time_done_by_staff_and_client_no" class="with-gap radio-col-deep-purple" value="No">
	                                <label for="miss_call_in_time_and_out_time_done_by_staff_and_client_no">No</label>
	                            </div>
	                        </div>
	                    	<div class="form-group">
	                            <label class="control-label col-md-5" for="followup_comments">Comments</label>
	                            <div class="col-md-7">
									<div class="form-line">
	                                	<textarea class="form-control" placeholder="Put your comment here" id="followup_comments" name="followup_comments"></textarea>
	                            	</div>
	                            </div>
	                        </div>
	                    </div>
					</div>
					<div class="modal-footer" style="border-top: none !important;">
						<input type="hidden" name="lead_id" value="{{Helper::encryptor('encrypt',$l->id)}}"/>
						{{ csrf_field() }}
						<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-success waves-effect pull-right btnSaveFollowup">Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<!-- Add Worklog Modal -->
	<div class="modal fade" id="addWorklogModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" role="document" style="width: 80% !important">
			<div class="modal-content" >
				<div id="worklog-report">
					<div class="modal-header bg-blue hidden-print">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="largeModalLabel">Update Worklog</h4>
					</div>
					<div class="modal-body">
						<br>
						<div class="row clearfix">
							<div class="col-lg-4 col-md-3 col-sm-5 col-xs-5 form-control-label" style="text-align: right !important">
								<label style="text-align: right !important" for="worklog_date">Schedule Date</label>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-8 col-xs-7" style="margin-bottom: 0px !important">
								<div class="form-group" style="margin-bottom: 0px !important">
									<div class="form-line">
										<select id="worklog_date" name="worklog_date" class="form-control show-tick" data-live-search="true" required="">
											<option value="">-</option>
											@if(isset($l) && count($l->schedules))
												@foreach ($l->schedules as $schedule)
													<option value="{{ $schedule->schedule_date->format('Y-m-d') }}">{{ $schedule->schedule_date->format('d-m-Y').' - '.$schedule->schedule_date->format('D') }}</option>
												@endforeach
											@endif
										</select>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-3 col-sm-5 col-xs-5 form-control-label">
								<a class="btn btn-warning btnGetWorklog">Continue</a>
							</div>
							<div class="clearfix"></div>
							<hr>
							<div class="loading-img" style="display: none;text-align:center">
								<img src="{{ asset('img/loading.gif') }}" style="width:9%;margin-bottom: -15px;"/>
							</div>
							<div class="worklog-block row clearfix" style="display: none">
								<div class="col-lg-4 col-md-3 col-sm-5 col-xs-5 form-control-label" style="text-align: right !important">
									<label style="text-align: right !important" for="worklog_caregiver">Caregiver</label>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-8 col-xs-7">
									<div class="form-group" style="margin-top: 8px;margin-bottom: 0px !important">
										<div class="form-line">
											<input type="hidden" id="worklog_schedule_id" value="" />
											<input type="hidden" id="worklog_caregiver_id" value="" />
											<span id="worklog_caregiver"></span>
										</div>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-lg-6 col-md-6">
									<h2 style="text-align:center; font-size: 16px !important; color: #444 !important; background: #eee;padding-top: 6px;padding-bottom: 6px">Vitals</h2>
									<table class="table table-bordered">
										<tr>
											<th class="text-center">Session</th>
											<th class="text-center">Morning</th>
											<th class="text-center">Afternoon</th>
											<th class="text-center">Evening</th>
											<th class="text-center">Night</th>
										</tr>
										<tr>
											<th>Blood Pressure</th>
											<td>
												<input class="form-control input-sm" type="text" id="worklog_blood_pressure_morning" placeholder="BP"/>
											</td>
											<td>
												<input class="form-control input-sm" type="text" id="worklog_blood_pressure_afternoon" placeholder="BP"/>
											</td>
											<td>
												<input class="form-control input-sm" type="text" id="worklog_blood_pressure_evening" placeholder="BP"/>
											</td>
											<td>
												<input class="form-control input-sm" type="text" id="worklog_blood_pressure_night" placeholder="BP"/>
											</td>
										</tr>
										<tr>
											<th>Sugar Level</th>
											<td><input class="form-control input-sm" type="number" id="worklog_sugar_level_morning" /></td>
											<td><input class="form-control input-sm" type="number" id="worklog_sugar_level_afternoon" /></td>
											<td><input class="form-control input-sm" type="number" id="worklog_sugar_level_evening" /></td>
											<td><input class="form-control input-sm" type="number" id="worklog_sugar_level_night" /></td>
										</tr>
										<tr>
											<th>Temperature</th>
											<td><input class="form-control input-sm" type="number" id="worklog_temperature_morning" /></td>
											<td><input class="form-control input-sm" type="number" id="worklog_temperature_afternoon" /></td>
											<td><input class="form-control input-sm" type="number" id="worklog_temperature_evening" /></td>
											<td><input class="form-control input-sm" type="number" id="worklog_temperature_night" /></td>
										</tr>
										<tr>
											<th>Pulse Rate</th>
											<td><input class="form-control input-sm" type="number" id="worklog_pulse_rate_morning" /></td>
											<td><input class="form-control input-sm" type="number" id="worklog_pulse_rate_afternoon" /></td>
											<td><input class="form-control input-sm" type="number" id="worklog_pulse_rate_evening" /></td>
											<td><input class="form-control input-sm" type="number" id="worklog_pulse_rate_night" /></td>
										</tr>
									</table>
								</div>
								<div class="col-lg-6 col-md-6">
									<h2 style="text-align:center; font-size: 16px !important; color: #444 !important; background: #eee;padding-top: 6px;padding-bottom: 6px">Tasks</h2>
									<div class="col-lg-12 multi-col-worklog tasks-block" style="width:100%;padding: 10px;">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-success btnSaveWorklog">Save</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- View Worklog Modal -->
	<div class="modal fade" id="viewWorklogModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content" >
				<div id="worklog-report">
					<div class="modal-header bg-blue hidden-print">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="largeModalLabel">Worklog</h4>
					</div>
					<div class="modal-body" id="view-assessment-data">
						<ul class="nav nav-tabs" role="tablist">
						    <li role="presentation" class="active">
						        <a href="#vitals" data-toggle="tab">
						            <i class="material-icons">assessment</i> Vitals
						        </a>
						    </li>
						    <li role="presentation">
						        <a href="#tasks" data-toggle="tab">
						            <i class="material-icons">playlist_add_check</i> Tasks
						        </a>
						    </li>
						</ul>

						<div class="tab-content">
							<div role="tabpanel" class="tab-pane fade in active" id="vitals">
								<div class="search-table-outter wrapper">
									<table border="1" class="table table-bordered condensed" id="viewWorklogTableVitals" style="margin-bottom: 0 !important;">
									</table>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade in" id="tasks">
								<div class="search-table-outter wrapper">
									<table border="1" class="table table-bordered condensed search-table inner routines" id="viewWorklogTableRoutines" style="margin-bottom: 0 !important;">
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer" style="border-top: none !important;">
						<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- View Treatment Modal -->
	<div class="modal fade" id="viewTreatmentModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content" >
				<div id="lab-report">
					<div class="modal-header bg-blue hidden-print">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="largeModalLabel">PHYSIOTHERAPY TREATMENTS</h4>
					</div>
					<div class="modal-body" id="view-treatment-data">
						<div id="print-content">
							<table border="1" class="table table-bordered condensed" id="viewTreatmentTable">
							</table>
						</div>
					</div>
					<div class="modal-footer" style="border-top: none !important;">
						<input type="hidden" name="careplan_id" value="{{Helper::encryptor('encrypt',$l->id)}}"/>
						<input type="hidden" name="crn_number" value="{{$l->lead_id}}" />
						<input type="hidden" name="patient_id" value="{{$l->patient_id}}" />
						<button type="button" class="btn btn-danger waves-effect pull-left noPrint " data-dismiss="modal">Close</button>
						<input type="button" class="btn btn-info noPrint " value="Print" onclick="window.print();"></input>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- View Schedules Modal -->
	<div class="modal fade" id="viewSchedulesModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" role="document" style="width: 78% !important;">
			<form id="createSchedulesForm" method="post" action="{{ route('lead.create-schedule-from-service') }}">
				<div class="modal-content">
					<div class="modal-header bg-blue">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="largeModalLabel">Schedules </h4>
					</div>
					<div class="modal-body" style="min-height: 400px !important">
						<div class="action-btns" style="margin-top:1%">
							<div class="col-md-5">
							@if($l->status == 'Converted')
								@ability('admin','leads-schedules-add,leads-schedules-edit')
								<a href="javascript:void(0);" class="btn bg-teal btnAddScheduleFromService">Add Schedule</a>
								<a class="btn btn-warning" id="passDates" data-toggle="modal" data-target="#scheduleStaffAssignModal">Assign Staff</a>
								<a class="btn btn-primary" id="btnCancelSchedule" data-toggle="modal" data-target="#cancellationScheduleModal">Cancel Schedule</a>
								@ability('admin','unconfirm-schedule')
								<a class="btn btn-primary" id="unconfirmSchedule">Unconfirm Schedule</a>
								@endability
								@endability
							@endif
								<code id="extensionWarning" style="position: fixed !important;left:38% !important;width: max-content !important;display: none;">Cannot Extend Schedules for Custom Dates</code>
							</div>
							<div class="col-md-7 addScheduleBlock" style="display:none;border: 1px solid #ccc;border-radius: 4px;padding-top: 10px">
								<div class="col-sm-12 outstanding-msg-block-schedules" style="display:none">
									<div class="alert alert-warning" style="background-color:#ff960073 !important;color: #ff0000 !important;font-size: 16px !important;">
									</div>
								</div>
								<div class="col-sm-8">
									<div class="row clearfix" style="margin-bottom:0">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label" style="margin-bottom:0">
											<label for="period">Period</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-6 col-xs-6" style="margin-bottom:0">
											<div class="form-group" style="margin-bottom:0">
												<div class="form-line">
													<input type="text" class="form-control daterange" placeholder="From date to To date"/>
													<input type="hidden" name="create_schedule_period_from" value="{{ date('Y-m-d') }}"/>
													<input type="hidden" name="create_schedule_period_to" value="{{ date('Y-m-d') }}"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-sm-3">
											<label class="">Interval</label>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<div class="form-line">
													<input style="font-size: 18px;" type="time" class="form-control time" id="create_schedule_start_time" name="create_schedule_start_time" placeholder="From Time"/>
												</div>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<div class="form-line">
													<input style="font-size: 18px;" type="time" class="form-control time" id="create_schedule_end_time" name="create_schedule_end_time" placeholder="To Time"/>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="extended_price">
												<input name="extended_price_rate" type="radio" class="radio-col-indigo" id="from_rate_card">
												<label for="from_rate_card"></label>
												<input name="extended_price_rate" type="radio" class="radio-col-green" id="from_prev_schedule" checked="">
												<label for="from_prev_schedule"></label>
											</div>
											<span style="color: red">Please confirm the price rate before proceeding.</span>
										</div>
									</div>
									<div class="row clearfix viewScheduleCaregiverBlock" style="margin-bottom:0">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label" style="margin-bottom:0">
											<label for="selectedStaffName">Caregiver</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-6 col-xs-6" style="margin-bottom:0">
											<div class="form-group" style="margin-bottom:0">
												<div style="margin-top: 8px;" id="selectedStaffName">-</div>
												<input type="hidden" id="caregiver_id" name="caregiver_id" value=""/>
											</div>
										</div>
									</div>
									<div class="row clearfix" style="border-top: 1px solid darkgrey;margin-top: 15px;">
										<div class="col-lg-4 col-md-3 col-sm-3 col-xs-12 form-control-label">
											<label for="delivery_address_schedule">Delivery Address</label>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
											<div class="form-group" style="margin-top: 1%">
												<input name="delivery_address_schedule" type="radio" id="delivery_address_schedule_default" class="with-gap radio-col-deep-purple" value="Default" checked="checked">
												<label for="delivery_address_schedule_default">Default</label>

												@foreach($extraAddress as $address)
												<input name="delivery_address_schedule" type="radio" id="delivery_address_schedule_{{ $address->id }}" class="with-gap radio-col-deep-purple" value="{{ $address->id }}">
												<label for="delivery_address_schedule_{{ $address->id }}">{{ $address->type }}</label>
												@endforeach
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4" style="margin-bottom:0">
									<div class="row clearfix" style="margin-bottom:0">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label" style="margin-bottom:0">
											<label for="period">Staff</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-6 col-xs-6" style="margin-bottom:0">
											<a class="btn bg-blue-grey btn-block waves-effect btnChooseStaff" data-block=".viewScheduleCaregiverBlock" data-profile="#selectedStaffName" data-caregiver="#caregiver_id" onclick="javascript: return clearSearchForm();">Choose Staff</a>
										</div>
									</div>
									<div class="row clearfix" style="margin-bottom:0">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label" style="margin-bottom:0">
											<label>&nbsp;</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-6 col-xs-6" style="margin-bottom:0"><br>
											<a class="btn btn-success waves-effect btnScheduleAction" data-mode="Schedule">Create Schedule</a>
										</div>
									</div>
								</div>
								<br>
								<input type="hidden" name="service_request_id" value="" />
								<input type="hidden" name="service_required" value="" />
								<input type="hidden" name="service_rate" value="" />
								<input type="hidden" name="road_distance" class="road_distance">
								<input type="hidden" name="ivrs_type" value="ivrs_visit_based_creation_from_schedule">
							</div>
						</div>
						<div class="clearfix"></div>
						<hr>
						<div class="row table-responsive clearfix" style="overflow-x: hidden;">
							<div class="loading-img" style="display: none;text-align:center">
								<img src="{{ asset('img/loading.gif') }}" style="width:9%;margin-top: -30px;margin-bottom: -15px;"/>
							</div>
							<table class="table table-bordered condensed table-schedules" style="zoom:80%;max-height: 400px;overflow:auto;">
								<thead>
									<tr>
										<th><input id="schedules-sel-all" type="checkbox" class="with-gap chk-col-blue filled-in" /><label for="schedules-sel-all">&nbsp;</label></th>
										<th>#</th>
										<th style="width:8% !important">Date</th>
										<th style="width:8% !important">ID</th>
										<th>Interval</th>
										<th style="width:8% !important">Staff ID</th>
										<th>Staff</th>
										<th style="width:5% !important">Phone</th>
										<th>Delivery To</th>
										<th>Status</th>
										<th style="width:25% !important" class="text-center">Action</th>
										<th>Amount</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
							<div class="clearfix"></div><br>
						</div>
					</div>
					<div class="modal-footer">
						@if(Helper::encryptor('decrypt',session('utype')) == 'Aggregator')
							<input type="hidden" name="provider_id" value="{{ isset($pid)?Helper::encryptor('encrypt',$pid):0  }}" />
						@endif
						<input type="hidden" name="lead_id" value="{{ Helper::encryptor('encrypt',$l->id) }}" />
						{{ csrf_field() }}
						<input type="hidden" name="mode" value="Schedule" />
						<button type="button" class="btn btn-danger waves-effect text-center" data-dismiss="modal">Close</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<!-- Assign Tasks Modal -->
	<div class="modal fade" id="assignTasksModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header bg-blue-grey">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Assign Task</h4>
				</div>
				<div class="modal-body">
					<div class="col-lg-12 multi-col" style="width:100%;padding: 10px;">
						@if(count($tasks))
							@foreach ($tasks as $task)
								<input type="checkbox" id="task_{{ $task->id }}" name="tasks[]" data-id="{{ $task->id }}" data-task="{{ $task->task_name }}" class="filled-in chk-col-indigo" />
								<label for="task_{{ $task->id }}" title="{{ $task->description }}" style="margin-right: 2px">{{ $task->task_name }}</label>
							@endforeach
						@endif
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-success waves-effect pull-right btnSelectTasks">Save</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Schedule Tasks Modal -->
	<div class="modal fade" id="scheduleTasksModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-md" role="document">
			<form id="scheduleTasksForm" action="{{ route('lead.update-schedule-tasks') }}" method="POST">
				{{ csrf_field() }}
				<div class="modal-content">
					<div class="modal-header bg-blue-grey">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="scheduleTasksLabel">Schedule Tasks</h4>
					</div>
					<div class="modal-body">
						<div class="row clearfix" style="margin-bottom:0">
							<div class="row clearfix" style="margin-bottom:0">
								<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label" style="margin-bottom:0">
									<label for="date">From</label>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="margin-bottom:0">
									<div class="form-group" style="margin-bottom:0">
										<div class="form-line">
											<input type="text" class="form-control" id="task_date" placeholder="Date" value="" style="color: #ff0000 !important"/>
											<input type="hidden" name="task_date" />
										</div>
									</div>
								</div>
								<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label" style="margin-bottom:0">
									<label for="replacement_to_date">To</label>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="margin-bottom:0">
									<div class="form-group" style="margin-bottom:0">
										<div class="form-line">
											<input type="text" class="form-control taskMultiAssign" name="task_to_date" id="task_to_date" placeholder="Date" style="color: #ff0000 !important"/>
										</div>
									</div>
								</div>
							</div>
						</div>
						<hr>
						<div class="col-lg-12 multi-col" style="width:100%;padding: 10px;">
							@if(count($tasks))
								@foreach ($tasks as $task)
									<input type="checkbox" id="task-{{ $task->id }}" name="tasks[]" data-id="{{ $task->id }}" data-task="{{ $task->task_name }}" class="filled-in chk-col-indigo" />
									<label for="task-{{ $task->id }}" title="{{ $task->description }}" style="margin-right: 2px">{{ $task->task_name }}</label>
								@endforeach
							@endif
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="modal-footer">
						<input type="hidden" name="assigned_tasks" />
						<input type="hidden" name="service_id" />
						<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-success waves-effect pull-right btnUpdateScheduleTasks">Update</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<!-- Service Cancellation Modal -->
	<div class="modal fade" id="cancellationModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-md" role="document">
			<form id="serviceCancellationForm" action="{{ route('lead.update-service-status') }}" method="POST">
				{{ csrf_field() }}
				<div class="modal-content">
					<div class="modal-header bg-blue">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="largeModalLabel">Cancel Service</h4>
					</div>
					<div class="modal-body">
						<div class="col-lg-6">
							<h4 class="card-inside-title">Reason For Cancellation</h4>
							<div class="demo-radio-button">
								<input name="cancelType" type="radio" class="with-gap" id="radio_1" checked="checked" value="Customer">
								<label for="radio_1">Cancelled By Customer</label><br>
								<input name="cancelType" type="radio" class="with-gap" id="radio_2" value="Provider">
								<label for="radio_2">Cancelled By Provider</label><br>
								<input name="cancelType" type="radio" class="with-gap" id="radio_3" value="Non Payment">
								<label for="radio_3">Cancelled Due to Non Payment</label><br>
								<input name="cancelType" type="radio" class="with-gap" id="radio_8" value="Rescheduled">
								<label for="radio_8">Rescheduled by Customer</label><br>
							</div>
						</div>
						<div class="col-lg-6">
							<h4 class="card-inside-title">Comments(if any)</h4>
							<textarea rows="3" style="width:100%;" id="cancelComment" name="cancelComment" placeholder="Comments"></textarea>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="modal-footer" style="border-top: none !important;">

						<input type="hidden" name="lead_id" id="cancellation_lead_id"/>
						<input type="hidden" name="id" id="cancellation_id" />
						<input type="hidden" name="status" id="cancellation_status" />

						<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-success waves-effect pull-right" onclick="this.disabled=true;this.form.submit();">Update</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<!-- Schedule Staff Assign Modal -->
	<div class="modal fade" id="scheduleStaffAssignModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-md" role="document">
				<div class="modal-content">
					<div class="modal-header bg-blue">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="largeModalLabel">Assign Staffs to Schedules</h4>
					</div>
					<div class="modal-body">
						<div class="row clearfix" style="margin-bottom:0">
							<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
								<label for="delivery_address_assign">Delivery Address</label>
							</div>
							<div class="col-lg-9 col-md-9 col-sm-6 col-xs-6" style="margin-bottom:0">
								<div class="form-group" style="margin-bottom:0">
									<input name="delivery_address_assign" type="radio" id="delivery_address_assign_default" class="with-gap radio-col-deep-purple" value="Default" checked="checked">
									<label for="delivery_address_assign_default">Default</label>
									@foreach($extraAddress as $address)
									<input name="delivery_address_assign" type="radio" id="delivery_address_assign_{{ $address->id }}" class="with-gap radio-col-deep-purple" value="{{ $address->id }}">
									<label for="delivery_address_assign_{{ $address->id }}">{{ $address->type }}</label>
									@endforeach
								</div>
							</div>
						</div>
						<div class="row clearfix" style="margin-bottom:0">
							<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label" style="margin-bottom:0">
								<label for="period">Staff</label>
							</div>
							<div class="col-lg-9 col-md-9 col-sm-6 col-xs-6" style="margin-bottom:0">
								<a class="btn bg-blue-grey btn-block waves-effect btnChooseStaff" data-block=".replaceScheduleCaregiverBlock" data-profile="#selectedStaffName" data-caregiver="#replaced_caregiver_id" onclick="javascript: return clearSearchForm();">Choose Staff</a>
							</div>
						</div>
						<div class="row clearfix replaceScheduleCaregiverBlock" style="margin-bottom:0">
							<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label" style="margin-bottom:0">
								<label for="selectedStaffName">Caregiver</label>
							</div>
							<div class="col-lg-9 col-md-9 col-sm-6 col-xs-6" style="margin-bottom:0">
								<div class="form-group" style="margin-bottom:0">
									<div style="margin-top: 8px;" id="selectedStaffName">-</div>
									<input type="hidden" id="replaced_caregiver_id" name="replaced_caregiver_id" value=""/>
								</div>
							</div>
						</div>
					</div>				
					<div class="clearfix"></div>
					<div class="modal-footer" style="border-top: none !important;">
						<input type="hidden" name="ivrs_type" class="ivrs_type" value="ivrs_scheduling" />
						<input type="hidden" name="datesFromSchedules" id="datesFromSchedules" value />
						<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-success waves-effect pull-right" id="assignStaff">Update</button>
					</div>
				</div>
		</div>
	</div>

	<!-- Schedule Cancellation Modal -->
	<div class="modal fade" id="cancellationScheduleModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-md" role="document">
			<form id="scheduleCancellationForm" action="{{ route('lead.cancel-schedules') }}" method="POST">
				{{ csrf_field() }}
				<div class="modal-content">
					<div class="modal-header bg-blue">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="largeModalLabel">Cancel Schedule(s)</h4>
					</div>
					<div class="modal-body">
						<div class="col-lg-6">
							<h4 class="card-inside-title">Reason For Schedule Cancellation</h4>
							<div class="demo-radio-button">
								<input name="cancelType" type="radio" class="with-gap" id="radio_4" checked="checked" value="Customer">
								<label for="radio_4">Cancelled By Customer</label><br>
								<input name="cancelType" type="radio" class="with-gap" id="radio_5" value="Provider">
								<label for="radio_5">Cancelled By Provider</label><br>
								<input name="cancelType" type="radio" class="with-gap" id="radio_6" value="Non Payment">
								<label for="radio_6">Cancelled Due to Non Payment</label><br>
								<input name="cancelType" type="radio" class="with-gap" id="radio_7" value="No Show">
								<label for="radio_7">No Show</label><br>
								<input name="cancelType" type="radio" class="with-gap" id="radio_9" value="Rescheduled">
								<label for="radio_9">Rescheduled by Customer</label><br>
							</div>
						</div>
						<div class="col-lg-6">
							<h4 class="card-inside-title">Comments(if any)</h4>
							<textarea rows="3" style="width:100%;" id="cancelScheduleComment" name="cancelScheduleComment" placeholder="Comments"></textarea>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="modal-footer" style="border-top: none !important;">
						<input type="hidden" name="ivrs_type" class="ivrs_type" value="ivrs_visit_based_cancellation_from_schedule" />
						<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-success waves-effect pull-right" id="cancelSchedules">Update</button>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection

@section('plugin.scripts')
	<!-- Select Plugin Js -->
	<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>

	<!-- Multiple Dates Picker -->
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
	<script src="https://cdn.rawgit.com/dubrox/Multiple-Dates-Picker-for-jQuery-UI/master/jquery-ui.multidatespicker.js"></script>

	<!-- Input Mask Plugin Js -->
	<script src="{{ ViewHelper::ThemePlugin('jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>
	<!-- Moment Plugin Js -->
	<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
	<!-- Bootstrap Material Datetime Picker Plugin Js -->
	<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>

	<!-- Bootstrap Tags Input Plugin Js -->
	<script src="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
	<script src="{{ ViewHelper::ThemePlugin('bootstrap-3-typeahead/bootstrap3-typeahead.js') }}"></script>

	<!-- Bootstrap Notify Plugin Js -->
	<script src="{{ ViewHelper::ThemePlugin('bootstrap-notify/bootstrap-notify.js') }}"></script>
	<script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
	<!-- noUiSlider-->
	<script src="{{ asset('themes/default/plugins/nouislider/nouislider.js') }}"></script>
	<script src="{{ asset('js/autocomplete.js')}}"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2-azeUdXbeAqQrvGtLZrbsXIFcofE_B8&libraries=places&region=IN"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.js"></script>
	<script src="https://clin-table-search.lhc.nlm.nih.gov/autocomplete-lhc-versions/13.0.2/autocomplete-lhc_jQueryUI.min.js"></script>
	<!--  Sweetalert -->
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="../../themes/default/js/pages/ui/tooltips-popovers.js"></script>
@endsection

@section('page.scripts')
	<script src="{{ ViewHelper::js('leads/form.js') }}"></script>
	<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
	<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
	<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
	<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
	<script src="/vendor/datatables/buttons.server-side.js"></script>

	<!-- Include Date Range Picker -->
	<script type="text/javascript" src="{{ ViewHelper::ThemeJs('daterangepicker.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
	<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.4.3/plugins/confirmDate/confirmDate.js"></script>
	<script src="../../themes/default/js/pages/ui/tooltips-popovers.js"></script>
	<script type="text/javascript">
		var assessmentFlag = '';
		var assessmentDone = '';
		var outstanding = 0;
		var nurseForm  = `@include('partials.forms.assessment.nurse')`;
		var doctorForm = `@include('partials.forms.assessment.doctor')`;
		var physioForm = `@include('partials.forms.assessment.physiotherapist')`;
		var labForm    = `@include('partials.forms.lab.labtest')`;
		var scheduledArr = [];
		var dateRangePickerConfig = {
			autoApply: true,
			autoUpdateInput: true,
			alwaysShowCalendars: true,
			opens: "center",
			parentEl: '#createSchedulesForm',
			locale: {
				format: 'DD-MM-YYYY',
				applyLabel: 'Apply',
				cancelLabel: 'Clear'
			},
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			isHighlightDate: function(arg){
			    if(scheduledArr.length){
			        // Prepare the date comparision
			        var thisMonth = arg._d.getMonth()+1;   // Months are 0 based
			        if (thisMonth<10){
			            thisMonth = "0"+thisMonth; // Leading 0
			        }
			        var thisDate = arg._d.getDate();
			        if (thisDate<10){
			            thisDate = "0"+thisDate; // Leading 0
			        }
			        var thisYear = arg._d.getYear()+1900;   // Years are 1900 based

			        var thisCompare = thisDate +"-"+ thisMonth +"-"+ thisYear;
			        //console.log('thisCompare: '+thisCompare+' => '+$.inArray(thisCompare,disabledArr));

			        if($.inArray(thisCompare,scheduledArr)!=-1){
			            //console.log("      ^--------- DATE FOUND HERE");
			            return true;
			        }
			    }
			}
		};

		function initDateRangePicker(){
			$('.daterange').daterangepicker(dateRangePickerConfig ,function(start, end, label) {
				$('input[name=create_schedule_period_from]').val(start.format('YYYY-MM-DD'));
				$('input[name=create_schedule_period_to]').val(end.format('YYYY-MM-DD'));
				rate = $('#createSchedulesForm .addScheduleBlock input[name=service_rate]').val();
				if(rate > 0){
					noOfDays = end.diff(start, 'days');
					totalAmount = noOfDays * rate;
					if(outstanding != 0){
						if(outstanding > 0 || totalAmount > outstanding){
							$('#createSchedulesForm .outstanding-msg-block-schedules > div').html('Customer has an outstanding balance, Schedules amount should not exceed outstanding amount!');
							$('#createSchedulesForm .outstanding-msg-block-schedules').show();
							$('#createSchedulesForm .btnChooseStaff').prop('disabled',true);
							$('#createSchedulesForm .btnScheduleAction').prop('disabled',true);
							$('#createSchedulesForm .btnChooseStaff').hide();
							$('#createSchedulesForm .btnScheduleAction').hide();
						}
					}else{
						$('#createSchedulesForm .outstanding-msg-block-schedules > div').html('');
						$('#createSchedulesForm .outstanding-msg-block-schedules').hide();
						$('#createSchedulesForm .btnChooseStaff').prop('disabled',false);
						$('#createSchedulesForm .btnScheduleAction').prop('disabled',false);
						$('#createSchedulesForm .btnChooseStaff').show();
						$('#createSchedulesForm .btnScheduleAction').show();
					}
				}
			});

			$('.daterange').on('apply.daterangepicker', function(ev, picker) {
				$(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
				$('#createSchedulesForm input[name=create_schedule_period_from]').val(picker.startDate.format('DD-MM-YYYY'));
				$('#createSchedulesForm input[name=create_schedule_period_to]').val(picker.endDate.format('DD-MM-YYYY'));
			});
		}

		function validator() {
			var fname = $('#editCaseForm #first_name').val();
			var gender = $('#editCaseForm #gender option:selected').val();
			var address = $('#editCaseForm #street_address').val();
			var city = $('#editCaseForm #city').val();
			var state = $('#editCaseForm #state').val();
			var country = $('#editCaseForm #country').val();
			var relationship = $('#editCaseForm #relationship_with_patient option:selected').val();
			var contactNumber = $('#contact_number').val();
			var email = $('#editCaseForm #email').val();
			@ability('admin','leads-service-details-add')
			var serviceRequired = $('#editCaseForm #service_required option:selected').val();
			var branchId = $('#editCaseForm #branch_id option:selected').val();
			@endability

			@ability('admin','leads-payment-details-add')
			var rateAgreed = $('#editCaseForm #rate_agreed').val();
			@endability

			if (fname == ""){
				alert("Please Enter First Name");
				return false;
			}
			if (gender == ""){
				alert("Please Enter Gender");
				return false;
			}
			if (address == ""){
				alert("Please Enter Address");
				return false;
			}
			if (city == ""){
				alert("Please Enter City");
				return false;
			}
			if (state == ""){
				alert("Please Enter State");
				return false;
			}
			if (country == ""){
				alert("Please Enter Country");
				return false;
			}
			if (relationship == ""){
				alert("Please Enter Relationship");
				return false;
			}
			if (contactNumber == ""){
				alert("Please Enter Contact Number");
				return false;
			}
			@if(Helper::getSetting('patient_email_mandatory')==1)
			if (email == ""){
				alert("Please Enter Email");
				return false;
			}
			@endif

			if($('#manager_id').val() != null){
				$('input[name=manager_id]').val($('#manager_id').val().join().toString());
			}

			return true;
		}

		$(function(){
			@if(isset($outstanding))
			outstanding = parseFloat('{{ $outstanding['outstanding_balance'] }}');
			@endif
			// console.log(outstanding);

			var autocomplete_init = false;
			var autocomplete;
			var map;

			$('.content').on('click','.editPatient',function(){
				$("#editPatientModal")
				.modal()
				.on("shown.bs.modal", function() {
					document.getElementById('pac-input').value = "";
					if(!autocomplete_init){
						initMap();
					}
					google.maps.event.trigger(map, 'resize');
				});
			});

			var componentForm = {
				sublocality_level_2: 'short_name',
				street_number: 'short_name',
				route: 'long_name',
				locality: 'long_name',
				administrative_area_level_1: 'long_name',
				country: 'long_name',
				postal_code: 'short_name'
			};

			var componentIds = {
				sublocality_level_2: 'area',
				locality: 'city',
				administrative_area_level_1: 'state',
				country: 'country',
				postal_code: 'zipcode'
			};

			function handleLocationError(browserHasGeolocation, infoWindow, pos) {
			}

			function initMap() {
				map = new google.maps.Map(document.getElementById('map'), {
					center: {lat: 16.28004385258969, lng: 78.10594640625004},
					zoom: 6
				});

				var card = document.getElementById('pac-card');
				var input = document.getElementById('pac-input');
				var types = document.getElementById('type-selector');
				var strictBounds = document.getElementById('strict-bounds-selector');

				map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

				autocomplete = new google.maps.places.Autocomplete(input);
				autocomplete_init = true;

				// Bind the map's bounds (viewport) property to the autocomplete object,
				// so that the autocomplete requests use the current map bounds for the
				// bounds option in the request.
				autocomplete.bindTo('bounds', map);

				var infowindow = new google.maps.InfoWindow();
				var infowindowContent = document.getElementById('infowindow-content');
				infowindow.setContent(infowindowContent);
				var marker = new google.maps.Marker({
					map: map,
					animation: google.maps.Animation.DROP,
					draggable: true,
					anchorPoint: new google.maps.Point(0, -29)
				});

				// Try HTML5 geolocation.
				if (navigator.geolocation) {
					navigator.geolocation.getCurrentPosition(function(position) {
						var pos = {
							lat: position.coords.latitude,
							lng: position.coords.longitude
						};
						map.setCenter(pos);
					}, function() {
						handleLocationError(true, infowindow, map.getCenter());
					});
				}
				else {
					// Browser doesn't support Geolocation
					handleLocationError(false, infowindow, map.getCenter());
				}

				google.maps.event.addListener(
					marker,
					'drag',
					function(event) {
						infowindow.close();
						document.getElementById('latitude').value = this.position.lat();
						document.getElementById('longitude').value = this.position.lng();
						//alert('drag');
				});

				google.maps.event.addListener(marker,'dragend',function(event) {
					infowindow.close();
					document.getElementById('latitude').value = this.position.lat();
					document.getElementById('longitude').value = this.position.lng();
					//alert('Drag end');
				});

				autocomplete.addListener('place_changed', function() {
					infowindow.close();
					marker.setVisible(false);
					var place = autocomplete.getPlace();
					if (!place.geometry) {
						// User entered the name of a Place that was not suggested and
						// pressed the Enter key, or the Place Details request failed.
						window.alert("No details available for input: '" + place.name + "'");
						return;
					}

					// If the place has a geometry, then present it on a map.
					if (place.geometry.viewport) {
						map.fitBounds(place.geometry.viewport);
					} else {
						map.setCenter(place.geometry.location);
						map.setZoom(17);  // Why 17? Because it looks good.
					}
					marker.setPosition(place.geometry.location);
					marker.setVisible(true);

					// Update Location Co-ordinates Values
					$('#latitude').val(place.geometry.location.lat());
					$('#longitude').val(place.geometry.location.lng());

					var address = '';
					if (place.address_components) {
						address = [
							(place.address_components[0] && place.address_components[0].short_name || ''),
							(place.address_components[1] && place.address_components[1].short_name || ''),
							(place.address_components[2] && place.address_components[2].short_name || '')
						].join(' ');

						// Get each component of the address from the place details
						// and fill the corresponding field on the form.
						for (var i = 0; i < place.address_components.length; i++) {
							var addressType = place.address_components[i].types[0];
							if (componentForm[addressType]) {
								var val = place.address_components[i][componentForm[addressType]];
								if(typeof componentIds[addressType] != 'undefined'){
									document.getElementById(componentIds[addressType]).value = val;
								}
							}
						}
					}

					infowindowContent.children['place-icon'].src = place.icon;
					infowindowContent.children['place-name'].textContent = place.name;
					infowindowContent.children['place-address'].textContent = address;
					infowindow.open(map, marker);
				});
			}


			initDateRangePicker();

			$('.content').on('change','#referral_category', function(){
				var val = $(this).val();
				var sourcesHtml = '<option value="">-- Select --</option>';
				var sources = sourcesList.filter(function(item){ return item.key === val; });

				if(sources.length > 0){
					for(key in sources){
						if(sources.hasOwnProperty(key)){
							sourcesHtml += '<option value="'+sources[key].id+'">'+sources[key].value+' - '+ sources[key].phone+'</option>';
						}
					}
					$('#referral_source').html(sourcesHtml);
					$('#referral_source').prop('disabled',false);
					$('#referral_source').selectpicker('refresh');
				}
			});

			var sourcesList = [];
			var percentagesList = [];
			@if(isset($sources))
			@foreach ($sources as $s)
			sourcesList.push({id: '{{ $s->id }}', key: '{{ $s->category_id }}', value:'{{ $s->source_name }}', phone: '{{ $s->phone_number }}', percentage: '{{ $s->referral_value }}'});
			percentagesList.push({id: '{{ $s->id }}', percentage: '{{ $s->referral_value }}', type: '{{ $s->charge_type }}' });
			@endforeach
			@endif

			$('.content').on('change','#referral_source', function(){
				var val = $(this).val();
				var result = $.grep(percentagesList, function(e){ return e.id == val; });
				$('#referral_value').val(result[0].percentage);
				$('#referral_type').val(result[0].type);
			});

			// Fill case details to edit modal
			@if(isset($l))
			$('#editCaseModal #referral_category').selectpicker('val','{{ $l->referral_category }}');
			$('#editCaseModal #referral_category').selectpicker('refresh');
			$('#editCaseModal #referral_category').trigger("change");

			$('#editCaseModal #referral_source').selectpicker('val','{{ $l->referral_source }}');
			$('#editCaseModal #referral_source').selectpicker('refresh');

			$('#editCaseModal #assessment_required').prop('checked','{{ (!empty($l->assessment_date) || $l->assessment_date != null)?true:false }}').trigger("change");

			$('#medical_conditions').tagsinput({
				typeahead: {
					minLength: 0,
					showHintOnFocus:"all",
					allowDuplicates: true,
				}
			});

			$('#medications').tagsinput({
				typeahead: {
					minLength: 0,
					showHintOnFocus:"all",
					allowDuplicates: true,
				}
			});

			$('#procedures').tagsinput({
				typeahead: {
					minLength: 0,
					showHintOnFocus:"all",
					allowDuplicates: true,
				}
			});

			var api_url = 'https://clin-table-search.lhc.nlm.nih.gov/api/';
			var conditionsArray = [];
			/*if($('#procedures_list').length){
				new Def.Autocompleter.Search('conditions', conditionsArray, {maxSelect: '*'});
				var searchConditions = new Def.Autocompleter.Search('conditions',api_url+'conditions/v3/search');

				searchConditions.options.onShow = function(d){
					$('#searchResults').attr('style', 'visibility:visible;color:green;top:302px !important');
				}

				searchConditions.onMouseDownListener = function(d){
					$('#medical_conditions').tagsinput('add',d.currentTarget.innerHTML);
				}
			}

			var medicationsArray = [];
			if($('#procedures_list').length){
				new Def.Autocompleter.Search('medications_list', medicationsArray, {maxSelect: '*'});
				var searchMedications = new Def.Autocompleter.Search('medications_list',api_url+'rxterms/v3/search?ef=STRENGTHS_AND_FORMS');

				searchMedications.onMouseDownListener = function(d){
					$('#medications').tagsinput('add',d.currentTarget.innerHTML);
				}
			}

			var proceduresArray = [];
			if($('#procedures_list').length){
				new Def.Autocompleter.Search('procedures_list', proceduresArray, {maxSelect: '*'});
				var searchProcedures = new Def.Autocompleter.Search('procedures_list',api_url+'procedures/v3/search');
				searchProcedures.onMouseDownListener = function(d){
					$('#procedures').tagsinput('add',d.currentTarget.innerHTML);
				}
			}

			$('.autocomp_selected ul').hide();
			if($('#medicine_name').length){
				new Def.Autocompleter.Search('medicine_name',api_url+'rxterms/v3/search?ef=STRENGTHS_AND_FORMS');
			}*/
			@endif

			@if($l->medical_conditions)
			$('#editCaseModal #medical_conditions').removeAttr('placeholder');
			@foreach(explode(",",$l->medical_conditions) as $mc)
			$('#editCaseModal #medical_conditions').tagsinput('add','{{ $mc }}');
			@endforeach
			@endif

			@if($l->medications)
			$('#editCaseModal #medications').removeAttr('placeholder');
			@foreach(explode(",",$l->medications) as $m)
			$('#editCaseModal #medications').tagsinput('add','{{ $m }}');
			@endforeach
			@endif

			@if($l->procedures)
			$('#editCaseModal #procedures').removeAttr('placeholder');
			@foreach(explode(",",$l->procedures) as $p)
			$('#editCaseModal #procedures').tagsinput('add','{{ $p }}');
			@endforeach
			@endif
			$('#editPatientModal #relationship_with_patient').val('{{ $l->patient->relationship_with_patient }}').change();
		});
	</script>
	<script>
		function checkSimpleAssessment(){
			var x = document.forms["clear_form1"]["schedule_details"].value;
		    if (x == "") {
		        alert("Please select a Manger for Assessment");
		        return false;
		    }
		}

		function editassessment(element,status){
		    var str = ``;
		    document.getElementById("case_forms_id").value = element;
		    str=$.parseHTML(nurseForm);
		    $('.forms_editassessment').html( str );
		    $('.forms_editassessment #assessment_date_nursing').datetimepicker({
		        format: 'DD-MM-YYYY',
		        showClear: true,
		        showClose: true,
		        useCurrent: false,
		        keepInvalid:true
		    });

		    var form_data;
			$( ".assessmentId_"+element).each(function() {
				form_data = $( this ).attr("data-form-data");
			});
			var formData = JSON.parse(form_data);
			$('.getdate').val(formData.assessment_date);
			$('input[name="assessment_method[]"][value="'+formData.assessment_method+'"]').prop('checked', true);
			$('input[name="patient_mobility[]"][value="'+formData.patient_mobility+'"]').prop('checked', true);
			$('input[name="patient_consciousness[]"][value="'+formData.patient_consciousness+'"]').prop('checked', true);
			var med_pro = formData.medical_procedures;
			$.each(med_pro, function(i){
				$('input[name="medical_procedures[]"][value="'+med_pro[i]+'"]').prop('checked', true);
			});
			$('input[name="suggested_home_care_professional"][value="'+formData.suggested_professional+'"]').prop('checked', true);
			$('input[name="assessment_status"][value="'+status+'"]').prop('checked','checked');
			$('textarea[name="notes"]').val(formData.comment);
			document.getElementById("assessor_name").value = formData.assessor_name;

			$('#editSimpleAssessmentModal').on('hidden.bs.modal', function (e) {
				document.getElementById("clear_form1").reset();
				document.getElementById("clear_form2").reset();
			});
		}

		function editFollowup(element,date){
		    var form_data;
			$( ".assessmentId_"+element).each(function() {
				form_data = $(this).attr("data-form-data");
			});
			var formData = JSON.parse(form_data);
			$('#followup_id').val(element);
			$('input[name="nails_clean_and_short"][value="'+formData.nails_clean_and_short+'"]').prop("checked",true);
			$('input[name="wearing_hair_band"][value="'+formData.wearing_hair_band+'"]').prop("checked",true);
			$('input[name="clean_cloth"][value="'+formData.clean_cloth+'"]').prop("checked",true);
			$('input[name="odor"][value="'+formData.odor+'"]').prop("checked",true);
			$('input[name="toilet_usage_and_sanitary_mgmt"][value="'+formData.toilet_usage_and_sanitary_mgmt+'"]').prop("checked",true);
			$('input[name="bed_making_done_properly"][value="'+formData.bed_making_done_properly+'"]').prop("checked",true);
			$('input[name="room_hygiene_maintained"][value="'+formData.room_hygiene_maintained+'"]').prop("checked",true);
			$('input[name="oral_care_done_properly"][value="'+formData.oral_care_done_properly+'"]').prop("checked",true);
			$('input[name="back_care_done_properly"][value="'+formData.back_care_done_properly+'"]').prop("checked",true);
			$('input[name="bed_sores_monitoring_or_check_observation"][value="'+formData.bed_sores_monitoring_or_check_observation+'"]').prop("checked",true);
			$('input[name="catheter_care_done_properly"][value="'+formData.catheter_care_done_properly+'"]').prop("checked",true);
			$('input[name="rt_and_peg_care_done_properly"][value="'+formData.rt_and_peg_care_done_properly+'"]').prop("checked",true);
			$('input[name="hair_wash_done_properly"][value="'+formData.hair_wash_done_properly+'"]').prop("checked",true);
			$('input[name="nail_care_observation"][value="'+formData.nail_care_observation+'"]').prop("checked",true);
			$('input[name="bp_and_temperature_checking_done_properly"][value="'+formData.bp_and_temperature_checking_done_properly+'"]').prop("checked",true);
			$('input[name="exercise_done_properly"][value="'+formData.exercise_done_properly+'"]').prop("checked",true);
			$('input[name="patient_work_schedule_checks"][value="'+formData.patient_work_schedule_checks+'"]').prop("checked",true);
			$('input[name="patient_hand_over_to_next_shift_person_done"][value="'+formData.patient_hand_over_to_next_shift_person_done+'"]').prop("checked",true);
			$('input[name="end_of_day_patient_feedback_to_client"][value="'+formData.end_of_day_patient_feedback_to_client+'"]').prop("checked",true);
			$('input[name="miss_call_in_time_and_out_time_done_by_staff_and_client"][value="'+formData.miss_call_in_time_and_out_time_done_by_staff_and_client+'"]').prop("checked",true);
			$('textarea[name="followup_comments"]').val(formData.followup_comments);
			$('#followup_manager_view_only').val(formData.followup_manager);
			$('#followup_date').val(date);

			$('#followup_manager').prop('required',false);
			$('#followupModal #largeModalLabel').html('Edit Follow-Up');
			$('#followupModal .btnSaveFollowup').html('Update');
			$('.changableManager').hide();
			$('.nonChangableManager').show();

			$('#followupModal').on('hidden.bs.modal', function(e){
				document.getElementById("clear_form3").reset();				
				$('#followup_manager').prop('required',true);
				$('#followup_id').val('');
				$('#followupModal #largeModalLabel').html('Save Follow-Up');
				$('#followupModal .btnSaveFollowup').html('Save');
				$('.changableManager').show();
				$('.nonChangableManager').hide();
			});
		}

		$('#followup_date').datetimepicker({
	        format: 'DD-MM-YYYY',
	        showClear: true,
	        showClose: true,
	        useCurrent: false,
	        keepInvalid:true
		});
	</script>
	<script type="text/javascript">
		$(function(){
			$(document).ready(function() {
				$("div.vertical-tab-menu>div.list-group>a").click(function(e) {
					e.preventDefault();
					$(this).siblings('a.active').removeClass("active");
					$(this).addClass("active");
					var index = $(this).index();
					$("div.vertical-tab>div.vertical-tab-content").removeClass("active");
					$("div.vertical-tab>div.vertical-tab-content").eq(index).addClass("active");
				});

				$('form').append(`<input type="hidden" name="lead_id" id="lead_id" value="{{ Helper::encryptor('encrypt',$l->id) }}">
				<input type="hidden" name="patient_id" id="patient_id" value="{{ Helper::encryptor('encrypt',$l->patient->id) }}">`);
			});

			$('.vertical-tab-menu .list-group > a').on('click', function (e) {
				// save the latest tab; use cookies if you like 'em better:
				localStorage.setItem('leadVerticalLastTab', $(this).index());
			});

			// go to the latest tab, if it exists:
			var lastTab = localStorage.getItem('leadVerticalLastTab');
			if (lastTab >= 0) {
				selectedItem = $('.vertical-tab-menu .list-group > a:eq('+ lastTab +')');
				$(selectedItem).siblings('a.active').removeClass("active");
				$(selectedItem).addClass("active");
				$("div.vertical-tab>div.vertical-tab-content").removeClass("active");
				$("div.vertical-tab>div.vertical-tab-content").eq(lastTab).addClass("active");
			}

			$('.content').on('click','.clearLocalStore', function() {
				return localStorage.removeItem('leadVerticalLastTab');
			});
		});
	</script>

	<script type="text/javascript">
		$(document).ready(function() {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': '{{ csrf_token() }}'//$('meta[name="csrf-token"]').attr('content')
				}
			});

			$('.content').on('change','#case_vendor', function(){
				// console.log($(this).val());
				if($(this).val() != null){
					$('#vendor_id').val($(this).val());
				}else{
					$('#vendor_id').val('');
				}
			});

			var dataTable = {
				"order": [[ 1, 'asc' ]],
				"displayLength": 50,
				"drawCallback": function ( settings ) {
					var api = this.api();
					var rows = api.rows( {page:'current'} ).nodes();
					var last = null;

					api.column(1, {page:'current'} ).data().each( function ( group, i ) {
						if(group != null){
							if ( last !== group ) {
								$(rows).eq( i ).before(
									'<tr class="group"><td colspan="6"><b>'+group.toUpperCase()+'</b></td></tr>'
								);
								last = group;
							}
						}
					});
				}
			};
		});

		$(function(){
			$('#editCaseModal #source').selectpicker('val','{{ isset($l->source)?$l->source:'' }}');

			$('#feedback_time').bootstrapMaterialDatePicker({
				format: 'DD-MM-YYYY - hh:mm A',
				time: true,
				clearButton: true,
			});
			$('.getdate').bootstrapMaterialDatePicker({
				format: 'DD-MM-YYYY',
				time: false,
				clearButton: true,
			});
			fpDate = $('.datepicker').flatpickr({
			    altInput: true,
			    allowInput:true,
			    altFormat: "d-m-Y",
			    dateFormat: "Y-m-d",
			    onChange: function(selectedDates, dateStr, instance) {
			        if(instance.element.id == 'cancel_from_date'){
			            fpDate[1].set('minDate',new Date($('#cancel_from_date').val()));
			            fpDate[1].setDate(new Date($('#cancel_from_date').val()));

			            fpDate[2].set('minDate',new Date($('#cancel_from_date').val()));
			            fpDate[2].setDate(new Date(new Date($('#cancel_from_date').val())));
			        }
			    },
			});

			fpDate = $('.datetimepicker').flatpickr({
			    enableTime: true,
    			dateFormat: "Y-m-d h:i K",
			});
		});

		$('.content').on('click','.btnsaveFeedbackComment',function(){
			var feedbackTime    = $('#feedback_time').val();
			var feedbackComment = $('#feedback_comment').val();
			var careplanID      = $('#careplan_id').val();

			if(feedbackTime && feedbackComment){
				$.ajax({
					url : '{{ route('case.save-feedback-comment') }}',
					type: 'POST',
					data: {feedback_time: feedbackTime, feedback_comment: feedbackComment, careplan_id: careplanID},
					success: function (data){
						window.location.reload();
					},
					error: function (error){
						console.log(error);
					}
				});
			}
		});

		function checkCaseLoss(optionValue)
		{
			if(optionValue){
				if(optionValue == 'Dropped'){
					document.getElementById("dropped").style.display = "block";
				}
				else{
					document.getElementById("dropped").style.display = "none";
				}

				if(optionValue == 'Follow Up'){
					document.getElementById("followup").style.display = "block";
				}
				else{
					document.getElementById("followup").style.display = "none";
				}
			}
			else{
				document.getElementById("dropped").style.display = "none";
				document.getElementById("followup").style.display = "none";
			}
		}

		function printValue(value) {
			if(typeof value != 'undefined'){
				return value;
			}
			return '-';
		}

		function formatKey(str){
			if(str){
				str = str.split("_").join(" ");
			}
			return str.toUpperCaseWords();
		}

		function viewAssessment(element)
		{
			var form_data;
			$( ".assessmentId_"+element).each(function() {
				form_data = $( this ).attr("data-form-data");
			});

			var data = JSON.parse(form_data);
			var html = `<tbody>`;

			var blockedVariables = ['careplan_id','schedule_id','assessform'];
			$.each(data, function(key, data) {
				//alert(key + ' is ' + data);
				if(!blockedVariables.contains(key)){
					html += `<tr>
					<th width="20%">`+formatKey(key)+`</th>
					<td>`+data+`</td>
					</tr>`;
				}
			});

			html += `</tbody>`;
			$('#viewassessmentModal .modal-body #viewAssessmentTable').html(html);
		}

		String.prototype.toUpperCaseWords = function () {
			return this.replace(/\w+/g, function(a){
				return a.charAt(0).toUpperCase() + a.slice(1).toLowerCase()
			})
		}

		Array.prototype.contains = function(obj) {
			var i = this.length;
			while (i--) {
				if (this[i] === obj) {
					return true;
				}
			}
			return false;
		}

		$(function(){
			$('.content').on('click', '.btnSelectProvider', function(){
				var id = $(this).attr('data-visit-id');

				var provider = $('input[name=choosen_provider]:checked').val();
				var name = $('input[name=choosen_provider]:checked').data('name');
				var location = $('input[name=choosen_provider]:checked').data('location');
				var phone = $('input[name=choosen_provider]:checked').data('phone');
				var logo = $('input[name=choosen_provider]:checked').data('logo');

				if(typeof provider != 'undefined' && provider != ''){
					$('#providerSearchModal').modal('hide');
					$('#editCaseModal #provider_id').val(provider);
					$('#editCaseModal #selectedProviderName').html('<div class="col-sm-3"><img src="{{ asset('img/provider') }}/'+logo+'" class="img-responsive" style="width: 100px"></div><div class="col-sm-8"><b>'+name+'</b><br>'+location+'<br>Phone: '+phone+'</div>');
				}else{
					alert("Please select provider !");
				}
			});

			$('.content').on('click','.toggle-row-view', function(){
				var id = $(this).data('row');
				// console.log(id);
				$('.providerSearchResults tr#'+id).toggle();
			});

			$('.content').on('click','.btnAddWorklog', function(){
				$('#addWorklogModal table tr td > input').val('');
				$('#addWorklogModal #worklog_date').selectpicker('val','');
				$('#addWorklogModal .loading-img').hide();
				$('#addWorklogModal .worklog-block').hide();
				$('#addWorklogModal .tasks-block').html('');
				$('#addWorklogModal .tasks-block').removeClass('multi-col-worklog');
				$('#addWorklogModal .tasks-block').html('<center>No task(s) assigned.</center>');
				$('#addWorklogModal .btnSaveWorklog').prop('disabled',true);

				$('#addWorklogModal').modal();
			});

			$('.content').on('change','.worklog-task', function(){
				id = $(this).data('id');
				$('#addWorklogModal .tasks-block #worklog_task_'+ id +'_time').prop('disabled', !$(this).is(':checked'));
				$('#addWorklogModal .tasks-block #worklog_task_'+ id +'_time').val('');
				if($(this).is(':checked') && $(this).data('date') != ''){
					$('#addWorklogModal .tasks-block #worklog_task_'+ id +'_time').val($(this).data('date'));
				}
			});

			$('.content').on('click','.btnGetWorklog', function(){
				var worklogDate = $('#addWorklogModal #worklog_date option:selected').val();
				if(worklogDate == ''){
					alert("Please select date!");
					return false;
				}
				$('#addWorklogModal .loading-img').show();
				$('#addWorklogModal .worklog-block').hide();

				$.ajax({
					url: '{{ route('ajax.get-worklog-by-date') }}',
					type: 'POST',
					data: {_token: '{{ csrf_token() }}', lead_id: $('#lead_id').val(), worklog_date: worklogDate},
					success: function (d){
						//console.log(d);
						var tasksHtml = ``;
						$('#addWorklogModal table tr td > input').val('');
						if(d.result){
							if(d.caregiver_id && d.caregiver_name){
								$('#addWorklogModal #worklog_caregiver_id').val(d.caregiver_id);
								$('#addWorklogModal #worklog_caregiver').html(d.caregiver_name);
							}
							if(d.schedule_id){
								$('#addWorklogModal #worklog_schedule_id').val(d.schedule_id);
							}

							// Get assigned tasks
							if(d.tasks){
								var tasksJson = $.parseJSON(d.tasks);
								if(tasksJson.length){
									$(tasksJson).each(function(i){
										tasksHtml += `<input type="checkbox" id="worklog_task_`+ tasksJson[i].id +`" name="worklog_tasks[]" data-id="`+ tasksJson[i].id +`" data-task="`+ tasksJson[i].task +`" class="filled-in chk-col-indigo worklog-task" />
										<label for="worklog_task_`+ tasksJson[i].id +`" title="`+ tasksJson[i].task +`" style="margin-right: 2px">`+ tasksJson[i].task +`</label>
										<input type="text" style="width:35%; display:inline-block !important" class="form-control input-sm worklog-time" id="worklog_task_`+ tasksJson[i].id +`_time" placeholder="Time" /><br>`;
									});
									$('#addWorklogModal .tasks-block').html(tasksHtml);
									$('.worklog-time').datetimepicker({
										format: 'hh:mm A',
										showClose: true,
										keepOpen: false
									});
								}
							}

							// Get worklog data, if any
							if(d.worklog){
								var vitalsJson = $.parseJSON(d.worklog.vitals);
								// console.log(vitalsJson);
								if(vitalsJson.length > 0){
									vitalsJson = vitalsJson[0];
									var keys = Object.keys(vitalsJson);
									//  console.log(keys);
									if(keys.length){
										$(keys).each(function(i){
											if(vitalsJson[keys[i]]){
												var vitals = Object.keys(vitalsJson[keys[i]]);
												// console.log(vitals);
												if(vitals.length){
													$(vitals).each(function(j){

														if($('#addWorklogModal #worklog_'+vitals[j]+'_'+keys[i]).length){
															$('#addWorklogModal #worklog_'+vitals[j]+'_'+keys[i]).val(vitalsJson[keys[i]][vitals[j]]);
														}
													});
												}
											}
										});
									}
								}

								var routinesJson = $.parseJSON(d.worklog.routines);
								if(routinesJson.length > 0 && $('#addWorklogModal .tasks-block .worklog-task').length > 0){
									$('#addWorklogModal .tasks-block').addClass('multi-col-worklog');
									$(routinesJson).each(function(i){
										var keys = Object.keys(routinesJson[i]);
										if(keys.length){
											$(keys).each(function(j){
												var chk = '#addWorklogModal .tasks-block input.worklog-task[data-task="'+ keys[j] +'"]';
												if($(chk).length > 0){
													$(chk).prop('checked', true);
													$('#addWorklogModal .tasks-block #worklog_task_'+ $(chk).data('id') +'_time').val(routinesJson[i][keys[j]]);
													$(chk).data('date', routinesJson[i][keys[j]]);
												}
											});
										}
									});
								} else {
									$('#addWorklogModal .tasks-block').removeClass('multi-col-worklog');
									$('#addWorklogModal .tasks-block').html('<center>No task(s) assigned.</center>');
								}
							}
						} else {
							$('#addWorklogModal .tasks-block').html(tasksHtml);
							$('#addWorklogModal .tasks-block').removeClass('multi-col-worklog');
							$('#addWorklogModal .tasks-block').html('<center>No task(s) assigned.</center>');
						}

						$('#addWorklogModal .loading-img').hide();
						$('#addWorklogModal .worklog-block').show();
						$('#addWorklogModal .btnSaveWorklog').prop('disabled',false);
					},
					error: function (err){
						console.log(err);
					}
				});
			});

			$('.content').on('click','.btnSaveWorklog', function(){
				var vitalsJson = "";
				var routinesJson = "";
				var caregiverId = null;

				// Get Vitals information
				var sessions = ["morning","afternoon","evening","night"];
				var vitals = ["blood_pressure","sugar_level","temperature","pulse_rate"];
				var vitalsArr = {};
				var vitalsArray = [];

				$(sessions).each(function(i){
					var temp = {};
					vitalsArr[sessions[i]] = "";
					$(vitals).each(function(j){
						var block = '#addWorklogModal #worklog_'+vitals[j]+'_'+sessions[i];
						if(typeof $(block).val() != 'undefined' && $(block).val() != ''){
							temp[vitals[j]] = $(block).val();
						}
					});
					if(Object.keys(temp).length)
					vitalsArr[sessions[i]] = temp;
				});
				if(Object.keys(vitalsArr).length)
				vitalsArray.push(vitalsArr);

				// Get Tasks information
				var tasksArray = {};
				var routinesArray = [];
				$('#addWorklogModal .worklog-task').each(function(i){
					if($(this).is(':checked')){
						id = $(this).data('id');
						task = $(this).data('task');
						time = $('#addWorklogModal #worklog_task_'+id+'_time').val();
						tasksArray[task] = time;
					}
				});
				if(Object.keys(tasksArray).length)
				routinesArray.push(tasksArray);

				emptyInputs = $("#addWorklogModal .table tr td input").filter(function () {
					return $.trim($(this).val()).length == 0
				}).length;

				if(Object.keys(vitalsArray).length && emptyInputs < $('#addWorklogModal .table tr td input').length){
					vitalsJson = JSON.stringify(vitalsArray);
				} else{
					alert("Please enter atleast one vital entry!");
					return false;
				}

				if(Object.keys(routinesArray).length) routinesJson = JSON.stringify(routinesArray);

				leadId = $('#lead_id').val();
				scheduleId = $('#addWorklogModal #worklog_schedule_id').val();
				caregiverId = $('#addWorklogModal #worklog_caregiver_id').val();
				worklogDate = $('#addWorklogModal #worklog_date option:selected').val();

				if(confirm("Are you sure ?")) {
					$('#addWorklogModal input').prop('disabled', true);
					$('#addWorklogModal .btnGetWorklog').hide();
					$('#addWorklogModal .btnSaveWorklog').hide();
					$.ajax({
						url: '{{ route('lead.save-worklog-by-date') }}',
						type: 'POST',
						data: {'lead_id': leadId, 'schedule_id': scheduleId, 'caregiver_id': caregiverId, 'worklog_date': worklogDate, 'vitals': vitalsJson, 'routines': routinesJson},
						success: function(d) {
							// console.log(d);
							location.reload();
						},
						error: function (err){
							console.log(err);
							$('#addWorklogModal .btnSaveWorklog').show();
							alert("Unable to save worklog!");
							return;
						}
					});
				}
			});
		});

		//CODING FOR WORKLOG STARTS
		function viewWorklog(element){
			var vitals = $(element).data("vitals");
			var routines = $(element).data("routines");

			var vitalsData =  $.parseJSON(JSON.stringify(vitals));
			var routinesData =  $.parseJSON(JSON.stringify(routines));
			var html = `<tbody>`;

			if(vitalsData[0]){
				html += `<tr><th colspan="5"><center>Vitals Information</center></th></tr>`;

				html += `<tr>
				<th style="text-align:center;">Vitals</th>
				<th style="text-align:center;">Morning</th>
				<th style="text-align:center;">Noon</th>
				<th style="text-align:center;">Evening</th>
				<th style="text-align:center;">Night</th>
				</tr>`;

				// Fetch Blood Pressure Information
				html += `<tr>
				<th style="vertical-align: middle;" >Blood Pressure</th>`;
				if(vitalsData[0]['morning'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['morning'], function(k, v){
						if( typeof(v['blood_pressure']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['blood_pressure']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				if(vitalsData[0]['afternoon'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['afternoon'], function(k, v){
						if( typeof(v['blood_pressure']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['blood_pressure']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				if(vitalsData[0]['evening'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['evening'], function(k, v){
						if( typeof(v['blood_pressure']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['blood_pressure']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				if(vitalsData[0]['night'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['night'], function(k, v){
						if( typeof(v['blood_pressure']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['blood_pressure']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				html += `</tr>`;

				// Fetch Sugar Level Information
				html += `<tr>
				<th style="vertical-align: middle;" >Sugar Level</th>`;
				if(vitalsData[0]['morning'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['morning'], function(k, v){
						if( typeof(v['sugar_level']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['sugar_level']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				if(vitalsData[0]['afternoon'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['afternoon'], function(k, v){
						if( typeof(v['sugar_level']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['sugar_level']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				if(vitalsData[0]['evening'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['evening'], function(k, v){
						if( typeof(v['sugar_level']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['sugar_level']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				if(vitalsData[0]['night'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['night'], function(k, v){
						if( typeof(v['sugar_level']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['sugar_level']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				html += `</tr>`;

				// Fetch Temperature Information
				html += `<tr>
				<th style="vertical-align: middle;" >Temperature</th>`;
				if(vitalsData[0]['morning'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['morning'], function(k, v){
						if( typeof(v['temperature']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['temperature']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				if(vitalsData[0]['afternoon'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['afternoon'], function(k, v){
						if( typeof(v['temperature']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['temperature']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				if(vitalsData[0]['evening'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['evening'], function(k, v){
						if( typeof(v['temperature']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['temperature']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				if(vitalsData[0]['night'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['night'], function(k, v){
						if( typeof(v['temperature']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['temperature']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				html += `</tr>`;

				// Fetch Pulse Rate Information
				html += `<tr>
				<th style="vertical-align: middle;" >Pulse Rate</th>`;
				if(vitalsData[0]['morning'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['morning'], function(k, v){
						if( typeof(v['pulse_rate']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['pulse_rate']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				if(vitalsData[0]['afternoon'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['afternoon'], function(k, v){
						if( typeof(v['pulse_rate']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['pulse_rate']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				if(vitalsData[0]['evening'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['evening'], function(k, v){
						if( typeof(v['pulse_rate']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['pulse_rate']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				if(vitalsData[0]['night'] != null){
					html += `<td><table style="width: -webkit-fill-available;">`;
					$.each(vitalsData[0]['night'], function(k, v){
						if( typeof(v['pulse_rate']) != "undefined")
						html += `<tr><td style="border: none;height: 20px;">`+printValue(v['pulse_rate']+' - '+v['captured_time'])+`</td></tr>`;
					});
					html += `</table></td>`;
				}
				else
				html += `<td>-</td>`;
				html += `</tr>`;
			}
			else if(vitalsData[0] == null || vitalsData[0] == undefined){
				html += `<tr> <th style="text-align:center;"> NO VITALS ADDED </th> </tr>`;
			}

			html += `</tbody>`;
			$('#viewWorklogModal .modal-body #viewWorklogTableVitals').html(html);

			var htmlRoutines = `<tbody>`;

			if(routinesData.length){
				htmlRoutines += `<tr><th colspan="100%" style="text-align:center;">Tasks</th></tr>`;
				htmlRoutines += `<tr>`;
				if(routinesData[0] != null){
					$.each(routinesData[0], function(k, v) {
							htmlRoutines += `<tr><td>`+printValue('<b>'+k+ '</b>')+`</td><td>`+printValue('<b>'+v+'</b>')+`</td></tr>`;
					});
					htmlRoutines += `</tr>`;
				}else{
					htmlRoutines += `<td>-</td>`;
					htmlRoutines += `</tr>`;
				}
			}else{
				htmlRoutines += `<tr> <th style="text-align:center;"> NO ROUTINES ADDED </th> </tr>`;
			}

			htmlRoutines += `</tbody>`;
			$('#viewWorklogModal .modal-body #viewWorklogTableRoutines').html(htmlRoutines);
		}
		//CODING FOR WORKLOG ENDS
	</script>

	<script type="text/javascript">
		$(function(){
			initBlock();
			/*  New Code Block Starts */
			$('.content').on('click','.btnCloseServiceRequestBlock, .btnResetServiceRequest', function(){
				$('#service-request-block').toggle();
				$('#serviceRequestForm').trigger("reset");
				$('#serviceRequestForm select').selectpicker('val','');
				$('#serviceRequestForm code span').html('');
				$('#serviceRequestForm #gross_rate').val('');
				$('#serviceRequestForm #service_request_amount').html('');
				$('.date').data('DateTimePicker').clear();
				$('#serviceRequestForm #frequency').selectpicker('val','Daily');
				$('html, body').stop().animate({
					'scrollTop': $('#service-request-block').offset().top-20
				}, 700, 'swing');
			});

			$('.content').on('click','.btnAddSchedule, .btnCloseScheduleBlock, .btnResetServiceSchedule', function(){
				$('#service-schedule-block').toggle();
				$('html, body').stop().animate({
					'scrollTop': $('#service-schedule-block').offset().top-20
				}, 700, 'swing', function () {
					//window.location.hash = $('#service-schedule-block');
				});
			});

			$('.content').on('click','.btnSelectTasks', function(e){
				e.preventDefault();
				var assignedTasks = [];
				var assignedTasksString = [];
				$('#assignTasksModal input[type=checkbox]:checked').each(function(i){
					assignedTasks.push({id: $(this).data('id'), task: $(this).data('task')});
					assignedTasksString.push($(this).data('task'));
				});
				if(assignedTasks.length > 0){
					$('#serviceRequestForm input[name="assigned_tasks"]').val(JSON.stringify(assignedTasks).toString());
					$('#serviceRequestForm #selectedTasks').html(assignedTasksString.join(", ").toString());
					$('#assignTasksModal').modal('hide');
				}else{
					alert("Please select atleast 1 task!");
					return;
				}
			});

			$('.content').on('click','.btnViewSchedules', function(){
				$('.loading-img').show();
				var data = $(this).data();
				var html = ``;
				if($.fn.DataTable.isDataTable('#viewSchedulesModal .table-schedules')){
					$('#DataTables_Table_0').dataTable().fnDestroy();
				}
				$('#viewSchedulesModal .table-schedules tbody').html(html);
				$('#createSchedulesForm .addScheduleBlock').hide();
				$('#viewSchedulesModal .modal-title').html('View Schedules - '+data.serviceName);
				$('#viewSchedulesModal .modal-body input[name=service_request_id]').val(data.serviceRequestId);
				$('#viewSchedulesModal .modal-body input[name=service_required]').val(data.serviceRequired);
				$('#viewSchedulesModal .modal-body input[name=service_rate]').val(data.serviceRate);

				if(data.status == 'Cancelled'){
					$('#createSchedulesForm .btnAddScheduleFromService').hide();
				}else{
					$('#createSchedulesForm .btnAddScheduleFromService').show();
				}

				if(data.serviceRequestId && data.leadId){
					setTimeout(function(){
						$.ajax({
							url: '{{ route('lead.get-schedules-from-service-request') }}',
							type: 'POST',
							data: {data,_token: '{{ csrf_token() }}'},
							success: function(d){
								if(d[1] == 'Custom'){
									$('.btnAddScheduleFromService').hide();
									$('#extensionWarning').css("display","block");
								}else{
									$('.btnAddScheduleFromService').show();
									$('#extensionWarning').css("display","none");
								}
								if(d[0].length){
									scheduledArr = [];
								    $(d[0]).each(function(i){
								    		scheduledArr.push(d[0][i]['schedule_date']);
								    });

									html = ``;
									$(d[0]).each(function(i){
										statusHtml = ``;
										assignTasksHtml = ``;
										settingsHtml = ``;
										if(d[0][i].status != 'Cancelled' && d[0][i].status != 'Completed'){
											$('#viewSchedulesModal .table-schedules thead th:eq(6)').css('width','15%');
											statusHtml = `
											<div class="btn-group chargeability" style="position: relative; display: block !important; float: left;">
											<button type="button" class="btn btn-info waves-effect current-schedule-status" data-id="`+d[0][i].id+`">`+d[0][i].status+`</button>
											<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
											<span class="caret caret-right"></span>
											<span class="sr-only">Toggle Dropdown</span>
											</button>`;

											if(d[0][i].changeability == 'Y'){
												statusHtml +=
												`<ul class="dropdown-menu" style="position: absolute;z-index: 99999;left:110%">
												<li><a href="javascript:void(0);" data-status="Completed" data-lead="`+d[0][i].lead_id+`" data-id="`+d[0][i].id+`" class="schedule-status waves-effect waves-block">Completed</a></li>
												</ul>`;
											}

											statusHtml += `</div>`;

											assignTasksHtml += `&nbsp;&nbsp;&nbsp;&nbsp;<a class="btn btn-primary btnAssignTasks" data-service-id="`+ d[0][i].service_request_id +`" data-id="`+ d[0][i].id +`" data-schedule-date="`+ d[0][i].schedule_date +`" data-assigned-tasks='`+ JSON.stringify(d[0][i].assigned_tasks).replace(/'/g, "\\'") +`'>Assign Tasks</a>`;
										}else{
											statusHtml += d[0][i].status;
										}

										chargeable = d[0][i].chargeable == 1?'Chargeable':'Non-Chargeable';
										if(d[0][i].chargeable == 1){											
											var isAccessible = false;
											@ability('admin','chargeable-after-confirm-schedule')
											isAccessible = true;
											@endability

											if(d[0][i].status != 'Cancelled' && d[0][i].status != 'Completed' || isAccessible){
										settingsHtml = `&nbsp;&nbsp;&nbsp;
										<div class="btn-group chargeability" style="position: relative; display: block !important; float: left;">
											<button type="button" class="btn bg-indigo waves-effect current-schedule-chargeable" data-id="`+d[0][i].id+`" style="width:115px" title="Schedule Chargeable Setting">`+ chargeable +`</button>
											<button type="button" class="btn bg-indigo dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
												<span class="caret caret-right"></span>
												<span class="sr-only">Toggle Dropdown</span>
											</button>
											<ul class="dropdown-menu" style="position: absolute;z-index: 99999;left:110%">
												<li style="background:#f9f5e9;border-bottom:1px solid #ccc"><a href="javascript:void(0);" data-value="1" data-lead="`+d[0][i].lead_id+`" data-id="`+d[0][i].id+`" class="schedule-chargeable waves-effect waves-block">Yes</a></li>
												<li style="background:#f9f5e9"><a href="javascript:void(0);" data-value="0" data-lead="`+d[0][i].lead_id+`" data-id="`+d[0][i].id+`" class="schedule-chargeable waves-effect waves-block">No</a></li>
											</ul>
										</div>`;
											}else{settingsHtml = chargeable;}
										}else{
											settingsHtml = `Non Chargeable`;
										}

										html += `
										<tr data-id=`+ d[0][i].id +`>
										<td class="text-center schedule-sel">
										<input id="schedule_`+ d[0][i].id +`" type="checkbox" class="with-gap chk-col-blue filled-in schedule-sel-chk" data-date="`+d[0][i].date+`" data-status="`+d[0][i].status+`" data-delivery-address="`+d[0][i].delivery_address_id+`" value="`+d[0][i].id+`"/><label for="schedule_`+ d[0][i].id +`">&nbsp;</label>
										</td>
										<td>`+ (i+1) +`</td>
										<td style="min-width:50px !important;">`+ d[0][i].schedule_date +`</td>
										<td style="min-width:50px !important;">`+ d[0][i].schedule_no +`</td>
										<td style="min-width:50px !important;">`+ d[0][i].timings+`</td>
										<td>`+ d[0][i].employee_id +`</td>
										<td>`+ d[0][i].employee_name +`</td>
										<td>`+ d[0][i].employee_phone +`</td>
										<td style="text-align:center;" title="`+ d[0][i].delivery_location +`"><img src="{{ asset('img/icons/location.png') }}" style="height: 30px;width: 30px;"></td>`;

										html += `<td>`+ statusHtml +`</td>
										<td style="text-align:right;">
										`+ assignTasksHtml +`
										`+ settingsHtml +`</td><td>`+ d[0][i].amount +`</td>
										</tr>`;
									});
								}
								else{
									html = `<tr><td colspan="11" class="text-center">No schedule(s) found.</td></tr>`;
									$('#viewSchedulesModal .table-schedules thead th:eq(6)').css('width','auto');
								}

								$('label[for=from_rate_card]').html('RateCard Price - <b style="color:navy">'+d[2]['ratecardAmount']+' /-</b>');
								$('#from_rate_card').val(d[2]['ratecardAmount']);

								if(d[0].length != 0){
									$('label[for=from_prev_schedule]').html('Last Schedule Price - <b style="color:green">'+d[2]['lastScheduleAmount']+' /-</b>');
									$('#from_prev_schedule').val(d[2]['lastScheduleAmount']);
									$('label[for="from_prev_schedule"]').show();
								}else{
									$('label[for="from_prev_schedule"]').hide();
								}

								if($.fn.DataTable.isDataTable('#viewSchedulesModal .table-schedules')){
									$('#DataTables_Table_0').dataTable().fnDestroy();
								}

								$('#viewSchedulesModal .table-schedules tbody').html(html);

								if(d[0].length){
									$('#viewSchedulesModal .table-schedules').dataTable({
										"dom": '<"top"lpf>t<"bottom"><"clear">',
										"scrollY": '50vh',
										"scrollCollapse": true,
										"paging": false,
										"columnDefs": [
											{ "width"    : "auto" , "targets": 8},
											{ "orderable": false, "targets": 0 }
										 ]
									});
								}
								$('.loading-img').hide();
							},
							error: function(d, err){
								console.log(err);
								$('.loading-img').hide();
							}
						});
					},1);
				}
				else {
					$('#viewSchedulesModal .table-schedules tbody').html(html);
				}

				$('#viewSchedulesModal').modal();
			});

			$('.content').on('click','.btnScheduleAction', function(e){
				e.preventDefault();

				if($(this).data('mode') == 'Schedule'){
					if($('#createSchedulesForm .addScheduleBlock input[name=create_schedule_period_from]').val() == '' || $('#createSchedulesForm .addScheduleBlock input[name=create_schedule_period_to]').val() == ''){
						alert("Please select period!");
						return false;
					}
				}
				if($('#create_schedule_start_time').val() == '' || $('#create_schedule_start_time').val() == ''){
					alert("Please select time interval!");
					return false;
				}

				var emplat = $('input[name=choosen_emp]:checked').data('emplat');
				var emplng = $('input[name=choosen_emp]:checked').data('emplng');
				var patlat = $('input[name=choosen_emp]:checked').data('patlat');
				var patlng = $('input[name=choosen_emp]:checked').data('patlng');

				if(emplat!=0 && emplng!=0 && patlat!=0 && patlng!=0) {
					if(typeof(emplat)!='undefined' && typeof(emplng)!='undefined' && typeof(patlat)!='undefined' && typeof(patlng)!='undefined') {
						emplat = emplat.toString().split(" ").join("");
						emplng = emplng.toString().split(" ").join("");
						patlat = patlat.toString().split(" ").join("");
						patlng = patlng.toString().split(" ").join("");
					getMapMyIndia('https://apis.mapmyindia.com/advancedmaps/v1/89oegcgbhdv1il54kcry8hrlfwet71kf/distance_matrix/driving/'+emplng+','+emplat+';'+patlng+','+patlat+'');
					} else { 
						$(".road_distance").val(''); 
					}
				} else {
					$(".road_distance").val('');
				}

				$(this).attr('disabled', true);
				$('#createSchedulesForm').submit();
			});

			$('#viewSchedulesModal').on('hidden.bs.modal', function (e) {
				$(this).find("input.daterange").val('');
				$(this).find("input[name=create_schedule_period_from]").val('');
				$(this).find("input[name=create_schedule_period_to]").val('');
				$(this).find("input[name=create_schedule_start_time]").val('');
				$(this).find("input[name=create_schedule_end_time]").val('');
				$(this).find("input[name=caregiver_id]").val('');
				$(this).find("div#selectedStaffName").html('');

				initDateRangePicker();
			});

			$('.content').on('click','.btnAddScheduleFromService', function(e){
				initDateRangePicker();
				$(".daterange").val(moment().format('DD-MM-YYYY') + ' - ' + moment().format('DD-MM-YYYY'));
				$('input[name=create_schedule_period_from]').val(moment().format('YYYY-MM-DD'));
				$('input[name=create_schedule_period_to]').val(moment().format('YYYY-MM-DD'));
				$('.addScheduleBlock').toggle();
			});

			$('.content').on('click','.btnReplaceStaff', function(e){
				e.preventDefault();
			});

			$('.content').on('click','.btnAssignTasks', function(e){
				var _data = $(this).data();
				assignedTasks = JSON.parse(_data.assignedTasks);
				tasksJson = [];
				if(assignedTasks != null && assignedTasks.length > 0){
					tasksJson = JSON.parse(assignedTasks);
				}
				$('#scheduleTasksModal input[type=checkbox]').each(function(i){
					$(this).prop('checked',false);
				});
				$('#scheduleTasksModal input[type=checkbox]').each(function(i){
					if(checkIfTaskExist(tasksJson, $(this).data('id'))){
						$(this).prop('checked',true);
					}
				});
				$('#scheduleTasksModal #scheduleTasksLabel').html('Schedule Tasks - '+_data.scheduleDate);
				$('#scheduleTasksModal #scheduleTasksForm input[name=service_id]').val(_data.serviceId);
				$('#scheduleTasksModal input[name=task_date]').val(_data.scheduleDate);
				$('#scheduleTasksModal #task_date').val(_data.scheduleDate);
				$('#scheduleTasksModal #task_to_date').val('');
				$('#scheduleTasksModal').modal();
			});

			$('.content').on('click','.btnUpdateScheduleTasks', function(e){
				e.preventDefault();
				var assignedTasks = [];
				$('#scheduleTasksModal input[type=checkbox]:checked').each(function(i){
					assignedTasks.push({id: $(this).data('id'), task: $(this).data('task')});
				});
				if(assignedTasks.length > 0){
					$('#scheduleTasksModal input[name="assigned_tasks"]').val(JSON.stringify(assignedTasks).toString());
					$('#scheduleTasksModal #scheduleTasksForm').submit();
				}else{
					alert("Please select atleast 1 task!");
					return;
				}
			});

			function checkIfTaskExist(taskJson, id){
				var hasMatch =false;
				for (var index = 0; index < taskJson.length; ++index) {
					var task = taskJson[index];
					if(task.id == id){
						hasMatch = true;
						break;
					}
				}
				return hasMatch;
			}

			function formatDate(date){
				const [day, month, year] = date.split("-")
				return new Date(year, month - 1, day)
			}

			$('.content').on('click','.cancel-service',function(e){
				$('#cancellationModal #cancellation_id').val($(this).data('id'));
				$('#cancellationModal #cancellation_lead_id').val($(this).data('lead'));
				$('#cancellationModal #cancellation_status').val($(this).data('status'));
				$('#cancellationModal').modal();
			});

			$('.content').on('click','.service-status',function(){
				if(confirm("Are your sure ?")){
					var id = $(this).data('id');
					var leadID = $(this).data('lead');
					var status = $(this).data('status');

					// Make AJAX Call
					$.ajax({
						url: '{{ route('lead.update-service-status') }}',
						type: 'POST',
						data: {id: id, lead_id: leadID, status: status},
						success: function (data){

							if(data.result){
								// Set the current status to button
								if(status == 'Cancelled'){
									if(data.schedulesDiff == 0){
										$('.table-services tbody tr[data-id="'+id+'"] td:eq(10)').text(status);
									}
									$('.table-services tbody tr[data-id="'+id+'"] ').addClass('light-red');
									$('.table-services tbody tr[data-id="'+id+'"] td:eq(10)').addClass('col-red');
								}

								if(status == 'Complete Service'){
									if(data.schedulesDiff == 0){
										$('.table-services tbody tr[data-id="'+id+'"] td:eq(9)').text('Completed');
									}
									$('.table-services tbody tr[data-id="'+id+'"] ').addClass('light-blue');
									$('.table-services tbody tr[data-id="'+id+'"] td:eq(10)').addClass('col-blue');
								}

								if(status == 'Pending' || status == 'No Supply'){
									setTimeout(function(){
										window.location.reload();
									},1000);
								}

								showNotification('bg-green', 'Status changed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
							}else{
								showNotification('bg-red', 'Unable to change status! Try again later', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
							}
						},
						error: function (error){
							console.log(error);
						}
					});
				}

				return true;
			});

			$('.content').on('click','.assessment-status',function(){
				if(confirm("Are your sure ?")){
					var id = $(this).data('id');
					var leadID = $(this).data('lead');
					var status = $(this).data('status');

					// Make AJAX Call
					$.ajax({
						url: '{{ route('lead.update-assessment-status') }}',
						type: 'POST',
						data: {id: id, lead_id: leadID, status: status},
						success: function (data){

							if(data.result){
								$('.table-assessment tbody tr[data-id="'+id+'"] td .current-assessment-status').html(status);
								showNotification('bg-green', 'Status changed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
							}else{
								showNotification('bg-red', 'Unable to change status! Try again later', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
							}
						},
						error: function (error){
							console.log(error);
						}
					});
				}

				return true;
			});

			$('.content').on('click','.schedule-status',function(){
				if(confirm("Are your sure ?")){
					var id = $(this).data('id');
					var leadID = $(this).data('lead');
					var status = $(this).data('status');

					//Make AJAX Call
					$.ajax({
						url: '{{ route('lead.complete-schedule') }}',
						type: 'POST',
						data: {id: id, lead_id: leadID, status: status},
						success: function (data){
							if(data.result){
								$('#viewSchedulesModal .table-schedules tbody tr[data-id="'+id+'"] td:eq(9)').text(status);
								$('#viewSchedulesModal .table-schedules tbody tr[data-id="'+id+'"] td:eq(0) .schedule-sel-chk').attr('data-status',status);
								$('#viewSchedulesModal .table-schedules tbody tr[data-id="'+id+'"] td:eq(10) .chargeability button').css("display", "none");
								@ability('admin','chargeable-after-confirm-schedule')
								$('#viewSchedulesModal .table-schedules tbody tr[data-id="'+id+'"] td:eq(10) .chargeability button').css("display", "inline");
								@endability
								$('#viewSchedulesModal .table-schedules tbody tr[data-id="'+id+'"] td:eq(10) .btnAssignTasks').addClass('hide');
								showNotification('bg-green', 'Status changed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
							}else{
								showNotification('bg-red', 'Unable to change status! Try again later', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
							}
						},
						error: function (error){
							console.log(error);
						}
					});
				}

				return true;
			});

			$('.content').on('click','.schedule-chargeable',function(){
				if(confirm("Are your sure ?")){
					var id = $(this).data('id');
					var value = $(this).data('value');

					//Make AJAX Call
					$.ajax({
						url: '{{ route('lead.update-schedule-chargeable') }}',
						type: 'POST',
						data: {id: id, value: value},
						success: function (data){
							console.log(data);
							if(data.result){
								chargeable = value==1?'CHARGEABLE':'NON CHARGEABLE';
								if(value == 1)
									$('#viewSchedulesModal .table-schedules tbody tr[data-id="'+id+'"] td:eq(10) .current-schedule-chargeable').text(chargeable);
								else{
									$('#viewSchedulesModal .table-schedules tbody tr[data-id="'+id+'"] td:eq(10) .chargeability').removeClass('btn-group');
									$('#viewSchedulesModal .table-schedules tbody tr[data-id="'+id+'"] td:eq(10) .chargeability').html(chargeable);
								}

								showNotification('bg-green', 'Schedule Chargeable changed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
							}else{
								showNotification('bg-red', 'Unable to change Chargeable value! Try again later', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
							}
						},
						error: function (error){
							console.log(error);
						}
					});
				}

				return true;
			});

			$('.content').on('change','#service_requested_id', function(){
				rate = $('#service_requested_id option:selected').data('rate');
				if(parseFloat(rate) > 0){
					$('input[name=discount_applicable]').prop('disabled',false);
					$('#serviceRequestForm #rate_lbl').html('&#8377; '+rate);
				}else if(parseFloat(rate) == 0){
					$('input[name=discount_applicable]').prop('disabled',true);
					$('#serviceRequestForm #rate_lbl').html('&#8377; '+rate);
				}else{
					$('input[name=discount_applicable]').prop('disabled',true);
					$('#serviceRequestForm #rate_lbl').html('Not defined!');
				}
				$('#serviceRequestForm #gross_rate').val(rate);
				$('#service_request_from_date').focus();
			});

			$('.content').on('change','input[name=period_type]', function(){
				$('#serviceRequestForm .btnChooseStaff').attr("disabled","disabled");
				if($(this).val() == 'Period'){
					$('#serviceRequestForm #daterange-block').show();
					$('#serviceRequestForm #custom-dates-block').hide();
					$('#mdates').val('');
					$('.mdate').multiDatesPicker('resetDates');
					$('.ui-datepicker-calendar tbody td a.ui-state-active').removeClass('ui-state-active');
					$('#serviceRequestForm input[name=custom_dates]').val('');
				}else{
					$('#serviceRequestForm #daterange-block').hide();
					$('#serviceRequestForm #custom-dates-block').show();
					$('#service_request_from_date').val('');
					$('#service_request_to_date').val('');
				}
			});

			$('.content').on('change','input[name=discount_applicable]', function(){
				if($(this).val() == 'Y'){
					$('input[name=discount_type]').prop('disabled',false);
					$('input[name=discount_value]').prop('disabled',false);
					$('#serviceRequestForm #discount_block').css('display','block');
				}else{
					$('input[name=discount_type]').prop('disabled',true);
					$('input[name=discount_value]').prop('disabled',true);
					$('input[name=discount_value]').val('');
					$('input[name=discounted_amount]').val('');
					$('#serviceRequestForm #discount_block').css('display','none');
				}
			});

			$('.content').on('input','.amtCal', function(){
				calculateTotal();
			});

			$('.content').on('change','input[name=discount_type]', function(){
				calculateTotal();
			});

			$('.content').on('change','#billable_category', function(){
				$('#billable_item').selectpicker('val','');
				$('#billable_item').selectpicker('show');
				$('input[name=item_name]').hide();
				$('input[name=item_name]').val('');
				if($(this).val() == ''){
					$('#billable_item').prop('disabled',true);
					$('#billable_item').prop('required',true);
					$('#billable_rate').val('');
					$('#billable_tax').val('');
					$('#billable_quantity').val(1);
					$('#billable_amount').val('');
				}else if($(this).val() == 'Pharmaceuticals' || $(this).val() == 'Others'){
					$('#billable_item').prop('required',false);
					$('#billable_item').val('0');
					$('#billable_tax').val('');
					$('#billable_rate').prop('disabled',false);
					$('#billable_rate').prop('readonly',false);
					$('#billable_item').selectpicker('hide');
					if($(this).val() == 'Others'){
						$('input[id=medicine_name]').hide();
						$('input[id=other_name]').show();
					}else{
						$('input[id=medicine_name]').show();
						$('input[id=other_name]').hide();
					}
					$('#billable_rate').val('');
					$('#billable_quantity').val(1);
					$('#billable_amount').val('');
				}
				else{
					$('#billable_item').prop('disabled',false);
				}

				$('#billable_item optgroup option').hide();
				$('#billable_item optgroup[label='+$(this).val()+'] option').show();
				$('#billable_item').selectpicker('refresh');
			});

			$('.content').on('change','#billable_item, #billable_rate', function(){
				calculateBillableItem();
			});

			$('.content').on('input','#billable_quantity', function(){
				calculateBillableItem();
			});

			$('.content').on('click','#mdates',function(){
				$('#service_request_from_date').val('');
				$('#service_request_to_date').val('');

				var dates = $('#mdates').val();
				dates = dates.replace(/,\s*$/, "");
				$('#serviceRequestForm input[name=custom_dates]').val(dates);				
				if($('#serviceRequestForm input[name=custom_dates]').val(dates)!=''){
					$('#serviceRequestForm .btnChooseStaff').removeAttr('disabled');
				}
			});

			/*-------------------------------------------
			/*  Handles Case Disposition Update
			/*-------------------------------------------*/
			$('.content').on('click','.lead-status',function(){
				$('#leadStatusModal #lead_status').val($(this).data('status')).change();
				$('#leadStatusModal #lead_disposition_date').val('{{ date('d-m-Y') }}');
				$('#leadStatusModal #comment').val('');
				$('#leadStatusModal').modal();
			});

			$('.content').on('click','.btnChangeLeadStatus',function(){
				$('#leadStatusModal #comment').val('');
				$('#leadStatusModal').modal();
			});

			$('.content').on('click','.updateLeadStatus', function(e){
				e.preventDefault();
				$(this).prop('readonly', true);
				$(this).prop("disabled", true);
				$(this).unbind("click").click(function(e) {
					e.preventDefault();
				});
				var id = $(this).data('lead-id');
				var status = $('#lead_status option:selected').val();

				var caselosscomment = $("input[name='dropped_reason[]']:checked").val();

				var date = $('#lead_disposition_date').val();
				var comment = $('#lead_comment').val();

				var followUpDate = $('#follow_up_date').val();
				var followUpComment = $('#follow_up_comment').val();
				if(status == 'Follow Up' && (followUpDate == '' || followUpComment == '')){
					alert('Choose a Follow Up Date and Comment');
					$(this).prop('readonly', false);
					$(this).prop("disabled", false);
					return false;
				}
				if(date == ''){
					alert('Choose the Disposition Date !');
					$(this).prop('readonly', false);
					$(this).prop("disabled", false);
					return false;
				}
				if(status){
					$.ajax({
						url: '{{ route('lead.update-disposition') }}',
						type: 'POST',
						data: {id: id, status: status, date: date, comment: comment, caselosscomment: caselosscomment, followupdate:followUpDate, followupcomment: followUpComment},
						success: function (data){
							console.log(data);
							if(data){
								$('#leadStatusModal').modal('hide');
								showNotification('bg-light-green', 'Status changed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
								setTimeout(function(){
									window.location.reload();
								},1000);
							}
						},
						error: function (error){
							console.log(error);
							$('.updateLeadStatus').attr('disabled', false);
						}
					});
				}
			});

			$('.content').on('click','.btnRemoveBillableItem',function(){
				id = $(this).data('id');
				if(confirm('Do you really want to remove?')){
					$.ajax({
						url: '{{ route('lead.remove-billable-item') }}',
						type: 'POST',
						data: {id: $(this).data('id')},
						success: function(d){
							if(d){
								$('.billablesTable tbody tr[data-id='+id+']').remove();
								var totals=[0,0.00];
								var $dataColumns = [
									$(".billablesTable tbody tr td:nth-child(8)"),
									$(".billablesTable tbody tr td:nth-child(9)"),
								];
								var $dataRows = $('.billablesTable tbody tr').length-1;
								for(var i=0; i < $dataColumns.length ;i++){
									for (var j = 0; j < $dataRows; j++) {
										totals[i] += parseFloat( $dataColumns[i][j].innerHTML );
										console.log(totals);
									}
								}

								$(".totalCol").each(function(i){
									$(this).text(totals[i]);
								});
								showNotification('bg-orange', 'Item removed', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
							}
						},
						error: function (d, err){
							console.log(err);
						}
					});
				}
			});

			/*  New Code Block Ends */

			$('#editCaseModal #source').selectpicker('val','{{ $l->source }}');

			$('.content').on('click','.btnAddComment',function(){
				$('#commentModal #comment').val('');
				$('#commentModal').modal();
			});

			$('.content').on('click','.btnEditCarePlan',function(){
				//$('#editCaseModal #comment').val('');
				$('#editCaseModal').modal();
			});

			$('.content').on('click','.btnChooseProvider', function(){
				var id = $(this).data('visit-id');
				$('#providerSearchForm #filter_service').selectpicker('val',$('#editCaseModal #service_id').val());
				$('#providerSearchForm #filter_city').val($('#city').val());
				$('.btnFilterProvider').trigger( "click" );
				$('#providerSearchModal .btnSelectProvider').attr('data-visit-id',id);
				$('#providerSearchModal').modal();
			});

			/*-------------------------------------------
			/*  Handles Existing Customer Check
			/*-------------------------------------------*/
			$('.content').on('click', '#checkForPreviousCarePlans', function(){
				checkIfExistingCustomer();
			});

			$('.content').on('click','.btnChooseStaff', function(){
				var block = $(this).attr('data-block');
				var profile = $(this).attr('data-profile');
				var caregiver = $(this).attr('data-caregiver');

				//$('#staffSearchModal .btnSelectStaff').attr('data-visit-id',id);
				$('#staffSearchModal .btnSelectStaff').attr('data-block',block);
				$('#staffSearchModal .btnSelectStaff').attr('data-profile',profile);
				$('#staffSearchModal .btnSelectStaff').attr('data-caregiver',caregiver);

				if(block == '.serviceRequestCaregiverBlock'){
					$('#filter_from_date').val($('#serviceRequestForm #service_request_from_date').val());
					$('#filter_to_date').val($('#serviceRequestForm #service_request_to_date').val());
					$('#datesFromSchedules').val('');
					$('#delivery_address_value').val($('input[name=delivery_address]:checked').val());
				}
				if(block == '.viewScheduleCaregiverBlock'){
					$('#filter_from_date').val($('#createSchedulesForm input[name=create_schedule_period_from]').val());
					$('#filter_to_date').val($('#createSchedulesForm input[name=create_schedule_period_to]').val());
					$('#custom_dates').val('');
					$('#datesFromSchedules').val('');
					$('#delivery_address_value').val($('input[name=delivery_address_schedule]:checked').val());
				}
				if(block == '.replaceScheduleCaregiverBlock'){
					$('#filter_from_date').val('');
					$('#filter_to_date').val('');
					$('#custom_dates').val('');
					$('#delivery_address_value').val($('input[name=delivery_address_assign]:checked').val());
				}

				$('#staffSearchModal').modal();
			});

			// Define a variable for your dataTable object to use
			var reportListDataTable = null;

			$('.content').on('click','.btnFilterStaff', function(){
				var flag = 'uninterrupted';
				var replacement = $('#staffSearchForm #replacement').val();
				var interruptedDates = $('#custom_dates').val();
				var datesFromSchedules = $('#datesFromSchedules').val();

				if(interruptedDates != ''){
					var flag = 'interrupted';
				}

				serviceDeliveryAddress = $('#delivery_address_value').val();

				fDate = $('#filter_from_date').val();
				tDate = $('#filter_to_date').val();
				
				fTime = $('#filter_from_time').val();
				tTime = $('#filter_to_time').val();

				specialization = $('#filter_specialization option:selected').val();
				experience = $('#filter_experience option:selected').val();
				experience_level = $('#filter_experience_level option:selected').val();
				gender_preference = $('#staff_gender_preference option:selected').val();
				city = $('#filter_city').val();

				showOnDutyStaff = $('#include_onduty_staff').is(':checked')?1:0;

				var language = [];
				$('#filter_language :selected').each(function(i, selected){
					language[i] = $(selected).text();
				});

				var skill = [];
				$('#filter_skill :selected').each(function(i, selected){
					skill[i] = $(selected).val();
				});

				$.ajax({
					url: '{{ route('ajax.caregiver.filter') }}',
					type: 'POST',
					data: {_token: '{{ csrf_token() }}', pid: '{{ Helper::encryptor('encrypt',$l->patient_id) }}',fd:fDate, td:tDate, ft:fTime, tt:tTime, s:specialization, e:experience, el:experience_level, gp:gender_preference, c:city, l:language, sk:skill, sods:showOnDutyStaff, flag:flag, dates:interruptedDates, replacement:replacement, datesFromSchedules:datesFromSchedules, sda:serviceDeliveryAddress },
					success: function (data){
						html = '<tr><td class="text-center" colspan="7">No caregiver(s) found. Modify Filters to get results</td></tr>';
						if(data['result'].length){
							html = '';
							rdata = data['result'];
							patientLocation = data['patient_location'];
							$.each(rdata, function(i){
								distanceTxt = '-';
								lat = lng = patLat = patLng = 0;
								trClass = rdata[i].onduty?'light-red':'';
								trTitle = rdata[i].onduty?'Already scheduled for a visit':'';

								var coordinates = rdata[i].coordinates;
								if(coordinates != '' && patientLocation != ''){
									lat = coordinates.split("_")[0];
									lng = coordinates.split("_")[1];

									patLat = patientLocation.split("_")[0];
									patLng = patientLocation.split("_")[1];
									distanceTxt = calculateDistance(lat, lng, patLat, patLng, "K");
								}

								html += `<tr class="`+trClass+`" title="`+trTitle+`">
								<td>`+rdata[i].employee_id+`</td>
								<td>`+rdata[i].full_name+`<br><small>`+rdata[i].location+`</small></td>
								<td>`+rdata[i].specialization+`</td>
								<td>`+rdata[i].experience+`</td>
								<td>`+rdata[i].level+`</td>
								<td>`+distanceTxt+`</td>
								<td class="text-center"><input name="choosen_emp" type="radio" id="choosen_emp_`+rdata[i].id+`" class="with-gap radio-col-blue" data-name="`+rdata[i].full_name+`" data-mobile="`+rdata[i].mobile+`" data-empLat="`+lat+`" data-empLng="`+lng+`" data-patLat="`+patLat+`" data-patLng="`+patLng+`" value="`+rdata[i].id+`" /><label for="choosen_emp_`+rdata[i].id+`"></label></td>
								</tr>`;
							});

							// Destroy the dataTable and empty because the columns may change!
							if (reportListDataTable !== null ) {
								// for this version use fnDestroy() instead of destroy()
								reportListDataTable.fnDestroy();
								reportListDataTable = null;
								// empty in case the columns change
								$('.staffSearchResults tbody').empty();
							}

							$('.staffSearchResults tbody').html(html);

							// Build dataTable with ajax, columns, etc.
							reportListDataTable = $('.staffSearchResults').dataTable({
								"dom": 'frtip',
								"order": [[ 5, 'asc' ]],
								"scrollY": '35vh',
								"scrollCollapse": true,
								"paging": false
							});

							if(localStorage.getItem('helpMode') == 'true'){
								swal({
									title: "Staffs Loaded !",
									text: "We have loaded staff as per the filters. Please use them to refine your search",
									icon: "success",
									closeOnClickOutside: false,
									closeOnEsc: false,
									buttons: {
										ok: {
											text: "Ok! I got it.",
										}
									},
								});
							}
						}else{
							$('.staffSearchResults tbody').html(html);
							if(localStorage.getItem('helpMode') == 'true'){
								swal({
									title: "No Staffs Loaded !",
									text: "We could not find staff as per the filters. Please refine your search",
									icon: "warning",
									closeOnClickOutside: false,
									closeOnEsc: false,
									buttons: {
										ok: {
											text: "Ok! I will refine it.",
										}
									},
								});
							}
						}
					},
					error: function (error){
						console.log(error);
					}
				});
			});

			$('.content').on('click','.btnReset', function(e){
				e.preventDefault();
				if (reportListDataTable !== null ) {
					reportListDataTable.fnDestroy();
					reportListDataTable = null;
					$('.staffSearchResults tbody').empty();
				}
				clearSearchForm();
			});

			$('.content').on('click', '.btnSelectStaff', function(){
				var block = $(this).attr('data-block');
				var profile = $(this).attr('data-profile');
				var caregiver = $(this).attr('data-caregiver');
				var emp = $('input[name=choosen_emp]:checked').val();
				var name = $('input[name=choosen_emp]:checked').data('name');
				var mobile = $('input[name=choosen_emp]:checked').data('mobile');
				var emplat = $('input[name=choosen_emp]:checked').data('emplat');
				var emplng = $('input[name=choosen_emp]:checked').data('emplng');
				var patlat = $('input[name=choosen_emp]:checked').data('patlat');
				var patlng = $('input[name=choosen_emp]:checked').data('patlng');

				if(typeof emp != 'undefined' && emp != ''){
					if(emplat!=0 && emplng!=0 && patlat!=0 && patlng!=0) {
						if(typeof(emplat)!='undefined' && typeof(emplng)!='undefined' && typeof(patlat)!='undefined' && typeof(patlng)!='undefined') {
							emplat = emplat.toString().split(" ").join("");
							emplng = emplng.toString().split(" ").join("");
							patlat = patlat.toString().split(" ").join("");
							patlng = patlng.toString().split(" ").join("");

						getMapMyIndia('https://apis.mapmyindia.com/advancedmaps/v1/89oegcgbhdv1il54kcry8hrlfwet71kf/distance_matrix/driving/'+emplng+','+emplat+';'+patlng+','+patlat+'');
						} else {
							$(".road_distance").val('');
						}
					} else {
						$(".road_distance").val('');
					}

					$('#staffSearchModal').modal('hide');

					$(block + ' ' + caregiver).val(emp);
					$(block + ' ' + profile).html('<div class="col-sm-3"><img src="/img/user.png" width="40" height="40" class="img-circle"></div><div class="col-sm-9">'+name+'<br>Mob: '+mobile+'</div><br>');
					$(block).show();
					$('.btnSaveServiceRequest').prop('disabled',false);
				}else{
					alert("Please select staff !");
				}
			});

			$('.content').on('click','.btnSaveServiceRequest', function(e){
				e.preventDefault();

				if($('#service_requested_id option:selected').val() == ''){
					alert("Please select service!");
					return false;
				}

				if( ($('#service_request_from_date').val() == '' || $('#service_request_to_date').val() == '') && $('#custom_dates').val() == '' ){
					alert("Please select period!");
					return false;
				}

				if( ($('#service_request_start_time').val() == '' || $('#service_request_end_time').val() == '')){
					alert("Please select time interval!");
					return false;
				}

				$(this).attr('disabled',true);
				$('#serviceRequestForm').submit();
			});
		});
		
		$('.content').on('click','.btnAddService', function(){
			if(assessmentFlag != ''){
				assessmentDone = assessmentFlag;
			}else{
				assessmentDone = '{{ $l->assessment_done }}'
			}

			if(assessmentDone == ''){
				swal({
				  title: "Has the assessment been done for this Episode?",
				  text: "Your choice will be saved for this episode !",
				  icon: "warning",
				  closeOnClickOutside: false,
				  closeOnEsc: false,
				  buttons: {
				  	done: {
				  		text: "Yes!",
				  		value: "Done",
				  	},
				  	notdone: {
				  		text: "No",
				  		value: "No",
				  	},
				  	notrequired: {
				  		text: "Not Required!",
				  		value: "Not Required",
				  	},
				  },
				})
				.then((value) => {
					switch (value) {
						case "Done":
							$.ajax({
								url: '{{ route('lead.assessent-done-status') }}',
								type: 'POST',
								data: {value: value, lead: '{{ $l->id}}' },
								success: function (d){
									swal("Great!", "Continue for service order!", "success");
									$('#service-request-block').toggle();
									$('#serviceRequestForm').trigger("reset");
									$('#serviceRequestForm select').selectpicker('val','');
									$('#serviceRequestForm code span').html('');
									$('#serviceRequestForm #gross_rate').val('');
									$('#serviceRequestForm #service_request_amount').html('');
									$('.date').data('DateTimePicker').clear();
									$('#serviceRequestForm #frequency').selectpicker('val','Daily');
									$('html, body').stop().animate({
										'scrollTop': $('#service-request-block').offset().top-20
									}, 700, 'swing');
									assessmentFlag = 'Done';
								},
								error: function (err){
									console.log(err);
								}
							});
							break;

						case "Not Required":
							$.ajax({
								url: '{{ route('lead.assessent-done-status') }}',
								type: 'POST',
								data: {value: value, lead: '{{ $l->id}}' },
								success: function (d){
									swal("Fine!", "We will remember that for you!", "success");
									$('#service-request-block').toggle();
									$('#serviceRequestForm').trigger("reset");
									$('#serviceRequestForm select').selectpicker('val','');
									$('#serviceRequestForm code span').html('');
									$('#serviceRequestForm #gross_rate').val('');
									$('#serviceRequestForm #service_request_amount').html('');
									$('.date').data('DateTimePicker').clear();
									$('#serviceRequestForm #frequency').selectpicker('val','Daily');
									$('html, body').stop().animate({
										'scrollTop': $('#service-request-block').offset().top-20
									}, 700, 'swing');
									assessmentFlag = 'Not Required';
								},
								error: function (err){
									console.log(err);
								}
							});
							break;

						default:
							swal("Please complete the assessment first!");
					}
				});
			}else{
				$('#service-request-block').toggle();
				$('#serviceRequestForm').trigger("reset");
				$('#serviceRequestForm select').selectpicker('val','');
				$('#serviceRequestForm code span').html('');
				$('#serviceRequestForm #gross_rate').val('');
				$('#serviceRequestForm #service_request_amount').html('');
				$('.date').data('DateTimePicker').clear();
				$('#serviceRequestForm #frequency').selectpicker('val','Daily');
				$('html, body').stop().animate({
					'scrollTop': $('#service-request-block').offset().top-20
				}, 700, 'swing');
			}
		});

		function initBlock()
		{
			var holidayList = ["<?php echo ($holidays); ?>"];
			var holidayListRev = ["<?php echo ($holidaysr); ?>"];
			var date = new Date();

			$('.taskMultiAssign').datetimepicker({
				format: 'DD-MM-YYYY',
				disabledDates: holidayListRev,
				showClose: true,
				showClear: true,
				useCurrent: false,
				keepInvalid:true,
			}).on('dp.show', function(e){
				$(this).data('DateTimePicker').minDate($('#task_date').val());
			});

			$('.bdate').datetimepicker({
				format: 'DD-MM-YYYY',
				showClose: true,
				useCurrent: false,
				keepInvalid:true,
				showClear: true,
				maxDate : moment(),
			});

			$('.date').datetimepicker({
				format: 'DD-MM-YYYY',
				disabledDates: holidayListRev,
				showClose: true,
				showClear: true,
				// minDate: date,
				useCurrent: false,
				keepInvalid:true
			}).on('dp.change', function(e){
				if($(e.target).attr('id') == 'service_request_from_date' || $(e.target).attr('id') == 'service_request_to_date'){
					$('.serviceRequestCaregiverBlock').css('display','none');
					$('.serviceRequestCaregiverBlock #caregiver_id').val('');
					calculateTotal();

					if($(e.target).attr('id') == 'service_request_from_date'){
						$('#service_request_to_date').focus();
					}

					if($('#service_request_from_date').val()!='' && $('#service_request_to_date').val()!=''){
						$('#serviceRequestForm .btnChooseStaff').removeAttr('disabled');
					} else {
						$('#serviceRequestForm .btnChooseStaff').attr("disabled","disabled");
					}

					$('#service_request_to_date').on('dp.change',function(){
						if(localStorage.getItem('helpMode') == 'true'){
							swal({
								title: "Staff allocation !",
								text: "Do you want to allocate a staff for this service entry ?",
								icon: "warning",
								closeOnClickOutside: false,
								closeOnEsc: false,
								buttons: {
									yes: {
										text: "Yes!",
										value: "Yes",
									},
									notNow: {
										text: "Not Now",
										value: "Not Now",
									},
								},
							})
							.then((value) => {
								switch (value) {
									case "Yes":
									$("#scheduleBlock .btnChooseStaff").trigger('click');
									$(".btnFilterStaff").trigger('click');
									break;

									case "Not Now":
									swal("Ok!", "", "success")
									break;
								}
							});
						}
					});
				}
			});

			$('.datetime').datetimepicker({
				format: 'DD-MM-YYYY hh:mm A',
				showClose: true,
				keepOpen: false,
				useCurrent: false,
				keepInvalid:true,
				minDate: moment(),
			});

			$('.schedule-from').datetimepicker({
				format: 'DD-MM-YYYY',
				showClear: true,
				minDate: moment().subtract(5,'d')
			});

			$('.content').on('dp.change','.schedule-from',function(e){
				if(e.oldDate != null){
					$('.schedule-to').datetimepicker({
						format: 'DD-MM-YYYY',
						showClear: true,
						useCurrent: false,
						keepInvalid:true,
						minDate: e.date,
						// maxDate: moment(e.date).add(5,'d')
					});
				}
			});

			$('.content').on('dp.change','input[name=start_time], input[name=end_time]', function(){
				var fromTime = $('input[name=start_time]').val();
				var toTime = $('input[name=end_time]').val();

				if(fromTime && toTime){
					startTime = moment(fromTime,'HH:mm');
					endTime = moment(toTime,'HH:mm');

					duration = moment.duration(endTime.diff(startTime));

					$('.timing_duration').show();
					$('.timing_duration').html('Duration: '+ parseFloat(duration.asHours()).toFixed(1) + ' hr(s) ');
				}
			});

			$('.mdate').multiDatesPicker({
				dateFormat: "dd-mm-yy",
				addDisabledDates: holidayList,
				altField: '#mdates',
				onSelect: function(){
					$('#mdates').trigger("click");
					calculateTotal();
				},
			});
		}

		$('.content').on('change','#frequency, #frequency_period, #discount_applicable_yes, #discount_applicable_no',function(){
			calculateTotal();
		});

		function formatCurrency(nStr){
			nStr += '';
			var x = nStr.split('.');
			var x1 = x[0];
			var x2 = x.length > 1 ? '.' + x[1] : '';
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(x1)) {
				x1 = x1.replace(rgx, '$1' + ',' + '$2');
			}
			return x1 + x2;
		}

		function calculateTotal()
		{
			var totalAmount = 0;
			var days = 0;

			var block = '#service-request-block';
			var amount = $(block+' #gross_rate').val();
			amount = amount?parseFloat(amount):0;

			var discount = $(block+' input[name=discount_applicable]:checked').val();
			var dt = $(block+' input[name=discount_type]:checked').val();

			var dv = $(block+' #discount_value').val();
			dv = dv?parseFloat(dv):0;

			actualAmount = amount;
			if(discount == 'Y'){
				if(dt == 'P'){
					actualAmount = parseFloat(actualAmount) - parseFloat(actualAmount) * (dv/100);
					$(block+' #discounted_amount').val((parseFloat(actualAmount) * (dv/100)).toFixed(2));
				}

				if(dt == 'F'){
					actualAmount = parseFloat(actualAmount) - dv;
					$(block+' #discounted_amount').val(parseFloat(dv).toFixed(2));
				}
			}
			totalAmount = actualAmount;

			var fromDate = $(block+' #service_request_from_date').val();
			var toDate = $(block+' #service_request_to_date').val();
			var frequency = $(block+' #frequency').val();
			var frequencyPeriod = $(block+' #frequency_period').val();
			if (fromDate != '' || toDate != ''){
				if(fromDate != ''){
					fromDate = moment(fromDate, "DD-MM-YYYY");
					if(toDate != ''){
						toDate = moment(toDate, "DD-MM-YYYY");
					}else{
						toDate = fromDate;
					}

					days = toDate.diff(fromDate, 'days') + 1;
				}
			}else{
				if($('#mdates').val() == '')
					days = 0;
				else
					days = $('#mdates').val().split(",").length;
			}

			if(frequency == 'Hourly'){
				totalAmount = frequencyPeriod * totalAmount;
			}else{
				totalAmount = days * totalAmount;
			}

			if(totalAmount > 0){
				if(outstanding != 0){
					if(outstanding < 0){
						noOfDays = Math.floor(Math.abs(outstanding) / actualAmount);
					}

					if(outstanding > 0 || totalAmount > outstanding){
						$(block+' .outstanding-msg-block > div').html('Customer has an outstanding balance, Service order cannot be created! <i class="material-icons" title="To override this, modify the setting in configuration">info</i>');
						$(block+' .outstanding-msg-block').show();
						$(block+' .btnSaveServiceRequest').prop('disabled',true);
						$(block+' .btnSaveServiceRequest').hide();
						$(block+' #scheduleBlock').hide();
						$(block+' #scheduleTasksBlock').hide();
					}
				}else{
					$(block+' .outstanding-msg-block').hide();
					$(block+' .outstanding-msg-block > div').html('');
					$(block+' .btnSaveServiceRequest').prop('disabled',false);
					$(block+' .btnSaveServiceRequest').show();
					$(block+' #scheduleBlock').show();
					$(block+' #scheduleTasksBlock').show();
				}
				$(block+' #service_request_amount').html(''+ formatMoney(totalAmount) +' ( Rate: '+formatMoney(parseFloat(actualAmount)) + ')');
				$(block+' #net_rate').hide();
				$(block+' #net_rate').val(parseFloat(actualAmount).toFixed(2));
			}else{
				$(block+' #net_rate').val(0);
			}
		}

		function calculateBillableItem()
		{
			var category = $('#billable_category option:selected').val();
			rateFromList = (category == 'Pharmaceuticals' || category == 'Others')?false:true;
			if(rateFromList)
			var rate = parseFloat($('#billable_item option:selected').data('rate'));
			else
			var rate = parseFloat($('#billable_rate').val());

			var quantity = parseInt($('#billable_quantity').val());
			var tax = parseFloat($('#billable_item option:selected').data('tax'));

			$('#billable_rate').val(rate);
			$('#billable_tax').val(tax);
			$('#billable_amount').val(parseFloat((rate + (rate * (tax/100))) * quantity).toFixed(2));
		}

		function clearSearchForm()
		{
			$('#staffSearchModal #staffSearchForm #filter_specialization').selectpicker('val','0');
			$('#staffSearchModal #staffSearchForm #filter_experience').selectpicker('val','0');
			$('#staffSearchModal #staffSearchForm #filter_experience_level').selectpicker('val','0');
			$('#staffSearchModal #staffSearchForm #filter_language').selectpicker('val','0');
			$('#staffSearchModal #staffSearchForm #filter_skill').selectpicker('val','0');
			$('#staffSearchModal .staffSearchResults tbody').html('<tr><td class="text-center" colspan="8">Click \'Search\' to getresults</td></tr>');
		}

		var visitListArray = [];

		$('.content').on('change','#filter_from_date',function(){
			var value = $(this).val();
			if(!$('.filter_period:eq(0)').is(':visible')){
				$('#filter_to_date').val(value);
			}
		});

		function getServiceInterruptionsList(scheduleID){
			if(scheduleID){
				var leadID = $('#lead_id').val();
				$.ajax({
					url: '{{ route('lead.get-service-interruption') }}',
					type: 'POST',
					data: {lead_id: leadID, schedule_id: scheduleID},
					success: function (data){
						if(data.length){
							html = ``;
							$(data).each(function(i){
								var fdate = new Date(data[i].from_date);
								var tdate = new Date(data[i].to_date);
								var cdate = new Date(data[i].created_at);
								noOfDays = moment.duration(moment(tdate).diff(moment(fdate)));

								html += `<tr>
								<td>`+(i+1)+`</td>
								<td>`+moment(fdate).format('DD-MM-YYYY')+`</td>
								<td>`+moment(tdate).format('DD-MM-YYYY')+`</td>
								<td>`+noOfDays.asDays()+`</td>
								<td>`+data[i].reason+`</td>
								<td>`+moment(cdate).format('DD-MM-YYYY')+`</td>
								<td>`+data[i].user.full_name+`</td>
								</tr>`;

								logHtml = `<ul>`;
								if(data[i].audits.length){
									var audits = data[i].audits;
									$(audits).each(function(j){

									});
									logHtml += `</ul>`;
								}
							});
							$('.serviceInterruptionsTable tbody').html(html);
						}
					},
					error: function (err){
						console.log(err);
					}
				});
			}
		}

		$('.content').on('click','.btnEditServiceRequests', function(){
			data = $(this).data();
			if(data){
				block = '#serviceRequestModal #serviceRequestForm ';
				$(block+' #service_requested_id').selectpicker('val',data.serviceRequested);
				$(block+' #service_requested_id').selectpicker('refresh');
				$(block+' #service_recommended_id').selectpicker('val',data.serviceRecommended);
				$(block+' #service_recommended_id').selectpicker('refresh');
				$(block+' #service_request_from_date').val(data.fromDate);
				$(block+' #service_request_to_date').val(data.toDate);
				$(block+' #frequency_period').val(data.frequencyPeriod);
				$(block+' #frequency').selectpicker('val',data.frequency);
				$(block+' #frequency').selectpicker('refresh');
				$(block+' input[name=id]').val(data.id);

				$('#serviceRequestModal').modal();
			}
		});

		$('.content').on('click','.btnSendToVendor', function(){
			var items = [];
			var vendorID = $('select[name=vendor_id] option:selected').val();
			if(vendorID == 0){
				alert("Please select vendor!");
				return false;
			}

			$('.billable-sel-chk').each(function(i){
				if($(this).is(':checked')){
					items.push($(this).val());
				}
			});

			if(items.length <= 0){
				alert("Please select items!");
				return false;
			}

			if(vendorID > 0 && items.length > 0){
				$(this).attr('disabled', true);
				$.ajax({
					url: '{{ route('lead.send-billables-to-vendor') }}',
					type: 'POST',
					data: {vendor_id: vendorID, items: items},
					success: function (d){
						if(d){
							showNotification('bg-green', 'Email sent successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');

							$('select[name=vendor_id]').val('0').change();
							$('.billable-sel-chk').each(function(i){
								if($(this).is(':checked')){
									$(this).prop('checked', false);
									items = [];
								}
							});
						}
						$(this).attr('disabled', false);
					},
					error: function (err){
						console.log(err);
						$(this).attr('disabled', false);
					}
				})
			}
		});

		function showAlert(type, message, block=''){
			var typeClass = 'alert-info';
			if(type == 'success') typeClass = 'alert-success';
			if(type == 'error') typeClass = 'alert-danger';

			if(block == ''){
				block = '.content > .container-fluid';
			}
			html = `<div class="alert ` + typeClass + ` alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			`+ message +`
			</div>`;

			$(block).prepend(html);
		}

		function calculateDistance(lat1, lon1, lat2, lon2, unit) {
			var radlat1 = Math.PI * lat1/180;
			var radlat2 = Math.PI * lat2/180;
			var theta = lon1-lon2;
			var radtheta = Math.PI * theta/180;
			var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
			dist = Math.acos(dist);
			dist = dist * 180/Math.PI;
			dist = dist * 60 * 1.1515;
			if (unit=="K") { dist = dist * 1.609344; }
			if (unit=="N") { dist = dist * 0.8684; }

			return dist.toFixed(2);
		}

		function calcCrow(lat1, lon1, lat2, lon2)
		{
			var R = 6371; // km
			var dLat = toRad(lat2-lat1);
			var dLon = toRad(lon2-lon1);
			var lat1 = toRad(lat1);
			var lat2 = toRad(lat2);

			var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
			Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
			var d = R * c;

			return d;
		}

		function getMapMyIndia(api_url)
		{
			$(".loader2").html(`<div class="page-loader-wrapper">
                <div class="loader">
                    <div class="md-preloader pl-size-md">
                        <svg viewbox="0 0 75 75">
                            <circle cx="37.5" cy="37.5" r="33.5" class="pl-red" stroke-width="4" />
                        </svg>
                    </div>
                    <p>Please wait...</p>
                </div>
            </div>`);
            
			$.ajax({
                url: '{{ route('lead.get-distance-from-map-my-india') }}',
                type: "POST",
                data: {url: api_url},
                success: function (data) {
                	if(data!=""){
	                    var jsondata = JSON.parse(data);
	                    if (jsondata.responseCode == 200) {
	                    	if (jsondata.results != null) {
	                    		console.log(jsondata.results['distances']);
	                    		var length = (jsondata.results['distances'][0][1] / 1000).toFixed(1) + " km";
	                        	$('.road_distance').val(length);
		                    } else {
		                    	$('.road_distance').val('');
		                    }
	                    } else {
	                    	$('.road_distance').val('');
	                        console.log('Something went wrong in the getMapMyIndia response');
                			console.log(data);
	                    }
	                    $(".loader2").html('');
                	}else{
                		$('.road_distance').val('');
                		console.log('Could not calculate road distance! MapMyIndia api not responding.');
                		$('.loader2').html('');
                	}
                },
                error: function(error) { 
                    console.log('Error: ' + error); 
                    $('.loader2').html('');
                }
            })
		}

		// Converts numeric degrees to radians
		function toRad(Value){
			return Value * Math.PI / 180;
		}

		function formatMoney(value){
			var formatter = new Intl.NumberFormat('en-US', {
			  style: 'currency',
			  currency: 'INR',
			  minimumFractionDigits: 2,
			});

			return formatter.format(value);
		}
	</script>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/2.0.4/css/Jcrop.min.css"></link>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/2.0.4/js/Jcrop.min.js"></script>
	<script type="text/javascript">
		function handleFileSelect(evt) {
			var files = evt.target.files; // FileList object

			// Loop through the FileList and render image files as thumbnails.
			for (var i = 0, f; f = files[i]; i++) {
				// Only process image files.
				if (!f.type.match('image.*')) {
					continue;
				}
				var reader = new FileReader();

				// Closure to capture the file information.
				reader.onload = (function(theFile) {
					return function(e) {
						// Render thumbnail.
						var span = document.createElement('span');
						span.innerHTML = ['<img style="width:160px !important;height: 160px  !important;" id="patient_image" class="img-responsive img-thumbnail" src="', e.target.result,
						'" title="', escape(theFile.name), '"/>'].join('');
						$('.profile-img-block').html(span);
					};
				})(f);

				// Read in the image file as a data URL.
				reader.readAsDataURL(f);
			}
		}

		document.getElementById('pic_upload').addEventListener('change', handleFileSelect, false);
	</script>

	<script type="text/javascript">
		$('.content').on('click','.addDiscount',function(){
			serviceID = $(this).data('service-id')
			$.ajax({
				type: 'POST',
				url: '{{ route('lead.get-service-order-for-discount') }}',
    			dataType: 'json',
				data: {serviceRequestId:serviceID ,_token:'{{csrf_token()}}'},
				success: function (d) {
					if(d){
						html = `<option value=''>Select Service Period</option>`;
						$.each(d, function(i){
							html += `<option data-gross-rate='`+d[i].gross_rate+`' data-schedule-dates='`+d[i].schedule_dates+`' value='`+d[i].service_order_id+`'>`+d[i].service_order_date+`</option>`;
						});

						$("#servicePeriod_"+serviceID).html(html); // Dropdown service order period
						$("#servicePeriod_"+serviceID).selectpicker('refresh'); // Dropdown service order period refreshed
						$(".apply_discount_"+serviceID).attr('disabled','disabled'); // Discount Period calender disabled
						$(".rateCardInput_after_"+serviceID).attr('disabled','disabled'); // Discount Value textbox disabled
						$(".discountSubmit_"+serviceID).attr('disabled','disabled'); // Submit button disabled
					}
				},
				error: function (err){
					console.log('Error: '+err);
				}
			})

			$('.showDiscountRow_'+serviceID).children('.row_'+serviceID).slideToggle();
		});
    	$('.servicePeriod').on('change', function(){
    		serviceID = $(this).data('service-id');
			$(".apply_discount_"+serviceID).attr('disabled',false); // Discount Period calender enabled
			$(".rateCardInput_after_"+serviceID).attr('disabled','disabled'); // Discount Value textbox disabled
			$(".discountSubmit_"+serviceID).attr('disabled','disabled'); // submit button disabled
    		var scheduleDates = $(this).find(':selected').data('schedule-dates');
			if(scheduleDates.length === 0){
				$(".apply_discount_"+serviceID).attr('disabled','disabled'); // Discount Period calender disabled
				$(".apply_discount_"+serviceID).attr('placeholder','No Pending Schedules for Discounting.');
				$(".discountSubmit_"+serviceID).attr('disabled','disabled'); // Submit button disabled
			}else{
				$(".apply_discount_"+serviceID).attr('placeholder','Discount Period');
				calculateTotalAfter(serviceID);
				dates = scheduleDates.split(',');
				$(".apply_discount_"+serviceID).flatpickr({
					enable:dates,
					dateFormat:"d-m-Y",
					mode:"range",
				});
			}
		});

		$('.content').on('change','input[name=apply_discount]', function(){
			calculateTotalAfter($(this).data('service-id'));
			$(".rateCardInput_after_"+serviceID).attr('disabled',false); // Discount Value textbox enabled
		});

		$('.content').on('input','.amtCal_after', function(){
			calculateTotalAfter($(this).data('service-id'));
			$(".discountSubmit_"+serviceID).attr('disabled',false); // Submit button enabled
		});

		$('.content').on('change','input[name=discount_type_after]', function(){
			calculateTotalAfter($(this).data('service-id'));
		});

		function calculateTotalAfter($serviceID)
		{
			var totalAmount = 0;
			var grossRate = $('#servicePeriod_'+$serviceID).find(':selected').data('gross-rate');

			var amount = grossRate;
			amount = amount?parseFloat(amount):0;

			var dt = $('input[name=discount_type_after_'+$serviceID+']:checked').val();

			var dv = $('#discount_value_after_'+$serviceID).val();
			dv = dv?parseFloat(dv):0;

			actualAmount = amount;
			if(dt == 'P'){
				actualAmount = parseFloat(actualAmount) - parseFloat(actualAmount) * (dv/100);
			}

			if(dt == 'F'){
				actualAmount = parseFloat(actualAmount) - dv;
			}



			// var noOfDays = $('.addDiscount[data-service-id='+$serviceID+']').data('no-of-days');
			var noOfDays = $('.apply_discount_'+$serviceID).val().split(',').length;
			totalAmount = actualAmount * noOfDays;

			if(totalAmount > 0){
				$('#service_request_amount_after_'+$serviceID).html(formatMoney(totalAmount));
				$('#net_rate_after_'+$serviceID).hide();
				$('#net_rate_after_'+$serviceID).val(parseFloat(actualAmount).toFixed(2));
			}else{
				$('#net_rate_after_'+$serviceID).val(0);
			}
		}
	</script>
	<script type="text/javascript">
		$('#schedules-sel-all').on('click',function(){
			if(this.checked){
				$('.schedule-sel-chk').each(function(){
					this.checked = true;
					$('.table-schedules tr').css('background-color', '#a5ddd5');
				});
			}else{
				$('.schedule-sel-chk').each(function(){
					this.checked = false;
					$('.table-schedules tr').css('background-color', '#ffffff');
				});
			}
		});

		$('.content').on('click','.schedule-sel-chk',function(){
			if($('.schedule-sel-chk:checked').length == $('.schedule-sel-chk').length){
				$('#schedules-sel-all').prop('checked',true);
			}else{
				$('#schedules-sel-all').prop('checked',false);
			}
			dataID = this.value.split("_").pop();
			if(this.checked){
				$('.table-schedules tr[data-id='+dataID+']').css('background-color', '#a5ddd5');
			}else{
				$('.table-schedules tr[data-id='+dataID+']').css('background-color', '#ffffff');
			}
		});

		$('.content').on('click','#btnCancelSchedule',function(){
			var scheduleDates = [];
			$('.schedule-sel-chk').each(function(i){
				if($(this).is(':checked')){
					scheduleDates.push($(this).attr('data-date'));
				}
			});
			if(scheduleDates.length <= 0){
				alert("There are no schedules selected for any Operation!");
				return false;
			}
		});

		$('.content').on('click','#passDates',function(){
			var scheduleDates = [];
			var deliveryAddress =[];
			var scheduleStatus =[];
			$('.schedule-sel-chk').each(function(i){
				if($(this).is(':checked')){
					scheduleDates.push($(this).attr('data-date'));
					scheduleStatus.push($(this).attr('data-status'));
					deliveryAddress.push($(this).attr('data-delivery-address'));
				}
			});
			if(scheduleDates.length <= 0){
				alert("There are no schedules selected for any Operation!");
				return false;
			}
			if(jQuery.inArray("Completed",scheduleStatus) != -1){
				swal("Selected schedule has completed service!","Select schedules with only pending status","warning");
				return false;
			}
			deliveryAddress.every(item => item == deliveryAddress[0])?'':swal("Selected schedules has multiple delivery addresses!","All selected schedule address will be changed to the new address","warning");

			$('#datesFromSchedules').val(scheduleDates);
		});

		$('.content').on('click','#assignStaff',function(){
			var schedules = [];
			var staffID = $('#replaced_caregiver_id').val();
			var daa = $('input[name=delivery_address_assign]:checked').val();
			if(staffID == 0){
				alert("Please select a staff for deployment!");
				return false;
			}

			var ivrsType = $('#scheduleStaffAssignModal .ivrs_type').val();

			$('.schedule-sel-chk').each(function(i){
				if($(this).is(':checked')){
					schedules.push($(this).val());
				}
			});

			if(schedules.length <= 0){
				alert("There are no schedules selected for any Operation!");
				return false;
			}

			if(staffID > 0 && schedules.length > 0){

				var roadDistance = $('.road_distance').val();

				$(this).attr('disabled', true);
				$.ajax({
					type: 'POST',
					url: '{{ route('lead.assign-staff-from-schedules') }}',
					data: {ivrs_type:ivrsType, staff_id:staffID, road_distance:roadDistance, schedules:schedules, delivery_address_assign:daa, _token:'{{csrf_token()}}'},
					success: function (d){
						if(d){
							showNotification('bg-green', 'Staff assigned successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
							setTimeout(function(){
								window.location.reload();
							},1000);
						}
						$('#assignStaff').attr('disabled', false);
					},
					error: function (err){
						console.log('Assign Staff error: '+err);
						$('#assignStaff').attr('disabled', false);
					}
				})
			}
		});

		$('.content').on('click','#cancelSchedules',function(){
			var schedules = [];
			var cancelType = $('input[name=cancelType]:checked').val();
			var cancelScheduleComment = $('#cancelScheduleComment').val();
			var ivrsType = $('#cancellationScheduleModal .ivrs_type').val();

			if(cancelType == ''){
				alert("Please select a reason for cancellation!");
				return false;
			}

			$('.schedule-sel-chk').each(function(i){
				if($(this).is(':checked')){
					schedules.push($(this).val());
				}
			});

			if(schedules.length <= 0){
				alert("There are no schedules selected for any Operation!");
				return false;
			}

			if(cancelType != '' && schedules.length > 0){
				$(this).attr('disabled', true);
				$.ajax({
					url: '{{ route('lead.cancel-schedules') }}',
					type: 'POST',
					data: {cancelType: cancelType, cancelScheduleComment: cancelScheduleComment,schedules: schedules,_token:'{{csrf_token()}}'},
					success: function (d){
						if(d){
							showNotification('bg-green', 'Schedule(s) cancelled successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
							setTimeout(function(){
								window.location.reload();
							},1000);
						}
						$(this).attr('disabled', false);
					},
					error: function (err){
						console.log(err);
						$(this).attr('disabled', false);
					}
				})
			}
		});

		$('.content').on('click','#unconfirmSchedule',function(){
			var schedules = [];
			var isNotComplete = false;
			$('.schedule-sel-chk').each(function(i){
				if($(this).is(':checked')){
					schedules.push($(this).val());

					if($(this).attr("data-status") != 'Completed'){
						alert("Please select only Completed schedule!");
						isNotComplete = true;
						return false;
					}
				}
			});
			if(schedules.length <= 0){
				alert("There are no schedules selected for any Operation!");
				return false;
			}

			if(schedules.length > 0 && isNotComplete == false){
				var retVal = confirm("The employee attendance for unconfirmed schedules will also be removed!");
            	if( retVal == true ){
                	$(this).attr('disabled', true);
					$.ajax({
						url: '{{ route('lead.unconfirm-schedules') }}',
						type: 'POST',
						data: {schedules: schedules, _token:'{{csrf_token()}}'},
						success: function (d){
							if(d){
								showNotification('bg-green', 'Schedule(s) Unconfirmed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
								setTimeout(function(){
									$('#viewSchedulesModal').modal('hide');
								},1000);
							}
							$(this).attr('disabled', false);
						},
						error: function (err){
							console.log(err);
							$(this).attr('disabled', false);
						}
					})
                	return true;
            	} else {return false;}
			}
		});

		$('input[type=radio][name=delivery_address]').change(function() {
			$('.serviceRequestCaregiverBlock').css('display','none');
			$('.serviceRequestCaregiverBlock #caregiver_id').val('');
		});

		$('input[type=radio][name=delivery_address_schedule]').change(function() {
			$('.viewScheduleCaregiverBlock').css('display','none');
			$('.viewScheduleCaregiverBlock #caregiver_id').val('');
		});

		$('input[type=radio][name=delivery_address_assign]').change(function() {
			$('.replaceScheduleCaregiverBlock #selectedStaffName').empty();
			$('.replaceScheduleCaregiverBlock #selectedStaffName').append('-');
			$('.replaceScheduleCaregiverBlock #replaced_caregiver_id').val('');
		});

	</script>
	<script type="text/javascript">
		$(window).load(function(){
			if(localStorage.getItem('helpMode') == 'true'){
				if("{{$l->status != 'Converted'}}"){
					swal({
						title: "The lead is in "+ '{{$l->status}}' +" status !",
						text: "Do you want to convert the Lead ?",
						icon: "warning",
						closeOnClickOutside: false,
						closeOnEsc: false,
						buttons: {
							yes: {
								text: "Yes!",
								value: "Yes",
							},
							no: {
								text: "No",
								value: "No",
							},
						},
					})
					.then((value) => {
						switch (value) {
							case "Yes":
								$(".btnChangeLeadStatus").trigger('click');
								break;

							case "No":
								swal("Ok!", "", "success")
								break;
						}
					});
				}else if("{{$l->status == 'Converted'}}"){
					if("{{!count($l->serviceRequests)}}"){
						swal({
							title: "The Episode has no Services !",
							text: "Do you want to create a service entry for the patient ?",
							icon: "warning",
							closeOnClickOutside: false,
							closeOnEsc: false,
							buttons: {
								yes: {
									text: "Yes!",
									value: "Yes",
								},
								no: {
									text: "No",
									value: "No",
								},
							},
						})
						.then((value) => {
							switch (value) {
								case "Yes":
									selectedItem = $('.vertical-tab-menu .list-group > a:eq(2)');
									$(selectedItem).siblings('a.active').removeClass("active");
									$(selectedItem).addClass("active");
									$("div.vertical-tab>div.vertical-tab-content").removeClass("active");
									$("div.vertical-tab>div.vertical-tab-content").eq(2).addClass("active");
									$(".btnAddService").trigger('click');
									break;

								case "No":
									swal("Ok!", "", "success")
									break;
							}
						});
					}
				}
			}
		})
	</script>
@endsection