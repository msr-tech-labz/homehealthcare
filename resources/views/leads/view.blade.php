@extends('layouts.main-layout')

@section('page_title','View Case - Cases |')

@section('active_cases','active')

@section('plugin.styles')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<!-- Bootstrap Tagsinput Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
@endsection

@section('page.styles')
<style>
	.nav-tabs > li{
		/*min-width: 12%;*/
	}
	.card .header .header-dropdown{
		top: 15px;
	}
	hr{
		margin-top: 10px;
		margin-bottom: 10px;
	}
	.table-borderless tbody tr td, .table-borderless tbody tr th{
		border: none;
		padding: 6px;
	}
	.table-borderless tbody tr td{
		min-width: 150px;
	}
	.table-borderless tbody tr td:before{
		content: ' :';
		padding-right: 10px;
		font-weight: bold;
	}
	.bootstrap-tagsinput{
		min-height: 40px;
		vertical-align: top;
		padding: 4px 0;
	}
	.bootstrap-tagsinput input{
		padding-left: 0;
	}
	.plus:after{
		content: ' + ';
		font-size: 18px;
		font-weight: bold;
	}
	.care-plan-card .header{
		padding: 15px 20px;
	}
	.modal-lg{
		width: 70%;
	}
	.modal .modal-header{
		padding: 20px 25px 15px 25px;
	}
	.modal-footer{
		border-top: 1px solid #ddd !important;
	}
	.details-pane .details{
		margin-top: 6px;
	}
	.details-pane .details .form-line{
		border-bottom: none;
	}
	.card .body .col-lg-9, .card .body .col-lg-3{
		margin-bottom: 8px !important;
	}
	.careplan-status button.waves-effect{
		width: 160px !important;
	}
	.comment-card .header, .comment-card .body { padding: 10px;}
	.comment-card th{ background: #ccc}
	.comment-card .table{ margin-bottom: 0}
</style>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="row clearfix">
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif

<div class="row">
	<form id="caseForm">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<a href="{{ route('case.index') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
						<i class="material-icons">arrow_back</i>
					</a>
					<h2 class="col-sm-10">
						{{ isset($c)?'Vew':'New'}} Care Plan
						<small>Case {{ isset($c)?'Updation':'Creation'}} Form</small>
					</h2>
					{{-- <ul class="header-dropdown m-r--5">
						<div class="btn-group" role="group">
							<button type="button" class="btn btn-success m-l-15 waves-effect"><i class="material-icons" style="color: #fff">save</i></button>
							<button type="submit" onclick="javascript: return saveCase();" class="btn btn-success waves-effect" style="line-height: 1.9;padding-left:0">Save Case</button>
						</div>&nbsp;&nbsp;
						{{-- <button type="submit" onclick="javascript: return saveCase();" class="btn btn-success btn-lg m-l-15 waves-effect">Save Employee</button> --}}
						{{-- <li class="dropdown">
							<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
								<i class="material-icons">more_vert</i>
							</a>
							<ul class="dropdown-menu pull-right">
								<li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>
								<li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>
								<li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>
							</ul>
						</li> --
					</ul> --}}
					<div class="clearfix"></div>
				</div>
				<div class="body" style="padding: 10px 8px;">
					<div clas="row clearfix" style="margin-bottom: 0">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="card" style="margin-bottom: 0">
								<div class="header bg-blue-grey" style="padding: 15px">
									<h2>
										Patient Information
									</h2>
								</div>
								<div class="body" style="padding: 8px; 10px">
									<div class="col-sm-1" style="width: 12%">
										<img src="/user.png" style="height:120px;width: 120px;" class="img-thumbnail">
									</div>
									<div class="col-sm-7">
										<table class="table table-condensed table-borderless" style="margin-bottom: 0">
											<tr>
												<th>Patient ID</th>
												<td>{{ $c->id }}</td>
												<th colspan="2"></th>
											</tr>
											<tr>
												<th>Patient Name</th>
												<td>{{ $c->patient->first_name }}</td>
												<th>Gender</th>
												<td>{{ $c->patient->gender }}</td>
											</tr>
											<tr>
												<th>Date of Birth</th>
												<td>{{ !empty($c->patient->date_of_birth)?$c->patient->date_of_birth->format('d-m-Y'):'-' }}</td>
												<th>Weight</th>
												<td>{{ $c->patient->patient_weight }}</td>
											</tr>
											<tr>
												<th>Mobile</th>
												<td>{{ $c->patient->mobile_number }}</td>
												<th>Email</th>
												<td>{{ $c->patient->email }}</td>
											</tr>
										</table>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="row clearfix" style="margin: 20px;">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active">
								<a href="#comments_tab" data-toggle="tab" aria-expanded="true">
									<i class="material-icons">comment</i> Case History {!! count($c->comments)?'&nbsp;&nbsp; <span class="badge bg-orange">'.count($c->comments).'</span>':'' !!}
								</a>
							</li>
							<li role="presentation" class="">
								<a href="#care_plan_tab" data-toggle="tab" aria-expanded="true">
									<i class="material-icons">child_friendly</i> Care Plan Details
								</a>
							</li>
							<li role="presentation" class="">
								<a href="#schedules_tab" data-toggle="tab">
									<i class="material-icons">date_range</i> Schedules {!! count($c->schedules)?'&nbsp;&nbsp; <span class="badge bg-cyan">'.count($c->schedules).'</span>':'' !!}
								</a>
							</li>
							<li role="presentation" class="">
								<a href="#billing_tab" data-toggle="tab">
									<i class="material-icons">credit_card</i> Billing {!! count($c->bills)?'&nbsp;&nbsp; <span class="badge bg-blue">'.count($c->bills).'</span>':'' !!}
								</a>
							</li>
						</ul>

						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane fade active in" id="comments_tab">
								<br>
								<!--<div class="row clearfix">
									<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 form-control-label">
										<label for="comment">Comment</label>
									</div>
									<div class="col-lg-7 col-md-7 col-sm-6 col-xs-6">
										<div class="form-group">
											<div class="form-line">
												<textarea id="comment" name="comment" class="form-control" placeholder="Comment"></textarea>
											</div>
										</div>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">
										<a class="btn btn-info btn-lg">Comment</a>
									</div>
								</div>
								<hr>								-->
								<div class="clearfix"></div>
								<div class="row clearfix">
									<div class="col-sm-6">
										<h2 class="card-inside-title">
											<button type="button" class="btn bg-blue btn-circle waves-effect waves-circle waves-float pull-right">
												<i class="material-icons">add</i>
											</button>
											Case comments
											<small>Comments and Notes</small>
										</h2>
										<div class="clearfix"></div>
										<hr>
									@if(isset($comments) && count($comments))
										@foreach($comments as $comment)
										<div class="card comment-card">
											<div class="header bg-light-blue">
												<h2>{{ $comment->user_name }}</h2>
												<ul class="header-dropdown m-r--5" style="top: 5px;">
													<li><a class="btn btn-danger waves-effect waves-block">{{ $comment->created_at->format('d M Y, h:i A')}}</a></li>
												</ul>
												</span>
											</div>
											<div class="body">
												{!! $comment->comment !!}
											</div>
										</div>
										@endforeach
									@else
										<div class="row clearfix">
											<div class="text-center"><i>No comments found</i></div>
										</div>
									@endif
									</div>

									<div class="col-sm-6">
										<h2 class="card-inside-title">
											Case Disposition
											<small>Dispostion History</small>
										</h2>
										<div class="clearfix"></div>
										<hr>
									@if(isset($dispositions) && count($dispositions))
										@foreach($dispositions as $disposition)
										<div class="card comment-card">
											<div class="header bg-indigo">
												<h2>Status: {{ $disposition->status }}</h2>
												@if($disposition->disposition_date != null)
												<ul class="header-dropdown m-r--5" style="top: 5px;">
													<li><a class="btn btn-danger waves-effect waves-block">{{ $disposition->disposition_date->format('d M Y')}}</a></li>
												</ul>
												@endif
												</span>
											</div>
											<div class="body">
												{!! $disposition->comment ?? '<i>No comments</i>' !!}
											</div>
										</div>
										@endforeach
									@else
										<div class="row clearfix">
											<div class="text-center"><i>No records found</i></div>
										</div>
									@endif
									</div>
								</div>
							</div>

							<div role="tabpanel" class="tab-pane fade in" id="care_plan_tab">
								{{-- <div class="row clearfix">
									<div class="col-sm-12 text-center">
										<a class="btn btn-info btnAddCarePlan"><span class="plus"></span> Add Care Plan</a>
									</div>
								</div> --}}
								<div class="row clearfix form-horizontal details-pane">
									<div class="col-sm-6">
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="careplan_name">Care Plan Name</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group details">
													<div class="form-line">
														{{ $c->careplan_name }}
													</div>
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="careplan_description">Description</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group details">
													<div class="form-line">
														{!! $c->careplan_description !!}
													</div>
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="medical_conditions">Medical Conditons</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group details">
													<div class="form-line">
														{!! $c->medical_conditions !!}
													</div>
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="medications">Medications</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group details">
													<div class="form-line">
														{!! $c->medications ?? '-' !!}
													</div>
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="designation">Service Required</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group details">
													{{ $c->service->service_name ?? '-' }}
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="no_of_hours">No. of hours</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group details">
													<div class="form-line">
														{{ $c->no_of_hours ?? '-' }}
													</div>
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="gender_preference">Gender Preference</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group details">
													{{ $c->gender_preference ?? '-' }}
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="language_preference">Language Preference</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group details">
													{{ $c->language_preference ?? '-' }}
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="status">Status</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group details">
													{{ $c->status ?? '-' }}
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<h2 class="card-inside-title">
											Case Disposition
											<small>Status, Comment</small>
										</h2>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
												<label for="careplan_status">Status</label>
											</div>
											<div class="col-lg-5 col-md-5 col-sm-5 col-xs-7">
												<div class="form-group">
													<select class="form-control show-tick" id="careplan_status">
														<option value="Pending" {{ $c->status=='Pending'?'selected':'' }}>Pending</option>
														<option value="Started" {{ $c->status=='Started'?'selected':'' }}>Started</option>
														<option value="Completed" {{ $c->status=='Completed'?'selected':'' }}>Completed</option>
														<option value="Discontinued" {{ $c->status=='Discontinued'?'selected':'' }}>Discontinued</option>
													</select>
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label">
												<label for="careplan_date">Date</label>
											</div>
											<div class="col-lg-5 col-md-5 col-sm-5 col-xs-7">
												<div class="form-group">
													<div class="form-line">
														<input type="text" class="form-control date" id="careplan_disposition_date" placeholder="Date">
													</div>
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label">
												<label for="careplan_comment">Comment</label>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group">
													<div class="form-line">
														<textarea class="form-control" id="careplan_comment" placeholder="Comment (if any)"></textarea>
													</div>
												</div>
											</div>
										</div><br>
										<div class="row clearfix">
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label">
											</div>
											<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
												<div class="form-group">
													<a class="btn btn-warning btn-lg waves-effect saveCarePlanDisposition" data-careplan-id="{{ Helper::encryptor('encrypt',$c->id) }}">Save Disposition</a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>

							<div role="tabpanel" class="tab-pane fade in" id="schedules_tab">
								<div class="row clearfix">
									<div class="col-sm-12 text-center">
										<a class="btn btn-info btnAddSchedule"><span class="plus"></span> Add Schedule</a>
									</div>
								</div>
								<div class="row clearfix">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>#</th>
												<th>Caregiver</th>
												<th>Start Date</th>
												<th>End Date</th>
												<th>Notes</th>
												<th>Billable</th>
												<th>Amount</th>
												<th>Status</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
									@if(count($c->schedules))
										@foreach($c->schedules as $i => $s)
											<tr>
												<td>{{ $i +1 }}</td>
												<td>
													<img src="/user.png" class="pull-right img-circle" width="40" height="40"/>
													{{ $s->caregiver->first_name }}<br>
													<small>{{ $s->caregiver->mobile_number }}</small>
												</td>
												<td>
													{{ $s->start_date->format('d-m-Y') }}<br>
													<small>{{ $s->start_time }}</small>
												</td>
												<td>
													{{ $s->end_date->format('d-m-Y') }}<br>
													<small>{{ $s->end_time }}</small>
												</td>
												<td>{!! $s->notes !!}</td>
												<td>{{ $s->billable?'Yes':'No' }}</td>
												<td>{{ $s->total_amount }}</td>
												<td>{{ $s->status }}</td>
												<td>
													<a class="btn btn-success btnViewSchedule" data-visit-status="{{ $s->status }}" data-visit-id="{{ Helper::encryptor('encrypt',$s->id) }}"><i class="material-icons">search</i></a>
												</td>
											</tr>
										@endforeach
									@endif
										</tbody>
									</table>
								</div>
								<div id="visit_list_pane" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								</div>
								<div class="clearfix"></div>
							</div>

							<div role="tabpanel" class="tab-pane fade in" id="billing_tab">
								<div class="row clearfix">
									<div class="col-sm-12 text-center">
										<a class="btn btn-info btnAddBill"><span class="plus"></span> Add Bill</a>
									</div>
								</div>
								<div class="row clearfix">
									<table class="table table-bordered table-condensed">
										<thead>
											<tr>
												<th>#</th>
												<th>Bill No</th>
												<th>Bill Date</th>
												<th>Visit ID</th>
												<th>Bill Amount</th>
												<th>Status</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td colspan="6" class="text-center">No billing records found.</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" name="careplan_id" id="careplan_id" value="{{ Helper::encryptor('encrypt',$c->id) }}">
		<input type="hidden" name="patient_id" id="patient_id" value="{{ Helper::encryptor('encrypt',$c->patient->id) }}">
	</form>
</div>

<!-- Visit List Template -->
<div id="visitListTemplate" class="card care-plan-card visitListCard hide">
	<div class="header bg-cyan">
		<h2>
			Visit Schedule - #1
		</h2>
		<ul class="header-dropdown m-r--5" style="top: 5px;">
			<li><a href="javascript:void(0);" class="removeVisitList btn btn-danger waves-effect waves-block" data-visit-id="1"><i class="material-icons">delete</i></a></li>
			<li>&nbsp;</li>
			<li><a href="javascript:void(0);" class="slideVisitList btn btn-warning waves-effect waves-block" data-visit-id="1"><i class="material-icons">expand_more</i></a></li>
		</ul>
	</div>
	<div class="body form-horizontal">
		<div class="col-sm-6">
			<h2 class="card-inside-title">
				Visit Details
				<small>Caregiver, Visit Type, Start Date & Time, End Date & Time</small>
			</h2>
			<div class="row clearfix">
				<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
					<label for="no_of_hours">Start Date</label>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					<div class="form-group">
						<div class="form-line">
							<input type="text" data-visit-id="1" id="start_date" class="form-control date" placeholder="Start Date">
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					<div class="form-group">
						<div class="form-line">
							<input type="text" data-visit-id="1" id="start_time" class="form-control time" placeholder="Start Time">
						</div>
					</div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
					<label for="no_of_hours">End Date</label>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					<div class="form-group">
						<div class="form-line">
							<input type="text" data-visit-id="1" id="end_date" class="form-control date" placeholder="End Date">
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					<div class="form-group">
						<div class="form-line">
							<input type="text" data-visit-id="1" id="end_time" class="form-control time" placeholder="End Time">
						</div>
					</div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"></div>
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
					<a class="btn btn-warning btn-block waves-effect btnChooseStaff" onclick="javascript: return clearSearchForm();" data-visit-id="0">Choose Staff</a>
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
					<label for="no_of_hours">Staff / Caregiver</label>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<div class="form-group">
						<div style="margin-top: 8px;" id="selectedStaffName_0" data-visit-id="0">-</div>
						<input type="hidden" id="caregiver_id" value=""/>
						{{-- <select class="form-control show-tick" placeholder="Type staff name" data-live-search="true" data-placeholder="Type staff name">
						</select> --}}
					</div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
					<label for="no_of_hours">Notes</label>
				</div>
				<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
					<div class="form-group">
						<div class="form-line">
							<textarea data-visit-id="1" id="notes" class="form-control" placeholder="Notes (if any)"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<h2 class="card-inside-title">
				Payment Details
				<small>Service Charges, Discount</small>
			</h2>
			<div class="row clearfix">
				<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
					<label for="no_of_hours">Billable</label>
				</div>
				<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
					<div class="form-group" style="margin-top: 1%">
						<input data-visit-id="1" name="billable_0" type="radio" id="billable_yes_0" class="with-gap radio-col-deep-purple billable" value="1" checked/>
						<label for="billable_yes_0">Yes</label>
						<input data-visit-id="1" name="billable_0" type="radio" id="billable_no_0" class="with-gap radio-col-deep-purple billable" value="0" />
						<label for="billable_no_0">No</label>
					</div>
				</div>
			</div>
			<div id="billable_block_0">
				<div class="row clearfix">
					<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
						<label for="no_of_hours">Amount</label>
					</div>
					<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
						<div class="form-group">
							<div class="form-line">
								<input type="number" data-visit-id="1" id="amount" class="form-control amtCal" placeholder="Amount in INR">
							</div>
						</div>
					</div>
				</div>
				<div class="row clearfix">
					<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
						<label for="no_of_hours">Special Travel Allowance (Food)</label>
					</div>
					<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
						<div class="form-group">
							<div class="form-line">
								<input type="number" data-visit-id="1" id="spl_travel_allowance" class="form-control amtCal" placeholder="Amount in INR">
							</div>
						</div>
					</div>
				</div>
				<div class="row clearfix">
					<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
						<label for="no_of_hours">Last Mile <br>(Payable by Customer)</label>
					</div>
					<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
						<div class="form-group">
							<div class="form-line">
								<input type="number" data-visit-id="1" id="last_mile" class="form-control amtCal" placeholder="Amount Payable in INR">
							</div>
						</div>
					</div>
				</div>
				<div class="row clearfix">
					<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
						<label for="no_of_hours">Discount</label>
					</div>
					<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
						<div class="form-group" style="margin-top: 1%">
							<input data-visit-id="1" name="discount_applicable_0" type="radio" id="discount_yes_0" class="with-gap radio-col-deep-purple discount_applicable" value="1"/>
							<label for="discount_yes_0">Yes</label>
							<input data-visit-id="1" name="discount_applicable_0" type="radio" id="discount_no_0" class="with-gap radio-col-deep-purple discount_applicable" value="0" checked/>
							<label for="discount_no_0">No</label>
						</div>
					</div>
				</div>
				<div id="discount_block_0" style="display: none">
					<div class="row clearfix">
						<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
							<label for="no_of_hours">Discount Type</label>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
							<div class="form-group" style="margin-top: 1%">
								<input data-visit-id="1" name="discount_type_0" type="radio" id="discount_type_percentage_0" class="with-gap radio-col-deep-purple" value="Percentage"/>
								<label for="discount_type_percentage_0">Percentage (%)</label>
								<input data-visit-id="1" name="discount_type_0" type="radio" id="discount_type_fixed_0" class="with-gap radio-col-deep-purple" value="Fixed" checked/>
								<label for="discount_type_fixed_0">Fixed Value (Rs.)</label>
							</div>
						</div>
					</div>
					<div class="row clearfix">
						<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
							<label for="no_of_hours">Discount Value</label>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line">
									<input type="number" data-visit-id="1" id="discount_value" class="form-control amtCal" placeholder="Discount Value in INR">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row clearfix">
					<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
						<label for="no_of_hours">Total Amount</label>
					</div>
					<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
						<div class="form-group">
							<div class="form-line">
								<input type="number" data-visit-id="1" id="total_amount" class="form-control" placeholder="Amount Payable in INR">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-12 clearfix text-center">
			<a class="btn btn-success btn-lg waves-effect btnSaveSchedule" data-visit-id="0">Save Schedule</a>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<!-- Staff Search Modal -->
<div class="modal fade" id="staffSearchModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="largeModalLabel">Employee Search Form</h4>
			</div>
			<div class="modal-body">
				<div class="row clearfix">
					<form id="staffSearchForm" class="col-sm-12">
						<div class="col-sm-3">
							<div class="form-group">
								<label for="specialization">Specialization</label>
								<div class="form-line">
									<select class="form-control show-tick" id="filter_specialization">
										<option value="0">-- Any --</option>
								@if(isset($specializations) && count($specializations))
									@foreach($specializations as $s)
										<option value="{{ $s->id }}">{{ $s->specialization_name }}</option>
									@endforeach
								@endif
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label for="experience">Experience</label>
								<div class="form-line">
									<select class="form-control show-tick" id="filter_experience">
										<option value="0">-- Any --</option>
										<option value="0_5">0-5 yr(s)</option>
										<option value="5_10">5-10 yr(s)</option>
										<option value="10_15">10-15 yr(s)</option>
										<option value="15_35">Above 15 yrs</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label for="city">City</label>
								<div class="form-line">
									<input class="form-control" id="filter_city" placeholder="City">
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<br>
							<a class="btn btn-primary waves-effect btnFilterStaff">Search</a>&nbsp;&nbsp;
							<button type="reset" class="btn btn-danger waves-effect btnReset">Reset</button>
						</div>
					</form>
					<div class="col-sm-12">
						<div class="page-loader-wrapper">
							<div class="loader">
								<div class="md-preloader pl-size-md">
									<svg viewbox="0 0 75 75">
										<circle cx="37.5" cy="37.5" r="33.5" class="pl-blue" stroke-width="4" />
									</svg>
								</div>
								<p>Please wait...</p>
							</div>
						</div>
						<table class="table table-bordered table-striped caseSearchResults">
							<thead>
								<tr>
									<th>Name</th>
									<th>Contact</th>
									<th>Designation</th>
									<th>Experience</th>
									<th>Total Visits</th>
									<th>Rating</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-center" colspan="7">Click 'Search' to get results</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success waves-effect btnSelectStaff" data-visit-id="0">Continue</button>
				<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>

<!-- View Schedule Modal -->
<div class="modal fade" id="viewScheduleModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="largeModalLabel">View Schedule</h4>
			</div>
			<div class="modal-body">
				<div class="row clearfix">
					<div class="col-sm-12">
						<div class="form-group">
							<label for="specialization">Status</label>
							<div class="demo-checkbox">
								<input type="radio" id="status_started" name="status" class="with-gap radio-col-light-blue" value="Started" />
								<label for="status_started">Started</label>
								<input type="radio" id="status_completed" name="status" class="with-gap radio-col-light-blue" value="Completed" />
								<label for="status_completed">Completed</label>
								<input type="radio" id="status_cancelled" name="status" class="with-gap radio-col-light-blue" value="Cancelled" />
								<label for="status_cancelled">Cancelled</label>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<label for="comment">Comment</label>
							<div class="form-line">
								<textarea class="form-control" id="comment" placeholder="Comment"></textarea>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success waves-effect pull-right">Update</button>
			</div>
		</div>
	</div>
</div>
@endsection

@section('plugin.scripts')
<!-- Select Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<!-- Input Mask Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>
<!-- Moment Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<!-- Bootstrap Tags Input Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
@endsection

@section('page.scripts')
<script>
	$(function(){
		initBlock();

		$('.tags-input').tagsinput({
			allowDuplicates: false
		});

		/*-------------------------------------------
		/*  Handles Schedule Add Block
		/*-------------------------------------------*/
		$('.content').on('click','.btnAddSchedule', function(){
			var cur_size = $('#schedules_tab > #visit_list_pane > .visitListCard').length;
			$('#visitListTemplate').clone().prependTo('#schedules_tab #visit_list_pane');

			var count = (cur_size+1);
			var div = $('#schedules_tab #visit_list_pane #visitListTemplate');
			$(div).attr('id','visitList_'+count);
			var block = '#schedules_tab > #visit_list_pane > #visitList_'+count;

			$(block).removeClass('hide');
			// Change Header title
			$(block+' > .header > h2').html('Visit Schedule - #'+count);
			// Change Header data ID
			$(block+' > .header > ul > li > a.removeVisitList').attr('data-visit-id',count);
			$(block+' > .header > ul > li > a.slideVisitList').attr('data-visit-id',count);
			// Change All input Data IDs
			$(block).find('input, select, textarea').each(function(){
				$(this).attr('data-visit-id',count);
			});

			$(block).find('.btnChooseStaff').attr('data-visit-id',count);
			$(block).find('#selectedStaffName_0').attr('data-visit-id',count);
			$(block).find('#selectedStaffName_0').attr('id','selectedStaffName_'+count);

			$(block).find('#billable_block_0').attr('id','billable_block_'+count);

			$(block).find('#billable_yes_0').next().attr('for','billable_yes_'+count);
			$(block).find('#billable_yes_0').attr('id','billable_yes_'+count);
			$(block).find('#billable_yes_'+count).attr('name','billable_'+count);
			$(block).find('#billable_no_0').next().attr('for','billable_no_'+count);
			$(block).find('#billable_no_0').attr('id','billable_no_'+count);
			$(block).find('#billable_no_'+count).attr('name','billable_'+count);

			$(block).find('#discount_block_0').attr('id','discount_block_'+count);

			$(block).find('#discount_yes_0').next().attr('for','discount_yes_'+count);
			$(block).find('#discount_yes_0').attr('id','discount_yes_'+count);
			$(block).find('#discount_yes_'+count).attr('name','discount_applicable_'+count);
			$(block).find('#discount_no_0').next().attr('for','discount_no_'+count);
			$(block).find('#discount_no_0').attr('id','discount_no_'+count);
			$(block).find('#discount_no_'+count).attr('name','discount_applicable_'+count);

			$(block).find('#discount_type_percentage_0').next().attr('for','discount_type_percentage_'+count);
			$(block).find('#discount_type_percentage_0').attr('id','discount_type_percentage_'+count);
			$(block).find('#discount_type_percentage_'+count).attr('name','discount_type_'+count);
			$(block).find('#discount_type_fixed_0').next().attr('for','discount_type_fixed_'+count);
			$(block).find('#discount_type_fixed_0').attr('id','discount_type_fixed_'+count);
			$(block).find('#discount_type_fixed_'+count).attr('name','discount_type_'+count);

			$(block).find('.btnSaveSchedule').attr('data-visit-id',count);

			initBlock();
		});

		$('.content').on('click','.slideVisitList', function(){
			var id = $(this).data('visit-id');
			var current_class = $(this).find('i').html();
			if(current_class == 'expand_more')
				$(this).find('i').html('expand_less');
			else
				$(this).find('i').html('expand_more');

			$('#visitList_'+id+' div.body').slideToggle("slow");
		});

		$('.content').on('click','.removeVisitList', function(){
			var id = $(this).data('visit-id');

			if(confirm("Are you sure?")){
				$('#visitList_'+id).remove();
			}
		});

		/*-------------------------------------------
		/*  Handles Existing Customer Check
		/*-------------------------------------------*/
		$('.content').on('click', '#checkForPreviousCarePlans', function(){
			checkIfExistingCustomer();
		});

		/*-------------------------------------------
		/*  Handles Case Disposition Update
		/*-------------------------------------------*/
		$('.content').on('click','.saveCarePlanDisposition', function(){
			var id = $(this).data('careplan-id');

			var status = $('#careplan_status option:selected').val();
			var date = $('#careplan_disposition_date').val();
			var comment = $('#careplan_comment').val();

			if(status){
				$.ajax({
					url: '{{ route('case.update-disposition') }}',
					type: 'POST',
					data: {_token: '{{ csrf_token() }}', cid: id, status: status, date: date, comment: comment},
					success: function (data){
						console.log(data);
						if(data){
							window.location.reload();
						}
					},
					error: function (error){
						console.log(error);
					}
				});
			}
		});

		$('.content').on('change','.billable', function(){
			var id = $(this).data('visit-id');
			if($(this).val() == 1){
				$('#billable_block_'+id).show();
			}else{
				$('#billable_block_'+id).hide();
			}
		});

		$('.content').on('change','.discount_applicable', function(){
			var id = $(this).data('visit-id');
			if($(this).val() == 1){
				$('#discount_block_'+id).show();
			}else{
				$('#discount_block_'+id).hide();
			}
		});

		$('.content').on('click','.btnChooseStaff', function(){
			var id = $(this).data('visit-id');

			$('#staffSearchModal .btnSelectStaff').attr('data-visit-id',id);
			$('#staffSearchModal').modal();
		});

		$('.content').on('click','.btnFilterStaff', function(){
			specialization = $('#filter_specialization option:selected').val();
			experience = $('#filter_experience option:selected').val();
			city = $('#filter_city').val();

			$.ajax({
				url: '{{ route('ajax.caregiver.filter') }}',
				type: 'POST',
				data: {_token: '{{ csrf_token() }}', s: specialization, e: experience, c: city},
				success: function (data){
					//console.log(data);
					html = '<tr><td class="text-center" colspan="7">Click \'Apply Filter\' to get results</td></tr>';
					if(data.length){
						html = '';
						$.each(data, function(i){
							html += `<tr>
										<td>`+data[i].full_name+`<br><small>`+data[i].location+`</small></td>
										<td>`+data[i].mobile+`<br><small>`+data[i].email+`</small></td>
										<td>`+data[i].specialization+`</td>
										<td>`+data[i].experience+`</td>
										<td>`+data[i].total_visits+`</td>
										<td>`+data[i].rating+`</td>
										<td class="text-center"><input name="choosen_emp" type="radio" id="choosen_emp_`+data[i].id+`" class="with-gap radio-col-blue" data-name="`+data[i].full_name+`" data-mobile="`+data[i].mobile+`" value="`+data[i].id+`" /><label for="choosen_emp_`+data[i].id+`"></label></td>
									</tr>`;
						});
					}
					$('.caseSearchResults tbody').html(html);
				},
				error: function (error){
					console.log(error);
				}
			})
		});

		$('.content').on('input','.amtCal', function(){
			var id = $(this).data('visit-id');
			calculateTotal(id);
		});

		$('.content').on('click','.btnReset', function(){
			clearSearchForm();
		});

		$('.content').on('click', '.btnSelectStaff', function(){
			var id = $(this).attr('data-visit-id');

			var emp = $('input[name=choosen_emp]:checked').val();
			var name = $('input[name=choosen_emp]:checked').data('name');
			var mobile = $('input[name=choosen_emp]:checked').data('mobile');

			 if(typeof emp != 'undefined' && emp != ''){
				 $('#staffSearchModal').modal('hide');
				 $('#visitList_'+id+' #caregiver_id').val(emp);
				 $('#visitList_'+id+' #selectedStaffName_'+id).html('<div class="col-sm-2"><img src="/user.png" width="40" height="40" class="img-circle"></div><div class="col-sm-9">'+name+'<br>Mob: '+mobile+'</div>');
			 }else{
				 alert("Please select staff !");
			 }
		});

		$('.content').on('click','.btnViewSchedule', function(){
			$('#viewScheduleModal').modal();
		});

		//$('.date').inputmask('dd-mm-yyyy', { placeholder: '__-__-____' });
		/* $('.money-inr').inputmask('₹ 999999.99', { numericInput: true, rightAlign: false }); */
	});

	function initBlock()
	{
		$('.date').inputmask('dd-mm-yyyy', { placeholder: '__-__-____' });
	}

	function checkIfExistingCustomer()
	{
		var fname = $('#first_name').val();
		var lname = $('#last_name').val();
		var mobile = $('#mobile').val();
		var email = $('#email').val();
		var dob = $('#date_of_birth').val();

		if(fname != '' || lname != '' || mobile != '' || email != '' || dob != ''){
			$.ajax({
				url: '{{ route('case.check-for-existing-case') }}',
				type: 'POST',
				data: {f: fname, l: lname, m: mobile, e: email, d: dob, _token: '{{ csrf_token() }}'},
				success: function (data){
					console.log(data);
				},
				error: function (error){
					console.log(error);
				}
			});
		}
	}

	function calculateTotal(id)
	{
		var totalAmount = 0;

		var block = '#visit_list_pane #visitList_'+id+' #billable_block_'+id;
		var amount = $(block+' #amount').val();
		amount = amount?parseFloat(amount):0;
		var spl = $(block+' #spl_travel_allowance').val();
		spl = spl?parseFloat(spl):0;
		var lm = $(block+' #last_mile').val();
		lm = lm?parseFloat(lm):0;

		var discount = $(block+' input[name=discount_applicable_'+id+']:checked').val();
		var dt = $(block+' input[name=discount_type_'+id+']:checked').val();

		var dv = $(block+' #discount_value').val();
		dv = dv?parseFloat(dv):0;

		//console.log(amount + ' : ' + spl + ' : ' + lm + ' : ' + dv);

		totalAmount = amount + spl + lm;
		if(discount){
			if(dt == 'Percentage'){
				totalAmount = parseFloat(totalAmount) - parseFloat(totalAmount) * (dv/100);
			}

			if(dt == 'Fixed'){
				totalAmount = parseFloat(totalAmount) - dv;
			}
		}

		$(block+' #total_amount').val(totalAmount);
	}

	function clearSearchForm()
	{
		$('#staffSearchModal #staffSearchForm #reset').trigger('click');
		$('#staffSearchModal #staffSearchForm #filter_specialization').selectpicker('val','0');
		$('#staffSearchModal #staffSearchForm #filter_experience').selectpicker('val','0');
		$('#staffSearchModal .caseSearchResults tbody').html('<tr><td class="text-center" colspan="7">Click \'Search\' to getresults</td></tr>');
	}

	var visitListArray = [];

	$('.content').on('click','.btnSaveSchedule', function(){
		var id = $(this).data('visit-id');
		var careplanID = $('#careplan_id').val();
		var patientID = $('#patient_id').val();

		// Parse all Visit List
		var block = '#schedules_tab #visit_list_pane #visitList_'+id;
		if($(block).length){
			visitListArray = [];

			var startDate = $(block+ ' #start_date').val();
			var startTime = $(block+ ' #start_time').val();
			var endDate = $(block+ ' #end_date').val();
			var endTime = $(block+ ' #end_time').val();
			var notes = $(block+ ' #notes').val();

			var caregiverID = $(block+ ' #caregiver_id').val();

			var billable = $(block+ ' input[name=billable_'+id+']:checked').val();
			var amount = $(block+ ' #amount').val();
			var splTravelAllowance = $(block+ ' #spl_travel_allowance').val();
			var lastMile = $(block+ ' #last_mile').val();

			var discount = $(block+ ' input[name=discount_applicable_'+id+']:checked').val();
			var discountType = $(block+ ' input[name=discount_type_'+id+']:checked').val();
			var discountValue = $(block+ ' #discount_value').val();

			var totalAmount = $(block+ ' #total_amount').val();

			visitListArray = {
				'start_date': startDate,
				'start_time': startTime,
				'end_date': endDate,
				'end_time': endTime,
				'notes': notes,
				'caregiver_id': caregiverID,
				'billable': billable,
				'amount': amount?parseFloat(amount):0,
				'spl_travel_allowance': splTravelAllowance?parseFloat(splTravelAllowance):0,
				'last_mile': lastMile?parseFloat(lastMile):0,
				'discount': discount,
				'discount_type': discountType,
				'discount_value': discountValue?parseFloat(discountValue):0,
				'total_amount': totalAmount?parseFloat(totalAmount):0,
			};
		}

		if(visitListArray){
			$.ajax({
				url: '{{ route('case.save-schedule') }}',
				type: 'POST',
				data: {_token: '{{ csrf_token() }}', careplan_id: careplanID, patient_id: patientID, visit: visitListArray},
				success: function (data){
					console.log(data);
					window.location.reload();
				},
				error: function (error){
					console.log(error);
				}
			});
		}
	});
</script>
@endsection
