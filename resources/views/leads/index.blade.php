@extends('layouts.main-layout')

@section('page_title','Leads - ')

@section('active_leads','active')

@section('page.styles')
<style>
.plus:after{
    content: ' + ';
    font-size: 18px;
    font-weight: bold;
}
</style>
@endsection

@section('plugin.styles')
<style>
.tab-nav-right > li > a{
    min-width: 180px !important;
    text-align: center;
}
.nav-tabs > li{
    margin-bottom:2px !important;
}
.nav-tabs > li.active{
    border-left: 1px solid #aabcfe;
    border-right: 1px solid #aabcfe;
    border-top: 4px solid #aabcfe;
    margin-bottom: 2px;
}
.nav-tabs > li.active > a{
    font-weight: bold;
    color: #2196F3 !important;
}
.nav-tabs > li > a:before{
    border-bottom: 0 !important;
}
table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td {
    /*text-transform: uppercase; !important;*/
}
.allocations:hover{
    cursor: pointer;
    background-color: darkred !important;
}

.followups:hover{
    cursor: pointer;
    background-color: darkred !important;
}

.filter{
    display: inline-block;
}
.filter-div > label{
    margin-right: 10px !important;
}
</style>
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection

@section('side-menu')
    @include('menus.leads')
@endsection

@section('content')
    <style>
        .tile{
            width: 20% !important;
            padding-right: 5px !important;
            padding-left: 5px !important;
        }
        .info-box .icon{
            width: 60px !important;
            padding-top: 5px;
        }
        .info-box .content{
            margin: 1px 5px !important;
        }
        .info-box .content .text{
            margin-top: 5px !important;
        }
    </style>
    <div class="block-header">
        <h2 class="col-sm-2">Leads</h2>
        @ability('admin','leads-create')
        <div class="col-sm-10 text-right">
            <a href="{{ route('lead.create') }}" class="btn btn-primary waves-effect">Create Lead</a>
        </div>
        @endability
        <div class="clearfix"></div>
    </div>
    @ability('admin','leads-index')
    <div class="row clearfix" style="margin-bottom: 30px;">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 tile">
            <div class="info-box bg-red hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">people</i>
                </div>
                <div class="content">
                    <div class="text">Pending</div>
                    <div class="number count-to">{{ count($leads->whereIn('status',['Pending','Follow-Up','Assessment Pending','Assessment Completed'])) }}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 tile">
            <div class="info-box bg-blue hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">list</i>
                </div>
                <div class="content">
                    <div class="text">Converted</div>
                    <div class="number count-to">{{ count($leads->where('status','Converted')) }}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 tile">
            <div class="info-box bg-green hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">account_balance_wallet</i>
                </div>
                <div class="content">
                    <div class="text">Closed | Dropped</div>
                    <div class="number count-to">{{ count($leads->where('status','Closed')) }} | {{ count($leads->where('status','Dropped')) }}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 tile">
            <div class="info-box bg-orange hover-expand-effect allocations" data-toggle="modal" data-target="#allocationsModal">
                <div class="icon">
                    <i class="material-icons">account_balance_wallet</i>
                </div>
                <div class="content">
                    <div class="text">Pending Service Allocations</div>
                    <div class="number count-to">{{count($allocationArray)}}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 tile">
            <div class="info-box bg-pink hover-expand-effect followups" data-toggle="modal" data-target="#followupsModal">
                <div class="icon">
                    <i class="material-icons">settings_phone</i>
                </div>
                <div class="content">
                    <div class="text">Follow Up</div>
                    <div class="number count-to">{{ count($followupArray) }}</div>
                </div>
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="card">
            <div class="body">
                <div class="filter-div" style="margin-top: 10px;">
                    <b>Filter by Status : &nbsp;&nbsp;</b>
                    <input type="checkbox" name="filter_status" id="filter_pending" value="Pending" class="filled-in chk-col-blue filter">
                    <label for="filter_pending">Pending</label>
                    <input type="checkbox" name="filter_status" id="filter_followup" value="Follow Up" class="filled-in chk-col-indigo filter">
                    <label for="filter_followup">Follow Up</label>
                    <input type="checkbox" name="filter_status" id="filter_converted" value="Converted" class="filled-in chk-col-green filter">
                    <label for="filter_converted">Converted</label>
                    <input type="checkbox" name="filter_status" id="filter_assessment_pending" value="Assessment Pending" class="filled-in chk-col-orange filter">
                    <label for="filter_assessment_pending">Assessment Pending</label>
                    <input type="checkbox" name="filter_status" id="filter_closed" value="Closed" class="filled-in chk-col-red filter">
                    <label for="filter_closed">Closed</label>
                    <input type="checkbox" name="filter_status" id="filter_dropped" value="Dropped" class="filled-in chk-col-red filter">
                    <label for="filter_dropped">Dropped</label>
                </div>
                <div class="clearfix"></div><br>
                <!-- Simplified Tabs -->
                <table class="table table-bordered table-hover table-condensed table-striped" id="leadsTable" style="zoom:83%;">
                    <thead>
                        <tr>
                            <th>Code Status</th>
                            <th>Source</th>
                            <th>Patient</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Location</th>
                            <th>Service Required</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($leads) && count($leads->whereIn('status',['Pending','Follow Up','Assessment Pending','Assessment Completed'])))
                            @foreach($leads->whereIn('status',['Pending','Follow Up','Assessment Pending','Assessment Completed']) as $i => $l)
                                <tr data-id="{{ Helper::encryptor('encrypt',$l->id) }}" @if($l->aggregator_lead){!! 'style="background:rgba(8, 131, 229, 0.15)"' !!}@endif>
                                    <td>
                                        <span class="label bg-orange">{{ $l->status }}</span>
                                        @if($l->registration_amount > 0)
                                        <span class="label bg-deep-purple">Registered</span>
                                        @endif<br><br>
                                        {{ $l->created_at->format('d-m-Y') }}
                                        <small>{{ $l->created_at->format('h:i A') }}</small>
                                    </td>
                                    <td>
                                        @if($l->aggregator_lead)
                                            {{ 'ApnaCare' }}
                                        @else
                                        {{ isset($l->referralSource)?$l->referralSource->source_name:'' }}<br>
                                        <small>{{ isset($l->referralCategory)?$l->referralCategory->category_name:'' }}</small>
                                        @endif
                                    </td>
                                    <td>
                                        {{ isset($l->patient->first_name)?$l->patient->first_name:''}} {{isset($l->patient->last_name)?$l->patient->last_name:'' }}<br>
                                        <small>Enquirer: {{ isset($l->patient->enquirer_name)?$l->patient->enquirer_name:'' }}</small>
                                    </td>
                                    <td>
                                        @if($l->aggregator_lead && $l->aggregator_lead_status == 'Pending')
                                            {{ isset($l->patient->contact_number)?str_pad(substr($l->patient->contact_number,0,3),10,'X',STR_PAD_RIGHT):'' }}<br>
                                            <small>Enquirer: {{ str_pad(substr($l->patient->alternate_number,0,3),10,'X',STR_PAD_RIGHT) }}</small>
                                        @else
                                        {{ isset($l->patient->contact_number)?$l->patient->contact_number:'' }}<br>
                                        <small>Enquirer: {{ $l->patient->alternate_number }}</small>
                                        @endif
                                    </td>
                                    <td>
                                        @if($l->aggregator_lead && $l->aggregator_lead_status == 'Pending')
                                            @if($l->patient->email)
                                            {? $emailParts = explode("@",$l->patient->email); ?}
                                            {{ str_pad(substr($emailParts[0],0,2),10,'X',STR_PAD_RIGHT).'@'.$emailParts[1] }}
                                            @else
                                                {{ '-' }}
                                            @endif
                                        @else
                                            {{ $l->patient->email ?? '-' }}
                                        @endif
                                    </td>
                                    <td>
                                        {{ $l->patient->city ?? '-' }}<br>
                                        <small>{{ $l->patient->area ?? '-' }}</small>
                                    </td>
                                    <td>
                                        @if($l->aggregator_lead && $l->status == 'Pending')
                                            {{ $l->aggregator_service }}
                                        @else
                                            {{ Helper::getServiceName($l->service_required) }}
                                        @endif
                                    </td>
                                    <td>{{ $l->status ?? '-' }}</td>
                                    <td>
                                        @if($l->aggregator_lead && $l->aggregator_lead_status == 'Pending')
                                            @ability('admin', 'leads-edit')
                                            <a href="javascript:void(0);" class="btn btn-md bg-orange btnViewLead" data-id="{{ Helper::encryptor('encrypt',$l->id) }}" data-patient-id="{{ Helper::encryptor('encrypt',$l->patient_id) }}" data-patient="{{ json_encode(collect($l->patient)->only('first_name','last_name','patient_age','patient_weight','gender','area','city')) }}" data-case="{{ json_encode(collect($l)->only('case_description','medical_conditions','medications','procedures','aggregator_service','language_preference','gender_preference','aggregator_rate','aggregator_rate_negotiable')) }}" title="View Lead Details">View</a>
                                            @endability
                                        @elseif($l->aggregator_lead && $l->aggregator_lead_status == 'Accepted')
                                            @ability('admin', 'leads-edit')
                                            <a href="{{ route('lead.view',Helper::encryptor('encrypt',$l->id)) }}" class="btn btn-md bg-orange" data-id="{{ Helper::encryptor('encrypt',$l->id) }}" title="Create Lead">View</a>
                                            @endability
                                        @elseif(!$l->aggregator_lead && $l->aggregator_lead_status == 'Pending')
                                            @ability('admin', 'leads-edit')
                                            <a href="{{ route('lead.view',Helper::encryptor('encrypt',$l->id)) }}" class="btn btn-md bg-orange" data-id="{{ Helper::encryptor('encrypt',$l->id) }}" title="View Lead">View</a>
                                            @endability
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        @if(isset($leads) && count($leads->whereIn('status',['Converted'])))
                            @foreach($leads->whereIn('status',['Converted']) as $i => $c)
                            <tr data-id="{{ Helper::encryptor('encrypt',$c->id) }}" @if($c->aggregator_lead){!! 'style="background:rgba(8, 131, 229, 0.15)"' !!}@endif>
                                <td>
                                    <span class="label bg-green">{{ $c->status }}</span>
                                    @if($c->registration_amount > 0)
                                    <span class="label bg-deep-purple">Registered</span>
                                    @endif<br><br>
                                    {{ $c->created_at->format('d-m-Y') }}
                                    <small>{{ $c->created_at->format('h:i A') }}</small>
                                </td>
                                <td>
                                    @if($c->aggregator_lead)
                                        {{ 'ApnaCare' }}
                                    @else
                                    {{ isset($c->referralSource)?$c->referralSource->source_name:'' }}<br>
                                    <small>{{ isset($c->referralCategory)?$c->referralCategory->category_name:'' }}</small>
                                    @endif
                                </td>
                                <td>
                                    {{ $c->patient->first_name.' '.$c->patient->last_name }}<br>
                                    <small>Enquirer: {{ $c->patient->enquirer_name }}</small>
                                </td>
                                <td>
                                    {{ $c->patient->contact_number }}<br>
                                    <small>Enquirer: {{ $c->patient->alternate_number }}</small>
                                </td>
                                <td>
                                    {{ $c->patient->email ?? '-' }}
                                </td>
                                <td>
                                    {{ $c->patient->city ?? '-' }}<br>
                                    <small>{{ $c->patient->area ?? '-' }}</small>
                                </td>
                                <td>
                                    @if($c->aggregator_lead && $c->status == 'Pending')
                                        {{ $c->aggregator_service }}
                                    @else
                                        {{ Helper::getServiceName($c->service_required) }}
                                    @endif
                                </td>
                                <td>{{ $c->status ?? '-' }}</td>
                                <td>
                                    @ability('admin', 'leads-edit')
                                    <a href="{{ route('lead.view',Helper::encryptor('encrypt',$c->id)) }}" class="btn btn-md bg-green" data-id="{{ Helper::encryptor('encrypt',$c->id) }}" title="View Lead">View</a>
                                    @endability
                                </td>
                            </tr>
                            @endforeach
                        @endif
                        @if(isset($leads) && count($leads->whereIn('status',['Closed','Dropped'])))
                            @foreach($leads->whereIn('status',['Closed','Dropped']) as $i => $c)
                                <tr data-id="{{ Helper::encryptor('encrypt',$c->id) }}" @if($c->aggregator_lead){!! 'style="background:rgba(8, 131, 229, 0.15)"' !!}@endif>
                                    <td>
                                        <span class="label bg-red">{{ $c->status }}</span>
                                        @if($c->registration_amount > 0)
                                        <span class="label bg-deep-purple">Registered</span>
                                        @endif<br><br>
                                        {{ $c->created_at->format('d-m-Y') }}
                                        <small>{{ $c->created_at->format('h:i A') }}</small>
                                    </td>
                                    <td>
                                        @if($c->aggregator_lead)
                                            {{ 'ApnaCare' }}
                                        @else
                                        {{ isset($c->referralSource)?$c->referralSource->source_name:'' }}<br>
                                        <small>{{ isset($c->referralCategory)?$c->referralCategory->category_name:'' }}</small>
                                        @endif
                                    </td>
                                    <td>
                                        {{ $c->patient->first_name.' '.$c->patient->last_name }}<br>
                                        <small>Enquirer: {{ $c->patient->enquirer_name }}</small>
                                    </td>
                                    <td>
                                        {{ $c->patient->contact_number }}<br>
                                        <small>Enquirer: {{ $c->patient->alternate_number }}</small>
                                    </td>
                                    <td>
                                        {{ $c->patient->email ?? '-' }}
                                    </td>
                                    <td>
                                        {{ $c->patient->city ?? '-' }}<br>
                                        <small>{{ $c->patient->area ?? '-' }}</small>
                                    </td>
                                    <td>
                                        @if($c->aggregator_lead && $c->status == 'Pending')
                                            {{ $c->aggregator_service }}
                                        @else
                                            {{ Helper::getServiceName($c->service_required) }}
                                        @endif
                                    </td>
                                    <td>{{ $c->status ?? '-' }}</td>
                                    <td>
                                        @ability('admin', 'leads-edit')
                                        <a href="{{ route('lead.view',Helper::encryptor('encrypt',$c->id)) }}" class="btn btn-md bg-red" data-id="{{ Helper::encryptor('encrypt',$c->id) }}" title="View">View</a>
                                        @endability
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        @if(isset($leads) && count($leads->whereIn('status',['No Supply'])))
                            @foreach($leads->whereIn('status',['No Supply']) as $i => $c)
                                <tr data-id="{{ Helper::encryptor('encrypt',$c->id) }}" @if($c->aggregator_lead){!! 'style="background:rgba(8, 131, 229, 0.15)"' !!}@endif>
                                    <td>
                                        <span class="label bg-black">{{ $c->status }}</span>
                                        @if($c->registration_amount > 0)
                                        <span class="label bg-deep-purple">Registered</span>
                                        @endif<br><br>
                                        {{ $c->created_at->format('d-m-Y') }}
                                        <small>{{ $c->created_at->format('h:i A') }}</small>
                                    </td>
                                    <td>
                                        @if($c->aggregator_lead)
                                            {{ 'ApnaCare' }}
                                        @else
                                        {{ isset($c->referralSource)?$c->referralSource->source_name:'' }}<br>
                                        <small>{{ isset($c->referralCategory)?$c->referralCategory->category_name:'' }}</small>
                                        @endif
                                    </td>
                                    <td>
                                        {{ $c->patient->first_name.' '.$c->patient->last_name }}<br>
                                        <small>Enquirer: {{ $c->patient->enquirer_name }}</small>
                                    </td>
                                    <td>
                                        {{ $c->patient->contact_number }}<br>
                                        <small>Enquirer: {{ $c->patient->alternate_number }}</small>
                                    </td>
                                    <td>
                                        {{ $c->patient->email ?? '-' }}
                                    </td>
                                    <td>
                                        {{ $c->patient->city ?? '-' }}<br>
                                        <small>{{ $c->patient->area ?? '-' }}</small>
                                    </td>
                                    <td>
                                        @if($c->aggregator_lead && $c->status == 'Pending')
                                            {{ $c->aggregator_service }}
                                        @else
                                            {{ Helper::getServiceName($c->service_required) }}
                                        @endif
                                    </td>
                                    <td>{{ $c->status ?? '-' }}</td>
                                    <td>
                                        <a href="{{ route('lead.view',Helper::encryptor('encrypt',$c->id)) }}" class="btn btn-md bg-black" data-id="{{ Helper::encryptor('encrypt',$c->id) }}" title="View Lead">View</a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="modal fade" id="viewAggregatorLeadModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content" >
                <div class="modal-header bg-blue">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="largeModalLabel">Lead Details</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <tr>
                                <th>Patient Name</th>
                                <td class="aggLead_patient"></td>
                            </tr>
                            <tr>
                                <th>Age, Gender &amp; Weight</th>
                                <td class="aggLead_gender_age_weight"></td>
                            </tr>
                            <tr>
                                <th>Location</th>
                                <td class="aggLead_location"></td>
                            </tr>
                            <tr>
                                <th>Case Description</th>
                                <td class="aggLead_case"></td>
                            </tr>
                            <tr>
                                <th>Medical Condition</th>
                                <td class="aggLead_condition"></td>
                            </tr>
                            <tr>
                                <th>Medications</th>
                                <td class="aggLead_medication"></td>
                            </tr>
                            <tr>
                                <th>Surgeries Undergone/Continuing</th>
                                <td class="aggLead_procedures"></td>
                            </tr>
                            <tr>
                                <th>Service Required</th>
                                <td class="aggLead_service"></td>
                            </tr>
                            <tr>
                                <th>Language Preference</th>
                                <td class="aggLead_lang_pref"></td>
                            </tr>
                            <tr>
                                <th>Gender Preference</th>
                                <td class="aggLead_gender_pref"></td>
                            </tr>
                            <tr>
                                <th>Rate Agreed</th>
                                <td class="aggLead_rate"></td>
                            </tr>
                            <tr>
                                <th>Rate Negotiable</th>
                                <td class="aggLead_negotiable"></td>
                            </tr>
                        </table>
                        <form action="{{ route('lead.aggregator-lead-status') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="lead_id" value="" />
                            <input type="hidden" name="patient_id" value="" />
                            <div class="col-sm-12 text-center">
                                <div class="col-sm-6 text-center">
                                    <button type="submit" onclick="javascript: return confirm('Are you sure?')" name="leadStatus" value="1" class="btn btn-block btn-success">Accept, I have staff</button>
                                </div>
                                <div class="col-sm-6 text-center">
                                    <button type="submit" onclick="javascript: return confirm('Are you sure?')" name="leadStatus" value="0" class="btn btn-block btn-warning">Decline, I do not have staff</button>
                                </div>
                            </div><br><br>
                        </form>
                        <code style="font-size: 10px;line-height:1">
                            <i class="material-icons" style="font-size:16px">info_outline</i></small> By accepting the case, you acknowledge the availability of staff to render the service to the patient.
                        </code>
                    </div>
                    <div class="clearfix"></div><br>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="allocationsModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-col-blue" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title" id="largeModalLabel">Required Allocations List</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive" style="margin-bottom: 0">
                        <table class="table table-bordered table-condensed table-hover distributionsTable" style="margin-bottom: 0">
                            <thead>
                                <tr>
                                    <th width="3%">#</th>
                                    <th>Patient</th>
                                    <th>Enquirer</th>
                                    <th>Phone</th>
                                    <th>Status</th>
                                    <th>Link</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(isset($allocationArray) && count($allocationArray))
                                @foreach($allocationArray as $i => $a)
                                    <tr>
                                        <td>{{ $i + 1 }}</td>
                                        <td>{{ $a['patient'] }}</td>
                                        <td>{{ $a['enquirer'] }}</td>
                                        <td>{{ $a['phone'] }}</td>
                                        <td>{{ $a['status'] }}</td>
                                        <td><a rel="noreferrer noopener" target="_blank" href="{{ route('lead.view',$a['id']) }}" class="btn btn-success">View</a></td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="text-center">
                                        No Required Allocation(s) found.
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div><br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="followupsModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-col-blue" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title" id="largeModalLabel">Required Follow Up List</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive" style="margin-bottom: 0">
                        <table class="table table-bordered table-condensed table-hover followupsTable" style="margin-bottom: 0">
                            <thead>
                                <tr>
                                    <th width="3%">#</th>
                                    <th>Patient</th>
                                    <th>Enquirer</th>
                                    <th>Date to follow up</th>
                                    <th>Time</th>
                                    <th>Comment</th>
                                    <th>Phone</th>
                                    <th>Link</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(isset($followupArray) && count($followupArray))
                                @foreach($followupArray as $i => $f)
                                    <tr>
                                        <td>{{ $i + 1 }}</td>
                                        <td>{{ $f['patient'] }}</td>
                                        <td>{{ $f['enquirer'] }}</td>
                                        <td>{{ $f['date'] }}</td>
                                        <td>{{ $f['time'] }}</td>
                                        <td>{{ $f['comment'] }}</td>
                                        <td>{{ $f['phone'] }}</td>
                                        <td><a rel="noreferrer noopener" target="_blank" href="{{ route('lead.view',$f['id']) }}" class="btn btn-success">View</a></td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="8" class="text-center">
                                        No Required Follow Up(s) found.
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div><br>
                </div>
                <div class="modal-footer" style="border-top: 1px solid #ccc">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
    @endability
@endsection

@section('page.scripts')
    @parent
    <script type="text/javascript">
		$(function(){
			$('.content').on('click','.clearLocalStore', function() {
			    return localStorage.removeItem('leadListLastTab');
			});

			// for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
		    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		        // save the latest tab; use cookies if you like 'em better:
		        localStorage.setItem('leadListLastTab', $(this).attr('href'));
		    });

		    // go to the latest tab, if it exists:
		    var lastTab = localStorage.getItem('leadListLastTab');
		    if (lastTab) {
		        $('[href="' + lastTab + '"]').tab('show');
		    }
		});
	</script>
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script>
    $(function(){
        @if(isset($leads) || isset($cases))
        var table = $('#leadsTable').DataTable({
            "order": [[ 0, 'desc' ]],
            "displayLength": 25,
            "drawCallback": function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null;

                api.column(1, {page:'current'} ).data().each( function ( group, i ) {
                    if(group != null){
                        if ( last !== group.substring(0, 10) ) {
                            $(rows).eq( i ).before(
                                '<tr class="group"><td colspan="10"><b>'+group.substring(0, 10)+'</b></td></tr>'
                            );

                            last = group.substring(0, 10);
                        }
                    }
                } );
            }
        });

        // Order by the grouping
        $('#leadsTable tbody').on( 'click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
                table.order( [ 1, 'desc' ] ).draw();
            }
            else {
                table.order( [ 1, 'asc' ] ).draw();
            }
        });
        @endif

        $('.content').on('click','input[name=filter_status]',function(){
            var searchString = "";
            // Get all checked status
            $('input[name=filter_status]:checked').each(function(i){
                searchString += $(this).val() + "|";
            });

            if(searchString.length > 0 && typeof table != 'undefined'){
                searchString = searchString.substring(0, searchString.length-1);
                table.search(searchString, true, false).draw();
            }else{
                table.search("").draw();
            }
        });

        $('.content').on('click','.btnViewLead',function(){
            var id = $(this).data('id');
            var patientID = $(this).data('patient-id');
            var patient = $(this).data('patient');
            var data = $(this).data('case');

            modal = '#viewAggregatorLeadModal';

            $(modal+' input[name=lead_id]').val(id);
            $(modal+' input[name=patient_id]').val(patientID);
            $(modal+' .aggLead_patient').html(patient.first_name+' '+patient.last_name);
            $(modal+' .aggLead_gender_age_weight').html(patient.patient_age+', '+patient.gender+', '+patient.patient_weight);
            $(modal+' .aggLead_location').html(patient.area+', '+patient.city);
            $(modal+' .aggLead_case').html(data.case_description);
            $(modal+' .aggLead_condition').html(data.medical_conditions);
            $(modal+' .aggLead_medication').html(data.medications);
            $(modal+' .aggLead_procedures').html(data.procedures);
            $(modal+' .aggLead_service').html(data.aggregator_service);
            $(modal+' .aggLead_lang_pref').html(data.language_preference);
            $(modal+' .aggLead_gender_pref').html(data.gender_preference);
            $(modal+' .aggLead_rate').html(data.aggregator_rate);
            $(modal+' .aggLead_negotiable').html(data.aggregator_rate_negotiable);

            $(modal).modal();
        });

        $('.content').on('click','.btnLeadType', function(){
            $('#leadTypeModal').modal();
            $('#leadTypeModal .btnOtherRequest').attr('data-id',$(this).data('id'));

            $('#leadTypeModal .openMedicalCase').attr('data-id',$(this).data('id'));
            $('#leadTypeModal .openMedicalCase').attr('href','');
            url = $('#leadTypeModal .openMedicalCase').data('href');
            url += '?ref='+$(this).data('id');
            $('#leadTypeModal .openMedicalCase').attr('href',url);
        });


        $('.content').on('click','.btnOtherRequest', function(){
            $('#leadTypeModal').modal('hide');
            id = $(this).attr('data-id');
            block = '#leadsTable tbody tr[data-id='+id+']';

            $('#otherRequestForm input[name=name]').val($(block+' td:eq(3)').html().replace(/<\/?[^>]+(>|$)/g, "").trim());
            $('#otherRequestForm input[name=phone_number]').val($(block+' td:eq(4)').html().replace(/<\/?[^>]+(>|$)/g, "").trim());
            $('#otherRequestForm input[name=email]').val($(block+' td:eq(5)').html().replace(/<\/?[^>]+(>|$)/g, "").trim());
            $('#otherRequestForm input[name=city]').val(removeTags($(block+' td:eq(6)').text()));

            // console.log($(block+' td:eq(6)').html()+"  :: "+removeTags($(block+' td:eq(6)').text()));

            $('#otherRequestForm #lead_id').val(id);

            $('#otherRequestsModal').modal();
        });

        $('.content').on('click', '.markLeadAsChecked', function(){
            id = $(this).data('id');
            if(confirm("Are you sure?")){
                $.ajax({
                    url: '{{ route('ajax.mark-lead-as-checked') }}',
                    type: 'POST',
                    data: {_token:'{{ csrf_token() }}',id: id},
                    success: function(data){
                        console.log(data);
                        $('#leadsTable tbody tr[data-id='+id+']')
                        .children('td, th')
                        .animate({ padding: 0 })
                        .wrapInner('<div />')
                        .children()
                        .slideUp(function() { $(this).closest('tr').remove(); });

                        //toastr.success("Lead Status has been updated");
                        alert("Lead Status has been updated");
                    },
                    error: function(err){
                        console.log(err);
                    }
                });
            }
        });
    });

    function removeTags(txt){
        var rex = /(<([^>]+)>)/ig;
        //return txt.replace(rex , ",").replace(/\s/g, '').replace(/,\s*$/, '');
        return txt.replace(/\s+/g,' ')
        .replace(/^\s+|\s+$/,'');
    }

    </script>
@endsection
