@extends('layouts.main-layout')

@section('page_title','Dashboard - ')

@section('active_home','active')

@section('page.styles')
<style>
    .theme-blue{
        /*background-color: #2196f3 !important;*/
        background-color: #85909b !important; /* #616f7d !important;*/
    }
    .card .header{
        padding: 12px 20px;
    }
    .dashboard-stat-list > li > span > b {
        font-size: 19px;
    }
    .dashboard-stat-list > li{
        padding: 14px 0 0 0 !important;
    }
    .dashboard-icon {
        width: 100px;
        height: 100px;
        background: #fff;
        border: 3px solid #2196F3 !important;
    }
    .dashboard-icon i.material-icons {
        font-size: 45px !important;
    }
    .dashboard-text{
        margin-top: 10px;
        font-size: 16px;
        text-align: center;
        font-weight: 500;
    }
    .dashboard-block{
        text-decoration: none !important;
    }
    .dashboard-block .material-icons{
        color: #333;
    }
    .dashboard-block button{
        text-align: center;
        margin: 0 auto !important;
    }
</style>
@endsection

@section('content')
    @if(Helper::encryptor('decrypt',session('utype')) == 'Aggregator')
        @include('partials.dashboard.aggregator')
    @else
        @include('partials.dashboard.provider')
    @endif
@endsection


@section('page.scripts')
    <script>
        $(function(){
            @if(Helper::encryptor('decrypt',session('utype')) == 'Aggregator')
            $('a').click(function() {
                if($(this).parent().hasClass('col-lg-12')){
                    $(this).attr('target', '_blank');
                }
            });
            @endif
        });
    </script>
@endsection
