@extends('layouts.main-layout')

@section('page_title','Call Report - ')

@section('active_reports','active')

@section('page.styles')
<style>
</style>
@endsection

@section('plugin.styles')
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
@endsection

@section('content')
<div class="row clearfix">
    <div style="display: block; font-size: 16px;text-align:center;">Here is a completed list of calls and cases that were received assigned today.</div><br>

    <style>
        table.tbl {
            border-collapse: collapse;
            width: auto;
            /* min-width: 64%; */
            /*max-width: 50%;*/
            /* margin: 0 auto; */
        }

        table.tbl, table.tbl th, table.tbl td {
            border: 1px solid black;
        }

        table.tbl th{
            width: 25%;
        }

        table.tbl th, table.tbl td{
            height: 20px;
            vertical-align: middle;
            padding: 8px 10px;
            text-align: left;
        }

        table.tbl th{
            background-color: #f2f2f2;
            width: 25%;
        }

    </style>

    <div class="col-md-12 clearfix">

        <form method="post" action="{{ route('call.reports') }}">

            <div class="col-md-2" style="margin-left: 30%">

                <input type="text" class="min" name="start_date" placeholder="From Date"><br>

            </div>

            <div class="col-md-2">

                <input type="text" class="max" name="end_date" placeholder="To Date">

            </div>

            <div class="col-md-2">

                <input type="submit" class="btn btn-primary" value="Filter"/>

            </div>

        </form>

    </div>

    <div class="clearfix"></div><br>
    <div class="col-md-6">
        <table class="tbl">
            <tr>
                <th colspan="6" style="text-align: center"><u>Leads</u></th>
            </tr>
            <tr>
                <th>Total Leads</th>
                <td colspan="5">{{ $data['leadstotal'] }}</td>
            </tr>
            <tr>
                <th>Leads From Website</th>
                <td colspan="5">{{ $data['leadswebsite'] }}</td>
            </tr>
            <tr>
                <th>Leads From Exotel</th>
                <td colspan="5">{{ $data['leadsexotel'] }}</td>
            </tr>
            <tr>
                <th>Leads From Referral</th>
                <td colspan="5">{{ $data['leadsexotel'] }}</td>
            </tr>
            <tr>
                <th colspan="6" style="text-align: center"><u>Non Productive Calls</u></th>
            </tr>
            <tr>
                <th>Leads for Jobs</th>
                <td colspan="5">{{ $data['leadsRequestsjob'] }}</td>
            </tr>
            <tr>
                <th>Leads for Partnerships</th>
                <td colspan="5">{{ $data['leadsRequestspartner'] }}</td>
            </tr>
            <tr>
                <th>Duplicate Leads</th>
                <td colspan="5">{{ $data['leadsRequestspartner'] }}</td>
            </tr>
            <tr>
                <th colspan="6" style="text-align: center"><u>Assigned Cases to Particular Caregiver Type</u></th>
            </tr>
            @foreach($result as $i => $r)
            <tr>
                <th><b>{{ $r['service_name'] }}</b></th>
                @if( $r['count'] == "0")
                <td  colspan="5" style="color: red;"><b>{{ $r['count'] }}</b></td>
                @else
                <td colspan="5" ><b>{{ $r['count'] }}</b></td>
                @endif
            </tr>
            @endforeach
            <tr>
                <th colspan="6" style="text-align: center"><u>MCM's Call Handling</u></th>
            </tr>
            <tr>
                <th>Name</th>
                <th>Leads</th>
                <th>Pending</th>
                <th>Ongoing</th>
                <th>Completed</th>
                <th>Follow up with SP</th>
            </tr>
        @if(isset($mcm))
            @foreach($mcm as $m)
            <tr>
                <td>{{ $m->full_name }}</td>
                <td><center>{{ $m->pending + $m->ongoing + $m->completed + $m->followup }}</center></td>
                <td><center>{{ $m->pending }}</center></td>
                <td><center>{{ $m->ongoing }}</center></td>
                <td><center>{{ $m->completed }}</center></td>
                <td><center>{{ $m->followup }}</center></td>
            </tr>
            @endforeach
        @endif            
        </table>
    </div>
    <div class="col-md-6">
        <table class="tbl">
            <tr>
                <th colspan="3" style="text-align: center"><u>Leads Handled by MCM City Wise</u></th>
            </tr>
            <tr>
                <th style="text-align: center"><u>City</u></th>
                <th style="text-align: center"><u>Received</u></th>
                <th style="text-align: center"><u>Converted</u></th>
            </tr>
            @if(isset($leadscity))
            @foreach($leadscity as $l)
            <tr>
                <th>{{ $l->city }}</th>
                <td>{{ $l->received }}</td>
                <td>{{ $l->converted }}</td>
            </tr>
            @endforeach
            @endif
            <tr>
                <th colspan="3" style="text-align: center"><u>City wise Requirement</u></th>
            </tr>
            <tr>
                <th rowspan="13">Bangalore</th>
                <tr>
                    <td>Doctor Visit</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>Nurse Visit</td>
                    <td>5</td>
                </tr>
                <tr>
                    <td>Physiotherapy</td>
                    <td>5</td>
                </tr>
                <tr>
                    <td>Old age home</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Short term nurse</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Short term nurse Assistance</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Long term nurse</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Long term nurse Assistance</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Counsellor</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Ambulance</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Labtesting</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Others</td>
                    <td>5</td>
                </tr>
            </tr>
            <tr>
                <th rowspan="13">Chennai</th>
                <tr>
                    <td>Doctor Visit</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>Nurse Visit</td>
                    <td>5</td>
                </tr>
                <tr>
                    <td>Physiotherapy</td>
                    <td>5</td>
                </tr>
                <tr>
                    <td>Old age home</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Short term nurse</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Short term nurse Assistance</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Long term nurse</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Long term nurse Assistance</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Counsellor</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Ambulance</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Labtesting</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Others</td>
                    <td>5</td>
                </tr>
            </tr>
            <tr>
                <th rowspan="13">Hyderabad</th>
                <tr>
                    <td>Doctor Visit</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>Nurse Visit</td>
                    <td>5</td>
                </tr>
                <tr>
                    <td>Physiotherapy</td>
                    <td>5</td>
                </tr>
                <tr>
                    <td>Old age home</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Short term nurse</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Short term nurse Assistance</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Long term nurse</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Long term nurse Assistance</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Counsellor</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Ambulance</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Labtesting</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Others</td>
                    <td>5</td>
                </tr>
            </tr>
            <tr>
                <th rowspan="13">Mumbai</th>
                <tr>
                    <td>Doctor Visit</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>Nurse Visit</td>
                    <td>5</td>
                </tr>
                <tr>
                    <td>Physiotherapy</td>
                    <td>5</td>
                </tr>
                <tr>
                    <td>Old age home</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Short term nurse</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Short term nurse Assistance</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Long term nurse</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Long term nurse Assistance</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Counsellor</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Ambulance</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Labtesting</td>
                    <td>5</td>
                </tr>                
                <tr>
                    <td>Others</td>
                    <td>5</td>
                </tr>
            </tr>
        </table>
    </div>
    <br>
</div>
@endsection

@section('plugin.scripts')
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>

<script>

    $(function(){

        $('.min').bootstrapMaterialDatePicker({

            format: 'DD-MM-YYYY',

            clearButton: true,

            time: false,

            maxDate : new Date(),

        });

        $('.max').bootstrapMaterialDatePicker({

            format: 'DD-MM-YYYY',

            clearButton: true,

            time: false,

            maxDate : new Date(),

        });

    });

</script>

@endsection
