<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Smart Health Connect</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                /*background-color: #fff;*/
                /*color: #636b6f;*/
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 7.2vw;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .warn{
                color: #636b6f;
                font-family: sans-serif !important;
                padding: 5px;
                font-weight: bolder;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="col-sm-12 content">
                <div>
                    <img src="{{ asset('img/flaticons/workspace.png') }}" style="height: 200px;width: 200px;"> 
                </div>
                <div class="title m-b-md">
                    Smart Health Connect
                </div>
                <div class="warn">You are currently trying to access the portal from a mobile device. Please switch on to a desktop/laptop version for proper accessibility and optimization.</div>
            </div>
        </div>
    </body>
</html>
