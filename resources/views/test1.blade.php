<html>
<head>
    <title>Print Invoice</title>
    <link href="{{ ViewHelper::ThemePlugin('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/print.css') }}" media="print" />
    <style>
        img.img-topbar{ margin-top: -5px !important;}
        .username:after{
            content: ' \25BE';
            padding-left: 10px;
            font-size: 20px;
        }
        .v-middle{vertical-align:middle !important;}
        body{
            font-size: 12px !important;
            background: rgb(204,204,204);
        }
        @page{
            size: A4;   /* auto is the initial value */
            margin: 0mm;  /* this affects the margin in the printer settings */
        }
        div.A4 {
            background: white;
            @if(Request::segment(3) == 'print')
            width: 21cm;
            height: auto;
            box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
            @else
            width: 793.707px;
            height: 1122.52px;
            box-shadow: 0 0 5px rgba(0,0,0,0.5);
            @endif
            display: block;
            margin: 0 auto;
        }
        .border-top{border-top: 1px solid #333 !important;}
        .border-bottom{border-bottom: 1px solid #333 !important;}
        .border-right{border-right: 1px solid #333 !important;}
        .border-left{border-left: 1px solid #333 !important;}
        .no-padding, .no-padding tbody tr td, .no-padding tbody tr th{
          padding: 0 !important;
          border: 1px solid #333;
        }
        .no-border, .no-border tbody tr td, .no-border tbody tr th{border: 0;}
        .services-table, .services-table thead tr th, .services-table tfoot tr th, .services-table tfoot tr td, .services-table  tbody tr th,.services-table tbody tr td{
            padding: 1px !important;
            border: 0;
        }
        .tbl-text{
            margin-top:1px !important;
            padding:2px !important;
            margin-bottom:1px !important;
        }
        h6{font-weight: bold;}
        .user-info{ background: rgba(96, 125, 139, 0.10) !important; border-bottom: 0.1em solid #3F51B5 !important; padding: 10px !important;}
    </style>
</head>
<body onload="javascript: window.print();">
    <div class="A4">
        <div class="col-lg-12 invoicereport" style="border: 0px solid black;background: white;margin: 15 0 0 0;">
            <br>
            <table class="table table-bordered no-padding" style="margin-bottom: 0 !important">
                <!-- Heading -->
                <tr>
                    <th colspan="3" class="text-center">
                        <h6 style="font-size:20px; margin-top:2px !important;margin-bottom:2px !important; padding:0px !important">INVOICE</h6>
                    </th>
                </tr>
                <!-- Logo, address and payment mode information -->
                <tr>
                    <td width="44%">
                        <table class="no-border" style="margin-bottom:0 !important">
                            <tr>
                                <td style="vertical-align: top !important;border: 0 !important">
                                    <center><img src="{{ asset('uploads/provider/'.Helper::encryptor('encrypt',$profile->tenant_id).'/'.$profile->organization_logo) }}" class="org-img" style="width: 60px;margin-top: 15px;clear:both;border-right:0 !important"></center>
                                </td>
                                <td style="border:0 !important">
                                    <div style="width:98%; display:inline-block; padding:5px; margin-left:5px; font-size:12px; padding-top:10px; border-left:0 !important; vertical-align:top !important; font-size:12px !important">
                                        <span style="font-size: 12px !important;font-weight: bold">BVRM</span><br>
                                        
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td width="21.5%" style="border-left: 0 !important">
                        <table class="table no-border no-padding no-bottom" style="margin-bottom: 5px !important">
                            <tr>
                                <td class="no-bottom" style="border:0 !important;border-top: 1px solid #333 !important">
                                    <h6 style="margin-top:2px !important;padding:5px !important;margin-bottom: 2px !important">MODE OF PAYMENT</h6>
                                    <div class="col-md-12" style="margin-top:2px !important;margin-left:0px;padding-left:5px !important;border: 0 !important">
                                        <table class="no-border" border="0" style="border: 0 !important;font-size: 12px !important;">
                                            <tr>
                                                <td>Cheque</td>
                                                <td style="padding-left: 20px !important;">DD</td>
                                            </tr>
                                            <tr>
                                                <td>Bank Transfer</td>
                                                <td style="padding-left: 20px !important;">Cash</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">Other</td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td style="border-left: 0 !important">
                        <table class="table no-border no-padding" style="margin-bottom: 5px !important">
                            <tr style="height:44px !important;">
                                <td class="border-bottom border-right">
                                    <h6 style="margin-top:2px !important; margin-bottom:2px !important; padding:5px !important;">INVOICE DATE</h6>
                                    <span style="font-size: 12px; padding:5px !important;">55-55-2020</span>
                                </td>
                                <td>
                                  <h6 style="margin-top:2px !important; margin-bottom:2px !important; padding:5px !important;">INVOICE NO.</h6>
                                    <span style="font-size: 12px; padding:5px !important;">55</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="padding: 5px !important;font-size: 12px !important;border-top: 1px solid #333 !important">
                                    Immediate by cheque / DD favouring<br>
                                    <h6 style="margin-top:2px !important;padding:5px !important">55</h6>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- Customer and bank information -->
                <tr>
                    <td width="40%" style="border: 0 !important">
                        <div style="padding: 1px 4px !important;margin-bottom: 0px !important;vertical-align: top !important">
                            <h6 style="font-size:13px;margin-top:2px !important;margin-bottom: 1px !important;">Patient Name: Satish</h6>
                            <div style="font-weight: normal !important;font-size: 12px !important;margin-bottom: 2px !important;font-style: italic">Patient ID: 55</div>
                            <address style="font-size: 12px !important;font-weight:500;margin-bottom: 0px !important;vertical-align: top !important">
                                
                                Phone: 753285732837<br>
                                Email: satish2gmail.com
                            </address>
                        </div>
                    </td>

                    <td colspan="2" style="border: 0 !important;">
                        <table class="table no-padding " style="border: 0 !important;margin-bottom: 0px !important;font-size: 11px !important;min-height: 123px">
                            <tr>
                                <th width="38.4%" class="border-right border-bottom" style="border-top: 0 !important"><h6 class="tbl-text" style="font-size: 11px !important;">BANK NAME</h6></th>
                                <td class="border-bottom" style="padding-left: 5px !important;border-top: 0 !important;border-right: 0 !important">{{ Helper::getSetting('bank_name') }}</td>
                            </tr>
                            <tr>
                                <th class="border-right border-bottom"><h6 class="tbl-text" style="font-size: 11px !important;">ADDRESS</h6></th>
                                <td class="border-bottom" style="padding-left: 5px !important;padding-top:2px !important">{{ Helper::getSetting('bank_address') }}</td>
                            </tr>
                            <tr>
                                <th class="border-right border-bottom"><h6 class="tbl-text" style="font-size: 11px !important;">BANK A/C NO</h6></th>
                                <td class="border-bottom" style="padding-left: 5px !important;padding-top:2px !important">{{ Helper::getSetting('account_number') }}</td>
                            </tr>
                            <tr>
                                <th class="border-right" style="border-bottom: 0 !important"><h6 class="tbl-text" style="font-size: 11px !important;">IFSC CODE</h6></th>
                                <td style="padding-left: 5px !important;padding-top:2px !important;border-bottom: 0 !important">{{ Helper::getSetting('ifsc_code') }}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <table class="table services-table" style="font-size:12px !important; margin-top:-1px !important; margin-bottom:0px !important">
                <thead>
                    <tr>
                        <th class="border-left border-top border-right border-bottom v-middle" style="font-size:10px!important; padding-left:6px!important;">DATE</th>
                        <th class="border-top border-right border-bottom v-middle" style="font-size:10px !important; padding-left:6px!important;">DESCRIPTION</th>
                        <th class="border-top border-right border-bottom v-middle" style="font-size:10px !important; padding-left:6px!important;">CATEGORY</th>
                        <th class="text-center border-top border-right border-bottom v-middle" style="font-size:10px !important">QTY</th>
                        <th class="text-right border-top border-right border-bottom v-middle" style="font-size:10px !important">DISCOUNT</th>
                        <th class="text-right border-top border-right border-bottom v-middle" style="font-size:10px !important">DEBIT</th>
                        <th class="text-right border-top border-right border-bottom v-middle" style="font-size:10px !important">CREDIT</th>
                        <th width="9%" class="text-right border-top border-right border-bottom v-middle" style="font-size:10px !important">TOTAL</th>
                    </tr>
                </thead>
                <tbody>
                   
                 
                     
                  

                  

                    
                </tbody>
                <tfoot>
                    <tr style="background:#ddd!important">
                        <th colspan="7" class="text-right border-left border-right" style="padding-right:10px!important;">Total</th>
                        <th class="text-right border-right border-top" style="padding-left:6px!important;">0.00</th>
                    </tr>
                   
                    <tr style="background:#ddd!important">
                        <th colspan="7" class="text-right border-left border-right" style="padding-right:10px!important;">Discount</th>
                        <th class="text-right border-right border-top" style="padding-left:6px!important;">2.00</th>
                    </tr>
                    <tr style="background:#ddd!important">
                        <th colspan="7" class="text-right border-left border-right" style="padding-right:10px!important;">Balance Due</th>
                     
                        <th class="text-right border-right border-top" style="padding-left:6px!important;">2.00</th>
                    </tr>
                </tfoot>
            </table>

            <table class="table" style="font-size:13px !important;margin-bottom: 2px !important;position:relative;bottom:0">
                <tr>
                    <td colspan="2" class="border-left border-top border-right">
                        BALANCE DUE IN WORDS:
                       
                        <b>Hyderabad</b><br>
                        <div class="col-lg-10">
                            <p style="color:#ed0505!important">*Late payment fee of 100 Rs per day will be applicable if the payment is done after 10th of the month.</p>
                        </div>
                        <div class="col-lg-2">
                            <h6 class="tbl-text text-right">E. &amp; O.E</h6><br>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="border-left border-top border-bottom border-right" width="50%">
                        <div style="font-size: 11px !important;padding-left:0 !important">PAN : Hyderabad</div>
                        <div style="font-size: 11px !important;padding-left:0 !important">GSTIN : Hyderabad</div>
                        <div style="font-size: 11px !important;padding-left:0 !important">CIN : {Hyderabad}}</div>
                       Hyderabad
                    </td>
                    <td class="no-padding border-left border-bottom border-right">
                        <h6 class="tbl-text text-center" style="font-size:13px; margin-top:5px !important; margin-right:1px !important;">for Hyderabad</h6>
                        <p class="text-center" style="margin-right:1px !important;">Tel No.: {!! !empty($profile->phone_number)?'+91-'.$profile->phone_number:'' !!}  {!! !empty($profile->landline_number)?'+91-'.$profile->landline_number:'' !!}</p>
                        <h6 class="sig" style="text-align:center; margin-top:10%; margin-bottom:0 !important; margin-right:1px !important">AUTHORISED SIGNATORY</h6>
                    </td>
                </tr>
            </table>
            <center>
                <b>SUBJECT TO Hyderabad JURISDICTION</b>
                <br>
                <span style="font-size:13px;margin-left:20px;">This is a computer generated invoice and does not require a signature.</span>
                <br>
            </center>
        </div>
    </div>
</body>
</html>
