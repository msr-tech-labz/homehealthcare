@extends('layouts.main-layout')

@section('page_title','Caregivers - Live Tracking |')

@section('active_trackings','active')

@section('plugin.styles')
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<style>
	#googleMap .loading{
		width: 100% !important;
		line-height: 15;
		text-align: center !important;
		font-size: 30px !important;
		font-weight: normal !important;
		color: #fff !important;
	}
</style>
@endsection

@section('content')
<div class="header clearfix">
	<h4 class="col-lg-6" style="padding: 0;color: #fff !important">
		Live Tracking - Caregiver
	</h4>
</div>
<div class="col-lg-12">
	<a href="{{ url('/tracking') }}" class="btn btn-primary waves-effect pull-left" style="margin-bottom: 5px;">Back</a>
	<button type="button" class="btn btn-success waves-effect pull-right" id="btnFilter" style="margin-bottom: 5px;">FILTER</button>
</div>
<br>
<div class="col-lg-12" id="googleMap" style="min-height:500px !important;border: 0.2em solid #00CFB9; position: relative;margin-bottom: 15px;">
	<div class="loading hide">Fetching tracking results. Please wait...</div>
</div>

<!-- Modal -->
<div id="employeeFilterModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Choose Caregiver</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-3">Caregiver</label>
					<div class="col-md-6">
						<select class="form-control empfilter" id="employeeID" data-live-search="true">
							<option value="0">-- Select Caregiver --</option>
							@forelse($caregivers as $e)
								<option data-evalue="{{ \Helper::encryptor('encrypt',$e->id) }}" value="{{ $e->id }}" data-firstname="{{ $e->first_name }}" data-lastname="{{ $e->last_name }}" data-mobile="{{ $e->mobile_number }}" data-gender="{{ $e->gender }}" data-city="{{ $e->current_city }}" data-picture="{{ $e->profile_image }}">{{ $e->first_name.' '.$e->last_name }}</option>
							@empty
							@endforelse
						</select>
					</div>
				</div>
				<div class="clearfix"></div><br>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
				<button type="button" id="selectEmployee" class="btn btn-success" data-dismiss="modal">OK</button>
			</div>
		</div>
		
	</div>
</div>
@endsection

@section('plugin.scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2-azeUdXbeAqQrvGtLZrbsXIFcofE_B8&libraries=places&region=IN"></script>
<script src="https://www.gstatic.com/firebasejs/3.3.0/firebase.js"></script>
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<script>
	var employeeName = '';
	var employeeMobile = '';
	var employeeGender = 'Male';
	var employeeCity = '';
	var employeePic = '';
	var isMapInitialized = false;

	var marker_male = '{{ url('/') }}/img/icons/male-2.png';
	var marker_female = '{{ url('/') }}/img/icons/female-2.png';

	$(function(){
		$('#btnFilter').on('click', function(){
			$('#employeeID').val('0');
			$('#employeeFilterModal').modal();
		});

		$('#selectEmployee').on('click', function(){
			var id = $('#employeeID option:selected').val();
			if(id > 0){
				$('#googleMap .loading').html('Fetching tracking results. Please wait...');
				$('#googleMap .loading').removeClass('hide');
				block = '#employeeID option:selected';
				employeeName = $(block).data('firstname') + ' ' + $(block).data('lastname');
				employeeGender = $(block).data('gender');
				employeeMobile = $(block).data('mobile');
				//employeeCity = $(block).data('city');
				if($(block).data('evalue') && $(block).data('picture')){
					employeePic = '{{ url('/')."/uploads/provider/".session('tenant_id')."/caregivers/" }}' + $(block).data('evalue') +'/'+ $(block).data('picture');
				}else{
					employeePic = '{{ url('/') }}/img/default-avatar.jpg';
				}

				getLiveTrackingByEmployee(id);
			}
		});
	});

	// Initialize Firebase
	var config       = {
		apiKey           : "AIzaSyCI2QbA1TINpRBxT-5VMCLmwX9nn_i8F5M",
		authDomain       : "gpstr-9f442.firebaseapp.com",
		databaseURL      : "https://gpstr-9f442.firebaseio.com",
		projectId        : "gpstr-9f442",
		storageBucket    : "gpstr-9f442.appspot.com",
		messagingSenderId: "847394590859"
	};
	firebase.initializeApp(config);

	// Get a reference to the database service
	var database = firebase.database();

	function getLiveTrackingByEmployee(id){
		if(id){
			var dbRef = database.ref().child('messages/'+{{ \Helper::getTenantID() }}+'_'+id);
			dbRef.on('value', function (snapshot) {
				var tdata = snapshot.val();
				console.log(tdata);
				if(tdata){
					for(var data in tdata){
						if (tdata.hasOwnProperty(data)) {
							for(var key in tdata[data]){
								//console.log(key + " -> " + tdata[data][key]);
								lat = tdata[data]['lat'];
								lng = tdata[data]['lng'];
								hours = tdata[data]['hours'];
								minutes = tdata[data]['minutes'];
								
								employeeCity = '';
								
								getAddress(lat, lng);
								
								if(!isMapInitialized)
									initMap();
								else
									redraw();
							}
						}
					}
				}else{
					$('#googleMap .loading').html('Could not find tracking results!');
				}
			});
		}
	}

	//writeUserData(1,'Nagesh H S',13.064156,77.5810103);
	function writeUserData(userId, name, lattitude, longitude) {
		firebase.database().ref('messages/' + userId).push({
			user: name,
			lat: lattitude,
			lng : longitude,
		});
	}

</script>
<script>
	var map;
	var map_marker;
	var lat = 23.054527;
	var lng = 78.152322;
	var hours = '';
	var minutes = '';
	var lineCoordinatesArray = [];

	function initMap(){
		getAddress();
		lat = parseFloat(lat);
		lng = parseFloat(lng);
		map = new google.maps.Map(document.getElementById('googleMap'), {
			zoom: 14,
			center: {lat: lat, lng : lng, alt: 0},
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			disableDefaultUI:   true,
			overviewMapControl: false,
			streetViewControl:  false,
			scaleControl:       true,
			mapTypeControl:     false,
			panControl:         true,
			panControlOptions:{
				position: google.maps.ControlPosition.TOP_RIGHT
			},
			zoomControl: true,
			zoomControlOptions: {
				style: google.maps.ZoomControlStyle.LARGE,
				position: google.maps.ControlPosition.TOP_RIGHT
			}
		});

		image = employeeGender=='Male'?marker_male:marker_female;

		map_marker = new google.maps.Marker({position: {lat: lat, lng: lng}, map: map, icon: image});
		map_marker.setMap(map);
		
		isMapInitialized = true;
	}

	$('#employeeFilterModal').modal();

	function redraw() {
		//var image = 'https://lh3.ggpht.com/XAjhu-6znztoLTr9AxuwM5v0wilaKiUJJMLKEiiFMn6lGOmBmY1Km7Kt1ohildzlIdWgkwy_5g=w9-h9';
		image = employeeGender=='Male'?marker_male:marker_female;
		lat = parseFloat(lat);
		lng = parseFloat(lng);
		// Clear Markers
		map_marker.setMap(null);

		map_marker = new google.maps.Marker({
			position: {lat: lat, lng : lng},
			map: map,
			draggable: false,
			icon: image
		});
		lat = parseFloat(lat);
		lng = parseFloat(lng);

		map.setCenter({lat: lat, lng : lng, alt: 0});
		map_marker.setPosition({lat: lat, lng : lng, alt: 0});
	}

	var infowindow = new google.maps.InfoWindow();
	
	function addInfoWindow(){
		console.log("addInfoWindow Called");
		
		var timestampString = '';
		if(hours != '' && minutes != ''){
		  timestampString = hours + ":" + minutes;
		}else{
		  timestampString = '-';
	  	}
		var contentString = '<div id="content">'+
	      '<div id="bodyContent">'+
	      '<p><img src="'+employeePic+'" style="max-width: 100px; float:left; margin-right: 10px;border: 1px solid #ccc; padding: 2px; border-radius: 2px;"/>' +
		  '<b style="font-size: 22px; font-weight: normal; color: #00b29c;">'+employeeName+'</b><br>' +
	      '<b style="line-height: 30px;">'+employeeMobile+'</b><br>'+
		  '<b style="line-height: 20px;">'+ employeeCity +'</b><br><br>'+
	      '<span style="line-height: 20px;">(Last seen at '+timestampString+')</span></p>'+
	      '</div>'+
	      '</div>';

		  map_marker.content = contentString;

		  google.maps.event.addListener(map_marker, 'click', function() {
			  infowindow.setContent(this.content);
			  infowindow.open(map, this);
			  setTimeout(function () { infowindow.close(); }, 5000);
          });
	}

	google.maps.event.addListener(infowindow,'closeclick',function(){
	   infowindow.close();
	});

	function getNonZeroRandomNumber(){
		return Math.floor(Math.random() * 201) - 1;
	}
	
	function getAddress(lat, lng){
		lat = parseFloat(lat);
		lng = parseFloat(lng);
		var latlng = new google.maps.LatLng(lat, lng);
		var geocoder = geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'latLng': latlng }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    employeeCity = results[1].formatted_address;
                }
            }
			addInfoWindow();
        });
	}
</script>
@endsection
