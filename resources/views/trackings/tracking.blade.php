@extends('layouts.main-layout')

@section('page_title','Caregivers - Tracking |')

@section('active_trackings','active')

@section('plugin.styles')
	<style>
	</style>
@endsection

@section('content')
<div class="header clearfix">
	<h4 class="col-lg-6" style="padding: 0;color: #fff !important">
		Employee Tracking
	</h4>
	<div class="col-sm-6 text-right" style="padding: 0;">
		<a href="{{ route('tracking.live-tracking') }}" class="btn-group" role="group">
			<button type="button" class="btn btn-success waves-effect"><i class="material-icons">map</i></button>
			<button type="button" class="btn btn-success waves-effect" style="line-height: 1.9;padding-left:0">LIVE TRACKING</button>
		</a>&nbsp;&nbsp;
	</div>
</div>
<button type="button" class="btn btn-success waves-effect" id="btnFilter" style="margin-bottom: 5px;">FILTER</button>
<br>
<div class="col-lg-12" id="googleMap" style="min-height:500px !important;border: 0.2em solid #00CFB9; position: relative;margin-bottom: 15px;">
</div>


<!-- Modal -->
<div id="employeeFilterModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Choose City</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-3">City</label>
					<div class="col-md-6">
						<select class="form-control" id="cityName">
							<option value="">-- Select City --</option>
							{{-- Get City From Branches --}}
						</select>
					</div>
				</div>
				<div class="clearfix"></div><br>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
				<button type="button" id="selectCity" class="btn btn-success" data-dismiss="modal">OK</button>
			</div>
		</div>
	</div>
</div>
@endsection

@section('plugin.scripts')
<script src="https://www.gstatic.com/firebasejs/3.3.0/firebase.js"></script>
<script>
	var isMapInitialized = false;
	// Initialize Firebase
	// TODO: Replace with your project's customized code snippet
	var config = {
		apiKey: "AIzaSyC6sEpFSvRvPxT-C1rfiD4vxJIUr6Drjqk",
		authDomain: "apnacare-portal.firebaseapp.com",
		databaseURL: "https://apnacare-portal.firebaseio.com",
		storageBucket: "apnacare-portal.appspot.com",
	};
	firebase.initializeApp(config);

	// Get a reference to the database service
    var database = firebase.database();

    var map;
    var map_marker;
	var lat = 23.054527;
	var lng = 78.152322;
    var lineCoordinatesArray = [];

    var trackingRef = database.ref('tracking/' + 197);

    var dbRef = database.ref().child('tracking/197');
    dbRef.on('value', function (snapshot) {
        var tdata = snapshot.val();
		console.log(tdata);
        for(var data in tdata){
            if (tdata.hasOwnProperty(data)) {
                for(var key in tdata[data]){
                    console.log(key + " -> ");
					console.log(tdata[data][key]);
                    lat = tdata[data]['lat'];
                    lng = tdata[data]['lng'];
                }
            }
        }
    });

    //writeUserData(1,'Nagesh H S',13.064156,77.5810103);

    function writeUserData(userId, name, lattitude, longitude) {
        firebase.database().ref('tracking/' + userId).push({
            user: name,
            lat: lattitude,
            lng : longitude
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2-azeUdXbeAqQrvGtLZrbsXIFcofE_B8&libraries=places&region=IN"></script>
<script>
	map = new google.maps.Map(document.getElementById('googleMap'), {
		zoom: 4,
		center: {lat: lat, lng : lng, alt: 0},
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		disableDefaultUI:   true,
		overviewMapControl: false,
		streetViewControl:  false,
		scaleControl:       true,
		mapTypeControl:     false,
		panControl:         true,
		panControlOptions:{
			position: google.maps.ControlPosition.TOP_RIGHT
		},
		zoomControl: true,
		zoomControlOptions: {
			style: google.maps.ZoomControlStyle.LARGE,
			position: google.maps.ControlPosition.TOP_RIGHT
		}
	});

	map_marker = new google.maps.Marker({position: {lat: lat, lng: lng}, map: map});
	map_marker.setMap(map);

	function redraw() {
		var image = 'https://lh3.ggpht.com/XAjhu-6znztoLTr9AxuwM5v0wilaKiUJJMLKEiiFMn6lGOmBmY1Km7Kt1ohildzlIdWgkwy_5g=w9-h9';

		new google.maps.Marker({
		  position: {lat: lat, lng : lng},
		  map: map,
		  draggable: false,
		  icon: image
		});

		lat = parseFloat(lat);
		lng = parseFloat(lng);
		
		map.setCenter({lat: lat, lng : lng, alt: 0});
		map_marker.setPosition({lat: lat, lng : lng, alt: 0});
	}

	function getNonZeroRandomNumber(){
		return Math.floor(Math.random() * 201) - 1;
	}
</script>
<script>
	// var employeeName = '';
	// var employeeMobile = '';
	// var employeeGender = 'Male';
	// var employeeCity = '';
	// var employeePic = '';
	//
	// var marker_male = '{{ url('/') }}/img/icons/male-2.png';
	// var marker_female = '{{ url('/') }}/img/icons/female-2.png';
	//
	// var markers  = [];
	// var infoWindowContent = [];
	//
	// $(function(){
	// 	$('#btnFilter').on('click', function(){
	// 		$('#cityName').val('');
	// 		$('#employeeFilterModal').modal();
	// 	});
	//
	// 	$('#selectCity').on('click', function(){
	// 		var city = $('#cityName option:selected').val();
	// 		if(city != ''){
	// 			block = '#cityName option:selected';
	// 			employeeName = $(block).data('firstname') + ' ' + $(block).data('lastname');
	// 			employeeMobile = $(block).data('mobile');
	// 			employeeCity = $(block).data('city');
	// 			employeePic = '{{ url('uploads/profile_picture/') }}' + $(block).data('picture');
	//
	// 			// initMap();
	//
	// 			// getEmployeeMapByLocation(city);
	// 		}
	// 	});
	//
	// 	getEmployeeLocation();
	// });

	// function getEmployeeLocation(){
	// 	$.ajax({
	// 		url: '{{ route('tracking.employee-location-coordinates') }}',
	// 		success: function (data){
	// 			//console.log(data);
	// 			if(data && data != null && data.length){
	// 				$(data).each(function(i){
	// 					image = data[i].prof_gender=='Male'?marker_male:marker_female;
	//
	// 					markers.push([data[i].prof_fname+ ' ' + data[i].prof_lname, parseFloat(data[i].lat),parseFloat(data[i].lng), data[i].prof_gender]);
	//
	// 					var content = '<div class="info_content">' +
	// 			        '<h3>'+data[i].prof_fname+ ' ' + data[i].prof_lname+'</h3>' +
	// 			        '<p>Last seen at '+data[i].timestamp+'</p>' + '</div>';
	//
	// 					var date = new Date(data[i].timestamp);
	// 					var tdate = new Date();
	// 					last_seen = '';
	// 					if(tdate.getDate() == date.getDate()){
	// 						last_seen = 'Today, ';
	// 					}else{
	// 						last_seen = date.getDate()+'-'+date.getMonth()+'-'+date.getYear();
	// 					}
	// 					last_seen += date.getHours()+':'+date.getMinutes();
	//
	// 					var contentString = '<div id="content">'+
	// 				      '<div id="bodyContent">'+
	// 					  '<div class="col-md-5" style="vertical-align:middle !important;padding-top: 10px;">' +
	// 				      '<img src="{{ url('uploads/profile_picture/') }}/'+data[i].profile_image_path+'" style="max-width: 100px; float:left; margin-right: 10px;border: 1px solid #ccc; padding: 2px; border-radius: 2px;"/>'+
	// 					  '</div>' +
	// 					  '<div class="col-md-7">' +
	// 					  '<b style="font-size: 20px; font-weight: normal; color: #00b29c;">'+data[i].prof_fname+ ' ' + data[i].prof_lname+'</b><br>' +
	// 				      '<b style="line-height: 15px;">'+data[i].prof_mobile+'</b><br>'+
	// 					  '<b style="line-height: 15px;">'+data[i].prof_city+'</b><br>'+
	// 				      '<b style="line-height: 30px;color: #607D8B">Last seen at</b><br>' +
	// 					  last_seen +
	// 					  '</div>' +
	// 				      '</div>' +
	// 				      '</div>';
	//
	// 					infoWindowContent.push(contentString);
	// 				});
	//
	// 				initMap();
	// 			}
	// 		},
	// 		error: function (err){
	// 			console.log(err);
	// 		}
	// 	});
	// }

</script>
@endsection
