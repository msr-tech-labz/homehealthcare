<div class="row clearfix">
    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
        <label for="branch">Branch</label>
    </div>
    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
        <div class="form-group">
        @foreach($branches as $b)
        @endforeach
            <select class="form-control show-tick" data-live-search="true" id="branch_id" name="branch_id" required>
                @foreach($branches as $b)
                <option value="{{ $b->id }}" {{ isset($l->branch_id) && $l->branch_id == $b->id ?'selected':''}}>{{ $b->branch_name }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

