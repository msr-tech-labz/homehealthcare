@section('plugin.styles')
<style>
.theme{
    background: linear-gradient(45deg,#1de09914,#1dc8cd) !important;
    background-color:#072951 !important;
}
.content{
    margin:80px 15px 0 !important;
}
.info-box .icon{
    width: 65px !important;
}
.tab-nav-right > li > a{
    min-width: 180px !important;
    text-align: center;
}
.nav-tabs > li.active{
    border-left: 2px solid #2196F3;
    border-right: 2px solid #2196F3;
    border-top: 2px solid #2196F3;
    margin-bottom: 2px;
    background: #efefef;
}
.nav-tabs > li.active > a{
    font-weight: bold;
    color: #2196F3 !important;
}
.nav-tabs > li > a:before{
    border-bottom: 0 !important;
}
table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td {
    text-transform: uppercase; !important;
}
.dashboard-icon{
    width: 150px !important;
    height: 150px !important;
}
a{
    text-decoration:none !important;
}
a{
    text-decoration:none !important;
}
.card .header {
    padding: 5px 20px !important;
}
@media only screen and (max-width: 1200px) {
    .stats{
        height: 100% !important;
    }
}

.icon {
  max-width: 100%;
  max-height: 80px;
  -moz-transition: all 0.3s;
  -webkit-transition: all 0.3s;
  transition: all 0.3s;
}
.icon:hover {
  -moz-transform: scale(1.1);
  -webkit-transform: scale(1.1);
  transform: scale(1.1);
}
.icon:hover a>div{
  font-weight: normal !important;
  font-size: 11px !important;
  color: #7138c0  !important;
}
body{
    overflow-y: hidden !important;
}
.popover[class*="tour-"]{
    /*background: linear-gradient(to bottom,#095573,#2B044A);*/
    /* background: linear-gradient(to bottom,rgba(63, 81, 180, 1) 30%,rgba(103, 58, 183, 1) 70%); */
    background: linear-gradient(to bottom,#1dc8cd,#152b3f) !important;
    border-radius: 30px;
    border: none !important;
}
.popover .popover-title{
    background: transparent !important;
    color: #fff !important;
    text-align: center !important;
}

/*.popover.left > .arrow:after,{
    border-bottom-color: #47b5b9 !important;
    border-top-color: #54cdc2 !important;
}*/
.popover.right > .arrow:after{
    border-right-color: #268383!important;
}
.popover.bottom > .arrow:after{
    border-bottom-color: #1dc3c8!important;
}
#bar_chart_leads{
    height: 100% !important;
}
#bar_chart_resource{
    height: 100% !important;
}
#datedropper{
    width: 100%;
    position: relative;
    text-align: center;
    font-family: Palatino Linotype;
    font-weight: bolder;
    border-radius: 15px;
    top: 40%;
    font-size: 18px;
    height:60px;
}
.table-fixed {
  width: 100%;
}
.table-fixed tbody {
  height: 120px;
  overflow-y: auto;
  width: 100%;
}
.table-fixed thead, .table-fixed tbody, .table-fixed tr, .table-fixed td, .table-fixed th {
  font-size:11px;
  display: block;
  color: black;
  font-weight: bolder;
  border: none !important;
}
.table-fixed tbody td {
  float: left;
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
}
.table-fixed thead tr th {
    color: #fff;
    padding: 3px !important;
    float: left;
    background-color:#6222b9;
    border-color:#6222b9;
}
.loader{
    background-image: url('img/loading.gif');
    background-repeat: no-repeat;
    background-position: center;
    background-size: 100px 100px;
    position: relative;
}
.dashstats{
    font-size:10px !important;
}
.panelTab {
    height: 210px;
    box-shadow: 0 20px 35px rgba(0, 0, 0, 0.12);
    background-color:#fff;
    padding:0;
}
.btn-success {
    background: linear-gradient(270deg, #c850c0, #4158d0) !important;
}
</style>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/themes/default.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/themes/default.date.css" />
<style>
.input-group{
    /*z-index: 0 !important;*/
}
.input-group input {
    background-color:#fff !important;
    height:37px !important;
    border-radius:0px !important;
    padding:0px;
}
.input-group input:hover {cursor:pointer;}
.picker {font-size:11px;}
.picker__frame{min-width:256px;max-width:300px;}
.picker__month {font-size:20px; font-weight:bold;}
.picker__year {font-size:15px;}
@media (min-height:40.125em){.picker__frame{margin-bottom:21%}}
.picker__table thead {border-top:4px solid #6222b9; background:#6222b9;}
.picker__weekday {color:#fff; text-align:center;}
.picker__table tbody td {color:#545454;}
.picker--focused .picker__day--selected, .picker__day--selected, .picker__day--selected:hover {background:#6222b9;}
.picker__day--today:before {border-top: .5em solid #6222b9;}
.picker__button--today:before {border-top:.66em solid #6222b9;}
.theme {
    background:#f1f6f6!important;
    background-image:url('img/dashboard_background1.png')!important;
    background-size:cover !important;
}
.dashboardSearch{
    background-image: url('/img/searchicon.png');
    background-position: 10px 12px;
    background-repeat: no-repeat;
    width: 100%;
    font-size: 14px;
    padding: 12px 8px 0px 35px;
    border: none;
}
</style>
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection

<div class="row clearfix" id="tourStart">
    <div class="container-fluid">
        <input type="checkbox" class="filled-in chk-col-purple" id="help_mode">
        <label for="help_mode" class="help_mode" id="tourHelp" style="float: right;margin: 5px 0 0 25px;">HELP MODE</label>
        <span class="btn btn-success startTour waves-effect waves-light-purple waves-ripple" style="float: right;">TOUR</span>
        <div class="clearfix"></div>
        <div class="row clearfix">
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-center" style="width: 13% !important">
                <div class="col-lg-12 col-sm-12 col-xs-6 icon" style="margin-bottom: 18px;" id="tourLead">
                    <div class="col-lg-12">
                        <a style="color: black;" href="{{ route('lead.index') }}">
                            <img src="{{ asset('img/flaticons2/leads.png') }}" style="height: 60px;width: 60px;margin-bottom: 12px;"><br>
                            <div style="font-weight: 500;letter-spacing:0.2em;font-size: 10px;">LEADS</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-12 col-sm-12 col-xs-6 icon" style="margin-bottom: 18px;" id="tourOperation">
                    <div class="col-lg-12">
                        <a style="color: black;" href="{{ route('lead.operations') }}">
                            <img src="{{ asset('img/flaticons2/operations.png') }}" style="height: 60px;width: 60px;margin-bottom: 12px;"><br>
                            <div style="font-weight: 500;letter-spacing:0.2em;font-size: 10px;">OPERATIONS</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-12 col-sm-12 col-xs-6 icon" style="margin-bottom: 18px;" id="tourBilling">
                    <div class="col-lg-12">
                        <a style="color: black;" href="{{route('billing.index')}}">
                            <img src="{{ asset('img/flaticons2/billing.png') }}" style="height: 60px;width: 60px;margin-bottom: 12px;"><br>
                            <div style="font-weight: 500;letter-spacing:0.2em;font-size: 10px;">BILLING</div>
                        </a>
                    </div>
                </div>
                
                <div class="col-lg-12 col-sm-12 col-xs-6 icon" style="margin-bottom: 18px;" id="tourHrm">
                    <div class="col-lg-12">
                        <a style="color: black;" href="{{route('hr.index')}}">
                            <img src="{{ asset('img/flaticons2/hrm.png') }}" style="height: 60px;width: 60px;margin-bottom: 12px;"><br>
                            <div style="font-weight: 500;letter-spacing:0.2em;font-size: 10px;">RESOURCES</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-12 col-sm-12 col-xs-6 icon" style="margin-bottom: 18px;" id="tourReports">
                    <div class="col-lg-12">
                        <a style="color: black;" href="{{route('reports.index')}}">
                            <img src="{{ asset('img/flaticons2/reports.png') }}" style="height: 60px;width: 60px;margin-bottom: 12px;"><br>
                            <div style="font-weight: 500;letter-spacing:0.2em;font-size: 10px;">REPORTS</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <div class="row clearfix">
                    <div style="width:200px; margin:auto;">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <a class="btn btn-success" role="button" href="#" id="previous_day" data-diff="-1"><i class="glyphicon glyphicon-chevron-left"></i></a>
                            </span>
                            <input id="datedropper" name="datedropper" type="text" class="form-control" data-value="{{date('dd-mm-yyyy')}}">
                            <span class="input-group-btn">
                                <a class="btn btn-success" role="button" href="#" id="next_day" data-diff="1"><i class="glyphicon glyphicon-chevron-right"></i></a>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-md-6">
                        <div class="col-md-12 panelTab">
                            <input type="text" class="dashboardSearch" id="deploymentsInput" onkeyup="myFunction('deployments')" placeholder="Search for patient name..">
                            <table class="table table-fixed deployments">
                                <thead>
                                    <tr>
                                        <th class="col-xs-12 text-center">PENDING SCHEDULES</th>
                                    </tr>
                                    <tr>
                                        <th class="col-xs-3">Patient</th>
                                        <th class="col-xs-3">Professional</th>
                                        <th class="col-xs-3">Service</th>
                                        <th class="col-xs-3" style="text-align:right;">Action</th>
                                    </tr>
                                </thead>
                                <tbody class="loader">
                                </tbody>
                            </table>
                        </div>
                        <div  class="col-md-12" style="height: 15px"></div>
                        <div class="col-md-12 panelTab">
                            <input type="text" class="dashboardSearch" id="followupsInput" onkeyup="myFunction('followups')" placeholder="Search for patient name..">
                            <table class="table table-fixed followups">
                                <thead>
                                    <tr>
                                        <th class="col-xs-12 text-center">FOLLOWUPS</th>
                                    </tr>
                                    <tr>
                                        <th class="col-xs-3">Patient</th>
                                        <th class="col-xs-3">Phone</th>
                                        <th class="col-xs-3">Time</th>
                                        <th class="col-xs-3" style="text-align:right;">Action</th>
                                    </tr>
                                </thead>
                                <tbody class="loader">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12 panelTab">
                            <input type="text" class="dashboardSearch" id="replacementsInput" onkeyup="myFunction('replacements')" placeholder="Search for patient name..">
                            <table class="table table-fixed replacements">
                                <thead>
                                    <tr>
                                        <th class="col-xs-12 text-center">REPLACEMENTS</th>
                                    </tr>
                                    <tr>
                                        <th class="col-xs-3">Patient</th>
                                        <th class="col-xs-3">Service</th>
                                        <th class="col-xs-3">Reason</th>
                                        <th class="col-xs-3" style="text-align:right;">Action</th>
                                    </tr>
                                </thead>
                                <tbody class="loader">
                                </tbody>
                            </table>
                        </div>
                        <div  class="col-md-12" style="height: 15px"></div>
                        <div class="col-md-12 panelTab">
                            <input type="text" class="dashboardSearch" id="serviceExpiryInput" onkeyup="myFunction('serviceExpiry')" placeholder="Search for patient name..">
                            <table class="table table-fixed serviceExpiry">
                                <thead>
                                    <tr>
                                        <th class="col-xs-12 text-center">SERVICE EXPIRY</th>
                                    </tr>
                                    <tr>
                                        <th class="col-xs-4">Patient</th>
                                        <th class="col-xs-4">Service</th>
                                        <th class="col-xs-4" style="text-align:right;">Action</th>
                                    </tr>
                                </thead>
                                <tbody class="loader">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="width:101%;height:40px;position: fixed;bottom: 0;background:#6222b9;left: -1px !important;line-height:40px;color:#fff">
    <span style="display:inline-block;float:left;padding-left: 10px"><small>Build v3.0.1 - 20-Jan-2020</small></span>
    <span style="display:inline-block;float:right;padding-right: 25px">{{ date('Y',strtotime('-2 year')) }} - {{ date('Y') }} &copy; Smart Health Connect</span>
</div>

@section('page.scripts')
@parent
<script src="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/picker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/picker.date.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/picker.time.js"></script>
<script>
    $(function(){
        var picker = $('#datedropper').pickadate({
            format: 'dd-mm-yyyy',
            clear: '',
            onClose: function() {document.getElementById('next_day').focus();},
            onSet: function() {
                getDeploymentsByDate($('#datedropper').val());
                getReplacementsByDate($('#datedropper').val());
                getFollowUpsByDate($('#datedropper').val());
                getServiceExpiryByDate($('#datedropper').val());
            }
        }).pickadate('picker');

        $('#previous_day, #next_day').click(function(e) {
            e.preventDefault();
            setDate($(this).data('diff'));
            getDeploymentsByDate($('#datedropper').val());
            getReplacementsByDate($('#datedropper').val());
            getFollowUpsByDate($('#datedropper').val());
            getServiceExpiryByDate($('#datedropper').val());
        })
        function setDate(diff) {
            var date = new Date(picker.get('select').pick);
            var newDate = date.setDate(date.getDate() + diff);
            picker.set('select', newDate)
        }

        $('.content').on('hover','.icon',function(){
            $(this).css('zoom',1);
        },function(){
            $(this).css('zoom',0);
        });

        $('.pick-submit').click(function() {
            getDeploymentsByDate($('#datedropper').val());
            getReplacementsByDate($('#datedropper').val());
            getFollowUpsByDate($('#datedropper').val());
            getServiceExpiryByDate($('#datedropper').val());
        });

        var tour = new Tour({
            storage: window.localStorage,
            smartPlacement: true,
            autoscroll: true,
            backdrop: true,
            backdropContainer: 'body',
            backdropPadding: 'top',
            debug: false,
            template: `<div class='popover tour'>
            <div class='arrow'></div>
            <h3 class='popover-title'></h3>
            <div class='popover-content'></div>
            <div class='popover-navigation'>
            <button class='btn btn-default btn-circle waves-effect' data-role='prev'><i class='material-icons' style='font-size:22px;top:3px !important;left:-3px !important'>chevron_left</i></button>
            <span data-role='separator'>&nbsp;</span>
            <button class='btn btn-default btn-circle waves-effect' data-role='next'><i class='material-icons' style='font-size:22px;top:3px !important;left:-3px !important'>chevron_right</i></button>
            <button class='btn btn-default btn-circle waves-effect' data-role='end'><i class='material-icons' style='font-size:22px;top:3px !important;left:-3px !important'>close</i></button>
            </div>
            </div>`,
        });

        tour.addSteps([
        {
            title: "Welcome To Smart Health Connect",
            content: "Lets start the tour. Press next to start the guide",
            placement: 'top',
            orphan: true,
        },
        {
            element: "#tourLead",
            title: "Lead Management",
            content: "Create and manage leads.",
            placement: 'right',
        },
        {
            element: "#tourOperation",
            title: "Operation Management",
            content: "Create and manage operations, operations history, assessment, schedules, daily tasks",
            placement: 'right',
        },
        {
            element: "#tourBilling",
            title: "Billing",
            content: "Create and manage invoice, payments, statement of account and credit memo.",
            placement: 'right',
        },
        {
            element: "#tourHrm",
            title: "HR Management",
            content: "Manage your Employees, Leaves, Attendance and Payroll",
            placement: 'right',
        },
        {
            element: "#tourReports",
            title: "Reports",
            content: "Generate reports and evaluate the statistics.",
            placement: 'right',
        },
        {
            element: "#tourHome",
            title: "The Homescreen",
            content: "Want to return back to the Homescreen? At any point of time, just click here.",
            placement: 'bottom',
            backdropContainer: 'body',
            onShown: function(tour){
                $('.tour-backdrop').css("z-index", "0")
            },
        },
        {
            element: "#notificationLink",
            title: "The Notifications",
            content: "Any new changes in the software will be notified here.",
            placement: 'bottom',
            backdropContainer: 'body',
            onShown: function(tour){
                $('.tour-backdrop').css("z-index", "0")
            },
        },
        {
            element: "#tourSubscription",
            title: "Subscription",
            content: "Manage or upgrade subscription license.",
            placement: 'bottom',
            backdropContainer: 'body',
            onShown: function(tour){
                $('.tour-backdrop').css("z-index", "0")
            },
        },
        {
            element: "#tourAdministration",
            title: "The Masters",
            content: "The first place to visit before starting off. Please fill all the relevant data before you start using the software.",
            placement: 'bottom',
            backdropContainer: 'body',
            onShown: function(tour){
                $('.tour-backdrop').css("z-index", "0");
            },
        },
        {
            element: "#tourLanguage",
            title: "Change Language",
            content: "Language Barriers? Choose the language that the employees are comfortable to work with.",
            placement: 'bottom',
            backdropContainer: 'body',
            onShown: function(tour){
                $('.tour-backdrop').css("z-index", "0");
            },
        },
        {
            element: "#tourHelp",
            title: "Help pop ups",
            content: "New to the software? Checking this will make popups at required places that will guide through the work flow.",
            placement: 'left',
            backdropContainer: 'body',
            onShown: function(tour){
                $('.tour-backdrop').css("z-index", "0");
            },
        },
        {
            title: "Thank you !",
            content: "In case of any other queries,please do write to us.",
            placement: 'top',
            orphan: true,
        },
        ]);

        tour.init();
        tour.start();

        $('.content').on('click', '.startTour', function(e){
            e.preventDefault();
            tour.init();
            tour.start();
            if(tour.ended()){
                tour.restart();
            }
        });

        getDeploymentsByDate();
        getReplacementsByDate();
        getFollowUpsByDate();
        getServiceExpiryByDate();

        function getDeploymentsByDate(date=null){
            if(date == null){
                date = new Date().toJSON().slice(0,10).split('-').reverse().join('-');
            }
            $('.deployments tbody').addClass('loader');
            $.ajax({
                url: '{{ route('ajax.get-deployments-by-date') }}',
                type: 'POST',
                data: { getByDate: date, _token: '{{ csrf_token() }}'},
                success: function (d){                    
                    if(d.length){
                        html = ``;
                        link = '{{ url('leads/view') }}';
                        $.each(d, function(i){
                            html += `<tr>
                            <td class="col-xs-3">`+ d[i]['patient'] +`</td>
                            <td class="col-xs-3">`+ d[i]['professional'] +`</td>
                            <td class="col-xs-3">`+ d[i]['service'] +`</td>
                            <td class="col-xs-3" style="text-align:right;"><a target="_blank" href="`+ link + `/` + d[i]['id'] +`" class="btn btn-xs btn-success dashstats">View</a></td>
                            </tr>`;
                        });
                        $('.deployments tbody').removeClass('loader');
                    }else{
                        html = `<tr>
                        <td class="col-xs-12" style="text-align:center !important;">
                        No deployments(s) found.
                        </td>
                        </tr>`;
                        $('.deployments tbody').removeClass('loader');
                    }
                    $('.deployments tbody').html(html);
                },
                error: function (err){
                    console.log(err);
                }
            });
        }
        function getReplacementsByDate(date=null){
            if(date == null){
                date = new Date().toJSON().slice(0,10).split('-').reverse().join('-');
            }
            $('.replacements tbody').addClass('loader');
            $.ajax({
                url: '{{ route('ajax.get-replacements-by-date') }}',
                type: 'POST',
                data: { getByDate: date, _token: '{{ csrf_token() }}'},
                success: function (d){ 
                    if(!$.isEmptyObject(d)){
                        html = ``;
                        link = '{{ url('leads/view') }}';
                        $.each(d, function(i){
                            html += `<tr>
                            <td class="col-xs-3">`+ d[i]['patient'] +`</td>
                            <td class="col-xs-3">`+ d[i]['service_name'] +`</td>
                            <td class="col-xs-3">`+ d[i]['reason'] +`</td>
                            <td class="col-xs-3" style="text-align:right;"><a target="_blank" href="`+ link + `/` + d[i]['id'] +`" class="btn btn-xs btn-success dashstats">View</a></td>
                            </tr>`;
                        });
                        $('.replacements tbody').removeClass('loader');
                    }else{
                        html = `<tr>
                        <td class="col-xs-12" style="text-align:center !important;">
                        No replacements(s) found.
                        </td>
                        </tr>`;
                        $('.replacements tbody').removeClass('loader');
                    }
                    $('.replacements tbody').html(html);
                },
                error: function (err){
                    console.log(err);
                }
            });
        }
        function getFollowUpsByDate(date=null){
            if(date == null){
                date = new Date().toJSON().slice(0,10).split('-').reverse().join('-');
            }
            $('.followups tbody').addClass('loader');
            $.ajax({
                url: '{{ route('ajax.get-follow-ups-by-date') }}',
                type: 'POST',
                data: { getByDate: date, _token: '{{ csrf_token() }}'},
                success: function (d){
                    if(d.length){
                        html = ``;
                        link = '{{ url('leads/view') }}';
                        $.each(d, function(i){
                            html += `<tr>
                            <td class="col-xs-3">`+ d[i]['patient'] +`</td>
                            <td class="col-xs-3">`+ d[i]['phone'] +`</td>
                            <td class="col-xs-3">`+ d[i]['time'] +`</td>
                            <td class="col-xs-3" style="text-align:right;"><a target="_blank" href="`+ link + `/` + d[i]['id'] +`" class="btn btn-xs btn-success dashstats">View</a></td>
                            </tr>`;
                        });
                        $('.followups tbody').removeClass('loader');
                    }else{
                        html = `<tr>
                        <td class="col-xs-12" style="text-align:center !important;">
                        No Follow up(s) found.
                        </td>
                        </tr>`;
                        $('.followups tbody').removeClass('loader');
                    }
                    $('.followups tbody').html(html);
                },
                error: function (err){
                    console.log(err);
                }
            });
        }
        function getServiceExpiryByDate(date=null){
            if(date == null){
                date = new Date().toJSON().slice(0,10).split('-').reverse().join('-');
            }
            $('.serviceExpiry tbody').addClass('loader');
            $.ajax({
                url: '{{ route('ajax.get-service-expiry-by-date') }}',
                type: 'POST',
                data: { getByDate: date, _token: '{{ csrf_token() }}'},
                success: function (d){               
                    if(d.length){
                        html = ``;
                        link = '{{ url('leads/view') }}';
                        $.each(d, function(i){
                            html += `<tr>
                            <td class="col-xs-4">`+ d[i]['patient'] +`</td>
                            <td class="col-xs-4">`+ d[i]['service_name'] +`</td>
                            <td class="col-xs-4" style="text-align:right;"><a target="_blank" href="`+ link + `/` + d[i]['id'] +`" class="btn btn-xs btn-success dashstats">View</a></td>
                            </tr>`;
                        });
                        $('.serviceExpiry tbody').removeClass('loader');
                    }else{
                        html = `<tr>
                        <td class="col-xs-12" style="text-align:center !important;">
                        No expiry(s) found.
                        </td>
                        </tr>`;
                        $('.serviceExpiry tbody').removeClass('loader');
                    }
                    $('.serviceExpiry tbody').html(html);
                },
                error: function (err){
                    console.log(err);
                }
            });
        }
        });
    </script>
    <script>
    function myFunction(tableClassName) {
      // Declare variables 
      var input, filter, table, tr, td, i, txtValue;
      input = document.getElementById(tableClassName+'Input');
      filter = input.value.toUpperCase();
      table = document.getElementsByClassName(tableClassName);
      tr = table[0].getElementsByTagName("tr");

      // Loop through all table rows, and hide those who don't match the search query
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
          txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
                } else {
            tr[i].style.display = "none";
                }
            } 
        }
    }
    $('.content').on('change','#help_mode',function(){
        if($(this).prop('checked')){
            localStorage.setItem('helpMode', true);
        }else{
             localStorage.removeItem('helpMode');
        }
    });
    if(localStorage.getItem('helpMode') == 'true'){
        $('#help_mode').prop('checked','checked');
    }
</script>
@endsection
