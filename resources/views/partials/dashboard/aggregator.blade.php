@section('plugin.styles')
<style>
.theme{
    background: linear-gradient(45deg,#1de09914,#1dc8cd) !important;
    background-color:  #072951 !important;
}
.content{
    margin: 100px 15px 0 !important;
}
.info-box .icon{
    width: 65px !important;
}
.tab-nav-right > li > a{
    min-width: 180px !important;
    text-align: center;
}
.nav-tabs > li.active{
    border-left: 2px solid #2196F3;
    border-right: 2px solid #2196F3;
    border-top: 2px solid #2196F3;
    margin-bottom: 2px;
    background: #efefef;
}
.nav-tabs > li.active > a{
    font-weight: bold;
    color: #2196F3 !important;
}
.nav-tabs > li > a:before{
    border-bottom: 0 !important;
}
table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td {
    text-transform: uppercase; !important;
}
.dashboard-icon{
    width: 150px !important;
    height: 150px !important;
}
a{
    text-decoration:none !important;
}
a{
    text-decoration:none !important;
}
.card .header {
    padding: 5px 20px !important;
}
@media only screen and (max-width: 1200px) {
    .stats{
        height: 100% !important;
    }
}

.icon {
  max-width: 100%;

  -moz-transition: all 0.3s;
  -webkit-transition: all 0.3s;
  transition: all 0.3s;
}
.icon:hover {
  -moz-transform: scale(1.1);
  -webkit-transform: scale(1.1);
  transform: scale(1.1);
}
.icon:hover a>div{
  font-weight: normal !important;
  font-size: 11px !important;
  color: #add413  !important;
}
body{
    overflow-y: hidden !important;
}
.popover[class*="tour-"]{
    /*background: linear-gradient(to bottom,#095573,#2B044A);*/
    /* background: linear-gradient(to bottom,rgba(63, 81, 180, 1) 30%,rgba(103, 58, 183, 1) 70%); */
    background: linear-gradient(to bottom,#1dc8cd,#1de09914) !important;
    border-radius: 30px;
    border: none !important;
}
.popover .popover-title{
    background: transparent !important;
    color: #fff !important;
    text-align: center !important;
}

.popover.bottom > .arrow:after, .popover.top > .arrow:after,.popover.left > .arrow:after,.popover.right > .arrow:after{
    border-bottom-color: #47b5b9 !important;
    border-top-color: #54cdc2 !important;
}
#bar_chart_leads{
    height: 100% !important;
}
#bar_chart_resource{
    height: 100% !important;
}
#datedropper{
    width: 100%;
    position: relative;
    text-align: center;
    font-family: Palatino Linotype;
    font-weight: bolder;
    border-radius: 15px;
    top: 40%;
    font-size: 18px;
    height:60px;
}
.table-fixed {
  width: 100%;
}
.table-fixed tbody {
  height: 150px;
  overflow-y: auto;
  width: 100%;
}
.table-fixed thead, .table-fixed tbody, .table-fixed tr, .table-fixed td, .table-fixed th {
  font-size:11px;
  display: block;
  color: black;
  font-weight: bolder;
  border: none !important;
}
.table-fixed tbody td {
  float: left;
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
}
.table-fixed thead tr th {
    color: #fff;
  padding: 3px !important;
  float: left;
  background-color: #12798d;
  border-color: #12798d;
}
.loader{
    background-image: url('img/loading.gif');
    background-repeat: no-repeat;
    background-position: center;
    background-size: 100px 100px;
    position: relative;
}
.dashstats{
    font-size: 8px !important;
}
</style>
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
<link href="{{ ViewHelper::ThemePlugin('datedropper/datedropper.css') }}" rel="stylesheet">
@endsection

<div class="row clearfix" id="tourStart">
    <div class="container-fluid">
        <div class="row clearfix" style="margin-top: -10px;margin-bottom: 20px">
            <div class="col-lg-12 hide" style="margin-bottom: 20px">
                {? $hour = date('H');if ($hour > 16) {$greetings = "Good Evening";} elseif ($hour >=  12){$greetings = "Good Afternoon";} elseif ($hour < 12) {$greetings = "Good Morning";} ?}
                <h2 style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif !important;font-size:20px; font-weight: normal;color: white;letter-spacing:0.1em">{{ $greetings }}, {{ session('user_name') }}
                    <a class="btn btn-primary startTour waves-effect waves-light-blue waves-ripple" style="float: right;">TOUR</a>
                </h2>
            </div>
        </div>
        <div class="clearfix"></div><br>
        <div class="row clearfix" style="margin-top: 10% !important">
            <div class="col-lg-12 text-center">
                <div class="col-lg-4 icon" style="margin-bottom: 20px;" id="tourLead">
                    <div class="col-lg-12">
                        <a style="color: black;" href="{{ route('lead.index') }}">
                            <img src="{{ asset('img/flaticons/leads.png') }}" style="height: 100px;width: 100px;margin-bottom: 12px;"><br>
                            <div style="font-weight: 500;letter-spacing:0.2em;font-size: 10px; color: white;">EPISODES</div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 icon" style="margin-bottom: 20px;" id="tourHrm">
                    <div class="col-lg-12">
                        <a style="color: black;" href="{{route('hr.index')}}">
                            <img src="{{ asset('img/flaticons/hrm.png') }}" style="height: 100px;width: 100px;margin-bottom: 12px;"><br>
                            <div style="font-weight: 500;letter-spacing:0.2em;font-size: 10px; color: white;">MY STAFF</div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 icon" style="margin-bottom: 20px;" id="tourReports">
                    <div class="col-lg-12">
                        <a style="color: black;" href="{{route('reports.index')}}">
                            <img src="{{ asset('img/flaticons/reports.png') }}" style="height: 100px;width: 100px;margin-bottom: 12px;"><br>
                            <div style="font-weight: 500;letter-spacing:0.2em;font-size: 10px; color: white;">REPORTS</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="width:101%;height:40px;position: fixed;bottom: 0;background: #52cbc9d4;left: -1px !important;line-height:40px;color:#fff">
    <span style="display:inline-block;float:left;padding-left: 10px"><small>Build 2.2.0</small></span>
    <span style="display:inline-block;float:right;padding-right: 25px">{{ date('Y') }} &copy; Smart Health Connect</span>
</div>

@section('page.scripts')
    @parent
    <script src="{{ ViewHelper::ThemePlugin('datedropper/datedropper.js') }}"></script>
    <script>
        $(function(){
            $('.content').on('hover','.icon',function(){
                $(this).css('zoom',1);
            },function(){
                $(this).css('zoom',0);
            });

            var tour = new Tour({
                storage: window.localStorage,
                smartPlacement: true,
                autoscroll: true,
                backdrop: true,
                backdropContainer: 'body',
                backdropPadding: 'top',
                debug: false,
                template: `<div class='popover tour'>
                   <div class='arrow'></div>
                   <h3 class='popover-title'></h3>
                   <div class='popover-content'></div>
                   <div class='popover-navigation'>
                       <button class='btn btn-default btn-circle waves-effect' data-role='prev'><i class='material-icons' style='font-size:22px;top:3px !important;left:-3px !important'>chevron_left</i></button>
                       <span data-role='separator'>&nbsp;</span>
                       <button class='btn btn-default btn-circle waves-effect' data-role='next'><i class='material-icons' style='font-size:22px;top:3px !important;left:-3px !important'>chevron_right</i></button>
                       <button class='btn btn-default btn-circle waves-effect' data-role='end'><i class='material-icons' style='font-size:22px;top:3px !important;left:-3px !important'>close</i></button>
                   </div>
                 </div>`,
            });

            tour.addSteps([
                {
                    title: "Welcome To Smart Health Connect",
                    content: "Lets start the tour. Press next to start the guide",
                    placement: 'top',
                    orphan: true,
                },
                {
                    element: "#tourLead",
                    title: "Lead Management",
                    content: "Create and manage leads, lead history, assessment, schedules, daily tasks",
                    placement: 'bottom',
                },
                {
                    element: "#tourBilling",
                    title: "Billing",
                    content: "Create and manage invoice, payments, statement of account and credit memo.",
                    placement: 'bottom',
                },
                {
                    element: "#tourReports",
                    title: "Reports",
                    content: "Generate reports and evaluate the statistics.",
                    placement: 'bottom',
                },
                {
                    element: "#tourHrm",
                    title: "HR Management",
                    content: "Manage your Employees, Leaves, Attendance and Payroll",
                    placement: 'bottom',
                },
                {
                    element: "#tourTracking",
                    title: "Tracking",
                    content: "Track the location of your employees.",
                    placement: 'bottom',
                },
                {
                    element: "#tourHome",
                    title: "The Homescreen",
                    content: "Want to return back to the Homescreen? At any point of time, just click here.",
                    placement: 'bottom',
                    backdropContainer: 'body',
                    onShown: function(tour){
                        $('.tour-backdrop').css("z-index", "0")
                    },
                },
                {
                    element: "#notificationLink",
                    title: "The Notifications",
                    content: "Any new changes in the software will be notified here.",
                    placement: 'bottom',
                    backdropContainer: 'body',
                    onShown: function(tour){
                        $('.tour-backdrop').css("z-index", "0")
                    },
                },
                {
                    element: "#tourHelp",
                    title: "Help Desk",
                    content: "Need Help? We are here to help you, Write to us for any queries.",
                    placement: 'bottom',
                    backdropContainer: 'body',
                    onShown: function(tour){
                        $('.tour-backdrop').css("z-index", "0")
                    },
                },
                {
                    element: "#tourSubscription",
                    title: "Subscription",
                    content: "Manage or upgrade subscription license.",
                    placement: 'bottom',
                    backdropContainer: 'body',
                    onShown: function(tour){
                        $('.tour-backdrop').css("z-index", "0")
                    },
                },
                {
                    element: "#tourAdministration",
                    title: "The Masters",
                    content: "The first place to visit before starting off. Please fill all the relevant data before you start using the software.",
                    placement: 'bottom',
                    backdropContainer: 'body',
                    onShown: function(tour){
                        $('.tour-backdrop').css("z-index", "0");
                    },
                },
                {
                    element: "#tourLanguage",
                    title: "Change Language",
                    content: "Language Barriers? Choose the language that the employees are comfortable to work with.",
                    placement: 'bottom',
                    backdropContainer: 'body',
                    onShown: function(tour){
                        $('.tour-backdrop').css("z-index", "0");
                    },
                },
                {
                    title: "Thank you !",
                    content: "In case of any other queries,please do write to us.",
                    placement: 'top',
                    orphan: true,
                },
            ]);

            $('.content').on('click', '.startTour', function(e){
                e.preventDefault();
                tour.init();
                tour.start();
                if(tour.ended()){
                    tour.restart();
                }
            });
        });
    </script>
@endsection
