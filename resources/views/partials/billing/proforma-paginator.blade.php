<table class="table table-hover table-bordered proformaInvoiceTable" style="zoom:85%;">
    <thead>
        <tr>
            <th>#</th>
            <th>Patient ID</th>
            <th>Patient</th>
            <th>Invoice No.</th>
            <th>Invoice Date</th>
            <th>Amount</th>
            <th>Due Date</th>
            <th>Status</th>
            <th width="10%">Payment</th>
            <th>Aging</th>
            <th class="text-center" style="width: 11% !important;">Action</th>
        </tr>
    </thead>
    <tbody>
        @if(isset($proformaInvoices) && count($proformaInvoices))
        @foreach($proformaInvoices as $i => $invoice)
        <tr <?php if($invoice->email_invoice != null){ ?>style="background-color:#a0c8ce;"<?php } ?>>
            <td class="text-center">{{ $i+1 }}</td>
            <td>{{ $invoice->patient->patient_id }}</td>
            <td>
                {{ $invoice->patient->full_name }}<br>
                <small>Enquirer: {{ $invoice->patient->enquirer_name }}</small>
            </td>
            <td>{{ Helper::getBranchSettings('proforma_invoice_inits',$invoice->patient_id).''.$invoice->invoice_no }}</td>
            <td>{{ $invoice->invoice_date->format('d-m-Y') }}</td>
            <td>Rs. {{ number_format($invoice->total_amount,2) }}</td>
            <td>{{ $invoice->invoice_due_date->format('d-m-Y') }}</td>
            <td>{{ $invoice->status }}</td>
            <td>
                @ability('admin','billing-payments-update')
                <a class="btn btn-info btnUpdatePayment" style="margin-left: 2%" data-invoice-id="{{ Helper::encryptor('encrypt',$invoice->id) }}" data-invoice-no="{{ Helper::getSetting('invoice_inits').$invoice->invoice_no }}" data-invoice-date="{{ $invoice->invoice_date->format('d-m-Y') }}" data-invoice-amount="{{ floatVal($invoice->total_amount) }}" data-amount-received="{{ floatVal($invoice->amount_paid) }}" data-patient-id="{{ isset($invoice->patient)?Helper::encryptor('encrypt',$invoice->patient->id):'' }}" data-pat-id="{{ isset($invoice->patient)?$invoice->patient->patient_id:'' }}" data-status="{{ $invoice->status }}" data-patient="{{ isset($invoice->patient)?$invoice->patient->full_name:'' }}" data-customer="{{ isset($invoice->patient)?$invoice->patient->enquirer_name:'' }}">View / Update</a>
                @endability
            </td>
            <td>
                <small>{{ date_diff(new DateTime(), $invoice->created_at)->format("%a day(s) %h hr(s) %i min(s)") }}</small>
            </td>
            <td class="text-left">
                @ability('admin','billing-invoice-view')
                @if($invoice->status != 'Cancelled')
                    <a href="{{ route('billing.view-invoice',[Helper::encryptor('encrypt',$invoice->id),'Proforma']) }}" class="btn btn-md btn-primary" title="View Invoice"><i class="material-icons">remove_red_eye</i></a>

                    <a href="{{ route('billing.invoicepdf',['invoice_id' => $invoice->id,'email'=>true,'type'=>'Proforma']) }}" class="btn btn-md btn-info hide" title="Email Invoice"><i class="material-icons">email</i></a>
                    @if(!($invoice->amount_paid > 0))
                        @if($invoice->status != 'Adjusted' && $invoice->status != 'Paid')
                        <a href="{{ route('billing.delete-invoice',['invoice_id' => Helper::encryptor('encrypt',$invoice->id),'type'=>'Proforma']) }}" class="btn btn-md btn-danger" title="Cancel Invoice" onclick="return confirm_click();"><i class="material-icons">cancel</i></a>
                        @endif
                    @endif
                @else
                    Cancelled
                @endif
                @endability
            </td>
        </tr>
        @endforeach
        @else
        <tr>
            <td colspan="12" class="text-center">No invoice(s) found</td>
        </tr>
        @endif
    </tbody>
</table>
{{ $proformaInvoices->links() }}
<p class="text-right" style="margin-bottom:0;">
    {{ 'Showing '.$proformaInvoices->firstItem().' to '.$proformaInvoices->lastItem().' out of Total '.$proformaInvoices->total() }}
</p>