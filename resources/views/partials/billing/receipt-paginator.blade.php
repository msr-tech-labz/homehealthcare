<table class="table table-hover table-bordered receiptTable" style="zoom:85%;">
    <thead>
        <tr>
            <th>#</th>
            <th>Patient ID</th>
            <th>Patient</th>
            <th>Receipt Type</th>
            <th>Receipt No.</th>
            <th>Receipt Date</th>
            <th>Amount</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @if(isset($receipts) && count($receipts))
        @foreach($receipts as $i => $receipt)
        <tr>
            <td class="text-center">{{ $i+1 }}</td>
            <td>{{ $receipt->patient->patient_id }}</td>
            <td>
                {{ $receipt->patient->full_name }}<br>
                <small>Enquirer: {{ $receipt->patient->enquirer_name }}</small>
            </td>
            <td>{{ $receipt->receipt_type }}</td>
            <td>{{ $receipt->receipt_no }}</td>
            <td>{{ $receipt->receipt_date->format('d-m-Y') }}</td>
            <td>Rs. {{ number_format($receipt->receipt_amount,2) }}</td>
            <td class="text-left">
                @ability('admin','billing-invoice-view')
                    <a href="{{ route('billing.view-receipt',[Helper::encryptor('encrypt',$receipt->id)]) }}" class="btn btn-md btn-primary" title="View Receipt"><i class="material-icons">remove_red_eye</i></a>
                @endability
            </td>
        </tr>
        @endforeach
        @else
        <tr>
            <td colspan="10" class="text-center">No receipt(s) found</td>
        </tr>
        @endif
    </tbody>
</table>
{{ $receipts->links() }}
<p class="text-right" style="margin-bottom:0;">
    {{ 'Showing '.$receipts->firstItem().' to '.$receipts->lastItem().' out of Total '.$receipts->total() }}
</p>