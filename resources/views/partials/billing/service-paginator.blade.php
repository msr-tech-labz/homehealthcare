<table class="table table-hover table-bordered serviceInvoiceTable" style="zoom:85%;">
    <thead>
        <tr>
            <th>#</th>
            <th>Patient ID</th>
            <th>Patient</th>
            <th>Invoice No.</th>
            <th>Invoice Date</th>
            <th>Amount</th>
            <th>Due Date</th>
            <th>Aging</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        @if(isset($serviceInvoices) && count($serviceInvoices))
        @foreach($serviceInvoices as $i => $invoice)
        <tr>
            <td class="text-center">{{ $i+1 }}</td>
            <td>{{ $invoice->patient->patient_id }}</td>
            <td>
                {{ $invoice->patient->full_name }}<br>
                <small>Enquirer: {{ $invoice->patient->enquirer_name }}</small>
            </td>
            <td>{{ Helper::getBranchSettings('invoice_inits',$invoice->patient_id).''.$invoice->invoice_no }}</td>
            <td>{{ $invoice->invoice_date->format('d-m-Y') }}</td>
            <td>Rs. {{ number_format($invoice->total_amount,2) }}</td>
            <td>{{ $invoice->invoice_due_date->format('d-m-Y') }}</td>
            <td>
                <small>{{ date_diff(new DateTime(), $invoice->created_at)->format("%a day(s) %h hr(s) %i min(s)") }}</small>
            </td>
            <td class="text-left">
                @ability('admin','billing-invoice-view')
                @if($invoice->status != 'Cancelled')
                    <a href="{{ route('billing.view-invoice',[Helper::encryptor('encrypt',$invoice->id),'Service']) }}" class="btn btn-md btn-primary" title="View Invoice"><i class="material-icons">remove_red_eye</i></a>

                    <a href="{{ route('billing.invoicepdf',['invoice_id' => $invoice->id,'email'=>true,'type'=>'Service']) }}" class="btn btn-md btn-info hide" title="Email Invoice"><i class="material-icons">email</i></a>

                    <a href="{{ route('billing.delete-invoice',['invoice_id' => Helper::encryptor('encrypt',$invoice->id),'type'=>'Service']) }}" class="btn btn-md btn-danger" title="Cancel Invoice" onclick="return confirm_click();"><i class="material-icons">cancel</i></a>
                @else
                    Cancelled
                @endif
                @endability
            </td>
        </tr>
        @endforeach
        @else
        <tr>
            <td colspan="10" class="text-center">No invoice(s) found</td>
        </tr>
        @endif
    </tbody>
</table>
{{ $serviceInvoices->links() }}
<p class="text-right" style="margin-bottom:0;">
    {{ 'Showing '.$serviceInvoices->firstItem().' to '.$serviceInvoices->lastItem().' out of Total '.$serviceInvoices->total() }}
</p>