<table class="table table-bordered table-striped operationsTable">
    <thead>
        <tr>
            <th>#</th>
            <th>Status</th>
            <th>Date</th>
            <th>Source</th>
            <th>Episode Id</th>
            <th>Patient</th>
            <th>Enquirer</th>
            <th>Phone</th>
            <th>Location</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($tableOperations as $i => $operation)
        <tr>
            <td> {{ ++$i }} </td>
            <td> {{ $operation->status }} </td>
            <td> {{ $operation->created_at->format('d-m-y') }} </td>
            <td> {{ !empty($operation->referralSource)?$operation->referralSource->source_name:'-' }} </td>
            <td> {{ !empty($operation->episode_id)?$operation->episode_id:'TEMPLD0AF'.$operation->id }} </td>
            <td> {{ !empty($operation->patient->full_name)?$operation->patient->full_name:'-' }} </td>
            <td> {{ !empty($operation->patient->enquirer_name)?$operation->patient->enquirer_name:'-' }} </td>
            <td> {{ isset($operation->patient->contact_number)?$operation->patient->contact_number:'' }} </td>
            <td> {{ (isset($operation->patient) && !empty($operation->patient->city))?$operation->patient->city:'-' }} </td>
            <?php if($operation->aggregator_operation && $operation->aggregator_lead_status == 'Pending'){
                if(\Entrust::hasRole('admin') || \Entrust::can('leads-view')){
                    $patientData = json_encode(collect($operation->patient)->only('first_name','last_name','patient_age','patient_weight','gender','street_address','area','city'));
                    $leadData = json_encode(collect($operation)->only('case_description','medical_conditions','medications','procedures','aggregator_service','language_preference','gender_preference','aggregator_rate','aggregator_rate_negotiable'));
                    $html = '<a href="javascript:void(0);" target="_blank" class="btn btn-md bg-primary btnViewLead" data-id="'. \Helper::encryptor('encrypt',$operation->id).'" data-patient-id="'.\Helper::encryptor('encrypt',$operation->patient_id).'" data-patient="'.str_replace("\"", "'", $patientData).'" data-case="'.str_replace("\"", "'", $leadData).'" title="View Lead Details">View</a>';
                }
            }elseif($operation->aggregator_lead && $operation->aggregator_lead_status == 'Accepted'){
                if(\Entrust::hasRole('admin') || \Entrust::can('leads-view')){
                    $html = '<a href="'.route('lead.view',\Helper::encryptor('encrypt',$operation->id)).'" class="btn btn-md bg-primary" data-id="'.\Helper::encryptor('encrypt',$operation->id).'" title="Create Lead">View</a>';
                }
            }elseif(!$operation->aggregator_lead && $operation->aggregator_lead_status == 'Pending'){
                if(\Entrust::hasRole('admin') || \Entrust::can('leads-view')){
                    $html = '<a href="'.route('lead.view',\Helper::encryptor('encrypt',$operation->id)).'" class="btn btn-md bg-primary" data-id="'.\Helper::encryptor('encrypt',$operation->id).'" title="View Lead">View</a>';
                }
            } 
            ?>
            <td> {? echo html_entity_decode($html); ?} </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ $tableOperations->links() }}
<p class="text-right" style="margin-bottom:0;">
    {{ 'Showing '.$tableOperations->firstItem().' to '.$tableOperations->lastItem().' out of Total '.$tableOperations->total() }}
</p>