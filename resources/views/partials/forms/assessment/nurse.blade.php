<style>
[type="checkbox"] + label {
	margin-bottom:30px !important;
}
</style>

<hr><br>

<div class="col-sm-6">
	<div class="row clearfix">
		<div class="col-lg-4 col-md-3 col-sm-5 col-xs-5 form-control-label">
			<label for="assessment_date">Assessment Date</label>
		</div>
		<div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
			<div class="form-group">
				<div class="form-line">
					<input type="text" id="assessment_date_nursing" name="assessment_date_nursing" class="form-control getdate" placeholder="Assessment Date" required>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-sm-6" style="display: inline-block;">
	<div class="row clearfix">
		<div class="col-lg-4 col-md-3 col-sm-5 col-xs-5 form-control-label">
			<label for="assessment_method">Assessment Method</label>
		</div>
		<div class="col-md-8" style="width:45%;border: 1px solid #ccc;height:30px;border-radius: 4px;margin-left:1%">
			<input name="assessment_method[]" value="Telephonic" type="radio" id="basic_checkbox_40" class="filled-in" />
			<label class="col-lg-6 text-left" for="basic_checkbox_40">Telephonic</label>
			<input name="assessment_method[]" value="In_Person" type="radio" id="basic_checkbox_41" class="filled-in" />
			<label class="col-lg-6 text-left" for="basic_checkbox_41">In Person</label>
		</div>
	</div>
</div>

<div class="col-lg-12 text-center">
	<div class="col-lg-12">
		<div class="col-lg-12">
			<div class="row clearfix">
				<h4 class="card-inside-title">Patient's Condition</h4>
				<div class="col-md-6" style="width:45%;border: 1px solid #ccc;height:30px;border-radius: 4px;margin-left:1%">
					<input name="patient_mobility[]" value="Mobile" type="radio" id="basic_checkbox_1" class="filled-in" />
					<label class="col-lg-4 text-left" for="basic_checkbox_1">Mobile</label>
					<input name="patient_mobility[]" value="Bedridden" type="radio" id="basic_checkbox_3" class="filled-in" />
					<label class="col-lg-4 text-left" for="basic_checkbox_3">Bedridden</label>
					<input name="patient_mobility[]" value="Semi-bedridden" type="radio" id="basic_checkbox_5" class="filled-in" />
					<label class="col-lg-4 text-left" for="basic_checkbox_5">Semi Bedridden</label>
				</div>
				<div class="col-md-6" style="width:45%;border: 1px solid #ccc;height:30px;border-radius: 4px;margin-left:2%">
					<input name="patient_consciousness[]" value="Conscious" type="radio" id="basic_checkbox_2" class="filled-in" />
					<label class="col-lg-4 text-left" for="basic_checkbox_2">Conscious</label>
					<input name="patient_consciousness[]" value="Unconscious" type="radio" id="basic_checkbox_6" class="filled-in" />
					<label class="col-lg-4 text-left" for="basic_checkbox_6">Unconscious</label>
					<input name="patient_consciousness[]" value="Semi-conscious" type="radio" id="basic_checkbox_4" class="filled-in" />
					<label class="col-lg-4 text-left" for="basic_checkbox_4">Semi Conscious</label>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="col-lg-12 text-center">
	<div class="col-lg-12">
		<div class="col-lg-12">
			<div class="row clearfix">
				<h4 class="card-inside-title">Medical Procedures</h4>
				<div class="basics col-lg-12">
				    <input name="medical_procedures[]" value="Bathing/Personal Hygeine" type="checkbox" id="basic_checkbox_28" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_28">Bathing/Personal Hygeine</label>
				    <input name="medical_procedures[]" value="Ambulation/Exercise" type="checkbox" id="basic_checkbox_29" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_29">Ambulation/Exercise</label>
				    <input name="medical_procedures[]" value="Bed Pan Support" type="checkbox" id="basic_checkbox_30" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_30">Bed Pan Support</label>
				    <input name="medical_procedures[]" value="Diaper Change" type="checkbox" id="basic_checkbox_31" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_31">Diaper Change</label>
				    <input name="medical_procedures[]" value="Administration Medication" type="checkbox" id="basic_checkbox_32" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_32">Administration Medication</label>
				    <input name="medical_procedures[]" value="Dressing" type="checkbox" id="basic_checkbox_33" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_33">Dressing</label>
				    <input name="medical_procedures[]" value="Grooming" type="checkbox" id="basic_checkbox_7" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_7">Grooming</label>
				    <input name="medical_procedures[]" value="Oral Care" type="checkbox" id="basic_checkbox_8" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_8">Oral Care</label>
				    <input name="medical_procedures[]" value="Feeding" type="checkbox" id="basic_checkbox_9" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_9">Feeding</label>
				    <input name="medical_procedures[]" value="Back Care" type="checkbox" id="basic_checkbox_10" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_10">Back Care</label>
				    <input name="medical_procedures[]" value="Monitor Vital Signs" type="checkbox" id="basic_checkbox_11" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_11">Monitor Vital Signs</label>
				    <input name="medical_procedures[]" value="Ryles Tube Feeding" type="checkbox" id="basic_checkbox_12" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_12">Ryles Tube Feeding</label>
				    <input name="medical_procedures[]" value="Peg Tube" type="checkbox" id="basic_checkbox_13" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_13">Peg Tube</label>
				    <input name="medical_procedures[]" value="Monitor Emotional Well Being" type="checkbox" id="basic_checkbox_14" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_14">Monitor Emotional Well Being</label>
				    <input name="medical_procedures[]" value="Catheter care" type="checkbox" id="basic_checkbox_15" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_15">Catheter care</label>
				    <input name="medical_procedures[]" value="Monitor Vitals" type="checkbox" id="basic_checkbox_16" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_16">Monitor Vitals</label>
				    <input name="medical_procedures[]" value="Injections" type="checkbox" id="basic_checkbox_17" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_17">Injections</label>
				    <input name="medical_procedures[]" value="Wound Care" type="checkbox" id="basic_checkbox_18" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_18">Wound Care</label>
				    <input name="medical_procedures[]" value="C PAP" type="checkbox" id="basic_checkbox_19" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_19">C PAP</label>
				    <input name="medical_procedures[]" value="BI PAP" type="checkbox" id="basic_checkbox_20" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_20">BI PAP</label>
				    <input name="medical_procedures[]" value="Catheter Change" type="checkbox" id="basic_checkbox_21" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_21">Catheter Change</label>
				    <input name="medical_procedures[]" value="Colonstomy care" type="checkbox" id="basic_checkbox_22" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_22">Colonstomy care</label>
				    <input name="medical_procedures[]" value="IV/IM/SC Injection" type="checkbox" id="basic_checkbox_23" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_23">IV/IM/SC Injection</label>
				    <input name="medical_procedures[]" value="Tracheotomy Care" type="checkbox" id="basic_checkbox_24" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_24">Tracheotomy Care</label>
				    <input name="medical_procedures[]" value="Suctioning" type="checkbox" id="basic_checkbox_25" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_25">Suctioning</label>
				    <input name="medical_procedures[]" value="O2 Monitoring" type="checkbox" id="basic_checkbox_26" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_26">O2 Monitoring</label>
				    <input name="medical_procedures[]" value="Ventilator" type="checkbox" id="basic_checkbox_27" class="filled-in" />
				    <label class="col-lg-2 text-left" for="basic_checkbox_27">Ventilator</label>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-lg-12 text-center">
	<div class="row clearfix">
		<h4 class="card-inside-title">Suggested Home Care Professional</h4>
		<div class="basics col-lg-12 col-md-offset-2">
			<div class="col-md-3 pull-left">
				<input name="suggested_home_care_professional" value="Registered Nurse-Male" type="radio" id="basic_checkbox_34" class="filled-in with-gap" />
				<label class="col-md-12 text-left" for="basic_checkbox_34">Registered Nurse-Male</label>
				<input name="suggested_home_care_professional" value="Registered Nurse-Female" type="radio" id="basic_checkbox_35" class="filled-in with-gap" />
				<label class="col-md-12 text-left" for="basic_checkbox_35">Registered Nurse-Female</label>
			</div>
			<div class="col-md-3 pull-left">
				<input name="suggested_home_care_professional" value="Associate Nurse-Male" type="radio" id="basic_checkbox_36" class="filled-in with-gap" />
				<label class="col-md-12 text-left" for="basic_checkbox_36">Associate Nurse-Male</label>
				<input name="suggested_home_care_professional" value="Associate Nurse-Female" type="radio" id="basic_checkbox_37" class="filled-in with-gap" />
				<label class="col-md-12 text-left" for="basic_checkbox_37">Associate Nurse-Female</label>
			</div>
			<div class="col-md-3 pull-left">
				<input name="suggested_home_care_professional" value="Nursing Attendent-Male" type="radio" id="basic_checkbox_38" class="filled-in with-gap" />
				<label class="col-md-12 text-left" for="basic_checkbox_38">Nursing Attendent-Male</label>
				<input name="suggested_home_care_professional" value="Nursing Attendent-Female" type="radio" id="basic_checkbox_39" class="filled-in with-gap" />
				<label class="col-md-12 text-left" for="basic_checkbox_39">Nursing Attendent-Female</label>
			</div>
		</div>
	</div>
</div>

<div class="clearfix"></div><br>

<div class="col-sm-6" style="display: inline-block;">
	<div class="row clearfix">
		<div class="col-lg-2 col-md-3 col-sm-5 col-xs-5 form-control-label">
			<label for="assessment_status">Status</label>
		</div>
		<div class="col-md-10" style="border: 1px solid #ccc;height:30px;border-radius: 4px;">
			<input name="assessment_status" value="Pending" type="radio" id="basic_checkbox_42" class="filled-in" checked="checked" />
			<label class="col-lg-4 text-left" for="basic_checkbox_42">Pending</label>
			<input name="assessment_status" value="Fit" type="radio" id="basic_checkbox_43" class="filled-in" />
			<label class="col-lg-4 text-left" for="basic_checkbox_43">Fit</label>
			<input name="assessment_status" value="Unfit" type="radio" id="basic_checkbox_44" class="filled-in" />
			<label class="col-lg-4 text-left" for="basic_checkbox_44">Unfit</label>
		</div>
	</div>
</div>

<div class="col-sm-6" style="display: inline-block;">
	<div class="row clearfix">
		<div class="col-lg-6 form-control-label" style="text-align: right;">
			<label for="notes">Other Remarks/Comment</label>
		</div>
		<div class="col-lg-6">
			<div class="form-group">
				<div class="form-line">
					<textarea rows="1" class="form-control no-resize notes" name="notes" placeholder="Notes"></textarea>
				</div>
			</div>
		</div>
	</div>
</div>