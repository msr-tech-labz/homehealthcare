<div class="row clearfix">
	<div class="row">
		<div class="col-sm-6">
			<div class="row clearfix">
				<div class="col-lg-4 col-md-3 col-sm-5 col-xs-5 form-control-label">
					<label for="assessment_date">Assessment Date</label>
				</div>
				<div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
					<div class="form-group">
						<div class="form-line">
							<input type="text" id="assessment_date" name="assessment_date" class="form-control" placeholder="Assessment Date" required>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="row clearfix">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
					<label for="assessor_name">Assessed By</label>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
					<div class="form-group">
						<div class="form-line">
							<input type="text" id="assessor_name" name="assessor_name" class="form-control" placeholder="Assessed By" required>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-lg-12 text-center">
	<label><h4>General Details</h4></label>
	<div class="col-lg-12">
		<div class="col-lg-6">
			<div class="row clearfix">
				<div class="col-lg-5 col-md-2 col-sm-4 col-xs-5 form-control-label">
					<label for="chief_complains">Chief Complaints</label>
				</div>
				<div class="col-lg-7 col-md-10 col-sm-8 col-xs-7">
					<div class="form-group">
						<div class="form-line">
							<textarea rows="1" class="form-control no-resize chief_complaints" name="chief_complaints" placeholder="Please type the problems associated with patient"></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-lg-5 col-md-2 col-sm-4 col-xs-5 form-control-label">
					<label for="diagnosis">Diagnosis</label>
				</div>
				<div class="col-lg-7 col-md-10 col-sm-8 col-xs-7">
					<div class="form-group">
						<div class="form-line">
							<textarea rows="1" class="form-control no-resize diagnosis" name="diagnosis" placeholder="Please type what Diagnosis is required"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="row clearfix">
				<div class="col-lg-5 col-md-2 col-sm-4 col-xs-5 form-control-label">
					<label for="symptoms">Symptoms</label>
				</div>
				<div class="col-lg-7 col-md-10 col-sm-8 col-xs-7">
					<div class="form-group">
						<div class="form-line">
							<textarea rows="1" class="form-control no-resize symptoms" name="symptoms" placeholder="Symptoms associated with the Patient"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-lg-12 text-center">
	<label><h4>Pain</h4></label>
	<div class="col-lg-12">
		<div class="col-lg-6">
			<div class="row clearfix">
				<div class="col-lg-5 col-md-2 col-sm-4 col-xs-5 form-control-label">
					<label for="site">Site</label>
				</div>
				<div class="col-lg-7 col-md-10 col-sm-8 col-xs-7">
					<div class="form-group">
						<div class="form-line">
							<textarea rows="1" class="form-control no-resize sites" name="sites" placeholder="Affected Site(s)"></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-lg-5 col-md-2 col-sm-4 col-xs-5 form-control-label">
					<label for="side">Side</label>
				</div>
				<div class="col-lg-7 col-md-10 col-sm-8 col-xs-7">
					<div class="form-group">
						<div class="form-line">
							<textarea rows="1" class="form-control no-resize sides" name="sides" placeholder="Affected Side(s)"></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-lg-5 col-md-2 col-sm-4 col-xs-5 form-control-label">
					<label for="duration">Duration</label>
				</div>
				<div class="col-lg-7 col-md-10 col-sm-8 col-xs-7">
					<div class="form-group">
						<div class="form-line">
							<textarea rows="1" class="form-control no-resize duration" name="duration" placeholder="How long the patient has been in pain?"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="row clearfix">
				<div class="col-lg-5 col-md-2 col-sm-4 col-xs-5 form-control-label">
					<label for="aggrevating_factors">Aggrevating Factors</label>
				</div>
				<div class="col-lg-7 col-md-10 col-sm-8 col-xs-7">
					<div class="form-group">
						<div class="form-line">
							<textarea rows="1" class="form-control no-resize aggrevating_factors" name="aggrevating_factors" placeholder="Factors Influencing the pain"></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-lg-5 col-md-2 col-sm-4 col-xs-5 form-control-label">
					<label for="relieving_factors">Relieving Factors</label>
				</div>
				<div class="col-lg-7 col-md-10 col-sm-8 col-xs-7">
					<div class="form-group">
						<div class="form-line">
							<textarea rows="1" class="form-control no-resize relieving_factors" name="relieving_factors" placeholder="Factors Relieving the pain"></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-md-12">
					<p><b>Pain</b></p>
					<div id="pain_slider_value" class="noUi-target noUi-ltr noUi-horizontal noUi-connect"></div>
					<div class="m-t-20 font-12"><b>Value: </b><span class="js-nouislider-value pain" name="pain" >0</span></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-lg-12 text-center">
<div class="col-lg-12">
<small>Assess the one which is applicable</small>
</div>
<div class="formselecttype">
    <input name="assessform" type="radio" id="ortho" value="ortho" />
    <label for="ortho">Orthopedic Session</label>
    <input name="assessform" type="radio" id="neuro" value="neuro" />
    <label for="neuro">Neurologic Session</label>
</div>
</div>
<div class="col-lg-12 text-center" style="border: 2px solid black;padding: 15px;">
	<div class="col-lg-12 text-center showformtype" id="blank">
		<b>Select a Session</b>
	</div>
	<div class="col-lg-12 text-center showformtype formortho hide" id="orthoform">
		<div class="col-lg-6">
			<div class="row clearfix">
				<div class="col-lg-5 col-md-2 col-sm-4 col-xs-5 form-control-label">
					<label for="rom">Range of Motion</label>
				</div>
				<div class="col-lg-7 col-md-10 col-sm-8 col-xs-7">
					<div class="form-group">
						<div class="form-line">
							<textarea rows="1" class="form-control no-resize rom" name="rom" placeholder="Range of Motion of the patient"></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-lg-5 col-md-2 col-sm-4 col-xs-5 form-control-label">
					<label for="gait">Gait</label>
				</div>
				<div class="col-lg-7 col-md-10 col-sm-8 col-xs-7">
					<div class="form-group">
						<div class="form-line">
							<textarea rows="1" class="form-control no-resize gait" name="gait" placeholder="Gait of the patient"></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-lg-5 col-md-2 col-sm-4 col-xs-5 form-control-label">
					<label for="posture">Posture</label>
				</div>
				<div class="col-lg-7 col-md-10 col-sm-8 col-xs-7">
					<div class="form-group">
						<div class="form-line">
							<textarea rows="1" class="form-control no-resize posture" name="posture" placeholder="Posture of the Patient"></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-lg-5 col-md-2 col-sm-4 col-xs-5 form-control-label">
					<label for="deformity">Deformity</label>
				</div>
				<div class="col-lg-7 col-md-10 col-sm-8 col-xs-7">
					<div class="form-group">
						<div class="form-line">
							<textarea rows="1" class="form-control no-resize deformity" name="deformity" placeholder="Deformity of the Patient"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="row clearfix">
				<div class="col-md-12">
					<p><b>Muscle Grade</b></p>
					<div id="musclegrade_slider_value" class="noUi-target noUi-ltr noUi-horizontal noUi-connect"></div>
					<div class="m-t-20 font-12"><b>Value: </b><span class="js-nouislider-value mus_gra" name="mus_gra" >0</span></div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="formselecttype" style="text-align: left;">
					<p><b>Tone Type</b></p>
					<input name="ortho_tone" type="radio" id="hyper" value="hyper" />
					<label for="hyper">Hyper Toned</label>
					<input name="ortho_tone" type="radio" id="hypo" value="hypo" />
					<label for="hypo">Hypo Toned</label>
				</div>
			</div>
			<div class="row clearfix">
				<p><b>Swelling</b></p>
				<div class="col-lg-12 col-md-10 col-sm-8 col-xs-7">
					<div class="form-group">
						<div class="form-line">
							<textarea rows="1" class="form-control no-resize swelling" name="swelling" placeholder="Swelling of the patient"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-12 text-center showformtype formneuro hide" id="neuroform">
		<div class="col-lg-6">
			<div class="row clearfix">
				<div class="col-lg-5 col-md-2 col-sm-4 col-xs-5 form-control-label">
					<label for="loc">Level of Consiousness</label>
				</div>
				<div class="col-lg-7 col-md-10 col-sm-8 col-xs-7">
					<div class="form-group">
						<div class="form-line">
							<textarea rows="1" class="form-control no-resize loc" name="loc" placeholder="Level of Consiousness of the patient"></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-lg-5 col-md-2 col-sm-4 col-xs-5 form-control-label">
					<label for="orientation">Orientation</label>
				</div>
				<div class="col-lg-7 col-md-10 col-sm-8 col-xs-7">
					<div class="form-group">
						<div class="form-line">
							<textarea rows="1" class="form-control no-resize orientation" name="orientation" placeholder="Orientation of the patient"></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-lg-5 col-md-2 col-sm-4 col-xs-5 form-control-label">
					<label for="comprehension">Comprehension</label>
				</div>
				<div class="col-lg-7 col-md-10 col-sm-8 col-xs-7">
					<div class="form-group">
						<div class="form-line">
							<textarea rows="1" class="form-control no-resize comprehension" name="comprehension" placeholder="Comprehension of the Patient"></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-lg-5 col-md-2 col-sm-4 col-xs-5 form-control-label">
					<label for="memory">Memory</label>
				</div>
				<div class="col-lg-7 col-md-10 col-sm-8 col-xs-7">
					<div class="form-group">
						<div class="form-line">
							<textarea rows="1" class="form-control no-resize memory" name="memory" placeholder="Memory of the patient"></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-lg-5 col-md-2 col-sm-4 col-xs-5 form-control-label">
					<label for="cne">Cranial Nerve Examination</label>
				</div>
				<div class="col-lg-7 col-md-10 col-sm-8 col-xs-7">
					<div class="form-group">
						<div class="form-line">
							<textarea rows="1" class="form-control no-resize cne" name="cne" placeholder="Cranial Nerve Examination of the Patient"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
		<div class="row clearfix">
			<div class="col-lg-5 col-md-2 col-sm-4 col-xs-5 form-control-label">
				<label for="pressure_sore">Pressure Sore</label>
			</div>
			<div class="col-lg-7 col-md-10 col-sm-8 col-xs-7">
				<div class="form-group">
					<div class="form-line">
						<textarea rows="1" class="form-control no-resize pressure_sore" name="pressure_sore" placeholder="Pressure Sore of the Patient"></textarea>
					</div>
				</div>
			</div>
		</div>
			<div class="row clearfix">
				<div class="formselecttype" style="text-align: left;">
					<p><b>Balance</b></p>
					<input name="balance" type="radio" id="sitting" value="sitting" />
					<label for="sitting">Sitting</label>
					<input name="balance" type="radio" id="standing" value="standing" />
					<label for="standing">Standing</label>
					<input name="balance" type="radio" id="lying" value="lying" />
					<label for="lying">Lying</label>
				</div>
			</div>
			<div class="row clearfix">
				<div class="formselecttype" style="text-align: left;">
					<p><b>Speech</b></p>
					<input name="speech" type="radio" id="sensory" value="sensory" />
					<label for="sensory">Sensory</label>
					<input name="speech" type="radio" id="motor" value="motor" />
					<label for="motor">Motor</label>
					<input name="speech" type="radio" id="global" value="global" />
					<label for="global">Global</label>
				</div>
			</div>
			<div class="row clearfix">
				<div class="formselecttype" style="text-align: left;">
					<p><b>Tone</b></p>
					<input name="neuro_tone" type="radio" id="spasticity" value="spasticity" />
					<label for="spasticity">Spasticity</label>
					<input name="neuro_tone" type="radio" id="flaccidity" value="flaccidity" />
					<label for="flaccidity">Flaccidity</label>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-lg-12 text-center">
	<label><h4>Final Observations</h4></label>
	<div class="col-lg-12">
		<div class="col-lg-6">
		<div class="row clearfix">
			<div class="col-lg-5 col-md-2 col-sm-4 col-xs-5 form-control-label">
				<label for="treatment_plan">Treatment Plan</label>
			</div>
			<div class="col-lg-7 col-md-10 col-sm-8 col-xs-7">
				<div class="form-group">
					<div class="form-line">
						<textarea rows="1" class="form-control no-resize treatment_plan" name="treatment_plan" placeholder="Treatment(s) recommended for the patient"></textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="row clearfix">
			<div class="col-lg-5 col-md-2 col-sm-4 col-xs-5 form-control-label">
				<label for="precautions">Precautions</label>
			</div>
			<div class="col-lg-7 col-md-10 col-sm-8 col-xs-7">
				<div class="form-group">
					<div class="form-line">
						<textarea rows="1" class="form-control no-resize precautions" name="precautions" placeholder="Precautions recommended for the patient"></textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="row clearfix">
			<div class="col-lg-5 col-md-2 col-sm-4 col-xs-5 form-control-label">
				<label for="exercise_program">Exercise Program</label>
			</div>
			<div class="col-lg-7 col-md-10 col-sm-8 col-xs-7">
				<div class="form-group">
					<div class="form-line">
						<textarea rows="1" class="form-control no-resize exercise_program" name="exercise_program" placeholder="Exercises suggested"></textarea>
					</div>
				</div>
			</div>
		</div>
		</div>
		<div class="col-lg-6">
		<div class="row clearfix">
			<div class="col-lg-5 col-md-3 col-sm-5 col-xs-5 form-control-label">
				<label for="health_progress">Progess in Health</label>
			</div>
			<div class="col-lg-7 col-md-5 col-sm-5 col-xs-7">
				<div class="form-group">
					<select class="form-control show-tick" id="health_progress" name="health_progress" >
						<option value="">--Please Choose--</option>
						<option value="Improving">Improving</option>
						<option value="Unchanging">Unchanging</option>
						<option value="Worsening">Worsening</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row clearfix">
			<div class="col-lg-5 col-md-2 col-sm-4 col-xs-5 form-control-label">
				<label for="feedback">Feedback from Patient</label>
			</div>
			<div class="col-lg-7 col-md-10 col-sm-8 col-xs-7">
				<div class="form-group">
					<div class="form-line">
						<textarea rows="1" class="form-control no-resize feedback" name="feedback" placeholder="What was the feedback from the patient"></textarea>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>
