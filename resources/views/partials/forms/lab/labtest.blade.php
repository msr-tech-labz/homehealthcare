<div class="clearfix">
	<h5 class="text-center">Lab Tests<div><small>A list of Lab Tests</small></div></h5>
	<div class="col-md-12">
		<div class="col-md-12">
			<div class="col-sm-6">

				<div class="col-lg-4 col-md-3 col-sm-5 col-xs-5 form-control-label">
					<label for="assessment_date">Visit Date</label>
				</div>
				<div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
					<div class="form-group">
						<div class="form-line">
							<input type="text" id="assessment_date" name="assessment_date" class="form-control" placeholder="Assessment Date">
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">

				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
					<label for="assessor_name">Visited By</label>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
					<div class="form-group">
						<div class="form-line">
							<input type="text" id="assessor_name" name="assessor_name" class="form-control" placeholder="Assessed By">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class=col-md-6>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
					<label for="test_id">Choose Test</label>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
					<div class="form-group">
						<select class="form-control show-tick" id="test_id" name="test_id[]" multiple data-live-search="true" liveSearchStyle="contains">
							<option data-subtext="--PRICE--" disabled value="">-- Please select --</option>
							@if(isset($servicelab) && count($servicelab))
							@foreach($servicelab as $s)
							<option data-subtext="{{ $s->ratecard->amount }}" value="{{ $s->id }}" @if($s->id == $c->service_id){{'selected'}}@endif>{{ $s->service_name }}</option>
							@endforeach
							@endif
						</select>
					</div>
				</div>
			</div>
			<div class=col-md-6>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
					<label for="price_type">Choose Price</label>
				</div>
				<div class="col-lg-8  col-md-8">
					<div class="form-group">
						<select class="form-control show-tick" id="price_type" name="price_type">
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="col-md-6">
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-control-label" style="text-align: left;">
					<label for="notes">Comments (If any)</label>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
					<div class="form-group">
						<div class="form-line">
							<textarea rows="1" class="form-control no-resize notes" id="labNotes" name="notes" placeholder="Notes"></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="col-lg-4 col-md-4" style="text-align: right;top: 10px;">
					<label for="Test">Sum Total</label>
				</div>
				<div class="col-lg-8 col-md-8" style="top: 10px;font-weight: bold;">
					<div id='sum'>
					</div>
				</div>
			</div>
		</div>
		<div class="text-center">
			<button type="button" class="btn btn-success waves-effect btnsaveLabTest">Save</button>
		</div>
	</div>
</div>