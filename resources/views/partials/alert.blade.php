
@if(session('subscription_payment_warning'))
<div class="alert bg-amber" role="alert" style="color:#ff0000 !important">
    {{ session('subscription_payment_warning') }} <a href="{{ url('/subscription') }}">Click here</a> to pay for subscription to have an uninterrupted service.
</div>
@endif

@if(session('trail_period_warning'))
<div class="alert alert-danger" role="alert">
    {{ session('trail_period_warning') }}
</div>
@endif

@if(session('alert_success'))
<div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    {{ session('alert_success') }}
</div>
@endif

@if(session('alert_error'))
<div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    {{ session('alert_error') }}
</div>
@endif

@if(session('alert_warning'))
<div class="alert alert-warning alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    {{ session('alert_warning') }}
</div>
@endif

@if(session('alert_info'))
<div class="alert alert-info alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    {{ session('alert_info') }}
</div>
@endif
