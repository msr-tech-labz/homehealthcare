<div class="table-responsive">
  <table style="zoom:85%;" class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTableDecline">
   <thead>
     <tr>
       <th width="3%">#</th>
       <th width="15%">Caregiver Name</th>
       <th>Date Received</th>
       <th>Leave Date</th>
       <th width="15%">Reason</th>
       <th>Type of leave</th>
       <th>Status</th>
       <th>Approver</th>
       <th>Declined By</th>
     </tr>
   </thead>
   <tbody>
     @forelse($declinedLeaves as $i => $dl)
     <tr>
       <td>{{ $i + 1 }}</td>
       <td>{{ (empty($dl->name) && isset($dl->caregiver))?$dl->caregiver->first_name.' '.$dl->caregiver->last_name:$dl->name }}</td>
       <td>{{ $dl->created_at->format('d-m-Y') }}</td>
       <td>{{ $dl->date->format('d-m-Y') }}</td>
       <td>{{ $dl->reason }}</td>
       <td>{{ $dl->type }}</td>
       <td>{{ $dl->status }}</td>
       <td>{{ $dl->approver->full_name ?? 'Super Admin' }}</td>
       <td>{{ $dl->approvedby->full_name ?? 'Super Admin' }}</td>
     </tr>
     @empty
     <tr>
       <td colspan="9" class="text-center">No record(s) found.</td>
     </tr>
     @endforelse
   </tbody>
 </table>
 {{ $declinedLeaves->links() }}
 <p class="text-right" style="margin-bottom:0;">
  {{ 'Showing '.$declinedLeaves->firstItem().' to '.$declinedLeaves->lastItem().' out of Total '.$declinedLeaves->total() }}
</p>
</div>
