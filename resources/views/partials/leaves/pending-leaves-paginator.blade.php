<div class="table-responsive">
  <table style="zoom:85%;" class="table table-bordered table-striped table-hover table-condensed js-basic-example">
   <thead>
     <tr>
       <th width="3%">#</th>
       <th width="15%">Caregiver Name</th>
       <th>Date Received</th>
       <th>Leave Date</th>
       <th width="15%">Reason</th>
       <th>Type of leave</th>
       <th>Status</th>
       <th>Approver</th>
       <th style="text-align: center;">Action</th>
     </tr>
   </thead>
   <tbody>
     @forelse($pendingLeaves as $i => $pl)
     <tr>
       <td>{{ $i + 1 }}</td>
       <td>{{ $pl->caregiver->fullname }}</td>
       <td>{{ $pl->created_at->format('d-m-Y') }}</td>
       <td>{{ $pl->date->format('d-m-Y') }}</td>
       <td>{{ $pl->reason }}</td>
       <td>{{ $pl->type }}</td>
       <td>{{ $pl->status }}</td>
       <td>{{ $pl->approver->fullname ?? 'Super Admin' }}</td>
       <td style="text-align: center;"><a href="javascript:void(0);" data-leave-id="{{ Helper::encryptor('encrypt',$pl->id) }}" data-emp="{{ $pl->caregiver->fullname }}" data-employee-id="{{ $pl->caregiver->employee_id }}" data-date="{{ $pl->date->format('d-m-Y') }}" data-type="{{ $pl->type }}" data-reason="{{ $pl->reason }}" data-approver-id="{{ Helper::encryptor('encrypt',$pl->approver_id) }}" class="btn btn-primary btn-lg respondLeave">View Request</a></td>
     </tr>
     @empty
     <tr>
       <td colspan="9" class="text-center">
         No record(s) found.
       </td>
     </tr>
     @endforelse
   </tbody>
 </table>
 {{ $pendingLeaves->links() }}
 <p class="text-right" style="margin-bottom:0;">
  {{ 'Showing '.$pendingLeaves->firstItem().' to '.$pendingLeaves->lastItem().' out of Total '.$pendingLeaves->total() }}
</p>
</div>
