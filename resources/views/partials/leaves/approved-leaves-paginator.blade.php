<div class="table-responsive">
  <table style="zoom:85%;" class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTableApprove">
   <thead>
     <tr>
       <th width="3%">#</th>
       <th width="15%">Caregiver Name</th>
       <th>Date Received</th>
       <th>Leave Date</th>
       <th width="15%">Reason</th>
       <th>Type of leave</th>
       <th>Status</th>
       <th>Approver</th>
       <th>Approved By</th>
       <th class="text-center">Action</th>
     </tr>
   </thead>
   <tbody>
     @forelse($approvedLeaves as $i => $al)
     <tr>
       <td>{{ $i + 1 }}</td>
       <td>{{ $al->caregiver->fullname }}</td>
       <td>{{ $al->created_at->format('d-m-Y') }}</td>
       <td>{{ $al->date->format('d-m-Y') }}</td>
       <td>{{ $al->reason }}</td>
       <td>{{ $al->type }}</td>
       <td>{{ $al->status }}</td>
       <td>{{ $al->approver->full_name ?? 'Super Admin' }}</td>
       <td>{{ $al->approvedby->full_name ?? 'Super Admin' }}</td>
       <td style="text-align: center;"><a data-toggle='modal' data-target='#leaveEditModal' data-leave-id="{{ Helper::encryptor('encrypt',$al->id) }}" data-caregiver-name="{{ $al->caregiver->fullname }}" data-employee-id="{{ $al->caregiver->employee_id }}" data-date="{{ $al->date->format('d-m-Y') }}" data-lType="{{ $al->type }}" data-reason="{{ $al->reason }}" data-approver-id="{{ Helper::encryptor('encrypt',$al->approver_id) }}" class='btn waves-effect btn-warning leaveEdit' title='Edit leave'>Edit</a>
        <a href="{{ route('leaves.leavesrevoke',['id' => Helper::encryptor('encrypt',$al->id)]) }}" onclick=\"return confirm('Are you sure to revoke the Leave?')\" class='btn waves-effect btn-danger' style='float: right;' title='Cancel leave'>Revoke</a></td>
      </tr>
      @empty
      <tr>
       <td colspan="10" class="text-center">
         No record(s) found.
       </td>
     </tr>
     @endforelse
   </tbody>
 </table>
{{ $approvedLeaves->links() }}
<p class="text-right" style="margin-bottom:0;">
  {{ 'Showing '.$approvedLeaves->firstItem().' to '.$approvedLeaves->lastItem().' out of Total '.$approvedLeaves->total() }}
</p>
</div>