<table class="table table-bordered table-striped leadsTable">
    <thead>
        <tr>
            <th>#</th>
            <th>Status</th>
            <th>Date</th>
            <th>Source</th>
            <th>Episode Id</th>
            <th>Patient</th>
            <th>Enquirer</th>
            <th>Phone</th>
            <th>Location</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($tableLeads as $i => $lead)
        <tr>
            <td> {{ ++$i }} </td>
            <td> {{ $lead->status }} </td>
            <td> {{ $lead->created_at->format('d-m-y') }} </td>
            <td> {{ !empty($lead->referralSource)?$lead->referralSource->source_name:'-' }} </td>
            <td> {{ !empty($lead->episode_id)?$lead->episode_id:'TEMPLD0AF'.$lead->id }} </td>
            <td> {{ !empty($lead->patient->full_name)?$lead->patient->full_name:'-' }} </td>
            <td> {{ !empty($lead->patient->enquirer_name)?$lead->patient->enquirer_name:'-' }} </td>
            <td> {{ isset($lead->patient->contact_number)?$lead->patient->contact_number:'' }} </td>
            <td> {{ (isset($lead->patient) && !empty($lead->patient->city))?$lead->patient->city:'-' }} </td>
            <?php if($lead->aggregator_lead && $lead->aggregator_lead_status == 'Pending'){
                if(\Entrust::hasRole('admin') || \Entrust::can('leads-view')){
                    $patientData = json_encode(collect($lead->patient)->only('first_name','last_name','patient_age','patient_weight','gender','street_address','area','city'));
                    $leadData = json_encode(collect($lead)->only('case_description','medical_conditions','medications','procedures','aggregator_service','language_preference','gender_preference','aggregator_rate','aggregator_rate_negotiable'));
                    $html = '<a href="javascript:void(0);" target="_blank" class="btn btn-md bg-primary btnViewLead" data-id="'. \Helper::encryptor('encrypt',$lead->id).'" data-patient-id="'.\Helper::encryptor('encrypt',$lead->patient_id).'" data-patient="'.str_replace("\"", "'", $patientData).'" data-case="'.str_replace("\"", "'", $leadData).'" title="View Lead Details">View</a>';
                }
            }elseif($lead->aggregator_lead && $lead->aggregator_lead_status == 'Accepted'){
                if(\Entrust::hasRole('admin') || \Entrust::can('leads-view')){
                    $html = '<a href="'.route('lead.view',\Helper::encryptor('encrypt',$lead->id)).'" class="btn btn-md bg-primary" data-id="'.\Helper::encryptor('encrypt',$lead->id).'" title="Create Lead">View</a>';
                }
            }elseif(!$lead->aggregator_lead && $lead->aggregator_lead_status == 'Pending'){
                if(\Entrust::hasRole('admin') || \Entrust::can('leads-view')){
                    $html = '<a href="'.route('lead.view',\Helper::encryptor('encrypt',$lead->id)).'" class="btn btn-md bg-primary" data-id="'.\Helper::encryptor('encrypt',$lead->id).'" title="View Lead">View</a>';
                }
            } 
            ?>
            <td> {? echo html_entity_decode($html); ?} </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ $tableLeads->links() }}
<p class="text-right" style="margin-bottom:0;">
    {{ 'Showing '.$tableLeads->firstItem().' to '.$tableLeads->lastItem().' out of Total '.$tableLeads->total() }}
</p>