<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<div class="card care-plan-card visitViewCard">
    <div class="header bg-cyan">
        <h2>
            View Schedule
        </h2>
    </div>
    <div class="body form-horizontal" style="padding-top: 5px !important">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#schedule_view_tab" data-toggle="tab" aria-expanded="true">
                    <i class="material-icons">comment</i> View Schedule
                </a>
            </li>
            <li role="presentation" class="">
                <a href="#schedule_staff_replacement_tab" data-toggle="tab" aria-expanded="true">
                    <i class="material-icons">child_friendly</i> Staff Replacement
                </a>
            </li>
            <li role="presentation" class="">
                <a href="#schedule_interruptions_tab" data-toggle="tab" aria-expanded="true">
                    <i class="material-icons">child_friendly</i> Service Interruptions
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade active in" id="schedule_view_tab">
                <div class="col-sm-6">
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                            <label for="visit_type">Visit Type</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <div class="form-group" style="margin-top: 1%">
                                <input name="visit_type" type="radio" id="visit_type_single" class="with-gap radio-col-deep-purple visit_type" value="Single" checked="">
                                <label for="visit_type_single">Single</label>
                                <input name="visit_type" type="radio" id="visit_type_period" class="with-gap radio-col-deep-purple visit_type" value="Period">
                                <label for="visit_type_period">Period</label>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                            <label for="no_of_hours">Period</label>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="start_date" name="start_date" class="form-control date" placeholder="Start Date">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 form-control-label toLabelBlock" style="display:none">
                            <label class="text-center">To</label>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 toDateBlock" style="display:none">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="end_date" name="end_date" class="form-control date" placeholder="End Date">
                                </div>
                            </div>
                        </div>
                    </div><br>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                            <label for="service_required">Service Required</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <div class="form-group">
                                <select class="form-control" id="service_required" name="service_required" data-live-search="true">
                                    <option value="">-- Select --</option>
                            @if(isset($services) && count($services))
                                @foreach ($services as $service)
                                    <option value="{{ $service->id }}">{{ $service->service_name }}</option>
                                @endforeach
                            @endif
                                </select>
                            </div>
                        </div>
                    </div><br>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"></div>
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                            <a class="btn btn-warning btn-block waves-effect btnChooseStaff" onclick="javascript: return clearSearchForm();">Choose Staff</a>
                        </div>
                    </div><br>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                            <label for="no_of_hours">Staff / Caregiver</label>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <div style="margin-top: 8px;" id="selectedStaffName_0">-</div>
                                <input type="hidden" id="caregiver_id" name="caregiver_id" value=""/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                            <label for="charge_type">Charge Type</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <div class="form-group" style="margin-top: 1%">
                                <input name="charge_type" type="radio" id="charge_type_single" class="with-gap radio-col-deep-purple charge_type" value="RateCard" checked="">
                                <label for="charge_type_single">Pick From Ratecard</label>
                                <input name="charge_type" type="radio" id="charge_type_period" class="with-gap radio-col-deep-purple charge_type" value="Custom">
                                <label for="charge_type_period">Custom</label>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix visit_rate_card_block">
                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                            <label for="visit_rate_card">Rate Card</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <div class="form-group">
                                <select class="form-control" id="visit_rate_card" name="visit_rate_card" data-live-search="true">
                                    <option value="">-- Select --</option>
                            @if(isset($l->ratecards) && count($l->ratecards))
                                @foreach($l->ratecards as $rc)
                                    <option value="{{ $rc->id.'_'.$rc->total_amount }}">{{ $rc->ratecard_name.' - Rs. '.$rc->total_amount }}</option>
                                @endforeach
                            @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                            <label for="visit_amount">Amount</label>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="number" id="visit_amount" name="rate" class="form-control" placeholder="Ex. 200.00"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                            <label for="other_charges">Other Charges</label>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="number" id="other_charges" name="other_charges" class="form-control" placeholder="Ex. 200.00"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                            <label for="total_amount">Total Amount</label>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="number" id="total_amount" name="total_amount" class="form-control" placeholder=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                            <label for="no_of_hours">Notes</label>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <textarea id="notes" name="notes" class="form-control" placeholder="Notes (if any)"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 clearfix text-center">
                    <input type="hidden" name="lead_id" value="{{ Helper::encryptor('encrypt',$l->id) }}" />
                    <input type="hidden" name="patient_id" value="{{ Helper::encryptor('encrypt',$l->patient_id) }}" />
                    <button type="submit" class="btn btn-success btn-lg waves-effect btnSaveSchedules">Save Schedule</a>
                </div>
                <div class="clearfix"></div>
            </div>

            <div role="tabpanel" class="tab-pane fade in" id="schedule_staff_replacement_tab">
                <div class="col-sm-7">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Staff</th>
                                <th>Comments</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="4" class="text-center">No record(s) found</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-5 form-horizontal">
                    <h2 class="card-inside-title">
                        Replace Staff
                    </h2>
                    <form id="replaceStaffForm">
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 form-control-label">
                                <label for="replacement_date">Replacement Date</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="replacement_date" name="replacement_date" class="form-control date" placeholder="Replacement Date" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"></div>
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                <a class="btn btn-warning btn-block waves-effect btnChooseStaff" onclick="javascript: return clearSearchForm();">Choose Staff</a>
                            </div>
                        </div><br>
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 form-control-label">
                                <label for="no_of_hours">Staff / Caregiver</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <div class="form-group">
                                    <div style="margin-top: 8px;" id="selectedStaffName_0">-</div>
                                    <input type="hidden" id="caregiver_id" name="caregiver_id" value=""/>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 form-control-label">
                                <label for="replacement_comments">Comments</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <textarea class="form-control" id="replacement_comments" name="replacement_comments" placeholder="Comments"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7 text-center">
                                <button type="submit" class="btn btn-success btnReplaceStaff">Replace Staff</button>
                            </div>
                        </div><br>
                    </form>
                </div>
            </div>

            <div role="tabpanel" class="tab-pane fade in" id="schedule_interruptions_tab">
                <div class="col-sm-8">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>No. of Days</th>
                                <th>Entered By</th>
                                <th>Reason</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="4" class="text-center">No record(s) found</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-4 form-horizontal">
                    <h2 class="card-inside-title">
                        Add Service Interruptions
                    </h2>
                    <form id="serviceInterruptionForm">
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 form-control-label">
                                <label for="no_of_days">No. of days</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="number" id="no_of_days" name="no_of_days" class="form-control" placeholder="No. of Days" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 form-control-label">
                                <label for="interruption_reason">Reason</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <textarea class="form-control" id="interruption_reason" name="interruption_reason" placeholder="Reason"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7 text-center">
                                <button type="submit" class="btn btn-success btnSaveServiceInterruption">Save</button>
                            </div>
                        </div><br>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<!-- Select Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
