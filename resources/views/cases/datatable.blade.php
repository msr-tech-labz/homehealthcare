@extends('layouts.main-layout')

@section('page_title','Cases - ')

@section('page.styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
    <style>
        .plus:after{
          content: ' + ';
          font-size: 18px;
          font-weight: bold;
      }
      table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td {
        text-transform: uppercase; !important;
      }
      table{
        width: 100% !important;
      }
    </style>
    <!-- JQuery DataTable Css -->
    {{-- <link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet"> --}}
@endsection

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <h2 class="col-sm-6">
                    Cases
                    <small>List of all cases</small>
                </h2>
                <div class="col-sm-6 text-right hide">
                    <a href="{{ route('case.create') }}" class="btn-group" role="group">
                        <button type="button" class="btn btn-primary waves-effect"><i class="material-icons">add_circle</i></button>
                        <button type="button" class="btn btn-primary waves-effect" style="line-height: 1.9;padding-left:0">New Case</button>
                    </a>&nbsp;&nbsp;
                </div>
            </div>
            <div class="body">
                <div class="col-sm-12">
                    <div class="col-sm-2" style="width: 12%">
                        <b>Filter By Status</b>
                    </div>
                    <div class="col-sm-10">
                        <input type="checkbox" id="status_pending" value="Pending" class="filled-in chk-col-amber statusFilter">
                        <label for="status_pending">Pending</label>&nbsp;&nbsp;
                        <input type="checkbox" id="status_started" value="SERVICE STARTED" class="filled-in chk-col-light-blue statusFilter">
                        <label for="status_started">Service Started</label>&nbsp;&nbsp;
                        <input type="checkbox" id="status_completed" value="SERVICE COMPLETED" class="filled-in chk-col-light-green statusFilter">
                        <label for="status_completed">Service Completed</label>&nbsp;&nbsp;
                        <input type="checkbox" id="status_caseloss" value="CASE LOSS" class="filled-in chk-col-red statusFilter">
                        <label for="status_caseloss">Case Loss</label>
                        <a class="btn btn-sm btn-success btnApplyFilter" style="margin-left: 15px">Filter</a>&nbsp;&nbsp;
                        <a class="btn btn-sm btn-danger btnResetFilter">Reset</a>
                    </div>
                    <div class="clearfix"></div>
                    @if(Helper::encryptor('decrypt',session('utype')) == 'Aggregator')
                    <div class="col-sm-2" style="width: 12%">
                        <b>Filter By Provider</b>
                    </div>
                    <div class="col-sm-10">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <div class="form-line">
                                    <select class="form-control show-tick" id="providerFilter" data-live-search="true">
                                        <option value="All">All</option>
                                    @if(isset($providers) && count($providers))
                                        @foreach ($providers as $p)
                                        <option value="{{ $p->organization_short_name }}">{{ $p->organization_name.' ['.$p->organization_city.']' }}</option>
                                        @endforeach
                                    @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <a class="btn btn-sm btn-success btnApplyProviderFilter" style="margin-left: 15px">Filter</a>&nbsp;&nbsp;
                        <a class="btn btn-sm btn-danger btnResetProviderFilter">Reset</a>
                    </div>
                    @endif
                </div>
                <div class="clearfix"></div>
                {!! $dataTable->table() !!}
            </div>
        </div>
    </div>

</div>
@endsection


@section('page.scripts')
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}" />
<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
{!! $dataTable->scripts() !!}
<script>
    $(function(){
        $( window ).load(function() {
            $('.sidebar-menu-toggle').trigger( "click" );
        });

        $('select').selectpicker();

        $('.content').on('click','.btnResetFilter', function(){
            $('#status_pending').prop('checked',false);
            $('#status_started').prop('checked',false);
            $('#status_completed').prop('checked',false);
            $('#status_caseloss').prop('checked',false);

            filterData('Status');
        });

        $('.content').on('click','.btnResetProviderFilter', function(){
            $('#providerFilter').selectpicker('val','All');
            filterData('Provider');
        });

        $('.content').on('change','.statusFilter', function(){
            //filterByStatus();
        });

        $('.content').on('click','.btnApplyFilter', function(){
            filterData('Status');
        });

        $('.content').on('click','.btnApplyProviderFilter', function(){
            filterData('Provider');
        });

    });

    function filterData(type){
        var table = $('.dataTable').DataTable();

        if(type == 'Status'){
            statusCodes = [];
            if($('#status_pending').is(':checked')){
                statusCodes.push('Pending');
            }
            if($('#status_started').is(':checked')){
                statusCodes.push('SERVICE STARTED');
            }
            if($('#status_completed').is(':checked')){
                statusCodes.push('SERVICE COMPLETED');
            }
            if($('#status_caseloss').is(':checked')){
                statusCodes.push('CASE LOSS');
            }

            table
            .columns(10)
            .search(statusCodes.join("|").toString(), true, true, true)
            .draw();
        }

        if(type == 'Provider'){
            providerName = '';
            if($('#providerFilter').selectpicker('val') != 'All'){
                providerName = $('#providerFilter').selectpicker('val');
            }

            table
            .columns(9)
            .search(providerName.toString(), false, false)
            .draw();
        }

    }
</script>
@endsection
