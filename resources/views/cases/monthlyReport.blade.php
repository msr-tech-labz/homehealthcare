@extends('layouts.main-layout')

@section('page_title','Cases - Monthly Report')

@section('active_monthly_report','active')

@section('page.styles')
<style>
    .card{
        overflow-x: scroll;
    }
    .table-bordered tbody tr td, .table-bordered tbody tr th {
        text-align: center;
    }
    .dt-buttons{
        position: static !important;
        float: right;
    }
    .paginate_button {
        cursor: pointer;
        margin:2px;
    }
    div.dataTables_wrapper div.dataTables_filter {
        text-align: center !important;
    }
    table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td {
      text-transform: uppercase; !important;
      overflow: hidden;
      text-overflow: ellipsis;
      white-space: nowrap;
    }
</style>
<!-- JQuery DataTable Css -->
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection

@section('plugin.styles')
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<style>
</style>
@endsection

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <h2 class="col-sm-9">
                   Case Reports
                   <small>List of all Cases</small>
               </h2>
           </div>
           <div class="body">
               <div class="row clearfix">
                   <form action="{{ route('case.reports') }}" method="post">
                       {{ csrf_field() }}
                       <table class="table table-bordered">
                           <tr>
                               <th width="20%">
                                   <select class="form-control" name="filter_status">
                                       <option value="">-- Any --</option>
                                       <option value="Converted">CONVERTED</option>
                                       <option value="PENDING">PENDING</option>
                                       <option value="SERVICE STARTED">SERVICE STARTED</option>
                                       <option value="SERVICE COMPLETED">SERVICE COMPLETED</option>
                                       <option value="CASE LOSS">CASE LOSS</option>
                                   </select>
                               </th>
                               @if(\Helper::encryptor('decrypt',session('utype')) == 'Aggregator')
                               <th width="20%">
                                   <select class="form-control" name="filter_provider">
                                       <option value="">-- Any Provider --</option>
                                @if(isset($providers))
                                   @foreach($providers as $p)
                                        <option style="text-transform: uppercase;" value="{{ $p->tenant_id }}">{{ $p->organization_name }}</option>
                                   @endforeach
                                @endif
                                   </select>
                               </th>
                               @endif
                               <th width="20%">
                                   <div class="col-md-6">
                                   <input type="text" class="min form-control" name="start_date" placeholder="From Date">
                                   </div>
                                   <div class="col-md-6">
                                   <input type="text" class="max form-control" name="end_date" placeholder="To Date">
                                   </div>
                               </th>
                               <th width="15%">
                                   <input type="submit" class="btn btn-success">
                               </th>
                           </tr>
                       </table>
                   </form>
               </div>
            <table class="hide" border="0" cellspacing="5" cellpadding="5">
                <tbody>
                    <tr>
                        <td>Start Date:</td>
                        <td><input type="text" id="min" name="min" placeholder="XX-XX-XXXX"></td>
                    </tr>
                    <tr>
                        <td>End Date:</td>
                        <td><input type="text" id="max" name="max" placeholder="XX-XX-XXXX"></td>
                    </tr>
                </tbody>
            </table>
            <table id="caseReport" class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable">
                <thead>
                    <th>#</th>
                    <th>Date</th>
                    <th>Case No.</th>
                    <th>Enquirer Name</th>
                    <th>Patient Name</th>
                    <th>Age</th>
                    <th>Weight</th>
                    <th>Requirement</th>
                    <th>Enquirer Phone</th>
                    <th>Enquirer Email</th>
                    <th>No of Hours</th>
                    <th>Medical Condition</th>
                    <th>Medications</th>
                    <th>Languages Spoken</th>
                    <th>Street Address</th>
                    <th>City</th>
                    <th>PinCode</th>
                    <th>State</th>
                    <th>Country</th>
                    <th>Provider</th>
                    <th>Caregiver</th>
                    <th>Case Status</th>
                    <th>Service Start Date</th>
                    <th>Service Completed Date</th>
                    <th>Case Loss Date</th>
                    <th>Case Charges</th>
                    <th>Comments</th>
                    <th>Case Loss Comment</th>
                    <th>Comment by MCM</th>
                </thead>
                <tbody>
                    @forelse($c as $i => $cr)
                    <tr>
                        <td>{{ $i + 1 }}</td>
                        <td>{{ $cr->created_at->format('d-m-Y') }}</td>
                        <td>{{ $cr->crn_number }}</td>
                        <td>{{ $cr->patient->enquirer_name }}</td>
                        <td>{{ $cr->patient->first_name }} {{ $cr->patient->last_name }}</td>
                        <td>{{ $cr->patient->patient_age }}</td>
                        <td>{{ $cr->patient->patient_weight }}</td>
                        <td>
                            @if(isset($cr->service))
                            {{ $cr->service->service_name }}
                            @else
                            -
                            @endif
                        </td>
                        <td>{{ $cr->patient->enquirer_phone }}</td>
                        <td>{{ $cr->patient->enquirer_email }}</td>
                        <td>{{ $cr->no_of_hours }}</td>
                        <td>{{ $cr->medical_conditions }}</td>
                        <td>{{ $cr->medications }}</td>
                        <td>{{ $cr->language_preference }}</td>
                        <td>{{ $cr->patient->street_address }}</td>
                        <td>{{ $cr->patient->city }}</td>
                        <td>{{ $cr->patient->zipcode }}</td>
                        <td>{{ $cr->patient->state }}</td>
                        <td>{{ $cr->patient->country }}</td>
                        <td>{{ App\Entities\Profile::where('tenant_id',$cr->tenant_id)->value('organization_name') }}</td>
                        {? $names = '-'; ?}
                        @if(isset($cr->schedules))
                        @foreach($cr->schedules as $s)
                            {? $names = $s->caregiver->first_name.' '.$s->caregiver->last_name; ?}
                        @endforeach
                        @endif
                        <td>{{ $names }}</td>
                        <td>{{ $cr->status }}</td>
                        {? $startDate = $cr->disposition()->whereStatus('SERVICE STARTED')->orderBy('updated_at','Desc')->value('disposition_date'); ?}
                        <td>{{ $startDate!=null?$startDate->format('d M Y'):'-' }}</td>
                        {? $completedDate = $cr->disposition()->whereStatus('SERVICE COMPLETED')->orderBy('updated_at','Desc')->value('disposition_date'); ?}
                        <td>{{ $completedDate!=null?$completedDate->format('d M Y'):'-' }}</td>
                        {? $lossDate = $cr->disposition()->whereStatus('CASE LOSS')->orderBy('updated_at','Desc')->value('disposition_date'); ?}
                        <td>{{ $lossDate!=null?$lossDate->format('d M Y'):'-' }}</td>
                        <td>{{ $cr->case_charges }}</td>
                        <td>{{ App\Entities\Comment::where('careplan_id',$cr->id)->orderBy('id','desc')->value('comment') }}</td>
                        @if($cr->disposition)
                        <td>{{ $cr->disposition()->orderBy('updated_at','Desc')->first()->caselosscomment ?? '-' }}</td>
                        <td>{{ $cr->disposition()->orderBy('updated_at','Desc')->first()->comment ?? '-' }}</td>
                        @else
                        <td>-</td>
                        @endif
                    </tr>
                    @empty
                    <tr>
                        <td colspan="30" class="text-center">
                            No lead(s) found.
                        </td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
@endsection


@section('page.scripts')
<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
{{-- <script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.13/filtering/row-based/range_dates.js"></script> --}}
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
<!-- Moment Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<script>
    $(document).ready(function() {
	@if(isset($c) && count($c))
        var table = $('#caseReport').DataTable( {
            "lengthMenu": [[50, 100, 200, -1], [50, 100, 200, "All"]],
            dom: 'Bflrtip',
            buttons: [
            'excel',
            {
                extend: 'pdfHtml5',
                pageSize: 'A3'
            },
            {
                extend: 'print',
                title: '<h3 style="text-align:center;">Lead Request Roster</h3>',
                customize: function ( win ) {
                    $(win.document.body)
                    .css( 'font-size', '7pt' );

                    $(win.document.body).find( 'table' )
                    .addClass( 'compact' )
                    .css( 'font-size', 'inherit' );
                }
            }
            ],
            "order": [[ 0, "desc" ]]
        });

        /* Custom filtering function which will search data in column four between two values */
        $.fn.dataTableExt.afnFiltering.push(
            function( oSettings, aData, iDataIndex ) {
                var iFini = document.getElementById('min').value;
                var iFfin = document.getElementById('max').value;
                var iStartDateCol = 1;
                var iEndDateCol = 1;

                iFini=iFini.substring(6,10) + iFini.substring(3,5)+ iFini.substring(0,2);
                iFfin=iFfin.substring(6,10) + iFfin.substring(3,5)+ iFfin.substring(0,2);

                var datofini=aData[iStartDateCol].substring(6,10) + aData[iStartDateCol].substring(3,5)+ aData[iStartDateCol].substring(0,2);
                var datoffin=aData[iEndDateCol].substring(6,10) + aData[iEndDateCol].substring(3,5)+ aData[iEndDateCol].substring(0,2);

                if ( iFini === "" && iFfin === "" )
                {
                    return true;
                }
                else if ( iFini <= datofini && iFfin === "")
                {
                    return true;
                }
                else if ( iFfin >= datoffin && iFini === "")
                {
                    return true;
                }
                else if (iFini <= datofini && iFfin >= datoffin)
                {
                    return true;
                }
                return false;
            }
            );

            $('#min, #max').keyup( function() {
                table.draw();
            });
        @endif
    });


    $(function(){
        $('.min').bootstrapMaterialDatePicker({
            format: 'DD-MM-YYYY',
            clearButton: true,
            time: false,
            maxDate : new Date(),
        });
        $('.max').bootstrapMaterialDatePicker({
            format: 'DD-MM-YYYY',
            clearButton: true,
            time: false,
            maxDate : new Date(),
        });
    });
</script>
@endsection
