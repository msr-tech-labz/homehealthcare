@extends('layouts.main-layout')

@section('page_title','Cases - ')

@section('active_cases','active')

@section('page.styles')
<style>
.plus:after{
    content: ' + ';
    font-size: 18px;
    font-weight: bold;
}
</style>
@endsection

@section('plugin.styles')
<style>
.info-box .icon{
    width: 65px !important;
}
.tab-nav-right > li > a{
    min-width: 180px !important;
    text-align: center;
}
.nav-tabs > li.active{
    border-left: 2px solid #2196F3;
    border-right: 2px solid #2196F3;
    border-top: 2px solid #2196F3;
    margin-bottom: 2px;
    background: #efefef;
}
.nav-tabs > li.active > a{
    font-weight: bold;
    color: #2196F3 !important;
}
.nav-tabs > li > a:before{
    border-bottom: 0 !important;
}
table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td {
    /*text-transform: uppercase; !important;*/
}
</style>
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection

@section('side-menu')
    @include('menus.leads')
@endsection

@section('content')
    <div class="block-header">
        <h2>Cases</h2>
        <small>List of all converted cases</small>
    </div>
    <div class="row clearfix">
        <div class="row clearfix">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="body" style="padding: 10px 15px;">
                        <div class="table-responsive">
                            <table id="leadsTable" class="table table-bordered table-condensed table-hover">
                                <thead>
                                    <tr>
                                        <th width="3%">#</th>
                                        <th width="10%">Date</th>
                                        <th width="15%">Patient</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Location</th>
                                        <th>Status</th>
                                        <th width="10%"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($leads as $i => $l)
                                        <tr data-id="{{ Helper::encryptor('encrypt',$l->id) }}">
                                            <td>{{ $i + 1 }}</td>
                                            <td>
                                                {{ $l->created_at->format('d-m-Y') }}<br>
                                                <small>{{ $l->created_at->format('h:i A') }}</small>
                                            </td>
                                            <td>
                                                {{ $l->patient->first_name.' '.$l->patient->last_name }}<br>
                                                <small>Enquirer: {{ $l->patient->enquirer_name }}</small>
                                            </td>
                                            <td>
                                                {{ $l->patient->contact_number }}<br>
                                                <small>Enquirer: {{ $l->patient->alternate_number }}</small>
                                            </td>
                                            <td>
                                                {{ $l->patient->email ?? '-' }}
                                            </td>
                                            <td>
                                                {{ $l->patient->city ?? '-' }}<br>
                                                <small>{{ $l->patient->area ?? '-' }}</small>
                                            </td>
                                            <td>{{ $l->status ?? '-' }}</td>
                                            <td>
                                                @ability('admin', 'cases-view')
                                                <a href="{{ route('lead.view',Helper::encryptor('encrypt',$l->id)) }}" class="btn btn-md btn-primary btn-block" data-id="{{ Helper::encryptor('encrypt',$l->id) }}" title="View Lead">View Case</a>
                                                @endability
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="9" class="text-center">
                                                No cases(s) found.
                                            </td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="leadTypeModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="col-md-6 text-center">
                        <a class="btn btn-block btn-primary btnOtherRequest" data-id="0">Job / Partnership Request</a>
                    </div>
                    <div class="col-md-6 text-center">
                        <a class="btn btn-block btn-success openMedicalCase" target="_blank" data-href="{{ route('case.create-from-lead') }}">Medical Case</a>
                    </div>
                    <div class="clearfix"></div><br>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="otherRequestsModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" >
                <form id="otherRequestForm" method="POST" action="{{ route('lead.save-other-request') }}" class="col-sm-12 form-horizontal">
                    <div class="modal-header bg-blue hidden-print">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="largeModalLabel">Other Request Form</h4>
                    </div>
                    <div class="modal-body">
                        <div class="col-sm-12">
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="full_name">Full Name</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="name" placeholder="Ex. Ashok Kumar"/>
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="phone_number">Phone Number</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="phone_number" placeholder="Ex. 9845098450"/>
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="email">Email</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="email" placeholder="Ex. ashok@gmail.com"/>
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="city">City</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="city" placeholder="Ex. Bengaluru"/>
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="requirement">Requirement Type</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="form-group">
                                        <div class="requirement_type">
                                            <input name="requirement_type" type="radio" id="JOB" value="JOB" checked />
                                            <label for="JOB">JOB</label>
                                            <input name="requirement_type" type="radio" id="PARTNERSHIP" value="PARTNERSHIP"/>
                                            <label for="PARTNERSHIP">PARTNERSHIP</label>
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="requirement">Requirement</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea class="form-control" name="requirement" placeholder="Ex. Job Requirement"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer" style="border-top: 1px solid #ccc !important;">
                        <input type="hidden" id="lead_id" name="lead_id" value="" />
                        <button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-success" onclick="javascript: return validate();" value="Save"></input>
                        {{ csrf_field() }}
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('page.scripts')
    @parent
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script>
    $(function(){
        // $('#leadsTable').DataTable({
        //     "order": [[0, "desc"]]
        // });
        var table = $('#leadsTable').DataTable({
            "order": [[ 0, 'asc' ]],
            "displayLength": 25,
            "drawCallback": function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null;

                api.column(1, {page:'current'} ).data().each( function ( group, i ) {
                    if(group != null){
                        if ( last !== group.substring(0, 10) ) {
                            $(rows).eq( i ).before(
                                '<tr class="group"><td colspan="10"><b>'+group.substring(0, 10)+'</b></td></tr>'
                            );

                            last = group.substring(0, 10);
                        }
                    }
                } );
            }
        } );

        // Order by the grouping
        $('#leadsTable tbody').on( 'click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
                table.order( [ 1, 'desc' ] ).draw();
            }
            else {
                table.order( [ 1, 'asc' ] ).draw();
            }
        });

        $('.dataTable').DataTable();

        $('.content').on('click','.btnLeadType', function(){
            $('#leadTypeModal').modal();
            $('#leadTypeModal .btnOtherRequest').attr('data-id',$(this).data('id'));

            $('#leadTypeModal .openMedicalCase').attr('data-id',$(this).data('id'));
            $('#leadTypeModal .openMedicalCase').attr('href','');
            url = $('#leadTypeModal .openMedicalCase').data('href');
            url += '?ref='+$(this).data('id');
            $('#leadTypeModal .openMedicalCase').attr('href',url);
        });


        $('.content').on('click','.btnOtherRequest', function(){
            $('#leadTypeModal').modal('hide');
            id = $(this).attr('data-id');
            block = '#leadsTable tbody tr[data-id='+id+']';

            $('#otherRequestForm input[name=name]').val($(block+' td:eq(3)').html().replace(/<\/?[^>]+(>|$)/g, "").trim());
            $('#otherRequestForm input[name=phone_number]').val($(block+' td:eq(4)').html().replace(/<\/?[^>]+(>|$)/g, "").trim());
            $('#otherRequestForm input[name=email]').val($(block+' td:eq(5)').html().replace(/<\/?[^>]+(>|$)/g, "").trim());
            $('#otherRequestForm input[name=city]').val(removeTags($(block+' td:eq(6)').text()));

            // console.log($(block+' td:eq(6)').html()+"  :: "+removeTags($(block+' td:eq(6)').text()));

            $('#otherRequestForm #lead_id').val(id);

            $('#otherRequestsModal').modal();
        });

        $('.content').on('click', '.markLeadAsChecked', function(){
            id = $(this).data('id');
            if(confirm("Are you sure?")){
                $.ajax({
                    url: '{{ route('ajax.mark-lead-as-checked') }}',
                    type: 'POST',
                    data: {_token:'{{ csrf_token() }}',id: id},
                    success: function(data){
                        console.log(data);
                        $('#leadsTable tbody tr[data-id='+id+']')
                        .children('td, th')
                        .animate({ padding: 0 })
                        .wrapInner('<div />')
                        .children()
                        .slideUp(function() { $(this).closest('tr').remove(); });

                        //toastr.success("Lead Status has been updated");
                        alert("Lead Status has been updated");
                    },
                    error: function(err){
                        console.log(err);
                    }
                });
            }
        });
    });

    function removeTags(txt){
        var rex = /(<([^>]+)>)/ig;
        //return txt.replace(rex , ",").replace(/\s/g, '').replace(/,\s*$/, '');
        return txt.replace(/\s+/g,' ')
        .replace(/^\s+|\s+$/,'');
    }

    </script>
@endsection
