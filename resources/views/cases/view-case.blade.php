@extends('layouts.main-layout')

@section('page_title','View File - Aggregator Leads |')

@section('active_cases','active')

@section('plugin.styles')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />

<link href="{{ ViewHelper::ThemePlugin('jquery-ui/jquery-ui.css') }}" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('jquery-ui/jquery-ui.structure.css') }}" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('jquery-ui/jquery-ui.theme.css') }}" rel="stylesheet" />
<!-- Multiple Dates Picker Css-->
<link href="{{ ViewHelper::ThemePlugin('multiple-dates-picker/jquery-ui.multidatespicker.css') }}" rel="stylesheet" />


<!-- Bootstrap Tagsinput Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
<link href="{{ ViewHelper::ThemePlugin('nouislider/nouislider.min.css') }}" />
<link href="{{ ViewHelper::ThemePlugin('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
<link rel="stylesheet" type="text/css" href="{{ asset('css/print.css') }}" media="print" />
@endsection

@section('page.styles')
<style>
	.bootstrap-datetimepicker-widget table td.disabled{
		color: red !important;
	}
	@page
	{
		size: auto;   /* auto is the initial value */
		margin: 0mm;  /* this affects the margin in the printer settings */
	}
	/*.bootstrap-notify-container{
		z-index: 99999 !important;
	}*/
	.light-red{
		background-color: rgba(239, 152, 152, 0.46) !important;
	}
	.form-control-label{
		text-align: left;
	}
	.form-group .form-line, .form-group{
		text-transform: uppercase; !important;
	}
	.form-group label{
		text-transform: capitalize !important;
	}
	.pac-container {
		z-index: 9999 !important;
	}
	.nav-tabs > li{
		/*min-width: 12%;*/
	}
	.card .header .header-dropdown{
		top: 15px;
	}
	hr{
		margin-top: 10px;
		margin-bottom: 10px;
	}
	.staffSearchResults thead tr th{
		font-size: 12px !important;
	}
	.table-borderless tbody tr td, .table-borderless tbody tr th{
		border: none;
		padding: 6px;
	}
	.table-borderless tbody tr td{
		min-width: 150px;
	}
	.routines tbody tr td{
		white-space: nowrap;
	}
	.table-borderless tbody tr td:before{
		content: ' :';
		padding-right: 10px;
		font-weight: bold;
	}
	.bootstrap-tagsinput{
		min-height: 40px;
		vertical-align: top;
		padding: 4px 0;
		width: 100%;
	}
	.bootstrap-tagsinput input{
		padding-left: 0;
	}
	.plus:after{
		content: ' + ';
		font-size: 18px;
		font-weight: bold;
	}
	.care-plan-card .header{
		padding: 15px 20px;
	}
	.modal-lg{
		width: 70%;
	}
	.modal .modal-header{
		padding: 20px 25px 15px 25px;
	}
	.modal-footer{
		border-top: 1px solid #ddd !important;
	}
	.details-pane .details{
		margin-top: 6px;
	}
	.details-pane .details .form-line{
		border-bottom: none;
	}
	.details-pane .details .form-line .form-control, .details-pane .details .form-line .form-control:focus{
		outline: 0;
		border-bottom: 1px solid #ff5722 !important;
		/*-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(255, 87, 34, .6);
		box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(255, 87, 34, .6);*/
	}

	.card .body .col-lg-9, .card .body .col-lg-3{
		margin-bottom: 8px !important;
	}
	.careplan-status button.waves-effect{
		width: 160px !important;
	}
	.comment-card { margin-bottom: 15px;}
	.comment-card .header { padding: 5px 10px;}
	.comment-card .body { padding: 10px;}
	.comment-card .header h2 {font-size: 16px}
	.comment-card .header .header-dropdown li > span {font-size: 13px}
	.comment-card th{ background: #ccc}
	.comment-card .table{ margin-bottom: 0}

	.details-pane div.col-lg-8, .details-pane div.col-lg-4{ margin-bottom: 8px !important}

	.noUi-horizontal .noUi-handle{
		background-color: #ff0000;
	}
	.text-muted {
	    color: #777;
	    position: absolute;
	    right: 10%;
	}
	.search-table-outter {border:2px solid red;}
	.search-table{margin:40px auto 0px auto; }
	.search-table, td, th{border-collapse:collapse; border:1px solid #777;}
	th{padding:20px 7px; font-size:15px; color:#444; background:#66C2E0;}
	td{padding:5px 10px; height:35px;}
	.search-table-outter {
		overflow-x: scroll;
	}
	.condensed tbody tr td, .condensed tbody tr th{
		padding: 3px 10px !important;
    	line-height: 2 !important;
	}
	.dataTables_length .bootstrap-select{
		display: inline !important;
	}
	.dataTables_empty{
		text-align: center !important;
	}
</style>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="row clearfix">
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif

<div class="row hidden-print">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card">
			<div class="header">
				<a href="{{ route('lead.index') }}" class="pull-left btn btn-info btn-circle clearLocalStore waves-effect waves-circle waves-float">
					<i class="material-icons">arrow_back</i>
				</a>
				<h2 class="col-sm-8">
					{{ isset($l)?'View':'New'}} File - {{ $l->episode_id ?? $l->patient->full_name }}
					<small>Patient Case Details</small>
				</h2>
				<div class="col-sm-2 pull-right">
					<span class="pull-left">Patient ID: <b>{{ $l->patient->patient_id }}</b></span><br>
					<span class="pull-left">Status: <b>{{ $l->status }}</b>&nbsp;&nbsp;
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="body" style="padding: 10px 8px;">
				<div clas="row clearfix" style="margin-bottom: 0">
					@if(!empty($l->special_instructions))
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card" style="margin-bottom: 0">
							<div class="header bg-orange" style="padding: 10px">
								<h2>
									Special Instructions
								</h2>
							</div>
							<div class="body" style="padding: 10px;min-height: 20px;font-size: 16px">
								{{ $l->special_instructions }}
							</div>
						</div>
					</div>
					@endif
					<div class="clearfix"></div>
				</div>

                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12" style="margin-bottom:5px !important;">
					<div class="panel-group" id="accordion_2" role="tablist" aria-multiselectable="true">
						<div class="panel panel-col-cyan">
							<div class="panel-heading" role="tab" id="headingOne_2">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion_2" href="#collapseOne_2" aria-expanded="true" aria-controls="collapseOne_2">
										Patient Details (Click to Show/Hide)
										<i class="material-icons" style="text-align: right;">arrow_drop_down_circle</i>
									</a>
								</h4>
							</div>
							<div id="collapseOne_2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne_2">
								<div class="panel-body" style="margin-top: 0 !important;padding-top: 0 !important;">
								<div class="clearfix"></div>
								@ability('admin','cases-patient-details')
								<div class="row clearfix form-horizontal details-pane">
									<div class="col-sm-6">
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
												<label for="patient_name">Patient Name</label>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
												<div class="form-group details">
													<div class="form-line" style="padding-top: 3px;">
														{{ $l->patient->full_name }}
													</div>
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
												<label for="patient_gender">Gender</label>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
												<div class="form-group details">
													<div class="form-line" style="padding-top: 3px;">
														{{ $l->patient->gender ?? '-'}}
													</div>
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
												<label for="patient_age">Age</label>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
												<div class="form-group details">
													<div class="form-line" style="padding-top: 3px;">
														{{ isset($l->patient->patient_age)?$l->patient->patient_age:'' }}
													</div>
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
												<label for="patient_weight">Weight</label>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
												<div class="form-group details">
													<div class="form-line">
														{{ isset($l->patient->patient_weight)?$l->patient->patient_weight:'' }}
													</div>
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
												<label for="contact_number">Contact Number</label>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
												<div class="form-group details">
													{{ $l->patient->contact_number ?? '-' }}
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
												<label for="alternate_number">Alternate Number</label>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
												<div class="form-group details">
													{{ $l->patient->alternate_number ?? '-' }}
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
												<label for="enquirer_name">Enquirer/Relative Name</label>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
												<div class="form-group details">
													{{ $l->patient->enquirer_name ?? '-' }}
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
												<label for="enquirer_name">Address</label>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
												<div class="form-group details">
													{{ $l->patient->street_address ?? '-' }},
													{{ $l->patient->area ?? '-' }},
													{{ $l->patient->city ?? '-' }},
													{{ $l->patient->zipcode ?? '-' }},
													{{ $l->patient->state ?? '-' }},
													{{ $l->patient->country ?? '-' }}
												</div>
											</div>
										</div>
									</div>

									<div class="col-sm-6">
										{{-- <h2 class="card-inside-title">
											<a class="btn bg-deep-orange btn-sm waves-effect pull-right btnEditCarePlan hide">Edit</a>
											Case Details
											<small>Medical Conditions, Diagnosis, Notes</small>
										</h2> --}}
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
												<label for="medical_conditions">Case Description</label>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
												<div class="form-group details">
													<div class="form-line">
														{!! $l->case_description !!}
													</div>
												</div>
											</div>
										</div>
										<div style="background: #b6dbf9; padding:10px;border-radius: 4px">
											<div class="row clearfix">
												<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
													<label for="medical_conditions">Medical Conditons/ Current Diagnosis</label>
												</div>
												<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
													<div class="form-group details">
														<div class="form-line">
															{!! $l->medical_conditions !!}
														</div>
													</div>
												</div>
											</div>
											<div class="row clearfix">
												<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
													<label for="medical_conditions">Medications</label>
												</div>
												<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
													<div class="form-group details">
														<div class="form-line">
															{!! $l->medications !!}
														</div>
													</div>
												</div>
											</div>
											<div class="row clearfix">
												<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
													<label for="medical_conditions">Procedures/Surgeries</label>
												</div>
												<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
													<div class="form-group details">
														<div class="form-line">
															{!! $l->procedures !!}
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
												<label for="service_required">Service Required</label>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
												<div class="form-group details">
													{{ $l->serviceRequired->service_name ?? '-' }}
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
												<label for="no_of_hours">Tentative period (days)</label>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
												<div class="form-group details">
													<div class="form-line">
														{{ $l->estimated_duration ?? '-' }}
													</div>
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
												<label for="gender_preference">Gender Preference</label>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
												<div class="form-group details">
													{{ $l->gender_preference ?? '-' }}
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
												<label for="language_preference">Language Preference</label>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
												<div class="form-group details">
													{{ $l->language_preference ?? '-' }}
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
												<label for="rate_agreed">Rate Agreed</label>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
												<div class="form-group details">
													{{ $l->rate_agreed ?? '-' }}
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
												<label for="status">Status</label>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
												<div class="form-group details">
													{{ $l->status ?? '-' }}
												</div>
											</div>
										</div>
									</div>
								</div>
								@else
									<div class="text-center restricted-access">Access to this information is restricted</div>
								@endability
								<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</div>

                <div class="row clearfix" style="margin: 0 20px 20px 20px;">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs view-lead-tabs" role="tablist">
                        <li role="presentation" class="active">
							<a href="#comments_tab" data-toggle="tab" aria-expanded="true">
								<i class="material-icons">comment</i> Comments
							</a>
						</li>
						<li role="presentation" class="">
							<a href="#schedules_tab" data-toggle="tab">
								<i class="material-icons">date_range</i> Schedules {!! count($l->schedules)?'&nbsp;&nbsp; <span class="badge bg-cyan">'.count($l->schedules).'</span>':'' !!}
							</a>
						</li>
						<li role="presentation" class="">
							<a href="#tasks_tab" data-toggle="tab">
								<i class="material-icons">view_list</i> Tasks
							</a>
						</li>
						<li role="presentation" class="">
							<a href="#feedback_tab" data-toggle="tab">
								<i class="material-icons">call</i> Feedback Call
							</a>
						</li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content tab-menu">
                        <div role="tabpanel" class="tab-pane fade in active" id="comments_tab">
							<div class="row clearfix">
                                <div class="col-sm-12">
									<h2 class="card-inside-title">
									@caseNotClosed($l->status)
										@ability('admin','cases-comments-add')
										<button type="button" class="btn bg-blue btn-circle waves-effect waves-circle waves-float pull-right btnAddComment hide">
											<i class="material-icons">add</i>
										</button>
										@endability
									@endCaseNotClosed
									</h2>
									<div class="col-sm-6">
									@ability('admin','cases-comments-view')
										@if(isset($l->comments) && count($l->comments))
											@foreach($l->comments as $comment)
											<div class="card comment-card" style="border-left: 5px solid #2196F3">
												<div class="body" style="display:inline-block;width: 78%">
													{!! $comment->comment !!}
												</div>
												<div class="user_info" style="display:inline-block;width: auto;margin-right: 1%;float:right">
													<small class="col-teal pull-right" style="line-height:1.6">{{ $comment->user_name }}</small><br>
													<div style="margin-top:-1px;font-style:italic;"><small>{{ $comment->created_at->format('d M Y, h:i A') }}</small></div>
												</div>
											</div>
											@endforeach
										@else
											<div class="row clearfix">
												<div class="text-center"><i>No comments found</i></div>
											</div>
										@endif
									@else
										<div class="text-center restricted-access">Access to this information is restricted</div>
									@endability
									</div>
									<div class="col-sm-6">
										@if(isset($l->dispositions) && count($l->dispositions))
											@foreach($l->dispositions as $disposition)
											<div class="card comment-card">
													<div class="header bg-light-blue">
														<small>{{ $disposition->status }} on
															{{ $disposition->created_at->format('d M Y, h:i A') }}
															@if(isset($disposition->user)){!! ' - <b>'.$disposition->user->full_name.'</b>' !!}@endif
														</small>
													</span>
												</div>
												<div class="body">
													{!! $disposition->comment !!}
												</div>
											</div>
											@endforeach
										@else
											<div class="row clearfix">
												<div class="text-center"><i>No disposition(s) found</i></div>
											</div>
										@endif
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>

						<div role="tabpanel" class="tab-pane fade in" id="schedules_tab">
							<div class="alert-block">
							</div>
							<div class="row clearfix">
								<div class="col-sm-4 pull-left" style="margin-left: 2%">
									<a class="btn btn-md bg-blue btnCloseSchedule pull-left" style="display:none"><i class="material-icons" style="font-size:15px">reply</i> Schedules List</a>
								</div>
							</div>
							@ability('admin','cases-schedules-view')
							<div id="schedulesListDiv" class="row clearfix" style="width: 98%; margin: 0 auto">
								{{-- <h2 class="card-inside-title">Schedules List</h2> --}}

								<div class="table-responsive">
									<table class="table table-bordered condensed pastSchedulesTable" style="width: 100%;margin-bottom:0px">
										<thead>
											<tr>
												<th>Date</th>
												<th>Service</th>
												<th>Caregiver / Staff</th>
												<th>Status</th>
												<th>Chargeable</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
							@else
								<div class="text-center restricted-access">Access to this information is restricted</div>
							@endability
							<div class="clearfix"></div>
						</div>

						<div role="tabpanel" class="tab-pane fade in" id="assessment_tab">
							<h2 class="card-inside-title">Forms</h2>
							<div class="row clearfix">
								<table class="table table-bordered condensed table-condensed">
									<thead>
										<tr>
											<th>Date</th>
											<th>Type</th>
											<th>Creator</th>
											<th>Report</th>
										</tr>
									</thead>
									<tbody>
										@foreach($l->assessment()->orderBy('created_at','Desc')->get() as $a)
										@if($a->form_type == "Assessment")
										{? $formData = json_decode($a->form_data,true); ?}
										<tr>
											<td>{{ $formData['assessment_date'] }}</td>
											<td>{{ $a->form_type }}</td>
											<td>{{ $formData['assessor_name'] }}</td>
											<td><a class="btn btn-info btnViewAssessment assessmentId_{{$a->id}}" data-toggle="modal" data-target="#viewassessmentModal" data-form-data="{{$a->form_data}}" onclick="viewAssessment({{$a->id}});"><span class="view"></span> View</a></td>
										</tr>
										@endif
										@if($a->form_type == "PhysioAssessment")
										<tr>
											<td>{{ $a->created_at->format('d-m-Y h:i A') }}</td>
											<td>{{ $a->form_type }}</td>
											<td>{{ isset($a->user)?$a->user->full_name:'' }}</td>
											<td><a class="btn btn-info btnViewLabRequirement requirementId_{{$a->id}}" data-toggle="modal" data-target="#viewrequirementModal" data-form-data="{{ $a->form_data }}" onclick="viewRequirement({{ $a->id }});"><span class="view"></span> View</a></td>
										</tr>
										@endif
										@endforeach
									</tbody>
								</table>
							</div>
						</div>

						<div role="tabpanel" class="tab-pane fade in" id="tasks_tab">
							<h2 class="card-inside-title">Daily Tasks</h2>
							<div class="row clearfix">
								<table class="table table-bordered condensed table-condensed">
									<thead>
										<tr>
											<th>#</th>
											<th>Type</th>
											<th>Date</th>
											<th>Caregiver</th>
											<th>Rating By Customer</th>
											<th>Comment By Customer</th>
											<th>View</th>
										</tr>
									</thead>
									<tbody>
										@forelse ($l->worklog as $index => $w)
										<tr>
											<td>{{ ($index + 1) }}</td>
											<td>Nursing</td>
											<td>{{ $w->created_at->format('d-m-Y') }}</td>
											<td>{{ isset($w->caregiver)?$w->caregiver->first_name.' '.$w->caregiver->last_name:'-' }}</td>
											<td>
											@if(isset($w->feedback->rating))
												@if (floor($w->feedback->rating) == $w->feedback->rating && $w->feedback->rating != '0')
													<?php echo str_repeat('<i class="fa fa-star" style="color:royalblue;"></i> ',$w->feedback->rating); ?>
												@elseif ($w->feedback->rating == '0')
													<?php echo str_repeat('<i class="fa fa-star-o" style="color:grey;"></i> ',5); ?>
												@else
													<?php echo str_repeat('<i class="fa fa-star" style="color:royalblue;"></i> ',$w->feedback->rating); ?>
													<?php echo is_float($w->feedback->rating) ? '<i class="fa fa-star-half" style="color:royalblue;"></i>' : ''; ?>
												@endif
											@else
												<?php echo str_repeat('<i class="fa fa-star-o" style="color:grey;"></i> ',5); ?>
											@endif
											</td>
											<td>{{ $w->feedback->comment ?? '-' }}</td>
											<td><a class="btn btn-info btnViewWorklog worklogId_{{ $w->id }}" data-toggle="modal" data-target="#viewWorklogModal" data-vitals="{{ $w->vitals }}" data-routines="{{ $w->routines }}" onclick="viewWorklog(this);"><span class="view"></span> View</a></td>
										</tr>
										@empty
										@endforelse
									</tbody>
								</table>
							</div>
						</div>

						<div role="tabpanel" class="tab-pane fade in" id="feedback_tab">
							<h2 class="card-inside-title">Feedback Calls</h2>
						</div>
				    </div>
			   </div>
		   	</div>
		</div>
	</div>
	<input type="hidden" name="lead_id" id="lead_id" value="{{ Helper::encryptor('encrypt',$l->id) }}">
	<input type="hidden" name="careplan_id" id="careplan_id" value="{{ Helper::encryptor('encrypt',$l->id) }}">
	<input type="hidden" name="patient_id" id="patient_id" value="{{ Helper::encryptor('encrypt',$l->patient->id) }}">
</div>

<!-- Comment Modal -->
<div class="modal fade" id="commentModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<form action="{{ route('lead.save-comment') }}" method="POST">
			<div class="modal-content">
				<div class="modal-header bg-blue">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="largeModalLabel">Add Comment</h4>
				</div>
				<div class="modal-body">
					<div class="row clearfix">
						<div class="col-sm-12">
							<div class="form-group">
								<div class="form-line">
									<textarea class="form-control" id="comment" name="comment" rows="3" placeholder="Enter Comment here" required></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					@if(Helper::encryptor('decrypt',session('utype')) == 'Aggregator')
					<input type="hidden" name="provider_id" value="{{ $l->tenant_id?Helper::encryptor('encrypt',$l->tenant_id):0  }}" />
					@endif
					<input type="hidden" name="lead_id" value="{{ Helper::encryptor('encrypt',$l->id) }}" />
					{{ csrf_field() }}
					<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-success waves-effect pull-right">Save</button>
				</div>
			</div>
		</form>
	</div>
</div>

<!-- Edit Patient Modal -->
<div class="modal fade" id="editPatientModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<form id="editPatientForm" method="POST" action="{{ route('patient.save-patient') }}">
			<div class="modal-content">
				<div class="modal-header bg-blue">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="largeModalLabel">Edit Patient Info <span class="crn_no"></span></h4>
				</div>
				<div class="modal-body">
					<div class="row clearfix">
						<div class="col-sm-6 form-horizontal">
							<h4 class="card-inside-title col-teal">
								Basic Details
							</h4>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="first_name">First Name</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="first_name" name="first_name" class="form-control searchCol" placeholder="Ex. Ashok" value="{{ isset($l)?$l->patient->first_name:'' }}" required>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="last_name">Last Name</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="last_name" name="last_name" class="form-control searchCol" placeholder="Ex. Kumar" value="{{ isset($l)?$l->patient->last_name:'' }}">
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="mobile_number">Mobile Number</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="mobile_number" name="mobile_number" class="form-control searchCol" placeholder="Ex. 9845098450" value="{{ isset($l)?$l->patient->mobile_number:'' }}">
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="patient_age">Patient Age</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="patient_age" name="patient_age" class="form-control" placeholder="Ex. 65" value="{{ isset($l)?$l->patient->patient_age:'' }}">
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="patient_weight">Patient Weight</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="patient_weight" name="patient_weight" class="form-control" placeholder="Ex. 65" value="{{ isset($l)?$l->patient->patient_weight:'' }}">
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="gender">Gender</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<select class="form-control show-tick" id="gender" name="gender" required>
											<option value="">-- Please select--</option>
											<option value="Male" @if(isset($l) && $l->patient->gender == 'Male'){{'selected'}}@endif>Male</option>
											<option value="Female" @if(isset($l) && $l->patient->gender == 'Female'){{'selected'}}@endif>Female</option>
											<option value="Other" @if(isset($l) && $l->patient->gender == 'Other'){{'selected'}}@endif>Other</option>
										</select>
									</div>
								</div>
							</div>
							<h4 class="card-inside-title col-teal">
								Patient Address
							</h4>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="street_address">Street Address</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="street_address" name="street_address" class="form-control" placeholder="Ex. #20, 3rd Cross, 5th Main" value="{{ isset($l)?$l->patient->street_address:'' }}"/>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="area">Area</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="area" name="area" class="form-control" placeholder="Ex. Sanjay Nagar" value="{{ isset($l)?$l->patient->area:'' }}"/>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="city">City</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="city" name="city" class="form-control" placeholder="Ex. Bangalore" value="{{ isset($l)?$l->patient->city:'' }}"/>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="zipcode">Zip Code</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="zipcode" name="zipcode" class="form-control" placeholder="Ex. 560001" value="{{ isset($l)?$l->patient->zipcode:'' }}"/>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="state">State</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="state" name="state" class="form-control" placeholder="Ex. Karnataka" value="{{ isset($l)?$l->patient->state:'' }}"/>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="country">Country</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="country" name="country" class="form-control" placeholder="Ex. India" value="{{ isset($l)?$l->patient->country:'' }}"/>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="location_coordinates">Location Co-ordinates</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="location_coordinates" name="location_coordinates" class="form-control" placeholder="Eg. 12.789666, 32.6589656" value="{{ isset($l)?$l->patient->location_coordinates:'' }}"/>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 form-horizontal">
							<h4 class="card-inside-title col-teal">
								Enquirer Details
							</h4>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="enquirer_name">Enquirer Name</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="enquirer_name" name="enquirer_name" class="form-control" placeholder="Ex. Ajay Rao" value="{{ isset($l)?$l->patient->enquirer_name:'' }}">
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="enquirer_phone">Enquirer Phone</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="enquirer_phone" name="enquirer_phone" class="form-control" placeholder="Ex. 9999999999" value="{{ isset($l)?$l->patient->enquirer_phone:'' }}">
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="enquirer_email">Enquirer Email</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="enquirer_email" name="enquirer_email" class="form-control" placeholder="Ex. ajay@gmail.com" value="{{ isset($l)?$l->patient->enquirer_email:'' }}">
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="relationship_with_patient">Relationship with Patient</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<select id="relationship_with_patient" name="relationship_with_patient" class="form-control show-tick" placeholder="Relationship with Patient">
											<option value="">-- Please select --</option>
											<option value="Father" @if(isset($l) && $l->patient->relationship_with_patient == 'Father'){{'selected'}}@endif>Father</option>
											<option value="Mother" @if(isset($l) && $l->patient->relationship_with_patient == 'Mother'){{'selected'}}@endif>Mother</option>
											<option value="Grand Father" @if(isset($l) && $l->patient->relationship_with_patient == 'Grand Father'){{'selected'}}@endif>Grand Father</option>
											<option value="Grand Mother" @if(isset($l) && $l->patient->relationship_with_patient == 'Grand Mother'){{'selected'}}@endif>Grand Mother</option>
											<option value="Sister" @if(isset($l) && $l->patient->relationship_with_patient == 'Sister'){{'selected'}}@endif>Sister</option>
											<option value="Brother" @if(isset($l) && $l->patient->relationship_with_patient == 'BROTHER'){{'selected'}}@endif>Brother</option>
											<option value="Self" @if(isset($l) && $l->patient->relationship_with_patient == 'Self'){{'selected'}}@endif>Self</option>
											<option value="Friend" @if(isset($l) && $l->patient->relationship_with_patient == 'FRIEND'){{'selected'}}@endif>Friend</option>
											<option value="Uncle" @if(isset($l) && $l->patient->relationship_with_patient == 'Uncle'){{'selected'}}@endif>Uncle</option>
											<option value="Aunt" @if(isset($l) && $l->patient->relationship_with_patient == 'Aunt'){{'selected'}}@endif>Aunt</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						{{ csrf_field() }}
						<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-success waves-effect pull-right btnSavePatientDetails">Save</button>
						<input type="hidden" name="patient_id" value="{{$l->patient->id}}">
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<!-- View Assessment Modal -->
<div class="modal fade" id="viewassessmentModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content" >
			<div id="assessment-report">
				<div class="modal-header bg-blue hidden-print">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="largeModalLabel">REPORT</h4>
				</div>
				<div class="modal-body" id="view-assessment-data">
				<div id="print-content">
					<table border="1" class="table table-bordered table-condensed" id="viewAssessmentTable">
					</table>
				</div>
				</div>
				<div class="modal-footer" style="border-top: none !important;">
					<input type="hidden" name="lead_id" value="{{Helper::encryptor('encrypt',$l->id)}}"/>
					<input type="hidden" name="episode_id" value="{{$l->episode_id}}" />
					<input type="hidden" name="caregiver_id" id="listed_caregiver_id" value="0" />
					<input type="hidden" name="patient_id" value="{{$l->patient_id}}" />
					<button type="button" class="btn btn-danger waves-effect pull-left noPrint " data-dismiss="modal">Close</button>
					<input type="button" class="btn btn-info noPrint " value="Print" onclick="window.print();"></input>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- View Physiotherapist Requirement Modal -->
<div class="modal fade" id="viewrequirementModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content" >
			<div id="lab-report">
				<div class="modal-header bg-blue hidden-print">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="largeModalLabel">PHYSIOTHERAPY ASSESSMENTS</h4>
				</div>
				<div class="modal-body" id="view-labrequirement-data">
				<div id="print-content">
					<table border="1" class="table table-bordered table-condensed" id="viewRequirementTable">
					</table>
				</div>
				</div>
				<div class="modal-footer" style="border-top: none !important;">
					<input type="hidden" name="careplan_id" value="{{Helper::encryptor('encrypt',$l->id)}}"/>
					<input type="hidden" name="crn_number" value="{{$l->lead_id}}" />
					<input type="hidden" name="patient_id" value="{{$l->patient_id}}" />
					<button type="button" class="btn btn-danger waves-effect pull-left noPrint " data-dismiss="modal">Close</button>
					<input type="button" class="btn btn-info noPrint " value="Print" onclick="window.print();"></input>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- View Worklog Modal -->
<div class="modal fade" id="viewWorklogModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content" >
			<div id="worklog-report">
				<div class="modal-header bg-blue hidden-print">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="largeModalLabel">Worklog</h4>
				</div>
				<div class="modal-body" id="view-assessment-data">
					<div class="search-table-outter wrapper">
						<table border="1" class="table table-bordered table-condensed" id="viewWorklogTableVitals">
						</table>
					</div>
					<div class="search-table-outter wrapper">
						<table border="1" class="table table-bordered table-condensed search-table inner routines" id="viewWorklogTableRoutines">
						</table>
					</div>
				</div>
				<div class="modal-footer" style="border-top: none !important;">
					<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- View Treatment Modal -->
<div class="modal fade" id="viewTreatmentModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content" >
			<div id="lab-report">
				<div class="modal-header bg-blue hidden-print">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="largeModalLabel">PHYSIOTHERAPY TREATMENTS</h4>
				</div>
				<div class="modal-body" id="view-treatment-data">
				<div id="print-content">
					<table border="1" class="table table-bordered table-condensed" id="viewTreatmentTable">
					</table>
				</div>
				</div>
				<div class="modal-footer" style="border-top: none !important;">
					<input type="hidden" name="careplan_id" value="{{Helper::encryptor('encrypt',$l->id)}}"/>
					<input type="hidden" name="crn_number" value="{{$l->lead_id}}" />
					<input type="hidden" name="patient_id" value="{{$l->patient_id}}" />
					<button type="button" class="btn btn-danger waves-effect pull-left noPrint " data-dismiss="modal">Close</button>
					<input type="button" class="btn btn-info noPrint " value="Print" onclick="window.print();"></input>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('plugin.scripts')
<!-- Select Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>

<script src="{{ ViewHelper::ThemePlugin('jquery-ui/jquery-ui.js') }}"></script>
<!-- Multiple Dates Picker -->
<script src="{{ ViewHelper::ThemePlugin('multiple-dates-picker/jquery-ui.multidatespicker.js') }}"></script>


<!-- Input Mask Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>
<!-- Moment Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>

<!-- Bootstrap Tags Input Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('bootstrap-3-typeahead/bootstrap3-typeahead.js') }}"></script>

<!-- Bootstrap Notify Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-notify/bootstrap-notify.js') }}"></script>
<script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
<!-- noUiSlider-->
<script src="{{ asset('themes/default/plugins/nouislider/nouislider.js') }}"></script>
<script src="{{ asset('js/autocomplete.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCvPCONT3odYk2HGHeP3hGhtwXZqx5rekQ&libraries=places"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
@endsection

@section('page.scripts')
	<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
	<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
    <script src="/vendor/datatables/buttons.server-side.js"></script>
    <script>
    var url ='{!! route('lead.get-schedules-ajax',['tid' => Helper::encryptor('encrypt',$l->tenant_id),'lead_id' => $l->id]) !!}';
        $('.pastSchedulesTable').DataTable({
            processing: true,
            lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]],
            serverSide: true,
            ajax: url,
            columns: [
                {data : 'schedule_date', name : 'schedule_date', searchable: true},
                {data : 'service_required', name : 'service_required', searchable: true},
                {data : 'caregiver.first_name', name : 'caregiver.first_name', searchable: true},
				{data : 'status', name : 'status'},
                {data : 'chargeable', name : 'chargeable'}
            ],
			//order: [[ 1, 'desc' ]],
            displayLength: 50,
            drawCallback: function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last = null;

                api.column(1, {page:'current'} ).data().each( function ( group, i ) {
                    if(group != null){
                        if ( last !== group ) {
                            $(rows).eq( i ).before(
								'<tr class="group"><td colspan="6"><b>'+group.toUpperCase()+'</b></td></tr>'
                            );
                            last = group;
                        }
                    }
                });
            }
        });
    </script>
	<script type="text/javascript">
		$(function(){
			$('.content').on('click','.clearLocalStore', function() {
				localStorage.removeItem('leadViewLastTab');
			    return localStorage.removeItem('billableViewLastTab');
			});

			// for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
		    $('.view-lead-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
				 $('html, body').stop().animate({
	 				'scrollTop': $('.tab-menu').offset().top-20
	 			}, 700, 'swing', function () {
	 				//window.location.hash = $('.tab-menu');
	 			});
		        // save the latest tab; use cookies if you like 'em better:
		        localStorage.setItem('leadViewLastTab', $(this).attr('href'));
		    });

		    // go to the latest tab, if it exists:
		    var lastTab = localStorage.getItem('leadViewLastTab');
		    if (lastTab) {
		        $('.view-lead-tabs [href="' + lastTab + '"]').tab('show');
		    }

			$('.billable-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		        // save the latest tab; use cookies if you like 'em better:
		        localStorage.setItem('billableViewLastTab', $(this).attr('href'));
		    });

		    // go to the latest tab, if it exists:
		    var billableLastTab = localStorage.getItem('billableViewLastTab');
		    if (billableLastTab) {
		        $('.billable-tabs [href="' + billableLastTab + '"]').tab('show');
		    }
		});
	</script>
	<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
	<script>

	$(document).ready(function() {

		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});

		$('.content').on('change','#case_vendor', function(){
			console.log($(this).val());
			if($(this).val() != null){
				$('#vendor_id').val($(this).val());
			}else{
				$('#vendor_id').val('');
			}
		});

	    if ($("select[name=checkState] option:selected").val() == 'CASE LOSS') {
	       document.getElementById("casesloss").style.display = "block";
	    }

		$('.content').on('change','.discount_applicable', function(){
			if($(this).val() == 'Y'){
				$('#discount_block').show();
			}else{
				$('#discount_block').hide();
			}
		});
	});

	$(function(){
		$('#editCaseModal #source').selectpicker('val','{{ isset($l->source)?$l->source:'' }}');

		$('#feedback_time').bootstrapMaterialDatePicker({
			format: 'DD-MM-YYYY - hh:mm A',
			time: true,
			clearButton: true,
		});
		$('.getdate').bootstrapMaterialDatePicker({
			format: 'DD-MM-YYYY',
			time: false,
			clearButton: true,
		});
	});

	$('.content').on('click','.btnAddServiceOrder',function(){
		$('#serviceOrderModal #order_service_id').selectpicker('render');
		document.getElementById("serviceOrderForm").reset();
		$('#serviceOrderModal').modal();
	});

	$('.content').on('click','.btnsaveFeedbackComment',function(){
		var feedbackTime    = $('#feedback_time').val();
		var feedbackComment = $('#feedback_comment').val();
		var careplanID      = $('#careplan_id').val();

		if(feedbackTime && feedbackComment){
			$.ajax({
				url : '{{ route('case.save-feedback-comment') }}',
				type: 'POST',
				data: {feedback_time: feedbackTime, feedback_comment: feedbackComment, careplan_id: careplanID},
				success: function (data){
					window.location.reload();
				},
				error: function (error){
					console.log(error);
				}
			});
		}
	});

	function checkCaseLoss(typeSelect)
	{
		if(typeSelect){
			optionValue = document.getElementById("caseloss").value;
			if(optionValue == typeSelect.value){
				document.getElementById("casesloss").style.display = "block";
			}
			else{
				document.getElementById("casesloss").style.display = "none";
			}
		}
		else{
			document.getElementById("casesloss").style.display = "none";
		}
	}

	function printValue(value) {
		if(typeof value != 'undefined'){
			return value;
		}
		return '-';
	}

	function formatKey(str){
		if(str){
			str = str.split("_").join(" ");
		}
		return str.toUpperCaseWords();
	}

	String.prototype.toUpperCaseWords = function () {
		return this.replace(/\w+/g, function(a){
			return a.charAt(0).toUpperCase() + a.slice(1).toLowerCase()
		})
	}

	Array.prototype.contains = function(obj) {
		var i = this.length;
		while (i--) {
			if (this[i] === obj) {
				return true;
			}
		}
		return false;
	}

	$(function(){

		$('.content').on('click', '.btnSelectProvider', function(){
			var id = $(this).attr('data-visit-id');

			var provider = $('input[name=choosen_provider]:checked').val();
			var name = $('input[name=choosen_provider]:checked').data('name');
			var location = $('input[name=choosen_provider]:checked').data('location');
			var phone = $('input[name=choosen_provider]:checked').data('phone');
			var logo = $('input[name=choosen_provider]:checked').data('logo');

			if(typeof provider != 'undefined' && provider != ''){
				$('#providerSearchModal').modal('hide');
				$('#editCaseModal #provider_id').val(provider);
				$('#editCaseModal #selectedProviderName').html('<div class="col-sm-3"><img src="{{ asset('img/provider') }}/'+logo+'" class="img-responsive" style="width: 100px"></div><div class="col-sm-8"><b>'+name+'</b><br>'+location+'<br>Phone: '+phone+'</div>');
			}else{
				alert("Please select provider !");
			}
		});

		$('.content').on('click','.toggle-row-view', function(){
		    var id = $(this).data('row');
		    console.log(id);
		    $('.providerSearchResults tr#'+id).toggle();
		});
	});

//CODING FOR FORMS STARTS

	//Script for form and slider
	var nurseForm  = `@include('partials.forms.assessment.nurse')`;
	var doctorForm = `@include('partials.forms.assessment.doctor')`;
	var physioForm = `@include('partials.forms.assessment.physiotherapist')`;
	var labForm    = `@include('partials.forms.lab.labtest')`;

	$( '#schedule_details_type' ).change(function () {
		var str = ``;
		$( "#schedule_details_type option:selected" ).each(function() {
			str = $( this ).attr("data-specialization");
			document.getElementById("listed_form_type").value = str;
		});

		switch(str) {
			case "nursing":
			str=$.parseHTML(nurseForm);
			$( '.forms' ).html( str );
			$('.forms #assessment_date').bootstrapMaterialDatePicker({format: 'DD-MM-YYYY',time: false,clearButton: true});
			break;
			case "physiotherapy":
			str=$.parseHTML(physioForm);
			$( '.forms' ).html( str );
			addSliderpain('pain_slider_value');
			addSlidermuscle('musclegrade_slider_value');
			$('.forms #assessment_date').bootstrapMaterialDatePicker({format: 'DD-MM-YYYY',time: false,clearButton: true});
			break;
			case "labtest":
			$( ".labhide" ).addClass('invisible');
			str=$.parseHTML(labForm);
			$( '.forms' ).html( str );
			$('.forms #test_id').selectpicker();
			$('.forms #price_type').selectpicker();
			$('.forms #assessment_date').bootstrapMaterialDatePicker({format: 'DD-MM-YYYY - hh:mm A',time: true,clearButton: true,shortTime: true});
			break;
			default:
			str="Please Choose a Caregiver";
			$( '.forms' ).html( str );
			break;
		}
	})

	$('.content').on('click', 'input[name=assessform]', function(){
		console.log($(this));
		if($(this).attr("id")=="ortho"){
			$(".showformtype").not(".formortho").addClass("hide");
			$(".formortho").removeClass("hide");
		}
		if($(this).attr("id")=="neuro"){
			$(".showformtype").not(".formoneuro").addClass("hide");
			$(".formneuro").removeClass("hide");
		}
	});

	function addSliderpain(element){
		var slider = document.getElementById(element);
		noUiSlider.create(slider, {
			start: [0],
			connect: true,
			step:1,
			range: {
				'min': 0,
				'max': 10
			}
		});
		getNoUISliderValuePain(slider, true);
		formshow();
	}

	function addSlidermuscle(element){
		var slider = document.getElementById(element);
		noUiSlider.create(slider, {
			start: [0],
			connect: true,
			step:1,
			range: {
				'min': 0,
				'max': 5
			}
		});
		getNoUISliderValueMuscleGrade(slider, true);
		formshow();
	}

	function formshow(){
		$("input[name=group2]").change(function() {
			var test = $(this).val();
			$(".desc").hide();
			$("#"+test).show();
		});
	};

	function getNoUISliderValuePain(slider, percentage) {
		slider.noUiSlider.on('update', function () {
			var val = slider.noUiSlider.get();
			if (percentage) {
				val = parseInt(val);
	        }
	        $(slider).parent().find('span.js-nouislider-value.pain').text(val);
	    });
	}

	function getNoUISliderValueMuscleGrade(slider, percentage) {
		slider.noUiSlider.on('update', function () {
			var val = slider.noUiSlider.get();
			if (percentage) {
				val = parseInt(val);
	        }
	        $(slider).parent().find('span.js-nouislider-value.mus_gra').text(val);
	    });
	}

	function viewAssessment(element)
	{
		var form_data;
		$( ".assessmentId_"+element).each(function() {
			form_data = $( this ).attr("data-form-data");
		});
		var data = JSON.parse(form_data);
		var html = `<tbody>`;

		var blockedVariables = ['careplan_id','schedule_id','assessform'];
		$.each(data, function(key, data) {
			    // alert(key + ' is ' + data);
			    if(!blockedVariables.contains(key)){
			    	html += `<tr>
			    	<th>`+formatKey(key)+`</th>
			    	<td>`+data+`</td>
			    </tr>`;
			}
		});

		html += `</tbody>`;
		$('#viewassessmentModal .modal-body #viewAssessmentTable').html(html);
	}

	function viewRequirement(element)
	{
		var form_data;
		$( ".requirementId_"+element).each(function() {
			form_data = $( this ).attr("data-form-data");
		});
		var data = JSON.parse(form_data);
		var html = `<tbody>`;

		html = `<tr>
				<th style="text-align:left;width:40%;">Assessment Date<br>Patient's Name<br>Physiotheratist's Name</th>
				<th>`+data['0']['Date']+`<br>{{ isset($l)?$l->patient->first_name:'' }}  {{ isset($l)?$l->patient->last_name:'' }}<br>{{ isset($a->user)?$a->user->full_name:'' }}</th>
				</tr>`;
		$.each(data[0], function(key, data) {
			    // alert(key + ' is ' + data);
			    	html += `<tr>
			    	<th><b>`+formatKey(key)+`</b></th>
			    	<td><b>`+data+`</b></td>
			    </tr>`;
		});
		html += `</tbody>`;
		$('#viewrequirementModal .modal-body #viewRequirementTable').html(html);
	}

	function viewTreatment(element)
	{
		var treatment_data;
		$( ".treatmentId_"+element).each(function() {
			treatment_data = $( this ).attr("data-treatment");
		});

		var data = JSON.parse(treatment_data);
		var html = `<tbody>`;

		html = `<tr>
				<th style="text-align:left;width:40%;">Treatment Date<br>Patient's Name<br>Physiotheratist's Name</th>
				<th>{{ isset($w)?Carbon\Carbon::parse($w->treatmentDate)->format('d-m-Y'):'' }}<br>{{ isset($l)?$l->patient->first_name:'' }}  {{ isset($l)?$l->patient->last_name:'' }}<br>{{ isset($a->user)?$a->user->full_name:'' }}</th>
				</tr>`;
		$.each(data[0], function(key, data) {
			    // alert(key + ' is ' + data);
			    	html += `<tr>
			    	<th><b>`+formatKey(key)+`</b></th>
			    	<td><b>`+data+`</b></td>
			    </tr>`;
		});
		html += `</tbody>`;
		$('#viewTreatmentModal .modal-body #viewTreatmentTable').html(html);
	}
//CODING FOR FORMS ENDS

//CODING FOR WORKLOG STARTS
function viewWorklog(element){
	var vitals = $(element).data("vitals");
	var routines = $(element).data("routines");

	var vitalsData =  $.parseJSON(JSON.stringify(vitals));
	var routinesData =  $.parseJSON(JSON.stringify(routines));

	var html = `<tbody>`;

	if(vitalsData[0]){
		html += `<tr><th colspan="4"><center>Vitals Information</center></th></tr>`;

		html += `<tr>
		<th>Vitals</th>
		<th>Morning</th>
		<th>Noon</th>
		<th>Evening</th>
	</tr>`;

			// Fetch Blood Pressure Information
			html += `<tr>
			<th>Blood Pressure</th>`;

			if(vitalsData[0]['morning'] != null)
				html += `<td>`+printValue(vitalsData[0]['morning']['blood_pressure'])+`</td>`;
			else
				html += `<td>-</td>`;

			if(vitalsData[0]['afternoon'] != null)
				html += `<td>`+printValue(vitalsData[0]['afternoon']['blood_pressure'])+`</td>`;
			else
				html += `<td>-</td>`;

			if(vitalsData[0]['evening'] != null)
				html += `<td>`+printValue(vitalsData[0]['evening']['blood_pressure'])+`</td>`;
			else
				html += `<td>-</td>`;

			html += `</tr>`;

			// Fetch Sugar Level Information
			html += `<tr>
			<th>Sugar Level</th>`;

			if(vitalsData[0]['morning'] != null)
				html += `<td>`+printValue(vitalsData[0]['morning']['sugar_level'])+`</td>`;
			else
				html += `<td>-</td>`;

			if(vitalsData[0]['afternoon'] != null)
				html += `<td>`+printValue(vitalsData[0]['afternoon']['sugar_level'])+`</td>`;
			else
				html += `<td>-</td>`;

			if(vitalsData[0]['evening'] != null)
				html += `<td>`+printValue(vitalsData[0]['evening']['sugar_level'])+`</td>`;
			else
				html += `<td>-</td>`;

			html += `</tr>`;

			// Fetch Temperature Information
			html += `<tr>
			<th>Temperature</th>`;

			if(vitalsData[0]['morning'] != null)
				html += `<td>`+printValue(vitalsData[0]['morning']['temperature'])+`</td>`;
			else
				html += `<td>-</td>`;

			if(vitalsData[0]['afternoon'] != null)
				html += `<td>`+printValue(vitalsData[0]['afternoon']['temperature'])+`</td>`;
			else
				html += `<td>-</td>`;

			if(vitalsData[0]['evening'] != null)
				html += `<td>`+printValue(vitalsData[0]['evening']['temperature'])+`</td>`;
			else
				html += `<td>-</td>`;

			html += `</tr>`;

			// Fetch Pulse Rate Information
			html += `<tr>
			<th>Pulse Rate</th>`;

			if(vitalsData[0]['morning'] != null)
				html += `<td>`+printValue(vitalsData[0]['morning']['pulse_rate'])+`</td>`;
			else
				html += `<td>-</td>`;

			if(vitalsData[0]['afternoon'] != null)
				html += `<td>`+printValue(vitalsData[0]['afternoon']['pulse_rate'])+`</td>`;
			else
				html += `<td>-</td>`;

			if(vitalsData[0]['evening'] != null)
				html += `<td>`+printValue(vitalsData[0]['evening']['pulse_rate'])+`</td>`;
			else
				html += `<td>-</td>`;

			html += `</tr>`;

		}
		else if(vitalsData[0] == null || vitalsData[0] == undefined){
			html += `<tr> <th style="text-align:center;"> NO VITALS ADDED </th> </tr>`;
		}
		html += `<\tbody>`;
		$('#viewWorklogModal .modal-body #viewWorklogTableVitals').html(html);

		var htmlRoutines = `<tbody>`;
		if(routinesData[0]){
			htmlRoutines += `<tr><th colspan="100%"><center>Routines Information</center></th></tr>`;

			htmlRoutines += `<tr>
			<th>Routines</th>
			<th colspan="100%" style="text-align:center;">Tasks</th></tr>`;

				// Fetch Morning Information
				htmlRoutines += `<tr>
				<th>Morning</th>`;
				if(routinesData[0]['morning'] != null)
					$.each(routinesData[0]['morning'], function(k, v) {
					    htmlRoutines += `<td>`+printValue('<b>'+k+ '</b><br><b><small>' + v+'</small></b>')+`</td>`;
					});
				else
					htmlRoutines += `<td>-</td>`;
				htmlRoutines += `</tr>`;

				htmlRoutines += `<tr>
				<th>Afternoon</th>`;
				if(routinesData[0]['afternoon'] != null)
					$.each(routinesData[0]['afternoon'], function(k, v) {
					    htmlRoutines += `<td>`+printValue('<b>'+k+ '</b><br><b><small>' + v+'</small></b>')+`</td>`;
					});
				else
					htmlRoutines += `<td>-</td>`;
				htmlRoutines += `</tr>`;

				htmlRoutines += `<tr>
				<th>Evening</th>`;
				if(routinesData[0]['evening'] != null)
					$.each(routinesData[0]['evening'], function(k, v) {
					    htmlRoutines += `<td>`+printValue('<b>'+k+ '</b><br><b><small>' + v+'</small></b>')+`</td>`;
					});
				else
					htmlRoutines += `<td>-</td>`;
				htmlRoutines += `</tr>`;
			}
			else if(routinesData[0] == null || routinesData[0] == undefined){
				htmlRoutines += `<tr> <th style="text-align:center;"> NO ROUTINES ADDED </th> </tr>`;
			}

		htmlRoutines += `</tbody>`;
		$('#viewWorklogModal .modal-body #viewWorklogTableRoutines').html(htmlRoutines);
	}
//CODING FOR WORKLOG ENDS

</script>
<script>
	var serviceRateList = [];
	@if(isset($services) && count($services))
	@foreach ($services as $service)
	serviceRateList[{{ $service->id }}] = '{{ isset($service->ratecard)?$service->ratecard->amount:0 }}';
	@endforeach
	@endif

	var caseRateCards = [];
	@if(isset($l->ratecards) && count($l->ratecards))
	@foreach ($l->ratecards()->whereStatus(1)->orderBy('id','Desc')->get() as $crc)
	caseRateCards[{{ $crc->service_id }}] = {{ $crc->total_amount }};
	@endforeach
	@endif

	$(function(){

		initBlock();

		// Fill case details to edit modal
		@if($l->medical_conditions)
		@foreach(explode(",",$l->medical_conditions) as $mc)
		$('#editCaseModal #medical_conditions').tagsinput('add','{{ $mc }}');
		@endforeach
		@endif

		$('.content').on('blur','.mdate',function(){
			$('#order_from_date').val('');
			$('#order_to_date').val('');

			var dates = $('#mdates').val();
			dates = dates.replace(/,\s*$/, "");
			$('#mdatesList').val(dates);
			var sessionItems = dates.split(',').length;

			amount = $('#order_rate').val();
			serviceID = $('#order_service_id').selectpicker('val');
			serviceID = serviceID.split("_")[0];

			if($('#mdates').val() != ''){
				days = sessionItems;
			}else{
				days = 1;
			}
			if(serviceRateList[serviceID] != 0){
				amount = parseInt(days) * parseFloat(caseRateCards[serviceID]);
			}

			$('#order_estimated_duration').val(days);
			$('#order_rate').val(amount);
		});

		$('.content').on('change dp.change','#order_from_date, #order_to_date',function(){
			$('#mdates').val('');
			$('#mdatesList').val('');

			if($('#order_from_date').val() != '' && $('#order_service_id option:selected').val()){
				amount = $('#order_rate').val();
				serviceID = $('#order_service_id').selectpicker('val');
				serviceID = serviceID.split("_")[0];

				fromDate = $('#order_from_date').val();
				fromDate = moment(fromDate,'DD-MM-YYYY').format('YYYY-MM-DD');

				if($('#order_to_date').val() != ''){
					toDate = $('#order_to_date').val();
					toDate = moment(toDate,'DD-MM-YYYY').format('YYYY-MM-DD');
					days = moment.duration(moment(toDate).diff(fromDate)).asDays() + 1;
				}else{
					days = 1;
				}
				if(serviceRateList[serviceID] != 0){
					amount = parseInt(days) * parseFloat(caseRateCards[serviceID]);
				}

				$('#order_estimated_duration').val(days);
				$('#order_rate_display').html('Rs. '+parseFloat(caseRateCards[serviceID])+' per day/session');
				$('#order_rate').val(amount);
			}
		});

		$('#editCaseModal #source').selectpicker('val','{{ $l->source }}');

		$('.content').on('click','.btnAddComment',function(){
			$('#commentModal #comment').val('');
			$('#commentModal').modal();
		});

		$('.content').on('click','.btnAddDisposition',function(){
			$('#caseDispositionModal #comment').val('');
			$('#caseDispositionModal').modal();
		});

		$('.content').on('click','.btnEditCarePlan',function(){
			//$('#editCaseModal #comment').val('');
			$('#editCaseModal').modal();
		});

		$('.content').on('click','.btnEditPatient',function(){
			$('#editPatientModal').modal();
		});

		$('.content').on('click','.btnSaveCaseDetails',function(e){
			e.preventDefault();
			var form_data = $('#editCaseForm').serialize();

			$.ajax({
				url: '{{ route('case.update-case-details') }}',
				type: 'POST',
				data: $('#editCaseForm').serialize(),
				success: function (response){
					console.log(response);
					$('#editCaseModal').modal('hide');
					// Update View
					var crnNumber = $('#editCaseModal #crn_number').val();
					var provider = $('#editCaseModal #selectedProviderName').html();
					var careplanName = $('#editCaseModal #careplan_name').val();
					var careplanDesc = $('#editCaseModal #careplan_description').val();
					var medicalConditions = $('#editCaseModal #medical_conditions').val();
					var medications = $('#editCaseModal #medications').val();
					var source = $('#editCaseModal #source option:selected').text();
					var referrerName = $('#editCaseModal #referrer_name').val();
					var service = $('#editCaseModal #service_id option:selected').text();
					var noOfHours = $('#editCaseModal #no_of_hours').val();
					var genderPreference = $('#editCaseModal #gender_preference:checked').val();
					var languagePreference = [];
					$("#editCaseModal input[type=checkbox][name=language_preference]:checked").map(function(){
						languagePreference.push($(this).val());
					});
					var crnNo = $('#editCaseModal #crn_number').val();

					$('#care_plan_tab .col-sm-6 .row:eq(0) > div:eq(1) > .form-group > .form-line').html(crnNumber);
					$('#care_plan_tab .col-sm-6 .row:eq(1) > div:eq(1) > .form-group > .form-line').html(provider);
					$('#care_plan_tab .col-sm-6 .row:eq(2) > div:eq(1) > .form-group > .form-line').html(careplanName);
					$('#care_plan_tab .col-sm-6 .row:eq(3) > div:eq(1) > .form-group > .form-line').html(careplanDesc);
					$('#care_plan_tab .col-sm-6 .row:eq(4) > div:eq(1) > .form-group > .form-line').html(medicalConditions);
					$('#care_plan_tab .col-sm-6 .row:eq(5) > div:eq(1) > .form-group > .form-line').html(medications);
					$('#care_plan_tab .col-sm-6 .row:eq(6) > div:eq(1) > .form-group > .form-line').html(service);
					$('#care_plan_tab .col-sm-6 .row:eq(7) > div:eq(1) > .form-group > .form-line').html(noOfHours);
					$('#care_plan_tab .col-sm-6 .row:eq(8) > div:eq(1) > .form-group > .form-line').html(genderPreference);
					$('#care_plan_tab .col-sm-6 .row:eq(9) > div:eq(1) > .form-group > .form-line').html(languagePreference.toString());
					$('#care_plan_tab .col-sm-6 .row:eq(10) > div:eq(1) > .form-group > .form-line').html(source);
					$('#care_plan_tab .col-sm-6 .row:eq(11) > div:eq(1) > .form-group > .form-line').html(referrerName);

					showNotification('bg-light-green', 'Details updated successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
					window.location.href = response.redirect;
				},
				error: function (error){
					console.log(error);
				}
			});

			return false;
		});

		/*-------------------------------------------
		/*  Handles Schedule Add Block
		/*-------------------------------------------*/
		$('.content').on('click','.btnAddSchedule', function(){
			//$('#serviceOrderSelectionModal').modal();
			$('#schedules_tab #schedulesListDiv').hide();
			$('#schedules_tab .btnCloseSchedule').show();
			$('#schedules_tab .btnAddSchedule').hide();
			$('#schedules_tab .visitListCard').show('slideLeft');

			$('html, body').stop().animate({
				'scrollTop': $('#visit_list_pane').offset().top-20
			}, 700, 'swing', function () {
				//window.location.hash = $('#visit_list_pane');
			});
		});

		$('.content').on('change','#order_service_id', function(){
			val = $(this).val();
			$('#order_from_date').val($('#order_service_id option:selected').data('from')).change();
			$('#order_to_date').val($('#order_service_id option:selected').data('to')).change();

			$('#ratecard_rate').val(caseRateCards[val.split("_")[0]]);
			if(val != 0 && typeof caseRateCards[val.split("_")[0]] != 'undefined'){
				$('#serviceOrderModal #order_rate_display').html('Rs.'+caseRateCards[val.split("_")[0]]+' per day/session');
			}
			$('#order_estimated_duration').val('1');
		});

		$('.content').on('click','.btnSaveServiceOrder', function(e){
			e.preventDefault();

			if($('#serviceOrderModal #order_rate').val() <= 0){
				$('#serviceOrderModal #order_rate').val(0);
				alert("Rate card not set for this service!");
				return false;
			}

			$('#serviceOrderModal #serviceOrderForm').submit();
		});

		// $('.content').on('click','.btnCreateScheduleFromOrder', function(){
		// 	serviceOrderId = $('#serviceOrderSelectionModal #serviceOrderId option:selected').val();
		//
		// 	if(serviceOrderId > 0){
		// 		$('#serviceOrderSelectionModal').modal('hide');
		// 		$('#schedules_tab .visitListCard input[name=service_order_id]').val(serviceOrderId);
		// 		$('#schedules_tab #schedulesListDiv').hide();
		// 		$('#schedules_tab .btnCloseSchedule').show();
		// 		$('#schedules_tab .btnAddSchedule').hide();
		// 		$('#schedules_tab .visitListCard').show('slideLeft');
		// 	}else{
		// 		alert("Please select service order!");
		// 		return;
		// 	}
		// });

		$('.content').on('click','.btnCloseSchedule', function(){
			$('#schedules_tab #schedulesListDiv').show();
			$('#schedules_tab .btnCloseSchedule').hide();
			$('#schedules_tab .btnAddSchedule').show();
			$('#schedules_tab .visitListCard').hide('slideLeft');
			$('#schedules_tab .visitViewCard').hide('slideLeft');
		});

		$('.content').on('click','.slideVisitList', function(){
			var id = $(this).attr('data-visit-id');
			var current_class = $(this).find('i').html();
			if(current_class == 'expand_more')
				$(this).find('i').html('expand_less');
			else
				$(this).find('i').html('expand_more');

			$('#visitList_'+id+' div.body').slideToggle("slow");
		});

		$('.content').on('click','.removeVisitList', function(){
			if(confirm("Are you sure?")){
				$('#schedules_tab #schedulesListDiv').show();
				$('#schedules_tab .btnCloseSchedule').hide();
				$('#schedules_tab .btnAddSchedule').show();
				$('#schedules_tab .visitListCard').hide('slideLeft');
				$('#schedules_tab .visitViewCard').hide('slideLeft');
			}
		});

		$('.content').on('click','.btnChooseProvider', function(){
			var id = $(this).data('visit-id');
			$('#providerSearchForm #filter_service').selectpicker('val',$('#editCaseModal #service_id').val());
			$('#providerSearchForm #filter_city').val($('#city').val());
			$('.btnFilterProvider').trigger( "click" );
			$('#providerSearchModal .btnSelectProvider').attr('data-visit-id',id);
			$('#providerSearchModal').modal();
		});

		/*-------------------------------------------
		/*  Handles Existing Customer Check
		/*-------------------------------------------*/
		$('.content').on('click', '#checkForPreviousCarePlans', function(){
			checkIfExistingCustomer();
		});

		/*-------------------------------------------
		/*  Handles Case Disposition Update
		/*-------------------------------------------*/
		$('.content').on('click','.saveCarePlanDisposition', function(){
			var id = $(this).data('lead-id');
			var status = $('#lead_status option:selected').val();

			var caselosscomment = $("input[name=loss]:checked").val();

			var date = $('#lead_disposition_date').val();
			var comment = $('#lead_comment').val();

			if(status){
				$.ajax({
					url: '{{ route('lead.update-disposition') }}',
					type: 'POST',
					data: {id: id, status: status, date: date, comment: comment, caselosscomment: caselosscomment},
					success: function (data){
						console.log(data);
						if(data){
							window.location.reload();
						}
					},
					error: function (error){
						console.log(error);
					}
				});
			}
		});

		$('.content').on('click','.btnChooseStaff', function(){
			var block = $(this).attr('data-block');
			var profile = $(this).attr('data-profile');
			var caregiver = $(this).attr('data-caregiver');

			//$('#staffSearchModal .btnSelectStaff').attr('data-visit-id',id);
			$('#staffSearchModal .btnSelectStaff').attr('data-block',block);
			$('#staffSearchModal .btnSelectStaff').attr('data-profile',profile);
			$('#staffSearchModal .btnSelectStaff').attr('data-caregiver',caregiver);

			if(block == '.visitListCard '){
				var fDate = $('.visitListCard #start_date').val();
				var tDate = $('.visitListCard #end_date').val();

				var fTime = $('.visitListCard #start_time').val();
				var tTime = $('.visitListCard #end_time').val();

				$('#staffSearchForm #filter_from_date').val(fDate);
				if(tDate != ''){
					$('#staffSearchForm #filter_to_date').val(tDate);
					$('#staffSearchForm .filter_period').show();
					if($('.visitListCard input[name=schedule_type]:checked').val() == 'Shift'){
						$('#staffSearchForm #filter_from_date').val(fDate);
						// if($('.visitListCard input[name=visit_type]:checked').val() == 'Period' && tDate != ''){
						if(tDate != ''){
							$('#staffSearchForm #filter_to_date').val(tDate);
							$('#staffSearchForm .filter_period').show();
						}else{
							$('#staffSearchForm #filter_to_date').val(fDate);
							$('#staffSearchForm .filter_period').hide();
						}
					}else{
						$('#staffSearchForm #filter_to_date').val(fDate);
						$('#staffSearchForm .filter_period').hide();
						$('#staffSearchForm #filter_from_date').val(fDate);
						// $('#staffSearchForm #filter_to_date').val(tDate);
						$('#staffSearchForm #filter_to_date').val(fDate);
					}
				}

				$('#staffSearchForm #filter_from_time').val(fTime);
				$('#staffSearchForm #filter_to_time').val(tTime);

				if(fDate == ''){
					alert("Please select date!");
					return false;
				}

				if(fTime == '' || tTime == ''){
					alert("Please select timings!");
					return false;
				}
			}

			$('#staffSearchModal').modal();
		});

		// Define a variable for your dataTable object to use
		var reportListDataTable = null;

		$('.content').on('click','.btnFilterStaff', function(){
			var flag = 'uninterrupted';
			var interruptedDates = $('.visitListCard #service_order_id').find(':selected').data('sessiondates');

			if(interruptedDates != ''){
				var flag = 'interrupted';
			}

			fDate = $('#filter_from_date').val();
			tDate = $('#filter_to_date').val();

			fTime = $('#filter_from_time').val();
			tTime = $('#filter_to_time').val();

			specialization = $('#filter_specialization option:selected').val();
			experience = $('#filter_experience option:selected').val();
			city = $('#filter_city').val();

			showOnDutyStaff = $('#include_onduty_staff').is(':checked')?1:0;

			var language = [];
			$('#filter_language :selected').each(function(i, selected){
			  language[i] = $(selected).text();
			});

			var skill = [];
			$('#filter_skill :selected').each(function(i, selected){
			  skill[i] = $(selected).val();
			});

			$.ajax({
				url: '{{ route('ajax.caregiver.filter') }}',
				type: 'POST',
				data: {pid: '{{ Helper::encryptor('encrypt',$l->patient_id) }}',fd: fDate, td: tDate, ft: fTime, tt: tTime, s: specialization, e: experience, c: city, l: language, sk: skill, sods: showOnDutyStaff, flag: flag, dates: interruptedDates},
				success: function (data){
					html = '<tr><td class="text-center" colspan="7">Click \'Apply Filter\' to get results</td></tr>';
					if(data['result'].length){
						html = '';
						rdata = data['result'];
						patientLocation = data['patient_location'];
						$.each(rdata, function(i){
							distanceTxt = '-';
							trClass = rdata[i].onduty?'light-red':'';
							trTitle = rdata[i].onduty?'Already scheduled for a visit':'';

							var coordinates = rdata[i].coordinates;
							if(coordinates != '' && patientLocation != ''){
								lat = coordinates.split("_")[0];
								lng = coordinates.split("_")[1];

								patLat = patientLocation.split("_")[0];
								patLng = patientLocation.split("_")[1];
								distanceTxt = calculateDistance(lat, lng, patLat, patLng);
							}

							html += `<tr class="`+trClass+`" title="`+trTitle+`">
								<td>`+rdata[i].full_name+`<br><small>`+rdata[i].location+`</small></td>
								<td>`+rdata[i].mobile+`<br><small>`+rdata[i].email+`</small></td>
								<td>`+rdata[i].specialization+`</td>
								<td>`+rdata[i].experience+`</td>
								<td>`+rdata[i].total_visits+`</td>
								<td>`+distanceTxt+`</td>
								<td class="text-center"><input name="choosen_emp" type="radio" id="choosen_emp_`+rdata[i].id+`" class="with-gap radio-col-blue" data-name="`+rdata[i].full_name+`" data-mobile="`+rdata[i].mobile+`" value="`+rdata[i].id+`" /><label for="choosen_emp_`+rdata[i].id+`"></label></td>
							</tr>`;
						});

						// Destroy the dataTable and empty because the columns may change!
						if (reportListDataTable !== null ) {
						    // for this version use fnDestroy() instead of destroy()
						    reportListDataTable.fnDestroy();
						    reportListDataTable = null;
						    // empty in case the columns change
						    $('.staffSearchResults tbody').empty();
						}

						$('.staffSearchResults tbody').html(html);

						// Build dataTable with ajax, columns, etc.
						reportListDataTable = $('.staffSearchResults').dataTable({
							"dom": 'frtip',
							"order": [[ 5, 'asc' ]],
							"scrollY": '50vh',
					        "scrollCollapse": true,
					        "paging": false
						});

						$('#staffSearchForm').toggle();
						$('.btnStaffFilter').toggle();
					}else{
						$('.staffSearchResults tbody').html(html);
					}
				},
				error: function (error){
					console.log(error);
				}
			});
		});

		$('.content').on('input','.amtCal', function(){
			calculateTotal();
		});

		$('.content').on('change','input[name=discount_type]', function(){
			calculateTotal();
		});

		$('.content').on('click','.btnReset', function(e){
			e.preventDefault();
			clearSearchForm();
		});

		$('.content').on('click', '.btnStaffFilter', function(){
			$('#staffSearchForm').toggle();
			$('.btnStaffFilter').toggle();
		});

		$('.content').on('click', '.btnSelectStaff', function(){
			// var id = $(this).attr('data-visit-id');
			// var mode = $(this).attr('data-mode');
			var block = $(this).attr('data-block');
			var profile = $(this).attr('data-profile');
			var caregiver = $(this).attr('data-caregiver');

			var emp = $('input[name=choosen_emp]:checked').val();
			var name = $('input[name=choosen_emp]:checked').data('name');
			var mobile = $('input[name=choosen_emp]:checked').data('mobile');

			if(typeof emp != 'undefined' && emp != ''){
				$('#staffSearchModal').modal('hide');
				console.log($(block + caregiver));
				$(block + ' ' + caregiver).val(emp);
				$(block + ' ' + profile).html('<div class="col-sm-3"><img src="/img/user.png" width="40" height="40" class="img-circle"></div><div class="col-sm-9">'+name+'<br>Mob: '+mobile+'</div><br>');

				// if(mode == 'view'){
				// 	$('#viewScheduleModal #caregiver_id').val(emp);
				// 	$('#viewScheduleModal #selectedStaffName_'+id).html('<div class="col-sm-3"><img src="/img/user.png" width="40" height="40" class="img-circle"></div><div class="col-sm-9">'+name+'<br>Mob: '+mobile+'</div><br>');
				// }else{
				// 	$('#visitList_'+id+' #caregiver_id').val(emp);
				// 	$('#visitList_'+id+' #selectedStaffName_'+id).html('<div class="col-sm-3"><img src="/img/user.png" width="40" height="40" class="img-circle"></div><div class="col-sm-9">'+name+'<br>Mob: '+mobile+'</div>');
				// }
			}else{
				alert("Please select staff !");
			}
		});

		$('.content').on('change','select[name=service_order_id]', function(){
			sessionDates = null;
			serviceID = $('.visitListCard select[name=service_order_id] option:selected').data('service');
			fromDate = $('.visitListCard select[name=service_order_id] option:selected').data('from');
			toDate = $('.visitListCard select[name=service_order_id] option:selected').data('to');

			if(fromDate == '' && toDate == ''){
				fromDate = $('.visitListCard select[name=service_order_id] option:selected').data('sessionFrom');
				toDate = $('.visitListCard select[name=service_order_id] option:selected').data('sessionTo');
				sessionDates = $('.visitListCard select[name=service_order_id] option:selected').data('session');
			}

			$('.visitListCard input[name=start_date]').val(fromDate);
			$('.visitListCard input[name=end_date]').val(toDate);
			$('.visitListCard input[name=service_required]').val(serviceID);
			$('.visitListCard input[name=session_dates]').val(sessionDates);
		});

		$('.content').on('change','select[name=service_order_id_0]', function(){
			serviceID = $('#schedules_tab .visitViewCard select[name=service_order_id_0] option:selected').data('service');
			fromDate = $('#schedules_tab .visitViewCard select[name=service_order_id_0] option:selected').data('from');
			toDate = $('#schedules_tab .visitViewCard select[name=service_order_id_0] option:selected').data('to');

			$('#schedules_tab .visitViewCard input[name=start_date_0]').val(fromDate);
			$('#schedules_tab .visitViewCard input[name=end_date_0]').val(toDate);
			$('#schedules_tab .visitViewCard input[name=service_required_0]').val(serviceID);
		});

		$('.rateCardInput').prop('disabled',true);
	});

	function initBlock()
	{

		var holidayList = [];
		var holidayListRev = [];
		var date = new Date();

		$('.date').datetimepicker({
			format: 'DD-MM-YYYY',
			disabledDates: holidayListRev,
			daysOfWeekDisabled:[0],
			showClose: true,
			showClear: true,
			minDate: date,
			useCurrent: false,
			keepInvalid:true
		});

		$('.schedule-from').bootstrapMaterialDatePicker({
			format: 'DD-MM-YYYY',
			time: false,
			clearButton: true,
			weekStart: 1,
			minDate: moment().subtract(5,'d')
		}).on('change', function (e, date){
			$('.schedule-to').bootstrapMaterialDatePicker({
				format: 'DD-MM-YYYY',
				time: false,
				clearButton: true,
				weekStart: 1,
				minDate: date,
				maxDate: moment(date).add(5,'d')
			});
		});


		$('.mdate').multiDatesPicker({
			minDate: 0,
			dateFormat: "dd-mm-yy",
			addDisabledDates: holidayList,
			beforeShowDay: function(day) {
				var day = day.getDay();
				if (day == 0) {
					return [false,""]
				} else {
					return [true,""]
				}
			}
		});

		$('.time').bootstrapMaterialDatePicker({
			format: 'HH:mm',
			time: true,
			date: false,
			shortTime: true,
			clearButton: true,
		});

		$('.content').on('change','input[name=start_time], input[name=end_time]', function(){
			var fromTime = $('input[name=start_time]').val();
			var toTime = $('input[name=end_time]').val();

			if(fromTime && toTime){
				startTime = moment(fromTime,'HH:mm');
				endTime = moment(toTime,'HH:mm');

				duration = moment.duration(endTime.diff(startTime));

				$('.timing_duration').show();
				$('.timing_duration').html('Duration: '+ parseFloat(duration.asHours()).toFixed(1) + ' hr(s) ');
			}
		});

		$('.content').on('change','input[name=schedule_type]', function(){
			if($(this).val() == 'Visit'){
				$('.shift_type_block').hide();
				$('.toLabelBlock, .toDateBlock').hide();
			}else{
				$('.shift_type_block').show();
				$('.toLabelBlock, .toDateBlock').show();

				var fDate = $('.schedule-from').val();
				//restrictEndDate(fDate);
			}
		});
	}

	function restrictEndDate(startDate){
		$('.schedule-to').bootstrapMaterialDatePicker({
			format: 'DD-MM-YYYY',
			time: false,
			clearButton: true,
			daysOfWeekDisabled: [0,6],
			weekStart: 1,
			minDate: startDate,
			maxDate: moment().add(5,'d')
		});
	}

	function clearSearchForm()
	{
		//$('#staffSearchModal #staffSearchForm #reset').trigger('click');
		$('#staffSearchModal #staffSearchForm #filter_specialization').selectpicker('val','0');
		$('#staffSearchModal #staffSearchForm #filter_experience').selectpicker('val','0');
		$('#staffSearchModal #staffSearchForm #filter_language').selectpicker('val','0');
		$('#staffSearchModal #staffSearchForm #filter_skill').selectpicker('val','0');
		$('#staffSearchModal .staffSearchResults tbody').html('<tr><td class="text-center" colspan="7">Click \'Search\' to getresults</td></tr>');
	}

	var visitListArray = [];

	$('.content').on('click','.btnSaveSchedule', function(e){
		e.preventDefault();
		var form = '#visit_list_pane';

		if($(form+' #service_order_id option:selected').val() == ''){
			alert("Please select service order!");
			return;
		}

		if($(form+' #start_date').val() == ''){
			alert("Please select date!");
			return;
		}

		if($(form+' #service_required option:selected').val() == ''){
			alert("Please select service required!");
			return;
		}

		// if($(form+' #caregiver_id').val() == '' || $(form+' #caregiver_id').val() == '0'){
		// 	alert("Please select staff!");
		// 	return;
		// }

		$('#visit_list_pane').submit();
	});

	// $('.content').on('click','.btnSaveSchedule', function(){
	// 	var leadID = $('#lead_id').val();
	// 	var patientID = $('#patient_id').val();
	//
	// 	// Parse all Visit List
	// 	var block = '#schedules_tab #visit_list_pane .visitListCard';
	// 	if($(block).length){
	// 		visitListArray = [];
	//
	// 		var id = $(block +' input[name=id]').val();
	// 		var serviceOrderId = $(block+ ' input[name=service_order_id]').val();
	// 		var scheduleType = $(block+ ' input[name=schedule_type]:checked').val();
	// 		var visitType = $(block+ ' input[name=visit_type]:checked').val();
	// 		var startDate = $(block+ ' #start_date').val();
	// 		var endDate = $(block+ ' #end_date').val();
	// 		if(endDate == ''){
	// 			endDate = startDate;
	// 		}
	// 		var startTime = $(block+ ' #end_time').val();
	// 		var endTime = $(block+ ' #end_time').val();
	// 		var shift = $(block+ ' #shift').val();
	// 		var serviceRequired = $(block+ ' #service_required').val();
	// 		var caregiverID = $(block+ ' #caregiver_id').val();
	// 		var nc = $(block+' input[name=chargeable]').is(':checked')?'1':'0';
	// 		var notes = $(block+ ' #notes').val();
	//
	// 		visitListArray = {
	// 			'lead_id': leadID,
	// 			'patient_id': patientID,
	// 			'service_order_id': serviceOrderId,
	// 			'schedule_type': scheduleType,
	// 			'visit_type':visitType,
	// 			'start_date': startDate,
	// 			'end_date': endDate,
	// 			'start_time': startTime,
	// 			'end_time': endTime,
	// 			'shift': shift,
	// 			'service_required':serviceRequired,
	// 			'caregiver_id':caregiverID,
	// 			'notes': notes,
	// 			'chargeable': nc
	// 		};
	//
	// 		console.log(visitListArray);
	// 	}
	//
	// 	if(visitListArray){
	// 		// $.ajax({
	// 		// 	url: '{{ route('lead.save-schedule') }}',
	// 		// 	type: 'POST',
	// 		// 	data: {id: id, lead_id: leadID, patient_id: patientID, service_order_id: serviceOrderId, schedule_type: scheduleType, start_date: startDate, end_date: endDate, start_time: startTime, end_time: endTime, shift: shift, service_required: serviceRequired, caregiver_id: caregiverID, chargeable: nc, notes: notes, status: status},
	// 		// 	// data: {_token: '{{ csrf_token() }}', crn_no: crnNo, careplan_id: careplanID, patient_id: patientID, visit: visitListArray},
	// 		// 	success: function (data){
	// 		// 		console.log(data);
	// 		// 		window.location.reload();
	// 		// 	},
	// 		// 	error: function (error){
	// 		// 		console.log(error);
	// 		// 	}
	// 		// });
	// 	}
	// });

	$('.content').on('click','.btnUpdateSchedule', function(e){
		e.preventDefault();

		var leadID = $('#lead_id').val();
		var patientID = $('#patient_id').val();

		// Parse all Visit List
		var block = '#schedules_tab .visitViewCard #schedule_view_tab';
		if($(block).length){
			visitListArray = [];

			var id = $(block +' input[name=id]').val();
			var serviceOrderId = $(block+ ' select[name=service_order_id_0] option:selected').val();
			var scheduleType = $(block+ ' input[name=schedule_type_0]').val();
			var scheduleDate = $(block+ ' #schedule_date_0').val();
			var startTime = $(block+ ' #start_time_0').val();
			var endTime = $(block+ ' #end_time_0').val();
			var shift = $(block+ ' input[name=shift_0]:checked').val();
			var serviceRequired = $(block+ ' input[name=service_required_0]').val();
			var caregiverID = $(block+ ' #caregiver_id_0').val();
			var nc = $(block+' input[name=chargeable_0]:checked').val();
			var notes = $(block+ ' #notes_0').val();
			var status = $(block+ ' #status_0').val();

			visitListArray = {
				'id': id,
				'lead_id': leadID,
				'patient_id': patientID,
				'service_order_id': serviceOrderId,
				'schedule_type': scheduleType,
				'schedule_date': scheduleDate,
				'start_time': startTime,
				'end_time': endTime,
				'shift': shift,
				'service_required':serviceRequired,
				'caregiver_id':caregiverID,
				'notes': notes,
				'chargeable': nc,
				'status': status,
			};

			console.log(visitListArray);
		}

		if(visitListArray){
			$.ajax({
				url: '{{ route('lead.save-schedule') }}',
				type: 'POST',
				data: {id: id, lead_id: leadID, patient_id: patientID, service_order_id: serviceOrderId, schedule_type: scheduleType, schedule_date: scheduleDate, start_time: startTime, end_time: endTime, shift: shift, service_required: serviceRequired, caregiver_id: caregiverID, chargeable: nc, notes: notes, status: status},
				success: function (data){
					console.log(data);
					window.location.reload();
				},
				error: function (error){
					console.log(error);
				}
			});
		}
	});

	function calculateTotal()
	{
		var totalAmount = 0;

		var block = '#rateCardForm';
		var amount = $(block+' #rate').val();
		amount = amount?parseFloat(amount):0;

		var discount = $(block+' input[name=discount_applicable]:checked').val();
		var dt = $(block+' input[name=discount_type]:checked').val();

		var dv = $(block+' #discount_value').val();
		dv = dv?parseFloat(dv):0;

		//console.log(amount + ' : ' + dv);

		totalAmount = amount;
		if(discount == 'Y'){
			if(dt == 'P'){
				totalAmount = parseFloat(totalAmount) - parseFloat(totalAmount) * (dv/100);
			}

			if(dt == 'F'){
				totalAmount = parseFloat(totalAmount) - dv;
			}
		}

		$(block+' #total_amount').val(parseFloat(totalAmount).toFixed(2));
	}

	$('.content').on('change','#ratecard_service',function(){
		if(serviceRateList.length){
			if(typeof serviceRateList[$(this).val()] != 'undefined' && serviceRateList[$(this).val()] != '0'){
				$('#rateCardForm #rate').val(serviceRateList[$(this).val()]).trigger("input");
				$('.rateCardInput').prop('disabled',false);
				$('.rateCardError').hide();
			}else{
				$('#rateCardForm #rate').val('').trigger("input");
				$('.rateCardInput').prop('disabled',true);
				$('.rateCardError').show();
			}
		}
	});

	$('.content').on('click','.btnSaveCaseRateCard', function(e){
		e.preventDefault();

		if($('#rateCardForm #ratecard_service option:selected').val() == ''){
			alert("Please select Service");
			return false;
		}

		if($('#rateCardForm #rate').val() == ''){
			alert("Please enter Rate");
			$('#name').focus();
			return false;
		}

		if($('#rateCardForm #total_amount').val() == ''){
			alert("Please enter total amount");
			$('#total_amount').focus();
			return false;
		}

		$.ajax({
			url: '{{ route('lead.save-case-ratecard') }}',
			type: 'POST',
			data: $('#rateCardForm').serialize()+'&lead_id='+$('#lead_id').val(),
			success: function(data){
				console.log(data);
				window.location.reload();
				if(data){
					status = data.status == 1?'Active':'Inactive';
					discount = '-';

					if(data.discount_applicable == 'Y'){
						var dtype = data.discount_type;
						if(dtype == 'P'){
							discount = data.discount_value +' %';
						}else{
							discount = 'Rs. '+data.discount_value;
						}
					}

					if(data.period_from != null){
						fromDate = moment(data.period_from,'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY');
					}
					if(data.period_to != null){
						toDate = moment(data.period_to,'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY');
					}

					html = `<tr>
								<td>`+data.service.service_name+`</td>
								<td>Rs. `+data.rate+`</td>
								<td>`+discount+`</td>
								<td>`+fromDate + ` to ` + toDate +`</td>
								<td>Rs. `+data.total_amount+`</td>
								<td>`+status+`</td>
							</tr>`;

					if($('#ratecard_tab .rateCardTable tbody tr:eq(0) td').length == 1){
						$('#ratecard_tab .rateCardTable tbody').html(html);
					}else{
						$('#ratecard_tab .rateCardTable tbody').prepend(html);
					}

					$('#rateCardForm')[0].reset();
				}
			},
			error: function (err){
				console.log(err);
			}
		});

		return false;
	});

	$('.content').on('change','input[name=charge_type]',function(){
		if($(this).val() == 'RateCard'){
			$('#schedules_tab .visitListCard .visit_rate_card_block').show();
			$('#schedules_tab .visitListCard #visit_rate_card').selectpicker('val','');
		}else{
			$('#schedules_tab .visitListCard .visit_rate_card_block').hide();
			$('#schedules_tab .visitListCard #visit_rate_card').selectpicker('val','');
			$('#schedules_tab .visitListCard #visit_amount').val('');
		}
	});

	$('.content').on('change','input[name=charge_type_0]',function(){
		if($(this).val() == 'RateCard'){
			$('#schedules_tab .visitViewCard .visit_rate_card_block').show();
			$('#schedules_tab .visitViewCard #visit_rate_card').selectpicker('val','');
		}else{
			$('#schedules_tab .visitViewCard .visit_rate_card_block').hide();
			$('#schedules_tab .visitViewCard #visit_rate_card').selectpicker('val','');
			$('#schedules_tab .visitViewCard #visit_amount').val('');
		}
	});

	$('.content').on('change','#visit_rate_card',function(){
		var value = $(this).val();
		$('#schedules_tab .visitListCard #visit_amount').val(value.split("_")[1]);
	});

	$('.content').on('change','#visit_rate_card_0',function(){
		var value = $(this).val();
		$('#schedules_tab .visitViewCard #visit_amount_0').val(value.split("_")[1]);
	});

	$('.content').on('change','#filter_from_date',function(){
		var value = $(this).val();
		if(!$('.filter_period:eq(0)').is(':visible')){
			$('#filter_to_date').val(value);
		}
	});

	$('.content').on('change','input[name=visit_type]',function(){
		if($(this).val() == 'Single'){
			$('#schedules_tab .visitListCard .toLabelBlock').hide();
			$('#schedules_tab .visitListCard .toDateBlock').hide();
		}else{
			$('#schedules_tab .visitListCard .toLabelBlock').show();
			$('#schedules_tab .visitListCard .toDateBlock').show();
		}
	});

	$('.content').on('change','input[name=schedule_type_0]',function(){
		if($(this).val() == 'Visit'){
			$('#schedules_tab .visitViewCard .toLabelBlock').hide();
			$('#schedules_tab .visitViewCard .toDateBlock').hide();
			$('#schedules_tab .visitViewCard .shift_type_block').hide();
		}else{
			$('#schedules_tab .visitViewCard .toLabelBlock').show();
			$('#schedules_tab .visitViewCard .toDateBlock').show();
			$('#schedules_tab .visitViewCard .shift_type_block').show();
		}
	});

	function getServiceInterruptionsList(scheduleID){
		if(scheduleID){
			var leadID = $('#lead_id').val();
			$.ajax({
				url: '{{ route('lead.get-service-interruption') }}',
				type: 'POST',
				data: {lead_id: leadID, schedule_id: scheduleID},
				success: function (data){
					//console.log(data);
					if(data.length){
						html = ``;
						$(data).each(function(i){
							var fdate = new Date(data[i].from_date);
							var tdate = new Date(data[i].to_date);
							var cdate = new Date(data[i].created_at);
							noOfDays = moment.duration(moment(tdate).diff(moment(fdate)));

							html += `<tr>
										<td>`+(i+1)+`</td>
										<td>`+moment(fdate).format('DD-MM-YYYY')+`</td>
										<td>`+moment(tdate).format('DD-MM-YYYY')+`</td>
										<td>`+noOfDays.asDays()+`</td>
										<td>`+data[i].reason+`</td>
										<td>`+moment(cdate).format('DD-MM-YYYY')+`</td>
										<td>`+data[i].user.full_name+`</td>
									 </tr>`;

							logHtml = `<ul>`;
							if(data[i].audits.length){
								var audits = data[i].audits;
								console.log(audits);
								$(audits).each(function(j){

								});
								logHtml += `</ul>`;
							}
						});
						$('.serviceInterruptionsTable tbody').html(html);
					}
				},
				error: function (err){
					console.log(err);
				}
			});
		}
	}

	$('.content').on('click','.btnSaveServiceInterruption',function(e){
		e.preventDefault();

		var tabBlock = '#schedules_tab .visitViewCard #schedule_interruptions_tab';

		var leadID = $('#lead_id').val();
		var scheduleID = $(this).data('id');

		var fromDate = $(tabBlock +' #from_date').val();
		var toDate = $(tabBlock +' #to_date').val();
		var reason = $(tabBlock +' #interruption_reason').val();

		if(fromDate == ''){
			alert("Please enter From date");
			$(tabBlock +' #from_date').focus();
			return false;
		}

		if(toDate == ''){
			alert("Please enter To date");
			$(tabBlock +' #to_date').focus();
			return false;
		}

		$.ajax({
			url: '{{ route('lead.save-service-interruption') }}',
			type: 'POST',
			data: { lead_id: leadID, schedule_id: scheduleID, from_date: fromDate, to_date: toDate, reason: reason},
			success: function (data){
				//console.log(data);
				showAlert('success',data.message,'#schedules_tab .alert-block');

				$(tabBlock+' #serviceInterruptionForm')[0].reset();

				getServiceInterruptionsList(scheduleID);
			},
			error: function (error){
				console.log(error);
				if(!error.res){
					showAlert('error',error.message,'#schedules_tab .alert-block');
				}
			}
		});
	});

	$('.content').on('click','.btnAddService', function(){
		block = '#serviceRequestModal #serviceRequestForm ';
		$(block+' #service_requested_id').selectpicker('val','0');
		$(block+' #service_requested_id').selectpicker('refresh');
		$(block+' #service_recommended_id').selectpicker('val','0');
		$(block+' #service_recommended_id').selectpicker('refresh');
		$(block+' #service_request_from_date').val('');
		$(block+' #service_request_to_date').val('');
		$(block+' #frequency_period').val('1');
		$(block+' #frequency').selectpicker('val','');
		$(block+' #frequency').selectpicker('refresh');
	});

	$('.content').on('click','.btnEditServiceRequests', function(){
		data = $(this).data();
		if(data){
			block = '#serviceRequestModal #serviceRequestForm ';
			$(block+' #service_requested_id').selectpicker('val',data.serviceRequested);
			$(block+' #service_requested_id').selectpicker('refresh');
			$(block+' #service_recommended_id').selectpicker('val',data.serviceRecommended);
			$(block+' #service_recommended_id').selectpicker('refresh');
			$(block+' #service_request_from_date').val(data.fromDate);
			$(block+' #service_request_to_date').val(data.toDate);
			$(block+' #frequency_period').val(data.frequencyPeriod);
			$(block+' #frequency').selectpicker('val',data.frequency);
			$(block+' #frequency').selectpicker('refresh');
			$(block+' input[name=id]').val(data.id);

			$('#serviceRequestModal').modal();
		}
	});

	$('.content').on('click','.btnEditServiceOrder', function(){
		data = $(this).data();
		console.log(data);
		if(data){
			block = '#serviceOrderModal #serviceOrderForm ';
			$(block+' #order_service_id').selectpicker('val',data.serviceId);
			$(block+' #order_service_id').selectpicker('refresh');
			$(block+' #order_from_date').val(data.fromDate).trigger("change");
			$(block+' #mdates').val(data.sessionDates).trigger("change");
			$(block+' #order_to_date').val(data.toDate).trigger("change");
			$(block+' #order_rate').val(data.orderRate);
			$(block+' input[name=id]').val(data.id);
			$(block+' input[name=service_order_status][value='+data.status+']').prop('checked',true);

			$('#serviceOrderModal').modal();
		}
	});

	function showAlert(type, message, block=''){
		var typeClass = 'alert-info';
		if(type == 'success') typeClass = 'alert-success';
		if(type == 'error') typeClass = 'alert-danger';

		if(block == ''){
			block = '.content > .container-fluid';
		}
		html = `<div class="alert ` + typeClass + ` alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					`+ message +`
                </div>`;

		$(block).prepend(html);
	}

	//calculateDistance(12.9467756,77.55191089999994,12.9719161,77.52988559999994, 'K');
	//calculateDistance(59.3293371,13.4877472, 59.3225525,13.4619422,'K');

	//calcCrow(12.9467756,77.55191089999994,12.9719161,77.52988559999994);
	//calcCrow(59.3293371,13.4877472, 59.3225525,13.4619422);

	function calculateDistance(lat1, lon1, lat2, lon2, unit) {
		var radlat1 = Math.PI * lat1/180;
		var radlat2 = Math.PI * lat2/180;
		var theta = lon1-lon2;
		var radtheta = Math.PI * theta/180;
		var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
		dist = Math.acos(dist);
		dist = dist * 180/Math.PI;
		dist = dist * 60 * 1.1515;
		if (unit=="K") { dist = dist * 1.609344; }
		if (unit=="N") { dist = dist * 0.8684; }

		//console.log('calculateDistance: '+dist);

		return dist.toFixed(2);
	}

	function calcCrow(lat1, lon1, lat2, lon2)
    {
		var R = 6371; // km
		var dLat = toRad(lat2-lat1);
		var dLon = toRad(lon2-lon1);
		var lat1 = toRad(lat1);
		var lat2 = toRad(lat2);

		var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
		Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		var d = R * c;
		console.log('calcCrow: '+d);
		return d;
    }

    // Converts numeric degrees to radians
    function toRad(Value)
    {
        return Value * Math.PI / 180;
    }
</script>
@endsection
