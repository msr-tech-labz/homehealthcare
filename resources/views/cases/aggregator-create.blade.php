@extends('layouts.main-layout')

@section('page_title','New Case - Cases |')

@section('active_cases','active')

@section('plugin.styles')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<!-- Bootstrap Tagsinput Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">

@endsection

@section('page.styles')
<style>
	.card .header .header-dropdown{
		top: 15px;
	}
	hr{
		margin-top: 10px;
		margin-bottom: 10px;
	}
	.bootstrap-tagsinput{
		min-height: 40px;
		vertical-align: top;
		padding: 4px 0;
		width: 100%;
	}
	.bootstrap-tagsinput input{
		padding-left: 0;
	}
	.plus:after{
		content: ' + ';
		font-size: 18px;
		font-weight: bold;
	}
	.care-plan-card .header{
		padding: 15px 20px;
	}
	.modal-lg{
		width: 70%;
	}
	.modal .modal-header{
		padding: 20px 25px 15px 25px;
	}
	.modal-footer{
		border-top: 1px solid #ddd !important;
	}
</style>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="row clearfix">
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif

<div class="row">
	<form id="caseForm" name="caseForm">
		{{ csrf_field() }}
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<a href="{{ route('case.index') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
						<i class="material-icons">arrow_back</i>
					</a>
					<h2 class="col-sm-10">
						{{ isset($e)?'Edit':'New'}} Case
						<small>Case {{ isset($e)?'Updation':'Creation'}} Form</small>
					</h2>
					<ul class="header-dropdown m-r--5">
						<div class="btn-group" role="group">
							<button type="button" class="btn btn-success m-l-15 waves-effect"><i class="material-icons" style="color: #fff">save</i></button>
							<button type="button" onclick="javascript: return validator();" class="btn btn-success waves-effect" style="line-height: 1.9;padding-left:0">Save Case</button>
						</div>&nbsp;&nbsp;
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="body" style="padding-top: 0">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active">
							<a href="#patient_details_tab" data-toggle="tab" aria-expanded="false">
								<i class="material-icons">person</i> Patient &amp; Enquirer Details
							</a>
						</li>
						<li role="presentation" class="">
							<a href="#care_plan_tab" data-toggle="tab" aria-expanded="true">
								<i class="material-icons">child_friendly</i> Case &amp; Provider Details
							</a>
						</li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane fade active in" id="patient_details_tab">
							<div class="row clearfix">
								<div class="col-sm-6 form-horizontal">
									<h2 class="card-inside-title">
										Patient Details
										<small>Name, Mobile Number, Email</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="first_name">First Name</label>
										</div>
										<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="first_name" name="first_name" class="form-control searchCol" placeholder="First Name" value="{{ isset($l)?$l->name:'' }}" required>
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="last_name" name="last_name" class="form-control searchCol" placeholder="Last Name" value="">
												</div>
											</div>
										</div>
									</div>
									{{-- <div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="date_of_birth">Date of Birth</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="date_of_birth" name="date_of_birth" class="form-control date searchCol" placeholder="Date of Birth"value="{{ isset($e)?$e->date_of_birth:'' }}">
												</div>
											</div>
										</div>
									</div> --}}
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="gender">Gender</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select class="form-control show-tick" id="gender" name="gender" required>
													<option value="">-- Please select--</option>
													<option value="Male">Male</option>
													<option value="Female">Female</option>
													<option value="Other">Other</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="patient_age">Patient Age</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="patient_age" name="patient_age" class="form-control" placeholder="Patient Age"value="{{ isset($e)?$e->patient_age:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="patient_weight">Patient Weight</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="patient_weight" name="patient_weight" class="form-control" placeholder="Patient Weight"value="{{ isset($e)?$e->patient_weight:'' }}">
												</div>
											</div>
										</div>
									</div>
									<h2 class="card-inside-title">
										Patient Address
										<small>Address / Area, City and State</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="street_address">Street Address</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="street_address" name="street_address" class="form-control" placeholder="Street Address" value="{{ isset($lead)?$lead->street_address:'' }}" autocomplete="off"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="area">Area</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="area" name="area" class="form-control" placeholder="area / Area" value="{{ isset($lead)?$lead->area:'' }}" autocomplete="off"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="city">City</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="city" name="city" class="form-control" placeholder="City" value="{{ isset($lead)?$lead->city:'' }}" autocomplete="off"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="zipcode">Zip Code</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="zipcode" name="zipcode" class="form-control" placeholder="Zip Code" autocomplete="off"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="state">State</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="state" name="state" class="form-control" placeholder="State" autocomplete="off"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="country">Country</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="country" name="country" class="form-control" placeholder="Country" autocomplete="off"/>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<div class="col-sm-6 form-horizontal">
									<h2 class="card-inside-title">
										Enquirer Details
										<small>Enquirer Name, Phone, Email, Relationship</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="enquirer_name">Enquirer Name</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="enquirer_name" name="enquirer_name" class="form-control" placeholder="Enquirer Name" value="{{ isset($l)?$l->name:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="enquirer_phone">Enquirer Phone</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="number" id="enquirer_phone" name="enquirer_phone" class="form-control" placeholder="Enquirer Phone" value="{{ isset($l)?$l->phone:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="alternate_number">Alternate Number</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="number" id="alternate_number" name="alternate_number" class="form-control searchCol" placeholder="Alternate Number" value="{{ isset($l)?$l->alternate_number:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="enquirer_email">Enquirer Email</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="email" id="enquirer_email" name="enquirer_email" class="form-control" placeholder="Enquirer Email" value="{{ isset($l)?$l->email:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="relationship_with_patient">Relationship with Patient</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select id="relationship_with_patient" name="relationship_with_patient" class="form-control show-tick" data-live-search="true" placeholder="Relationship with Patient">
													<option value=""> -- Select Relationship -- </option>
													<option value="BROTHER">BROTHER</option>
													<option value="BROTHER-IN-LAW">BROTHER-IN-LAW</option>
													<option value="DAUGHTER">DAUGHTER</option>
													<option value="FATHER">FATHER</option>
													<option value="FATHER-IN-LAW">FATHER-IN-LAW</option>
													<option value="FRIEND">FRIEND</option>
													<option value="GRANDFATHER">GRANDFATHER</option>
													<option value="GRANDMOTHER">GRANDMOTHER</option>
													<option value="HUSBAND">HUSBAND</option>
													<option value="MOTHER">MOTHER</option>
													<option value="MOTHER-IN-LAW">MOTHER-IN-LAW</option>
													<option value="SELF">SELF</option>
													<option value="SISTER">SISTER</option>
													<option value="SON">SON</option>
													<option value="WIFE">WIFE</option>
													<option value="OTHER">OTHER</option>
												</select>
											</div>
										</div>
									</div>
									<h2 class="card-inside-title">
										Case History
										<small>Previous Case History (If any)</small>
									</h2>
									<div class="row clearfix text-center" style="min-height: 200px;">
										<a class="btn btn-primary btn-lg waves-effect" id="checkForPreviousCarePlans">Check for Previous Careplans</a>
									</div>
								</div>
							</div>
						</div>

						<div role="tabpanel" class="tab-pane fade" id="care_plan_tab">
							<div class="row clearfix form-horizontal">
								<div class="col-sm-6">
									<h2 class="card-inside-title">
										Case Details
										<small>Medical Condition, Medication, Notes</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="medical_conditions">Medical Conditons</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input class="medical_conditions" id="medical_conditions" name="medical_conditions" data-provide="typeahead"  type="text" />
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="medications">Medications</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input class="medications" name="medications" id="medications" data-provide="typeahead" type="text"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="designation">Service Required</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select class="form-control show-tick" data-live-search="true" id="service_id" name="service_id">
													<option value="0">-- Please select --</option>
												@if(isset($services) && count($services))
													@foreach($services as $s)
													<option value="{{ $s->id }}">{{ $s->service_name }}</option>
													@endforeach
												@endif
												</select>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="no_of_hours">No. of hours</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="number" id="no_of_hours" name="no_of_hours" class="form-control" placeholder="No. of hours">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="gender_preference">Gender Preference</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group" style="margin-top: 1%">
												<input name="gender_preference" type="radio" id="gender_preference_any" class="with-gap radio-col-deep-purple" value="Any" checked />
												<label for="gender_preference_any">Any</label>
												<input name="gender_preference" type="radio" id="gender_preference_male" class="with-gap radio-col-deep-purple" value="Male" />
												<label for="gender_preference_male">Male</label>
												<input name="gender_preference" type="radio" id="gender_preference_female" class="with-gap radio-col-deep-purple" value="Female"/>
												<label for="gender_preference_female">Female</label>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="language_preference">Language Preference</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group" style="margin-top: 2.5%">
												<div class="demo-checkbox language-preference">
											@if(isset($languages) && count($languages))
												@foreach($languages as $l)
													<input type="checkbox" id="languages_{{ strtolower($l->language_name) }}" name="language_preference[]" class="filled-in chk-col-light-blue" value="{{ $l->language_name }}" />
													<label for="languages_{{ strtolower($l->language_name) }}">{{ $l->language_name }}</label>
												@endforeach
											@endif
												</div>
											</div>
										</div>
									</div>

									<h2 class="card-inside-title">
										Referral Details
										<small>Referrer Name, Case Source</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="source">Source</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select class="form-control show-tick" data-live-search="true" id="source" name="source">
													<option value="">--Select--</option>
													<option value="Google Search">Google Search</option>
													<option value="Website">Website</option>
													<option value="Referral from Service Partner">Referral from Service Partner</option>
													<option value="Online Classifieds">Online Classifieds</option>
													<option value="Email Campaign">Email Campaign</option>
													<option value="Facebook">Facebook</option>
													<option value="Twitter">Twitter</option>
													<option value="Google Plus">Google Plus</option>
													<option value="Events">Events</option>
													<option value="Exhibitions">Exhibitions</option>
													<option value="Flyer">Flyer</option>
													<option value="Print Classifieds">Print Classifieds</option>
													<option value="Radio">Radio</option>
													<option value="Television">Television</option>
													<option value="Print Ads">Print Ads</option>
													<option value="News/Media">News/Media</option>
													<option value="Other">Other</option>
												</select>
											</div>
										</div>
									</div>
									<input type="hidden" id="referrer_name" value="Apnacare">
									<input type="hidden" id="lead_id" value="{{ isset($lead)?$lead->id:0 }}">
								</div>
								<div class="col-sm-6">
									<h2 class="card-inside-title">
										Provider Details
										<small>Service Provider Assignment</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"></div>
										<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
											<a class="btn btn-warning btn-block waves-effect btnChooseProvider" data-visit-id="1">Choose Provider</a>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="provider">Selected Provider</label>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
											<div class="form-group">
												<input type="hidden" id="provider_id" value="" />
												<div style="margin-top: 8px;" id="selectedProviderName" data-visit-id="1">-</div>
											</div>
										</div>
									</div><br>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="case_charges">Case Charges</label>
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
											<div class="form-group">
												<div class="form-line">
													<input type="text" class="form-control" id="case_charges" name="case_charges" placeholder="Ex. 650">
												</div>
											</div>
										</div>
									</div><br>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="provider">Provider Notes</label>
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
											<div class="form-group">
												<div class="form-line">
													<textarea style="text-transform: capitalize;" class="form-control" rows="4" id="provider_notes" name="provider_notes" placeholder="Date or Timing Preference, Pricing Preference or any notes to provider"></textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>

<!-- Provider Search Modal -->
<div class="modal fade" id="providerSearchModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="largeModalLabel">Provider Search Form</h4>
			</div>
			<div class="modal-body">
				<div class="row clearfix">
					<form id="providerSearchForm" class="col-sm-12">
						<div class="col-sm-4">
							<div class="form-group">
								<label for="filter_service">Service Required</label>
								<div class="form-line">
									<select class="form-control show-tick" data-live-search="true" id="filter_service">
										<option value="0">-- Please select --</option>
									@if(isset($services) && count($services))
										@foreach($services as $s)
										<option value="{{ $s->id }}">{{ $s->service_name }}</option>
										@endforeach
									@endif
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label for="filter_gender">Gender Preference</label>
								<div class="form-line">
									<select class="form-control show-tick" id="filter_gender">
										<option value="0">-- Any --</option>
										<option value="Male">Male</option>
										<option value="Female">Female</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label for="filter_city">City</label>
								<div class="form-line">
									<input class="form-control" id="filter_city" placeholder="City">
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<br>
							<a class="btn btn-primary waves-effect btnFilterProvider">Search</a>&nbsp;&nbsp;
							<button type="reset" class="btn btn-danger waves-effect btnReset">Reset</button>
						</div>
					</form>
					<div class="col-sm-12" style="max-height: 400px;overflow:auto;">
						<div class="page-loader-wrapper">
							<div class="loader">
								<div class="md-preloader pl-size-md">
									<svg viewbox="0 0 75 75">
										<circle cx="37.5" cy="37.5" r="33.5" class="pl-blue" stroke-width="4" />
									</svg>
								</div>
								<p>Please wait...</p>
							</div>
						</div>
						<table class="table table-bordered table-striped providerSearchResults">
							<thead>
								<tr>
									<th>Provider</th>
									<th>Location</th>
									<th>Contact</th>
									<th width="20%">Availability</th>
									<th width="10%" class="text-center">Caregivers</th>
									<th width="10%" class="text-center">Rates</th>
									<th width="5%"></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-center" colspan="7">Click 'Search' to get results</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success waves-effect btnSelectProvider" data-visit-id="0">Continue</button>
				<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>
@endsection

@section('plugin.scripts')
<!-- Select Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<!-- Input Mask Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>
<!-- Moment Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<!-- Bootstrap Tags Input Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/typeahead.min.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('bootstrap-3-typeahead/bootstrap3-typeahead.js') }}"></script>

<!-- Bootstrap Notify Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-notify/bootstrap-notify.js') }}"></script>
<script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>

<script src="{{ asset('js/autocomplete.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCvPCONT3odYk2HGHeP3hGhtwXZqx5rekQ&libraries=places"></script>

<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
@endsection

@section('page.scripts')
<script type="text/javascript">
    window.addEventListener('keydown', function(e) {
        if (e.keyIdentifier == 'U+000A' || e.keyIdentifier == 'Enter' || e.keyCode == 13) {
            if (e.target.nodeName == 'INPUT' && e.target.type == 'text' && !$(e.path[1]).hasClass('bootstrap-tagsinput')) {
                e.preventDefault();
                return false;
            }
        }
    }, true);
</script>
<script>
	$(function(){
		initBlock();

		initAutoComplete({
			'route': 'area',
			'locality': 'city',
			'postal_code': 'zipcode',
			'administrative_area_level_1': 'state',
			'country': 'country'
		});

		var medicalConditions = [
		  {name: "Hospitalized"},
		  {name: "COPD"},
		  {name: "Post Operative Care"},
		  {name: "Ortho Case"},
		  {name: "Neuro Case"},
		  {name: "Cardiac Case"},
		  {name: "Hyper Tension"},
		  {name: "Hypo Tension"},
		  {name: "Diabetes"},
		  {name: "Alzhiemers"},
		  {name: "Parkinsons"},
		  {name: "Bed Ridden"},
		  {name: "Bladder Incontinence"},
		  {name: "Weakness"},
		  {name: "Arthritis"},
		  {name: "TKR"},
		  {name: "Dressing"},
		  {name: "BedSore"},
		  {name: "Cancer"},
		  {name: "Dialysis"},
		  {name: "Stroke"},
		  {name: "Injections"},
		  {name: "Tracheotomy"},
		  {name: "Suction"},
		  {name: "Catheter"},
		  {name: "hip Surgery"},
		  {name: "Knee Replacement"}
		];


		$('.medical_conditions').tagsinput({
		  typeahead: {
		    source: medicalConditions.map(function(item) { return item.name }),
		    afterSelect: function() {
		    	this.$element[0].value = '';
		    }
		  }
		});

		var medications = [
		  {name: "Cardiac Medicine"},
		  {name: "Diabetic Medicine"},
		  {name: "Ryles Tube"},
		  {name: "Peg Tube"},
		  {name: "Hypertension Medicine"},
		  {name: "Pain Killer"},
		  {name: "Oral Mediation"},
		  {name: "IV/IM Medications"}
		];

		$('.medications').tagsinput({
		  typeahead: {
		    source: medications.map(function(item) { return item.name }),
		    afterSelect: function() {
		    	this.$element[0].value = '';
		    }
		  }
		});

		$('.tags-input').tagsinput({
			allowDuplicates: false
		});

		var referralNames = new Bloodhound({
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			local:  ["(A)labama","Alaska","Arizona","Arkansas","Arkansas2","Barkansas"]
		});
		referralNames.initialize();

		$('.referrer-name').tagsinput({
			typeaheadjs: {
				name: 'referralNames',
				source: referralNames.ttAdapter()
			}
		});

		$('.content').on('click', '#checkForPreviousCarePlans', function(){
			checkIfExistingCustomer();
		});

		$('.content').on('click','.btnChooseProvider', function(){
			var id = $(this).data('visit-id');

			$('#providerSearchForm #filter_city').val($('#patient_details_tab #city').val());
			$('#providerSearchModal .btnSelectProvider').attr('data-visit-id',id);
			$('#providerSearchModal').modal();
		});

		$('.content').on('click', '.btnSelectProvider', function(){
			var id = $(this).attr('data-visit-id');

			var provider = $('input[name=choosen_provider]:checked').val();
			var name = $('input[name=choosen_provider]:checked').data('name');
			var location = $('input[name=choosen_provider]:checked').data('location');
			var phone = $('input[name=choosen_provider]:checked').data('phone');
			var logo = $('input[name=choosen_provider]:checked').data('logo');

			 if(typeof provider != 'undefined' && provider != ''){
				 $('#providerSearchModal').modal('hide');
				 $('#provider_id').val(provider);
				 $('#selectedProviderName').html('<div class="col-sm-3"><img src="{{ asset('uploads/provider') }}/'+provider+'/'+logo+'" class="img-responsive" style="width: 100px"></div><div class="col-sm-8"><b>'+name+'</b><br>'+location+'<br>Phone: '+phone+'</div>');
			 }else{
				 alert("Please select provider !");
			 }
		});

		$('.content').on('click','.btnFilterProvider', function(){
			gender = $('#filter_gender option:selected').val();
			city = $('#filter_city').val();

			$.ajax({
				url: '{{ route('ajax.provider.filter') }}',
				type: 'POST',
				data: {_token: '{{ csrf_token() }}', g: gender, c: city},
				success: function (data){
					html = '<tr><td colspan="6">No matching provider(s) found</td></t>';
					if(data.length){
						html = '';
						$(data).each(function(i){
							var details = data[i].details;
							var caregivers = data[i].caregivers;
							var rates = data[i].rates;

							html += `<tr data-id="`+details.id+`" class="provider-details">`;

							if(details){
								html += `<td title="`+details.organization_name+`">`+details.organization_short_name+`<br><b> [`+details.orgcode+`]</b></td>`;
								html += `<td>`+details.city+`<br><small>`+details.area+`</small></td>`;
								html += `<td>`+details.contact_person+`<br><small>`+details.phone_number+`</small></td>`;
								html += `<td><table><tr><th>Male</th><td>`+details.availability.m+`</td><th>Female</th><td>`+details.availability.f+`</td></tr></table></td>`;
								if(caregivers.length)
									html += `<td class="text-center"><a href="javascript: void(0);" class="toggle-row-view" data-row="caregivers_`+details.id+`">View</a></td>`;
								else
									html += `<td class="text-center">-</td>`;

								if(rates.length)
									html += `<td class="text-center"><a href="javascript: void(0);" class="toggle-row-view" data-row="rates_`+details.id+`">View</a></td>`;
								else
									html += `<td class="text-center">-</td>`;

								html += `<td class="text-center"><input name="choosen_provider" type="radio" id="choosen_provider_`+details.id+`" class="with-gap radio-col-blue" data-name="`+details.organization_name+`" data-location="`+details.city+`" value="`+details.id+`" data-phone="`+details.phone_number+`" data-logo="`+details.logo+`" /><label for="choosen_provider_`+details.id+`"></label></td>`;
							}

							html += `</tr>`;

							if(caregivers.length){
								html += `<tr style="display: none; background: #fbe7ad" id="caregivers_`+details.id+`"><td colspan="7">
											<table class="table table-striped table-condensed" style="margin:0">
												<thead class="bg-cyan">
													<tr>
														<th>Caregiver Name</th>
														<th>Age</th>
														<th>Specialization</th>
														<th>Qualification</th>
														<th>Experience</th>
														<th>Languages Known</th>
													</tr>
												</thead>
												<tbody>`;
								$(caregivers).each(function(j){
									html += `<tr>`;
									html += `<td>`+caregivers[j].caregiver_name+`</td>` +
											`<td>`+caregivers[j].caregiver_age+`</td>` +
											`<td>`+caregivers[j].caregiver_specialization+`</td>` +
											`<td>`+caregivers[j].caregiver_qualification+`</td>` +
											`<td>`+caregivers[j].caregiver_experience+`</td>` +
											`<td>`+caregivers[j].caregiver_languages_known+`</td>`;
									html += `</tr>`;
								});

								html += `</tbody></table></td></tr>`;
							}

							if(rates.length){
								html += `<tr style="display: none; background: #fbe7ad" id="rates_`+details.id+`"><td colspan="7">
											<table class="table table-striped table-condensed" style="margin:0">
												<thead class="bg-teal">
													<tr>
														<th>Servie Name</th>
														<th>Rate</th>
													</tr>
												</thead>
												<tbody>`;
								$(rates).each(function(k){
									html += `<tr>`;
									html += `<td>`+rates[k].service_name+`</td>` +
											`<td>`+rates[k].rate+`</td>`;
									html += `</tr>`;
								});

								html += `</tbody></table></td></tr>`;
							}

						});
					}

					$('.providerSearchResults tbody').html(html);
					$('.providerSearchResults').DataTable();
				},
				error: function (error){
					console.log(error);
				}
			})
		});

		$('.content').on('click','.toggle-row-view',function(){
			id = $(this).data('row');
			$('.providerSearchResults tbody tr#'+id).toggle();
		})

		$('.content').on('input','.amtCal', function(){
			var id = $(this).data('visit-id');
			calculateTotal(id);
		});

		$('.content').on('click','.btnReset', function(){
			clearSearchForm();
		});
	});

	function initBlock()
	{
		$('.date').inputmask('dd-mm-yyyy', { placeholder: '__-__-____' });
	}

	function checkIfExistingCustomer()
	{
		var fname = $('#first_name').val();
		var lname = $('#last_name').val();
		var mobile = $('#mobile').val();
		var email = $('#email').val();
		var dob = $('#date_of_birth').val();

		if(fname != '' || lname != '' || mobile != '' || email != '' || dob != ''){
			$.ajax({
				url: '{{ route('case.check-for-existing-case') }}',
				type: 'POST',
				data: {f: fname, l: lname, m: mobile, e: email, d: dob, _token: '{{ csrf_token() }}'},
				success: function (data){
					console.log(data);
				},
				error: function (error){
					console.log(error);
				}
			});
		}
	}

	function clearSearchForm()
	{
		$('#providerSearchModal #providerSearchForm #reset').trigger('click');
		$('#providerSearchModal #providerSearchForm #filter_gender').selectpicker('val','0');
		$('#providerSearchModal .providerSearchResults tbody').html('<tr><td class="text-center" colspan="7">Click \'Search\' to getresults</td></tr>');
	}

	var visitListArray = [];

	function saveCase()
	{

		// Parse Patient Details
		var patientDetails = [];
		var fname = $('#first_name').val();
		var lname = $('#last_name').val();
		var patientAge = $('#patient_age').val();
		var patientWeight = $('#patient_weight').val();
		var gender = $('#gender option:selected').val();

		var address = $('#street_address').val();
		var area = $('#area').val();
		var city = $('#city').val();
		var zip = $('#zipcode').val();
		var state = $('#state').val();
		var country = $('#country').val();

		// Parse Enquirer Details
		var enquirerName = $('#enquirer_name').val();
		var enquirerPhone = $('#enquirer_phone').val();
		var alternateNumber = $('#alternate_number').val();
		var enquirerEmail = $('#enquirer_email').val();
		var relationship = $('#relationship_with_patient option:selected').val();

		patientDetails = {
			'first_name': fname,
			'last_name': lname,
			'gender': gender,
			'patient_age': patientAge,
			'patient_weight': patientWeight,
			'street_address': address,
			'area': area,
			'city': city,
			'zipcode': zip,
			'state': state,
			'country': country,
			'enquirer_name': enquirerName,
			'enquirer_phone': enquirerPhone,
			'alternate_number': alternateNumber,
			'enquirer_email': enquirerEmail,
			'relationship_with_patient': relationship
		};

		console.log(patientDetails);

		// Parse Care Plan Details
		var carePlanDetails = [];
		var medicalConditions = $('#medical_conditions').val();
		var medications = $('#medications').val();

		var serviceRequired = $('#service_id option:selected').val();
		var serviceName = $('#service_id option:selected').text();
		var noOfHours = $('#no_of_hours').val();
		var caseCharges = $('#case_charges').val();
		var genderPreference = $('input[name=gender_preference]:checked').val();
		var languagePreference = [];
		$(".language-preference input[type=checkbox]:checked").map(function(){
			languagePreference.push($(this).val());
		});
		var lead = $('#lead_id').val();
		var source = $('#source').val();
		var referrerName = $('#referrer_name').val();

		var providerID = $('#provider_id').val();
		var providerNotes = $('#provider_notes').val();

		carePlanDetails = {
			'lead_id': lead,
			'provider_notes': providerNotes,
			'medical_conditions': medicalConditions,
			'medications': medications,
			'service_id': serviceRequired,
			'service_name': serviceName,
			'no_of_hours': noOfHours,
			'case_charges': caseCharges,
			'gender_preference': genderPreference,
			'language_preference': languagePreference.toString(),
			'source': source,
			'referrer_name': referrerName
		};

		console.log(carePlanDetails);

			if(patientDetails && carePlanDetails){
				$.ajax({
					url: '{{ route('case.save-case-aggregator') }}',
					type: 'POST',
					data: {_token:'{{ csrf_token() }}',pid: providerID, patient: patientDetails, careplan: carePlanDetails},
					success: function (data){
						console.log(data);
						alert("Case Created Successfully");
						window.location.href = '{{ route('case.index',['viewer' => 'self']) }}';
					},
					error: function (error){
						console.log(error);
					}
				});
			}else{
				showNotification('bg-red', 'Please enter case details!', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
			}
		// console.log(patientDetails);
		// console.log(carePlanDetails);

		return false;
	}

	function validator() {
		var fname = $('#first_name').val();
		var gender = $('#gender option:selected').val();
		var patientAge = $('#patient_age').val();
		var address = $('#street_address').val();
		var city = $('#city').val();
		var state = $('#state').val();
		var enquirerName = $('#enquirer_name').val();
		var enquirerPhone = $('#enquirer_phone').val();
		var medicalConditions = $('#medical_conditions').val();
		var medications = $('#medications').val();
		var serviceRequired = $('#service_id option:selected').val();
		var noOfHours = $('#no_of_hours').val();

		if (fname == ""){
			alert("Please Enter First Name");
			$("label[for='"+$('#first_name').attr('id')+"']").addClass('required');
			document.getElementById("first_name").focus();

			return false;
		}
		if (serviceRequired == ""){
			alert("Please Enter Service Required");
			$("label[for='"+$('#service_id').attr('id')+"']").addClass('required');
			document.getElementById("service_id").focus();

			return false;
		}
		if (gender == ""){
			alert("Please Enter Gender");
			$("label[for='"+$('#gender').attr('id')+"']").addClass('required');
			document.getElementById("gender").focus();

			return false;
		}
		if (patientAge == ""){
			alert("Please Enter Patient's Age");
			$("label[for='"+$('#patient_age').attr('id')+"']").addClass('required');
			document.getElementById("patient_age").focus();

			return false;
		}
		if (address == ""){
			alert("Please Enter Address");
			$("label[for='"+$('#address').attr('id')+"']").addClass('required');
			document.getElementById("address").focus();

			return false;
		}
		if (city == ""){
			alert("Please Enter City");
			$("label[for='"+$('#city').attr('id')+"']").addClass('required');
			document.getElementById("city").focus();

			return false;
		}
		if (state == ""){
			alert("Please Enter State");
			$("label[for='"+$('#state').attr('id')+"']").addClass('required');
			document.getElementById("state").focus();

			return false;
		}
		if (enquirerName == ""){
			alert("Please Enter Enquirer Name");
			$("label[for='"+$('#enquirer_name').attr('id')+"']").addClass('required');
			document.getElementById("enquirer_name").focus();

			return false;
		}
		if (enquirerPhone == ""){
			alert("Please Enter Enquirer Phone");
			$("label[for='"+$('#enquirer_phone').attr('id')+"']").addClass('required');
			document.getElementById("enquirer_phone").focus();

			return false;
		}
		if (medicalConditions == ""){
			alert("Please Enter Medical Condition");
			$("label[for='"+$('#medical_conditions').attr('id')+"']").addClass('required');
			document.getElementById("medical_conditions").focus();

			return false;
		}
		if (medications == ""){
			alert("Please Enter Medications");
			$("label[for='"+$('#medications').attr('id')+"']").addClass('required');
			document.getElementById("medications").focus();

			return false;
		}
	    if (noOfHours == ""){
	    	alert("Please Enter no. of Hours");
	    	$("label[for='"+$('#no_of_hours').attr('id')+"']").addClass('required');
			document.getElementById("no_of_hours").focus();

	    	return false;
	    }
	    saveCase();
	}
</script>
@endsection
