@extends('layouts.main-layout')

@section('page_title','View Case - Cases |')

@section('active_cases','active')

@section('plugin.styles')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<!-- Bootstrap Tagsinput Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
<link href="{{ ViewHelper::ThemePlugin('nouislider/nouislider.min.css') }}" />

<link rel="stylesheet" type="text/css" href="{{ asset('css/print.css') }}" media="print" />
@endsection

@section('page.styles')
<style>
	@page
	{
		size: auto;   /* auto is the initial value */
		margin: 0mm;  /* this affects the margin in the printer settings */
	}
	/*.bootstrap-notify-container{
		z-index: 99999 !important;
	}*/
	.form-group .form-line, .form-group{
		text-transform: uppercase; !important;
	}
	.form-group label{
		text-transform: capitalize !important;
	}
	.pac-container {
		z-index: 9999 !important;
	}
	.nav-tabs > li{
		/*min-width: 12%;*/
	}
	.card .header .header-dropdown{
		top: 15px;
	}
	hr{
		margin-top: 10px;
		margin-bottom: 10px;
	}
	.table-borderless tbody tr td, .table-borderless tbody tr th{
		border: none;
		padding: 6px;
	}
	.table-borderless tbody tr td{
		min-width: 150px;
	}
	.table-borderless tbody tr td:before{
		content: ' :';
		padding-right: 10px;
		font-weight: bold;
	}
	.bootstrap-tagsinput{
		min-height: 40px;
		vertical-align: top;
		padding: 4px 0;
		width: 100%;
	}
	.bootstrap-tagsinput input{
		padding-left: 0;
	}
	.plus:after{
		content: ' + ';
		font-size: 18px;
		font-weight: bold;
	}
	.care-plan-card .header{
		padding: 15px 20px;
	}
	.modal-lg{
		width: 70%;
	}
	.modal .modal-header{
		padding: 20px 25px 15px 25px;
	}
	.modal-footer{
		border-top: 1px solid #ddd !important;
	}
	.details-pane .details{
		margin-top: 6px;
	}
	.details-pane .details .form-line{
		border-bottom: none;
	}
	.details-pane .details .form-line .form-control, .details-pane .details .form-line .form-control:focus{
		outline: 0;
		border-bottom: 1px solid #ff5722 !important;
		/*-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(255, 87, 34, .6);
		box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(255, 87, 34, .6);*/
	}

	.card .body .col-lg-9, .card .body .col-lg-3{
		margin-bottom: 8px !important;
	}
	.careplan-status button.waves-effect{
		width: 160px !important;
	}
	.comment-card { margin-bottom: 15px;}
	.comment-card .header { padding: 5px 10px;}
	.comment-card .body { padding: 10px;}
	.comment-card .header h2 {font-size: 16px}
	.comment-card .header .header-dropdown li > span {font-size: 13px}
	.comment-card th{ background: #ccc}
	.comment-card .table{ margin-bottom: 0}

	.details-pane div.col-lg-8, .details-pane div.col-lg-4{ margin-bottom: 8px !important}

	.noUi-horizontal .noUi-handle{
		background-color: #ff0000;
	}
	.text-muted {
	    color: #777;
	    position: absolute;
	    right: 10%;
	}	
/*    .showformtype{
    display: none;
}*/
</style>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="row clearfix">
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif

<div class="row  hidden-print">
	<form id="caseForm">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<a href="{{ route('case.index') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
						<i class="material-icons">arrow_back</i>
					</a>
					<h2 class="col-sm-10">
						{{ isset($c)?'View':'New'}} Care Plan
						<small>Patient Careplan Details</small>
					</h2>
					<div class="clearfix"></div>
				</div>
				<div class="body" style="padding: 10px 8px;">
					<div clas="row clearfix" style="margin-bottom: 0">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="card" style="margin-bottom: 0">
								<div class="header bg-blue-grey" style="padding: 15px">
									<h2>
										<a style="margin-top: -5px;" class="btn bg-deep-orange btn-sm waves-effect pull-right btnEditPatient">Edit Info</a>
										Patient Information
									</h2>
								</div>
								<div class="body" style="padding: 8px; 10px">
									<div class="col-sm-2" style="width: 15%">
										<img src="/img/user.png" style="height:100px;width: 100px;" class="img-thumbnail">
									</div>
									<div class="col-sm-10">
										<table class="table table-condensed table-borderless" style="margin-bottom: 0">
											<tr>
												<th colspan="2"></th>
											</tr>
											<tr>
												<th>Patient Name</th>
												<td>{{ $c->patient->first_name }}</td>
												<th>Gender</th>
												<td>{{ $c->patient->gender ?? '-' }}</td>
												<th>Address</th>
												<td>{{ $c->patient->street_address ?? '-' }}</td>
											</tr>
											<tr>
												<th>Age</th>
												<td>{{ !empty($c->patient->patient_age)?$c->patient->patient_age:'-' }}</td>
												<th>Weight</th>
												<td>{{ $c->patient->patient_weight }}</td>
												<th>Area</th>
												<td>{{ $c->patient->area ?? '-' }}</td>
											</tr>
										</table>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="row clearfix" style="margin: 20px;">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active">
								<a href="#comments_tab" data-toggle="tab" aria-expanded="true">
									<i class="material-icons">comment</i> Case History {!! count($c->comments)?'&nbsp;&nbsp; <span class="badge bg-orange">'.count($c->comments).'</span>':'' !!}
								</a>
							</li>
							<li role="presentation" class="">
								<a href="#care_plan_tab" data-toggle="tab" aria-expanded="true">
									<i class="material-icons">child_friendly</i> Case Details
								</a>
							</li>
							<li role="presentation" class="">
								<a href="#schedules_tab" data-toggle="tab">
									<i class="material-icons">date_range</i> Schedules {!! count($c->schedules)?'&nbsp;&nbsp; <span class="badge bg-cyan">'.count($c->schedules).'</span>':'' !!}
								</a>
							</li>
							<li role="presentation" class="">
								<a href="#assessment_tab" data-toggle="tab">
									<i class="material-icons">assignment</i> Forms  {!! count($c->assessment)?'&nbsp;&nbsp; <span class="badge bg-cyan">'.count($c->assessment).'</span>':'' !!}
								</a>
							</li>
							<li role="presentation" class="">
								<a href="#tasks_tab" data-toggle="tab">
									<i class="material-icons">view_list</i> Tasks Log {!! count($c->worklog)?'&nbsp;&nbsp; <span class="badge bg-blue">'.count($c->worklog).'</span>':'' !!}
								</a>
							</li>
							<li role="presentation" class="">
								<a href="#feedback_tab" data-toggle="tab">
									<i class="material-icons">call</i> Feedback Call {!! count($feedbackcomments)?'&nbsp;&nbsp; <span class="badge bg-blue">'.count($feedbackcomments).'</span>':'' !!}
								</a>
							</li>
						</ul>

						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane fade active in" id="comments_tab">
								<br>
								<div class="clearfix"></div>
								<div class="row clearfix">
									<div class="col-sm-6">
										<h2 class="card-inside-title">
											<button type="button" class="btn bg-blue btn-circle waves-effect waves-circle waves-float pull-right btnAddComment">
												<i class="material-icons">add</i>
											</button>
											Case Comments
											<small>Comments and Notes</small>
										</h2>
										<div class="clearfix"></div>
										<hr>
										@if(isset($comments) && count($comments))
										@foreach($comments as $comment)
										<div class="card comment-card">
											<div class="header">
												<h2 class="col-teal">{!! $comment->user_name ?? '&nbsp;' !!} - <span class="col-grey">{{ $comment->user_type }}</span></h2>
												<ul class="header-dropdown m-r--5" style="top: 5px;">
													<li><span class="col-pink">{{ $comment->created_at->format('d M Y, h:i A')}}</span></li>
												</ul>
											</span>
										</div>
										<div class="body">
											{!! $comment->comment !!}
										</div>
									</div>
									@endforeach
									@else
									<div class="row clearfix">
										<div class="text-center"><i>No comments found</i></div>
									</div>
									@endif
								</div>

								<div class="col-sm-6">
									<h2 class="card-inside-title">
										Case Disposition
										<small>Dispostion History</small>
									</h2>
									<div class="clearfix"></div>
									<hr>
									@if(isset($dispositions) && count($dispositions))
									@foreach($dispositions as $disposition)
									<div class="card comment-card">
										<div class="header">
											<h2>Status: <span class="col-cyan">{{ $disposition->status }}</span></h2>
											@if($disposition->disposition_date != null)
											<ul class="header-dropdown m-r--5" style="top: 5px;">
												<li><span class="col-pink">{{ $disposition->disposition_date->format('d M Y')}}</span></li>
											</ul>
											@endif
										</span>
									</div>
									<div class="body">
										{!! $disposition->comment ?? '<i>No comments</i>' !!}
									</div>
								</div>
								@endforeach
								@else
								<div class="row clearfix">
									<div class="text-center"><i>No records found</i></div>
								</div>
								@endif
							</div>
						</div>
					</div>

					<div role="tabpanel" class="tab-pane fade in" id="care_plan_tab">
						<div class="row clearfix form-horizontal details-pane">
							<div class="col-sm-6">
								<h2 class="card-inside-title">
									<a class="btn bg-deep-orange btn-sm waves-effect pull-right btnEditCarePlan">Edit</a>
									<a class="btn bg-green btn-sm waves-effect pull-right btnEditCarePlan hide">Save</a>
									Case Details
									<small>Careplan, Medical Conditions, Medications</small>
								</h2>
								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
										<label for="crn_number">CRN Number</label>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
										<div class="form-group details">
											<div class="form-line" style="padding-top: 3px;">
												{{ $c->crn_number }}
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
										<label for="provider">Provider</label>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
										<div class="form-group details">
											<div class="form-line" style="padding-top: 3px;">
												{{ Helper::getProviderName($c->tenant_id) }}
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
										<label for="careplan_name">Care Plan Name</label>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
										<div class="form-group details">
											<div class="form-line">
												{{ $c->careplan_name }}
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix hide">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
										<label for="careplan_description">Description</label>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
										<div class="form-group details">
											<div class="form-line">
												{!! $c->careplan_description !!}
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
										<label for="medical_conditions">Medical Conditons</label>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
										<div class="form-group details">
											<div class="form-line">
												{!! $c->medical_conditions !!}

											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
										<label for="medications">Medications</label>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
										<div class="form-group details">
											<div class="form-line">
												{!! $c->medications ?? '-' !!}
											</div>
										</div>
									</div>
								</div>
								@if(isset($c->branch))
								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
										<label for="branch">Branch</label>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
										<div class="form-group details">
											{{ $c->branch->branch_name ?? '-' }}
										</div>
									</div>
								</div>
								@endif
								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
										<label for="designation">Service Required</label>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
										<div class="form-group details">
											{{ $c->service->service_name ?? '-' }}
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
										<label for="no_of_hours">No. of hours</label>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
										<div class="form-group details">
											<div class="form-line">
												{{ $c->no_of_hours ?? '-' }}
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
										<label for="case_charges">Case Charges</label>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
										<div class="form-group details">
											<div class="form-line">
												{{ $c->case_charges ?? '-' }}
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
										<label for="gender_preference">Gender Preference</label>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
										<div class="form-group details">
											{{ $c->gender_preference ?? '-' }}
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
										<label for="language_preference">Language Preference</label>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
										<div class="form-group details">
											{{ $c->language_preference ?? '-' }}
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
										<label for="status">Status</label>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
										<div class="form-group details">
											{{ $c->status ?? '-' }}
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<h2 class="card-inside-title">
									Case Disposition
									<small>Status, Comment</small>
								</h2>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
										<label for="careplan_status">Status</label>
									</div>
									<div class="col-lg-5 col-md-5 col-sm-5 col-xs-7">
										<div class="form-group">
											<select name="checkState" class="form-control show-tick" id="careplan_status" onchange="checkCaseLoss(this);">
												<option value="Pending" {{ $c->status=='Pending'?'selected':'' }}>PENDING</option>
												@if(\Helper::encryptor('decrypt',session('utype')) == 'Aggregator')
												<option value="FOLLOW UP WITH SP" {{ $c->status=='FOLLOW UP WITH SP'?'selected':'' }}>FOLLOW UP WITH SP</option>
												@endif
												<option value="SERVICE STARTED" {{ $c->status=='SERVICE STARTED'?'selected':'' }}>SERVICE STARTED</option>
												<option value="SERVICE COMPLETED" {{ $c->status=='SERVICE COMPLETED'?'selected':'' }}>SERVICE COMPLETED</option>
												<option value="DISCONTINUED" {{ $c->status=='DISCONTINUED'?'selected':'' }}>DISCONTINUED</option>
												<option id="caseloss" value="CASE LOSS" {{ $c->status=='CASE LOSS'?'selected':'' }}>CASE LOSS</option>
											</select>
										</div>
									</div>
								</div>
									@if($losscomment != null)
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label>Case Loss Reason</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-7 col-xs-7 form-control-label" style="text-align: left !important;">
											<label>{{ $losscomment }}</label>
										</div>
									</div>
									@endif

								<div class="row clearfix" id="casesloss" style="display:none;border-radius: 25px;background: rgba(162, 18, 18, 0.06); padding: 15px;">
									<div class="col-lg-12 form-control-label" style="text-align: center !important;">
										<label for="caseloss_reason">Case Loss Reason</label>
									</div>
									<div class="col-lg-12 text-left">
										<div class="form-group" style="margin-top: 1%">
											<input name="loss" type="radio" id="0" class="with-gap radio-col-deep-purple loss" value="Emergency Service" checked/>
											<label for="0">Emergency Service</label><br>

											<input name="loss" type="radio" id="1" class="with-gap radio-col-deep-purple loss" value="No Response" />
											<label for="1">No Response</label><br>

											<input name="loss" type="radio" id="2" class="with-gap radio-col-deep-purple loss" value="Prices are High" />
											<label for="2">Prices are High</label><br>

											<input name="loss" type="radio" id="3" class="with-gap radio-col-deep-purple loss" value="Household Helper" />
											<label for="3">Household Helper</label><br>

											<input name="loss" type="radio" id="4" class="with-gap radio-col-deep-purple loss" value="Patient is hospitaized" />
											<label for="4">Patient is hospitaized</label><br>

											<input name="loss" type="radio" id="5" class="with-gap radio-col-deep-purple loss" value="No Staff/Out of servic area" />
											<label for="5">No Staff/Out of servic area</label><br>

											<input name="loss" type="radio" id="6" class="with-gap radio-col-deep-purple loss" value="Service taken from others" />
											<label for="6">Service taken from others</label><br>

											<input name="loss" type="radio" id="7" class="with-gap radio-col-deep-purple loss" value="Patient Passed away" />
											<label for="7">Patient Passed away</label><br>

											<input name="loss" type="radio" id="8" class="with-gap radio-col-deep-purple loss" value="General Inquiry about apnacare services and charges" />
											<label for="8">General Inquiry about our services and charges</label><br>

											<input name="loss" type="radio" id="9" class="with-gap radio-col-deep-purple loss" value="Not willing to share any details" />
											<label for="9">Not willing to share any details</label><br>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label">
										<label for="careplan_date">Date</label>
									</div>
									<div class="col-lg-5 col-md-5 col-sm-5 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<input type="text" class="form-control date" id="careplan_disposition_date" placeholder="Date">
											</div>
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label">
										<label for="careplan_comment">Comment</label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">
												<textarea class="form-control" id="careplan_comment" placeholder="Comments / Reason / Notes (if any)"></textarea>
											</div>
										</div>
									</div>
								</div><br>
								<div class="row clearfix">
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-control-label">
									</div>
									<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<div class="form-group">
											<a class="btn btn-warning btn-lg waves-effect saveCarePlanDisposition" data-careplan-id="{{ Helper::encryptor('encrypt',$c->id) }}" data-provider-id="{{ Helper::encryptor('encrypt',$c->tenant_id) }}">Save Disposition</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>

					<div role="tabpanel" class="tab-pane fade in" id="schedules_tab">
						<div class="row clearfix">
							<div class="col-sm-12 text-center">
								<a class="btn btn-info btnAddSchedule"><span class="plus"></span> Add Visit</a>
							</div>
						</div>
						<div class="row clearfix">
							@if(count($c->schedules))
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>#</th>
										<th>Caregiver Staff</th>
										<th width="25%" class="text-center">Period</th>
										<th>Notes</th>
										<th>Status</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									@foreach($c->schedules as $i => $s)
									<tr>
										<td>{{ $i +1 }}</td>
										<td>
											<img src="/img/user.png" class="pull-right img-circle" width="40" height="40"/>
											{{ isset($s->caregiver->first_name)?$s->caregiver->first_name:'' }}<br>
											<small>{{ isset($s->caregiver->mobile_number)?$s->caregiver->mobile_number:'' }}</small>
										</td>
										<td>
											<div class="col-sm-5 text-center" style="margin-bottom:0">
												{{ isset($s->start_date)?$s->start_date->format('d-m-Y'):'' }}
											</div>
											<div class="col-sm-1 text-center" style="margin-bottom:0"><b>to</b></div>
											<div class="col-sm-5 text-center" style="margin-bottom:0">
												{{ isset($s->end_date)?$s->end_date->format('d-m-Y'):'' }}
											</div>
										</td>
										<td>{!! isset($s->notes)?$s->notes:'' !!}</td>
										<td>{{ isset($s->status)?$s->status:'' }}</td>
										<td>
											<a class="btn btn-success btnViewSchedule" data-careplan-id="{{ isset($s->careplan_id)?Helper::encryptor('encrypt',$s->careplan_id):'' }}" data-patient-id="{{ isset($s->patient_id)?Helper::encryptor('encrypt',$s->patient_id):'' }}" data-visit-id="{{ isset($s->id)?Helper::encryptor('encrypt',$s->id):'' }}" data-visit-status="{{ isset($s->status)?$s->status:'' }}" data-start-datetime="{{ isset($s->start_date)?$s->start_date->format('d-m-Y'):'' }}" data-end-datetime="{{ isset($s->end_date)?$s->end_date->format('d-m-Y'):'' }}" data-notes="{{ isset($s->notes)?$s->notes:'' }}" data-caregiver-id="{{ isset($s->caregiver_id)?Helper::encryptor('encrypt',$s->caregiver_id):'' }}" data-caregiver="{{ isset($s->caregiver->full_name)?$s->caregiver->full_name:'' }}" data-caregiver-mobile="{{ isset($s->caregiver)?$s->caregiver->mobile_number:'' }}"><i class="material-icons">search</i></a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
							@endif
						</div>
						<div id="visit_list_pane" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						</div>
						<div class="clearfix"></div>
					</div>

					<div role="tabpanel" class="tab-pane fade in" id="assessment_tab">
						<div class="row clearfix">
							<div class="col-sm-12 text-center">
								<a class="btn btn-info btnAddAssessment" data-toggle="modal" data-target="#assessmentModal"><span class="plus"></span> Add Form</a>
							</div>
						</div>
						<div class="row clearfix">
							<table class="table table-bordered table-condensed">
								<thead>
									<tr>
										<th>Date</th>
										<th>Type</th>
										<th>Creator</th>
										<th>Report</th>
									</tr>
								</thead>
								<tbody>
									@foreach($c->assessment()->orderBy('created_at','Desc')->get() as $a)
									@if($a->form_type == "Assessment")
									{? $formData = json_decode($a->form_data,true); ?}
									<tr>
										<td>{{ $formData['assessment_date'] }}</td>
										<td>{{ $a->form_type }}</td>
										<td>{{ $formData['assessor_name'] }}</td>
										<td><a class="btn btn-info btnViewAssessment assessmentId_{{$a->id}}" data-toggle="modal" data-target="#viewassessmentModal" data-form-data="{{$a->form_data}}" onclick="viewAssessment({{$a->id}});"><span class="view"></span> View</a></td>
									</tr>
									@elseif($a->form_type == "LabTest")
										<tr>
											<td>{{ $a->created_at }}</td>
											<td>{{ $a->form_type }}</td>
											<td>{{ $a->user->full_name }}</td>
											<td><a class="btn btn-info btnViewLabRequirement requirementId_{{$a->id}}" data-toggle="modal" data-target="#viewrequirementModal" data-form-data="{{ $a->form_data }}" onclick="viewRequirement({{$a->id}});"><span class="view"></span> View</a></td>
										</tr>
									@endif
									@endforeach
								</tbody>
							</table>
						</div>
					</div>

					<div role="tabpanel" class="tab-pane fade in" id="tasks_tab">
						<div class="row clearfix">
							<table class="table table-bordered table-condensed">
								<thead>
									<tr>
										<th>#</th>
										<th>Date</th>
										<th>Caregiver</th>
										<th>View</th>
									</tr>
								</thead>
								<tbody>
									@forelse ($c->worklog as $index => $w)
									<tr>
										<td>{{ ($index + 1) }}</td>
										<td>{{ $w->created_at->format('d-m-Y') }}</td>
										<td>{{ isset($w->caregiver)?$w->caregiver->first_name.' '.$w->caregiver->last_name:'-' }}</td>
										<td><a class="btn btn-info btnViewWorklog worklogId_{{ $w->id }}" data-toggle="modal" data-target="#viewWorklogModal" data-vitals="{{ $w->vitals }}" data-routines="{{ $w->routines }}" onclick="viewWorklog(this);"><span class="view"></span> View</a></td>
									</tr>
									@empty
									<tr>
										<td colspan="6" class="text-center">No task records found.</td>
									</tr>
									@endforelse
								</tbody>
							</table>
						</div>
					</div>

					<div role="tabpanel" class="tab-pane fade in" id="feedback_tab">
						<div class="row clearfix col-lg-4">
							<div class="col-md-12">
								<b>Date Time</b>
								<div class="input-group">
									<span class="input-group-addon">
										<i class="material-icons">date_range</i>
									</span>
									<div class="form-line">
										<input type="text" class="form-control feedback_time" name="feedback_time" id="feedback_time" placeholder="Ex: 30/07/2016 23:59">
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<b>Feedback Comment</b>
								<div class="input-group">
									<span class="input-group-addon">
										<i class="material-icons">comment</i>
									</span>
									<div class="form-line">
										<textarea class="form-control feedback_comment" name="feedback_comment" id="feedback_comment" placeholder="Ex: Comment"></textarea>
									</div>
								</div>
							</div>
							<div class="text-center"><button type="button" class="btn btn-success btnsaveFeedbackComment">Save</button></div>
						</div>
						<div class="row clearfix col-lg-8">
							<table class="table table-bordered table-striped table-condensed" style="border: 2px solid black;">
								<thead>
									<tr>
										<th>#</th>
										<th>Date</th>
										<th>User Name</th>
										<th>Comment</th>
									</tr>
								</thead>
								<tbody>
									@if(count($feedbackcomments))
									@foreach($feedbackcomments as $i => $fc)
									<tr>
										<td class="text-center">{{$i+1}}</td>
										<td class="text-center">{{ $fc->time }}</td>
										<td class="text-center">{{ $fc->user_name }}</td>
										<td class="text-center">{{ $fc->comment }}</td>
									</tr>
									@endforeach
									@endif
								</tbody>
							</table>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<input type="hidden" name="careplan_id" id="careplan_id" value="{{ Helper::encryptor('encrypt',$c->id) }}">
<input type="hidden" name="patient_id" id="patient_id" value="{{ Helper::encryptor('encrypt',$c->patient->id) }}">
</form>
</div>

<!-- Visit List Template -->
<div id="visitListTemplate" class="card care-plan-card visitListCard hide">
	<div class="header bg-cyan">
		<h2>
			Visit Schedule - #1
		</h2>
		<ul class="header-dropdown m-r--5" style="top: 5px;">
			<li><a href="javascript:void(0);" class="removeVisitList btn btn-danger waves-effect waves-block" data-visit-id="1"><i class="material-icons">delete</i></a></li>
			<li>&nbsp;</li>
			<li><a href="javascript:void(0);" class="slideVisitList btn btn-warning waves-effect waves-block" data-visit-id="1"><i class="material-icons">expand_more</i></a></li>
		</ul>
	</div>
	<div class="body form-horizontal">
		<div class="col-sm-6">
			<h2 class="card-inside-title">
				Visit Details
				<small>Caregiver, Visit Type, Start Date, End Date</small>
			</h2>
			<div class="row clearfix">
				<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
					<label for="no_of_hours">Start Date</label>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					<div class="form-group">
						<div class="form-line">
							<input type="text" data-visit-id="1" id="start_date" class="form-control date" placeholder="Ex. 01-12-2016">
						</div>
					</div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
					<label for="no_of_hours">End Date</label>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					<div class="form-group">
						<div class="form-line">
							<input type="text" data-visit-id="1" id="end_date" class="form-control date" placeholder="Ex. 03-12-2016">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="row clearfix">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"></div>
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
					<a class="btn btn-warning btn-block waves-effect btnChooseStaff" onclick="javascript: return clearSearchForm();" data-visit-id="0">Choose Staff</a>
				</div>
			</div><br>
			<div class="row clearfix">
				<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
					<label for="no_of_hours">Staff / Caregiver</label>
				</div>
				<div class="col-lg-9 col-md-9 col-sm-6 col-xs-6">
					<div class="form-group">
						<div style="margin-top: 8px;" id="selectedStaffName_0" data-visit-id="0">-</div>
						<input type="hidden" id="caregiver_id" value=""/>
					</div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
					<label for="no_of_hours">Notes</label>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<div class="form-group">
						<div class="form-line">
							<textarea data-visit-id="1" id="notes" class="form-control" placeholder="Notes (if any)"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-12 clearfix text-center">
		<a class="btn btn-success btn-lg waves-effect btnSaveSchedule" data-crn-no="{{$c->crn_number}}" data-visit-id="0">Save Schedule</a>
	</div>
	<div class="clearfix"></div>
</div>
</div>

<!-- Staff Search Modal -->
<div class="modal fade" id="staffSearchModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" style="z-index: 1052 !important">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="largeModalLabel">Employee Search Form</h4>
			</div>
			<div class="modal-body">
				<div class="row clearfix">
					<form id="staffSearchForm" class="col-sm-12">
						<div class="col-sm-3">
							<div class="form-group">
								<label for="specialization">Specialization</label>
								<div class="form-line">
									<select class="form-control show-tick" id="filter_specialization">
										<option value="0">-- Any --</option>
										@if(isset($specializations) && count($specializations))
										@foreach($specializations as $s)
										<option value="{{ $s->id }}">{{ $s->specialization_name }}</option>
										@endforeach
										@endif
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label for="experience">Experience</label>
								<div class="form-line">
									<select class="form-control show-tick" id="filter_experience">
										<option value="0">-- Any --</option>
										<option value="0_5">0-5 yr(s)</option>
										<option value="5_10">5-10 yr(s)</option>
										<option value="10_15">10-15 yr(s)</option>
										<option value="15_35">Above 15 yrs</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label for="city">City</label>
								<div class="form-line">
									<input class="form-control" id="filter_city" placeholder="City">
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label for="language">Language</label>
								<div class="form-line">
									<select class="form-control show-tick" id="filter_language" multiple>
										<option value="0">-- Any --</option>
										@foreach($languages as $l)
										<option value="{{ $l->id }}">{{ $l->language_name }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label for="skill">Skill</label>
								<div class="form-line">
									<select class="form-control show-tick" id="filter_skill" multiple>
										<option value="0">-- Any --</option>
										@foreach($skill as $sk)
										<option value="{{ $sk->id }}">{{ $sk->skill_name }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<br>
							<a class="btn btn-primary waves-effect btnFilterStaff">Search</a>&nbsp;&nbsp;
							<button type="reset" class="btn btn-danger waves-effect btnReset">Reset</button>
						</div>
					</form>
					<div class="col-sm-12">
						<div class="page-loader-wrapper">
							<div class="loader">
								<div class="md-preloader pl-size-md">
									<svg viewbox="0 0 75 75">
										<circle cx="37.5" cy="37.5" r="33.5" class="pl-blue" stroke-width="4" />
									</svg>
								</div>
								<p>Please wait...</p>
							</div>
						</div>
						<table class="table table-bordered table-striped caseSearchResults">
							<thead>
								<tr>
									<th>Name</th>
									<th>Contact</th>
									<th>Designation</th>
									<th>Experience</th>
									<th>Total Visits</th>
									<th>Rating</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-center" colspan="7">Click 'Search' to get results</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success waves-effect btnSelectStaff" data-visit-id="0">Continue</button>
				<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>

<!-- View Schedule Modal -->
<div class="modal fade" id="viewScheduleModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<form id="visitScheduleEditForm" method="POST">
				<div class="modal-header bg-blue">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="largeModalLabel">View Schedule</h4>
				</div>
				<div class="modal-body form-horizontal">
					<div class="col-sm-6">
						<div class="row clearfix">
							<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
								<label for="no_of_hours">Start Date</label>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
								<div class="form-group">
									<div class="form-line">
										<input type="text" data-visit-id="1" id="start_date" name="start_date" class="form-control getdate" placeholder="Start Date">
									</div>
								</div>
							</div>
						</div><br>
						<div class="row clearfix">
							<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
								<label for="no_of_hours">End Date</label>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
								<div class="form-group">
									<div class="form-line">
										<input type="text" data-visit-id="1" id="end_date" class="form-control getdate" placeholder="End Date">
									</div>
								</div>
							</div>
						</div><br>
						<div class="row clearfix">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"></div>
							<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
								<a class="btn btn-warning btn-block waves-effect btnChooseStaff" onclick="javascript: return clearSearchForm();" data-visit-id="0" data-mode="view">Change Staff</a>
							</div>
						</div><br>
						<div class="row clearfix">
							<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
								<label for="no_of_hours">Staff / Caregiver</label>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
								<div class="form-group">
									<div style="margin-top: 8px;" id="selectedStaffName_0" data-visit-id="0">-</div>
									<input type="hidden" id="caregiver_id" value=""/>
								</div>
							</div>
						</div><br>
					</div>
					<div class="clearfix">
						<div class="col-sm-6">
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="no_of_hours">Notes</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<textarea data-visit-id="1" id="notes" class="form-control" placeholder="Notes (if any)"></textarea>
										</div>
									</div>
								</div>
							</div><br>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="status">Status</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<select data-visit-id="1" id="status" class="form-control show-tick">
												<option value="PENDING">PENDING</option>
												<option value="SERVICE STARTED">SERVICE STARTED</option>
												<option value="SERVICE COMPLETED">SERVICE COMPLETED</option>
												<option value="SERVICE CANCELLED">SERVICE CANCELLED</option>
												<option value="SERVICE POSTPONED">SERVICE POSTPONED</option>
											</select>
										</div>
									</div>
								</div>
							</div><br>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="comment">Comment</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<textarea class="form-control" id="comment" placeholder="Comment"></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-success waves-effect pull-right btnUpdateVisitSchedule" data-crn-number="0" data-visit-id="0" data-caregiver-id="0" data-patient-id="0">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Comment Modal -->
<div class="modal fade" id="commentModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<form action="{{ route('case.save-comment') }}" method="POST">
			<div class="modal-content">
				<div class="modal-header bg-blue">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="largeModalLabel">Add Comment</h4>
				</div>
				<div class="modal-body">
					<div class="row clearfix">
						<div class="col-sm-12">
							<div class="form-group">
								<div class="form-line">
									<textarea class="form-control" id="comment" name="comment" rows="3" placeholder="Enter Comment here" required></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					@if(Helper::encryptor('decrypt',session('utype')) == 'Aggregator')
					<input type="hidden" name="provider_id" value="{{ $pid?Helper::encryptor('encrypt',$pid):0  }}" />
					@endif
					<input type="hidden" name="careplan_id" value="{{ Helper::encryptor('encrypt',$c->id) }}" />
					{{ csrf_field() }}
					<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-success waves-effect pull-right">Save</button>
				</div>
			</div>
		</form>
	</div>
</div>

<!-- Edit Patient Modal -->
<div class="modal fade" id="editPatientModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<form id="editPatientForm" method="POST" action="{{ route('patient.save-patient') }}">
			<div class="modal-content">
				<div class="modal-header bg-blue">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="largeModalLabel">Edit Patient Info <span class="crn_no"></span></h4>
				</div>
				<div class="modal-body">
					<div class="row clearfix">
						<div class="col-sm-6 form-horizontal">
							<h4 class="card-inside-title col-teal">
								Basic Details
							</h4>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="first_name">First Name</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="first_name" name="first_name" class="form-control searchCol" placeholder="Ex. Ashok" value="{{ isset($c)?$c->patient->first_name:'' }}" required>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="last_name">Last Name</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="last_name" name="last_name" class="form-control searchCol" placeholder="Ex. Kumar" value="{{ isset($c)?$c->patient->last_name:'' }}">
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="mobile_number">Mobile Number</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="mobile_number" name="mobile_number" class="form-control searchCol" placeholder="Ex. 9845098450" value="{{ isset($c)?$c->patient->mobile_number:'' }}">
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="patient_age">Patient Age</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="patient_age" name="patient_age" class="form-control" placeholder="Ex. 65" value="{{ isset($c)?$c->patient->patient_age:'' }}">
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="patient_weight">Patient Weight</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="patient_weight" name="patient_weight" class="form-control" placeholder="Ex. 65" value="{{ isset($c)?$c->patient->patient_weight:'' }}">
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="gender">Gender</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<select class="form-control show-tick" id="gender" name="gender" required>
											<option value="">-- Please select--</option>
											<option value="Male" @if(isset($c) && $c->patient->gender == 'Male'){{'selected'}}@endif>Male</option>
											<option value="Female" @if(isset($c) && $c->patient->gender == 'Female'){{'selected'}}@endif>Female</option>
											<option value="Other" @if(isset($c) && $c->patient->gender == 'Other'){{'selected'}}@endif>Other</option>
										</select>
									</div>
								</div>
							</div>
							<h4 class="card-inside-title col-teal">
								Patient Address
							</h4>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="street_address">Street Address</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="street_address" name="street_address" class="form-control" placeholder="Ex. #20, 3rd Cross, 5th Main" value="{{ isset($c)?$c->patient->street_address:'' }}"/>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="area">Area</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="area" name="area" class="form-control" placeholder="Ex. Sanjay Nagar" value="{{ isset($c)?$c->patient->area:'' }}"/>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="city">City</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="city" name="city" class="form-control" placeholder="Ex. Bangalore" value="{{ isset($c)?$c->patient->city:'' }}"/>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="zipcode">Zip Code</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="zipcode" name="zipcode" class="form-control" placeholder="Ex. 560001" value="{{ isset($c)?$c->patient->zipcode:'' }}"/>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="state">State</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="state" name="state" class="form-control" placeholder="Ex. Karnataka" value="{{ isset($c)?$c->patient->state:'' }}"/>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="country">Country</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="country" name="country" class="form-control" placeholder="Ex. India" value="{{ isset($c)?$c->patient->country:'' }}"/>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="location_coordinates">Location Co-ordinates</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="location_coordinates" name="location_coordinates" class="form-control" placeholder="Eg. 12.789666, 32.6589656" value="{{ isset($c)?$c->patient->location_coordinates:'' }}"/>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 form-horizontal">
							<h4 class="card-inside-title col-teal">
								Enquirer Details
							</h4>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="enquirer_name">Enquirer Name</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="enquirer_name" name="enquirer_name" class="form-control" placeholder="Ex. Ajay Rao" value="{{ isset($c)?$c->patient->enquirer_name:'' }}">
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="enquirer_phone">Enquirer Phone</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="enquirer_phone" name="enquirer_phone" class="form-control" placeholder="Ex. 9999999999" value="{{ isset($c)?$c->patient->enquirer_phone:'' }}">
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="enquirer_email">Enquirer Email</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="enquirer_email" name="enquirer_email" class="form-control" placeholder="Ex. ajay@gmail.com" value="{{ isset($c)?$c->patient->enquirer_email:'' }}">
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="relationship_with_patient">Relationship with Patient</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<select id="relationship_with_patient" name="relationship_with_patient" class="form-control show-tick" placeholder="Relationship with Patient">
											<option value="">-- Please select --</option>
											<option value="Father" @if(isset($c) && $c->patient->relationship_with_patient == 'Father'){{'selected'}}@endif>Father</option>
											<option value="Mother" @if(isset($c) && $c->patient->relationship_with_patient == 'Mother'){{'selected'}}@endif>Mother</option>
											<option value="Grand Father" @if(isset($c) && $c->patient->relationship_with_patient == 'Grand Father'){{'selected'}}@endif>Grand Father</option>
											<option value="Grand Mother" @if(isset($c) && $c->patient->relationship_with_patient == 'Grand Mother'){{'selected'}}@endif>Grand Mother</option>
											<option value="Sister" @if(isset($c) && $c->patient->relationship_with_patient == 'Sister'){{'selected'}}@endif>Sister</option>
											<option value="Brother" @if(isset($c) && $c->patient->relationship_with_patient == 'BROTHER'){{'selected'}}@endif>Brother</option>
											<option value="Self" @if(isset($c) && $c->patient->relationship_with_patient == 'Self'){{'selected'}}@endif>Self</option>
											<option value="Friend" @if(isset($c) && $c->patient->relationship_with_patient == 'FRIEND'){{'selected'}}@endif>Friend</option>
											<option value="Uncle" @if(isset($c) && $c->patient->relationship_with_patient == 'Uncle'){{'selected'}}@endif>Uncle</option>
											<option value="Aunt" @if(isset($c) && $c->patient->relationship_with_patient == 'Aunt'){{'selected'}}@endif>Aunt</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						{{ csrf_field() }}
						<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-success waves-effect pull-right btnSavePatientDetails">Save</button>
						<input type="hidden" name="patient_id" value="{{$c->patient->id}}">
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<!-- Edit Case Modal -->
<div class="modal fade" id="editCaseModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<form id="editCaseForm" method="POST">
			<div class="modal-content">
				<div class="modal-header bg-blue">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="largeModalLabel">Edit Case Details <span class="crn_no"></span></h4>
				</div>
				<div class="modal-body">
					<div class="row clearfix form-horizontal">
						<div class="col-sm-6">
							<h4 class="card-inside-title col-teal">
								Care Plan Details
							</h4>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="careplan_name">Care Plan Name</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="careplan_name" name="careplan_name" class="form-control" placeholder="Care Plan Name" value="{{ $c->careplan_name }}">
										</div>
									</div>
								</div>
							</div><br>
							<div class="row clearfix hide">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="careplan_description">Description</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<textarea id="careplan_description" name="careplan_description" class="form-control" placeholder="Description">{{ $c->careplan_description }}</textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="medical_conditions">Medical Conditons</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input class="medical_conditions" id="medical_conditions" name="medical_conditions" data-provide="typeahead"  type="text" value="{{ $c->medical_conditions }}"/>
										</div>
									</div>
								</div>
							</div><br>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="medications">Medications</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input class="medications" name="medications" id="medications" data-provide="typeahead" type="text" value="{{ $c->medications }}"/>
										</div>
									</div>
								</div>
							</div><br>
							<h4 class="card-inside-title col-teal">
								Referral Details
							</h4>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="source">Source</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<select class="form-control show-tick" id="source" name="source">
											<option value="">--Select--</option>
											<option value="Google Search">Google Search</option>
											<option value="Website">Website</option>
											<option value="Referral from Service Partner">Referral from Service Partner</option>
											<option value="Online Classifieds">Online Classifieds</option>
											<option value="Email Campaign">Email Campaign</option>
											<option value="Facebook">Facebook</option>
											<option value="Twitter">Twitter</option>
											<option value="Google Plus">Google Plus</option>
											<option value="Events">Events</option>
											<option value="Exhibitions">Exhibitions</option>
											<option value="Flyer">Flyer</option>
											<option value="Print Classifieds">Print Classifieds</option>
											<option value="Radio">Radio</option>
											<option value="Television">Television</option>
											<option value="Print Ads">Print Ads</option>
											<option value="News/Media">News/Media</option>
											<option value="Other">Other</option>
										</select>
									</div>
								</div>
							</div><br>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="referrer_name">Referrer Name</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="referrer_name" name="referrer_name" class="form-control referrer-name" placeholder="Referrer Name" value="{{ $c->referrer_name }}">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<h4 class="card-inside-title col-teal">
								Service Details
								@if(Helper::encryptor('decrypt',session('utype')) == 'Aggregator')
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 pull-right">
									<a class="btn btn-warning btn-block waves-effect btnChooseProvider" data-visit-id="1">Choose Provider</a>
								</div>
								@endif
							</h4>
							{{-- <div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"></div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
									<a class="btn btn-warning btn-block waves-effect btnChooseProvider" data-visit-id="1">Choose Provider</a>
								</div>
							</div> --}}
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="provider">Selected Provider</label>
								</div>
								<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
									<div class="form-group">
										<input type="hidden" id="provider_id" name="tenant_id" value="{{ $c->tenant_id }}" />
										<div style="margin-top: 8px;" id="selectedProviderName" data-visit-id="1">{{ Helper::getProviderName($c->tenant_id) }}</div>
									</div>
								</div>
							</div><br>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="designation">Service Required</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<select class="form-control show-tick" id="service_id" name="service_id">
											<option value="0">-- Please select --</option>
											@if(isset($services) && count($services))
											@foreach($services as $s)
											<option value="{{ $s->id }}" @if($s->id == $c->service_id){{'selected'}}@endif>{{ $s->service_name }}</option>
											@endforeach
											@endif
										</select>
									</div>
								</div>
							</div><br>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="no_of_hours">No. of hours</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="no_of_hours" name="no_of_hours" class="form-control" placeholder="No. of hours" value="{{ $c->no_of_hours }}">
										</div>
									</div>
								</div>
							</div><br>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="case_charges">Case Charges</label>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
									<div class="form-group">
										<div class="form-line">
											<input type="text" class="form-control" id="case_charges" name="case_charges" placeholder="Ex. 650" value="{{ $c->case_charges }}">
										</div>
									</div>
								</div>
							</div><br>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="gender_preference">Gender Preference</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group" style="margin-top: 1%">
										<input name="gender_preference" type="radio" id="gender_preference_any" class="with-gap radio-col-deep-purple" value="Any" @if($c->gender_preference == 'Any'){{'checked'}}@endif />
										<label for="gender_preference_any">Any</label>
										<input name="gender_preference" type="radio" id="gender_preference_male" class="with-gap radio-col-deep-purple" value="Male" @if($c->gender_preference == 'Male'){{'checked'}}@endif />
										<label for="gender_preference_male">Male</label>
										<input name="gender_preference" type="radio" id="gender_preference_female" class="with-gap radio-col-deep-purple" value="Female" @if($c->gender_preference == 'Female'){{'checked'}}@endif/>
										<label for="gender_preference_female">Female</label>
									</div>
								</div>
							</div><br>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="language_preference">Language Preference</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group" style="margin-top: 2.5%">
										<div class="demo-checkbox language-preference">
											@if(isset($languages) && count($languages))
											@foreach($languages as $l)
											<input type="checkbox" id="languages_{{ strtolower($l->language_name) }}" name="language_preference[]" class="filled-in chk-col-light-blue" value="{{ $l->language_name }}"  @if(in_array($l->language_name,explode(",",$c->language_preference))){{'checked'}}@endif />
											<label for="languages_{{ strtolower($l->language_name) }}">{{ $l->language_name }}</label>
											@endforeach
											@endif
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="modal-footer">
					@if(Helper::encryptor('decrypt',session('utype')) == 'Aggregator')
					<input type="hidden" name="provider_id" value="{{ $pid?Helper::encryptor('encrypt',$pid):0  }}" />
					@endif
					<input type="hidden" name="careplan_id" value="{{ Helper::encryptor('encrypt',$c->id) }}" />
					<input type="hidden" name="crn_number" value="{{ $c->crn_number }}" />
					<input type="hidden" name="branch_id" value="{{ Helper::encryptor('encrypt',$c->branch_id) }}" />
					<input type="hidden" name="patient_id" value="{{ Helper::encryptor('encrypt',$c->patient_id) }}" />
					{{ csrf_field() }}
					<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-success waves-effect pull-right btnSaveCaseDetails">Save</button>
				</div>
			</div>
		</form>
	</div>
</div>

<!-- Add Form Modal -->
<div class="modal fade" id="assessmentModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" style="width: 80% !important;" role="document">
		<form action="{{ route('case.save-form') }}" method="POST">
			<div class="modal-content">
				<div class="modal-header bg-blue">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="largeModalLabel">Add Form</h4>
				</div>
				<div class="modal-body">
					<div class="col-lg-4">
						<select class="form-control show-tick" data-live-search="true" id="schedule_details_type" name="schedule_details_type">
							<option selected disabled value="">-- Please select Form--</option>
							<optgroup label="Assessment">
							<option data-category="Assessment" data-specialization="physiotherapy">Physiotherapy</option>
							<option data-category="Assessment" data-specialization="nursing">Nursing</option>
							</optgroup>
							<optgroup label="Lab Test">
							<option data-category="LabTest" data-specialization="labtest">Lab Test</option>
							</optgroup>
						</select>
					</div>
					<div class="col-lg-8 labhide">
						<select class="form-control show-tick" data-live-search="true" id="schedule_details" name="schedule_details">
							<option value="">-- Please select a Person--</option>
							@foreach($persons as $i => $p)
							<option data-user-id="{{$p->id}}">{{$p->first_name}} {{$p->last_name}}</option>
							@endforeach
						</select>
					</div>
					<div class="forms" style="margin-top: 30px;">
						{{-- include various forms here--}}
					</div>
				</div>
				<div class="modal-footer labhide" style="border-top: none !important;">
					<input type="hidden" name="careplan_id" value="{{Helper::encryptor('encrypt',$c->id)}}"/>
					<input type="hidden" name="user_id" id="listed_user_id" value="0" />
					<input type="hidden" name="form_type" id="listed_form_type" value="0" />
					{{ csrf_field() }}
					<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-success waves-effect pull-right" onclick="javascript: return prepareAssessmentForm();">Save</button>
				</div>
			</div>
		</form>

	</div>
</div>

<!-- View Assessment Modal -->
<div class="modal fade" id="viewassessmentModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content" >
			<div id="assessment-report">
				<div class="modal-header bg-blue hidden-print">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="largeModalLabel">REPORT</h4>
				</div>
				<div class="modal-body" id="view-assessment-data">
					<table border="1" class="table table-bordered table-condensed" id="viewAssessmentTable">
					</table>
				</div>
				<div class="modal-footer" style="border-top: none !important;">
					<input type="hidden" name="careplan_id" value="{{Helper::encryptor('encrypt',$c->id)}}"/>
					<input type="hidden" name="crn_number" value="{{$c->crn_number}}" />
					<input type="hidden" name="caregiver_id" id="listed_caregiver_id" value="0" />
					<input type="hidden" name="patient_id" value="{{$c->patient_id}}" />
					<button type="button" class="btn btn-danger waves-effect pull-left noPrint " data-dismiss="modal">Close</button>
					<input type="button" class="btn btn-info noPrint " value="Print" onclick="window.print();"></input>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- View Lab Requirement Modal -->
<div class="modal fade" id="viewrequirementModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content" >
			<div id="lab-report">
				<div class="modal-header bg-blue hidden-print">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="largeModalLabel">CASE LAB TEST REQUIREMENT</h4>
				</div>
				<div class="modal-body" id="view-labrequirement-data">
					<table border="1" class="table table-bordered table-condensed" id="viewRequirementTable">
					</table>
				</div>
				<div class="modal-footer" style="border-top: none !important;">
					<input type="hidden" name="careplan_id" value="{{Helper::encryptor('encrypt',$c->id)}}"/>
					<input type="hidden" name="crn_number" value="{{$c->crn_number}}" />
					<input type="hidden" name="patient_id" value="{{$c->patient_id}}" />
					<button type="button" class="btn btn-danger waves-effect pull-left noPrint " data-dismiss="modal">Close</button>
					<input type="button" class="btn btn-info noPrint " value="Print" onclick="window.print();"></input>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- View Worklog Modal -->
<div class="modal fade" id="viewWorklogModal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content" >
			<div id="worklog-report">
				<div class="modal-header bg-blue hidden-print">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="largeModalLabel">Worklog</h4>
				</div>
				<div class="modal-body" id="view-assessment-data">
					<table border="1" class="table table-bordered table-condensed" id="viewWorklogTable">
					</table>
				</div>
				<div class="modal-footer" style="border-top: none !important;">
					<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Provider Search Modal -->
<div class="modal fade" id="providerSearchModal" style="z-index: 1052" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="largeModalLabel">Provider Search Form</h4>
			</div>
			<div class="modal-body">
				<div class="row clearfix">
					<form id="providerSearchForm" class="col-sm-12">
						<div class="col-sm-4">
							<div class="form-group">
								<label for="filter_service">Service Required</label>
								<div class="form-line">
									<select class="form-control show-tick" data-live-search="true" id="filter_service">
										<option value="0">-- Please select --</option>
										@if(isset($services) && count($services))
										@foreach($services as $s)
										<option value="{{ $s->id }}">{{ $s->service_name }}</option>
										@endforeach
										@endif
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label for="filter_gender">Gender Preference</label>
								<div class="form-line">
									<select class="form-control show-tick" id="filter_gender">
										<option value="0">-- Any --</option>
										<option value="Male">Male</option>
										<option value="Female">Female</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label for="filter_city">City</label>
								<div class="form-line">
									<input class="form-control" id="filter_city" placeholder="City">
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<br>
							<a class="btn btn-primary waves-effect btnFilterProvider">Search</a>&nbsp;&nbsp;
							<button type="reset" class="btn btn-danger waves-effect btnReset">Reset</button>
						</div>
					</form>
					<div class="col-sm-12" style="max-height: 400px;overflow:auto;">
						<div class="page-loader-wrapper">
							<div class="loader">
								<div class="md-preloader pl-size-md">
									<svg viewbox="0 0 75 75">
										<circle cx="37.5" cy="37.5" r="33.5" class="pl-blue" stroke-width="4" />
									</svg>
								</div>
								<p>Please wait...</p>
							</div>
						</div>
						<table class="table table-bordered table-striped providerSearchResults">
							<thead>
								<tr>
									<th>Provider</th>
									<th>Location</th>
									<th>Contact</th>
									<th width="20%">Availability</th>
									<th width="10%" class="text-center">Caregivers</th>
									<th width="10%" class="text-center">Rates</th>
									<th width="5%">&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-center" colspan="7">Click 'Search' to get results</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success waves-effect btnSelectProvider" data-visit-id="0">Continue</button>
				<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>
@endsection

@section('plugin.scripts')
<!-- Select Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<!-- Input Mask Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>
<!-- Moment Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<!-- Bootstrap Tags Input Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('bootstrap-3-typeahead/bootstrap3-typeahead.js') }}"></script>

<!-- Bootstrap Notify Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-notify/bootstrap-notify.js') }}"></script>
<script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
<!-- noUiSlider-->
<script src="{{ asset('themes/default/plugins/nouislider/nouislider.js') }}"></script>
<script src="{{ asset('js/autocomplete.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCvPCONT3odYk2HGHeP3hGhtwXZqx5rekQ&libraries=places"></script>
@endsection

@section('page.scripts')
	<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
	<script>

	$(document).ready(function() {
	    if ($("select[name=checkState] option:selected").val() == 'CASE LOSS') {
	       document.getElementById("casesloss").style.display = "block";
	    }
	    if($('.saveCarePlanDisposition').data('provider-id') == 'WWlXZ1FCUFBwWVRTV3RpcEFnVDd5UT09' && $('#careplan_status option:selected').val() != 'CASE LOSS'){
	    	alert("Please Choose a Provider");
	    	return false;
	    }
		$('.content').on('change','#price_type',function(){
				var sum = 0;
				var subtext = $('#price_type option:selected').text();
				var id = $('#price_type option:selected').val();
				$('.forms #test_id').find("option[value='"+id+"']").data("subtext", subtext);
				$(".forms #test_id").selectpicker('refresh');
				$('.forms #test_id option:selected').each(function() {
				sum += Number($(this).data('subtext'));
				});
				$(".forms #sum").html(sum);
	    });
	    $('.content').on('change','#test_id',function(){
	    	var sum = 0;
	    	$('.forms #test_id option:selected').each(function() {
	    		sum += Number($(this).data('subtext'));
	    	});
	    	$(".forms #sum").html(sum);
	    });
	});

	$('.content').on('click','.btnsaveLabTest',function(){
		var testdetails = [];
		testdetails.push({"assessment_date":$('.forms #assessment_date').val(),"assessor_name":$('.forms #assessor_name').val(),"total_price":$('.forms #sum').text()})
		testdetails.push({"comment":$('.forms #labNotes').val()})
		$('.forms #test_id option:selected').each(function(){
		    testdetails.push({"test_name":$(this).text(),"test_price":$(this).data('subtext')});
		});
		var price      = $('.forms #sum').text();
		var careplanID = $('#careplan_id').val();
		var form_type  = $('#schedule_details_type option:selected').data('category');

		if($('.forms #test_id option:selected').length && price!=0 && $('.forms #assessment_date').val() !='' && $('.forms #assessor_name').val() != ''){
			$.ajax({
				url : '{{ route('ajax.save-caselabTest') }}',
				type: 'POST',
				data: {_token: '{{ csrf_token() }}', testdetails: testdetails, total_price: price, careplan_id: careplanID, form_type: form_type},
				success: function (data){
					window.location.reload();
				},
				error: function (error){
					console.log(error);
				}
			});
		}else{
			alert("Select the Desired Lab Tests, Respective Price, Date, and Technician Name");
		}
	});
	//Load Lab Forms Price List
	var previouslySelected = [];
	$('.content').on('change','#test_id',function(){
		$('#price_type').empty()
		var dropDown = document.getElementById("test_id");
		try{
			var currentlySelected = $(this).val();
			var newSelections = currentlySelected.filter(function (element) {
			return previouslySelected.indexOf(element) == -1;
			});
			previouslySelected = currentlySelected;

			if (newSelections.length) {
			// If there are multiple new selections, we'll take the last in the list
			var labId = newSelections.reverse()[0];
			}
		}catch(e){
		}
		if(labId){
			$.ajax({
				url : '{{ route('ajax.get-ratecardLabTest') }}',
				type: 'POST',
				data: {_token: '{{ csrf_token() }}', labId: labId },
				success: function (data){
					var opts = $.parseJSON(data);
					$('.forms #price_type').append($("<option></option>").attr("value",'').text("Select"));
					$.each(opts, function(i, d){
						// $('.forms #price_type').selectpicker('destroy');
						$('.forms #price_type').append($("<option></option>").attr("value",newSelections).text(d));
					});
					$('.forms #price_type').selectpicker('refresh');
					$('.forms #price_type').selectpicker('toggle');
				},
				error: function (error){
					console.log(error);
				}
			});
		}
	});

	$(function(){
		$('#editCaseModal #source').selectpicker('val','{{ isset($c->source)?$c->source:'' }}');

		$('#feedback_time').bootstrapMaterialDatePicker({
			format: 'DD-MM-YYYY - hh:mm A',
			time: true,
			clearButton: true,
		});
		$('.getdate').bootstrapMaterialDatePicker({
			format: 'DD-MM-YYYY',
			time: false,
			clearButton: true,
		});
	});

	$('.content').on('click','.btnsaveFeedbackComment',function(){
		var feedbackTime    = $('#feedback_time').val();
		var feedbackComment = $('#feedback_comment').val();
		var careplanID      = $('#careplan_id').val();


		if(feedbackTime && feedbackComment){
			$.ajax({
				url : '{{ route('case.save-feedback-comment') }}',
				type: 'POST',
				data: {_token: '{{ csrf_token() }}', feedback_time: feedbackTime, feedback_comment: feedbackComment, careplan_id: careplanID},
				success: function (data){
					window.location.reload();
				},
				error: function (error){
					console.log(error);
				}
			});
		}
	});

	function checkCaseLoss(typeSelect)
	{
		if(typeSelect){
			optionValue = document.getElementById("caseloss").value;
			if(optionValue == typeSelect.value){
				document.getElementById("casesloss").style.display = "block";
			}
			else{
				document.getElementById("casesloss").style.display = "none";
			}
		}
		else{
			document.getElementById("casesloss").style.display = "none";
		}
	}

	//Script for form and slider
	var nurseForm  = `@include('partials.forms.assessment.nurse')`;
	var doctorForm = `@include('partials.forms.assessment.doctor')`;
	var physioForm = `@include('partials.forms.assessment.physiotherapist')`;
	var labForm    = `@include('partials.forms.lab.labtest')`;

	function prepareAssessmentForm()
	{
		var caregiver_id;
		var schedule_id;
		$( "#schedule_details option:selected" ).each(function() {
			user_id = $( this ).attr("data-user-id");
		});
		document.getElementById("listed_user_id").value = user_id;
		return true;
	}

	function viewAssessment(element)
	{
		var form_data;
		$( ".assessmentId_"+element).each(function() {
			form_data = $( this ).attr("data-form-data");
		});

		var data = JSON.parse(form_data);
		var html = `<tbody>`;

		var blockedVariables = ['careplan_id','schedule_id','assessform'];
		$.each(data, function(key, data) {
			    //alert(key + ' is ' + data);
			    if(!blockedVariables.contains(key)){
			    	html += `<tr>
			    	<th>`+formatKey(key)+`</th>
			    	<td>`+data+`</td>
			    </tr>`;
			}
		});

		html += `</tbody>`;
		$('#viewassessmentModal .modal-body #viewAssessmentTable').html(html);
	}

	function viewRequirement(element)
	{
		var form_data;
		$( ".requirementId_"+element).each(function() {
			form_data = $( this ).attr("data-form-data");
		});
		var data = JSON.parse(form_data);
		var html = `<tbody>`;
		var counter = '1';
		var sum = 0;
		// html = `<tr><th>CRN Number:-`+{{ $c->crn_number }}+`</th><th colspan="2">Order Date:-`+data['0']['assessment_date']+`<br>Patient's Name:-{{ $c->patient->first_name }} {{ $c->patient->last_name }}<br>Technician's Name:-`+data['0']['assessor_name']+`</th></tr>`;
		html = `<tr><th style="text-align:right;">CRN Number<br>Order Date<br>Patient's Name<br>Technician's Name</th><th colspan="2">{{ $c->crn_number }}<br>`+data['0']['assessment_date']+`<br>{{ $c->patient->first_name }} {{ $c->patient->last_name }}<br>`+data['0']['assessor_name']+`</th></tr>`;
		html += `<tr>
					<th>#</th>
					<th>Test</th>
					<th>Price</th>
				</tr>`;
		var blockedVariables = [data['0'],data['1']];
		$.each(data, function(key, data) {
			if(!blockedVariables.contains(data)){
			    	html += `<tr>
			    	<th width="20%">`+counter+`</th>
			    	<td>`+data['test_name']+`</td>
			    	<td>`+data['test_price']+`</td>
			    </tr>`;
			counter++;
			sum = parseInt(sum) + parseInt(data['test_price']);
			}
		});

		html +=`<tr><th>Notes</th><th colspan="2" style="text-align:center;">`+data['1']['comment']+`</th></tr>`;
		html += `<tr><th colspan="2">TOTAL</th><th>`+sum+`</th></tr>`;
		html += `</tbody>`;
		$('#viewrequirementModal .modal-body #viewRequirementTable').html(html);
	}

	function viewWorklog(element)
	{
		var vitals = $(element).data("vitals");
		var routines = $(element).attr("data-routines");

		var vitalsData =  $.parseJSON(JSON.stringify(vitals));
		var routinesData =  $.parseJSON(JSON.stringify(routines));
		var html = `<tbody>`;

		if(vitalsData[0]){
			html += `<tr><th colspan="4"><center>Vitals Information</center></th></tr>`;

			html += `<tr>
			<th>Vitals</th>
			<th>Morning</th>
			<th>Noon</th>
			<th>Evening</th>
			</tr>`;

			// Fetch Blood Pressure Information
			html += `<tr>
			<th>Blood Pressure</th>
			<td>`+printValue(vitalsData[0]['morning']['blood_pressure'])+`</td>
			<td>`+printValue(vitalsData[0]['afternoon']['blood_pressure'])+`</td>
			<td>`+printValue(vitalsData[0]['evening']['blood_pressure'])+`</td>
			</tr>`;

			// Fetch Sugar Level Information
			html += `<tr>
			<th>Sugar Level</th>
			<td>`+printValue(vitalsData[0]['morning']['sugar_level'])+`</td>
			<td>`+printValue(vitalsData[0]['afternoon']['sugar_level'])+`</td>
			<td>`+printValue(vitalsData[0]['evening']['sugar_level'])+`</td>
			</tr>`;

			// Fetch Temperature Information
			html += `<tr>
			<th>Temperature</th>
			<td>`+printValue(vitalsData[0]['morning']['temperature'])+`</td>
			<td>`+printValue(vitalsData[0]['afternoon']['temperature'])+`</td>
			<td>`+printValue(vitalsData[0]['evening']['temperature'])+`</td>
			</tr>`;

			// Fetch Pulse Rate Information
			html += `<tr>
			<th>Pulse Rate</th>
			<td>`+printValue(vitalsData[0]['morning']['pulse_rate'])+`</td>
			<td>`+printValue(vitalsData[0]['afternoon']['pulse_rate'])+`</td>
			<td>`+printValue(vitalsData[0]['evening']['pulse_rate'])+`</td>
			</tr>`;
		}

		html += `</tbody>`;
		$('#viewWorklogModal .modal-body #viewWorklogTable').html(html);
    }

	function printValue(value) {
		if(typeof value != 'undefined'){
			return value;
		}
		return '-';
	}

	function formatKey(str){
		if(str){
			str = str.replace("_"," ");
		}
		return str.toUpperCaseWords();
	}

	String.prototype.toUpperCaseWords = function () {
		return this.replace(/\w+/g, function(a){
			return a.charAt(0).toUpperCase() + a.slice(1).toLowerCase()
		})
	}

	Array.prototype.contains = function(obj) {
		var i = this.length;
		while (i--) {
			if (this[i] === obj) {
				return true;
			}
		}
		return false;
	}

$('#schedule_details_type').change(function () {
	var str = ``;
	$( "#schedule_details_type option:selected" ).each(function() {
		str = $( this ).attr("data-specialization");
		document.getElementById("listed_form_type").value = str;
	});

	switch(str) {
		case "nursing":
			str = $.parseHTML(nurseForm);
			$( '.forms' ).html( str );
			$('.forms #assessment_date').bootstrapMaterialDatePicker({format: 'DD-MM-YYYY',time: false,clearButton: true});
			break;
		case "physiotherapy":
			str = $.parseHTML(physioForm);
			$( '.forms' ).html( str );
			addSliderpain('pain_slider_value');
			addSlidermuscle('musclegrade_slider_value');
			$('.forms #assessment_date').bootstrapMaterialDatePicker({format: 'DD-MM-YYYY',time: false,clearButton: true});
			break;
		case "labtest":
			$( ".labhide" ).addClass('invisible');
			str = $.parseHTML(labForm);
			$( '.forms' ).html( str );
			$('.forms #test_id').selectpicker();
			$('.forms #price_type').selectpicker();
			$('.forms #assessment_date').bootstrapMaterialDatePicker({format: 'DD-MM-YYYY - hh:mm A',time: true,clearButton: true});
			break;
		default:
			str = "Please Choose a Caregiver";
			$( '.forms' ).html( str );
			break;
	}
})

$(function(){
	$('.content').on('change', 'input[type=radio][name=assessform]', function(){
		console.log($(this));
		if($(this).attr("id")=="ortho"){
			$(".showformtype").not(".formortho").addClass("hide");
			$(".formortho").removeClass("hide");
		}
		if($(this).attr("id")=="neuro"){
			$(".showformtype").not(".formoneuro").addClass("hide");
			$(".formneuro").removeClass("hide");
		}
	});

	$('.content').on('click', '.btnSelectProvider', function(){
		var id = $(this).attr('data-visit-id');

		var provider = $('input[name=choosen_provider]:checked').val();
		var name = $('input[name=choosen_provider]:checked').data('name');
		var location = $('input[name=choosen_provider]:checked').data('location');
		var phone = $('input[name=choosen_provider]:checked').data('phone');
		var logo = $('input[name=choosen_provider]:checked').data('logo');

		if(typeof provider != 'undefined' && provider != ''){
			$('#providerSearchModal').modal('hide');
			$('#editCaseModal #provider_id').val(provider);
			$('#editCaseModal #selectedProviderName').html('<div class="col-sm-3"><img src="{{ asset('img/provider') }}/'+logo+'" class="img-responsive" style="width: 100px"></div><div class="col-sm-8"><b>'+name+'</b><br>'+location+'<br>Phone: '+phone+'</div>');
		}else{
			alert("Please select provider !");
		}
	});

	$('.content').on('click','.toggle-row-view', function(){
	    var id = $(this).data('row');
	    console.log(id);
	    $('.providerSearchResults tr#'+id).toggle();
	});

	$('.content').on('click','.btnFilterProvider', function(){
		gender = $('#filter_gender option:selected').val();
		city = $('#providerSearchForm #filter_city').val();

		$('.providerSearchResults tbody').html('<tr><td colspan="6"><center>Searching providers... Please wait...</center></td></tr>');

		$.ajax({
			url: '{{ route('ajax.provider.filter') }}',
			type: 'POST',
			data: {_token: '{{ csrf_token() }}', g: gender, c: city},
			success: function (data){
				html = '<tr><td colspan="6">No matching provider(s) found</td></t>';
				if(data.length){
					html = '';
					$(data).each(function(i){
						var details = data[i].details;
						var caregivers = data[i].caregivers;
						var rates = data[i].rates;

						html += `<tr data-id="`+details.id+`" class="provider-details">`;

						if(details){
							html += `<td>`+details.organization_name+`</td>`;
							html += `<td>`+details.city+`<br><small>`+details.area+`</small></td>`;
							html += `<td>`+details.contact_person+`<br><small>`+details.phone_number+`</small></td>`;
							html += `<td><table><tr><th>Male</th><td>`+details.availability.m+`</td><th>Female</th><td>`+details.availability.f+`</td></tr></table></td>`;
							if(caregivers.length)
								html += `<td class="text-center"><a href="javascript: void(0);" class="toggle-row-view" data-row="caregivers_`+details.id+`">View</a></td>`;
							else
								html += `<td class="text-center">-</td>`;

							if(rates.length)
								html += `<td class="text-center"><a href="javascript: void(0);" class="toggle-row-view" data-row="rates_`+details.id+`">View</a></td>`;
							else
								html += `<td class="text-center">-</td>`;

							html += `<td class="text-center"><input name="choosen_provider" type="radio" id="choosen_provider_`+details.id+`" class="with-gap radio-col-blue" data-name="`+details.organization_name+`" data-location="`+details.city+`" value="`+details.id+`" data-phone="`+details.phone_number+`" data-logo="`+details.logo+`" /><label for="choosen_provider_`+details.id+`"></label></td>`;
						}

						html += `</tr>`;

						if(caregivers.length){
							html += `<tr style="display: none; background: #fbe7ad" id="caregivers_`+details.id+`"><td colspan="7">
							<table class="table table-striped table-condensed" style="margin:0">
								<thead class="bg-cyan">
									<tr>
										<th>Caregiver Name</th>
										<th>Age</th>
										<th>Specialization</th>
										<th>Qualification</th>
										<th>Experience</th>
										<th>Languages Known</th>
									</tr>
								</thead>
								<tbody>`;
									$(caregivers).each(function(j){
										html += `<tr>`;
										html += `<td>`+caregivers[j].caregiver_name+`</td>` +
										`<td>`+caregivers[j].caregiver_age+`</td>` +
										`<td>`+caregivers[j].caregiver_specialization+`</td>` +
										`<td>`+caregivers[j].caregiver_qualification+`</td>` +
										`<td>`+caregivers[j].caregiver_experience+`</td>` +
										`<td>`+caregivers[j].caregiver_languages_known+`</td>`;
										html += `</tr>`;
									});

									html += `</tbody></table></td></tr>`;
								}

								if(rates.length){
									html += `<tr style="display: none; background: #fbe7ad" id="rates_`+details.id+`"><td colspan="7">
									<table class="table table-striped table-condensed" style="margin:0">
										<thead class="bg-teal">
											<tr>
												<th>Servie Name</th>
												<th>Rate</th>
											</tr>
										</thead>
										<tbody>`;
											$(rates).each(function(k){
												html += `<tr>`;
												html += `<td>`+rates[k].service_name+`</td>` +
												`<td>`+rates[k].rate+`</td>`;
												html += `</tr>`;
											});

											html += `</tbody></table></td></tr>`;
										}

									});
				}

				$('.providerSearchResults tbody').html(html);
				//$('.providerSearchResults').DataTable();
			},
			error: function (error){
				console.log(error);
			}
		})
	});
});


function addSliderpain(element){
	var slider = document.getElementById(element);
	noUiSlider.create(slider, {
		start: [0],
		connect: true,
		step:1,
		range: {
			'min': 0,
			'max': 10
		}
	});
	getNoUISliderValuePain(slider, true);
	formshow();
}

function addSlidermuscle(element){
	var slider = document.getElementById(element);
	noUiSlider.create(slider, {
		start: [0],
		connect: true,
		step:1,
		range: {
			'min': 0,
			'max': 5
		}
	});
	getNoUISliderValueMuscleGrade(slider, true);
	formshow();
}

function formshow(){
	$("input[name=group2]").change(function() {
		var test = $(this).val();
		$(".desc").hide();
		$("#"+test).show();
	});
};

function getNoUISliderValuePain(slider, percentage) {
	slider.noUiSlider.on('update', function () {
		var val = slider.noUiSlider.get();
		if (percentage) {
			val = parseInt(val);
            //val += '%';
        }
        console.log(val);
        $(slider).parent().find('span.js-nouislider-value.pain').text(val);
    });
}

function getNoUISliderValueMuscleGrade(slider, percentage) {
	slider.noUiSlider.on('update', function () {
		var val = slider.noUiSlider.get();
		if (percentage) {
			val = parseInt(val);
            //val += '%';
        }
        console.log(val);
        $(slider).parent().find('span.js-nouislider-value.mus_gra').text(val);
    });
}
</script>
<script>

	$(function(){
		initBlock();

		initAutoComplete({
			'route': 'street_address',
			'administrative_area_level_2': 'area',
			'locality': 'city',
			'postal_code': 'zipcode',
			'administrative_area_level_1': 'state',
			'country': 'country',
			'geometry': 'location_coordinates'
		});

		var medicalConditions = [
		{name: "Hospitalized"},
		{name: "COPD"},
		{name: "Post Operative Care"},
		{name: "Ortho Case"},
		{name: "Neuro Case"},
		{name: "Cardiac Case"},
		{name: "Hyper Tension"},
		{name: "Hypo Tension"},
		{name: "Diabetes"},
		{name: "Alzhiemers"},
		{name: "Parkinsons"},
		{name: "Bed Ridden"},
		{name: "Bladder Incontinence"},
		{name: "Weakness"},
		{name: "Arthritis"},
		{name: "TKR"},
		{name: "Dressing"},
		{name: "BedSore"},
		{name: "Cancer"},
		{name: "Dialysis"},
		{name: "Stroke"},
		{name: "Injections"},
		{name: "Tracheotomy"},
		{name: "Suction"},
		{name: "Catheter"},
		{name: "Hip Surgery"},
		{name: "Knee Replacement"}
		];


		$('.medical_conditions').tagsinput({
			typeahead: {
				source: medicalConditions.map(function(item) { return item.name }),
				afterSelect: function() {
					this.$element[0].value = '';
				}
			}
		});

		var medications = [
		{name: "Cardiac Medicine"},
		{name: "Diabetic Medicine"},
		{name: "Ryles Tube"},
		{name: "Peg Tube"},
		{name: "Hypertension Medicine"},
		{name: "Pain Killer"},
		{name: "Oral Mediation"},
		{name: "IV/IM Medications"}
		];

		$('.medications').tagsinput({
			typeahead: {
				source: medications.map(function(item) { return item.name }),
				afterSelect: function() {
					this.$element[0].value = '';
				}
			}
		});

		$('.tags-input').tagsinput({
			allowDuplicates: false
		});

		// Fill case details to edit modal
		@if($c->medical_conditions)
		@foreach(explode(",",$c->medical_conditions) as $mc)
		$('#editCaseModal #medical_conditions').tagsinput('add','{{ $mc }}');
		@endforeach
		@endif

		@if($c->medications)
		@foreach(explode(",",$c->medications) as $m)
		$('#editCaseModal #medications').tagsinput('add','{{ $m }}');
		@endforeach
		@endif
		$('#editCaseModal #source').selectpicker('val','{{ $c->source }}');

		$('.content').on('click','.btnAddComment',function(){
			$('#commentModal #comment').val('');
			$('#commentModal').modal();
		});

		$('.content').on('click','.btnEditCarePlan',function(){
			//$('#editCaseModal #comment').val('');
			$('#editCaseModal').modal();
		});

		$('.content').on('click','.btnEditPatient',function(){
			$('#editPatientModal').modal();
		});

		$('.content').on('click','.btnSaveCaseDetails',function(e){
			e.preventDefault();
			var form_data = $('#editCaseForm').serialize();

			$.ajax({
				url: '{{ route('case.update-case-details') }}',
				type: 'POST',
				data: $('#editCaseForm').serialize(),
				success: function (response){
					console.log(response);
					$('#editCaseModal').modal('hide');
					// Update View
					var crnNumber = $('#editCaseModal #crn_number').val();
					var provider = $('#editCaseModal #selectedProviderName').html();
					var careplanName = $('#editCaseModal #careplan_name').val();
					var careplanDesc = $('#editCaseModal #careplan_description').val();
					var medicalConditions = $('#editCaseModal #medical_conditions').val();
					var medications = $('#editCaseModal #medications').val();
					var source = $('#editCaseModal #source option:selected').text();
					var referrerName = $('#editCaseModal #referrer_name').val();
					var service = $('#editCaseModal #service_id option:selected').text();
					var noOfHours = $('#editCaseModal #no_of_hours').val();
					var genderPreference = $('#editCaseModal #gender_preference:checked').val();
					var languagePreference = [];
					$("#editCaseModal input[type=checkbox][name=language_preference]:checked").map(function(){
						languagePreference.push($(this).val());
					});
					var crnNo = $('#editCaseModal #crn_number').val();

					$('#care_plan_tab .col-sm-6 .row:eq(0) > div:eq(1) > .form-group > .form-line').html(crnNumber);
					$('#care_plan_tab .col-sm-6 .row:eq(1) > div:eq(1) > .form-group > .form-line').html(provider);
					$('#care_plan_tab .col-sm-6 .row:eq(2) > div:eq(1) > .form-group > .form-line').html(careplanName);
					$('#care_plan_tab .col-sm-6 .row:eq(3) > div:eq(1) > .form-group > .form-line').html(careplanDesc);
					$('#care_plan_tab .col-sm-6 .row:eq(4) > div:eq(1) > .form-group > .form-line').html(medicalConditions);
					$('#care_plan_tab .col-sm-6 .row:eq(5) > div:eq(1) > .form-group > .form-line').html(medications);
					$('#care_plan_tab .col-sm-6 .row:eq(6) > div:eq(1) > .form-group > .form-line').html(service);
					$('#care_plan_tab .col-sm-6 .row:eq(7) > div:eq(1) > .form-group > .form-line').html(noOfHours);
					$('#care_plan_tab .col-sm-6 .row:eq(8) > div:eq(1) > .form-group > .form-line').html(genderPreference);
					$('#care_plan_tab .col-sm-6 .row:eq(9) > div:eq(1) > .form-group > .form-line').html(languagePreference.toString());
					$('#care_plan_tab .col-sm-6 .row:eq(10) > div:eq(1) > .form-group > .form-line').html(source);
					$('#care_plan_tab .col-sm-6 .row:eq(11) > div:eq(1) > .form-group > .form-line').html(referrerName);

					showNotification('bg-light-green', 'Details updated successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
					window.location.href = response.redirect;
				},
				error: function (error){
					console.log(error);
				}
			});

			return false;
		});

		/*-------------------------------------------
		/*  Handles Schedule Add Block
		/*-------------------------------------------*/
		$('.content').on('click','.btnAddSchedule', function(){
			var cur_size = $('#schedules_tab > #visit_list_pane > .visitListCard').length;
			var table_size = $('#schedules_tab table tbody tr').length;
			var rowCount = parseInt(cur_size) + parseInt(table_size);
			console.log(cur_size+' '+table_size+' '+rowCount);
			$('#visitListTemplate').clone().prependTo('#schedules_tab #visit_list_pane');

			var count = (cur_size+1);
			var div = $('#schedules_tab #visit_list_pane #visitListTemplate');
			$(div).attr('id','visitList_'+count);
			var block = '#schedules_tab > #visit_list_pane > #visitList_'+count;

			$(block).removeClass('hide');
			// Change Header title
			//var rowCount = $('#schedules_tab table tbody tr').length;
			$(block+' > .header > h2').html('Visit Schedule - #'+(rowCount+1));
			// Change Header data ID
			$(block+' > .header > ul > li > a.removeVisitList').attr('data-visit-id',count);
			$(block+' > .header > ul > li > a.slideVisitList').attr('data-visit-id',count);
			// Change All input Data IDs
			$(block).find('input, select, textarea').each(function(){
				$(this).attr('data-visit-id',count);
			});

			$(block).find('.btnChooseStaff').attr('data-visit-id',count);
			$(block).find('#selectedStaffName_0').attr('data-visit-id',count);
			$(block).find('#selectedStaffName_0').attr('id','selectedStaffName_'+count);

			$(block).find('.btnSaveSchedule').attr('data-visit-id',count);

			initBlock();

			$('html, body').animate({
				scrollTop: $('#visitList_'+count).offset().top
			}, 800);
		});

		$('.content').on('click','.slideVisitList', function(){
			var id = $(this).attr('data-visit-id');
			var current_class = $(this).find('i').html();
			if(current_class == 'expand_more')
				$(this).find('i').html('expand_less');
			else
				$(this).find('i').html('expand_more');

			$('#visitList_'+id+' div.body').slideToggle("slow");
		});

		$('.content').on('click','.removeVisitList', function(){
			var id = $(this).attr('data-visit-id');

			if(confirm("Are you sure?")){
				$('#visitList_'+id).remove();
			}
		});

		$('.content').on('click','.btnChooseProvider', function(){
			var id = $(this).data('visit-id');
			$('#providerSearchForm #filter_service').selectpicker('val',$('#editCaseModal #service_id').val());
			$('#providerSearchForm #filter_city').val($('#city').val());
			$('.btnFilterProvider').trigger( "click" );
			$('#providerSearchModal .btnSelectProvider').attr('data-visit-id',id);
			$('#providerSearchModal').modal();
		});

		/*-------------------------------------------
		/*  Handles Existing Customer Check
		/*-------------------------------------------*/
		$('.content').on('click', '#checkForPreviousCarePlans', function(){
			checkIfExistingCustomer();
		});

		/*-------------------------------------------
		/*  Handles Case Disposition Update
		/*-------------------------------------------*/
		$('.content').on('click','.saveCarePlanDisposition', function(){
			if($('.saveCarePlanDisposition').data('provider-id') == 'WWlXZ1FCUFBwWVRTV3RpcEFnVDd5UT09' && $('#careplan_status option:selected').val() != 'CASE LOSS'){
				alert("Please Choose a Provider");
				return false;
			}
			var id = $(this).data('careplan-id');
			var pid = $(this).data('provider-id');
			var status = $('#careplan_status option:selected').val();

			var caselosscomment = $("input[name=loss]:checked").val();

			var date = $('#careplan_disposition_date').val();
			var comment = $('#careplan_comment').val();

			if(status){
				$.ajax({
					url: '{{ route('case.update-disposition') }}',
					type: 'POST',
					data: {_token: '{{ csrf_token() }}', cid: id, pid: pid, status: status, date: date, comment: comment, caselosscomment: caselosscomment},
					success: function (data){
						console.log(data);
						if(data){
							window.location.reload();
						}
					},
					error: function (error){
						console.log(error);
					}
				});
			}
		});

		$('.content').on('click','.btnChooseStaff', function(){
			var id = $(this).attr('data-visit-id');
			var mode = $(this).data('mode');

			$('#staffSearchModal .btnSelectStaff').attr('data-visit-id',id);
			$('#staffSearchModal .btnSelectStaff').attr('data-mode',mode);
			$('#staffSearchModal').modal();
		});

		$('.content').on('click','.btnFilterStaff', function(){
			specialization = $('#filter_specialization option:selected').val();
			experience = $('#filter_experience option:selected').val();
			city = $('#filter_city').val();

			var language = [];
			$('#filter_language :selected').each(function(i, selected){
			  language[i] = $(selected).text();
			});

			var skill = [];
			$('#filter_skill :selected').each(function(i, selected){
			  skill[i] = $(selected).val();
			});

			$.ajax({
				url: '{{ route('ajax.caregiver.filter') }}',
				type: 'POST',
				data: {_token: '{{ csrf_token() }}', s: specialization, e: experience, c: city, l: language, sk: skill},
				success: function (data){

					html = '<tr><td class="text-center" colspan="7">Click \'Apply Filter\' to get results</td></tr>';
					if(data.length){
						html = '';
						$.each(data, function(i){
							html += `<tr>
								<td>`+data[i].full_name+`<br><small>`+data[i].location+`</small></td>
								<td>`+data[i].mobile+`<br><small>`+data[i].email+`</small></td>
								<td>`+data[i].specialization+`</td>
								<td>`+data[i].experience+`</td>
								<td>`+data[i].total_visits+`</td>
								<td>`+data[i].rating+`</td>
								<td class="text-center"><input name="choosen_emp" type="radio" id="choosen_emp_`+data[i].id+`" class="with-gap radio-col-blue" data-name="`+data[i].full_name+`" data-mobile="`+data[i].mobile+`" value="`+data[i].id+`" /><label for="choosen_emp_`+data[i].id+`"></label></td>
							</tr>`;
						});
					}
					$('.caseSearchResults tbody').html(html);
				},
				error: function (error){
					console.log(error);
				}
			})
		});

		$('.content').on('input','.amtCal', function(){
			var editMode = false;
			var id = $(this).attr('data-visit-id');
			editMode = $(this).attr('data-mode');

			calculateTotal(id,editMode);
		});

		$('.content').on('click','.btnReset', function(){
			clearSearchForm();
		});

		$('.content').on('click', '.btnSelectStaff', function(){
			var id = $(this).attr('data-visit-id');
			var mode = $(this).attr('data-mode');

			var emp = $('input[name=choosen_emp]:checked').val();
			var name = $('input[name=choosen_emp]:checked').data('name');
			var mobile = $('input[name=choosen_emp]:checked').data('mobile');

			if(typeof emp != 'undefined' && emp != ''){
				$('#staffSearchModal').modal('hide');
				if(mode == 'view'){
					$('#viewScheduleModal #caregiver_id').val(emp);
					$('#viewScheduleModal #selectedStaffName_'+id).html('<div class="col-sm-3"><img src="/img/user.png" width="40" height="40" class="img-circle"></div><div class="col-sm-9">'+name+'<br>Mob: '+mobile+'</div><br>');
				}else{
					$('#visitList_'+id+' #caregiver_id').val(emp);
					$('#visitList_'+id+' #selectedStaffName_'+id).html('<div class="col-sm-3"><img src="/img/user.png" width="40" height="40" class="img-circle"></div><div class="col-sm-9">'+name+'<br>Mob: '+mobile+'</div>');
				}
			}else{
				alert("Please select staff !");
			}
		});

		$('.content').on('click','.btnViewSchedule', function(){
			var data = $(this).data();
			console.log(data);
			var visitID = data.visitId;
			$('#viewScheduleModal *').filter(function(){
				if(typeof $(this).attr('data-visit-id') != 'undefined'){
					$(this).attr('data-visit-id',visitID);
				}
			});

			$('#viewScheduleModal .btnUpdateVisitSchedule').attr('data-caregiver-id',data.caregiverId);
			$('#viewScheduleModal .btnUpdateVisitSchedule').attr('data-careplan-id',data.careplanId);
			$('#viewScheduleModal .btnUpdateVisitSchedule').attr('data-patient-id',data.patientId);

			$('#viewScheduleModal #status').selectpicker('val',data.visitStatus);


			$('#viewScheduleModal #selectedStaffName_0').html('<div class="col-sm-3"><img src="/img/user.png" width="40" height="40" class="img-circle"></div><div class="col-sm-9">'+data.caregiver+'<br>Mob: '+data.caregiverMobile+'</div><br>');
			$('#viewScheduleModal #caregiver_id').val(data.caregiverId);


			/* Time Details */
			$('#viewScheduleModal #start_date').val(data.startDatetime);
			$('#viewScheduleModal #end_date').val(data.endDatetime);

			$('#viewScheduleModal #notes').val(data.notes);
			$('#viewScheduleModal').modal();
		});

		//$('.date').inputmask('dd-mm-yyyy', { placeholder: '__-__-____' });
		/* $('.money-inr').inputmask('₹ 999999.99', { numericInput: true, rightAlign: false }); */
	});

	$('.content').on('click','.btnUpdateVisitSchedule',function(){
		var id = $(this).attr('data-visit-id');
		var careplanID = $(this).attr('data-careplan-id');
		var patientID = $(this).attr('data-patient-id');

		// Parse Schedule Details
		var block = '#visitScheduleEditForm';
		visitListArray = [];

		var startDate = $(block+ ' #start_date').val();
		var endDate = $(block+ ' #end_date').val();
		var notes = $(block+ ' #notes').val();

		var caregiverID = $(block+ ' #caregiver_id').val();

		var status = $(block+' #status').selectpicker('val');
		var comment = $(block+' #comment').val();

		visitListArray = {
			'id': id,
			'start_date': startDate,
			'end_date': endDate,
			'notes': notes,
			'caregiver_id': caregiverID,
			'status': status,
			'comment': comment
		};

		console.log(visitListArray);
		if(visitListArray){
			$.ajax({
				url: '{{ route('case.save-schedule') }}',
				type: 'POST',
				data: {_token: '{{ csrf_token() }}', careplan_id: careplanID, patient_id: patientID, visit: visitListArray},
				success: function (data){
					console.log(data);
					window.location.reload();
				},
				error: function (error){
					console.log(error);
				}
			});
		}
	});

	function initBlock()
	{
		// $('.date').inputmask('dd-mm-yyyy', { placeholder: '__-__-____' });
		$('.date').bootstrapMaterialDatePicker({
			format: 'DD-MM-YYYY',
			time: false,
			clearButton: true,
		});
	}

	function checkIfExistingCustomer()
	{
		var fname = $('#first_name').val();
		var lname = $('#last_name').val();
		var mobile = $('#mobile').val();
		var email = $('#email').val();
		var dob = $('#date_of_birth').val();

		if(fname != '' || lname != '' || mobile != '' || email != '' || dob != ''){
			$.ajax({
				url: '{{ route('case.check-for-existing-case') }}',
				type: 'POST',
				data: {f: fname, l: lname, m: mobile, e: email, d: dob, _token: '{{ csrf_token() }}'},
				success: function (data){
					console.log(data);
				},
				error: function (error){
					console.log(error);
				}
			});
		}
	}

	function clearSearchForm()
	{
		$('#staffSearchModal #staffSearchForm #reset').trigger('click');
		$('#staffSearchModal #staffSearchForm #filter_specialization').selectpicker('val','0');
		$('#staffSearchModal #staffSearchForm #filter_experience').selectpicker('val','0');
		$('#staffSearchModal .caseSearchResults tbody').html('<tr><td class="text-center" colspan="7">Click \'Search\' to getresults</td></tr>');
	}

	var visitListArray = [];

	$('.content').on('click','.btnSaveSchedule', function(){
		var id = $(this).attr('data-visit-id');
		var crnNo = $(this).attr('data-crn-no')
		var careplanID = $('#careplan_id').val();
		var patientID = $('#patient_id').val();

		// Parse all Visit List
		var block = '#schedules_tab #visit_list_pane #visitList_'+id;
		if($(block).length){
			visitListArray = [];

			var startDate = $(block+ ' #start_date').val();
			var endDate = $(block+ ' #end_date').val();
			var notes = $(block+ ' #notes').val();

			var caregiverID = $(block+ ' #caregiver_id').val();

			visitListArray = {
				'start_date': startDate,
				'end_date': endDate,
				'notes': notes,
				'caregiver_id': caregiverID,
			};
		}

		if(visitListArray){
			$.ajax({
				url: '{{ route('case.save-schedule') }}',
				type: 'POST',
				data: {_token: '{{ csrf_token() }}', crn_no: crnNo, careplan_id: careplanID, patient_id: patientID, visit: visitListArray},
				success: function (data){
					console.log(data);
					window.location.reload();
				},
				error: function (error){
					console.log(error);
				}
			});
		}
	});
</script>
@endsection
