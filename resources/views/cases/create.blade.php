@extends('layouts.main-layout')

@section('page_title','New Case - Cases |')

@section('active_cases','active')

@section('plugin.styles')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<!-- Bootstrap Tagsinput Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
@endsection

@section('page.styles')
<style>
	.card .header .header-dropdown{
		top: 15px;
	}
	hr{
		margin-top: 10px;
		margin-bottom: 10px;
	}
	.bootstrap-tagsinput{
		min-height: 40px;
		vertical-align: top;
		padding: 4px 0;
		width: 100%;
	}
	.bootstrap-tagsinput input{
		padding-left: 0;
	}
	.plus:after{
		content: ' + ';
		font-size: 18px;
		font-weight: bold;
	}
	.care-plan-card .header{
		padding: 15px 20px;
	}
	.modal-lg{
		width: 70%;
	}
	.modal .modal-header{
		padding: 20px 25px 15px 25px;
	}
	.modal-footer{
		border-top: 1px solid #ddd !important;
	}
</style>
@endsection

@section('content')
@if (count($errors) > 0)
<div class="row clearfix">
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif

<div class="row">
	<form id="caseForm" action="{{ isset($e)?route('case.update', Helper::encryptor('encrypt',$e->id)):route('case.save-case') }}" method="POST">
		{{ csrf_field() }}
		@if(isset($e)){{ method_field('PUT') }}@endif
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<a href="{{ route('case.index') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
						<i class="material-icons">arrow_back</i>
					</a>
					<h2 class="col-sm-10">
						{{ isset($e)?'Edit':'New'}} Case
						<small>Case {{ isset($e)?'Updation':'Creation'}} Form</small>
					</h2>
					<ul class="header-dropdown m-r--5">
						<div class="btn-group" role="group">
							<button type="button" class="btn btn-success m-l-15 waves-effect"><i class="material-icons" style="color: #fff">save</i></button>
							<button type="button" onclick="javascript: return validator();" class="btn btn-success waves-effect" style="line-height: 1.9;padding-left:0">Save Case</button>
						</div>&nbsp;&nbsp;
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="body" style="padding-top: 0">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active">
							<a href="#patient_details_tab" data-toggle="tab" aria-expanded="false">
								<i class="material-icons">person</i> Patient &amp; Enquirer Details
							</a>
						</li>
						<li role="presentation" class="">
							<a href="#care_plan_tab" data-toggle="tab" aria-expanded="true">
								<i class="material-icons">child_friendly</i> Care Plan Details
							</a>
						</li>
						<li role="presentation" class="">
							<a href="#scheduling_tab" data-toggle="tab">
								<i class="material-icons">date_range</i> Scheduling
							</a>
						</li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane fade active in" id="patient_details_tab">
							<div class="row clearfix">
								<div class="col-sm-6 form-horizontal">
									<h2 class="card-inside-title">
										Patient Details
										<small>Name, Mobile Number, Email</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="first_name">First Name</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="first_name" name="first_name" class="form-control searchCol" placeholder="Ex. Ashok" value="{{ isset($e)?$e->first_name:'' }}" required>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="last_name">Last Name</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="last_name" name="last_name" class="form-control searchCol" placeholder="Ex. Kumar" value="{{ isset($e)?$e->last_name:'' }}">
												</div>
											</div>
										</div>
									</div>
									{{-- <div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="date_of_birth">Date of Birth</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="date_of_birth" name="date_of_birth" class="form-control date searchCol" placeholder="Date of Birth" value="{{ isset($e)?$e->date_of_birth:'' }}">
												</div>
											</div>
										</div>
									</div> --}}
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="gender">Gender</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select class="form-control show-tick" id="gender" name="gender" required>
													<option value="">-- Please select--</option>
													<option value="Male">Male</option>
													<option value="Female">Female</option>
													<option value="Other">Other</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="patient_age">Patient Age</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="patient_age" name="patient_age" class="form-control" placeholder="Patient Age"value="{{ isset($e)?$e->patient_age:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="patient_weight">Patient Weight</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="patient_weight" name="patient_weight" class="form-control" placeholder="Patient Weight"value="{{ isset($e)?$e->patient_weight:'' }}">
												</div>
											</div>
										</div>
									</div>
									<h2 class="card-inside-title">
										Patient Address
										<small>Address / Area, City and State</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="street_address">Street Address</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="street_address" name="street_address" class="form-control" placeholder="Ex. #20, 3rd Cross, 5th Main" value="{{ isset($e)?$e->street_address:'' }}"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="area">Area</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="area" name="area" class="form-control" placeholder="Ex. Sanjay Nagar" value="{{ isset($e)?$e->current_area:'' }}"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="requiredad" for="city">City</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="city" name="city" class="form-control" placeholder="Ex. Bangalore" value="{{ isset($e)?$e->current_city:'' }}"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="zipcode">Zip Code</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="zipcode" name="zipcode" class="form-control" placeholder="Ex. 560001" value="{{ isset($e)?$e->current_zipcode:'' }}"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="state">State</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="state" name="state" class="form-control" placeholder="Ex. Karnataka" value="{{ isset($e)?$e->current_state:'' }}"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="country">Country</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="country" name="country" class="form-control" placeholder="Ex. India" value="{{ isset($e)?$e->current_country:'' }}"/>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="country">Location Co-ordinates</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="location_coordinates" name="location_coordinates" class="form-control" placeholder="Eg. 12.789666, 32.6589656" value="{{ isset($e)?$e->location_coordinates:'' }}"/>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="col-sm-6 form-horizontal">
									<h2 class="card-inside-title">
										Enquirer Details
										<small>Enquirer Name, Phone, Email, Relationship</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="enquirer_name">Enquirer Name</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="enquirer_name" name="enquirer_name" class="form-control" placeholder="Ex. Ajay Rao">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="enquirer_phone">Enquirer Phone</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="enquirer_phone" name="enquirer_phone" class="form-control" placeholder="Ex. 9845698456">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="alternate_number">Alternate Number</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="alternate_number" name="alternate_number" class="form-control searchCol" placeholder="Alternate Number">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="enquirer_email">Enquirer Email</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" id="enquirer_email" name="enquirer_email" class="form-control" placeholder="Ex. ajay@gmail.com">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="relationship_with_patient">Relationship with Patient</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select id="relationship_with_patient" name="relationship_with_patient" class="form-control show-tick" data-live-search="true" placeholder="Relationship with Patient">
													<option value=""> -- Select Relationship -- </option>
													<option value="BROTHER">BROTHER</option>
													<option value="BROTHER-IN-LAW">BROTHER-IN-LAW</option>
													<option value="DAUGHTER">DAUGHTER</option>
													<option value="FATHER">FATHER</option>
													<option value="FATHER-IN-LAW">FATHER-IN-LAW</option>
													<option value="FRIEND">FRIEND</option>
													<option value="GRANDFATHER">GRANDFATHER</option>
													<option value="GRANDMOTHER">GRANDMOTHER</option>
													<option value="HUSBAND">HUSBAND</option>
													<option value="MOTHER">MOTHER</option>
													<option value="MOTHER-IN-LAW">MOTHER-IN-LAW</option>
													<option value="SELF">SELF</option>
													<option value="SISTER">SISTER</option>
													<option value="SON">SON</option>
													<option value="WIFE">WIFE</option>
													<option value="OTHER">OTHER</option>
												</select>
											</div>
										</div>
									</div>
									<h2 class="card-inside-title">
										Case History
										<small>Previous Case History (If any)</small>
									</h2>
									<div class="row clearfix text-center" style="min-height: 200px;">
										<a class="btn btn-primary btn-lg waves-effect" id="checkForPreviousCarePlans">Check for Previous Careplans</a>
									</div>
								</div>
							</div>
						</div>

						<div role="tabpanel" class="tab-pane fade" id="care_plan_tab">
							{{-- <div class="row clearfix">
								<div class="col-sm-12 text-center">
									<a class="btn btn-info btnAddCarePlan"><span class="plus"></span> Add Care Plan</a>
								</div>
							</div> --}}
							<div class="row clearfix form-horizontal">
								<div class="col-sm-6">
									<h2 class="card-inside-title">
										Care Plan Details
										<small>Name, Medical Condition, Medication, Notes</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="careplan_name">Care Plan Name</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="careplan_name" name="careplan_name" class="form-control" placeholder="Care Plan Name">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="careplan_description">Description</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<textarea style="text-transform: capitalize;" id="careplan_description" name="careplan_description" class="form-control" placeholder="Description"></textarea>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="medical_conditions">Medical Conditons</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input class="medical_conditions" id="medical_conditions" name="medical_conditions" data-provide="typeahead"  type="text" />
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="medications">Medications</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input class="medications" name="medications" id="medications" data-provide="typeahead" type="text"/>
												</div>
											</div>
										</div>
									</div>
									<h2 class="card-inside-title">
										Referral Details
										<small>Referrer Name, Case Source</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="source">Source</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select class="form-control show-tick" data-live-search="true" id="source" name="source">
													<option value="">--Select--</option>
													<option value="Google Search">Google Search</option>
													<option value="Website">Website</option>
													<option value="Referral from Service Partner">Referral from Service Partner</option>
													<option value="Online Classifieds">Online Classifieds</option>
													<option value="Email Campaign">Email Campaign</option>
													<option value="Facebook">Facebook</option>
													<option value="Twitter">Twitter</option>
													<option value="Google Plus">Google Plus</option>
													<option value="Events">Events</option>
													<option value="Exhibitions">Exhibitions</option>
													<option value="Flyer">Flyer</option>
													<option value="Print Classifieds">Print Classifieds</option>
													<option value="Radio">Radio</option>
													<option value="Television">Television</option>
													<option value="Print Ads">Print Ads</option>
													<option value="News/Media">News/Media</option>
													<option value="Other">Other</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="referrer_name">Referrer Name</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input style="text-transform: capitalize;" type="text" id="referrer_name" name="referrer_name" class="form-control referrer-name" placeholder="Referrer Name">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<h2 class="card-inside-title">
										Service Details
										<small>Service Type, Duration, Preference, Care Taker</small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="designation">Service Required</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select class="form-control show-tick" data-live-search="true" id="service_id" name="service_id">
													<option value="0">-- Please select --</option>
												@if(isset($services) && count($services))
													@foreach($services as $s)
													<option value="{{ $s->id }}">{{ $s->service_name }}</option>
													@endforeach
												@endif
												</select>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label class="required" for="no_of_hours">No. of hours</label>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="number" id="no_of_hours" name="no_of_hours" class="form-control" placeholder="No. of hours">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
                                                                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                                                                        <label class="required" for="case_charges">Case Charges</label>
                                                                                </div>
                                                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-7">
                                                                                        <div class="form-group">
                                                                                                <div class="form-line">
                                                                                                        <input type="number" id="case_charges" name="case_charges" class="form-control"/>
                                                                                                </div>
                                                                                        </div>
                                                                                </div>
                                                                        </div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="gender_preference">Gender Preference</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group" style="margin-top: 1%">
												<input name="gender_preference" type="radio" id="gender_preference_any" class="with-gap radio-col-deep-purple" value="Any" checked />
												<label for="gender_preference_any">Any</label>
												<input name="gender_preference" type="radio" id="gender_preference_male" class="with-gap radio-col-deep-purple" value="Male" />
												<label for="gender_preference_male">Male</label>
												<input name="gender_preference" type="radio" id="gender_preference_female" class="with-gap radio-col-deep-purple" value="Female"/>
												<label for="gender_preference_female">Female</label>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="language_preference">Language Preference</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group" style="margin-top: 2.5%">
												<div class="demo-checkbox language-preference">
											@if(isset($languages) && count($languages))
												@foreach($languages as $l)
													<input type="checkbox" id="languages_{{ strtolower($l->language_name) }}" name="language_preference[]" class="filled-in chk-col-light-blue" value="{{ $l->language_name }}" />
													<label for="languages_{{ strtolower($l->language_name) }}">{{ $l->language_name }}</label>
												@endforeach
											@endif
												</div>
											</div>
										</div>
									</div>
									@if(count($branches))
									<h2 class="card-inside-title">
										Branch Details
										<small></small>
									</h2>
									<div class="row clearfix">
										<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
											<label for="branch">Branch</label>
										</div>
										<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
											<div class="form-group">
												<select class="form-control show-tick" id="branch_id" name="branch_id">
													<option value="0">-- Please select --</option>
													@foreach($branches as $b)
													<option value="{{ $b->id }}">{{ $b->branch_name }}</option>
													@endforeach
												</select>
											</div>
										</div>
									</div>
									@else
										<input type="hidden" name="branch_id" id="branch_id" value="0">
									@endif
								</div>
							</div>
							<div class="clearfix"></div>
						</div>

						<div role="tabpanel" class="tab-pane fade" id="scheduling_tab">
							<div class="row clearfix">
								<div class="col-sm-12 text-center">
									<a class="btn btn-info btnAddSchedule"><span class="plus"></span> Add Visit</a>
								</div>
							</div>
							<div id="visit_list_pane" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" name="skills" id="skills" value="">
		<input type="hidden" name="professional_details_id" value="{{ isset($e->professional)?Helper::encryptor('encrypt',$e->professional->id):0}}" />
		<input type="hidden" name="account_details_id" value="{{ isset($e->account)?Helper::encryptor('encrypt',$e->account->id):0}}" />
		<input type="hidden" name="payroll_details_id" value="{{ isset($e->payroll)?Helper::encryptor('encrypt',$e->payroll->id):0}}" />
		<input type="hidden" name="accommodation_details_id" value="{{ isset($e->accommodation)?Helper::encryptor('encrypt',$e->accommodation->id):0}}" />
	</form>
</div>

<!-- Visit List Template -->
<div id="visitListTemplate" class="card care-plan-card visitListCard hide">
	<div class="header bg-cyan">
		<h2>
			Visit Schedule - #1
		</h2>
		<ul class="header-dropdown m-r--5" style="top: 5px;">
			<li><a href="javascript:void(0);" class="removeVisitList btn btn-danger waves-effect waves-block" data-visit-id="1"><i class="material-icons">delete</i></a></li>
			<li>&nbsp;</li>
			<li><a href="javascript:void(0);" class="slideVisitList btn btn-warning waves-effect waves-block" data-visit-id="1"><i class="material-icons">expand_more</i></a></li>
		</ul>
	</div>
	<div class="body form-horizontal">
		<div class="col-sm-6">
			<h2 class="card-inside-title">
				Visit Details
				<small>Caregiver, Visit Type, Start Date, End Date</small>
			</h2>
			<div class="row clearfix">
				<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
					<label for="no_of_hours">Start Date</label>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					<div class="form-group">
						<div class="form-line">
							<input type="text" data-visit-id="1" id="start_date" class="form-control date" placeholder="Start Date">
						</div>
					</div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
					<label for="no_of_hours">End Date</label>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					<div class="form-group">
						<div class="form-line">
							<input type="text" data-visit-id="1" id="end_date" class="form-control date" placeholder="End Date">
						</div>
					</div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
					<label for="no_of_hours">Notes</label>
				</div>
				<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
					<div class="form-group">
						<div class="form-line">
							<textarea data-visit-id="1" id="notes" class="form-control" placeholder="Notes (if any)"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<h2 class="card-inside-title">
				Staff Details
				<small>Caregiver</small>
			</h2>
			<div class="row clearfix">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"></div>
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
					<a class="btn btn-warning btn-block waves-effect btnChooseStaff" onclick="javascript: return clearSearchForm();" data-visit-id="0">Choose Staff</a>
				</div>
			</div><br>
			<div class="row clearfix">
				<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
					<label for="no_of_hours">Staff / Caregiver</label>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<div class="form-group">
						<div style="margin-top: 8px;" id="selectedStaffName_0" data-visit-id="0">-</div>
						<input type="hidden" id="caregiver_id" value=""/>
						{{-- <select class="form-control show-tick" placeholder="Type staff name" data-live-search="true" data-placeholder="Type staff name">
						</select> --}}
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<!-- Staff Search Modal -->
<div class="modal fade" id="staffSearchModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="largeModalLabel">Employee Search Form</h4>
			</div>
			<div class="modal-body">
				<div class="row clearfix">
					<form id="staffSearchForm" class="col-sm-12">
						<div class="col-sm-3">
							<div class="form-group">
								<label for="specialization">Specialization</label>
								<div class="form-line">
									<select class="form-control show-tick" id="filter_specialization">
										<option value="0">-- Any --</option>
								@if(isset($specializations) && count($specializations))
									@foreach($specializations as $s)
										<option value="{{ $s->id }}">{{ $s->specialization_name }}</option>
									@endforeach
								@endif
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label for="experience">Experience</label>
								<div class="form-line">
									<select class="form-control show-tick" id="filter_experience">
										<option value="0">-- Any --</option>
										<option value="0_5">0-5 yr(s)</option>
										<option value="5_10">5-10 yr(s)</option>
										<option value="10_15">10-15 yr(s)</option>
										<option value="15_35">Above 15 yrs</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label for="city">City</label>
								<div class="form-line">
									<input class="form-control" id="filter_city" placeholder="City">
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<br>
							<a class="btn btn-primary waves-effect btnFilterStaff">Search</a>&nbsp;&nbsp;
							<button type="reset" class="btn btn-danger waves-effect btnReset">Reset</button>
						</div>
					</form>
					<div class="col-sm-12">
						<div class="page-loader-wrapper">
							<div class="loader">
								<div class="md-preloader pl-size-md">
									<svg viewbox="0 0 75 75">
										<circle cx="37.5" cy="37.5" r="33.5" class="pl-blue" stroke-width="4" />
									</svg>
								</div>
								<p>Please wait...</p>
							</div>
						</div>
						<table class="table table-bordered table-striped caregiverSearchResults">
							<thead>
								<tr>
									<th>Name</th>
									<th>Contact</th>
									<th>Designation</th>
									<th>Experience</th>
									<th>Total Visits</th>
									<th>Rating</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-center" colspan="7">Click 'Search' to get results</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success waves-effect btnSelectStaff" data-visit-id="0">Continue</button>
				<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>

<!-- Case Search Modal -->
<div class="modal fade" id="caseSearchModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="largeModalLabel">Search Results - Case History</h4>
			</div>
			<div class="modal-body">
				<div class="row clearfix">
					<div class="col-sm-12">
						<table class="table table-bordered table-striped caseSearchResults">
							<thead>
								<tr>
									<th></th>
									<th>Date</th>
									<th>Careplan</th>
									<th>Service</th>
									<th>Medical Conditions</th>
									<th>Medications</th>
									<th>Visits</th>
									<th>Status</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-center" colspan="7">No case record(s) found</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success waves-effect btnSelectCase" data-careplan-id="0">Continue</button>
				<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>
@endsection

@section('plugin.scripts')
<!-- Select Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<!-- Input Mask Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>
<!-- Moment Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<!-- Bootstrap Tags Input Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/typeahead.min.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('bootstrap-3-typeahead/bootstrap3-typeahead.js') }}"></script>
<!-- Bootstrap Notify Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-notify/bootstrap-notify.js') }}"></script>
<script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>

<script src="{{ asset('js/autocomplete.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCvPCONT3odYk2HGHeP3hGhtwXZqx5rekQ&libraries=places"></script>
@endsection

@section('page.scripts')
<script type="text/javascript">
    window.addEventListener('keydown', function(e) {
        if (e.keyIdentifier == 'U+000A' || e.keyIdentifier == 'Enter' || e.keyCode == 13) {
            if (e.target.nodeName == 'INPUT' && e.target.type == 'text' && !$(e.path[1]).hasClass('bootstrap-tagsinput')) {
                e.preventDefault();
                return false;
            }
        }
    }, true);
</script>

<script>
	$(function(){
		initBlock();

		initAutoComplete({
			'route': 'street_address',
			'locality': 'city',
			'postal_code': 'zipcode',
			'administrative_area_level_1': 'state',
			'country': 'country',
			'geometry': 'location_coordinates',
		});

		var medicalConditions = [
		  {name: "Hospitalized"},
		  {name: "COPD"},
		  {name: "Post Operative Care"},
		  {name: "Ortho Case"},
		  {name: "Neuro Case"},
		  {name: "Cardiac Case"},
		  {name: "Hyper Tension"},
		  {name: "Hypo Tension"},
		  {name: "Diabetes"},
		  {name: "Alzhiemers"},
		  {name: "Parkinsons"},
		  {name: "Bed Ridden"},
		  {name: "Bladder Incontinence"},
		  {name: "Weakness"},
		  {name: "Arthritis"},
		  {name: "TKR"},
		  {name: "Dressing"},
		  {name: "BedSore"},
		  {name: "Cancer"},
		  {name: "Dialysis"},
		  {name: "Stroke"},
		  {name: "Injections"},
		  {name: "Tracheotomy"},
		  {name: "Suction"},
		  {name: "Catheter"},
		  {name: "hip Surgery"},
		  {name: "Knee Replacement"}
		];


		$('.medical_conditions').tagsinput({
		  typeahead: {
		    source: medicalConditions.map(function(item) { return item.name }),
		    afterSelect: function() {
		    	this.$element[0].value = '';
		    }
		  }
		});

		var medications = [
		  {name: "Cardiac Medicine"}, 
		  {name: "Diabetic Medicine"},
		  {name: "Ryles Tube"},
		  {name: "Peg Tube"},
		  {name: "Hypertension Medicine"},
		  {name: "Pain Killer"},
		  {name: "Oral Mediation"},
		  {name: "IV/IM Medications"}
		];

		$('.medications').tagsinput({
		  typeahead: {
		    source: medications.map(function(item) { return item.name }),
		    afterSelect: function() {
		    	this.$element[0].value = '';
		    }
		  }
		});

		$('.tags-input').tagsinput({
			allowDuplicates: false
		});

		var referralNames = new Bloodhound({
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			local:  [""]
		});
		referralNames.initialize();

		$('.referrer-name').tagsinput({
			typeaheadjs: {
				name: 'referralNames',
				source: referralNames.ttAdapter()
			}
		});

		$('.content').on('click','.btnAddSchedule', function(){
			var cur_size = $('#scheduling_tab > #visit_list_pane > .visitListCard').length;
			$('#visitListTemplate').clone().prependTo('#scheduling_tab #visit_list_pane');

			var count = (cur_size+1);
			var div = $('#scheduling_tab #visit_list_pane #visitListTemplate');
			$(div).attr('id','visitList_'+count);
			var block = '#scheduling_tab > #visit_list_pane > #visitList_'+count;

			$(block).removeClass('hide');
			// Change Header title
			$(block+' > .header > h2').html('Visit Schedule - #'+count);
			// Change Header data ID
			$(block+' > .header > ul > li > a.removeVisitList').attr('data-visit-id',count);
			$(block+' > .header > ul > li > a.slideVisitList').attr('data-visit-id',count);
			// Change All input Data IDs
			$(block).find('input, select, textarea').each(function(){
				$(this).attr('data-visit-id',count);
			});

			$(block).find('.btnChooseStaff').attr('data-visit-id',count);
			$(block).find('#selectedStaffName_0').attr('data-visit-id',count);
			$(block).find('#selectedStaffName_0').attr('id','selectedStaffName_'+count);

			initBlock();
		});

		$('.content').on('click','.slideVisitList', function(){
			var id = $(this).data('visit-id');
			var current_class = $(this).find('i').html();
			if(current_class == 'expand_more')
				$(this).find('i').html('expand_less');
			else
				$(this).find('i').html('expand_more');

			$('#visitList_'+id+' div.body').slideToggle("slow");
		});

		$('.content').on('click','.removeVisitList', function(){
			var id = $(this).data('visit-id');

			if(confirm("Are you sure?")){
				$('#visitList_'+id).remove();
			}
		});

		$('.content').on('click', '#checkForPreviousCarePlans', function(){
			checkIfExistingCustomer();
		});

		$('.content').on('click','.btnChooseStaff', function(){
			var id = $(this).data('visit-id');

			$('#staffSearchModal .btnSelectStaff').attr('data-visit-id',id);
			$('#staffSearchModal').modal();
		});

		$('.content').on('click','.btnFilterStaff', function(){
			specialization = $('#filter_specialization option:selected').val();
			experience = $('#filter_experience option:selected').val();
			city = $('#filter_city').val();

			$.ajax({
				url: '{{ route('ajax.caregiver.filter') }}',
				type: 'POST',
				data: {_token: '{{ csrf_token() }}', s: specialization, e: experience, c: city},
				success: function (data){
					//console.log(data);
					html = '<tr><td class="text-center" colspan="7">Click \'Apply Filter\' to get results</td></tr>';
					if(data.length){
						html = '';
						$.each(data, function(i){
							html += `<tr>
										<td>`+data[i].full_name+`<br><small>`+data[i].location+`</small></td>
										<td>`+data[i].mobile+`<br><small>`+data[i].email+`</small></td>
										<td>`+data[i].specialization+`</td>
										<td>`+data[i].experience+`</td>
										<td>`+data[i].total_visits+`</td>
										<td>`+data[i].rating+`</td>
										<td class="text-center"><input name="choosen_emp" type="radio" id="choosen_emp_`+data[i].id+`" class="with-gap radio-col-blue" data-name="`+data[i].full_name+`" data-mobile="`+data[i].mobile+`" value="`+data[i].id+`" /><label for="choosen_emp_`+data[i].id+`"></label></td>
									</tr>`;
						});
					}
					$('.caregiverSearchResults tbody').html(html);
				},
				error: function (error){
					console.log(error);
				}
			})
		});

		$('.content').on('input','.amtCal', function(){
			var id = $(this).data('visit-id');
			calculateTotal(id);
		});

		$('.content').on('click','.btnReset', function(){
			clearSearchForm();
		});

		$('.content').on('click', '.btnSelectStaff', function(){
			var id = $(this).attr('data-visit-id');

			var emp = $('input[name=choosen_emp]:checked').val();
			var name = $('input[name=choosen_emp]:checked').data('name');
			var mobile = $('input[name=choosen_emp]:checked').data('mobile');

			 if(typeof emp != 'undefined' && emp != ''){
				 $('#staffSearchModal').modal('hide');
				 $('#visitList_'+id+' #caregiver_id').val(emp);
				 $('#visitList_'+id+' #selectedStaffName_'+id).html('<div class="col-sm-2"><img src="/img/user.png" width="40" height="40" class="img-circle"></div><div class="col-sm-9">'+name+'<br>Mob: '+mobile+'</div>');
			 }else{
				 alert("Please select staff !");
			 }
		});

		//$('.date').inputmask('dd-mm-yyyy', { placeholder: '__-__-____' });
		/* $('.money-inr').inputmask('₹ 999999.99', { numericInput: true, rightAlign: false }); */
	});

	function initBlock()
	{
		$('.date').inputmask('dd-mm-yyyy', { placeholder: '__-__-____' });
	}

	function checkIfExistingCustomer()
	{
		var fname = $('#first_name').val();
		var lname = $('#last_name').val();
		var mobile = $('#mobile_number').val();
		var email = $('#email').val();
		var dob = $('#date_of_birth').val();

		if(fname != '' || lname != '' || mobile != '' || email != '' || dob != ''){
			$.ajax({
				url: '{{ route('case.check-for-existing-case') }}',
				type: 'POST',
				data: {f: fname, l: lname, m: mobile, e: email, d: dob, _token: '{{ csrf_token() }}'},
				success: function (data){
					//console.log(data);
					showSearchResults(data);
				},
				error: function (error){
					console.log(error);
				}
			});
		}
	}

	function showSearchResults(data)
	{
		html = '';
		if(data.length){
			$.each(data, function(i){
				html += `<tr>
							<td>`+(i+1)+`</td>
							<td>`+moment(data[i].created_at).format('DD-MM-YYYY')+`<br><small>`+moment(data[i].created_at).format('hh:mm A')+`</small></td>
							<td>`+data[i].careplan_name+`</td>
							<td>`+data[i].service+`</td>
							<td>`+data[i].medical_conditions+`</td>
							<td>`+data[i].medications+`</td>
							<td>`+data[i].visits+`</td>
							<td>`+data[i].status+`</td>
							<td class="text-center"><input name="choosen_case" type="radio" id="choosen_case_`+data[i].id+`" class="with-gap radio-col-blue" data-id="`+data[i].id+`" value="`+data[i].id+`" /><label for="choosen_case_`+data[i].id+`"></label></td>
						</tr>`;
			});

			$('.caseSearchResults tbody').html(html);

			$('#caseSearchModal').modal();
		}
	}

	function clearSearchForm()
	{
		$('#staffSearchModal #staffSearchForm #reset').trigger('click');
		$('#staffSearchModal #staffSearchForm #filter_specialization').selectpicker('val','0');
		$('#staffSearchModal #staffSearchForm #filter_experience').selectpicker('val','0');
		$('#staffSearchModal .caregiverSearchResults tbody').html('<tr><td class="text-center" colspan="7">Click \'Search\' to getresults</td></tr>');
	}

	var visitListArray = [];

	function saveCase()
	{
		// Parse Patient Details
		var patientDetails = [];
		var fname = $('#first_name').val();
		var lname = $('#last_name').val();
		var patientAge = $('#patient_age').val();
		var patientWeight = $('#patient_weight').val();
		var gender = $('#gender option:selected').val();

		var address = $('#street_address').val();
		var area = $('#area').val();
		var city = $('#city').val();
		var zip = $('#zipcode').val();
		var state = $('#state').val();
		var country = $('#country').val();
		var locationCoordinates = $('#location_coordinates').val();

		// Parse Enquirer Details
		var enquirerName = $('#enquirer_name').val();
		var enquirerPhone = $('#enquirer_phone').val();
		var alternateNumber = $('#alternate_number').val();
		var enquirerEmail = $('#enquirer_email').val();
		var relationship = $('#relationship_with_patient option:selected').val();

		patientDetails = {
			'first_name': fname,
			'last_name': lname,
			'gender': gender,
			'patient_age': patientAge,
			'patient_weight': patientWeight,
			'street_address': address,
			'area': area,
			'city': city,
			'zipcode': zip,
			'state': state,
			'country': country,
			'location_coordinates': locationCoordinates,
			'enquirer_name': enquirerName,
			'enquirer_phone': enquirerPhone,
			'alternate_number': alternateNumber,
			'enquirer_email': enquirerEmail,
			'relationship_with_patient': relationship
		};


		// Parse Care Plan Details
		var carePlanDetails = [];
		var careplanName = $('#careplan_name').val();
		var careplanDescription = $('#careplan_description').val();
		var medicalConditions = $('#medical_conditions').val();
		var medications = $('#medications').val();

		@if(count($branches))
		var branchID = $('#branch_id option:selected').val();
		@else
		var branchID = $('#branch_id').val();
		@endif
		var serviceRequired = $('#service_id option:selected').val();
		var serviceRequired = $('#service_id option:selected').val();
		var noOfHours = $('#no_of_hours').val();
		var caseCharges = $('#case_charges').val();
		var genderPreference = $('input[name=gender_preference]:checked').val();
		var languagePreference = [];
		$(".language-preference input[type=checkbox]:checked").map(function(){
			languagePreference.push($(this).val());
		});
		var source = $('#source option:selected').val();
		var referrerName = $('#referrer_name').val();

		carePlanDetails = {
			'careplan_name': careplanName,
			'careplan_description': careplanDescription,
			'medical_conditions': medicalConditions,
			'medications': medications,
			'branch_id': branchID,
			'service_id': serviceRequired,
			'no_of_hours': noOfHours,
			'case_charges': caseCharges,
			'gender_preference': genderPreference,
			'language_preference': languagePreference.toString(),
			'source': source,
			'referrer_name': referrerName
		};


		// Parse all Visit List
		visitListArray = [];
		if($('.visitListCard').length){
			$('.visitListCard').each(function(){
				var id = $(this).attr('id');
				if(id != 'visitListTemplate'){
					var n = id.split("_")[1];
					var block = '#scheduling_tab #visit_list_pane #visitList_'+n;

					var startDate = $(block+ ' #start_date').val();
					var endDate = $(block+ ' #end_date').val();
					var notes = $(block+ ' #notes').val();
					var caregiverID = $(block+ ' #caregiver_id').val();

					visitListArray.push({
						'start_date': startDate,
						'end_date': endDate,
						'notes': notes,
						'caregiver_id': caregiverID,
					});
				}
			});
		}

		if(patientDetails && carePlanDetails){
			$.ajax({
				url: '{{ route('case.save-case') }}',
				type: 'POST',
				data: {patient: patientDetails, careplan: carePlanDetails, visits: visitListArray,_token: '{{csrf_token()}}'},
				success: function (data){
					// console.log(data);
					window.location.href = '{{ route('case.index') }}';
				},
				error: function (error){
					// console.log(error);
				}
			});
		}else{
			showNotification('bg-red', 'Please enter case details!', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
		}
		return false;
	}

	function validator() {
		var fname = $('#first_name').val();
		var gender = $('#gender option:selected').val();
		var patientAge = $('#patient_age').val();
		var address = $('#street_address').val();
		var city = $('#city').val();
		var state = $('#state').val();
		var enquirerName = $('#enquirer_name').val();
		var enquirerPhone = $('#enquirer_phone').val();
		var medicalConditions = $('#medical_conditions').val();
		var medications = $('#medications').val();
		var serviceRequired = $('#service_id option:selected').val();
		var noOfHours = $('#no_of_hours').val();

		if (fname == ""){
			alert("Please Enter First Name");
			$("label[for='"+$('#first_name').attr('id')+"']").addClass('required');
			document.getElementById("first_name").focus();

			return false;
		}
		if (gender == ""){
			alert("Please Enter Gender");
			$("label[for='"+$('#gender').attr('id')+"']").addClass('required');
			document.getElementById("gender").focus();

			return false;
		}
		if (patientAge == ""){
			alert("Please Enter Patient's Age");
			$("label[for='"+$('#patient_age').attr('id')+"']").addClass('required');
			document.getElementById("patient_age").focus();

			return false;
		}
		if (address == ""){
			alert("Please Enter Address");
			$("label[for='"+$('#street_address').attr('id')+"']").addClass('required');
			document.getElementById("street_address").focus();

			return false;
		}
		if (city == ""){
			alert("Please Enter City");
			$("label[for='"+$('#city').attr('id')+"']").addClass('required');
			document.getElementById("city").focus();

			return false;
		}
		if (state == ""){
			alert("Please Enter State");
			$("label[for='"+$('#state').attr('id')+"']").addClass('required');
			document.getElementById("state").focus();

			return false;
		}
		if (enquirerName == ""){
			alert("Please Enter Enquirer Name");
			$("label[for='"+$('#enquirer_name').attr('id')+"']").addClass('required');
			document.getElementById("enquirer_name").focus();

			return false;
		}
		if (enquirerPhone == ""){
			alert("Please Enter Enquirer Phone");
			$("label[for='"+$('#enquirer_phone').attr('id')+"']").addClass('required');
			document.getElementById("enquirer_phone").focus();

			return false;
		}
		if (medicalConditions == ""){
			alert("Please Enter Medical Condition");
			$("label[for='"+$('#medical_conditions').attr('id')+"']").addClass('required');
			document.getElementById("medical_conditions").focus();

			return false;
		}
		if (medications == ""){
			alert("Please Enter Medications");
			$("label[for='"+$('#medications').attr('id')+"']").addClass('required');
			document.getElementById("medications").focus();

			return false;
		}
		if (serviceRequired == ""){
			alert("Please Enter Service Required");
			$("label[for='"+$('#service_id').attr('id')+"']").addClass('required');
			document.getElementById("service_id").focus();

			return false;
		}
	    if (noOfHours == ""){
	    	alert("Please Enter no. of Hours");
	    	$("label[for='"+$('#no_of_hours').attr('id')+"']").addClass('required');
			document.getElementById("no_of_hours").focus();

	    	return false;
	    }
	    saveCase();
	}
</script>
@endsection
