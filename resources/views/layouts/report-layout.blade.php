@extends('layouts.main-layout')

@section('page.styles')
<style>
    .card{
        overflow-x: scroll;
    }
    .table-bordered tbody tr th {
        text-align: center;
    }
    .dt-buttons{
        position: static !important;
        float: right;
        margin-top: 15px;
    }
    .paginate_button {
        cursor: pointer;
        margin:2px;
    }
    div.dataTables_wrapper div.dataTables_filter {
        text-align: center !important;
    }
    table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td {
      text-transform: uppercase; !important;
      overflow: hidden;
      text-overflow: ellipsis;
      /*white-space: nowrap;*/
    }
    .report-filter{
        width: 95%;
        margin:auto 0 !important;
        background:#f6f6f6;
        border-radius:4px;
        border:1px solid #ccc;
        padding: 10px;
        margin-top:10px !important;
        margin-bottom:10px !important;
        min-height:50px;
        margin-left: 2.5% !important;
    }
</style>
<!-- JQuery DataTable Css -->
<link href="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endsection


@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card" style="overflow:visible">
                <div class="header clearfix">
                    <a href="{{ route('reports.index') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                        <i class="material-icons">arrow_back</i>
                    </a>
                    <h2 class="col-sm-10">
                       @yield('report-title')
                       <small>@yield('report-summary')</small>
                   </h2>
               </div>
               @yield('report-filter')
               @yield('report-content')
           </div>
       </div>
   </div>
@endsection

@section('page.scripts')
<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
<script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>

@stack('report.scripts')
@endsection
