<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
	@media only screen and (max-width: 500px) {
		#orgname{
			font-size: 12px !important;
		}
		#header{
			min-height: 50px !important;
		}
		#maincontent{
			padding: 0;
		}
		.stats{
			width: 49% !important;
		}
		.stats>h5{
			font-size: 150% !important;
		}
	}

	</style>
	<title></title>
</head>
<body style="background: #e2e1e0; font-family: 'Roboto', sans-serif !important;">
	<div style="display:block; width: 95%; margin: 0 auto; background: #fff; border-radius: 2px; box-shadow: 0 10px 20px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);">
		<!-- Header -->
		<div id="header" style="display: block; max-width: 99%; min-height: 80px; border-bottom: 3px solid #fa6423; padding: 10px 10px 5px 10px; ">
			<div style="text-align:center;">
				<div  id="orgname" style="font-size: 28px; padding-left: 15px; font-weight: 300; height: 45px; color: #009688; line-height: 45px; vertical-align: middle; margin-right: 10px; letter-spacing: 1px">{{ $data['organization_name'] }}</div>
			</div>
		</div>

		<!-- Main Content -->
		<div id="maincontent" style="min-height: 200px; padding: 10px 15px; font-size: 14px; line-height: 22px; text-align: justify; margin: 10px 0; border-bottom: 1px solid #E0E0E0">
			@yield('content')
		</div>

		<!-- Footer -->
		<div style="display: block; position:relative; bottom: 0; max-width: 99%; min-height: 90px; padding: 5px 10px 0px 10px; ">
			<div style="width: 49%; display: inline-block; font-size: 12px;">
				<span>Copyright &copy; {{ date('Y')}} <br> {{ $data['organization_name'] }} <br> All Rights Reserved</span><br><br>
				<span style="font-size: 11px; font-weight: 600">
					<a href="" style="color: #2899dc !important">Terms and Conditions</a> |
					<a href="" style="color: #2899dc !important">Privacy Policy</a>
				</span>
			</div>
			<div style="width: 49%; display: inline-block; text-align: right; vertical-align: top; font-size: 14px">
				<p style="margin-bottom: 10px;margin-top:0;padding-top:0">Powered by</p>
				<b><small>Smart Health Connect</small></b>s
			</div>
			<br><br>
		</div>
	</div>
</body>
</html>
