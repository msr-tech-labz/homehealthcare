<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title id='notificationsTitleCount'>
    @yield('page_title') Smart Health Connect
    </title>
    <!-- Favicon-->
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <meta name="csrf_token" content="{{ csrf_token() }}">
    <meta name="robots" content="noindex" />
    <meta name="robots" content="nofollow" />
    <meta name="robots" content="noarchive" />
    <meta name="googlebot" content="nosnippet" />
    <meta name="robots" content="noodp" />
    <meta name="slurp" content="noydir">
    <meta name="robots" content="noimageindex,nomediaindex" />

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <link href="{{ ViewHelper::ThemePlugin('bootstrap-tour/bootstrap-tour-standalone.css') }}" rel="stylesheet">

    <!-- Bootstrap Core Css -->
    <link href="{{ ViewHelper::ThemePlugin('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ ViewHelper::ThemePlugin('node-waves/waves.css') }}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ ViewHelper::ThemePlugin('animate-css/animate.css') }}" rel="stylesheet" />

    <!-- Preloader Css -->
    <link href="{{ ViewHelper::ThemePlugin('material-design-preloader/md-preloader.css') }}" rel="stylesheet" />

    <!--WaitMe Css-->
    <link href="{{ViewHelper::ThemePlugin('waitme/waitMe.css') }}" rel="stylesheet" />

    @yield('plugin.styles')

    <!-- Custom Css -->
    <link href="{{ ViewHelper::ThemeCss('style.css') }}" rel="stylesheet">

    <!-- You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ ViewHelper::ThemeCss('themes/all-themes.css') }}" rel="stylesheet" />


    @yield('page.styles')
    <style>
    /* New Colors *//*
        Text: #1565C0;
        Navbar BG: #009688      (old)#c47b02;
        BG: linear-gradient(to top, #20002c, #cbb4d4);
    */
        .block-header h2{
            color: #fff !important;
        }
        ::-webkit-input-placeholder {
           font-style: italic;
        }
        .top-org-logo{
          width: auto !important;
          max-width: 15% !important;
        }
        .block-header h2{
            font-size: 20px;
        }
        .top-org-logo > img{
            max-height: 70px;
            padding: 2px;
            width: auto;
            height: 60px;
            margin-top: 5px;
            border-radius: 5px;
            box-shadow: 3px 3px;
            background: #fff;
        }
        img.img-topbar{ margin-top: -5px !important;}
        .username:after{
            content: ' \25BE';
            padding-left: 10px;
            font-size: 20px;
        }
        .user-info{ background: rgba(96, 125, 139, 0.10) !important; border-bottom: 0.1em solid #3F51B5 !important; padding: 10px !important;}
        .quickCaseBtn:hover{
            color: #fff !important;
        }
        .modal .modal-body, .modal-footer
        {
            background: #fff !important;
        }
        div.dataTables_wrapper div.dataTables_filter input {
            border:2px solid black;
            padding: 1px;
        }
        @media only screen and (max-width: 830px) {
          .top-org-logo{
            display: none;
          }
        }
        .restricted-access{
            font-size: 18px;
            font-weight: normal;
            color: #ff0000 !important;
        }
        #google_translate_element{
            display: block;
        }
        .goog-te-gadget .goog-te-combo{
            margin: 0px !important;
        }
        body > .skiptranslate{
            display: none !important;
            visibility: hidden !important;
            height: 0 !important;
        }
        html, body {
            /* Page Break Isssue due to the below commented code. Uncomment in case any other view fails to load properly */
            /*position: fixed !important;*/
            /*height: 100% !important;*/
            top: 0 !important;
            bottom: 0 !important;
            left: 0 !important;
            right: 0 !important;
            margin: 0 !important;
            padding: 0 !important;
            overflow-y: overlay !important;
        }
        .table-responsive {
            overflow-x: hidden;
        }
        thead{
            border-top: 4px solid #aabcfe;
            border-bottom: 1px solid #fff;
            padding: 8px;
            font-size: 1em;
            font-weight: 600;
            color: #555;
            /*background: #f6f6f6;*/
        }
        tbody td {
            text-transform: uppercase;
            border-bottom: 1px solid #fff;
            color: #669;
            border-top: 1px solid transparent;
            padding: 8px;
        }
        .info-box{
            margin-bottom: 3px !important;
        }
        .info-box .icon{
            width: 80px !important;
        }

        .info-box .icon i {
            color: #fff;
            font-size: 40px;
            line-height: 70px;
        }
        .info-box .content {
            margin: 8px !important;
        }
        .theme{
            background:#f1f6f6;
            overflow-y: hidden !important;
        }
        .block-header h2{
            color: #666 !important;
        }
        .card, .info-box {
            border: 1px solid #1dc8cd;
        }
        .btn-primary {
            background: linear-gradient(270deg, #1de099, #1dc8cd) !important;
        }
        .bg-blue,.btn-info, .btn-info:hover, .btn-info:active, .btn-info:focus,.bg-indigo {
            background: #1dc8cd !important;
        }
        .btn-danger, .btn-danger:hover, .btn-danger:active, .btn-danger:focus{
            background: #ff6b60 !important
        }
        /*End notification*/
    </style>
</head>
<body class="theme-indigo theme">
    <!-- Page Loader -->
    <div class="page-loader-wrapper hide">
        <div class="loader">
            <div class="md-preloader pl-size-md">
                <svg viewbox="0 0 75 75">
                    <circle cx="37.5" cy="37.5" r="33.5" class="pl-red" stroke-width="4" />
                </svg>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar hidden-print" style="background:#8835a7;">
        <div class="container-fluid">
            <div class="col-sm-3 col-lg-2 top-org-logo">
                <img src="{{ empty(session('corporate.logo'))?asset('img/smarthealthglobal.png'):asset('uploads/corporate/'.session('corporate.logo')) }}" title="{{ session('corporate.name') }}" alt="logo" style="max-height: 70px;" class="img-responsive">
            </div>
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                {{-- <a href="javascript:void(0);" class="bars"></a> --}}
                <a class="navbar-brand" href="{{ route('corporate.dashboard') }}"><span>{{ session('corporate.name') }}</span></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li style="margin-top: -5px;margin-right: 5px;"><a href="{{ route('corporate.dashboard') }}" id="tourHome"><img src="{{ asset('img/flaticons2/home.png') }}" style="height: 36px;width: 36px;" title="Home"></a></li>
                    <li style="margin-top: -5px;-right: 5px;"><a href="{{ route('corporate.settings') }}"><img src="{{ asset('img/flaticons2/settings.png') }}" style="height: 36px;width: 36px;" title="Administration" id="tourAdministration"></a></li>
                    <?php
                        if(session('corporate.gender') == 'Female'){
                            $userAvatar = asset('img/flaticons2/woman.png');
                        }elseif (session('corporate.gender') == 'Male') {
                            $userAvatar = asset('img/flaticons2/man.png');
                        }else{
                            $userAvatar = asset('img/flaticons2/user.png');
                        }
                    ?>
                    {? $hour = date('H');if ($hour > 16) {$greetings = "Good Evening";} elseif ($hour >=  12){$greetings = "Good Afternoon";} elseif ($hour < 12) {$greetings = "Good Morning";} ?}
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle username" data-toggle="dropdown" role="button"><img src="{{ $userAvatar }}" class="img-circle img-topbar" width="38" height="38" alt="User" />&nbsp;&nbsp;{{ $greetings.', '.session('corporate.user_name') }}</a>
                        <ul class="dropdown-menu" style="margin-top: 10px !important;">
                            <li>
                            <a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="material-icons">input</i>Sign Out</a>
                            </li>
                            <form id="logout-form" action="{{ url('/corporate/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <section class="content" style="margin: 100px 15px 0;">
        <div class="container-fluid">
            <span id="channel" data-channel="{{ session('dbName').session('tenant_id') }}"></span>
            @include('partials.alert')

            @yield('content')
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="{{ ViewHelper::ThemePlugin('jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ ViewHelper::ThemePlugin('bootstrap/js/bootstrap.js') }}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('node-waves/waves.js') }}"></script>

    <script src="{{ ViewHelper::ThemePlugin('bootstrap-tour/bootstrap-tour-standalone.js') }}"></script>

    @yield('plugin.scripts')

    <!-- Custom Js -->
    <script src="{{ ViewHelper::ThemeJs('admin.js') }}"></script>
    {{-- <script src="{{ ViewHelper::ThemeJs('pages/index.js') }}"></script> --}}

    <!-- Demo Js -->
    <script src="{{ ViewHelper::ThemeJs('demo.js') }}"></script>

    <!-- Wait Me Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('waitme/waitMe.js') }}"></script>

    @yield('page.scripts')

</body>
</html>