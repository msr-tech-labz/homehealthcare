<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>@yield('page_title') Smart Health Connect</title>
    <!-- Favicon-->
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">

    <!-- Bootstrap Core Css -->
    <link href="{{ ViewHelper::ThemePlugin('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    
    @yield('plugin.styles')

    <!-- Custom Css -->
    {{-- <link href="{{ ViewHelper::ThemeCss('style.css') }}" rel="stylesheet"> --}}
    
    <link rel="stylesheet" type="text/css" href="{{ asset('css/print.css') }}" media="print" />

    @yield('page.styles')
    <style>
        .block-header h2{
            color: #fff !important;
        }
        html, body {
            position: fixed !important;
            top: 0 !important;
            bottom: 0 !important;
            left: 0 !important;
            right: 0 !important;
            margin: 0 !important;
            padding: 0 !important;
            height: 100% !important;
            overflow-y: overlay !important;
        }
        .card{
            background: #fff;
            min-height: 50px;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
            position: relative;
            margin-bottom: 30px;
        }
        .card .header {
            color: #555;
            padding: 20px;
            position: relative;
            border-bottom: 1px solid rgba(204, 204, 204, 0.35);
        }
        .card .header h2 {
            margin: 0;
            font-size: 18px;
            font-weight: normal;
            color: #111;
        }
        .card .body {
            font-size: 14px;
            color: #555;
            padding: 10px;
        }
        /*table {
            display: inline;
        }
        .tr {
            display: table-row;
        }*/
        .theme{
            background: -moz-radial-gradient(at bottom,#427a93,#05264f,#041124);
            background: -o-radial-gradient(at bottom,#427a93,#05264f,#041124);
            background: radial-gradient(at bottom,#427a93,#05264f,#041124);
            background-color:  #072951;
        }        
    </style>
</head>
<body class="theme-indigo theme">
    
    <nav class="navbar hidden-print" style="background: #3F51B5 !important">
        <div class="container-fluid">
            @yield('block-header')  
        </div>
    </nav>
    
    <section class="content" style="margin: 30px 15px 0;">
        <div class="container-fluid">
            @yield('content')
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="{{ ViewHelper::ThemePlugin('jquery/jquery.min.js') }}"></script>
    
    <!-- Bootstrap Core Js -->
    <script src="{{ ViewHelper::ThemePlugin('bootstrap/js/bootstrap.js') }}"></script>

    @yield('plugin.scripts')    

    @yield('page.scripts')
</body>

</html>
