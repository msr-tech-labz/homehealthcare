<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>@yield('page_title') Smart Health Connect | Central Console</title>
    <link rel="icon" href="../favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link href="{{ ViewHelper::ThemePlugin('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ ViewHelper::ThemePlugin('node-waves/waves.css') }}" rel="stylesheet" />
    <link href="{{ ViewHelper::ThemePlugin('material-design-preloader/md-preloader.css') }}" rel="stylesheet" />
    @yield('plugin.styles')
    <link href="{{ ViewHelper::ThemeCss('style.css') }}" rel="stylesheet">
    <link href="{{ ViewHelper::ThemeCss('themes/theme-blue.css') }}" rel="stylesheet" />
    <link href="{{ ViewHelper::ThemePlugin('animate-css/animate.css') }}" rel="stylesheet" />
    @yield('page.styles')
</head>

<body class="theme-blue" id="translate-content">
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="md-preloader pl-size-md">
                <svg viewbox="0 0 75 75">
                    <circle cx="37.5" cy="37.5" r="33.5" class="pl-red" stroke-width="4" />
                </svg>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <div class="overlay"></div>
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand"  href="{{ route('console.dashboard') }}" >Smart Health Connect - Central Console</a>
            </div>
        </div>
    </nav>
    <section>
        <aside id="leftsidebar" class="sidebar">
            <div class="user-info">
                <div class="image">
                    <img src="{{ ViewHelper::ThemeImage('user.png') }}" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ session('console.admin_fname').' '.session('console.admin_lname') }}</div>
                    <div class="email">{{ session('console.admin_email') }}</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="{{ url('/admin/logout') }}"><i class="material-icons">input</i>Sign Out</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="menu">
                <ul class="list">
                    <li class="@yield('active_home')">
                        <a href="{{ url('admin/dashboard') }}">
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li class="@yield('active_subscribers')">
                        <a href="{{ route('console.corporates') }}">
                            <i class="material-icons">group</i>
                            <span>Corporates</span>
                        </a>
                    </li>
                    <!--<li class="@yield('active_billing')">
                        <a href="{{ route('console.billing') }}">
                            <i class="material-icons">credit_card</i>
                            <span>Billing</span>
                        </a>
                    </li>-->
                    <li class="@yield('active_users')">
                        <a href="{{ route('console.active_users') }}">
                            <i class="material-icons">people</i>
                            <span>Active Users</span>
                        </a>
                    </li>
                    <li class="@yield('active_reports')">
                        <a href="{{ route('console.reports') }}">
                            <i class="material-icons">insert_chart</i>
                            <span>Reports</span>
                        </a>
                    </li>
                    <li class="header">Utilities</li>
                    <li class="@yield('active_administration')">
                        <a href="{{ route('console.utilities.administration') }}">
                            <i class="material-icons">settings</i>
                            <span>Administration</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="legal text-center">
                <div class="copyright">
                     &copy; 2018-{{ date('y') }} <b>Smart Health Connect</b>
                </div>
                <div class="version">
                    <b>Version: </b> 2.5.1
                </div>
            </div>
        </aside>
    </section>

    <section class="content">
        <div class="container-fluid">
            @include('partials.alert')

            @yield('content')
        </div>
    </section>
    <script src="{{ ViewHelper::ThemePlugin('jquery/jquery.min.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('bootstrap/js/bootstrap.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('node-waves/waves.js') }}"></script>
    @yield('plugin.scripts')
    <script src="{{ ViewHelper::ThemeJs('admin.js') }}"></script>
    <script type="text/javascript">
        window.setTimeout(function() {
            $(".alert").fadeTo(500, 0).slideUp(500, function(){
                $(this).remove(); 
            });
        }, 4000);
    </script>
    @yield('page.scripts')
</body>
</html>
