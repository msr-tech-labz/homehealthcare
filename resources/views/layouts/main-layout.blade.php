<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title id='notificationsTitleCount'>
    @if(isset($notifications) && count($notifications))
        @if(count(collect($notifications)->where('status','Unread')))
        ({{ count(collect($notifications)->where('status','Unread')) }})
        @endif
    @endif
    @yield('page_title') Smart Health Connect
    </title>
    <!-- Favicon-->
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <meta name="csrf_token" content="{{ csrf_token() }}">
    <meta name="robots" content="noindex" />
    <meta name="robots" content="nofollow" />
    <meta name="robots" content="noarchive" />
    <meta name="googlebot" content="nosnippet" />
    <meta name="robots" content="noodp" />
    <meta name="slurp" content="noydir">
    <meta name="robots" content="noimageindex,nomediaindex" />

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <link href="{{ ViewHelper::ThemePlugin('bootstrap-tour/bootstrap-tour-standalone.css') }}" rel="stylesheet">

    <!-- Bootstrap Core Css -->
    <link href="{{ ViewHelper::ThemePlugin('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ ViewHelper::ThemePlugin('node-waves/waves.css') }}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ ViewHelper::ThemePlugin('animate-css/animate.css') }}" rel="stylesheet" />

    <!-- Preloader Css -->
    <link href="{{ ViewHelper::ThemePlugin('material-design-preloader/md-preloader.css') }}" rel="stylesheet" />

    <!--WaitMe Css-->
    <link href="{{ViewHelper::ThemePlugin('waitme/waitMe.css') }}" rel="stylesheet" />

    @yield('plugin.styles')

    <!-- Custom Css -->
    <link href="{{ ViewHelper::ThemeCss('style.css') }}" rel="stylesheet">

    <!-- You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ ViewHelper::ThemeCss('themes/all-themes.css') }}" rel="stylesheet" />


    @yield('page.styles')
    <style>
    /* New Colors *//*
        Text: #1565C0;
        Navbar BG: #009688      (old)#c47b02;
        BG: linear-gradient(to top, #20002c, #cbb4d4);
    */
        .block-header h2{
            color: #fff !important;
        }
        ::-webkit-input-placeholder {
           font-style: italic;
        }
        .top-org-logo{
          width: auto !important;
          max-width: 15% !important;
        }
        .block-header h2{
            font-size: 20px;
        }
        .top-org-logo > img{
            max-height: 70px;
            padding: 2px;
            width: auto;
            height: 60px;
            margin-top: 5px;
            border-radius: 5px;
            box-shadow: 3px 3px;
            background: #fff;
        }
        img.img-topbar{ margin-top: -5px !important;}
        .username:after{
            content: ' \25BE';
            padding-left: 10px;
            font-size: 20px;
        }
        .user-info{ background: rgba(96, 125, 139, 0.10) !important; border-bottom: 0.1em solid #3F51B5 !important; padding: 10px !important;}
        .quickCaseBtn:hover{
            color: #fff !important;
        }
        .modal .modal-body, .modal-footer
        {
            background: #fff !important;
        }
        div.dataTables_wrapper div.dataTables_filter input {
            border:2px solid black;
            padding: 1px;
        }
        @media only screen and (max-width: 830px) {
          .top-org-logo{
            display: none;
          }
        }
        .restricted-access{
            font-size: 18px;
            font-weight: normal;
            color: #ff0000 !important;
        }
        #google_translate_element{
            display: block;
        }
        .goog-te-gadget .goog-te-combo{
            margin: 0px !important;
        }
        body > .skiptranslate{
            display: none !important;
            visibility: hidden !important;
            height: 0 !important;
        }
        html, body {
            /* Page Break Isssue due to the below commented code. Uncomment in case any other view fails to load properly */
            /*position: fixed !important;*/
            /*height: 100% !important;*/
            top: 0 !important;
            bottom: 0 !important;
            left: 0 !important;
            right: 0 !important;
            margin: 0 !important;
            padding: 0 !important;
            overflow-y: overlay !important;
        }
        .table-responsive {
            overflow-x: hidden;
        }
        thead{
            border-top: 4px solid #aabcfe;
            border-bottom: 1px solid #fff;
            padding: 8px;
            font-size: 1em;
            font-weight: 600;
            color: #555;
            /*background: #f6f6f6;*/
        }
        tbody td {
            text-transform: uppercase;
            border-bottom: 1px solid #fff;
            color: #669;
            border-top: 1px solid transparent;
            padding: 8px;
        }
        /*Notificcations*/
        #notification_li
        {
        position:relative
        }
        #notificationContainer
        {
        background-color: #fff;
        border: 1px solid rgba(100, 100, 100, .4);
        -webkit-box-shadow: 0 3px 8px rgba(0, 0, 0, .25);
        overflow: visible;
        position: absolute;
        top: 75px;
        margin-left: -144px;
        width: 350px;
        z-index: -1;
        display: none;
        }

        #notificationContainer:before {
        content: '';
        display: block;
        position: absolute;
        width: 0;
        height: 0;
        color: transparent;
        border: 10px solid black;
        border-color: transparent transparent white;
        margin-top: -20px;
        margin-left: 163px;
        }

        #notificationTitle{
        font-weight: bold;
        padding: 7px;
        font-size: 13px;
        background-color: #ffffff;
        position: fixed;
        z-index: 1000;
        width: 348px;
        border-bottom: 1px solid #dddddd;
        }

        #notificationsBody{
        padding: 33px 0px 0px 0px !important;
        min-height:300px;
        }

        #notificationFooter
        {
        text-align: center;
        font-weight: bold;
        padding: 8px;
        font-size: 12px;
        border-top: 1px solid #dddddd;
        }

        #notification_count
        {
        padding: 3px 7px 3px 7px;
        background: #cc0000;
        color: #ffffff;
        font-weight: bold;
        margin-left: 30px;
        border-radius: 9px;
        -moz-border-radius: 9px;
        -webkit-border-radius: 9px;
        position: absolute;
        margin-top: 22px;
        font-size: 11px;
        z-index: 1;
        }
        .info-box{
            margin-bottom: 3px !important;
        }
        .info-box .icon{
            width: 80px !important;
        }

        .info-box .icon i {
            color: #fff;
            font-size: 40px;
            line-height: 70px;
        }
        .info-box .content {
            margin: 8px !important;
        }
        .theme{
            /* background: -moz-radial-gradient(at bottom,#427a93,#05264f,#041124);
            background: -o-radial-gradient(at bottom,#427a93,#05264f,#041124);
            background: radial-gradient(at bottom,#427a93,#05264f,#041124); */
            /* background: linear-gradient(45deg,#1de09914,#1dc8cd) !important; */
            /*background-attachment: fixed;*/
            background:#f1f6f6;
            overflow-y: hidden !important;
        }
        .block-header h2{
            color: #666 !important;
        }
        .card, .info-box {
            border: 1px solid #1dc8cd;
        }
        .btn-primary {
            background: linear-gradient(270deg, #1de099, #1dc8cd) !important;
        }
        .bg-blue,.btn-info, .btn-info:hover, .btn-info:active, .btn-info:focus,.bg-indigo {
            background: #1dc8cd !important;
        }
        .btn-danger, .btn-danger:hover, .btn-danger:active, .btn-danger:focus{
            background: #ff6b60 !important
        }
        /*End notification*/
        .show-tick .dropdown-menu.open {overflow:visible!important;}
    </style>
</head>
<body class="theme-indigo theme">
    <!-- Page Loader -->
    <div class="page-loader-wrapper hide">
        <div class="loader">
            <div class="md-preloader pl-size-md">
                <svg viewbox="0 0 75 75">
                    <circle cx="37.5" cy="37.5" r="33.5" class="pl-red" stroke-width="4" />
                </svg>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar hidden-print">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar hidden-print" style="background:#6222b9;">
        <div class="container-fluid">
            <div class="col-sm-3 col-lg-2 top-org-logo">
                <img src="{{ asset('uploads/provider/'.session('tenant_id').'/'.session('organization_logo')) }}" title="{{ session('organization_name') }}" alt="logo" style="max-height: 70px;" class="img-responsive">
            </div>
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                {{-- <a href="javascript:void(0);" class="bars"></a> --}}
                <a class="navbar-brand" href="{{ route('dashboard') }}"><span>{{ session('organization_name') }}</span></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                    <li style="margin-top: -5px;margin-right: 5px;"><a href="{{ route('dashboard') }}" id="tourHome"><img src="{{ asset('img/flaticons2/home.png') }}" style="height: 36px;width: 36px;" title="Home"></a></li>
                    <li style="margin-top: -5px;margin-right: 5px;" id="notification_li">
                        @if(isset($notifications) && count($notifications))
                            @if(count(collect($notifications)->where('status','Unread')))
                                <a href="#" id="notificationLink">
                                    <img src="{{ asset('img/flaticons2/notification-receive.gif') }}" style="height: 36px;width: 36px;" title="Notifications">
                                </a>
                            @endif
                        @else
                        <a href="#" id="notificationLink">
                            <img src="{{ asset('img/flaticons2/bell.png') }}" style="height: 36px;width: 36px;" title="Notifications">
                        </a>
                        @endif
                        <div id="notificationContainer">
                        <div id="notificationTitle">
                            <span style="float: left;">Notifications</span>
                        </div>
                        <div id="notificationsBody" class="notifications" style="max-height:325px;overflow-y: auto;">
                            @if(isset($notifications) && count($notifications))
                                <div class="caughtUpMsg" style="text-align: center;font-size: 25px;color: darkgrey;margin-top: 5%;display: none;">All Caught Up !</div>
                                <div class="caughtUpImage" style="text-align: center;display: none;">
                                <img style="height: 100px;width: 110px;margin-top: 10%;" src="{{ asset('img/flaticons2/notification-caught-up.png') }}">
                                </div>
                            @else
                                <div class="caughtUpMsg" style="text-align: center;font-size: 25px;color: darkgrey;margin-top: 5%;display: block;">All Caught Up !</div>
                                <div class="caughtUpImage" style="text-align: center;display: block;">
                                <img style="height: 100px;width: 110px;margin-top: 10%;" src="{{ asset('img/flaticons2/notification-caught-up.png') }}">
                                </div>
                            @endif
                                <div id="notificationsBodyList">
                                {{-- Pusher Notifictaions goes here --}}
                                @if(isset($notifications) && count($notifications))
                                    @foreach($notifications as $n)
                                    <div class="info-box notificationList" @if($n->status == 'Read') style="background:#8893d1" @else style="background:#6222b9" @endif style="max-height:70px;">
                                        <div class="content" style="padding:0px !important">
                                            <div class="text" style="margin-top:0px;color:#fff !important">{{ $n->message }}</div>
                                            <div class="text-right col-white" style="font-size: 12px">{{ $n->created_at->format('d-m-Y h:i A') }}</div>
                                        </div>
                                    </div>
                                    @endforeach
                                @endif
                                </div>
                        </div>
                        <div id="notificationFooter">
                            <a id="readNotifications" style="float: left" href="#">Mark all as Read</a>
                            <a id="clearNotifications" style="float: right" href="#">Clear all</a></div>
                        </div>
                    </li>
                    {{-- <li style="margin-top: -5px;margin-right: 5px;"><a href="javascript: void(0);" class="support-icon"><img src="{{ asset('img/flaticons2/support.png') }}" style="height: 36px;width: 36px;" title="Help Desk" id="tourHelp"></a></li> --}}
                    @role('admin')
                    <li style="margin-top: -5px;margin-right: 5px;"><a href="{{ route('subscription.index') }}"><img src="{{ asset('img/flaticons2/subscription.png') }}" style="height: 36px;width: 36px;" title="Subscription" id="tourSubscription"></a></li>
                    <li style="margin-top: -5px;-right: 5px;"><a href="{{ route('settings') }}"><img src="{{ asset('img/flaticons2/settings.png') }}" style="height: 36px;width: 36px;" title="Administration" id="tourAdministration"></a></li>
                    @endrole
                    <li style="margin-top: -5px;margin-right: 5px;"><a class="notranslate"><img src="{{ asset('img/flaticons2/translate.png') }}" style="height: 36px;width: 36px;" title="Translate" id="tourLanguage"></a></li>
                    <div id="google_translate_element" class="pull-left" style="display:none;margin-top: 20px !important;margin-right: 5px !important;"></div>
                    <?php
                        if(isset(Auth::user()->caregiver) && Auth::user()->caregiver->gender == 'Female'){
                            $userAvatar = asset('img/flaticons2/woman.png');
                        }elseif (isset(Auth::user()->caregiver) && Auth::user()->caregiver->gender == 'Male') {
                            $userAvatar = asset('img/flaticons2/man.png');
                        }else{
                            $userAvatar = asset('img/flaticons2/user.png');
                        }
                    ?>
                    {? $hour = date('H');if ($hour > 16) {$greetings = "Good Evening";} elseif ($hour >=  12){$greetings = "Good Afternoon";} elseif ($hour < 12) {$greetings = "Good Morning";} ?}
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle username" data-toggle="dropdown" role="button"><img src="{{ $userAvatar }}" class="img-circle img-topbar" width="38" height="38" alt="User" />&nbsp;&nbsp;{{ $greetings.', '.session('user_name') }}</a>
                        <ul class="dropdown-menu" style="margin-top: 10px !important;">
                            <li><a href="{{ route('profile') }}"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li>
                            <a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="material-icons">input</i>Sign Out</a>
                            </li>
                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <section class="content" style="margin: 100px 15px 0;">
        <div class="container-fluid">
            <span id="channel" data-channel="{{ session('dbName').session('tenant_id') }}"></span>
            @include('partials.alert')

            @yield('content')
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="{{ ViewHelper::ThemePlugin('jquery/jquery.min.js') }}"></script>
    <script>
        $(function(){
            $('.material-icons').addClass('notranslate');

            // //setContentMargin($('.goog-te-combo option:selected').val(), false);
            //
            // $('html').on('change','.goog-te-combo',function(){
            //     //setContentMargin($(this).val(), true);
            // });
        });

        // function setContentMargin(lang, reload){
        //     if(typeof lang == 'undefined'){
        //         lang = $('.goog-te-combo option:selected').text();
        //     }
        //     console.log(lang);
        //     if(lang == 'en' || lang == 'English' || lang == 'Selected' || lang == ''){
        //         $('.content').css('margin-top','100px');
        //         if(reload) location.reload();
        //     }else{
        //         $('.content').css('margin-top','60px');
        //     }
        // }
    </script>

    <!-- Bootstrap Core Js -->
    <script src="{{ ViewHelper::ThemePlugin('bootstrap/js/bootstrap.js') }}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('node-waves/waves.js') }}"></script>

    <script src="{{ ViewHelper::ThemePlugin('bootstrap-tour/bootstrap-tour-standalone.js') }}"></script>

    @yield('plugin.scripts')

    <!-- Custom Js -->
    <script src="{{ ViewHelper::ThemeJs('admin.js') }}"></script>
    {{-- <script src="{{ ViewHelper::ThemeJs('pages/index.js') }}"></script> --}}

    <!-- Demo Js -->
    <script src="{{ ViewHelper::ThemeJs('demo.js') }}"></script>

    <!-- Wait Me Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('waitme/waitMe.js') }}"></script>

    @yield('page.scripts')

    <script type="text/javascript">
        window.setTimeout(function() {
            $(".alert").fadeTo(500, 0).slideUp(500, function(){
                $(this).remove(); 
            });
        }, 4000);

        $(function(){
            $( document ).on( 'focus', ':input', function(){
                $( this ).attr( 'autocomplete', 'off' );
            });
            $(document).on('submit', 'form', function(e){
                //$(this).append('<input type="hidden" name="_token" value="'+$('[name="csrf_token"]').attr('content')+'">');
            });

            $('.content').on('click','a.otherRequests', function(){
                $('#otherRequestsModal').modal();
            });
        });

        function validate()
        {
            $('#otherRequestForm #lead_id').val();
            return true;
        }
    </script>

    <script type="text/javascript">
        function googleTranslateElementInit() {
            new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
        }
        $(window).load(function(){
            $(".goog-te-combo").addClass('form-control');
            $(".goog-logo-link").empty();
            $('.goog-te-gadget').html($('.goog-te-gadget').children());
        })
    </script>

    <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
    <script type="text/javascript">
        $("#tourLanguage").click(function(){
            $('#google_translate_element').animate({
                width: "toggle",
                opacity: "toggle"
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function()
        {
            $("#notificationLink").click(function()
            {
                $("#notificationContainer").fadeToggle(300);
                $("#notification_count").fadeOut("slow");
                return false;
            });

        //Document Click hiding the popup
        $(document).click(function()
        {
            $("#notificationContainer").hide();
        });

        //Popup on click
        $("#notificationContainer").click(function()
        {
            return false;
        });

    });
</script>
<script src="https://js.pusher.com/4.2/pusher.min.js"></script>
<!-- Bootstrap Notify Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-notify/bootstrap-notify.js') }}"></script>
<script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
<script>
    //Remember to replace key and cluster with your credentials.
    var pusher = new Pusher('a855b4a7218515da56af', {
        cluster: 'ap2',
        encrypted: true
    });
    var room_channel = $('#channel').data('channel');

    //Also remember to change channel and event name if your's are different.
    var channel      = pusher.subscribe(room_channel+'notify');
    channel.bind('notify-event', function(message) {
    $('.caughtUpMsg').hide();
    $('.caughtUpImage').hide();
    showNotification('bg-green', 'You have a new Notification.Please Check in the Notification Panel', 'top', 'center', 'animated bounceInDown');
    html = `<div class="info-box notificationList" style="max-height:70px;background:#6222b9;">
                <div class="content" style="padding:0px !important">
                    <div class="text" style="margin-top:0px;color:#fff !important">`+message[0]+`</div>
                    <div class="text-right col-white" style="font-size: 12px">`+message[1]+' '+message[2]+`</div>
                </div>
            </div>`;

    $('#notificationsBodyList').append(html);

    var title = document.title.replace(/\d+|\(|\)/gm, '');
    count = message[3];
    var newTitle = '(' + count + ') ' + title;
    document.title = newTitle;
    $('#notificationLink > img').attr('src','{{ asset('img/flaticons2/notification-receive.gif') }}');
    });
</script>
<script type="text/javascript">
    $('#readNotifications').click(function(event){
        $('#notificationLink > img').attr('src','{{ asset('img/flaticons2/bell.png') }}');
        $.ajax({
            url: '{{ route('ajax.readNotifications') }}',
            type: 'GET',
            success: function (){
                {{ isset($notifications)?$notifications == null:'' }};
                $('.notificationList').css('background','#8893d1');
            },
            error: function (error){
                console.log(error);
            }
        });
       document.title = document.title.replace(/\d+|\(|\)/gm, '');
    });

    $('#clearNotifications').click(function(event){
        $('#notificationLink > img').attr('src','{{ asset('img/flaticons2/bell.png') }}');
        $('#notificationsBodyList').html('');
        $('#notificationsBody > img').attr('src','{{ asset('img/flaticons2/notification-caught-up.png') }}');
        $('.caughtUpMsg').show();
        $('.caughtUpImage').show();
        $.ajax({
            url: '{{ route('ajax.clearNotifications') }}',
            type: 'GET',
            success: function (){
                {{ isset($notifications)?$notifications == null:'' }};
            },
            error: function (error){
                console.log(error);
            }
        });
       document.title = document.title.replace(/\d+|\(|\)/gm, '');
    });
</script>
</body>

</html>
