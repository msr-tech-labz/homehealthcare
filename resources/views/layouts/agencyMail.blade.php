<!DOCTYPE html>
<html>
<head>
	<style>
	@media (max-width: 360px) {
		.mailOrg{
			font-size: 20px !important;
		}
	}
	</style>
	<title></title>
</head>
<body style="font-family: 'Roboto', sans-serif !important;">
	<div style="display:block; margin: 0 auto; border-radius: 2px;background: aliceblue;">
		<!-- Header -->
		<div style="display: block; max-width: 99%; min-height: 50px; border-bottom: 1px solid #E0E0E0; padding: 10px 10px 5px 10px; ">
			<div style="display: inline-block; float:left;">
				<div class="mailOrg" style="font-size: 28px; padding-left: 15px; font-weight: 300; height: 45px; color: #009688; line-height: 45px; vertical-align: middle; margin-right: 10px; letter-spacing: 1px">{{ $caregiver->provider->organization_name }}</div>
			</div>
		</div>

		<!-- Main Content -->
		<div style="min-height: 200px; padding: 10px 15px; font-size: 14px; line-height: 22px; text-align: justify; margin: 10px 0; border-bottom: 1px solid #E0E0E0">
			<span style="font-size: 16px; font-weight: 500; margin-bottom: 20px; display: block;">Greetings,</span>

			@yield('content')
            <table>
                <tr>
                    <td>
                        <img src="{{ asset('img/provider/'.session('organization_logo')) }}" style="max-height: 110px; margin: 5px 0 0 10px" title="{{ session('orgnization_name') }}">
                    </td>
                    <td width="4%"></td>
                    <td style="vertical-align: top; font-size: 13px !important;width: 100%;">
                        <span style="font-size: 14px !important;font-weight: bold">{{ session('org_name') }}</span><br>
                        {{ session('organization_address') }}<br>
                        {{ session('organization_city') }} - {{ session('organization_zipcode') }}<br>
                        {{ session('organization_state') }}, {{ session('organization_country') }}<br><br>
                        Phone: {{ session('phone_number') }}
                    </td>
                </tr>
            </table>
            <div style="text-align:center;background-color:#2899dc;min-height:50px;padding:5px;line-height:50px;font-size:14px;text-transform:uppercase;font-weight:bold;color:#fff">Connecting Your Dear Ones With Best-In-Class Healthcare Providers</div>
		</div>

		<!-- Footer -->
		<div style="display: block; position:relative; bottom: 0; max-width: 99%; min-height: 90px; padding: 5px 10px 0px 10px; ">
			<div style="font-size: 14px;display: inline-block;float: left;">
				<span>Copyright &copy; {{date('Y')}} {{ session('organization_name') }} All Rights Reserved</span><br><br>
				<span style="font-size: 12px; font-weight: 600">
					<a href="" style="color: #2899dc !important">Terms and Conditions</a> |
					<a href="" style="color: #2899dc !important">Privacy Policy</a>
				</span>
			</div>
			<div style="text-align: right;font-size: 14px;float: right;">
				<p style="margin-bottom: 10px;margin-top:0;padding-top:0">Powered by</p>
				<b><small>Smart Health Connect</small></b>
				{{-- <img src="https://portal.apnacare.in/img/provider/d0p4M1paYXJpU3pZeEN4dXlkUHQ2QT09.png" alt="{{ session('orgnization_name') }}" title="{{ session('orgnization_name') }}" style="width: 120px"/> --}}
			</div>
		</div>
	</div>
</body>
</html>
