@extends('layouts.main-layout')

@section('page_title','Import Employees - ')
@section('active_caregivers','active')

@section('page.styles')
    <style>
        .card .header{
            padding: 10px;
        }
        .table tr th{
            text-align: center;
            vertical-align: middle !important;
        }
    </style>
@endsection

@section('content')
<div class="row clearfix">
    <div class="col-sm-6" style="padding-left: 0">
        <div class="card">
            <div class="header clearfix">
                <h2 class="col-sm-6">
                    Sample Excel File
                </h2>
            </div>
            <div class="body text-center" style="height: 120px;">
                <a class="btn bg-teal" href="{{ asset('documents/samples/Sample_Employee_CSV.csv') }}">Download File</a>
                <div class="clearfix"></div><br>
                <span>
                    <b>Note:</b> <i class="text-red">Do not modify/alter columns, keep the fields blank if not applicable</i>
                </span>
            </div>
        </div>
    </div>

    <div class="col-sm-6" style="padding-right: 0">
        <div class="card">
            <div class="header clearfix">
                <h2 class="col-sm-6">
                    Upload Excel File
                </h2>
            </div>
            <div class="body text-center" style="height: 120px;">
                <form id="import-form" action="{{ route('employee.bulk-import') }}" method="POST" enctype="multipart/form-data">
					<div class="col-md-6 text-center">
						<input type="file" name="excel_file" class="form-control" required="">
					</div>
					<div class="col-md-4 text-center">
						<button type="submit" class="btn btn-block btn-primary" href="">Upload File</button>
					</div>
					<div class="clearfix"></div>
                    <span>
                        <b>Note:</b> <i class="text-red">Do not modify/alter columns, keep the fields blank if not applicable</i>
                    </span>
					{{ csrf_field() }}
				</form>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-sm-6" style="padding-left: 0">
        <div class="card">
            <div class="header clearfix">
                <h2 class="col-sm-12">
                    Things to do before uploading the excel file
                </h2>
            </div>
            <div class="body text-left" style="height: 250px;">
                <div class="clearfix"></div>
                <span>
                    <i class="text-red">
                    1- Download the sample excel file.<br>
                    2- Add the required field details as given in the sample file.<br>
                    3- Remove the top sample example record(s) in the file before uploading the file.<br>
                    4- Email Address field is mandatory for each record.<br>
                    5- Date Format that has to be used is DD-MMM-YYYY (eg. 20-Mar-1992).<br>
                    6- If a field has to be left empty,please keep it blank,do not add - , NA, Not Available,etc.<br>
                    7- Role Field should be "Caregiver" or "Manager".<br>
                    8- If there is any field which demands an input of years, then only numeric value has to be given (eg. Experience(in years) field should have 9 as a value, and not 9 years or 9 yrs).<br>
                    9- Save the file.<br>
                    10- Choose File and Click Upload.<br>
                    </i>
                </span><br>
            </div>
        </div>
    </div>

    <div class="col-sm-6" style="padding-right: 0">
        <div class="card">
            <div class="header clearfix">
                <h2 class="col-sm-12">
                    Things to do after uploading the excel file
                </h2>
            </div>
            <div class="body text-left" style="height: 250px;">
            <div class="clearfix"></div>
            <span>
                <i class="text-red">
                    1- Go to Employees from dashboard.<br>
                    2- Click on the Edit Button for the required Employees.<br>
                    3- Provide a password for the Employees.<b>(IMPORTANT)</b><br>
                    4- Choose the specialization (required), profile photo(.jpeg /.jpg /.png format only), Skills and proficiency set.<br>
                    5- Update any other fields as per requirement.<br>
                    6- Upload documents if available and necessary.<br>
                </i>
            </span><br>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix hide">
    <div class="card">
        <div class="header clearfix">
            <h2 class="col-md-8">Employees To Import</h2>
        </div>
        <div class="body">
            <div class="table-responsive">
                <table class="hide table table-bordered table-striped" style="overflow: auto">
					<thead>
						<tr>
							<th><label><input type="checkbox" class="check_all" name="employee_all"></label></th>
							<th width="5%">#</th>
							<th>Employee ID</th>
							<th>Type</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>DOB</th>
							<th>Gender</th>
							<th>Languages Known</th>
							<th>Mobile</th>
							<th>Email</th>
							<th>Current Address</th>
							<th>Permanent Address</th>
							<th>City</th>
							<th>State</th>
							<th>Designation</th>
							<th>Experience</th>
					</thead>
					<tbody>
                    @forelse ($caregivers as $c)
                        <tr>
                            <td></td>
                        </tr>
                    @empty
                        <tr>
                            <td></td>
                        </tr>
                    @endforelse
                    </tbody>
				</table>
            </div>
        </div>
    </div>
</div>
@endsection
