@extends('layouts.main-layout')

@section('page_title','Profile Management |')

@section('active_profilemanagement','active')

@section('plugin.styles')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<style>
    .uploaded-documents>tbody>tr>th{
        text-align: center;
    }
    .uploaded-documents {
        counter-reset: rowNumber;
    }

    .uploaded-documents tr:not(:first-child) {
        counter-increment: rowNumber;
    }

    .uploaded-documents tr td:not(.empty):first-child::before {
        content: counter(rowNumber);
        min-width: 1em;
        margin-right: 0.5em;
    }
</style>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="row clearfix">
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif

@if (Session::has('message'))
    <li class="alert alert-info">{!! session('message') !!}</li>
@endif

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="body">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist" style="margin-top: -20px;">
                <li role="presentation" class="active">
                    <a href="#basicdetails" data-toggle="tab" aria-expanded="true">
                        <i class="material-icons">subject</i> BASIC DETAILS
                    </a>
                </li>
                <li role="presentation" class="">
                    <a href="#professionaldetails" data-toggle="tab" aria-expanded="false">
                        <i class="material-icons">assignment_turned_in</i> PROFESSIONAL DETAILS
                    </a>
                </li>
                <li role="presentation" class="">
                    <a href="#timings" data-toggle="tab" aria-expanded="false">
                        <i class="material-icons">access_time</i> TIMINGS
                    </a>
                </li>
                <li role="presentation" class="">
                    <a href="#uploaddocuments" data-toggle="tab" aria-expanded="false">
                        <i class="material-icons">cloud_upload</i> UPLOAD DOCUMENTS
                    </a>
                </li>
                <li role="presentation" class="">
                    <a href="#verification" data-toggle="tab" aria-expanded="false">
                        <i class="material-icons">verified_user</i> VERIFICATION
                    </a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="basicdetails">
                    <b>Edit Personal Details</b>
                    <form class="form-horizontal" method="POST" action="{{ route('update-personal-details') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="col-lg-6">
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="first_name">First Name</label>
                                </div>
                                <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="first_name" id="first_name" class="form-control" placeholder="Nitish" value="{{isset($caregiver)?$caregiver->first_name:''}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="last_name">Last Name</label>
                                </div>
                                <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Patra" value="{{isset($caregiver)?$caregiver->last_name:''}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="mobile">Mobile</label>
                                </div>
                                <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="number" name="mobile_number" id="mobile_number" class="form-control" placeholder="9668684048" value="{{isset($caregiver)?$caregiver->mobile_number:''}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="email">Email</label>
                                </div>
                                <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="email" name="email" id="email" class="form-control" placeholder="nitish@apnacare.in" value="{{isset($caregiver)?$caregiver->email:''}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="age">Date of Birth</label>
                                </div>
                                <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                        <input type="text" name="date_of_birth" id="date_of_birth" class="birthdate form-control" placeholder="24" value="{{isset($caregiver)?$caregiver->date_of_birth:''}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="city">Current City</label>
                                </div>
                                <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="current_city" id="current_city" class="form-control" placeholder="Bengaluru" value="{{isset($caregiver)?$caregiver->current_city:''}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="state">Current State</label>
                                </div>
                                <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="current_state" id="current_state" class="form-control" placeholder="Karnataka" value="{{isset($caregiver)?$caregiver->current_state:''}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="address">Current Address</label>
                                </div>
                                <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="current_address" id="current_address" class="form-control" placeholder="Sahakar Nagar" value="{{isset($caregiver)?$caregiver->current_address:''}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="gender">Gender</label>
                                </div>
                                <div class="form-group">
                                    <input type="radio" name="gender" id="male" class="with-gap" <?php if( $caregiver->gender == "Male") { echo 'checked="checked"'; } ?> value="Male" >
                                    <label style="margin-top: 5px;" for="male">Male</label>
                                    <input type="radio" name="gender" id="female" class="with-gap" <?php if( $caregiver->gender == "Female") { echo 'checked="checked"'; } ?> value="Female" >
                                    <label style="margin-top: 5px;" for="female" class="m-l-20">Female</label>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="city">Permanent City</label>
                                </div>
                                <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="permanent_city" id="permanent_city" class="form-control" placeholder="Bengaluru" value="{{isset($caregiver)?$caregiver->permanent_city:''}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="state">Permanent State</label>
                                </div>
                                <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="permanent_state" id="permanent_state" class="form-control" placeholder="Karnataka" value="{{isset($caregiver)?$caregiver->permanent_state:''}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="address">Permanent Address</label>
                                </div>
                                <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="permanent_address" id="permanent_address" class="form-control" placeholder="Sahakar Nagar" value="{{isset($caregiver)?$caregiver->permanent_address:''}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                        </div>
                        <b>Change Profile Photo</b>
                        <div class="row clearfix">
                            <div class="col-sm-1"></div>
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 profile-img-block">
                                <img src="@if(isset($caregiver))@if(file_exists(public_path('uploads/caregivers/'.$caregiver->prof_folder.'/'.$caregiver->profile_image_path))){{ asset('uploads/caregivers/'.$caregiver->prof_folder.'/'.$caregiver->profile_image)}}@else{{ asset('default-avatar.jpg') }}@endif @endif" style="max-width: 360px; max-height: 160px; padding: 5px;" class="img img-thumbnail img-profile"/>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center" style="margin-top: 40px;">
                                <br>
                                <input type="file" id="prof_pic" name="prof_pic" class="form-control profile-picture"><br><br>
                            </div>
                        </div>
                        <b>Change Password</b>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="curr_password">Current Password</label>
                            </div>
                            <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="password" name="curr_password" id="curr_password" class="form-control" placeholder="Enter your current password">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="new_password">New Password</label>
                            </div>
                            <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="password" name="new_password" id="new_password" class="form-control" placeholder="Enter your new password">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="conf_password">Confirm Password</label>
                            </div>
                            <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="password" name="new_password_confirmation" id="new_password_confirmation" class="form-control" placeholder="Confirm your new password">
                                    </div>
                                    <span id='message'></span>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5 text-center">
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">SAVE</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="professionaldetails">
                    <b>About my Work Experience</b>
                    <form class="form-horizontal" method="POST" action="{{ route('update-professional-details')}}">
                        {{ csrf_field() }}
                        <div class="row clearfix">
                            <div class="col-sm-6 form-horizontal">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                        <label for="specialization">Specialization</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="">
                                                <select class="form-control show-tick" id="specialization" name="specialization">
                                                    <option value="0">-- Please select --</option>
                                                    @if(isset($specializations) && count($specializations))
                                                    @foreach($specializations as $s)
                                                    <option value="{{ $s->id }}">{{ $s->specialization_name }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                        <label for="qualification">Qualification</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="qualification" name="qualification" class="form-control" placeholder="Qualification" value="{{ isset($caregiver)?$caregiver->professional->qualification:'' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                        <label for="experience">Experience</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="experience" name="experience" class="form-control" placeholder="Experience" value="{{ isset($caregiver)?$caregiver->professional->experience:'' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                        <label for="current_address_line">Languages Known</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="demo-checkbox" style="padding-top: 1.5%">
                                                @if(isset($languages) && count($languages))
                                                @foreach($languages as $l)
                                                <input type="checkbox" id="languages_{{ strtolower($l->language_name) }}" name="languages_known[]" class="filled-in chk-col-light-blue" value="{{ $l->language_name }}" />
                                                <label for="languages_{{ strtolower($l->language_name) }}">{{ $l->language_name }}</label>
                                                @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                        <label for="achievements">Achievements</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <textarea id="achievements" name="achievements" class="form-control" placeholder="Achievements">{{ isset($caregiver)?$caregiver->professional->achievements:'' }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 form-horizontal">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-2 col-sm-4 col-xs-4 form-control-label">
                                        <label for="skill_id">Skill</label>
                                    </div>
                                    <div class="col-lg-9 col-md-8 col-sm-7 col-xs-7">
                                        <div class="form-group">
                                            <select class="form-control" id="skill_id">
                                                <option value="">-- Please select --</option>
                                                @if(count($skills))
                                                @foreach($skills as $s)
                                                <option value="{{ $s->id }}">{{ $s->skill_name }}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-2 col-sm-4 col-xs-4 form-control-label">
                                        <label for="specialization">Proficiency</label>
                                    </div>
                                    <div class="col-lg-9 col-md-8 col-sm-7 col-xs-7">
                                        <div class="form-group">
                                            <select class="form-control show-tick" id="proficiency">
                                                <option value="Excellent">Excellent</option>
                                                <option value="Very Good">Very Good</option>
                                                <option value="Good">Good</option>
                                                <option value="Sufficient">Sufficient</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-2 col-sm-3 col-xs-3 col-md-offset-3">
                                        <button type="button" class="btn btn-primary btn-block btnAddSkill">Add Skill</button>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <table id="skills_table" class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Skill Name</th>
                                                    <th>Proficiency</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr id="default"><td colspan="3" class="text-center">No skill(s) added</td></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5 text-center">
                                <button type="submit"  onclick="javascript: return checkSkills();" class="btn btn-primary m-t-15 waves-effect">SAVE</button>
                            </div>
                        </div>
                        <input type="hidden" name="skills" id="skills" value="">
                    </form>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="timings">
                    <b>My Availability</b>
                    <form class="form-horizontal update-timings" method="POST" action="{{ route('update-timings')}}">
                        {{ csrf_field() }}
                        <div class="body table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Day</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Start Time</th>
                                    <th class="text-center">End Time</th>
                                </tr>
                                @foreach(range(1,7) as $day)
                                <?php $current_day = strtolower(strftime('%A', strtotime("last saturday +$day day"))); ?>
                                <tr>
                                    <td class="text-center">{{ $day }}</td>
                                    <td class="text-center">{{ ucfirst($current_day) }}</td>
                                    <td>
                                        <div class="col-lg-12 col-md-8 col-sm-7 col-xs-7">
                                            {{-- <div class="form-group"> --}}
                                                <select class="show-tick {{$current_day.'_status'}}" name="{{$current_day.'_status'}}">
                                                    <option value="Working" {{!empty($timings->$current_day->status) && ($timings->$current_day->status)=="Working"?'Selected':''}}>Working</option>
                                                    <option value="Holiday" {{!empty($timings->$current_day->status) && ($timings->$current_day->status)=="Holiday"?'Selected':''}}>Holiday</option>
                                                </select>
                                            {{-- </div> --}}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-lg-12 col-sm-4">
                                            {{-- <div class="form-group"> --}}
                                                {{-- <div class="form-line"> --}}
                                                    <input type="text" name="{{$current_day.'_start'}}" id="proftime" class="timepicker form-control {{$current_day.'_start'}}" placeholder="Please choose a time..." data-dtp="dtp_dmqHL" value="{{!empty($timings->$current_day->start_time)?$timings->$current_day->start_time:false}}">
                                                {{-- </div> --}}
                                            {{-- </div> --}}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-lg-12 col-sm-4">
                                            {{-- <div class="form-group"> --}}
                                                {{-- <div class="form-line"> --}}
                                                    <input type="text" name="{{$current_day.'_end'}}" id="proftime" class="timepicker form-control {{$current_day.'_end'}}" placeholder="Please choose a time..." data-dtp="dtp_dmqHL" value="{{!empty($timings->$current_day->end_time)?$timings->$current_day->end_time:false}}">
                                                {{-- </div> --}}
                                            {{-- </div> --}}
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5 text-center">
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">SAVE</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="uploaddocuments">
                    <b>My Documents</b>
                    <div style="text-align:center;color:red;margin-bottom: 30px;">
                        <b>You must upload atleast one document from each of these Sub-sections.</b>
                    </div>
                    <div class="row clearfix">
                        <form class="form-horizontal" method="POST" action="{{ route('update-documents') }}" enctype="multipart/form-data">
                        {{csrf_field()}}
                            <div class="col-lg-6">
                                <div class="col-lg-5 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="specialization">Verification Documents</label>
                                </div>
                                <div class="col-lg-7 col-md-9 col-sm-8 col-xs-7" style="margin-bottom: 0 !important;">
                                    <div class="form-group">
                                        <select class="form-control show-tick" name="verification_documents" required>
                                            <optgroup label="'Government Issued Identification">
                                                <option value="Government Issued Identification_PAN Card">PAN Card</option>
                                                <option value="Government Issued Identification_Aadhar Card">Aadhar Card</option>
                                                <option value="Government Issued Identification_Driving License">Driving License</option>
                                                <option value="Government Issued Identification_Voter ID">Voter ID</option>
                                            </optgroup>
                                            <optgroup label="Permanent Address">
                                                <option value="Permanent Address_Cooking Gas Bill">Cooking Gas Bill</option>
                                                <option value="Permanent Address_Voter ID">Voter ID</option>
                                                <option value="Permanent Address_BSNL Bill">BSNL Bill</option>
                                            </optgroup>
                                            <optgroup label="Current Address">
                                                <option value="Current Address_Rental Agreement">Rental Agreement</option>
                                                <option value="Current Address_Elecrticity Bill">Elecrticity Bill</option>
                                                <option value="Current Address_Water Bill">Water Bill</option>
                                                <option value="Current Address_Cooking Gas Bill">Cooking Gas Bill</option>
                                            </optgroup>
                                            <optgroup label="Educational Document">
                                                <option value="Educational Document_Degree Certificate">Degree Certificate</option>
                                                <option value="Educational Document_UG Certificate">UG Certificate</option>
                                                <option value="Educational Document_PG Certificate">PG Certificate</option>
                                            </optgroup>
                                            <optgroup label="IMC Document">
                                                <option value="IMC Document_Indian Medical Council Certificate">Indian Medical Council Certificate</option>
                                            </optgroup>
                                        </select>
                                  </div>
                              </div>
                              <div class="col-lg-12">
                                  <div class="col-lg-5 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                      <label for="doc_desc">Document Description</label>
                                  </div>
                                  <div class="col-lg-7 col-md-9 col-sm-8 col-xs-7">
                                      <div class="form-group">
                                          <div class="form-line">
                                          <input type="text" name="verification_documents_comment" id="verification_documents_comment" class="form-control" required placeholder="Document Name,Institute Name,etc">
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-lg-12 col-md-4 col-sm-4 col-xs-4 text-center" style="margin-top: 0px;margin-bottom: 0px;">
                                  <input type="file" id="prof_pic" name="verification_documents_document" class="form-control" required><br><br>
                              </div>
                          </div>
                          <div class="col-lg-6">
                            <table class="table table-striped table-bordered"  style="text-align:center;">
                                <th colspan="2" style="text-align: center;">Progress</th>
                                <tr>
                                    <th>Government Issued Identification</th>
                                    <td>
                                        @if(in_array('Government Issued Identification', $uploaded_document_categories))
                                        <i class="material-icons" style="color: green;font-size: 20px;">check_circle</i>
                                        @else
                                        <i class="material-icons" style="color: red;font-size: 20px;">error</i>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Permanent Address</th>
                                    <td>
                                        @if(in_array('Permanent Address', $uploaded_document_categories))
                                        <i class="material-icons" style="color: green;font-size: 20px;">check_circle</i>
                                        @else
                                        <i class="material-icons" style="color: red;font-size: 20px;">error</i>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Current Address</th>
                                    <td>
                                        @if(in_array('Current Address', $uploaded_document_categories))
                                        <i class="material-icons" style="color: green;font-size: 20px;">check_circle</i>
                                        @else
                                        <i class="material-icons" style="color: red;font-size: 20px;">error</i>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Educational Document(s)</th>
                                    <td>
                                        @if(in_array('Educational Document', $uploaded_document_categories))
                                       <i class="material-icons" style="color: green;font-size: 20px;">check_circle</i>
                                        @else
                                        <i class="material-icons" style="color: red;font-size: 20px;">error</i>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>IMC Document</th>
                                    <td>
                                        @if(in_array('IMC Document', $uploaded_document_categories))
                                        <i class="material-icons" style="color: green;font-size: 20px;">check_circle</i>
                                        @else
                                        <i class="material-icons" style="color: red;font-size: 20px;">error</i>
                                        @endif
                                    </td>
                                </tr>
                            </table>
                          </div>
                          <div class="row clearfix">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-left: 0px !important;">
                                  <button type="submit" class="btn btn-primary m-t-15 waves-effect">UPLOAD</button>
                              </div>
                          </div>
                          <div class="col-lg-12">
                              <table class="table table-striped table-bordered table-striped uploaded-documents"  style="text-align:center;">
                                <div style="text-align:center;color:red;"><b>Your Uploaded Documents will be displayed here, you must remove an uploaded document before reuploading for the same.</b></div>
                                <tr>
                                    <th>#</th>
                                    <th><u>Document</u></th>
                                    <th><u>Document Name</u></th>
                                    <th><u>View</u></th>
                                    <th><u>Action</u></th>
                                </tr>
                                @forelse($caregiver->verifications as $ver)
                                <tr style="text-align:center;">
                                    <td>
                                        
                                    </td>
                                    <td>
                                        <b><i>{{ $ver->doc_type }}</i></b>
                                    </td>
                                    <td>
                                        <b><i>{{ $ver->doc_name }}</i></b>
                                    </td>
                                    <td>
                                    @if(str_contains( $ver->doc_path, ['.pdf'] ) == false)
                                        <img style="width: 100px;height: 100px;pointer-events: none;" src="{{ asset('uploads/caregivers/'.$caregiver->prof_folder.'/document/'.$ver->doc_path)}}" class="btn btn-info fa fa-search">
                                    @endif
                                        <a href="{{ asset('uploads/caregivers/'.$caregiver->prof_folder.'/document/'.$ver->doc_path)}}" target="_blank" class="btn btn-info"><i class="material-icons">pageview</i></a>
                                    </td>
                                    <td>
                                        <button type="button" name="data" class="btn btn-danger remove" value="[{{$ver}},{{$caregiver}}]">Remove</button>
                                    </td>
                                </tr>
                                
                                @empty
                                <td class="text-center empty" colspan="3">
                                    <b><i>No Documents Uploaded</i></b>
                                </td>
                                @endforelse
                              </table>
                          </div>
                      </form>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="verification">
                    <b>Verify Me !!</b>
                    <div class="pre-req-info">
                        <b style="color: red;">Before Applying for verifications,make sure you have uploaded the required documents.</b><br>
                        <br>
                        <u><i>Basic Requirements</i></u><br>
                        <br>
                        1)Government Issued Identification Check (Atleast one of PAN card / Aadhar card / Driving License / Voter ID)<br>
                        2)Permanent Address (Atleast one of Cooking gas Bill / Voter ID / BSNL bill)<br>
                        3)Current Address (Atleast one of Rental Agreement / Electricity Bill / Water Bill / Cooking Gas Bill)<br>
                        4)Education (Atleast one of Xth / Intermediate / Degree / UG / PG certificate. Preferably your highest qualification.)<br>
                        5)IMC registration Number with Certificate.<br>
                        <br>
                        Verification is optional,but the verified caregiver would occur on top of non verified caregiver for searches made by customers.<br>
                        <br>
                        <b style="color: blue;">Note:-To be a verified professional,you must be verified in documents, police, educational and employment check.</b><br>
                        <form class="form-horizontal" method="POST" action="{{ route('apply-for-verification') }}">
                        {{csrf_field()}}
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-left: 0px !important;">
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">APPLY</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('plugin.scripts')
<!-- Select Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<!-- Input Mask Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>
<!-- Moment Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
@endsection


@section('page.scripts')
<script>
    var skillsArray = [];
    


    document.getElementById('prof_pic').addEventListener('change', handleFileSelect, false);

    $(function(){
        @if(isset($caregiver))
        $('#specialization').selectpicker('val', '{{ $caregiver->professional->specialization }}');

        @if(!empty($caregiver->professional->languages_known))
        {? $languages = explode(",", $caregiver->professional->languages_known); ?}
        @foreach($languages as $l)
        $('input[id=languages_{{ strtolower($l) }}]').attr('checked',true);
        @endforeach
        @endif

        // Take Skills Values
        {? if(isset($caregiver_skills)){ ?}
        {? $html = ''; ?}
        {? foreach($caregiver_skills as $skills){ ?}
        {?    $html .= '<tr data-id="'.$skills->skill_id.'"><td data-skill="'.$skills->skill_id.'">'.$skills->skill->skill_name.'</td><td data-proficiency="'.$skills->proficiency.'">'.$skills->proficiency.'</td><td><a class="btn btn-danger btn-xs btnRemoveSkill" data-skill="'.$skills->skill_id.'"><i class="material-icons">delete</i></a></td></tr>'; ?}
        $('#skill_id option[value={{ $skills->skill_id }}]').attr('disabled',true);
        {? } ?}
        $('#skill_id').selectpicker('val','');
        $('#skill_id').selectpicker('refresh');
        $('#skills_table tbody').html('{!! $html !!}');
        {? } ?}
        @endif
        // Add skills to the set
        $('.content').on('click','.btnAddSkill', function(){
            skill_id = $('#skill_id option:selected').val();
            skill = $('#skill_id option:selected').text();
            proficiency = $('#proficiency option:selected').val();

            html = '<tr data-id="'+skill_id+'"><td data-skill="'+skill_id+'">'+skill+'</td><td data-proficiency="'+proficiency+'">'+proficiency+'</td><td><a class="btn btn-danger btn-xs btnRemoveSkill" data-skill="'+skill_id+'"><i class="material-icons">delete</i></a></td></tr>';

            $('#skill_id option[value='+skill_id+']').attr('disabled',true);
            $('#skill_id').selectpicker('val','');
            $('#skill_id').selectpicker('refresh');

            if($('#skills_table tbody tr#default').length == 1){
                $('#skills_table tbody').html(html);
            }else
                $('#skills_table tbody').append(html);
        });
        
        // Remove skills to from the set
        $('.content').on('click','.btnRemoveSkill', function(){
            skill = $(this).data('skill');
            $('#skills_table tbody').find('tr[data-id='+skill+']').remove();

            $('#skill_id option[value='+skill_id+']').removeAttr('disabled',true);
            $('#skill_id').selectpicker('val','');
            $('#skill_id').selectpicker('refresh');

            if($('#skills_table tbody tr').length == 0){
                $('#skills_table tbody').html('<tr id="default"><td colspan="3" class="text-center">No skill(s) added</td></tr>');
            }
        });
    });

    function handleFileSelect(evt) {
        var files = evt.target.files; // FileList object

        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {
            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }
            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function(theFile) {
                return function(e) {
                    // Render thumbnail.
                    var span = document.createElement('span');
                    span.innerHTML = ['<img style="width:160px;height: 160px;" class="img-responsive img-thumbnail" src="', e.target.result,
                    '" title="', escape(theFile.name), '"/>'].join('');
                    $('.profile-img-block').html(span);
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
    }

    $('#new_password_confirmation').on('keyup', function () {
        if ($(this).val() == $('#new_password').val()) {
            $('#message').html('matching').css('color', 'green');
        } else $('#message').html('not matching').css('color', 'red');
    });

    $(function(){
        $('.timepicker').bootstrapMaterialDatePicker({
            date: false,
            format: 'HH:mm A',
            clearButton: true
        });
        $('.birthdate').bootstrapMaterialDatePicker({ 
            format: 'DD-MM-YYYY',
            clearButton: true,
            time: false,
            minDate : new Date(),
        });
    });

    $(".update-timings").submit(function(e){
        var days = ['sunday','monday','tuesday','wednesday','thursday','friday','saturday'];
        days.forEach(function(day){
            status = $('.' + day + '_status').val();
            start = $('.' + day + '_start').val();
            end = $('.' + day + '_end').val();
            parentrow = $('.' + day + '_start').closest('tr');

            parentrow.removeClass('danger');
            if(status == 'Holiday'){
                return false;
            }
            if(start > end){
                alert(day + ': Start Time cannot be greater than End Time');
                parentrow.addClass('danger');
                e.preventDefault();
            }
        });
    });

    function checkSkills(){
        if($('#skills_table tbody tr').length > 0){
            $('#skills_table tbody tr').each(function(){
                skill = $(this).find('td:eq(0)').data('skill');
                proficiency = $(this).find('td:eq(1)').data('proficiency');

                skillsArray.push({skill_id: skill, proficiency: proficiency});
            });

            $('#skills').val(JSON.stringify(skillsArray));
            console.log(JSON.stringify(skillsArray));
        }
        return true;
    }

    $(".remove").click(function(){
        if(confirm('Are you sure?')){
            var jsonObj = JSON.parse($(this).val());
            var jsonObjVer = jsonObj[0];
            var jsonObjProf = jsonObj[1];
            var id= jsonObjVer.caregiver_id;
            // var prof_id = jsonObjVer.prof_id;
            var doc_path = jsonObjVer.doc_path;
            var prof_folder = jsonObjProf.prof_folder;
            var form_data = {
                id:id,
                // prof_id:prof_id,
                doc_path:doc_path,
                prof_folder:prof_folder,
                _token: '{{csrf_token()}}'
            };
            console.log(form_data);
            $.ajax({
                url: '{{route('ajax.document-remove')}}',
                type: 'POST',
                data: form_data,
                success: function(d){
                // console.log(d);
                // toastr.success("The document has been removed.");
                      setTimeout(function(){// wait for 0 secs
                                 location.reload(); // then reload the page.
                                }, 0000);
                    },
                    error: function(e){
                      // console.log(e);
                      // toastr.warning("Oops! Something went wrong. Try again.");
                    }
                });
        }
    });
</script>
@endsection
