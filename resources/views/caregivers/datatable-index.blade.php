@extends('layouts.main-layout')

@section('page_title','Employees - ')
@section('active_caregivers','active')
@section('plugin.styles')
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />
<style>
    .table-bordered tbody tr td, .table-bordered tbody tr th {
        white-space: nowrap;
        font-size: 13px;
    }
</style>
@endsection
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('hr.index') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-6">
                    Employees
                    <small>List of all employees</small>
                </h2>
                <div class="col-sm-5 text-right">
                    @ability('admin','employees-create')
                    <a href="{{ route('employee.create') }}" class="btn-group" role="group">
                        <button type="button" class="btn btn-success waves-effect"><i class="material-icons">add_circle</i></button>
                        <button type="button" class="btn btn-success waves-effect" style="line-height: 1.9;padding-left:0">New Employee</button>
                    </a>&nbsp;&nbsp;
                    <a href="{{ route('employee.import') }}" class="btn-group hide" role="group">
                        <button type="button" class="btn btn-primary waves-effect"><i class="material-icons">cloud_upload</i></button>
                        <button type="button" class="btn btn-primary waves-effect" style="line-height: 1.9;padding-left:0">Bulk Import</button>
                    </a>
                    @endability                    
                </div>
            </div>
            @ability('admin','employees-list')
            <div class="body">
                {!! $dataTable->table(['class' => 'table table-bordered table-striped table-hover table-condensed', 'style' => 'zoom:90%;width:100%']) !!}
            </div>
            @endability
        </div>
    </div>
</div>
@endsection

@section('page.scripts')
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
    <!-- Bootstrap Notify Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-notify/bootstrap-notify.js') }}"></script>
    <!-- Select Plugin Js -->
    <script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>    
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css" />    
    {!! $dataTable->scripts() !!}
    <script>
        $(document).ready(function() {
            if('{{ \Helper::encryptor('decrypt',session('utype')) }}' != 'Admin'){
                var table = $('.dataTable').DataTable();
            }else{
                var table = $('.dataTable').DataTable();
            }

            table.on('page.dt', function() {
              $('html, body').animate({
                scrollTop: $(".dataTables_wrapper").offset().top
               }, 'slow');
            });
        });
        $(function(){
            $('.content').on('click','.work-status',function(){
                if(confirm("Are your sure ?")){
                    var id = $(this).data('id');
                    var status = $(this).data('status');

                    // Make AJAX Call
                    $.ajax({
                        url: '{{ route('employee.update-status') }}',
                        type: 'POST',
                        data: {_token:'{{ csrf_token() }}', id: id, status: status},
                        success: function (data){
                            // Set the current status to button
                            $('button.workstatus[data-id="'+id+'"]').text(status);

                            showNotification('bg-green', 'Status changed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                        },
                        error: function (error){
                            console.log(error);
                        }
                    });
                }
                return true;
            });

            $('.content').on('click','.work-status-availability',function(){
                if(confirm("Are your sure ?")){
                    var id = $(this).data('id');
                    var status = $(this).data('status-availibility');

                    // Make AJAX Call
                    $.ajax({
                        url: '{{ route('employee.update-availability') }}',
                        type: 'POST',
                        data: {_token:'{{ csrf_token() }}', id: id, status: status},
                        success: function (data){
                            // Set the current status to button
                            $('button.workstatusavailability[data-id="'+id+'"]').text(status);

                            showNotification('bg-green', 'Availability changed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                        },
                        error: function (error){
                            console.log(error);
                        }
                    });
                }
                return true;
            });
        });
    </script>
@endsection
