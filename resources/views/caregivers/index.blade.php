@extends('layouts.main-layout')

@section('page_title','Employees - ')
@section('active_caregivers','active')
@section('plugin.styles')
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />
<style>
    .table-bordered tbody tr td, .table-bordered tbody tr th {
        white-space: nowrap;
        font-size: 13px;
    }
</style>
@endsection
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('hr.index') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-6">
                    Employees
                    <small>List of all employees</small>
                </h2>
                <div class="col-sm-5 text-right">
                    @ability('admin','employees-create')
                    <a href="{{ route('employee.create') }}" class="btn-group" role="group">
                        <button type="button" class="btn btn-success waves-effect"><i class="material-icons">add_circle</i></button>
                        <button type="button" class="btn btn-success waves-effect" style="line-height: 1.9;padding-left:0">New Employee</button>
                    </a>&nbsp;&nbsp;
                    <a href="{{ route('employee.import') }}" class="btn-group hide" role="group">
                        <button type="button" class="btn btn-primary waves-effect"><i class="material-icons">cloud_upload</i></button>
                        <button type="button" class="btn btn-primary waves-effect" style="line-height: 1.9;padding-left:0">Bulk Import</button>
                    </a>
                    @endability
                    {{-- <a href="{{ route('employee.create') }}" class="pull-right btn btn-success waves-effect waves-float">
                        <i class="material-icons">add</i> New Caregiver
                    </a> --}}
                </div>
            </div>
            @ability('admin','employees-list')
            <div class="body">
                <table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable" style="zoom:90%;">
                    <thead>
                        <tr>
                            <th>#<br></th>
                            <th>Employee ID</th>
                            <th>Field Staff</th>
                            <th>Employment</th>
                            @if(\Helper::encryptor('decrypt',session('utype')) == 'Admin')
                            <th>Branch</th>
                            @endif
                            <th>Name</th>
                            <th>Gender</th>
                            <th>Contact</th>
                            <th>Specialization</th>
                            <th>Experience</th>
                            <th>Rating</th>
                            <th>Verification</th>
                            <th>Work Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($caregivers as $i => $e)
                        <tr>
                            <td>{{ $i + 1 }}</td>
                            <td>{{ isset($e->employee_id)?$e->employee_id:'' }}</td>
                            @if(isset($e->professional->role) && $e->professional->role == "Manager")
                            <td style="color: red;font-weight: bold;">No</td>
                            @else
                            <td><b>Yes</b></td>
                            @endif
                            @if(isset($e->professional->employment_type) && $e->professional->employment_type == "Freelancer")
                            <td class="font-bold col-pink">{{ $e->professional->employment_type }}</td>
                            @else
                            <td class="font-bold">{{ $e->professional->employment_type }}</td>
                            @endif
                            @if(\Helper::encryptor('decrypt',session('utype')) == 'Admin')
                                @if(isset($e->branch_id))
                                    @if($e->branch_id =='0' || $e->branch_id == null)
                                    <td>NA</td>
                                    @else
                                    <td>{{ $e->branchName->branch_city }}</td>
                                    @endif
                                @else
                                    <td>NA</td>
                                @endif
                            @endif
                            <td>
                                <div style="display: inline-block; vertical-align: top;">
                                        <img src="@if($e->profile_image && file_exists(public_path('uploads/provider/'.session('tenant_id').'/caregivers/'.\Helper::encryptor('encrypt',$e->id)).'/'.$e->profile_image)){{ asset('uploads/provider/'.session('tenant_id').'/caregivers/'.\Helper::encryptor('encrypt',$e->id).'/'.$e->profile_image)}}@else{{ asset('img/default-avatar.jpg') }}@endif" width="40" height="40" class="img-circle" alt="{{ $e->first_name }}" class="img-responsive img-thumbnail" title="{{ $e->first_name }}" />
                                </div>
                                <div style="display: inline-block; margin-left: 2%">
                                    {!! $e->first_name !!}<br>{!! $e->middle_name !!}<br>{!! $e->last_name !!}<br>
                                </div>
                            </td>
                            <td>{{ $e->gender }}</td>
                            <td>{!! $e->mobile_number.'<br><small>'.$e->email.'</small>' !!}</td>
                            <td>{{ isset($e->professional->specializations)?$e->professional->specializations->specialization_name:'-' }}</td>
                            <td>{{ (isset($e->professional) && !empty($e->professional->experience) && is_numeric($e->professional->experience))?$e->professional->experience.' yr(s)':'-' }}</td>
                            <td>{{ '-' }}</td>
                            @if(isset($e->prof_verification) && $e->prof_verification == "Rejected")
                                <td style="color: red;font-weight: bold;">{{ $e->prof_verification }}</td>
                            @elseif(isset($e->prof_verification) && $e->prof_verification == "Pending")
                                <td style="color: blue;font-weight: bold;">{{ $e->prof_verification }}</td>
                            @elseif(isset($e->prof_verification) && $e->prof_verification == "Verified")
                                <td style="color: green;font-weight: bold;">{{ $e->prof_verification }}</td>
                            @else
                                <td style="color: grey;font-weight: bold;">{{ $e->prof_verification }}</td>
                            @endif
                            <td width="10%">
                                @ability('admin','employees-edit')
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info waves-effect workstatus" data-id="{{ Helper::encryptor('encrypt',$e->id) }}">{{ $e->work_status ?? 'Change Status' }}</button>
                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="javascript:void(0);" data-status="Available" data-id="{{ Helper::encryptor('encrypt',$e->id) }}" class="work-status waves-effect waves-block">Available</a></li>
                                        <li><a href="javascript:void(0);" data-status="Training" data-id="{{ Helper::encryptor('encrypt',$e->id) }}" class="work-status waves-effect waves-block">Training</a></li>
                                    </ul>
                                </div>
                                @else
                                    {{ $e->work_status ?? 'Change Status' }}
                                @endability
                            </td>
                            <td>
                                @ability('admin','employees-edit')
                                <a href="{{ route('employee.edit',Helper::encryptor('encrypt',$e->id)) }}" class="btn btn-warning btn-sm"><i class="material-icons">edit</i></a>
                                @endability
                                @ability('admin','employees-delete')
                                {{-- <form style="display: inline" action="{{ route('employee.destroy',Helper::encryptor('encrypt',$e->id)) }}" method="POST">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" onclick="javascript: return confirm('Are you sure?');" class="btn btn-danger btn-sm"><i class="material-icons">delete</i></a>
                                </form> --}}
                                @endability
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="20" class="text-center">No Employee(s) found. <a href="{{ route('employee.create') }}" class="btn btn-info btn-sm">Create Employee</a></td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            @endability
        </div>
    </div>
</div>
@endsection

@section('page.scripts')
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
    <!-- Bootstrap Notify Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-notify/bootstrap-notify.js') }}"></script>
    <!-- Select Plugin Js -->
    <script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            if('{{ \Helper::encryptor('decrypt',session('utype')) }}' != 'Admin'){
                $('.dataTable').DataTable();
            }else{
                $('.dataTable').DataTable();
            }
        });
        $(function(){
            $('.content').on('click','.work-status',function(){
                if(confirm("Are your sure ?")){
                    var id = $(this).data('id');
                    var status = $(this).data('status');

                    // Make AJAX Call
                    $.ajax({
                        url: '{{ route('employee.update-status') }}',
                        type: 'POST',
                        data: {_token:'{{ csrf_token() }}', id: id, status: status},
                        success: function (data){
                            // Set the current status to button
                            $('button.workstatus[data-id="'+id+'"]').text(status);

                            showNotification('bg-green', 'Status changed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                        },
                        error: function (error){
                            console.log(error);
                        }
                    });
                }
                return true;
            });

            $('.content').on('click','.work-status-availability',function(){
                if(confirm("Are your sure ?")){
                    var id = $(this).data('id');
                    var status = $(this).data('status-availibility');

                    // Make AJAX Call
                    $.ajax({
                        url: '{{ route('employee.update-availability') }}',
                        type: 'POST',
                        data: {_token:'{{ csrf_token() }}', id: id, status: status},
                        success: function (data){
                            // Set the current status to button
                            $('button.workstatusavailability[data-id="'+id+'"]').text(status);

                            showNotification('bg-green', 'Availability changed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                        },
                        error: function (error){
                            console.log(error);
                        }
                    });
                }
                return true;
            });
        });
    </script>
@endsection
