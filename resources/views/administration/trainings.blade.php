@extends('layouts.main-layout')

@section('page_title','Training Master - ')

@section('active_administration','active')

@section('page.styles')
<!-- Moment Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<!-- Multi Select Css -->
<link href="{{ ViewHelper::ThemePlugin('multi-select/css/multi-select.css') }}" rel="stylesheet">
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<style>
</style>
@endsection

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
            <div class="header clearfix">
                <a href="{{ route('settings') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-10">
                    Training Master
                    <small>Training</small>
                </h2>
            </div>
            <div class="header">
                <h2 class="action_lbl">
                    New Training
                </h2>
            </div>
            <div class="body">
                <div class="row clearfix">
                        <div class="col-lg-3">
                          <div class="row clearfix">
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 form-control-label">
                                  <label for="trainers_name">Trainers Name</label>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                  <div class="form-group">
                                      <div class="form-line">
                                          <input type="text" id="trainers_name" name="trainers_name" class="form-control" placeholder="Trainer Name" required>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="row clearfix">
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 form-control-label">
                                  <label for="training_name">Training Name</label>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                  <div class="form-group">
                                      <div class="form-line">
                                          <input type="text" id="training_name" name="training_name" class="form-control" placeholder="Training Name" required>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="row clearfix">
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 form-control-label">
                                  <label for="start_date">Start Date</label>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                  <div class="form-group">
                                      <div class="form-line">
                                          <input type="text" id="start_date" name="start_date" class="form-control date" placeholder="Start Date" required>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="row clearfix">
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 form-control-label">
                                  <label for="end_date">End Date</label>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                  <div class="form-group">
                                      <div class="form-line">
                                          <input type="text" id="end_date" name="end_date" class="form-control date" placeholder="End Date" required>
                                      </div>
                                  </div>
                              </div>
                          </div>  
                        </div>
                      <div class="col-lg-7">
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-5 col-xs-5 form-control-label">
                                <label for="employee_id">Employee List</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <select id="trainingEmployees" class="ms" multiple="multiple">
                                        @foreach($specialization as $s)
                                            <optgroup label="{{ $s->specialization_name }}">
                                                @foreach($caregivers as $c)
                                                    @if($c->professional->specialization == $s->id)
                                                        <option value="{{ $c->id }}">{{ $c->first_name.' '.$c->middle_name.' '.$c->last_name }}</option>
                                                    @endif
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                      </div>
                        <div class="col-lg-2" style="margin-top: 5%;">
                            <input type="hidden" id="id" value="0">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <button type="submit" class="btn btn-success btn-block btn-lg waves-effect pull-left saveTraining">Save</button>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hide">
                                <button type="reset" class="btn btn-danger btn-block btn-lg waves-effect pull-right">Clear</button>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body">
                <table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Trainer's Name</th>
                            <th>Training Name</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Employees Involved</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse($trainings as $index => $t)
                    <tr>
                        <td>{{ $index + 1 }}.</td>
                        <td>{{ $t->trainer_name }}</td>
                        <td>{{ $t->training_name }}</td>
                        <td>{{ Carbon\Carbon::parse($t->start_date)->format('d-M-Y') }}</td>
                        <td>{{ Carbon\Carbon::parse($t->start_date)->format('d-M-Y') }}</td>
                        <td>{{ $t->caregiver->first_name.' '.$t->caregiver->middle_name.' '.$t->caregiver->last_name }}</td>
                        <td>
                        <form class="form-inline" action="{{ route('trainings.destroy',Helper::encryptor('encrypt',$t->id)) }}" method="POST" style="display: inline">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit" onclick="return confirm('Remove {{ $t->caregiver->first_name.' '.$t->caregiver->middle_name.' '.$t->caregiver->last_name }} from Current Training ?');" class="btn btn-danger btn-sm waves-effect btnRemove"><i class="material-icons small">delete</i></button>
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="7" class="text-center">No Training Session(s) found.</td>
                    </tr>
                    @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Trainer's Name</th>
                            <th>Training Name</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Employees Involved</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page.scripts')
<!-- Bootstrap Notify Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-notify/bootstrap-notify.js') }}"></script>
<script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
<!-- Multi Select Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('multi-select/js/jquery.multi-select.js') }}"></script>
<!-- Moment Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>

<script>
    $(function(){
        initBlock();
    });

    function initBlock(){
        $('.date').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD',
            minDate : new Date(),
            time: false,
            clearButton: true,
        });
    }

    var selectedCount = [];
    $('#trainingEmployees').multiSelect({
       selectableOptgroup: true,
       afterSelect: afterSelect,
       afterDeselect: afterDeselect
   });

    function afterSelect (values) {
        $(values).each(function(index, values){
            selectedCount.push(values);
        });
    }

    function afterDeselect (values) {
      $(values).each(function(index, values){
            selectedCount.pop(values);
        });
    }

    $(function(){
        $('.saveTraining').on('click', function(){
            trainerName = $('#trainers_name').val(); 
            trainingName = $('#training_name').val();
            startDate = $('#start_date').val();
            endDate = $('#end_date').val();
            idList = selectedCount;

            $.ajax({
                url : '{{ route('trainings.store') }}',
                type: 'POST',
                data: {trainer_name: trainerName, training_name: trainingName, start_date: startDate, end_date:endDate, employee_id:idList, _token: '{{ csrf_token() }}'},
                success: function (data){
                    showNotification('bg-light-green', 'Training stored successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                    setTimeout(function(){// wait for 2 secs
                        location.reload(); // then reload the page.
                    }, 2000);
                },
                error: function (error){
                    console.log(error);
                }
            });

        });
    });
</script>
@endsection
