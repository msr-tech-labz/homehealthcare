@extends('layouts.main-layout')

@section('page_title','Holiday Master - ')

@section('active_administration','active')

@section('plugin.styles')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
@endsection

@section('content')
<div class="row clearfix">
    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('settings') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-10">
                    Holidays List
                    <small>Manage Holidays</small>
                </h2>
            </div>
            <div class="body">
                <table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable">
                    <thead>
                        <tr>
                            <th width="10%">#</th>
                            <th>Holiday Name</th>
                            <th width="20%">Date</th>
                            <th width="20%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($holidays as $index => $d)
                            <tr>
                            <td>{{ $index + 1 }}.</td>
                            <td>{{ $d->holiday_name }}</td>
                            <td>{{ $d->holiday_date->format('d-m-Y').' ('.$d->holiday_date->format('D').')' }}</td>
                            <td>
                                <button type="button" data-id="{{ Helper::encryptor('encrypt',$d->id) }}" data-branch="{{ $d->branch_id }}" data-name="{{ $d->holiday_name }}" data-date="{{ $d->holiday_date }}" data-formatted-date="{{ $d->holiday_date->format('d-m-Y') }}" data-url="{{ route('holiday.update',Helper::encryptor('encrypt',$d->id)) }}" class="btn btn-success btn-sm waves-effect btnEdit"><i class="material-icons small">edit</i></button>
                                <form class="form-inline" action="{{ route('holiday.destroy',Helper::encryptor('encrypt',$d->id)) }}" method="POST" style="display: inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" onclick="return confirm('Are you sure?');" class="btn btn-danger btn-sm waves-effect btnRemove"><i class="material-icons small">delete</i></button>
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="4" class="text-center">No Holiday(s) found.</td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Holiday Name</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
        <div class="card">
            <div class="header">
                <h2 class="action_lbl">
                    New Holiday
                </h2>
            </div>
            <div class="body">
                <div class="row clearfix">
                    <form class="form-horizontal newForm" method="POST" action="{{ route('holiday.store') }}">
                        {{ method_field('POST') }}
                        {{ csrf_field() }}
                        @include('partials.branch-input')
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                <label for="holiday_name">Name</label>
                            </div>
                            <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="holiday_name" name="holiday_name" class="form-control" placeholder="Holiday Name" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                <label for="holiday_date">Date</label>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control date" placeholder="Holiday Date" />
                                        <input type="hidden" id="holiday_date" name="holiday_date" value="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <input type="hidden" id="id" value="0">
                            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 col-md-offset-2">
                                <button type="submit" class="btn btn-success btn-block btn-lg waves-effect pull-left">Save</button>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                                <button type="reset" class="btn btn-danger btn-block btn-lg waves-effect pull-right">Clear</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page.scripts')
    <!-- Moment Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<script>
    $(function(){
        $('.date').bootstrapMaterialDatePicker({
			format: 'DD-MM-YYYY',
            //disabledDays: [7],
			time: false,
			clearButton: true,
		}).on('change', function(e, date)
        {
            $('#holiday_date').val(moment(date).format('YYYY-MM-DD'));
        });

        $('.btnEdit').on('click', function(){
            id = $(this).data('id');
            branch = $(this).data('branch');
            name = $(this).data('name');
            hDate = $(this).data('date');
            fDate = $(this).data('formatted-date');

            $('.action_lbl').html('Edit Holiday - '+name);
            $('.newForm').attr('action',$(this).data('url'));
            $('.newForm').attr('method','POST');
            $('.newForm input[name=_method]').val('PUT');
            $('.newForm input[name=_token]').val('{{ csrf_token() }}');
            $('.newForm #id').val(id);
            $('.newForm #branch_id').val(branch);
            $('.newForm #holiday_name').val(name);
            $('.newForm .date').val(fDate);
            $('.newForm #holiday_date').val(hDate);
        });
    });
</script>
@endsection
