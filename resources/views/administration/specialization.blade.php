@extends('layouts.main-layout')

@section('page_title','Specializations Master - ')

@section('active_administration','active')

@section('content')
    @if (count($errors) > 0)
    <div class="row clearfix">
    	<div class="alert alert-danger">
    		<ul>
    			@foreach ($errors->all() as $error)
    				<li>{{ $error }}</li>
    			@endforeach
    		</ul>
    	</div>
    </div>
    @endif

<div class="row clearfix">
    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('settings') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-10">
                    Specialization Master
                    <small>Employee specializations</small>
                </h2>
            </div>
            <div class="body">
                <table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="30%">Name</th>
                            <th>Description</th>
                            <th width="20%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($specializations as $index => $d)
                            <tr>
                            <td>{{ $index + 1 }}.</td>
                            <td>{{ $d->specialization_name }}</td>
                            <td>{!! $d->description ?? '-' !!}</td>
                            @if($d->created_by == 'Custom')
                            <td>
                                <button type="button" data-id="{{ Helper::encryptor('encrypt',$d->id) }}" data-branch="{{ $d->branch_id }}" data-name="{{ $d->specialization_name }}" data-description="{{ $d->description }}" data-url="{{ route('specialization.update',Helper::encryptor('encrypt',$d->id)) }}" class="btn btn-success btn-sm waves-effect btnEdit"><i class="material-icons small">edit</i></button>
                                <form class="form-inline" action="{{ route('specialization.destroy',Helper::encryptor('encrypt',$d->id)) }}" method="POST" style="display: inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" onclick="return confirm('Are you sure?');" class="btn btn-danger btn-sm waves-effect btnRemove"><i class="material-icons small">delete</i></button>
                                </form>
                            </td>
                            @else
                            <td></td>
                            @endif
                        </tr>
                        @empty
                        <tr>
                            <td colspan="4" class="text-center">No specialization(s) found.</td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
        <div class="card">
            <div class="header">
                <h2 class="action_lbl">
                    New Specialization
                </h2>
            </div>
            <div class="body">
                <div class="row clearfix">
                    <form class="form-horizontal newForm" method="POST" action="{{ route('specialization.store') }}">
                        {{ method_field('POST') }}
                        {{ csrf_field() }}
                        @include('partials.branch-input')
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                <label for="specialization_name">Name</label>
                            </div>
                            <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="specialization_name" name="specialization_name" class="form-control" placeholder="Specialization Name" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                <label for="specialization_name">Description</label>
                            </div>
                            <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <textarea id="description" name="description" class="form-control" placeholder="Description"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <input type="hidden" id="id" name="id" value="0">
                            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 col-md-offset-2">
                                <button type="submit" class="btn btn-success btn-block btn-lg waves-effect pull-left">Save</button>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                                <button type="reset" class="btn btn-danger btn-block btn-lg waves-effect pull-right">Clear</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page.scripts')
<script>
    $(function(){
        $('.btnEdit').on('click', function(){
            id = $(this).data('id');
            branch = $(this).data('branch');
            name = $(this).data('name');
            description = $(this).data('description');

            $('.action_lbl').html('Edit Specialization - '+name);
            $('.newForm').attr('action',$(this).data('url'));
            $('.newForm').attr('method','POST');
            $('.newForm input[name=_method]').val('PUT');
            $('.newForm input[name=_token]').val('{{ csrf_token() }}');
            $('.newForm #id').val(id);
            $('.newForm #branch_id').val(branch);
            $('.newForm #specialization_name').val(name);
            $('.newForm #description').val(description);
        });
    });
</script>
@endsection
