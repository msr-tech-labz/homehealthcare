@extends('layouts.main-layout')

@section('page_title','Consumables Master - ')

@section('active_administration','active')

@section('content')
<div class="row clearfix">
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('settings') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-10">
                    Consumables Master
                    <small>Manage Consumables</small>
                </h2>
            </div>
            <div class="body">
                <table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable" style="zoom:90%;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th width="30%">Name</th>
                            <th>Price</th>
                            <th>Tax Class</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse($consumables as $index => $c)
                        <tr>
                        <td>{{ $index + 1 }}.</td>
                        <td>
                            {{ $c->consumable_name }}<br>
                            <small>{!! $c->description ?? '' !!}</small>
                        </td>
                        <td>{{ $c->consumable_price }}</td>
                        <td>{{ $c->tax->tax_rate ?? '' }} {{ (isset($c->tax) && $c->tax->type=='Percentage')?'%':'' }}</td>
                        <td>
                            <button type="button" data-id="{{ Helper::encryptor('encrypt',$c->id) }}" data-branch="{{ $c->branch_id }}" data-name="{{ $c->consumable_name }}" data-description="{{ $c->description }}" data-price="{{ $c->consumable_price }}" data-tax-id="{{ $c->tax_id }}" data-url="{{ route('consumables.update',Helper::encryptor('encrypt',$c->id)) }}" class="btn btn-success btn-sm waves-effect btnEdit"><i class="material-icons small">edit</i></button>
                            <form class="form-inline" action="{{ route('consumables.destroy',Helper::encryptor('encrypt',$c->id)) }}" method="POST" style="display: inline">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit" onclick="return confirm('Are you sure?');" class="btn btn-danger btn-sm waves-effect btnRemove"><i class="material-icons small">delete</i></button>
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="8" class="text-center">No Consumable(s) found.</td>
                    </tr>
                    @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th width="30%">Name</th>
                            <th>Price</th>
                            <th>Tax Class</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">
        <div class="card">
            <div class="header">
                <h2 class="action_lbl">
                    New Consumable
                </h2>
            </div>
            <div class="body">
                <div class="row clearfix" style="margin-left: -10px;">
                    <form class="form-horizontal newForm" method="POST" action="{{ route('consumables.store') }}">
                        {{ method_field('POST') }}
                        {{ csrf_field() }}
                        @include('partials.branch-input')
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                <label for="consumable_name">Name</label>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="consumable_name" name="consumable_name" class="form-control" placeholder="Consumable Name" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                <label for="description">Description</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="description" name="description" class="form-control" placeholder="Description">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                <label for="consumable_price">Price</label>
                            </div>
                            <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="number" step="0.01" id="consumable_price" name="consumable_price" class="form-control" placeholder="Consumable Price"></input>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                <label for="taxable">Tax Class</label>
                            </div>
                            <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                <div class="form-line">
                                    <select name="tax_id" id="tax_id" class="form-control" data-live-search="true">
                                        <option value="">-- Select--</option>
                                        @foreach($taxRates as $tr)
                                        <option {{ $tr->id=='1'?'selected':'' }} value="{{ $tr->id }}" data-tax-type="{{ $tr->type }}" data-tax="{{ $tr->tax_rate }}" >{{ $tr->tax_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <input type="hidden" id="id" value="0">
                            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 col-md-offset-2">
                                <button type="submit" class="btn btn-success btn-block btn-lg waves-effect pull-left">Save</button>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                                <button type="reset" class="btn btn-danger btn-block btn-lg waves-effect pull-right">Clear</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page.scripts')
<script>
    $(function(){
        $('.btnEdit').on('click', function(){
            id = $(this).data('id');
            branch = $(this).data('branch');
            name = $(this).data('name');
            description = $(this).data('description');
            price = $(this).data('price');
            taxID = $(this).data('tax-id');

            $('.action_lbl').html('Edit Consumable - '+name);
            $('.newForm').attr('action',$(this).data('url'));
            $('.newForm').attr('method','POST');
            $('.newForm input[name=_method]').val('PUT');
            $('.newForm input[name=_token]').val('{{ csrf_token() }}');
            $('.newForm #id').val(id);
            $('.newForm #branch_id').val(branch);
            $('.newForm #consumable_name').val(name);
            $('.newForm #description').val(description);
            $('.newForm #consumable_price').val(price);
            $('.newForm select[name=tax_id][value='+taxID+']').prop('selected',true);
        });
    });
</script>
@endsection
