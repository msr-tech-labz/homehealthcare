@extends('layouts.main-layout')

@section('page_title','Rate Card - ')

@section('active_administration','active')

@section('plugin.styles')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<style>
    .inactive{
        background: #efcccc !important;
    }
</style>
@endsection

@section('content')
    @if (count($errors) > 0)
    <div class="row clearfix">
    	<div class="alert alert-danger">
    		<ul>
    			@foreach ($errors->all() as $error)
    				<li>{{ $error }}</li>
    			@endforeach
    		</ul>
    	</div>
    </div>
    @endif

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('settings') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-10">
                    Rate Card Master
                    <small>Service Rate Card</small>
                </h2>
                <div class="col-sm-1">
                    <a class="btn btn-success btn-lg waves-effect" data-toggle="modal" data-target="#rateCardModal">Add Rate Card</a>
                </div>
            </div>
            <div class="body">
                <table class="table table-bordered rateCardTable" style="zoom:90%;">
                    <thead>
                        <tr>
                            <th>Service</th>
                            <th>Amount</th>
                            <th>Tax - Type</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($ratecards as $index => $c)
                        <tr {{ $c->status == 0?'class="inactive"':'' }}>
                            <td>{{ $c->service->service_name ?? '' }}</td>
                            <td>&#8377; {!! $c->amount ?? '0.0' !!}</td>
                            <td>
                            @if(isset($c->tax))
                                {{ $c->tax->tax_rate }} {{ $c->tax->type=='Percentage'?'%':'' }}
                            @else
                                {{ '-' }}
                            @endif
                            </td>
                            <td>
                                 {{ $c->status==1?'Active':'Inactive' }}
                            </td>
                            <td>
                                @if($c->status == 1)
                                <button type="button" data-id="{{ Helper::encryptor('encrypt',$c->id) }}" data-branch="{{ $c->branch_id }}" data-service="{{ $c->service_id }}" data-amount="{{ $c->amount }}" data-tax-rate="{{ $c->tax_id }}" data-name="{{ $c->service->service_name }}" data-url="{{ route('ratecard.update',Helper::encryptor('encrypt',$c->id)) }}" class="btn btn-success btn-sm waves-effect btnEdit"><i class="material-icons small">edit</i></button>
                                @endif
                                <form class="form-inline hide" action="{{ route('ratecard.destroy',Helper::encryptor('encrypt',$c->id)) }}" method="POST" style="display: inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" onclick="return confirm('Are you sure?');" class="btn btn-danger btn-sm waves-effect btnRemove"><i class="material-icons small">delete</i></button>
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="9" class="text-center">No rate card(s) found.</td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Service</th>
                            <th>Amount</th>
                            <th>Tax - Type</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Rate Card Modal -->
<div class="modal fade" id="rateCardModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-md" role="document">
        <form class="form-horizontal rateCardForm" method="POST" action="{{ route('ratecard.store') }}">
            {{ method_field('POST') }}
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header bg-blue">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title action_lbl" id="largeModalLabel">New Rate Card</h4>
                </div>
                <div class="modal-body">
                    <div class="row clearfix">
                        <div class="col-sm-12 form-horizontal">
                            @include('partials.branch-input')
                            <br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="service_id">Service</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <select class="form-control" data-live-search="true" name="service_id" required="">
                                                <option value="">-- Please select --</option>
                                            @if(isset($services) && count($services))
                                                @foreach($services as $s)
                                                <option value="{{ $s->id }}">{{ $s->service_name }}</option>
                                                @endforeach
                                            @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="amount">Amount</label>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="number" step="0.01" min="0" name="amount" id="amount" class="form-control" placeholder="Rate in INR" required="">
                                        </div>
                                    </div>
                                </div>
                            </div><br>

                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="tax_id">Tax Type</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <select name="tax_id" id="tax_id" class="form-control">
                                                @foreach($taxRates as $tr)
                                                <option value="{{ $tr->id }}" data-tax-type="{{ $tr->type }}" data-tax="{{ $tr->tax_rate }}" >{{ $tr->tax_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success waves-effect btnSelectStaff">Continue</button>
                    <button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Cancel</button>
                </div>
            </form>
		</div>
	</div>
</div>
@endsection

@section('plugin.scripts')
<!-- Select Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<!-- Input Mask Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>
<!-- Moment Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<!-- Bootstrap Notify Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-notify/bootstrap-notify.js') }}"></script>
<script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
@endsection

@section('page.scripts')
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
<script>
    $(function(){
        if({{ count($ratecards) }}){
            var table = $('.rateCardTable').DataTable({
                "displayLength": 5,
            } );
        }

        $('.content').on('change','input[name=ratecard_status]', function(){
            var id = $(this).data('id');
            var status = $(this).is(':checked')?1:0;

            $.ajax({
                url: '{{ route('ratecard.update-status')}}',
                type: 'POST',
                data: {id: id, status: status, _token: '{{ csrf_token() }}'},
                success: function (data){
                    console.log(data);
                    showNotification('bg-green', 'Status changed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                },
                error: function (error){
                    console.log(error);
                }
            });
        });

        $('.content').on('click', '.btnEdit', function(){
            id = $(this).data('id');
            name = $(this).data('name');
            branch = $(this).data('branch');
            service = $(this).data('service');
            amount = $(this).data('amount');
            tax_rate = $(this).data('tax-rate');

            $('.action_lbl').html('Edit Rate Card - '+name);
            $('.rateCardForm').attr('action',$(this).data('url'));
            $('.rateCardForm').attr('method','POST');
            $('.rateCardForm input[name=_method]').val('PUT');
            $('.rateCardForm input[name=_token]').val('{{ csrf_token() }}');

            $('.rateCardForm #id').val(id);
            $('.rateCardForm #branch_id').val(branch);
            $('.rateCardForm select[name=service_id]').selectpicker('val',service);
            $('.rateCardForm input[name=amount]').val(amount);
            $('.rateCardForm select[name=tax_id]').selectpicker('val',tax_rate);

            $('#rateCardModal').modal('show');
        });

        $('#rateCardModal').on('hidden.bs.modal', function (e) {
            $('.action_lbl').html('New Rate Card');
            $('.rateCardForm').attr('action','{{ route('ratecard.store') }}');
            $('.rateCardForm').attr('method','POST');
            $('.rateCardForm input[name=_method]').val('PUT');
            $('.rateCardForm input[name=_token]').val('{{ csrf_token() }}');
            $('.rateCardForm #branch_id').val(branch);
            $('.rateCardForm select[name=service_id]').selectpicker('val','');
            $('.rateCardForm input[name=amount]').val('');
            $('.rateCardForm select[name=tax_id]').selectpicker('val',1);
        });
    });
</script>
@endsection
