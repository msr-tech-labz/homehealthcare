@extends('layouts.main-layout')

@section('page_title','Branch Master - ')

@section('active_administration','active')

@section('plugin.styles')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
@endsection

@section('content')

@if (count($errors) > 0)
<div class="row clearfix">
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('settings') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-10">
                    Branch Master
                    <small>Branch Info</small>
                </h2>
                <div class="col-sm-1">
                    <a class="btn btn-success btn-lg waves-effect" data-toggle="modal" data-target="#branchModal">Add Branch</a>
                </div>
            </div>
            <div class="body">
                <table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable">
                    <thead>
                        <tr>
                            <th width="4%">#</th>
							<th width="10%">Code</th>
                            <th width="15%">Branch</th>
                            <th width="20%">Email</th>
                            <th width="15%">Contact Person</th>
                            <th width="12%">Location</th>
                            <th width="10%">State</th>
                            <th width="5%">Status</th>
							<th width="10%">Action</th>
                        </tr>
                    </thead>
                    <tbody>						
						@if(isset($branch) && count($branch))
                        	@foreach($branch as $index => $b)
                            <tr>
	                            <td>{{ $index + 1 }}.</td>
								<td>{!! $b->branch_code ?? '-' !!}</td>
								<td>{!! $b->branch_name ?? '-' !!}</td>
								<td>{!! $b->branch_email ?? '-' !!}</td>
	                            <td>{!! '<b>'.$b->branch_contact_person.'</b><br><small>'.$b->branch_contact_number.'</small>' !!}</td>
	                            <td>{!! '<b>'.$b->branch_city.'</b><br><small>'.$b->branch_area.'</small>' !!}</td>
	                            <td>{!! $b->branch_state ?? '-' !!}</td>
	                            <td>
	                                <div class="switch">
	                                    <label><input type="checkbox" name="status" data-id="{{ Helper::encryptor('encrypt',$b->id) }}" {{ $b->status==1?'checked':'' }}><span class="lever"></span></label>
	                                </div>
	                            </td>
	                            <td>
	                                <button type="button" 
                                    data-id="{{ $b->id }}"
                                    data-branch-code="{{ $b->branch_code }}"
                                    data-branch-name="{{ $b->branch_name }}"
                                    data-address="{{ $b->branch_address }}"
                                    data-email="{{ $b->branch_email }}"
                                    data-contact-person="{{ $b->branch_contact_person }}" 
                                    data-contact-number="{{ $b->branch_contact_number }}"
                                    data-address="{{ $b->branch_address }}" 
                                    data-area="{{ $b->branch_area }}" 
                                    data-city="{{ $b->branch_city }}" 
                                    data-zipcode="{{ $b->branch_zipcode }}" 
                                    data-state="{{ $b->branch_state }}" 
                                    data-auth_signatory="{{ $b->auth_signatory }}" 
                                    data-signtory_pos="{{ $b->signtory_pos }}" 
                                    data-proforma_invoice_inits="{{ $b->proforma_invoice_inits }}" 
                                    data-proforma_invoice_start_no="{{ $b->proforma_invoice_start_no }}" 
                                    data-invoice_inits="{{ $b->invoice_inits }}" 
                                    data-invoice_start_no="{{ $b->invoice_start_no }}" 
                                    data-receipt_inits="{{ $b->receipt_inits }}" 
                                    data-receipt_start_no="{{ $b->receipt_start_no }}" 
                                    data-credit_inits="{{ $b->credit_inits }}" 
                                    data-credit_start_no="{{ $b->credit_start_no }}" 
                                    data-cutoff_period="{{ $b->cutoff_period }}" 
                                    data-patient_id_prefix="{{ $b->patient_id_prefix }}" 
                                    data-patient_id_start_no="{{ $b->patient_id_start_no }}" 
                                    data-employee_id_prefix="{{ $b->employee_id_prefix }}" 
                                    data-employee_id_start_no="{{ $b->employee_id_start_no }}" 
                                    data-invoice_payee="{{ $b->invoice_payee }}" 
                                    data-bank_name="{{ $b->bank_name }}" 
                                    data-bank_address="{{ $b->bank_address }}" 
                                    data-acc_num="{{ $b->acc_num }}" 
                                    data-ifsc_code="{{ $b->ifsc_code }}" 
                                    data-branch_state="{{ $b->branch_state }}" 
                                    data-pan_number="{{ $b->pan_number }}" 
                                    data-gstin="{{ $b->gstin }}" 
                                    data-cin="{{ $b->cin }}" 
                                    data-payment_due_date="{{ $b->payment_due_date }}" 
                                    data-url="{{ route('branch.update',Helper::encryptor('encrypt',$b->id)) }}" class="btn btn-success btn-sm waves-effect btnEdit"><i class="material-icons small">edit</i></button>
	                                <form class="form-inline" action="{{ route('branch.destroy',Helper::encryptor('encrypt',$b->id)) }}" method="POST" style="display: inline">
	                                    {{ method_field('DELETE') }}
	                                    {{ csrf_field() }}
	                                    <button type="submit" onclick="return confirm('Are you sure?');" class="btn btn-danger btn-sm waves-effect btnRemove"><i class="material-icons small">delete</i></button>
	                                </form>
	                            </td>
	                        </tr>
							@endforeach
						@else
                        <tr>
                            <td colspan="8" class="text-center">No branch record(s) found.</td>
                        </tr>
						@endif
                    </tbody>
                    <tfoot>
                        <tr>
							<th width="4%">#</th>
                            <th width="10%">Code</th>
							<th width="15%">Branch</th>
                            <th width="20%">Email</th>
                            <th width="15%">Contact Person</th>
                            <th width="12%">Location</th>
                            <th width="10%">State</th>
							<th width="5%">Status</th>
							<th width="10%">Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Rate Card Modal -->
<div class="modal fade" id="branchModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
        <form class="form-horizontal branchForm" method="POST" action="{{ route('branch.store') }}">
            {{ method_field('POST') }}
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header bg-blue">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title action_lbl" id="largeModalLabel">New Branch</h4>
                </div>
                <div class="modal-body">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#basic_details" data-toggle="tab" aria-expanded="false">
                                <i class="material-icons">assignment</i>BRANCH INFO
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#configaration" data-toggle="tab" aria-expanded="false">
                                <i class="material-icons">assignment</i>CONFIGURATION
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#billing" data-toggle="tab" aria-expanded="false">
                                <i class="material-icons">assignment</i>BANKING DETAILS
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade active in" id="basic_details">
                        <div class="row clearfix">
                            <div class="col-sm-12 form-horizontal">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                        <label for="branch_name">Branch Name</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="branch_name" class="form-control" placeholder="Branch Name">
                                            </div>
                                        </div>
                                    </div>
                                </div><br>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                        <label for="branch_code">Branch Code</label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="branch_code" class="form-control" placeholder="Branch Code">
                                            </div>
                                        </div>
                                    </div>
                                </div><br>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                        <label for="branch_email">Email</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="email" name="branch_email" class="form-control" placeholder="Email">
                                            </div>
                                        </div>
                                    </div>
                                </div><br>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                        <label for="branch_address">Address</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="branch_address" class="form-control" placeholder="Address">
                                            </div>
                                        </div>
                                    </div>
                                </div><br>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                        <label for="branch_area">Area</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="branch_area" class="form-control" placeholder="Area">
                                            </div>
                                        </div>
                                    </div>
                                </div><br>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                        <label for="branch_city">City</label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="branch_city" class="form-control" placeholder="City">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="branch_zipcode" class="form-control" placeholder="Zip Code">
                                            </div>
                                        </div>
                                    </div>
                                </div><br>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                        <label for="branch_state">State</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="branch_state" class="form-control" placeholder="State">
                                            </div>
                                        </div>
                                    </div>
                                </div><br>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                        <label for="branch_contact_person">Contact Person</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="branch_contact_person" class="form-control" placeholder="Branch Contact Person">
                                            </div>
                                        </div>
                                    </div>
                                </div><br>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                        <label for="branch_contact_number">Phone Number</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="branch_contact_number" class="form-control" placeholder="Phone Number">
                                            </div>
                                        </div>
                                    </div>
                                </div><br>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                        <label for="notes">Status</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="switch">
                                                <label><input type="checkbox" name="status" value="1" checked><span class="lever"></span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div><br>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="configaration">
                        <div class="row clearfix">
                            <div class="col-sm-6 form-horizontal">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                        <label for="auth_signatory">Authorized Signatory</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="auth_signatory" name="auth_signatory" class="form-control" placeholder="Eg. John Doe">
                                            </div>
                                        </div>
                                    </div>
                                </div><br>

                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                        <label for="signtory_pos">Signatory's Position</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="signtory_pos" name="signtory_pos" class="form-control" placeholder="Eg. CEO" >
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div><br>

                            <div class="col-sm-6 form-horizontal">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                        <label for="pan_number">PAN Number</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="pan_number" name="pan_number" class="form-control" placeholder="Eg. AACCR2314P">
                                            </div>
                                        </div>
                                    </div>
                                </div><br>

                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                        <label for="gstin">GSTIN</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="gstin" name="gstin" class="form-control" placeholder="Eg. 29AACCR23PSD001">
                                            </div>
                                        </div>
                                    </div>
                                </div><br>

                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                        <label for="cin">CIN</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="cin" name="cin" class="form-control" placeholder="Eg. CR23PSD001">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><br>

                            <div class="col-sm-12 form-horizontal">
                                <div class="col-sm-6">
                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
                                            <label for="proforma_invoice_inits">Proforma Invoice Prefix</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="proforma_invoice_inits" name="proforma_invoice_inits" class="form-control" placeholder="Eg. ABINP">
                                                </div>
                                            </div>
                                        </div>
                                    </div><br>

                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
                                            <label for="proforma_invoice_start_no">Proforma Invoice Start Number</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="number" id="proforma_invoice_start_no" name="proforma_invoice_start_no" class="form-control" placeholder="Eg. 1000">
                                                </div>
                                            </div>
                                        </div>
                                    </div><br>

                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
                                            <label for="invoice_inits">Service Invoice Prefix</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="invoice_inits" name="invoice_inits" class="form-control" placeholder="Eg. ABINS">
                                                </div>
                                            </div>
                                        </div>
                                    </div><br>

                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
                                            <label for="invoice_start_no">Service Invoice Start Number</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="number" id="invoice_start_no" name="invoice_start_no" class="form-control" placeholder="Eg. 1000">
                                                </div>
                                            </div>
                                        </div>
                                    </div><br>

                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
                                            <label for="receipt_inits">Receipt Prefix</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="receipt_inits" name="receipt_inits" class="form-control" placeholder="Eg. ABRE">
                                                </div>
                                            </div>
                                        </div>
                                    </div><br>

                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
                                            <label for="receipt_start_no">Receipt Start Number</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="number" id="receipt_start_no" name="receipt_start_no" class="form-control" placeholder="Eg. 1000">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><br>

                                <div class="col-sm-6">
                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
                                            <label for="credit_inits">CreditMemo Prefix</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="credit_inits" name="credit_inits" class="form-control" placeholder="Eg. ABCM">
                                                </div>
                                            </div>
                                        </div>
                                    </div><br>

                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
                                            <label for="credit_start_no">CreditMemo Start Number</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="number" id="credit_start_no" name="credit_start_no" class="form-control" placeholder="Eg. 1000">
                                                </div>
                                            </div>
                                        </div>
                                    </div><br>

                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
                                            <label for="patient_id_prefix">Patient ID Prefix</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="patient_id_prefix" name="patient_id_prefix" class="form-control" placeholder="Eg. ABRP">
                                                </div>
                                            </div>
                                        </div>
                                    </div><br>

                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
                                            <label for="patient_id_start_no">Patient ID Start No</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="number" id="patient_id_start_no" name="patient_id_start_no" class="form-control" placeholder="Eg. 1000">
                                                </div>
                                            </div>
                                        </div>
                                    </div><br>

                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
                                            <label for="employee_id_prefix">Employee ID Prefix</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="employee_id_prefix" name="employee_id_prefix" class="form-control" placeholder="Eg. TKOH">
                                                </div>
                                            </div>
                                        </div>
                                    </div><br>

                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                            <label for="gstin">Payment Due Date</label>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="number" id="payment_due_date" name="payment_due_date" class="form-control" placeholder="Eg. 7">
                                                </div>
                                            </div>
                                        </div>
                                    </div><br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="billing">
                        <div class="row clearfix">
                            <div class="col-sm-12 form-horizontal">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                        <label for="invoice_payee">Payee</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="invoice_payee" name="invoice_payee" class="form-control" placeholder="ABC Company">
                                            </div>
                                        </div>
                                    </div>
                                </div><br>

                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                        <label for="bank_name">Banking Name</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="bank_name" name="bank_name" class="form-control" placeholder="Eg. State Bank of India">
                                            </div>
                                        </div>
                                    </div>
                                </div><br>

                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                        <label for="bank_address">Bank Address</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="bank_address" name="bank_address" class="form-control" placeholder="Eg. HMT Area,Jalahalli,Bangalore,560013">
                                            </div>
                                        </div>
                                    </div>
                                </div><br>

                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                        <label for="acc_num">Account Number</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="number" id="acc_num" name="acc_num" class="form-control" placeholder="Eg. 1234567890">
                                            </div>
                                        </div>
                                    </div>
                                </div><br>

                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                        <label for="ifsc_code">IFSC Code</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="ifsc_code" name="ifsc_code" class="form-control" placeholder="Eg. SBI0001">
                                            </div>
                                        </div>
                                    </div>
                                </div><br>
                                <input type="hidden" name="id" id="id">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success waves-effect">Save</button>
                    <button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Cancel</button>
                </div>
            </form>
		</div>
	</div>
</div>
@endsection

@section('plugin.scripts')
<!-- Select Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<!-- Input Mask Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>
<!-- Moment Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<!-- Bootstrap Notify Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-notify/bootstrap-notify.js') }}"></script>
<script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
@endsection

@section('page.scripts')
<script>
    $(function(){
        $('.date').inputmask('dd-mm-yyyy', { placeholder: '__-__-____' });

        $('.content').on('change','input[name=status]', function(){
            var id = $(this).data('id');
            var status = $(this).is(':checked')?1:0;

            $.ajax({
                url: '{{ route('branch.update-status')}}',
                type: 'POST',
                data: {id: id, status: status, _token: '{{ csrf_token() }}'},
                success: function (data){
                    console.log(data);
                    showNotification('bg-green', 'Status changed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                },
                error: function (error){
                    console.log(error);
                }
            });
        });

        $('.content').on('change','input[name=status]', function(){
            if($(this).is(':checked')){
                $(this).val('1');
                $(this).attr('checked',true);
            }else{
                $(this).val('0');
                $(this).removeAttr('checked');
            }
        });

        $('.btnEdit').on('click', function(){
            id = $(this).data('id');
			code = $(this).data('branch-code');
            bname = $(this).data('branch-name');
			email = $(this).data('email');
            address = $(this).data('address');
            area = $(this).data('area');
			city = $(this).data('city');
            zipcode = $(this).data('zipcode');
			state = $(this).data('state');
			person = $(this).data('contact-person');
			number = $(this).data('contact-number');
            auth_signatory = $(this).data('auth_signatory');
            signtory_pos = $(this).data('signtory_pos');
            proforma_invoice_inits = $(this).data('proforma_invoice_inits');
            proforma_invoice_start_no    = $(this).data('proforma_invoice_start_no');
            invoice_inits = $(this).data('invoice_inits');
            invoice_start_no = $(this).data('invoice_start_no');
            receipt_inits = $(this).data('receipt_inits');
            receipt_start_no = $(this).data('receipt_start_no');
            credit_inits = $(this).data('credit_inits');
            credit_start_no = $(this).data('credit_start_no');
            cutoff_period = $(this).data('cutoff_period');
            patient_id_prefix = $(this).data('patient_id_prefix');
            patient_id_start_no = $(this).data('patient_id_start_no');
            employee_id_prefix = $(this).data('employee_id_prefix');
            employee_id_start_no = $(this).data('employee_id_start_no');
            invoice_payee = $(this).data('invoice_payee');
            bank_name = $(this).data('bank_name');
            bank_address = $(this).data('bank_address');
            acc_num = $(this).data('acc_num');
            ifsc_code = $(this).data('ifsc_code');
            pan_number = $(this).data('pan_number');
            gstin = $(this).data('gstin');
            cin = $(this).data('cin');
            payment_due_date = $(this).data('payment_due_date');
            $('.action_lbl').html('Edit Branch - '+bname);
            $('.branchForm').attr('action',$(this).data('url'));
            $('.branchForm').attr('method','POST');
            $('.branchForm input[name=_method]').val('PUT');
            $('.branchForm input[name=_token]').val('{{ csrf_token() }}');

            $('.branchForm #id').val(id);
			$('.branchForm input[name=branch_code]').val(code);
            $('.branchForm input[name=branch_name]').val(bname);
			$('.branchForm input[name=branch_email]').val(email);
            $('.branchForm input[name=branch_address]').val(address);
            $('.branchForm input[name=branch_area]').val(area);
			$('.branchForm input[name=branch_city]').val(city);
			$('.branchForm input[name=branch_zipcode]').val(zipcode);
			$('.branchForm input[name=branch_state]').val(state);
			$('.branchForm input[name=branch_contact_person]').val(person);
			$('.branchForm input[name=branch_contact_number]').val(number);
            $('.branchForm input[name=auth_signatory]').val(auth_signatory);
            $('.branchForm input[name=signtory_pos]').val(signtory_pos);
            $('.branchForm input[name=proforma_invoice_inits]').val(proforma_invoice_inits);
            $('.branchForm input[name=proforma_invoice_start_no]').val(proforma_invoice_start_no);
            $('.branchForm input[name=invoice_inits]').val(invoice_inits);
            $('.branchForm input[name=invoice_start_no]').val(invoice_start_no);
            $('.branchForm input[name=receipt_inits]').val(receipt_inits);
            $('.branchForm input[name=receipt_start_no]').val(receipt_start_no);
            $('.branchForm input[name=credit_inits]').val(credit_inits);
            $('.branchForm input[name=credit_start_no]').val(credit_start_no);
            $('.branchForm input[name=cutoff_period]').val(cutoff_period);
            $('.branchForm input[name=patient_id_prefix]').val(patient_id_prefix);
            $('.branchForm input[name=patient_id_start_no]').val(patient_id_start_no);
            $('.branchForm input[name=employee_id_prefix]').val(employee_id_prefix);
            $('.branchForm input[name=employee_id_start_no]').val(employee_id_start_no);
            $('.branchForm input[name=invoice_payee]').val(invoice_payee);
            $('.branchForm input[name=bank_name]').val(bank_name);
            $('.branchForm input[name=bank_address]').val(bank_address);
            $('.branchForm input[name=acc_num]').val(acc_num);
            $('.branchForm input[name=ifsc_code]').val(ifsc_code);
            $('.branchForm input[name=pan_number]').val(pan_number);
            $('.branchForm input[name=gstin]').val(gstin);
            $('.branchForm input[name=cin]').val(cin);
            $('.branchForm input[name=payment_due_date]').val(payment_due_date);

            $('#branchModal').modal();
        });
    });
</script>
@endsection
