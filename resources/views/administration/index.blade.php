@extends('layouts.main-layout')

@section('page_title','Administration -     ')

@section('active_administration','active')

@section('page.styles')
@endsection

@section('content')
<style>
    .info-box{
        cursor: pointer;
        text-decoration: none !important;
        margin-bottom: 10px !important;
    }
    .info-box .content .text{
        margin-top: 2px;
        font-size: 15px;
        color: #777;
    }
    .info-box .content .number{
        margin-top: -2px;
        font-size: 20px
    }
</style>
<div class="block-header">
    <h2>Administration</h2>
</div>

<div class="block-body">
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{ route('profile.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-red">
                <i class="material-icons">person</i>
            </div>
            <div class="content">
                <div class="number">Profile Settings</div>
                <div class="text">Manage your company's profile</div>
            </div>
        </a>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{ route('acl.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-red">
                <i class="material-icons">security</i>
            </div>
            <div class="content">
                <div class="number">User Access Roles</div>
                <div class="text">Manage user's roles and permissions</div>
            </div>
        </a>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{ route('user.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-red">
                <i class="material-icons">people</i>
            </div>
            <div class="content">
                <div class="number">Users</div>
                <div class="text">Manage user accounts</div>
            </div>
        </a>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{ route('billingProfile.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-red">
                <i class="material-icons">contact_mail</i>
            </div>
            <div class="content">
                <div class="number">Vendor Profile</div>
                <div class="text">Manage vendor profiles</div>
            </div>
        </a>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{ route('plugin.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-red">
                <i class="material-icons">extension</i>
            </div>
            <div class="content">
                <div class="number">Plugins</div>
                <div class="text">Manage plugins or extensions</div>
            </div>
        </a>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{ route('feedback.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-red">
                <i class="material-icons">feedback</i>
            </div>
            <div class="content">
                <div class="number">Feedback</div>
                <div class="text">Feedback or Feature requests</div>
            </div>
        </a>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{ route('mailers.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-red">
                <i class="material-icons">mail</i>
            </div>
            <div class="content">
                <div class="number">Email Subscribers</div>
                <div class="text">Mail Receivers' List</div>
            </div>
        </a>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{ route('composition.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-red">
                <i class="material-icons">build</i>
            </div>
            <div class="content">
                <div class="number">Management Info</div>
                <div class="text">Manage Info Cadre</div>
            </div>
        </a>
    </div>
    <div class="clearfix"></div><br>

    <div class="block-header">
        <h2>Organization</h2>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{ route('branch.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-cyan">
                <i class="material-icons">location_city</i>
            </div>
            <div class="content">
                <div class="number">Branch</div>
                <div class="text">Manage company branch</div>
            </div>
        </a>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{ route('language.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-cyan">
                <i class="material-icons">language</i>
            </div>
            <div class="content">
                <div class="number">Languages</div>
                <div class="text">Languages known to your employees</div>
            </div>
        </a>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{ route('service.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-cyan">
                <i class="material-icons">format_list_bulleted</i>
            </div>
            <div class="content">
                <div class="number">Services</div>
                <div class="text">Manage services provided and their categories</div>
            </div>
        </a>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{ route('consumables.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-cyan">
                <i class="material-icons">swap_vertical_circle</i>
            </div>
            <div class="content">
                <div class="number">Consumables</div>
                <div class="text">Record of consumables and prices</div>
            </div>
        </a>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{ route('surgicals.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-cyan">
                <i class="material-icons">content_cut</i>
            </div>
            <div class="content">
                <div class="number">Equipments and Surgicals</div>
                <div class="text">Record of Equipment, Surgicals and their prices</div>
            </div>
        </a>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{ route('labtests.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-cyan">
                <i class="material-icons">gamepad</i>
            </div>
            <div class="content">
                <div class="number">Laboratory Investigation</div>
                <div class="text">Record of tests their prices</div>
            </div>
        </a>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{ route('ratecard.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-cyan">
                <i class="material-icons">attach_money</i>
            </div>
            <div class="content">
                <div class="number">Rate Card</div>
                <div class="text">Manage Service Rates</div>
            </div>
        </a>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{ route('referral.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-cyan">
                <i class="material-icons">group_add</i>
            </div>
            <div class="content">
                <div class="number">Referral</div>
                <div class="text">Manage Referral Category and Source</div>
            </div>
        </a>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{ route('tasks.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-cyan">
                <i class="material-icons">assignment</i>
            </div>
            <div class="content">
                <div class="number">Tasks Master</div>
                <div class="text">Manage Staff tasks and routines</div>
            </div>
        </a>
    </div>

    <div class="clearfix"></div><br>

    <div class="block-header">
        <h2>HR Masters</h2>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{ route('designation.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-indigo">
                <i class="material-icons">business_center</i>
            </div>
            <div class="content">
                <div class="number">Designations</div>
                <div class="text">Manage Employee Designations</div>
            </div>
        </a>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{ route('department.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-indigo">
                <i class="material-icons">domain</i>
            </div>
            <div class="content">
                <div class="number">Departments</div>
                <div class="text">Manage departments</div>
            </div>
        </a>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{ route('skill.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-indigo">
                <i class="material-icons">widgets</i>
            </div>
            <div class="content">
                <div class="number">Skills</div>
                <div class="text">Manage skills</div>
            </div>
        </a>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xskills-12">
        <a href="{{ route('specialization.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-indigo">
                <i class="material-icons">developer_board</i>
            </div>
            <div class="content">
                <div class="number">Specialization</div>
                <div class="text">Manage Employee Specializations</div>
            </div>
        </a>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xskills-12">
        <a href="{{ route('holiday.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-indigo">
                <i class="material-icons">beach_access</i>
            </div>
            <div class="content">
                <div class="number">Holiday Master</div>
                <div class="text">Manage Company's Holidays</div>
            </div>
        </a>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xskills-12">
        <a href="{{ route('employeesource.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-indigo">
                <i class="material-icons">link</i>
            </div>
            <div class="content">
                <div class="number">Employee Source</div>
                <div class="text">Manage Employee Source</div>
            </div>
        </a>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xskills-12">
        <a href="{{ route('trainings.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-indigo">
                <i class="material-icons">forum</i>
            </div>
            <div class="content">
                <div class="number">Training</div>
                <div class="text">Manage Employee Trainingss</div>
            </div>
        </a>
    </div>

    <div class="clearfix"></div><br>

    <div class="block-header">
        <h2>Accounting</h2>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{ route('taxrate.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-deep-purple">
                <i class="material-icons">view_list</i>
            </div>
            <div class="content">
                <div class="number">Tax Rates</div>
                <div class="text">Manage different Rates</div>
            </div>
        </a>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{ route('costcentre.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-deep-purple">
                <i class="material-icons">view_list</i>
            </div>
            <div class="content">
                <div class="number">Cost Centre</div>
                <div class="text">Manage Tally Cost Centre</div>
            </div>
        </a>
    </div>

    <div class="clearfix"></div><br>

    <div class="block-header">
        <h2>Verification</h2>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{ route('agency.index') }}" class="info-box hover-expand-effect">
            <div class="icon bg-light-green">
                <i class="material-icons">verified_user</i>
            </div>
            <div class="content">
                <div class="number">Verification Agencies</div>
                <div class="text">Manage different Employee Verification agencies</div>
            </div>
        </a>
    </div>
</div>
@endsection
