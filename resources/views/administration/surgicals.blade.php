@extends('layouts.main-layout')

@section('page_title','Equipments and Surgicals Master - ')

@section('active_administration','active')

@section('content')
<div class="row clearfix">
    <div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('settings') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-10">
                    Equipments and Surgicals Master
                    <small>Manage Equipments and Surgicals</small>
                </h2>
            </div>
            <div class="body">
                <table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable" style="zoom:90%;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th width="30%">Name</th>
                            <th>Price</th>
                            <th>Tax Class</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse($surgicals as $index => $s)
                        <tr>
                        <td>{{ $index + 1 }}.</td>
                        <td>
                            {{ $s->surgical_name }}<br>
                            <small>{!! $s->description ?? '' !!}</small>
                        </td>
                        <td>{{ $s->surgical_price }}</td>
                        <td>{{ $s->tax->tax_rate ?? '' }} {{ (isset($s->tax) && $s->tax->type=='Percentage')?'%':'' }}</td>
                        <td>
                            <button type="button" data-id="{{ Helper::encryptor('encrypt',$s->id) }}" data-branch="{{ $s->branch_id }}" data-name="{{ $s->surgical_name }}" data-description="{{ $s->description }}" data-price="{{ $s->surgical_price }}" data-tax-id="{{ $s->tax_id }}" data-url="{{ route('surgicals.update',Helper::encryptor('encrypt',$s->id)) }}" class="btn btn-success btn-sm waves-effect btnEdit"><i class="material-icons small">edit</i></button>
                            <form class="form-inline" action="{{ route('surgicals.destroy',Helper::encryptor('encrypt',$s->id)) }}" method="POST" style="display: inline">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit" onclick="return confirm('Are you sure?');" class="btn btn-danger btn-sm waves-effect btnRemove"><i class="material-icons small">delete</i></button>
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="8" class="text-center">No Equipment(s) and Surgical(s) found.</td>
                    </tr>
                    @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th width="30%">Name</th>
                            <th>Price</th>
                            <th>Tax Class</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">
        <div class="card">
            <div class="header">
                <h2 class="action_lbl">
                    New Equipment or Surgical
                </h2>
            </div>
            <div class="body">
                <div class="row clearfix" style="margin-left: -10px;">
                    <form class="form-horizontal newForm" method="POST" action="{{ route('surgicals.store') }}">
                        {{ method_field('POST') }}
                        {{ csrf_field() }}
                        @include('partials.branch-input')
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                <label for="surgical_name">Name</label>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="surgical_name" name="surgical_name" class="form-control" placeholder="Item Name" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                <label for="description">Description</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="description" name="description" class="form-control" placeholder="Description">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                <label for="surgical_price">Price</label>
                            </div>
                            <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="number" step="0.01" id="surgical_price" name="surgical_price" class="form-control" placeholder="Item Price"></input>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                <label for="taxable">Tax Class</label>
                            </div>
                            <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                <div class="form-line">
                                    <select name="tax_id" id="tax_id" class="form-control" data-live-search="true">
                                        <option value="">-- Select--</option>
                                        @foreach($taxRates as $tr)
                                        <option {{ $tr->id=='1'?'selected':'' }} value="{{ $tr->id }}" data-tax-type="{{ $tr->type }}" data-tax="{{ $tr->tax_rate }}" >{{ $tr->tax_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <input type="hidden" id="id" value="0">
                            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 col-md-offset-2">
                                <button type="submit" class="btn btn-success btn-block btn-lg waves-effect pull-left">Save</button>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                                <button type="reset" class="btn btn-danger btn-block btn-lg waves-effect pull-right">Clear</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page.scripts')
<script>
    $(function(){
        $('.btnEdit').on('click', function(){
            id = $(this).data('id');
            branch = $(this).data('branch');
            name = $(this).data('name');
            description = $(this).data('description');
            price = $(this).data('price');
            taxID = $(this).data('tax-id');

            $('.action_lbl').html('Edit Consumable - '+name);
            $('.newForm').attr('action',$(this).data('url'));
            $('.newForm').attr('method','POST');
            $('.newForm input[name=_method]').val('PUT');
            $('.newForm input[name=_token]').val('{{ csrf_token() }}');
            $('.newForm #id').val(id);
            $('.newForm #branch_id').val(branch);
            $('.newForm #surgical_name').val(name);
            $('.newForm #description').val(description);
            $('.newForm #surgical_price').val(price);
            $('.newForm select[name=tax_id][value='+taxID+']').prop('selected',true);
        });
    });
</script>
@endsection
