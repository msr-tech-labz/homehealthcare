@extends('layouts.main-layout')

@section('page_title','Tasks Master - ')

@section('active_administration','active')

@section('content')
    @if (count($errors) > 0)
    <div class="row clearfix">
    	<div class="alert alert-danger">
    		<ul>
    			@foreach ($errors->all() as $error)
    				<li>{{ $error }}</li>
    			@endforeach
    		</ul>
    	</div>
    </div>
    @endif

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('settings') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-10">
                    Tasks Master
                    <small>Employee tasks</small>
                </h2>
            </div>
            <div class="body">
                <form class="form-inline taskForm" method="POST" action="{{ route('tasks.store') }}" style="display: inline">
                    {{ method_field('POST') }}
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="0" />
                    <table class="table table-bordered condensed">
                        <thead>
                            <tr>
                                <th width="30%" class="text-center" style="padding:3px !important;">Task Name</th>
                                <th class="text-center" style="padding:3px !important;">Category</th>
                                <th width="40%" class="text-center" style="padding:3px !important;">Description</th>
                                <th width="10%" class="text-center" style="padding:2px !important;"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <input type="text" class="form-control input-sm" style="width:99%" id="task_name" name="task_name" size="150" value="" placeholder="Task Name"/>
                                </td>
                                <td>
                                    <input type="text" class="form-control input-sm" style="width:99%" id="category" name="category" size="50" value="" placeholder="Category"/>
                                </td>
                                <td class="text-center">
                                    <textarea type="text" class="form-control input-sm" style="width:99%" id="description" name="description" placeholder="Task description"></textarea>
                                </td>
                                <td class="text-center">
                                    <button type="submit" class="btn btn-primary btnAdd">+ Add</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
                <table class="table table-bordered table-striped table-hover table-condensed dataTable" style="zoom:90%;">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="25%">Name</th>
                            <th>Category</th>
                            <th>Description</th>
                            <th width="15%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                @if(count($tasks))
                    {? $count = 0; ?}
                    @foreach ($tasks as $task)
                    {? $count++; ?}
                    <tr>
                        <td>{{ $count }}.</td>
                        <td>{{ $task->task_name }}</td>
                        <td>{{ $task->category }}</td>
                        <td>{{ $task->description }}</td>
                        <td>
                            <button type="button" data-id="{{ Helper::encryptor('encrypt',$task->id) }}" data-task-name="{{ $task->task_name }}" data-category="{{ $task->category }}" data-description="{{ $task->description }}" data-url="{{ route('tasks.update',Helper::encryptor('encrypt',$task->id)) }}" class="btn btn-success btn-sm waves-effect btnEdit"><i class="material-icons small">edit</i></button>
                            <form class="form-inline" action="{{ route('tasks.destroy',Helper::encryptor('encrypt',$task->id)) }}" method="POST" style="display: inline">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit" onclick="return confirm('Are you sure?');" class="btn btn-danger btn-sm waves-effect btnRemove"><i class="material-icons small">delete</i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th width="5%">#</th>
                            <th width="30%">Name</th>
                            <th>Category</th>
                            <th>Description</th>
                            <th width="20%">Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page.scripts')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(function(){
        var availableCategories = [];
        @if(count($categories))
            @foreach ($categories as $cat)
            availableCategories.push('{{ $cat }}');
            @endforeach
        @endif

        $('#category').autocomplete({
            minLength: 3,
            source: availableCategories
        });

        $('.btnEdit').on('click', function(){
            id = $(this).data('id');
            name = $(this).data('task-name');
            category = $(this).data('category');
            description = $(this).data('description');

            $('.action_lbl').html('Edit Task - '+name);
            $('.taskForm').attr('action',$(this).data('url'));
            $('.taskForm').attr('method','POST');
            $('.taskForm input[name=_method]').val('PUT');
            $('.taskForm input[name=_token]').val('{{ csrf_token() }}');
            $('.taskForm #id').val(id);
            $('.taskForm #task_name').val(name);
            $('.taskForm #category').val(category);
            $('.taskForm #description').val(description);
            $('.btnAdd').text('Update Task');
            $('.btnAdd').removeClass('btn-primary');
            $('.btnAdd').addClass('btn-success');
            $('.taskForm tbody tr:eq(0) td').each(function(i){
                $(this).css('background-color','#f5efbb');
            });
        });
    });
</script>
@endsection
