@extends('layouts.main-layout')

@section('page_title','Management Composition Master - ')

@section('active_administration','active')

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('settings') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-10">
                    Management Info Master
                    <small>Manage the cadre to be available at important sections (e.g. MIS, Leads Converted by etc)</small>
                </h2>
            </div>
            <div class="body">
                <div class="card mis_hr_card">
                    <div class="header bg-red" style="padding: 10px;">
                        <h2>
                            MIS HR Cadre <small>Staff belonging to the below designations cadre will be shown in the MIS HR Report(Max 4)</small>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="col-md-10 compose_mis_hr">
                            @foreach($designations as $designation)
                            <input type="checkbox" name="misHr" value="{{ $designation->id }}" id="designation_{{ $designation->id }}" class="filled-in chk-col-red" @if(in_array($designation->id, $data['cadreHR'])) checked="checked" @endif>
                            <label class="col-md-3" for="designation_{{ $designation->id }}">{{ $designation->designation_name }}</label>
                            @endforeach
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-primary" id="compose_mis_hr_btn" type="button">SAVE</button>
                            <input type="hidden" name="mis_hr" value="mis_hr">
                        </div>
                        <div class="row clearfix"></div>
                    </div>
                </div>

                <div class="card lead_visibility_card">
                    <div class="header bg-green" style="padding: 10px;">
                        <h2>
                            Lead Visibility <small>Leads can be viewed only by the Assigned Managers, Allocated Caregivers and the Admin by default. In case any other department/team wishes to view the leads, please check them below.</small>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="col-md-10 compose_lead_visibility">
                            @foreach($departments as $department)
                            <input type="checkbox" name="leadsVisibility" value="{{ $department->id }}" id="leadvisibility_{{ $department->id }}" class="filled-in chk-col-green" @if(in_array($department->id, $data['cadreLeadsVisibility'])) checked="checked" @endif>
                            <label class="col-md-3" for="leadvisibility_{{ $department->id }}">{{ $department->department_name }}</label>
                            @endforeach
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-primary" id="compose_lead_visibility_btn" type="button">SAVE</button>
                            <input type="hidden" name="leads_viewable_by" value="leads_viewable_by">
                        </div>
                        <div class="row clearfix"></div>
                    </div>
                </div>

                <div class="card leads_converted_by">
                    <div class="header bg-blue-grey" style="padding: 10px;">
                        <h2>
                            Leads Conversion Cadre <small>Staff from the below checked Departments will be shown under "Converted by" drop down while creating leads</small>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="col-md-10 compose_leads_converted_by">
                            @foreach($departments as $department)
                            <input type="checkbox" name="leadsConvertedBy" value="{{ $department->id }}" id="department_{{ $department->id }}" class="filled-in chk-col-blue-grey" @if(in_array($department->id, $data['cadreLeadsConvertedBy'])) checked="checked" @endif>
                            <label class="col-md-3" for="department_{{ $department->id }}">{{ $department->department_name }}</label>
                            @endforeach
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-primary" id="compose_leads_converted_by_btn" type="button">SAVE</button>
                            <input type="hidden" name="leads_converted_by" value="leads_converted_by">
                        </div>
                        <div class="row clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page.scripts')
<script>
    $(function(){
        $('.content').on('click','#compose_mis_hr_btn',function(){
            var hrlist = [];
            $('input[name="misHr"]:checked').each(function() {
               hrlist.push(this.value);
            });
            if(hrlist.length <= 4){
                $.ajax({
                    url: '{{ route('composition.update') }}',
                    type: 'POST',
                    data: {list: hrlist, type:'mis_hr',_token: '{{ csrf_token() }}'},
                    success: function (){
                        showNotification('bg-orange', 'MIS Hr Cadre Updated successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                    },
                    error: function (xhr,status,error){
                        var err = xhr.responseText;
                        showNotification('bg-red',err, 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                    }
                });      
            }else{
                alert('Max 4 selections are allowed');
            }
        });

        $('.content').on('click','#compose_lead_visibility_btn',function(){
            var visibilitylist = [];
            $('input[name="leadsVisibility"]:checked').each(function() {
             visibilitylist.push(this.value);
            });
            if(visibilitylist.length > 0){
                $.ajax({
                    url: '{{ route('composition.update') }}',
                    type: 'POST',
                    data: {list: visibilitylist, type:'leads_viewable_by',_token: '{{ csrf_token() }}'},
                    success: function (){
                        showNotification('bg-green', 'Leads Viewable by Cadre Updated successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                    },
                    error: function (xhr,status,error){
                        var err = xhr.responseText;
                        showNotification('bg-red',err, 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                    }
                });
            }else{
                alert('Please select cadre(s)');
            }
        });

        $('.content').on('click','#compose_leads_converted_by_btn',function(){
            var lcblist = [];
            $('input[name="leadsConvertedBy"]:checked').each(function() {
               lcblist.push(this.value);
            });
            if(lcblist.length <= 4){
                $.ajax({
                    url: '{{ route('composition.update') }}',
                    type: 'POST',
                    data: {list: lcblist, type:'leads_converted_by',_token: '{{ csrf_token() }}'},
                    success: function (){
                        showNotification('bg-blue-grey', 'Leads Converted Cadre Updated successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                    },
                    error: function (xhr,status,error){
                        var err = xhr.responseText;
                        showNotification('bg-red',err, 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                    }
                });
            }else{
                alert('Max 4 selections are allowed');
            }
        });
    });
</script>
@endsection
