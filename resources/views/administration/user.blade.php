@extends('layouts.main-layout')

@section('page_title','Users - ')

@section('active_administration','active')

@section('plugin.styles')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />
@endsection

@section('content')

@if (count($errors) > 0)
<div class="row clearfix">
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('settings') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-10">
                    User Accounts
                    <small>List of application user accounts</small>
                </h2>
                <div class="col-sm-1">
                    <a class="btn btn-success btn-lg waves-effect newUser hide" data-toggle="modal" data-target="#userModal" data-newUser="true">Add User</a>
                </div>
            </div>
            <div class="body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#actives" data-toggle="tab">
                            <i class="material-icons">check_box</i> ACTIVE
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#non_actives" data-toggle="tab">
                            <i class="material-icons">do_not_disturb</i> INACTIVE
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="actives">
                        <b style="margin-left: 20px;">Active Users</b>
						<a href="{{ route('user.reload-privileges') }}" class="btn bg-deep-purple pull-right" style="margin-right: 20px" onclick="javascript: return confirm('Do you really want to continue?')">Reload Privileges</a>
                        <p>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="body">
                                    <table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable alist">
                                        <thead>
                                            <tr>
                                                <th width="4%">#</th>
                                                <th width="4%">ID</th>
                                                <th width="15%">Name</th>
                                                @if(isset($branches) && count($branches))
                                                <th width="15%">Branch</th>
                                                @endif
                                                <th width="20%">Email</th>
                                                <th width="15%">Mobile</th>
                                                <th width="12%">Role</th>
                                                <th width="10%">Status</th>
                                                <th width="20%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {? $i = 1; ?}
                                            @forelse($users->where('status',1) as $index => $u)
                                            <tr>
                                                <td>{{ $i++ }}.</td>
                                                <td>{{ $u->caregiver->employee_id ?? '' }}</td>
                                                <td>{{ $u->full_name }}</td>
                                                @if(isset($branches) && count($branches))
                                                <td>{!! $u->branch->branch_name ?? '-' !!}</td>
                                                @endif
                                                <td>{!! $u->email ?? '-' !!}</td>
                                                <td>{!! $u->caregiver->mobile_number ?? '-' !!}</td>
                                                <td>{!! $u->role->display_name ?? '-' !!}</td>
                                                @if($u->user_type != 'Admin' && $u->user_type != 'Aggregator')
                                                <td>
                                                    <div class="switch">
                                                        <label><input type="checkbox" name="status" data-id="{{ Helper::encryptor('encrypt',$u->id) }}" {{ $u->status==1?'checked':'' }}><span class="lever"></span></label>
                                                    </div>
                                                </td>
                                                <td>
<button type="button" data-user-id="{{ $u->id }}" data-id="{{ Helper::encryptor('encrypt',$u->id) }}" data-full-name="{{ $u->full_name }}"  data-email="{{ $u->email }}" data-mobile="{{ $u->mobile }}" data-role="{{ $u->role_id }}" data-url="{{ route('user.update', ['id' => Helper::encryptor('encrypt',$u->id)])  }}" class="btn btn-success btn-sm waves-effect btnEdit"><i class="material-icons small">edit</i></button>
                                                    <form class="form-inline" action="{{ route('user.destroy') }}" method="POST" style="display: inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="id" value="{{ isset($u)?Helper::encryptor('encrypt',$u->id):0 }}"/>
                                                        <button type="submit" onclick="return confirm('Are you sure?');" class="btn btn-danger btn-sm waves-effect btnRemove hide"><i class="material-icons small">delete</i></button>
                                                    </form>
                                                </td>
                                                @else
                                                <td></td>
                                                <td></td>
                                                @endif
                                            </tr>
                                            @empty
                                            <tr>
                                                <td colspan="8" class="text-center">No user(s) found.</td>
                                            </tr>
                                            @endforelse
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th width="5%">#</th>
                                                <th width="4%">ID</th>
                                                <th width="15%">Name</th>
                                                @if(isset($branches) && count($branches))
                                                <th width="15%">Branch</th>
                                                @endif
                                                <th width="20%">Email</th>
                                                <th width="15%">Mobile</th>
                                                <th width="12%">Role</th>
                                                <th width="10%">Status</th>
                                                <th width="20%">Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        </p>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="non_actives">
                        <b style="margin-left: 20px;">Inactive or Disabled Users</b>
                        <p>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="body">
                                    <table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable ilist">
                                        <thead>
                                            <tr>
                                                <th width="4%">#</th>
                                                <th width="4%">ID</th>
                                                <th width="15%">Name</th>
                                                @if(isset($branches) && count($branches))
                                                <th width="15%">Branch</th>
                                                @endif
                                                <th width="20%">Email</th>
                                                <th width="15%">Mobile</th>
                                                <th width="12%">Role</th>
                                                <th width="10%">Status</th>
                                                <th width="20%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                                {? $i = 1; ?}
                                            @forelse($users->where('status',0) as $index => $u)
                                            <tr>
                                                <td>{{ $i++ }}.</td>
                                                <td>{{ $u->caregiver->employee_id ?? '-' }}</td>
                                                <td>{{ $u->full_name }}</td>
                                                @if(isset($branches) && count($branches))
                                                <td>{!! $u->branch->branch_name ?? '-' !!}</td>
                                                @endif
                                                <td>{!! $u->email ?? '-' !!}</td>
                                                <td>{!! $u->mobile ?? '-' !!}</td>
                                                <td>{!! $u->role->display_name ?? '-' !!}</td>
                                                <td>
                                                    <div class="switch">
                                                        <label><input type="checkbox" name="status" data-id="{{ Helper::encryptor('encrypt',$u->id) }}" {{ $u->status==1?'checked':'' }}><span class="lever"></span></label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" data-id="{{ Helper::encryptor('encrypt',$u->id) }}" data-full-name="{{ $u->full_name }}"  data-email="{{ $u->email }}" data-mobile="{{ $u->mobile }}" data-role="{{ $u->role_id }}" data-url="{{ route('user.update', ['id' => Helper::encryptor('encrypt',$u->id)]) }}" class="btn btn-success btn-sm waves-effect btnEdit"><i class="material-icons small">edit</i></button>
                                                    <form class="form-inline" action="{{ route('user.destroy') }}" method="POST" style="display: inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="id" value="{{ isset($u)?Helper::encryptor('encrypt',$u->id):0 }}"/>
                                                        <button type="submit" onclick="return confirm('Are you sure?');" class="btn btn-danger btn-sm waves-effect btnRemove hide"><i class="material-icons small">delete</i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                            @empty
                                            <tr>
                                                <td colspan="8" class="text-center">No user(s) found.</td>
                                            </tr>
                                            @endforelse
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th width="5%">#</th>
                                                <th width="4%">ID</th>
                                                <th width="15%">Name</th>
                                                @if(isset($branches) && count($branches))
                                                <th width="15%">Branch</th>
                                                @endif
                                                <th width="20%">Email</th>
                                                <th width="15%">Mobile</th>
                                                <th width="12%">Role</th>
                                                <th width="10%">Status</th>
                                                <th width="20%">Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- User Modal -->
<div class="modal fade" id="userModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-md" role="document">
        <form class="form-horizontal userForm" method="POST" action="{{ route('user.store') }}">
            {{ method_field('POST') }}
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header bg-blue">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title action_lbl" id="largeModalLabel">New User</h4>
                </div>
                <div class="modal-body">
                    <div class="row clearfix">
                        <div class="col-sm-12 form-horizontal">
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="notes">Full Name</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="full_name" class="form-control" placeholder="Full Name">
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="notes">Email</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input autocomplete="off" id="email" onkeyup="checkemail();" type="email" name="email" class="form-control" placeholder="Email">
                                        </div>
										<span id="email_status" style="font-weight: bold;"></span>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="notes">Password</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="password" class="form-control" placeholder="Password">
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="notes">Role</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <select name="role_id" class="form-control show-tick">
                                                <option value="">-- Please select --</option>
                                            @if(isset($roles) && count($roles))
                                                @foreach($roles as $r)
                                                <option value="{{ $r->id }}">{{ $r->display_name }}</option>
                                                @endforeach
                                            @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="notes">Status</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="switch">
                                            <label><input type="checkbox" name="status" value="1" checked><span class="lever"></span></label>
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success waves-effect">Save</button>
                    <button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Cancel</button>
                </div>
            </form>
		</div>
	</div>
</div>
@endsection

@section('plugin.scripts')
<!-- Select Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<!-- Input Mask Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>
<!-- Moment Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<!-- Bootstrap Notify Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-notify/bootstrap-notify.js') }}"></script>
<script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
@endsection

@section('page.scripts')
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        if('{{ count($users->where('status',1)) > 0 }}'){
            $('.alist').DataTable();
        }
        if('{{ count($users->where('status',0)) > 0 }}'){
            $('.ilist').DataTable();
        }
    } );
</script>
<script>
    $(function(){
        $('.content').on('click','.newUser', function(){
            $("input[name=email]").attr('readonly',false);
        });
        @if(isset($u))
        //$("input[name=email]").attr('readonly',true);
        @endif
        $('.date').inputmask('dd-mm-yyyy', { placeholder: '__-__-____' });

        $('.content').on('change','input[name=status]', function(){
            var id = $(this).data('id');
            var status = $(this).is(':checked')?1:0;

            $.ajax({
                url: '{{ route('user.update-status')}}',
                type: 'POST',
                data: {id: id, status: status, _token: '{{ csrf_token() }}'},
                success: function (state){
                    if(state == "userHasSchedule"){
                        showNotification('bg-red', 'The Person is already assigned to a schedule.', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                    }
                    if(state == "userReachedMaxActive"){
                        showNotification('bg-red', 'You have already reached the maximum number of active users,please upgrade the package or inactive few users to activate others', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                    }
                    if(state == "statusChanged"){
                        showNotification('bg-green', 'Status changed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                    }
                    if(state == "error"){
                        showNotification('bg-red', 'Something went Wrong !', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                    }
                    window.setTimeout(function(){location.reload()},3000);
                },
                error: function (error){
                    console.log(error);
                }
            });
        });

        $('.content').on('change','input[name=status]', function(){
            if($(this).is(':checked')){
                $(this).val('1');
                $(this).attr('checked',true);
            }else{
                $(this).val('0');
                $(this).removeAttr('checked');
            }
        });

        $('.table').on('click', '.btnEdit', function(){
            id = $(this).data('id');
            fname = $(this).data('full-name');
            email = $(this).data('email');
            role = $(this).data('role');
			branch = $(this).data('branch');

            $('.action_lbl').html('Edit User - '+fname);
            $('.userForm').attr('action',$(this).data('url'));
            $('.userForm').attr('method','POST');
            $('.userForm input[name=_method]').val('POST');
            $('.userForm input[name=_token]').val('{{ csrf_token() }}');

            $('.userForm #id').val(id);
            $('.userForm input[name=full_name]').val(fname);
            $('.userForm input[name=email]').val(email);
            $('.userForm select[name=role_id]').selectpicker('val',role);

            $('#userModal').modal('show');
        });
    });

    $('#userModal').on('hidden.bs.modal', function (e) {
      $(this)
        .find("input,textarea,select")
           .val('')
           .end()
        .find("input[type=checkbox], input[type=radio]")
           .prop("checked", "checked")
           .end()
        .find("span")
            .html('');
    });

	function checkemail(){
		var email = $('input[name=email]').val();
		if(email){
			$.ajax({
				type: 'post',
				url: '{{ route('employee.checkemail') }}',
				data: { user_email:email, _token: '{{ csrf_token() }}'},
				success: function (response){
					$( '#email_status' ).html(response);
					if(response=="OK"){
						$('#email_status').css('color','green');
						return true;
					}else if(response == 'Enter a valid email id.'){
						$('#email_status').css('color','red');
						return false;
					}else if(response == 'Email Already Exist. Please Choose a unique email id.'){
						$('#email_status').css('color','red');
						  setTimeout(function(){
									 $('#email').val('');
									 $('#email_status').html('');
									 //alert('Email Already Exist. Please Choose a unique email id.');
								 }, 1000);
						return false;
					}
				}
			});
		}else{
			$( '#email_status' ).html("");
			return false;
		}
	}
</script>
@endsection
