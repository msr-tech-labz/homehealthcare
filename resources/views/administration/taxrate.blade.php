@extends('layouts.main-layout')

@section('page_title','Tax Rates - ')

@section('active_administration','active')

@section('content')
<div class="row clearfix">
    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('settings') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-10">
                    Tax Rates
                    <small>Manage Tax Rate List</small>
                </h2>
            </div>
            <div class="body">
                <table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="30%">Tax Name</th>
                            <th>Rate</th>
                            <th>Type</th>
                            <th>Order</th>
                            <th>Status</th>
                            <th width="20%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($taxrates as $index => $d)
                            <tr>
                            <td>{{ $index + 1 }}.</td>
                            <td>{{ $d->tax_name }}</td>
                            <td>{!! $d->tax_rate !!}</td>
                            <td>{{ $d->type }}</td>
                            <td>{{ $d->order }}</td>
                            <td>{{ $d->status?'Active':'Inactive' }}</td>
                            @if($d->created_by == 'Custom')
                            <td>
                                <button type="button" data-id="{{ Helper::encryptor('encrypt',$d->id) }}" data-name="{{ $d->tax_name }}" data-rate="{{ $d->tax_rate }}" data-type="{{ $d->type }}" data-order="{{ $d->order }}" data-status="{{ $d->status }}" data-url="{{ route('taxrate.update',Helper::encryptor('encrypt',$d->id)) }}" class="btn btn-success btn-sm waves-effect btnEdit"><i class="material-icons small">edit</i></button>
                                <form class="form-inline hide" action="{{ route('taxrate.destroy',Helper::encryptor('encrypt',$d->id)) }}" method="POST" style="display: inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" onclick="return confirm('Are you sure?');" class="btn btn-danger btn-sm waves-effect btnRemove"><i class="material-icons small">delete</i></button>
                                </form>
                            </td>
                            @else
                            <td></td>
                            @endif
                        </tr>
                        @empty
                        <tr>
                            <td colspan="7" class="text-center">No taxrate(s) found.</td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <th width="5%">#</th>
                            <th width="30%">Tax Name</th>
                            <th>Rate</th>
                            <th>Type</th>
                            <th>Order</th>
                            <th>Status</th>
                            <th width="20%">Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
        <div class="card">
            <div class="header">
                <h2 class="action_lbl">
                    New Taxrate
                </h2>
            </div>
            <div class="body">
                <div class="row clearfix">
                    <form class="form-horizontal newForm" method="POST" action="{{ route('taxrate.store') }}">
                        {{ method_field('POST') }}
                        {{ csrf_field() }}
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                <label for="tax_name">Tax Name</label>
                            </div>
                            <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="tax_name" name="tax_name" class="form-control" placeholder="Tax Name" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                <label for="tax_rate">Tax Rate</label>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="number" step="0.01" id="tax_rate" name="tax_rate" class="form-control" placeholder="Tax Rate" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                <label for="type">Type</label>
                            </div>
                            <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                <div class="form-group" style="margin-top: 1%">
                                    <input name="type" type="radio" id="type_percentage" class="with-gap radio-col-deep-purple" value="Percentage" checked="">
                                    <label for="type_percentage">Percentage</label>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                <label for="order">Order</label>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="number" id="order" name="order" class="form-control" placeholder="Order" value="{{ count($taxrates) + 1 }}" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                <label for="status">Status</label>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select id="status" name="status" class="form-control">
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <input type="hidden" id="id" value="0">
                            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 col-md-offset-2">
                                <button type="submit" class="btn btn-success btn-block btn-lg waves-effect pull-left">Save</button>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                                <button type="reset" class="btn btn-danger btn-block btn-lg waves-effect pull-right">Clear</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page.scripts')
<script>
    $(function(){
        $('.btnEdit').on('click', function(){
            id = $(this).data('id');
            name = $(this).data('name');
            rate = $(this).data('rate');
            type = $(this).data('type');
            order = $(this).data('order');
            status = $(this).data('status');

            $('.action_lbl').html('Edit Taxrate - '+name);
            $('.newForm').attr('action',$(this).data('url'));
            $('.newForm').attr('method','POST');
            $('.newForm input[name=_method]').val('PUT');
            $('.newForm input[name=_token]').val('{{ csrf_token() }}');
            $('.newForm #id').val(id);
            $('.newForm #tax_name').val(name);
            $('.newForm #tax_rate').val(rate);
            $('.newForm input[name=type][value='+type+']').prop('checked',true);
            $('.newForm input[name=order]').val(order);
            $('.newForm #status').val(status);
        });
    });
</script>
@endsection
