@extends('layouts.main-layout')

@section('page_title','Access Roles - ')

@section('active_administration','active')

@section('page.styles')
    <style>
        .multi-col{
            columns: 5;
            column-count: 5;
        }
    </style>
@endsection

@section('content')
<form action="{{ isset($role)?route('acl.update',['id' => $role->id]):route('acl.save') }}" class="form-horizontal" method="POST">
    {{ method_field('POST') }}
    {{ csrf_field() }}
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <a href="{{ route('acl.index') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                        <i class="material-icons">arrow_back</i>
                    </a>
                    <h2 class="col-sm-10 action_lbl">
                        {{ isset($role)?'Edit':'Add' }} Role
                        <small>{{ isset($role)?'Edit':'Add' }} role and access permissions</small>
                    </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="body">
                    <div class="col-lg-12">
                        <div class="row clearfix">
                            @include('partials.branch-input')
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="display_name">Role Name</label>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="display_name" name="display_name" class="form-control" value="{{ isset($role)?$role->display_name:'' }}" placeholder="Display Name" autocomplete="off" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix hide">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="name">Name</label>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="name" name="name" class="form-control" placeholder="Name" value="{{ isset($role)?$role->name:'' }}" autocomplete="off" readonly="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="role_name">Description</label>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea id="description" name="description" class="form-control" placeholder="Description">{{ isset($role)?$role->description:'' }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div><br>
                            <div class="row">
                                <h3 class="text-center">
                                    <span class="pull-left" style="margin-left:3%">
                                        <input type="checkbox" class="filled-in chk-col-blue" name="check_all" id="checkall"/>
                                        <label for="checkall">Check All</label><br>
                                    </span>
                                    Role Permissions</h3><br>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            @if(isset($permissions))
                                @foreach($permissions as $group => $perms)
                                    @if(count($perms))
                                        <div class="col-lg-12 col-md-3 col-sm-3 col-xs-12">
                                            <legend>
                                                <fieldset class="card-inside-title col-blue">{{ $group }}</fieldset>
                                                <div class="multi-col">
                                                    @foreach ($perms as $p)
                                                        <input type="checkbox" class="filled-in chk-col-blue perm_checkbox" @if(isset($role_permissions)){{in_array($p->id,$role_permissions)?"checked":""}}@endif name="permission[]" id="permission_{{ $p->id }}" value="{{ $p->id }}">
                                                        <label for="permission_{{ $p->id }}">{{ $p->display_name }}</label><br>
                                                    @endforeach
                                                </div>
                                            </legend>
                                        </div>
                                    @endif
                                @endforeach
                            @endif
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-5 form-control-label">
                                    <button type="submit" class="btn btn-success col-lg-2 col-md-offset-5">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('page.scripts')
<script>
    $(function(){
        $('#display_name').on('input', function(){
            var text = $(this).val();
            $('#name').val(text.replace(/[^\w\s]*/gi, '').replace(/\d/g, '').replace(/\s/g, '-').toLowerCase());
        });

        $('.content').on('change','input[name=check_all]', function (){
            if($(this).is(':checked')){
                $('.perm_checkbox').prop('checked', true);
            }else{
                $('.perm_checkbox').prop('checked', false);
            }
        });
    });
</script>
@endsection
