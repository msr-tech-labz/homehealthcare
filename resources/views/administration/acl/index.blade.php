@extends('layouts.main-layout')

@section('page_title','Access Control List - ')

@section('active_administration','active')

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('settings') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-10">
                    User Access Roles
                    <small>Access Control List</small>
                </h2>
                <div class="col-sm-1">
                    <a href="{{ route('acl.create') }}" class="btn btn-primary btn-block waves-effect"> + Add Role</a>
                </div>
            </div>
            <div class="body">
                <table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="30%">Name</th>
                            <th>Description</th>
                            <th width="20%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($roles as $index => $d)
                            <tr>
                            <td>{{ $index + 1 }}.</td>
                            <td>{{ $d->display_name }}</td>
                            <td>{!! $d->description ?? '-' !!}</td>
                            @if($d->tenant_id != null)
                            <td>
                                @if($d->created_by != 'Default')
                                <a href="{{ route('acl.edit',['id' => $d->id]) }}" class="btn btn-success btn-sm waves-effect"><i class="material-icons small">edit</i></a>
                                <form class="form-inline" action="{{ route('acl.delete') }}" method="POST" style="display: inline">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id" value="{{ $d->id }}"/>
                                    <button type="submit" onclick="return confirm('Are you sure?');" class="btn btn-danger btn-sm waves-effect btnRemove"><i class="material-icons small">delete</i></button>
                                </form>
                                @endif
                            </td>
                            @else
                                <td></td>
                            @endif
                        </tr>
                        @empty
                        <tr>
                            <td colspan="4" class="text-center">No role(s) found.</td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page.scripts')
<script>
    $(function(){
        $('.btnEdit').on('click', function(){
            id = $(this).data('id');
            name = $(this).data('name');
            description = $(this).data('description');

            $('.action_lbl').html('Edit Role - '+name);
            $('.newForm').attr('action',$(this).data('url'));
            $('.newForm').attr('method','POST');
            $('.newForm input[name=_method]').val('PUT');
            $('.newForm input[name=_token]').val('{{ csrf_token() }}');
            $('.newForm #id').val(id);
            $('.newForm #display_name').val(display_name);
            $('.newForm #description').val(description);
        });
    });
</script>
@endsection
