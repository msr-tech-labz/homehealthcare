@extends('layouts.main-layout')

@section('page_title','Feedbacks - ')

@section('active_administration','active')

@section('plugin.styles')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
@endsection

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('settings') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-8">
                    Feedbacks
                    <small>Feedbacks and Feature requests</small>
                </h2>
                <div class="col-sm-2 pull-right">
                    <a class="btn btn-primary" onclick="javascript: $('.addFeedbackForm').toggle();">+ New Feedback/Request</a>
                </div>
            </div>
            <div class="body">
                <div class="row clearfix addFeedbackForm" style="width:90%;display:none;margin:0 auto;margin-bottom:20px;border: 1px solid #ccc;border-radius:4px;padding: 10px 0">
                    <form class="form-horizontal" action="{{ route('feedback.store') }}" method="post" enctype="multipart/form-data">
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                <label for="type">Type</label>
                            </div>
                            <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                <div class="form-group" style="margin-top: 1%">
                                    <input name="type" type="radio" id="type_feedback" class="with-gap radio-col-deep-purple" value="Feedback" checked="">
                                    <label for="type_feedback">Feedback</label>
                                    <input name="type" type="radio" id="type_feature" class="with-gap radio-col-deep-purple" value="Feature">
                                    <label for="type_feature">Feature Request</label>
                                    <input name="type" type="radio" id="type_bug" class="with-gap radio-col-deep-purple" value="Bug">
                                    <label for="type_bug">Bug / Issue</label>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                <label for="module">Module</label>
                            </div>
                            <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                <div class="form-group" style="margin-top: 1%">
                                    <input name="module" type="radio" id="module_leads" class="with-gap radio-col-deep-purple" value="Leads" checked="">
                                    <label for="module_leads">Leads</label>
                                    <input name="module" type="radio" id="module_hrm" class="with-gap radio-col-deep-purple" value="HRM">
                                    <label for="module_hrm">HRM</label>
                                    <input name="module" type="radio" id="module_billing" class="with-gap radio-col-deep-purple" value="Billing">
                                    <label for="module_billing">Billing</label>
                                    <input name="module" type="radio" id="module_reports" class="with-gap radio-col-deep-purple" value="Reports">
                                    <label for="module_reports">Reports</label>
                                    <input name="module" type="radio" id="module_tracking" class="with-gap radio-col-deep-purple" value="Bug">
                                    <label for="module_tracking">Tracking</label>
                                    <input name="module" type="radio" id="module_configuration" class="with-gap radio-col-deep-purple" value="Configuration">
                                    <label for="module_configuration">Configuration</label>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                <label for="message">Message</label>
                            </div>
                            <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <textarea id="tinymce" name="message" class="form-control" placeholder="Your message here"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                <label for="document">Document</label>
                            </div>
                            <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <input type="file" id="document" name="document" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row clearfix text-center">
                            <button type="reset" class="btn btn-lg btn-danger">Clear</button>&nbsp;&nbsp;&nbsp;&nbsp;
                            <button type="submit" class="btn btn-lg btn-success">Submit</button>
                        </div>
                    </form>
                </div>

                <table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="10%">Date</th>
                            <th width="10%">Type</th>
                            <th width="10%">Module</th>
                            <th>Message</th>
                            <th width="10%">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($feedbacks as $index => $f)
                            <tr>
                            <td>{{ $index + 1 }}.</td>
                            <td>{{ $f->created_at->format('d-m-Y') }}</td>
                            <td>{{ $f->type }}</td>
                            <td>{{ $f->module }}</td>
                            <td>{!! strip_tags($f->message) !!}</td>
                            <td>{{ $f->status }}</td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="10" class="text-center">No record(s) found.</td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <th width="5%">#</th>
                            <th width="10%">Date</th>
                            <th width="10%">Type</th>
                            <th width="10%">Category</th>
                            <th>Message</th>
                            <th width="10%">Status</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page.scripts')
    <!-- Moment Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ ViewHelper::ThemePlugin('tinymce/tinymce.js') }}"></script>
<script>
    $(function(){
        //TinyMCE
        tinymce.init({
            selector: 'textarea',
            height: 300,
            menubar: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            content_css: [
            'http://fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            'http://www.tinymce.com/css/codepen.min.css']
        });
        tinymce.suffix = ".min";
        tinyMCE.baseURL = '/themes/default/plugins/tinymce';

    });
</script>
@endsection
