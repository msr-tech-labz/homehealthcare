@extends('layouts.main-layout')

@section('page_title','Services Master - ')

@section('active_administration','active')

@section('content')
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<div class="row clearfix">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('settings') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-10">
                    Services Master
                    <small>Manage Services</small>
                </h2>
            </div>
            <div class="body">
                <div class="row clearfix">
                    <form class="form-horizontal serviceForm" method="POST" action="{{ route('service.store') }}">
                        {{ method_field('POST') }}
                        {{ csrf_field() }}
                        @include('partials.branch-input')
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                <label for="service_name">Name</label>
                            </div>
                            <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="service_name" name="service_name" class="form-control" placeholder="Service Name" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                <label for="hsn">HSN Number</label>
                            </div>
                            <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="hsn" name="hsn" class="form-control" placeholder="HSN Number" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                <label for="category_id">Category</label>
                            </div>
                            <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select id="category_id" name="category_id" class="form-control">
                                            <option value="">-- Select Category --</option>
                                    @if(isset($categories) && count($categories))
                                        @foreach ($categories as $cat)
                                            <option value="{{ $cat->id }}">{{ $cat->category_name }}</option>
                                        @endforeach
                                    @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                <label for="description">Description</label>
                            </div>
                            <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <textarea id="description" name="description" class="form-control" placeholder="Description"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <input type="hidden" id="id" name="id" value="0">
                            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 col-md-offset-2">
                                <button type="submit" class="btn btn-success btn-block btn-lg waves-effect pull-left">Save</button>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                                <button type="reset" class="btn btn-danger btn-block btn-lg waves-effect pull-right">Clear</button>
                            </div>
                        </div>
                    </form>
                </div><br><br>
                <table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable" style="zoom:90%;">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="30%">Name</th>
                            <th width="30%">HSN Number</th>
                            <th width="20%">Category</th>
                            <th>Description</th>
                            <th width="20%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($services as $index => $d)
                            <tr>
                            <td>{{ $index + 1 }}.</td>
                            <td>{{ $d->service_name }}</td>
                            <td>{{ $d->hsn ?? '-' }}</td>
                            <td>{{ isset($d->category)?$d->category->category_name:'-' }}</td>
                            <td>{!! $d->description ?? '-' !!}</td>
                            @if($d->created_by == 'Custom')
                                <td>
                                    <button type="button" data-id="{{ Helper::encryptor('encrypt',$d->id) }}" data-branch="{{ $d->branch_id }}" data-name="{{ $d->service_name }}" data-category="{{ $d->category_id }}" data-description="{{ $d->description }}" data-hsn="{{ $d->hsn }}" data-url="{{ route('service.update',Helper::encryptor('encrypt',$d->id)) }}" class="btn btn-success btn-sm waves-effect btnEdit"><i class="material-icons small">edit</i></button>
                                    <form class="form-inline hide" action="{{ route('service.destroy',Helper::encryptor('encrypt',$d->id)) }}" method="POST" style="display: inline">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <button type="submit" onclick="return confirm('Are you sure?');" class="btn btn-danger btn-sm waves-effect btnRemove"><i class="material-icons small">delete</i></button>
                                    </form>
                                </td>
                            @else
                                <td></td>
                            @endif
                        </tr>
                        @empty
                        <tr>
                            <td colspan="6" class="text-center">No service(s) found.</td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>HSN Number</th>
                            <th>Category</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <h2 class="col-sm-10">
                    Services Category
                    <small>Manage service categories</small>
                </h2>
            </div>
            <div class="body">
                <div class="row clearfix">
                    <form class="form-horizontal categoryForm" method="POST" action="{{ route('servicecategory.store') }}">
                        {{ method_field('POST') }}
                        {{ csrf_field() }}
                        @include('partials.branch-input')
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                <label for="category_name">Category Name</label>
                            </div>
                            <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="category_name" name="category_name" class="form-control" placeholder="Category Name" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <input type="hidden" id="id" value="0">
                            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 col-md-offset-2">
                                <button type="submit" class="btn btn-success btn-block btn-lg waves-effect pull-left">Save</button>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                                <button type="reset" class="btn btn-danger btn-block btn-lg waves-effect pull-right">Clear</button>
                            </div>
                        </div>
                    </form>
                </div><br><br>
                <table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable" style="zoom:90%;">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="30%">Category</th>
                            <th width="20%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($categories as $index => $c)
                            <tr>
                            <td>{{ $index + 1 }}.</td>
                            <td>{{ $c->category_name }}</td>
                            @if($c->created_by == 'Custom')
                                <td>
                                    <button type="button" data-id="{{ Helper::encryptor('encrypt',$c->id) }}" data-branch="{{ $c->branch_id }}" data-name="{{ $c->category_name }}" data-description="{{ $c->description }}" data-url="{{ route('servicecategory.update',Helper::encryptor('encrypt',$c->id)) }}" class="btn btn-success btn-sm waves-effect btnEditCategory"><i class="material-icons small">edit</i></button>
                                    <form class="form-inline" action="{{ route('servicecategory.destroy',Helper::encryptor('encrypt',$c->id)) }}" method="POST" style="display: inline">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <button type="submit" onclick="return confirm('Are you sure?');" class="btn btn-danger btn-sm waves-effect btnRemove"><i class="material-icons small">delete</i></button>
                                    </form>
                                </td>
                            @else
                                <td></td>
                            @endif
                        </tr>
                        @empty
                        <tr>
                            <td colspan="4" class="text-center">No categories(s) found.</td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Category</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page.scripts')
    <!-- Select Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<script>
    $(function(){
        $('.btnEdit').on('click', function(){
            $('html, body').animate({
                scrollTop: 0
            }, 800, function(){
                $('.serviceForm #service_name').focus();
            });
            id = $(this).data('id');
            branch = $(this).data('branch');
            name = $(this).data('name');
            hsn = $(this).data('hsn');
            category = $(this).data('category');
            description = $(this).data('description');

            $('.action_lbl').html('Edit Service - '+name);
            $('.serviceForm').attr('action',$(this).data('url'));
            $('.serviceForm').attr('method','POST');
            $('.serviceForm input[name=_method]').val('PUT');
            $('.serviceForm input[name=_token]').val('{{ csrf_token() }}');
            $('.serviceForm #id').val(id);
            $('.serviceForm #branch_id').val(branch);
            $('.serviceForm #service_name').val(name);
            $('.serviceForm #hsn').val(hsn);
            $('.serviceForm #category_id').val(category).selectpicker('render');
            $('.serviceForm #description').val(description);
        });

        $('.btnEditCategory').on('click', function(){
            id = $(this).data('id');
            branch = $(this).data('branch');
            name = $(this).data('name');

            $('.categoryForm').attr('action',$(this).data('url'));
            $('.categoryForm').attr('method','POST');
            $('.categoryForm input[name=_method]').val('PUT');
            $('.categoryForm input[name=_token]').val('{{ csrf_token() }}');
            $('.categoryForm #id').val(id);
            $('.categoryForm #branch_id').val(branch);
            $('.categoryForm #category_name').val(name);
        });
    });
</script>
@endsection
