@extends('layouts.main-layout')

@section('page_title','Referral Masters - ')

@section('active_administration','active')

@section('plugin.styles')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
@endsection

@section('content')
    @if (count($errors) > 0)
    <div class="row clearfix">
    	<div class="alert alert-danger">
    		<ul>
    			@foreach ($errors->all() as $error)
    				<li>{{ $error }}</li>
    			@endforeach
    		</ul>
    	</div>
    </div>
    @endif

    <div class="block-header">
        <div class="col-sm-10">
            <a href="{{ route('settings') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                <i class="material-icons">arrow_back</i>
            </a>
        </div>
        <div class="clearfix"></div>
    </div>

<div class="row clearfix">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <h2 class="col-sm-9">
                    Referral Category
                    <small>Manage referral category</small>
                </h2>
                <div class="col-sm-1">
                    <a class="btn btn-success btn-lg waves-effect" data-toggle="modal" data-target="#referralCategoryModal">Add Category</a>
                </div>
            </div>
            <div class="body">
                <table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable" style="zoom:90%;">
                    <thead>
                        <tr>
                            <th width="4%">#</th>
                            <th width="20%">Category</th>
                            <th width="20%">Description</th>
                            <th width="15%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($categories as $index => $c)
                            <tr>
                            <td>{{ $index + 1 }}.</td>
                            <td>{{ $c->category_name }}</td>
                            <td>{{ $c->description }}</td>
                            <td>
                                <button type="button" data-id="{{ Helper::encryptor('encrypt',$c->id) }}" data-branch="{{ $c->branch_id }}" data-category="{{ $c->category_name }}" data-description="{{ $c->description }}" data-url="{{ route('referral.save-category',Helper::encryptor('encrypt',$c->id)) }}" class="btn btn-success btn-sm waves-effect btnEditCategory"><i class="material-icons small">edit</i></button>
                                <form class="form-inline" action="{{ route('referral.destroy-category') }}" method="POST" style="display: inline">
                                    <input type="hidden" name="id" value="{{ Helper::encryptor('encrypt',$c->id) }}"/>
                                    {{ csrf_field() }}
                                    <button type="submit" onclick="return confirm('Are you sure?');" class="btn btn-danger btn-sm waves-effect btnRemoveCategory"><i class="material-icons small">delete</i></button>
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="8" class="text-center">No category(s) found.</td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <th width="4%">#</th>
                            <th width="20%">Category</th>
                            <th width="20%">Description</th>
                            <th width="15%">Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <h2 class="col-sm-9">
                    Referral Source
                    <small>Manage referral sources</small>
                </h2>
                <div class="col-sm-1">
                    <a class="btn btn-success btn-lg waves-effect" data-toggle="modal" data-target="#referralSourceModal">Add Source</a>
                </div>
            </div>
            <div class="body">
                <table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable" style="zoom:90%;">
                    <thead>
                        <tr>
                            <th width="4%">#</th>
                            <th width="20%">Source</th>
                            <th width="20%">Category</th>
                            <th width="20%">Description</th>
                            <th width="15%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($sources as $index => $s)
                            <tr>
                            <td>{{ $index + 1 }}.</td>
                            <td>{{ $s->source_name }} <br> <small>{{ $s->phone_number }}</small></td>
                            <td>{{ $s->category->category_name ?? '' }}</td>
                            <td>{{ $s->description }}</td>
                            <td>
                                <button type="button" data-id="{{ Helper::encryptor('encrypt',$s->id) }}" data-branch="{{ $s->branch_id }}" data-source="{{ $s->source_name }}" data-phone="{{ $s->phone_number }}" data-category="{{ $s->category_id }}" data-description="{{ $s->description }}" data-type="{{ $s->charge_type }}" data-referral-value="{{ $s->referral_value }}" data-bank-name="{{ $s->bank_name }}" data-bank-address="{{ $s->bank_address }}" data-account-number="{{ $s->account_number }}" data-ifsc-code="{{ $s->ifsc_code }}" data-url="{{ route('referral.save-source',Helper::encryptor('encrypt',$s->id)) }}" class="btn btn-success btn-sm waves-effect btnEditSource"><i class="material-icons small">edit</i></button>
                                <form class="form-inline" action="{{ route('referral.destroy-source') }}" method="POST" style="display: inline">
                                    <input type="hidden" name="id" value="{{ Helper::encryptor('encrypt',$s->id) }}"/>
                                    {{ csrf_field() }}
                                    <button type="submit" onclick="return confirm('Are you sure?');" class="btn btn-danger btn-sm waves-effect btnRemoveSource"><i class="material-icons small">delete</i></button>
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="8" class="text-center">No source(s) found.</td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <th width="4%">#</th>
                            <th width="20%">Source</th>
                            <th width="20%">Category</th>
                            <th width="20%">Description</th>
                            <th width="15%">Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Referral Category Modal -->
<div class="modal fade" id="referralCategoryModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-md" role="document">
        <form class="form-horizontal referralCategoryForm" method="POST" action="{{ route('referral.save-category') }}">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header bg-blue">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title action_lbl" id="largeModalLabel">New Referral Category</h4>
                </div>
                <div class="modal-body">
                    <div class="row clearfix">
                        <div class="col-sm-12 form-horizontal">
                            @include('partials.branch-input')<br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                    <label for="category_name">Name</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="category_name" class="form-control" placeholder="Category Name">
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="description">Description</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea name="description" class="form-control" placeholder="Description"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id" value="0"/>
                    <button type="submit" class="btn btn-success waves-effect">Continue</button>
                    <button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Cancel</button>
                </div>
            </form>
		</div>
	</div>
</div>

<!-- Referral Source Modal -->
<div class="modal fade" id="referralSourceModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
        <form class="form-horizontal referralSourceForm" method="POST" action="{{ route('referral.save-source') }}">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header bg-blue">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title action_lbl" id="largeModalLabel">New Referral Source</h4>
                </div>
                <div class="modal-body">
                    <div class="row clearfix">
                        <div class="col-sm-6 form-horizontal">
                            @include('partials.branch-input')<br>
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label required">
                                    <label for="source_name">Name</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="source_name" class="form-control" placeholder="Source Name">
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
                                    <label for="phone_number">Phone Number</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="number" name="phone_number" class="form-control" placeholder="Ex. 9845098450">
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label required">
                                    <label for="category_id">Category</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <select name="category_id" class="form-control" required>
                                                <option value="">-- Select Category --</option>
                                        @if(isset($categories))
                                            @foreach ($categories as $cat)
                                                <option value="{{ $cat->id }}">{{ $cat->category_name }}</option>
                                            @endforeach
                                        @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
                                    <label for="description">Description</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea name="description" class="form-control" placeholder="Locality"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 form-control-label">
                                    <label for="charge_type">Charge Type</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <div class="form-group" style="margin-top: 1%">
                                        <input name="charge_type" type="radio" id="charge_type_percentage" class="with-gap radio-col-deep-purple" value="P" checked/>
                                        <label for="charge_type_percentage">Percentage (%)</label>
                                        <input name="charge_type" type="radio" id="charge_type_fixed" class="with-gap radio-col-deep-purple" value="F"/>
                                        <label for="charge_type_fixed">Fixed Value (Rs.)</label>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
                                    <label for="referral_value">Value</label>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="number" step="0.01" class="form-control" id="referral_value" min="0" name="referral_value" value="{{ isset($l)?$l->referral_value:'' }}"/>
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                        </div>
                        <div class="col-sm-6 form-horizontal">
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
                                    <label for="bank_name">Bank Name</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="bank_name" name="bank_name" class="form-control" placeholder="Eg. State Bank of India" value="{{ isset($b)?$b->bank_name:'' }}">
                                        </div>
                                    </div>
                                </div>
                            </div><br>

                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
                                    <label for="bank_address">Bank Address</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="bank_address" name="bank_address" class="form-control" placeholder="Eg. HMT Area,Jalahalli,Bangalore,560013" value="{{ isset($b)?$b->bank_address:'' }}">
                                        </div>
                                    </div>
                                </div>
                            </div><br>

                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
                                    <label for="acc_num">Account Number</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="number" id="account_number" name="account_number" class="form-control" placeholder="Eg. 1234567890" value="{{ isset($b)?$b->account_number:'' }}">
                                        </div>
                                    </div>
                                </div>
                            </div><br>

                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label">
                                    <label for="ifsc_code">IFSC Code</label>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="ifsc_code" name="ifsc_code" class="form-control" placeholder="Eg. SBI0001" value="{{ isset($b)?$b->ifsc_code:'' }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id" value="0"/>
                    <button type="submit" class="btn btn-success waves-effect">Continue</button>
                    <button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Cancel</button>
                </div>
            </form>
		</div>
	</div>
</div>
@endsection

@section('plugin.scripts')
<!-- Select Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<!-- Moment Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<!-- Bootstrap Notify Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-notify/bootstrap-notify.js') }}"></script>
<script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
@endsection

@section('page.scripts')
<script>
    $(function(){
        $('.content').on('change','input[name=ratecard_status]', function(){
            var id = $(this).data('id');
            var status = $(this).is(':checked')?1:0;

            $.ajax({
                url: '{{ route('ratecard.update-status')}}',
                type: 'POST',
                data: {id: id, status: status, _token: '{{ csrf_token() }}'},
                success: function (data){
                    console.log(data);
                    showNotification('bg-green', 'Status changed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                },
                error: function (error){
                    console.log(error);
                }
            });
        });

        $('.btnEditCategory').on('click', function(){
            id = $(this).data('id');
            branch = $(this).data('branch');
            name = $(this).data('category');
            description = $(this).data('description');

            $('.action_lbl').html('Edit Referral Category - '+name);
            //$('.referralCategoryForm').attr('action',$(this).data('url'));
            $('.referralCategoryForm').attr('method','POST');
            $('.referralCategoryForm input[name=_token]').val('{{ csrf_token() }}');

            $('.referralCategoryForm input[name=id]').val(id);
            $('.referralCategoryForm input[name=branch_id]').val(branch);
            $('.referralCategoryForm input[name=category_name]').val(name);
            $('.referralCategoryForm textarea[name=description]').val(description);

            $('#referralCategoryModal').modal();
        });

        $('.btnEditSource').on('click', function(){
            id = $(this).data('id');
            branch = $(this).data('branch');
            name = $(this).data('source');
            phone = $(this).data('phone');
            category = $(this).data('category');
            description = $(this).data('description');
            type = $(this).data('type');
            referralValue = $(this).data('referral-value');
            bankName = $(this).data('bank-name');
            bankAddress = $(this).data('bank-address');
            accountNumber = $(this).data('account-number');
            ifscCode = $(this).data('ifsc-code');

            $('.action_lbl').html('Edit Referral Source - '+name);
            $('.referralSourceForm').attr('method','POST');
            $('.referralSourceForm input[name=_token]').val('{{ csrf_token() }}');

            $('.referralSourceForm input[name=id]').val(id);
            $('.referralSourceForm input[name=branch_id]').val(branch);
            $('.referralSourceForm input[name=source_name]').val(name);
             $('.referralSourceForm input[name=phone_number]').val(phone);
            $('.referralSourceForm select[name=category_id]').selectpicker('val',category);
            $('.referralSourceForm textarea[name=description]').val(description);
            if(type != ''){
                $('.referralSourceForm input[name=charge_type][value='+type+']').prop('checked',true);
            }
            $('.referralSourceForm input[name=referral_value]').val(referralValue);
            $('.referralSourceForm input[name=bank_name]').val(bankName);
            $('.referralSourceForm input[name=bank_address]').val(bankAddress);
            $('.referralSourceForm input[name=account_number]').val(accountNumber);
            $('.referralSourceForm input[name=ifsc_code]').val(ifscCode);

            $('#referralSourceModal').modal();
        });
    });
</script>
@endsection
