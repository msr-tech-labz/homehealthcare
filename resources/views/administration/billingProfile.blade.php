@extends('layouts.main-layout')

@section('page_title','Billables Settings Master - ')

@section('active_administration','active')

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('settings') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-10">
                    Vendor Profile Master
                    <small>Vendor Profiles</small>
                </h2>
                <button type="button" class="btn btn-primary waves-effect btn-lg pull-right" data-toggle="modal" data-target="#billablesModal">Add New Vendor Profile</button>
            </div>
            <div class="body">
                <table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Logo</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th width="15%">Address</th>
                            <th>PAN</th>
                            <th>Service TAX no</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse($settingsBillable as $index => $a)
                        <tr>
                        <td>{{ $index + 1 }}</td>
                        <td><img style="width: 40px;height: 40px;" src="{{ asset('uploads/provider/'.session('tenant_id').'/billables/'.$a->logo) }}"></td>
                        <td>{{ $a->name }}</td>
                        <td>{{ $a->email }}</td>
                        <td>{{ $a->phone }}</td>
                        <td>{{ $a->address.','.$a->area.','.$a->city.','.$a->state.','.$a->zipcode.','.$a->country }}</td>
                        <td>{{ $a->pan_number }}</td>
                        <td>{{ $a->service_tax_no }}</td>

                        <td>
                            <button type="button" data-id="{{ Helper::encryptor('encrypt',$a->id) }}" data-logo="{{ $a->logo }}" data-name="{{ $a->name }}" data-email="{{ $a->email }}" data-address="{{ $a->address }}" data-area="{{ $a->area }}" data-city="{{ $a->city }}" data-state="{{ $a->state }}" data-zipcode="{{ $a->zipcode }}" data-phone="{{ $a->phone }}" data-country="{{ $a->country }}" data-pannumber="{{ $a->pan_number }}" data-servicetaxno="{{ $a->service_tax_no }}" data-url="{{ route('billingProfile.update',Helper::encryptor('encrypt',$a->id)) }}" class="btn btn-success btn-sm waves-effect btnEdit" data-toggle="modal" data-target="#billablesModal"><i class="material-icons small">edit</i></button>
                            <form class="form-inline" action="{{ route('billingProfile.destroy',Helper::encryptor('encrypt',$a->id)) }}" method="POST" style="display: inline">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit" onclick="return confirm('Are you sure?');" class="btn btn-danger btn-sm waves-effect btnRemove"><i class="material-icons small">delete</i></button>
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="12" class="text-center">No Profiles(s) found.</td>
                    </tr>
                    @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Logo</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Address</th>
                            <th>PAN</th>
                            <th>Service TAX no</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="billablesModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="width: 200%;left: -50%;">
                <div class="modal-body">
                    <div class="card">
                        <div class="header">
                            <h2 class="action_lbl">
                                New Vendor Profile
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <form class="form-horizontal newForm" method="POST" action="{{ route('billingProfile.store') }}" enctype="multipart/form-data">
                                    {{ method_field('POST') }}
                                    {{ csrf_field() }}
                                <div class="col-lg-6">
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                            <label for="name">Name</label>
                                        </div>
                                        <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="name" name="name" class="form-control" placeholder="Vendor Name" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                            <label for="email">Email</label>
                                        </div>
                                        <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="email" id="email" name="email" class="form-control" placeholder="Email" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                            <label for="address">Address</label>
                                        </div>
                                        <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="address" name="address" class="form-control" placeholder="Address" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                            <label for="area">Area</label>
                                        </div>
                                        <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="area" name="area" class="form-control" placeholder="Area" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                            <label for="city">City</label>
                                        </div>
                                        <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="city" name="city" class="form-control" placeholder="City" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                            <label for="logo">Logo</label>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 profile-img-block">
                                            {? $filePath = asset('img/default-avatar.jpg'); ?}
                                            <img src="{{ $filePath }}" style="width: 160px; height: 160px;" class="img img-thumbnail img-profile"/>
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="file" id="logo" name="logo" class="form-control" placeholder="Vendor Logo">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                            <label for="zipcode">Zipcode</label>
                                        </div>
                                        <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="zipcode" name="zipcode" class="form-control" placeholder="Zipcode" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                            <label for="phone">Phone</label>
                                        </div>
                                        <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="number" id="phone" name="phone" class="form-control" placeholder="Phone" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                            <label for="state">State</label>
                                        </div>
                                        <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="state" name="state" class="form-control" placeholder="State" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                            <label for="country">Country</label>
                                        </div>
                                        <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="country" name="country" class="form-control" placeholder="Country" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                            <label for="pan_number">Pan Number</label>
                                        </div>
                                        <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="pan_number" name="pan_number" class="form-control" placeholder="Pan Number" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                            <label for="Service Tax No">Service Tax No</label>
                                        </div>
                                        <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="service_tax_no" name="service_tax_no" class="form-control" placeholder="Service Tax No" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer" style="text-align: center;">
                    <div class="row clearfix">
                        <input type="hidden" id="id" value="0">
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 col-lg-offset-3">
                            <button type="submit" class="btn btn-success btn-block btn-lg waves-effect pull-left">Save</button>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2">
                            <button type="reset" class="btn btn-danger btn-block btn-lg waves-effect pull-right">Clear</button>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2">
                            <button type="button" class="btn btn-info btn-block btn-link waves-effect" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page.scripts')
<script>
    $(function(){
        var defImg = "{{ asset('img/default-avatar.jpg') }}";
        $("#billablesModal").on('hidden.bs.modal', function () {
            $('.action_lbl').html('New Profile');
            $('.img-profile').attr('src',defImg);
            $(this)
            .find("input,textarea,select")
            .val('')
            .end()
        });

        $('.btnEdit').on('click', function(){
            id = $(this).data('id');
            name = $(this).data('name');
            logo = $(this).data('logo');
            email = $(this).data('email');
            address = $(this).data('address');
            area = $(this).data('area');
            city = $(this).data('city');
            state = $(this).data('state');
            phone = $(this).data('phone');
            zipcode = $(this).data('zipcode');
            country = $(this).data('country');
            pannumber = $(this).data('pannumber');
            servicetaxno = $(this).data('servicetaxno');

            $('.action_lbl').html('Edit Profile - '+name);
            $('.newForm').attr('action',$(this).data('url'));
            $('.newForm').attr('method','POST');
            $('.newForm input[name=_method]').val('PUT');
            $('.newForm input[name=_token]').val('{{ csrf_token() }}');
            $('.modal-footer .row #id').val(id);
            $('.newForm #name').val(name);
            $('.newForm #email').val(email);

            var logoURL = '{{ asset('uploads/provider/'. session('tenant_id').'/billables') }}' + '/' + logo;
            $('.img-profile').attr('src', logoURL);


            $('.newForm #address').val(address);
            $('.newForm #area').val(area);
            $('.newForm #city').val(city);
            $('.newForm #state').val(state);
            $('.newForm #phone').val(phone);
            $('.newForm #zipcode').val(zipcode);
            $('.newForm #country').val(country);
            $('.newForm #pan_number').val(pannumber);
            $('.newForm #service_tax_no').val(servicetaxno);
        });

        function handleFileSelect(evt) {
                var files = evt.target.files; // FileList object

                // Loop through the FileList and render image files as thumbnails.
                for (var i = 0, f; f = files[i]; i++) {

                    // Only process image files.
                    if (!f.type.match('image.*')) {
                        continue;
                    }

                    var reader = new FileReader();

                    // Closure to capture the file information.
                    reader.onload = (function(theFile) {
                        return function(e) {
                            // Render thumbnail.
                            var span = document.createElement('span');
                            span.innerHTML = ['<img style="width:160px;height: 160px;" class="img-responsive img-thumbnail" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
                            $('.profile-img-block').html(span);
                        };
                    })(f);

                    // Read in the image file as a data URL.
                    reader.readAsDataURL(f);
                }
            }

            document.getElementById('logo').addEventListener('change', handleFileSelect, false);
    });
</script>
@endsection
