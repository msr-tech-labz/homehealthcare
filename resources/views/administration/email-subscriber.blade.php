@extends('layouts.main-layout')

@section('page_title','Email Subscribers - ')

@section('active_administration','active')

@section('plugin.styles')
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />
<style>
	label.col-sm-4{ width: 32.33% !important}
</style>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="row clearfix">
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('settings') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-10">
                    Email Subscribers
                    <small>List of subscribers for email notification.</small>
                </h2>
                <div class="col-sm-1">
                    <a class="btn btn-success btn-lg waves-effect newUser" data-toggle="modal" data-target="#mailersModal" data-newUser="true">Add Subscriber</a>
                </div>
            </div>
            <div class="body">
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="body">
							<table class="table table-bordered table-striped table-condensed dataTable" style="zoom: 90%">
								<thead>
									<tr>
										<th width="5%">#</th>
										<th>Full Name</th>
										<th>Email</th>
										<th width="20%">Mailer Type</th>
										<th width="15%">Action</th>
									</tr>
								</thead>
								<tbody>
									{? $i = 1; ?}
									@forelse($mailers as $index => $u)
									<tr>
										<td>{{ $i++ }}.</td>
										<td>{{ $u->name }}</td>
										<td>{!! $u->email ?? '-' !!}</td>
										<td>{{ $u->mailer_types }}</td>
										<td>
											<button type="button" data-id="{{ Helper::encryptor('encrypt',$u->id) }}"  data-name="{{ $u->name }}"  data-email="{{ $u->email }}" data-mailer-types="{{ $u->mailer_types }}" data-url="{{ route('mailers.update', ['id' => Helper::encryptor('encrypt',$u->id)])  }}" class="btn btn-success btn-sm waves-effect btnEdit"><i class="material-icons small">edit</i></button>
											<form class="form-inline" action="{{ route('mailers.destroy') }}" method="POST" style="display: inline">
												{{ method_field('DELETE') }}
												{{ csrf_field() }}
												<input type="hidden" name="id" value="{{ isset($u)?Helper::encryptor('encrypt',$u->id):0 }}"/>
												<button type="submit" onclick="return confirm('Are you sure?');" class="btn btn-danger btn-sm waves-effect btnRemove"><i class="material-icons small">delete</i></button>
											</form>
										</td>
									</tr>
									@empty
									<tr>
										<td colspan="6" class="text-center">No Entries found.</td>
									</tr>
									@endforelse
								</tbody>
								<tfoot>
									<tr>
										<th>#</th>
										<th>Full Name</th>
										<th>Email</th>
										<th>Mailer Type</th>
										<th>Action</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>

<!-- User Modal -->
<div class="modal fade" id="mailersModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
        <form class="form-horizontal subscriberForm" method="POST" action="{{ route('mailers.store') }}">
            {{ method_field('POST') }}
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header bg-blue">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title action_lbl" id="largeModalLabel">New Subscriber</h4>
                </div>
                <div class="modal-body">
                    <div class="row clearfix">
                        <div class="col-sm-12 form-horizontal">
                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-5 col-xs-5 form-control-label required">
                                    <label for="notes">Full Name</label>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="name" class="form-control" placeholder="Full Name" required>
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-5 col-xs-5 form-control-label required">
                                    <label for="notes">Email</label>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input autocomplete="off" id="email" type="email" name="email" class="form-control" placeholder="Email" required>
                                        </div>
										<span id="email_status" style="font-weight: bold;"></span>
                                    </div>
                                </div>
                            </div><br>
							<div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-5 col-xs-5 form-control-label required">
                                    <label for="mailer_type">Mailer Type</label>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
										<div class="col-sm-12">
											<input type="checkbox" id="mailer_type_1" class="filled-in chk-col-indigo mailer_type" value="Daily MIS Report" />
											<label class="col-sm-4" for="mailer_type_1" title="Daily MIS Report" style="margin-right: 2px;margin-top: 5px;">MIS</label>
                                            <input type="checkbox" id="mailer_type_2" class="filled-in chk-col-indigo mailer_type" value="Daily Leads Report" />
                                            <label class="col-sm-4" for="mailer_type_2" title="Daily Leads Report" style="margin-right: 2px;margin-top: 5px;">LEADS</label>
										</div>
                                    </div>
									<input type="hidden" name="mailer_types" />
                                </div>
                            </div><br>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="border-top: 1px solid #ccc;">
                    <button type="button" class="btn btn-success waves-effect btnSave">Save</button>
                    <button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Cancel</button>
                </div>
            </form>
		</div>
	</div>
</div>
@endsection

@section('plugin.scripts')
<!-- Select Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<!-- Bootstrap Notify Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-notify/bootstrap-notify.js') }}"></script>
<script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
@endsection

@section('page.scripts')
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        if('{{ count($mailers) > 0 }}'){
            $('.dataTable').DataTable();
        }
    } );
</script>
<script>
    $(function(){
        $('.table').on('click', '.btnEdit', function(){
            id = $(this).data('id');
            fname = $(this).data('name');
            email = $(this).data('email');
            mailerTypes = $(this).data('mailer-types');

            $('.action_lbl').html('Edit Subscriber - '+fname);
            $('.subscriberForm').attr('action',$(this).data('url'));
            $('.subscriberForm').attr('method','POST');
            $('.subscriberForm input[name=_method]').val('POST');
            $('.subscriberForm input[name=_token]').val('{{ csrf_token() }}');

            $('.subscriberForm #id').val(id);
            $('.subscriberForm input[name=name]').val(fname);
            $('.subscriberForm input[name=email]').val(email);
			$('.subscriberForm input[name=mailer_types]').val(mailerTypes);
			if(mailerTypes != ""){
				mailerType = mailerTypes.split(",");
				$(mailerType).each(function(i){
					$('.subscriberForm .mailer_type[value="'+mailerType[i]+'"]').prop('checked',true);
				})
			}

            $('#mailersModal').modal('show');
        });

		$('.content').on('click','.btnSave',function (e){
			e.preventDefault();
			mailerTypes = [];
			if($('.subscriberForm input[name=name]').val() == ''){
				$('.subscriberForm input[name=name]').focus();
				alert("Please enter full name!");
				return;
			}

			if($('.subscriberForm input[name=email]').val() == ''){
				$('.subscriberForm input[name=email]').focus();
				alert("Please enter email!");
				return;
			}

			$('.subscriberForm .mailer_type').each(function(i){
				if($(this).is(':checked')){
					mailerTypes.push($(this).val());
				}
			});

			if(mailerTypes.length <= 0){
				alert("Please select atleast one mailer type!");
				return;
			}

			$('.subscriberForm input[name=mailer_types]').val(mailerTypes.join(","));

			$('.subscriberForm').submit();
		})
    });

    $('#mailersModal').on('hidden.bs.modal', function (e) {
      $(this)
        .find("input,textarea,select")
           .val('')
           .end()
        .find("input[type=checkbox], input[type=radio]")
           .prop("checked", false)
           .end()
        .find("span")
            .html('');
      $('.action_lbl').html('New Entry');
      $('.subscriberForm').attr('action','{{ route('mailers.store') }}');
    });
</script>
@endsection
