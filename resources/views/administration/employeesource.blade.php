@extends('layouts.main-layout')

@section('page_title','Employee Source Master - ')

@section('active_administration','active')

@section('plugin.styles')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
@endsection

@section('content')
<div class="row clearfix">
    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('settings') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-10">
                    Employee Source
                    <small>Manage Employee Source</small>
                </h2>
            </div>
            <div class="body">
                <table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable">
                    <thead>
                        <tr>
                            <th width="10%">#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th width="20%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($employeesources as $index => $d)
                            <tr>
                            <td>{{ $index + 1 }}.</td>
                            <td>{{ $d->name }}</td>
                            <td>{{ $d->description }}</td>
                            <td>{{ $d->status?'Active':'Inactive' }}</td>
                            <td>
                                <button type="button" data-id="{{ Helper::encryptor('encrypt',$d->id) }}" data-branch="{{ $d->branch_id }}" data-name="{{ $d->name }}" data-url="{{ route('employeesource.update',Helper::encryptor('encrypt',$d->id)) }}" class="btn btn-success btn-sm waves-effect btnEdit"><i class="material-icons small">edit</i></button>
                                <form class="form-inline" action="{{ route('employeesource.destroy',Helper::encryptor('encrypt',$d->id)) }}" method="POST" style="display: inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" onclick="return confirm('Are you sure?');" class="btn btn-danger btn-sm waves-effect btnRemove"><i class="material-icons small">delete</i></button>
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="5" class="text-center">No source(s) found.</td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <th width="10%">#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th width="20%">Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
        <div class="card">
            <div class="header">
                <h2 class="action_lbl">
                    New Source
                </h2>
            </div>
            <div class="body">
                <div class="row clearfix">
                    <form class="form-horizontal newForm" method="POST" action="{{ route('employeesource.store') }}">
                        {{ method_field('POST') }}
                        {{ csrf_field() }}
                        @include('partials.branch-input')
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                <label for="name">Name</label>
                            </div>
                            <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="name" name="name" class="form-control" placeholder="Source Name" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                <label for="description">Description</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-4">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" id="description" name="description" placeholder="Description" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                <label for="status">Status</label>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <select id="status" name="status" class="form-control">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>
                        </div><br>
                        <div class="row clearfix">
                            <input type="hidden" id="id" value="0">
                            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 col-md-offset-2">
                                <button type="submit" class="btn btn-success btn-block btn-lg waves-effect pull-left">Save</button>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                                <button type="reset" class="btn btn-danger btn-block btn-lg waves-effect pull-right">Clear</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page.scripts')
<script>
    $(function(){
        $('.btnEdit').on('click', function(){
            id = $(this).data('id');
            branch = $(this).data('branch');
            name = $(this).data('name');
            description = $(this).data('description');

            $('.action_lbl').html('Edit Source - '+name);
            $('.newForm').attr('action',$(this).data('url'));
            $('.newForm').attr('method','POST');
            $('.newForm input[name=_method]').val('PUT');
            $('.newForm input[name=_token]').val('{{ csrf_token() }}');
            $('.newForm #id').val(id);
            $('.newForm #branch_id').val(branch);
            $('.newForm #name').val(name);
            $('.newForm #description').val(description);
        });
    });
</script>
@endsection
