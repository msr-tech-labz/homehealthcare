@extends('layouts.main-layout')

@section('page_title','Plugins - ')

@section('active_administration','active')

@section('plugin.styles')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
@endsection

@section('content')

@if (count($errors) > 0)
<div class="row clearfix">
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('settings') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-10">
                    Plugins
                    <small>List of plugins</small>
                </h2>
            </div>
            <div class="body">
				<ul class="nav nav-tabs tab-nav-right tab-col-orange" role="tablist">
                    <li role="presentation" class="active"><a href="#callhandling_tab" data-toggle="tab">Call Handling</a></li>
                    <li role="presentation"><a href="#paymentgateway_tab" data-toggle="tab">Payment Gateway</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content"><br>
                    <div role="tabpanel" class="tab-pane fade in active" id="callhandling_tab">
						<table class="table table-bordered ch-plugins">
							<thead>
								<tr>
									<th width="5%" class="text-center">#</th>
									<th>Plugin Name and Description</th>
									<th width="20%" class="text-center">Action</th>
									<th width="15%" class="text-center">Set Default</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="vertical-align:middle;text-align:center">1</td>
									<td>
										<img src="http://dnd.knowlarity.com/static/images/logo.png" style="width:120px;float:left;" />
										<h6 style="font-size:14px">Knowlarity</h6>
										<p>
											Knowlarity is leading cloud telephony solutions provider.
											<a href="https://www.knowlarity.com/" target="_blank">More Info</a>
										</p>
									</td>
									<td style="vertical-align:middle;text-align:center">
										<a href="javascript:void(0);" class="btn-group config-plugin" data-id="configure_knowlarity" role="group">
											<button type="button" class="btn bg-teal waves-effect" disabled=""><i class="material-icons">settings</i></button>
											<button type="button" class="btn bg-teal waves-effect" disabled="" style="line-height: 1.9;padding-left:0"> Configure</button>
										</a>
									</td>
									<td style="vertical-align:middle;text-align:center">
									@if(isset($pg['pg_knowlarity']))
										@if($pg['pg_knowlarity'][0]['is_default'] == 1)
											Yes
										@else
											<form action="{{ route('plugin.set-default') }}" method="post">
												{{ csrf_field() }}
												<input type="hidden" name="id" value="{{ Helper::encryptor('encrypt',$pg['pg_ccavenue'][0]['id']) }}" />
												<input type="hidden" name="category" value="Call Handling" />
												<input type="hidden" name="plugin_name" value="pg_knowlarity" />
												<button type="submit" class="btn btn-info">Set default</button>
											</form>
										@endif
									@endif
									</td>
								</tr>
								<tr style="display:none" id="configure_knowlarity">
									<td colspan="5">
										<div class="col-md-8">
											<h4>Configure Post Call Hooks</h4>
											<span>Use the below generated API details to configure Post Call Hook in knowlarity SuperReceptionist app.</span><br><br>
											<table>
												<tr>
													<th>Request Type</th>
													<td><code>POST</code></td>
												</tr>
												<tr>
													<th>Name</th>
													<td><code>smarthealthconnect_lead</code></td>
												</tr>
												<tr>
													<th>API URL</th>
													<td style="font-size:14px"><code>{{ route('knowlarity.call-log',['tid' => session('tenant_id')]) }}</code></td>
												</tr>
											</table>
										</div>
										<div class="col-md-4" style="padding-left:0">
											<br>
											<iframe width="400" height="300" src="https://www.youtube.com/embed/qYhAwB4m-qY" frameborder="0" allowfullscreen></iframe>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div role="tabpanel" class="tab-pane fade in" id="paymentgateway_tab">
						<table class="table table-bordered pg-plugins">
							<thead>
								<tr>
									<th width="5%" class="text-center">#</th>
									<th>Plugin Name and Description</th>
									<th width="20%" class="text-center">Action</th>
									<th width="15%" class="text-center">Default</th>
								</tr>
							</thead>
							<tbody>
								@if(isset($pg['pg_ccavenue']))
								{? $settings = json_decode($pg['pg_ccavenue'][0]['settings']); ?}
								@endif
								<tr>
									<td style="vertical-align:middle;text-align:center">1</td>
									<td>
										<img src="https://www.nopcommerce.com/images/thumbs/0002309_400.png" style="width:120px;float:left;" />
										<h6 style="font-size:14px">CCAvenue</h6>
										<p>
											CCAvenue is a PCI DSS 3.2 compliant payments platform for ecommerce businesses in India. It is designed to help its 1 lakh+ merchants accept online payments..
											<a style="float: right;" href="https://ccavenue.com" target="_blank">More Info</a>
										</p>
									</td>
									<td style="vertical-align:middle;text-align:center">
										<a href="javascript:void(0);" class="btn-group config-plugin" data-id="configure_ccavenue" role="group">
											<button type="button" class="btn bg-teal waves-effect"><i class="material-icons" @if(isset($pg['pg_ccavenue']) && $pg['pg_ccavenue'][0]['is_default'] == 0){{ 'disabled=""' }}@endif>settings</i></button>
											<button type="button" class="btn bg-teal waves-effect" style="line-height: 1.9;padding-left:0" @if(isset($pg['pg_ccavenue']) && $pg['pg_ccavenue'][0]['is_default'] == 0){{ 'disabled=""' }}@endif> Configure</button>
										</a>
									</td>
									<td style="vertical-align:middle;text-align:center">
									@if(isset($pg['pg_ccavenue']))
										@if($pg['pg_ccavenue'][0]['is_default'] == 1)
											Yes
										@else
											<form action="{{ route('plugin.set-default') }}" method="post">
												{{ csrf_field() }}
												<input type="hidden" name="id" value="{{ Helper::encryptor('encrypt',$pg['pg_ccavenue'][0]['id']) }}" />
												<input type="hidden" name="category" value="Payment Gateway" />
												<input type="hidden" name="plugin_name" value="pg_ccavenue" />
												<button type="submit" class="btn btn-info">Set default</button>
											</form>
										@endif
									@endif
									</td>
								</tr>
								<tr style="display:none" id="configure_ccavenue">
									<td colspan="5">
										<form method="POST" action="{{ route('paymentgateway.ccavenue') }}" class="col-md-12">
											<h5>Integration Credentials</h5>
											<br>
											<table class="table">
												<tr>
													<th style="vertical-align: middle;text-align:right;">Merchant ID</th>
													<td><input type="input" name="merchant_id" class="form-control" placeholder="Eg. 1256486" value="{{ $settings->merchant_id ?? '' }}"/></td>
													<th style="vertical-align: middle;text-align:right;">Access Code</th>
													<td><input type="input" name="access_code" class="form-control" placeholder="Eg. HDTEYSJSPSJSKSS" value="{{ $settings->access_code ?? '' }}"/></td>
												</tr>
												<tr>
													<th style="vertical-align: middle;text-align:right;">Working Key</th>
													<td colspan="3"><input type="input" name="working_key" class="form-control" placeholder="Eg. 29ABE0930A436D42CD6F5EC987" value="{{ $settings->working_key ?? '' }}"/></td>
												</tr>
											</table>
											<center><button type="submit" class="btn btn-success btn-md">Save</button></center>
											{{ csrf_field() }}
											<input type="hidden" name="category" value="Payment Gateway" />
											<input type="hidden" name="plugin_name" value="pg_ccavenue" />
										</form>
									</td>
								</tr>
{{-- 								@if(isset($pg['pg_atom']))
								{? $settings = json_decode($pg['pg_atom'][0]['settings']); ?}
								@endif
								<tr>
									<td style="vertical-align:middle;text-align:center">2</td>
									<td>
										<img src="https://pgreports.atomtech.in/PaynetzWebMerchant/newpaynetz/img/newatom.png" style="width:120px;float:left;" />
										<h6 style="font-size:14px">Atom</h6>
										<p>
											atom technologies Limited is an end to end payment services provider offering a vast range of payment services and solutions through Online &amp; Offline platforms.
											<a href="https://www.atomtech.in" target="_blank">More Info</a>
										</p>
									</td>
									<td style="vertical-align:middle;text-align:center">
										<a href="javascript:void(0);" class="btn-group config-plugin" data-id="configure_atom" role="group">
											<button type="button" class="btn bg-teal waves-effect"@if(isset($pg['pg_atom']) && $pg['pg_atom'][0]['is_default'] == 0){{ 'disabled=""' }}@endif><i class="material-icons">settings</i></button>
											<button type="button" class="btn bg-teal waves-effect" style="line-height: 1.9;padding-left:0" @if(isset($pg['pg_atom']) && $pg['pg_atom'][0]['is_default'] == 0){{ 'disabled=""' }}@endif> Configure</button>
										</a>
									</td>
									<td style="vertical-align:middle;text-align:center">
									@if(isset($pg['pg_atom']))
										@if($pg['pg_atom'][0]['is_default'] == 1)
											Yes
										@else
											<form action="{{ route('plugin.set-default') }}" method="post">
												{{ csrf_field() }}
												<input type="hidden" name="id" value="{{ Helper::encryptor('encrypt',$pg['pg_atom'][0]['id']) }}" />
												<input type="hidden" name="category" value="Payment Gateway" />
												<input type="hidden" name="plugin_name" value="pg_atom" />
												<button type="submit" class="btn btn-info">Set default</button>
											</form>
										@endif
									@endif
									</td>
								</tr>
								<tr style="display:none" id="configure_atom">
									<td colspan="5">
										<form method="POST" action="{{ route('paymentgateway.atom') }}" class="col-md-12">
											<h5>Server Credentials</h5>
											<br>
											<table class="table">
												<tr>
													<th style="vertical-align: middle;text-align:right;">Server IP</th>
													<td><input type="input" name="server_ip" class="form-control" placeholder="Eg. yourcompany.com" value="{{ $settings->server_ip ?? '' }}"/></td>
													<th style="vertical-align: middle;text-align:right;">Login ID</th>
													<td><input type="input" name="login_id" class="form-control" placeholder="Eg. 60" value="{{ $settings->login_id ?? '' }}"/></td>
												</tr>
												<tr>
													<th style="vertical-align: middle;text-align:right;">Product ID</th>
													<td><input type="input" name="product_id" class="form-control" placeholder="Eg. NSE" value="{{ $settings->product_id ?? '' }}"/></td>
													<th style="vertical-align: middle;text-align:right;">Password</th>
													<td><input type="password" name="password" class="form-control" value="{{ $settings->password ?? '' }}"/></td>
												</tr>
											</table>
											<center><button type="submit" class="btn btn-success btn-md">Save</button></center>
											{{ csrf_field() }}
											<input type="hidden" name="category" value="Payment Gateway" />
											<input type="hidden" name="plugin_name" value="pg_atom" />
										</form>
									</td>
								</tr> --}}
							</tbody>
						</table>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('plugin.scripts')
<!-- Select Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<!-- Input Mask Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>
<!-- Moment Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<!-- Bootstrap Notify Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-notify/bootstrap-notify.js') }}"></script>
<script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
@endsection

@section('page.scripts')
<script>
    $(function(){
		$('.content').on('change','.plugin-status', function(e){
			if($(this).is(':checked')){
				$('tbody tr td a.config-plugin[data-id='+$(this).data('id')+'] .btn').prop('disabled',false);
			}else{
				$('tbody tr td a.config-plugin[data-id='+$(this).data('id')+'] .btn').prop('disabled',true);
			}

			//$(this).prop('disabled',false);
        });

        $('.content').on('click','.config-plugin', function(){
			$('tbody tr[id='+$(this).data('id')+']').toggle();
        });
    });
</script>
@endsection
