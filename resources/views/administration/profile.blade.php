@extends('layouts.main-layout')

@section('page_title','Profile Settings - ')

@section('active_administration','active')

@section('plugin.styles')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
@endsection

@section('content')

@if (count($errors) > 0)
<div class="row clearfix">
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('settings') }}" class="pull-left btn btn-info btn-circle clearLocalStore waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-10">
                    Profile Settings
                    <small>Manage profile and application settings</small>
                </h2>
            </div>
            <div class="body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#profile_tab" data-toggle="tab" aria-expanded="false">
                            <i class="material-icons">location_city</i> Company Profile
                        </a>
                    </li>
                    <li role="presentation" class="">
                        <a href="#billing_customize" data-toggle="tab" aria-expanded="true">
                            <i class="material-icons">settings</i> Configuration
                        </a>
                    </li>
                    <li role="presentation" class="">
                        <a href="#banking_customize" data-toggle="tab" aria-expanded="true">
                            <i class="material-icons">monetization_on</i> Banking Details
                        </a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade active in" id="profile_tab">
                        <form id="profileForm" action="{{ route('profile.updateCompanyDetails') }}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row clearfix" style="padding:5px 10px">
                                <div class="col-sm-6 form-horizontal">
                                    <h2 class="card-inside-title">
                                        Basic Details
                                        <small>Organization Name, Address</small>
                                    </h2>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                            <label for="organization_name">Organization Name</label>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="organization_name" name="organization_name" class="form-control" placeholder="Organization Name" value="{{ isset($p)?$p->organization_name:'' }}" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                            <label for="organization_short_name">Short Name</label>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="organization_short_name" name="organization_short_name" class="form-control" placeholder="Short Name" value="{{ isset($p)?$p->organization_short_name:'' }}" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                            <label for="organization_address">Address</label>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="organization_address" name="organization_address" class="form-control" placeholder="Address" value="{{ isset($p)?$p->organization_address:'' }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                            <label for="organization_area">Area</label>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="organization_area" name="organization_area" class="form-control" placeholder="Area" value="{{ isset($p)?$p->organization_area:'' }}" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                            <label for="organization_city">City</label>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="organization_city" name="organization_city" class="form-control" placeholder="City" value="{{ isset($p)?$p->organization_city:'' }}" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                            <label for="organization_zipcode">Zip Code</label>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="organization_zipcode" name="organization_zipcode" class="form-control" placeholder="Zip Code" value="{{ isset($p)?$p->organization_zipcode:'' }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                            <label for="organization_state">State</label>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                            <div class="form-group">
												<select class="form-control show-tick" id="organization_state" name="organization_state" data-live-search="true">
													<option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
													<option value="Andhra Pradesh">Andhra Pradesh</option>
													<option value="Arunachal Pradesh">Arunachal Pradesh</option>
													<option value="Assam">Assam</option>
													<option value="Bihar">Bihar</option>
													<option value="Chandigarh">Chandigarh</option>
													<option value="Chhattisgarh">Chhattisgarh</option>
													<option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
													<option value="Daman and Diu">Daman and Diu</option>
													<option value="Delhi">Delhi</option>
													<option value="Goa">Goa</option>
													<option value="Gujarat">Gujarat</option>
													<option value="Haryana">Haryana</option>
													<option value="Himachal Pradesh">Himachal Pradesh</option>
													<option value="Jammu and Kashmir">Jammu and Kashmir</option>
													<option value="Jharkhand">Jharkhand</option>
													<option value="Karnataka">Karnataka</option>
													<option value="Kerala">Kerala</option>
													<option value="Lakshadweep">Lakshadweep</option>
													<option value="Madhya Pradesh">Madhya Pradesh</option>
													<option value="Maharashtra">Maharashtra</option>
													<option value="Manipur">Manipur</option>
													<option value="Meghalaya">Meghalaya</option>
													<option value="Mizoram">Mizoram</option>
													<option value="Nagaland">Nagaland</option>
													<option value="Odisha">Odisha</option>
													<option value="Pondicherry">Pondicherry</option>
													<option value="Punjab">Punjab</option>
													<option value="Rajasthan">Rajasthan</option>
													<option value="Sikkim">Sikkim</option>
													<option value="Tamil Nadu">Tamil Nadu</option>
                                                    <option value="Telangana">Telangana</option>
													<option value="Tripura">Tripura</option>
													<option value="Uttaranchal">Uttaranchal</option>
													<option value="Uttar Pradesh">Uttar Pradesh</option>
													<option value="West Bengal">West Bengal</option>
												</select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                            <label for="organization_country">Country</label>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="organization_country" name="organization_country" class="form-control" placeholder="Country" value="{{ isset($p)?$p->organization_country:'' }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                            <label for="website">Website</label>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="url" id="website" name="website" class="form-control" placeholder="Website"value="{{ isset($p)?$p->website:'' }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 form-horizontal">
                                    <h2 class="card-inside-title">
                                        Communication Details
                                        <small>Phone Number, Email, Website</small>
                                    </h2>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                            <label for="contact_person">Contact Person</label>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="contact_person" name="contact_person" class="form-control" placeholder="Contact Person"value="{{ isset($p)?$p->contact_person:'' }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                            <label for="phone_number">Phone Number</label>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="phone_number" name="phone_number" class="form-control" placeholder="Phone Number" value="{{ isset($p)?$p->phone_number:'' }}" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                            <label for="landline_number">Landline Number</label>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="landline_number" name="landline_number" class="form-control" placeholder="Landline Number" value="{{ isset($p)?$p->landline_number:'' }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                            <label for="email">Accounts Email</label>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="email" id="email" name="email" class="form-control searchCol" placeholder="Email address"value="{{ isset($p)?$p->email:'' }}" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h2 class="card-inside-title">
                                        Logo
                                        <small>Organization Logo</small>
                                    </h2>
                                    <div class="row clearfix">
                                      <div class="col-sm-1"></div>
                                      <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 profile-img-block">
                                       <img src="@if(isset($p))@if(file_exists(public_path('uploads/provider/'.session('tenant_id').'/'.$p->organization_logo))){{ asset('uploads/provider/'.session('tenant_id').'/'.$p->organization_logo)}}@else{{ asset('img/default-avatar.jpg') }}@endif @endif" style="max-width: 360px; max-height: 160px; padding: 5px;" class="img img-thumbnail img-profile"/>
                                   </div>
                                   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">
                                       <br>
                                       <input type="file" id="organization_logo" name="organization_logo" class="form-control profile-picture"><br><br>
                                   </div>
                               </div>
                           </div>
                       </div>

                       <div class="row text-center">
                        <button type="submit" class="btn btn-success btn-lg waves-effect">Save Profile</button>
                    </div>
                </form>
            </div>

            <div role="tabpanel" class="tab-pane fade in" id="billing_customize">
                <div class="row clearfix" style="padding:5px 10px">
					<form id="settingsForm" action="{{ route('profile.updateSettings') }}" method="POST" enctype="multipart/form-data">
	                    {{ csrf_field() }}
                        <!--<div class="col-sm-6 form-horizontal">
                            <h2 class="card-inside-title">
                                Billing Details
                                <small>Details to be added in generated bills and invoices</small>
                            </h2>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="auth_signatory">Authorized Signatory</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="auth_signatory" name="auth_signatory" class="form-control" placeholder="Eg. John Doe" value="{{ isset($b)?$b->auth_signatory:'' }}" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="signtory_pos">Signatory's Position</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="signtory_pos" name="signtory_pos" class="form-control" placeholder="Eg. CEO" value="{{ isset($b)?$b->signtory_pos:'' }}" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

						<div class="col-sm-6 form-horizontal">
							<h2>&nbsp;</h2>
							<div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="pan_number">PAN Number</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="pan_number" name="pan_number" class="form-control" placeholder="Eg. AACCR2314P" value="{{ isset($b)?$b->pan_number:'' }}" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

							<div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="gstin">GSTIN</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="gstin" name="gstin" class="form-control" placeholder="Eg. 29AACCR23PSD001" value="{{ isset($b)?$b->gstin:'' }}" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="cin">CIN</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="cin" name="cin" class="form-control" placeholder="Eg. CR23PSD001" value="{{ isset($b)?$b->cin:'' }}" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

						<div class="col-sm-12 form-horizontal">
							<h2 class="card-inside-title">
                                Prefix and Numbering
                                <small>Invoice, Receipt, Patient, Employee prefix and numbering</small>
                            </h2>
							<div class="col-sm-6">
								<div class="row clearfix">
	                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
	                                    <label for="proforma_invoice_inits">Proforma Invoice Prefix</label>
	                                </div>
	                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
	                                    <div class="form-group">
	                                        <div class="form-line">
	                                            <input type="text" id="proforma_invoice_inits" name="proforma_invoice_inits" class="form-control" placeholder="Eg. ABINP" value="{{ isset($b)?$b->proforma_invoice_inits:'' }}" required>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="row clearfix">
	                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
	                                    <label for="proforma_invoice_start_no">Proforma Invoice Start Number</label>
	                                </div>
	                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
	                                    <div class="form-group">
	                                        <div class="form-line">
	                                            <input type="number" id="proforma_invoice_start_no" name="proforma_invoice_start_no" class="form-control" placeholder="Eg. 1000" value="{{ isset($b)?$b->proforma_invoice_start_no:'' }}" required>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>

								<div class="row clearfix">
	                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
	                                    <label for="invoice_inits">Service Invoice Prefix</label>
	                                </div>
	                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
	                                    <div class="form-group">
	                                        <div class="form-line">
	                                            <input type="text" id="invoice_inits" name="invoice_inits" class="form-control" placeholder="Eg. ABINS" value="{{ isset($b)?$b->invoice_inits:'' }}" required>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="row clearfix">
	                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
	                                    <label for="invoice_start_no">Service Invoice Start Number</label>
	                                </div>
	                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
	                                    <div class="form-group">
	                                        <div class="form-line">
	                                            <input type="number" id="invoice_start_no" name="invoice_start_no" class="form-control" placeholder="Eg. 1000" value="{{ isset($b)?$b->invoice_start_no:'' }}" required>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>

								<div class="row clearfix">
	                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
	                                    <label for="receipt_inits">Receipt Prefix</label>
	                                </div>
	                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
	                                    <div class="form-group">
	                                        <div class="form-line">
	                                            <input type="text" id="receipt_inits" name="receipt_inits" class="form-control" placeholder="Eg. ABRE" value="{{ isset($b)?$b->receipt_inits:'' }}" required>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="row clearfix">
	                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
	                                    <label for="receipt_start_no">Receipt Start Number</label>
	                                </div>
	                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
	                                    <div class="form-group">
	                                        <div class="form-line">
	                                            <input type="number" id="receipt_start_no" name="receipt_start_no" class="form-control" placeholder="Eg. 1000" value="{{ isset($b)?$b->receipt_start_no:'' }}" required>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
							</div>

							<div class="col-sm-6">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
                                        <label for="credit_inits">CreditMemo Prefix</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="credit_inits" name="credit_inits" class="form-control" placeholder="Eg. ABCM" value="{{ isset($b)?$b->credit_inits:'' }}" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
                                        <label for="credit_start_no">CreditMemo Start Number</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="number" id="credit_start_no" name="credit_start_no" class="form-control" placeholder="Eg. 1000" value="{{ isset($b)?$b->credit_start_no:'' }}" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
								<div class="row clearfix">
	                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
	                                    <label for="patient_id_prefix">Patient ID Prefix</label>
	                                </div>
	                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
	                                    <div class="form-group">
	                                        <div class="form-line">
	                                            <input type="text" id="patient_id_prefix" name="patient_id_prefix" class="form-control" placeholder="Eg. ABRP" value="{{ isset($b)?$b->patient_id_prefix:'' }}">
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="row clearfix">
	                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
	                                    <label for="patient_id_start_no">Patient ID Start No</label>
	                                </div>
	                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
	                                    <div class="form-group">
	                                        <div class="form-line">
	                                            <input type="number" id="patient_id_start_no" name="patient_id_start_no" class="form-control" placeholder="Eg. 1000" value="{{ isset($b)?$b->patient_id_start_no:'' }}">
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>

								<div class="row clearfix">
	                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5 form-control-label">
	                                    <label for="employee_id_prefix">Employee ID Prefix</label>
	                                </div>
	                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
	                                    <div class="form-group">
	                                        <div class="form-line">
	                                            <input type="text" id="employee_id_prefix" name="employee_id_prefix" class="form-control" placeholder="Eg. TKOH" value="{{ isset($b)?$b->employee_id_prefix:'' }}">
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>

                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                        <label for="gstin">Payment Due Date</label>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="number" id="payment_due_date" name="payment_due_date" class="form-control" placeholder="Eg. 7" value="{{ isset($b)?$b->payment_due_date:'' }}" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							</div>
						</div>-->
						
						<div class="col-sm-12 form-horizontal">
							<h2 class="card-inside-title">
                                Leads Configuration
                                <small>Service Order, Schedules</small>
                            </h2>
							<div class="col-sm-6">
								<div class="row clearfix">
	                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<label style="font-weight:normal;font-size: 16px">Patient email address mandatory</label>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5">
										<input type="radio" id="patient_email_mandatory_yes" name="patient_email_mandatory" class="with-gap radio-col-indigo" value="1" {{ (isset($b) && $b->patient_email_mandatory == 1)?'checked=""':'' }}/>
										<label for="patient_email_mandatory_yes">Yes</label>
										<input type="radio" id="patient_email_mandatory_no" name="patient_email_mandatory" class="with-gap radio-col-orange" value="0" {{ (isset($b) && $b->patient_email_mandatory == 0)?'checked=""':'' }}/>
										<label for="patient_email_mandatory_no">No</label>
	                                </div>
	                            </div>
								
								<div class="row clearfix">
	                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<label style="font-weight:normal;font-size: 16px">Allow scheduling of service, if customer has outstanding balance</label>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5">
										<input type="radio" id="schedule_creation_for_outstanding_customer_yes" name="schedule_creation_for_outstanding_customer" class="with-gap radio-col-indigo" value="1" {{ (isset($b) && $b->schedule_creation_for_outstanding_customer == 1)?'checked=""':'' }}/>
										<label for="schedule_creation_for_outstanding_customer_yes">Yes</label>
										<input type="radio" id="schedule_creation_for_outstanding_customer_no" name="schedule_creation_for_outstanding_customer" class="with-gap radio-col-orange" value="0" {{ (isset($b) && $b->schedule_creation_for_outstanding_customer == 0)?'checked=""':'' }}/>
										<label for="schedule_creation_for_outstanding_customer_no">No</label>
	                                </div>
	                            </div>
								
								<div class="row clearfix">
	                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
										<label style="font-weight:normal;font-size: 16px">Auto-complete schedules end of the day</label>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5">
										<input type="radio" id="auto_complete_schedules_yes" name="auto_complete_schedules" class="with-gap radio-col-indigo" value="1" {{ (isset($b) && $b->auto_complete_schedules == 1)?'checked=""':'' }}/>
										<label for="auto_complete_schedules_yes">Yes</label>
										<input type="radio" id="auto_complete_schedules_no" name="auto_complete_schedules" class="with-gap radio-col-orange" value="0" {{ (isset($b) && $b->auto_complete_schedules == 0)?'checked=""':'' }}/>
										<label for="auto_complete_schedules_no">No</label>
	                                </div>
	                            </div>
							</div>
						</div>

						<div class="clearfix"></div><hr>

						<div class="row text-center">
							<button type="submit" class="btn btn-success btn-lg waves-effect">Save Details</button>
						</div>

					</form>
				</div>
            </div>

            <div role="tabpanel" class="tab-pane fade in" id="banking_customize">
                <div class="row clearfix" style="padding:5px 10px">
                    <form id="billingForm" action="{{ route('profile.updateBankingDetails') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="col-sm-6 form-horizontal">
                            <h2 class="card-inside-title">
                                Banking Details
                                <small>Banking details to be added in generated bills</small>
                            </h2>

                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                    <label for="invoice_payee">Payee</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="invoice_payee" name="invoice_payee" class="form-control" placeholder="ABC Company" value="{{ isset($b)?$b->invoice_payee:'' }}" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                    <label for="bank_name">Banking Name</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="bank_name" name="bank_name" class="form-control" placeholder="Eg. State Bank of India" value="{{ isset($b)?$b->bank_name:'' }}" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                    <label for="bank_address">Bank Address</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="bank_address" name="bank_address" class="form-control" placeholder="Eg. HMT Area,Jalahalli,Bangalore,560013" value="{{ isset($b)?$b->bank_address:'' }}" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                    <label for="acc_num">Account Number</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="number" id="acc_num" name="acc_num" class="form-control" placeholder="Eg. 1234567890" value="{{ isset($b)?$b->acc_num:'' }}" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                    <label for="ifsc_code">IFSC Code</label>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="ifsc_code" name="ifsc_code" class="form-control" placeholder="Eg. SBI0001" value="{{ isset($b)?$b->ifsc_code:'' }}" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row text-center">
                                <button type="submit" class="btn btn-success btn-lg waves-effect">Save Details</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
@endsection

@section('plugin.scripts')
	<script type="text/javascript">
		$(function(){
			$('.content').on('click','.clearLocalStore', function() {
			    return localStorage.removeItem('profileSettingsLastTab');
			});

			// for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
		    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		        // save the latest tab; use cookies if you like 'em better:
		        localStorage.setItem('profileSettingsLastTab', $(this).attr('href'));
		    });

		    // go to the latest tab, if it exists:
		    var lastTab = localStorage.getItem('profileSettingsLastTab');
		    if (lastTab) {
		        $('[href="' + lastTab + '"]').tab('show');
		    }
		});
	</script>
<!-- Select Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<!-- Input Mask Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>
<!-- Moment Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<!-- Bootstrap Notify Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-notify/bootstrap-notify.js') }}"></script>
<script src="{{ ViewHelper::ThemeJs('pages/ui/notifications.js') }}"></script>
@endsection

@section('page.scripts')
<script>
    $(function(){
		@if(isset($p))
		$('#organization_state').val('{{ $p->organization_state }}').change();
		@endif

        @if(isset($b))
            $("input[name=proforma_invoice_inits]").attr('readonly',true);
            $("input[name=proforma_invoice_start_no]").attr('readonly',true);
            $("input[name=invoice_inits]").attr('readonly',true);
            $("input[name=invoice_start_no]").attr('readonly',true);
            $("input[name=receipt_inits]").attr('readonly',true);
            $("input[name=receipt_start_no]").attr('readonly',true);
            $("input[name=credit_inits]").attr('readonly',true);
            $("input[name=credit_start_no]").attr('readonly',true);
            $("input[name=patient_id_prefix]").attr('readonly',true);
            $("input[name=patient_id_start_no]").attr('readonly',true);
            $("input[name=employee_id_prefix]").attr('readonly',true);
        @endif

        $('.date').inputmask('dd-mm-yyyy', { placeholder: '__-__-____' });

        $('.content').on('change','input[name=status]', function(){
            var id = $(this).data('id');
            var status = $(this).is(':checked')?1:0;

            $.ajax({
                url: '{{ route('user.update-status')}}',
                type: 'POST',
                data: {id: id, status: status, _token: '{{ csrf_token() }}'},
                success: function (data){
                    console.log(data);
                    showNotification('bg-green', 'Status changed successfully', 'top', 'center', 'animated bounceInDown', 'animated bounceOutDown');
                },
                error: function (error){
                    console.log(error);
                }
            });
        });
    });

    function handleFileSelect(evt) {
		var files = evt.target.files; // FileList object

		// Loop through the FileList and render image files as thumbnails.
		for (var i = 0, f; f = files[i]; i++) {
			// Only process image files.
			if (!f.type.match('image.*')) {
				continue;
			}
			var reader = new FileReader();

			// Closure to capture the file information.
			reader.onload = (function(theFile) {
				return function(e) {
					// Render thumbnail.
					var span = document.createElement('span');
					span.innerHTML = ['<img style="width:160px;height: 160px;" class="img-responsive img-thumbnail" src="', e.target.result,
					'" title="', escape(theFile.name), '"/>'].join('');
					$('.profile-img-block').html(span);
				};
			})(f);

			// Read in the image file as a data URL.
			reader.readAsDataURL(f);
		}
	}

	document.getElementById('organization_logo').addEventListener('change', handleFileSelect, false);
</script>
@endsection
