@extends('layouts.main-layout')

@section('page_title','Cost Centre - ')

@section('active_administration','active')

@section('plugin.styles')
    <link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
@endsection

@section('content')
<div class="row clearfix">
    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <a href="{{ route('settings') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">arrow_back</i>
                </a>
                <h2 class="col-sm-10">
                    Cost Centre
                    <small>Manage Tally Cost Centre</small>
                </h2>
            </div>
            <div class="body">
                <table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="30%">Cost Centre</th>
                            <th>Specialization</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th width="20%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($costcentres as $index => $d)
                            <tr>
                            <td>{{ $index + 1 }}.</td>
                            <td>{{ $d->cost_centre }}</td>
                            <td>{{ $d->specialization->specialization_name ?? '' }}</td>
                            <td>{{ $d->description }}</td>
                            <td>{{ $d->status?'Active':'Inactive' }}</td>
                            <td>
                                <button type="button" data-id="{{ Helper::encryptor('encrypt',$d->id) }}" data-name="{{ $d->cost_centre }}" data-specialization="{{ $d->specialization_id }}" data-description="{{ $d->description }}" data-status="{{ $d->status }}" data-url="{{ route('costcentre.update',Helper::encryptor('encrypt',$d->id)) }}" class="btn btn-success btn-sm waves-effect btnEdit"><i class="material-icons small">edit</i></button>
                                <form class="form-inline" action="{{ route('costcentre.destroy',Helper::encryptor('encrypt',$d->id)) }}" method="POST" style="display: inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" onclick="return confirm('Are you sure?');" class="btn btn-danger btn-sm waves-effect btnRemove"><i class="material-icons small">delete</i></button>
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="6" class="text-center">No cost centre(s) found.</td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <th width="5%">#</th>
                            <th width="30%">Cost Centre</th>
                            <th>Specialization</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th width="20%">Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
        <div class="card">
            <div class="header">
                <h2 class="action_lbl">
                    New Cost Centre
                </h2>
            </div>
            <div class="body">
                <div class="row clearfix">
                    <form class="form-horizontal newForm" method="POST" action="{{ route('costcentre.store') }}">
                        {{ method_field('POST') }}
                        {{ csrf_field() }}
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                <label for="cost_centre">Cost Centre</label>
                            </div>
                            <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="cost_centre" name="cost_centre" class="form-control" placeholder="Cost Centre" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                <label for="specialization_id">Specialization</label>
                            </div>
                            <div class="col-lg-8 col-md-9 col-sm-8 col-xs-7">
                                <div class="form-group" style="margin-top: 1%">
                                    <select class="form-control show-tick" id="specialization_id" name="specialization_id" required="">
                                        <option value="">-- Select --</option>
                                @if(isset($specializations) && count($specializations))
                                    @foreach ($specializations as $specialization)
                                        <option value="{{ $specialization->id }}">{{ $specialization->specialization_name }}</option>
                                    @endforeach
                                @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
                                <label for="description">Description</label>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="description" name="description" class="form-control" placeholder="Description">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label required">
                                <label for="status">Status</label>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <select id="status" name="status" class="form-control">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>
                        </div><br>
                        <div class="row clearfix">
                            <input type="hidden" id="id" value="0">
                            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 col-md-offset-2">
                                <button type="submit" class="btn btn-success btn-block btn-lg waves-effect pull-left">Save</button>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                                <button type="reset" class="btn btn-danger btn-block btn-lg waves-effect pull-right">Clear</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page.scripts')
    <!-- Select Plugin Js -->
    <script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<script>
    $(function(){
        $('.btnEdit').on('click', function(){
            id = $(this).data('id');
            name = $(this).data('name');
            specialization = $(this).data('specialization');
            description = $(this).data('description');
            status = $(this).data('status');

            $('.action_lbl').html('Edit Cost Centre - '+name);
            $('.newForm').attr('action',$(this).data('url'));
            $('.newForm').attr('method','POST');
            $('.newForm input[name=_method]').val('PUT');
            $('.newForm input[name=_token]').val('{{ csrf_token() }}');
            $('.newForm #id').val(id);
            $('.newForm #cost_centre').val(name);
            $('.newForm #description').val(description);
            $('.newForm #specialization_id').val(specialization).change();
            $('.newForm #specialization_id').selectpicker('refresh');
            $('.newForm #status').val(status);
        });
    });
</script>
@endsection
