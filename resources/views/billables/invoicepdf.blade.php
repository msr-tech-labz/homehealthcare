<!-- Bootstrap Core Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

<!-- Custom Css -->

<link rel="stylesheet" type="text/css" href="{{ asset('css/print.css') }}" media="print" />

<style>
  img.img-topbar{ margin-top: -5px !important;}
  .username:after{
    content: ' \25BE';
    padding-left: 10px;
    font-size: 20px;
  }
  @page
  {
    size: auto;   /* auto is the initial value */
    margin: 0mm;  /* this affects the margin in the printer settings */
  }

  body{
    font-size: 13px;
  }
  .border-top{
    border-top: 2px solid #333 !important;
  }
  .border-bottom{
    border-bottom: 2px solid #333 !important;
  }
  .border-right{
    border-right: 2px solid #333 !important;
  }
  .border-left{
    border-left: 2px solid #333 !important;
  }

  .no-padding, .no-padding tbody tr td, .no-padding tbody tr th{
    padding: 0 !important;
    border: 2px solid #333;
  }
  .no-border, .no-border tbody tr td, .no-border tbody tr th{
    border: 0;
  }
  .services-table, .services-table thead tr th, .services-table tfoot tr th, .services-table tfoot tr td, .services-table  tbody tr th,.services-table tbody tr td{
   padding: 1px !important;
   border: 0;
 }

 .tbl-text{
  margin-top:1px !important;
  padding:2px !important;
  margin-bottom:1px !important;
}

h6{
  font-weight: bold;
}

.user-info{ background: rgba(96, 125, 139, 0.10) !important; border-bottom: 0.1em solid #3F51B5 !important; padding: 10px !important;}
</style>

<body class="theme-blue">
  <div class="col-lg-10 col-lg-offset-1 invoicereport" style="border: 0px solid black;background: white;">
    <br>
    <table class="table table-bordered no-padding" style="margin-bottom: 0 !important">
      <!-- Heading -->
      <tr>
        <th colspan="3" class="text-center">
          <h6 style="font-size:20px; margin-top:2px !important;margin-bottom:2px !important; padding:0px !important">
            INVOICE
          </h6>
        </th>
      </tr>
      <!-- Logo, address and payment mode information -->
      <tr>
        <td width="44%">
          <table class="no-border" style="margin-bottom:0 !important">
            <tr>
              <td>
                <center><img src="{{ asset('uploads/provider/'.session('tenant_id').'/billables/'.$profile->logo) }}" class="org-img" style="width: 60px;margin-left:15px;margin-top: 5px;clear:both;border-right:0 !important"></center>
              </td>
              <td>
                <div style="width:100%;display:inline-block;padding: 5px;margin-left:5px;font-size:12px;padding-top:40px;border-left:0 !important">
                  <b>{{ $profile->name }}</b><br>
                  {{ $profile->address }}<br>
                  {{ $profile->area }}<br>
                  {{ $profile->city }} - {{ $profile->zipcode}}<br>
                  {{ $profile->state }}<br>
                  {{ $profile->country }}<br>
                  Tel No.: +91 - {{ $profile->phone }}
                </div>
              </td>
            </tr>
          </table>
          <div style="display:inline-block;margin-right:5px;text-align:center;vertical-align:top;padding-top:10px">

          </div>

        </td>

        <td width="25%">
          <table class="table no-border no-padding no-bottom" style="margin-bottom: 5px !important">
            <tr>
              <td class="border-bottom no-bottom">
                <h6 style="display:inline-block;margin-top:2px !important;padding:5px !important;">Patient No</h6><br>
                <h6 style="display:inline-block;margin-top:2px !important;margin-right:5px;font-weight:normal;padding:5px !important">{{ isset($patient->patient_id)?$patient->patient_id:'' }}</h6>
              </td>
            </tr>
            <tr>
              <td class="no-bottom" style="border:0 !important">
                <h6 style="margin-top:2px !important;padding:5px !important;margin-bottom: 2px !important">MODE OF PAYMENT</h6>
                <div class="col-md-12" style="margin-top:2px !important;margin-left:0px;padding-left:5px !important">
                  <div style="display:inline-block;min-width:25%;margin-right:10px">Cheque</div><br>
                  <div style="display:inline-block">Bank Transfer</div><br>
                  <div style="display:inline-block;min-width:25%;margin-right:10px">DD</div><br>
                  <div style="display:inline-block">Other</div><br>
                  <div style="display:inline-block;min-width:25%;margin-right:10px">Cash</div>
                </div>
              </td>
            </tr>
          </table>
        </td>

        <td>
          <table class="table no-border no-padding" style="margin-bottom: 5px !important">
            <tr>
              <td class="border-bottom border-right">
                <h6 style="margin-top:2px !important;padding:5px !important">DATED</h6>
              </td>
              <td></td>
              <td style="margin-top:2px !important;padding:5px !important;font-style:italic;">{{ date('d-M-Y') }}</td>
            </tr>
            <tr>
              <td colspan="3" style="padding: 5px !important">
                Immediate by cheque / DD favouring<br>
                <h6 style="margin-top:2px !important;padding:5px !important">{{ strtoupper(Helper::getSetting('invoice_payee')) }}</h6>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <!-- Customer and bank information -->
      <tr>
        <td width="40%">
          <div style="padding: 1px 4px !important">
            <h6 style="margin-top:5px !important">
              <h6 style="font-size:13px">{{ $patient->first_name.' '.$patient->last_name }}</h6>
              <address style="font-weight:500">
                {{ $patient->street_address }}<br>
                {{ $patient->area }}<br>
                {{ $patient->city.' - '.$patient->zipcode.', '.$patient->state }}<br>
                Phone: {{ $patient->contact_number.','.$patient->alternate_number }}<br>
                Email: {{ $patient->email }}
              </address>
            </h6>
          </div>
        </td>

        <td colspan="2">
          <table class="table no-padding no-border" style="margin-bottom: 0px !important">
            <tr>
              <th width="44.7%" class="border-right border-bottom"><h6 class="tbl-text">PATIENT REFERENCE</h6></th>
              <td class="border-bottom" style="padding-left: 5px !important">{{ $patient->patient_id }}</td>
            </tr>
            <tr>
              <th class="border-right border-bottom"><h6 class="tbl-text">SO REFERENCE</h6></th>
              <td class="border-bottom" style="padding-left: 5px !important"></td>
            </tr>
            <tr>
              <th class="border-right border-bottom"><h6 class="tbl-text">SO DATE</h6></th>
              <td class="border-bottom" style="padding-left: 5px !important"></td>
            </tr>
            <tr>
              <th class="border-right border-bottom"><h6 class="tbl-text">BANK NAME</h6></th>
              <td class="border-bottom" style="padding-left: 5px !important">{{ Helper::getSetting('bank_name') }}</td>
            </tr>
            <tr>
              <th class="border-right border-bottom"><h6 class="tbl-text">ADDRESS</h6></th>
              <td class="border-bottom" style="padding-left: 5px !important">{{ Helper::getSetting('bank_address') }}</td>
            </tr>
            <tr>
              <th class="border-right border-bottom"><h6 class="tbl-text">BANK A/C NO</h6></th>
              <td class="border-bottom" style="padding-left: 5px !important">{{ Helper::getSetting('acc_num') }}</td>
            </tr>
            <tr>
              <th class="border-right"><h6 class="tbl-text">IFSC CODE</h6></th>
              <td style="padding-left: 5px !important">{{ Helper::getSetting('ifsc_code') }}</td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

    <table class="table services-table" style="margin-top: 0px !important;margin-bottom: 0px !important">
      <thead>
        <tr>
          <th width="5%" class="text-center border-left border-right border-bottom" style="font-size:12px !important">S.No</th>
          <th class="text-center border-right border-bottom" style="font-size:12px !important">DESCRIPTION OF SERVICE</th>
          <th class="text-center border-right border-bottom" style="font-size:12px !important">UNIT</th>
          <th class="text-center border-right border-bottom" style="font-size:12px !important">RATE in Rs.</th>
          <th class="text-center border-right border-bottom" style="font-size:12px !important">AMOUNT in Rs.</th>
        </tr>
      </thead>
      <tbody>
        @if(count($vendorItems))
        {? $price = 0; ?}
        @foreach ($vendorItems as $index => $item)
        <tr>
          <th class="text-center border-left border-right">{{ $index + 1 }}</th>
          <td class="border-right" style="padding: 2px 5px !important;">{{ \Helper::getVendorItemName($item,$type) }}</td>
          <td class="text-center border-right">{{ $item->quantity }}</td>
          <td class="text-center border-right">Rs. {{ \Helper::getVendorItemPrice($item,$type) }}</td>
          <td class="text-right border-right" style="padding-right: 20px !important;">Rs. {{ number_format(intval(\Helper::getVendorItemPrice($item,$type)) * $item->quantity,2) }}</td>
        </tr>
        {? $price += number_format(intval(\Helper::getVendorItemPrice($item,$type)) * $item->quantity,2); ?}
        @endforeach
        @endif
      </tbody>

      <tfoot>
        <tr>
          <td class="border-right border-left border-top"></td>
          <th class="text-center border-right border-top">Total</th>
          Add a comment to this line
          <th colspan="3" class="text-right border-right border-top" style="padding-right: 20px !important;">Rs. {{ number_format($price,2) }}</th>
        </tr>
      </tfoot>
    </table>
    <table class="table no-border" style="margin-bottom: 2px !important;position:relative;bottom:0">
      <tr>
        <td colspan="2" class="border-left border-top border-right">
          AMOUNT CHARGABLE IN WORDS:
          {? $f = new \NumberFormatter("en", \NumberFormatter::SPELLOUT); ?}
          <b>{{ ucwords($f->format((round($price)))).' Rupees Only' }}</b><br>
          <h6 class="tbl-text text-right">E. &amp; O.E</h6><br>
        </td>
      </tr>
      <tr>
        <td class="border-left border-bottom border-right" width="50%">
          <div style="min-width:25%;display:inline-block;padding-left:0 !important"><u>PAN</u>: {{ $profile->pan_number }}</div><br>
          <div style="min-width:25%;display:inline-block;padding-left:0 !important"><u>Service Tax No</u>: {{ $profile->service_tax_no }}</div><br>
          <div style="margin-top:5px">
            <small style="font-weight:500">Note: Payment has to be made immediately.<br> Delayed payment shall attract interest at 18% p.a. <br> We declare that this invoice shows the actual price of the goods/ service described and that all particulars are true and correct</small>
          </div>
        </td>
        <td class="no-padding border-left border-top border-bottom border-right">
          <h6 class="tbl-text text-right" style="font-size:13px;margin-top:5px !important; margin-right: 8% !important;">for {{ strtoupper($profile->name) }}</h6>
          <h6 class="sig" style="text-align: right;margin-top: 16%; margin-bottom: 0 !important; margin-right: 20%">AUTHORISED SIGNATORY</h6>
        </td>
      </tr>
    </table>
    <center><b>SUBJECT TO {{ strtoupper($profile->state) }} JURISDICATION</b></center>
    <span style="font-size:13px;margin-left:20px;">This is a computer generated invoice.</span><br><br>
  </div>