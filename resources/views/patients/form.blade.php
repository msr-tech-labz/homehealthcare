@extends('layouts.main-layout')

@section('page_title',isset($e)?'Edit Patient - Patients | ':'New Patient - Patients |')

@section('active_patients','active')

@section('plugin.styles')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<link href="{{ ViewHelper::ThemePlugin('bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
<!-- Bootstrap Tagsinput Css -->
<link href="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
@endsection

@section('page.styles')
<style>
	.card .header .header-dropdown{
		top: 15px;
	}
	hr{
		margin-top: 10px;
		margin-bottom: 10px;
	}
	.bootstrap-tagsinput{
		min-height: 40px;
		vertical-align: top;
		padding: 4px 0;
	}
	.bootstrap-tagsinput input{
		padding-left: 0;
	}
	.plus:after{
		content: ' + ';
		font-size: 18px;
		font-weight: bold;
	}
	.care-plan-card .header{
		padding: 15px 20px;
	}
	.modal-lg{
		width: 70%;
	}
	.modal .modal-header{
		padding: 20px 25px 15px 25px;
	}
	.modal-footer{
		border-top: 1px solid #ddd !important;
	}

	/* Map Styles */
	  #infowindow-content .title {
        font-weight: bold;
      }

      #infowindow-content {
        display: none;
      }

      #map #infowindow-content {
        display: inline;
      }

      .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
      }

      #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
      }

      .pac-controls {
        display: inline-block;
        padding: 5px 11px;
      }

      .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 20px;
        font-weight: 500;
        padding: 6px 12px;
      }
</style>
@endsection

@section('content')

@if (count($errors) > 0)
<div class="row clearfix">
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif

<div class="row">
	<form id="patientForm" action="{{ isset($e)?route('patient.update', Helper::encryptor('encrypt',$e->id)):route('patient.store') }}" method="POST">
		{{ csrf_field() }}
		@if(isset($e)){{ method_field('PUT') }}@endif
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<a href="{{ route('patient.index') }}" class="pull-left btn btn-info btn-circle waves-effect waves-circle waves-float">
						<i class="material-icons">arrow_back</i>
					</a>
					<h2 class="col-sm-10">
						{{ isset($e)?'Edit':'New'}} Patient
						<small>Patient {{ isset($e)?'Updation':'Creation'}} Form</small>
					</h2>
					<ul class="header-dropdown m-r--5">
						<div class="btn-group" role="group">
							<button type="button" class="btn btn-success m-l-15 waves-effect"><i class="material-icons" style="color: #fff">save</i></button>
							<button type="submit" class="btn btn-success waves-effect" style="line-height: 1.9;padding-left:0">Save Patient</button>
						</div>&nbsp;&nbsp;
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="body">
					<div class="row clearfix">
						<div class="col-sm-6 form-horizontal">
							<h2 class="card-inside-title">
								Basic Details
								<small>Name, Gender, Age, Weight</small>
							</h2>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="first_name">First Name</label>
								</div>
								<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="first_name" name="first_name" class="form-control searchCol" placeholder="First Name" value="{{ isset($e)?$e->first_name:'' }}" required>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="last_name" name="last_name" class="form-control searchCol" placeholder="Last Name" value="{{ isset($e)?$e->last_name:'' }}">
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="date_of_birth">Date of Birth</label>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="date_of_birth" name="date_of_birth" class="form-control date" placeholder="Date of Birth"value="{{ (isset($e) && !empty($e->date_of_birth))?$e->date_of_birth->format('d-m-Y'):'' }}">
										</div>
									</div>
								</div>
								<div class="col-sm-1" style="line-height: 3"><b> or </b></div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="form-group">
										<div class="form-line">
											<input type="number" id="patient_age" name="patient_age" class="form-control" placeholder="Age" value="{{ isset($e)?$e->patient_age:'' }}">
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="weight">Patient Weight</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="patient_weight" name="patient_weight" class="form-control" placeholder="Patient Weight" value="{{ isset($e)?$e->patient_weight:'' }}">
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="gender">Gender</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<select class="form-control show-tick" id="gender" name="gender" required>
											<option value="">-- Please select--</option>
											<option value="Male" {{ (isset($e) && $e->gender == 'Male')?'selected':'' }}>Male</option>
											<option value="Female" {{ (isset($e) && $e->gender == 'Female')?'selected':'' }}>Female</option>
											<option value="Other" {{ (isset($e) && $e->gender == 'Other')?'selected':'' }}>Other</option>
										</select>
									</div>
								</div>
							</div>
						</div>

						<div class="col-sm-6 form-horizontal">
							<h2 class="card-inside-title">
								Communication Details
								<small>Enquirer Name, Phone, Email, Relationship</small>
							</h2>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="contact_number">Contact Number</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="contact_number" name="contact_number" class="form-control" placeholder="Primary Contact Number" value="{{ isset($e)?$e->contact_number:'' }}" required>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="alternate_number">Alternate Number</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="alternate_number" name="alternate_number" class="form-control" placeholder="Alternate Number" value="{{ isset($e)?$e->alternate_number:'' }}">
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="email">Email</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="email" name="email" class="form-control searchCol" placeholder="Email address"value="{{ isset($e)?$e->email:'' }}">
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="enquirer_name">Enquirer Name</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="enquirer_name" name="enquirer_name" class="form-control" placeholder="Enquirer Name" value="{{ isset($e)?$e->enquirer_name:'' }}">
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="relationship_with_patient">Relationship with Patient</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<select id="relationship_with_patient" name="relationship_with_patient" class="form-control show-tick" placeholder="Relationship with Patient">
												<option value="">-- Please select --</option>
												<option value="FATHER">FATHER</option>
												<option value="MOTHER">MOTHER</option>
												<option value="BROTHER">BROTHER</option>
												<option value="SISTER">SISTER</option>
												<option value="HUSBAND">HUSBAND</option>
												<option value="WIFE">WIFE</option>
												<option value="DAUGHTER">DAUGHTER</option>
												<option value="SON">SON</option>
												<option value="MOTHER-IN-LAW">MOTHER-IN-LAW</option>
												<option value="FATHER-IN-LAW">FATHER-IN-LAW</option>
												<option value="BROTHER-IN-LAW">BROTHER-IN-LAW</option>
												<option value="GRANDFATHER">GRANDFATHER</option>
												<option value="GRANDMOTHER">GRANDMOTHER</option>
												<option value="UNCLE">UNCLE</option>
												<option value="AUNT">AUNT</option>
												<option value="FRIEND">FRIEND</option>
												<option value="SELF">SELF</option>
												<option value="OTHER">OTHER</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row clearfix">
						<div class="col-sm-5 form-horizontal">
							<h2 class="card-inside-title">
								Patient Address
								<small>Address / Area, City and State</small>
							</h2>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="street_address">Address</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="street_address" name="street_address" class="form-control" placeholder="Address / Area" value="{{ isset($e)?$e->street_address:'' }}"/>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="area">Area</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="area" name="area" class="form-control" placeholder="Area" value="{{ isset($e)?$e->area:'' }}"/>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="city">City</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="city" name="city" class="form-control" placeholder="City" value="{{ isset($e)?$e->city:'' }}"/>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="zipcode">Zip Code</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="zipcode" name="zipcode" class="form-control" placeholder="Zip Code" value="{{ isset($e)?$e->zipcode:'' }}"/>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="state">State</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="state" name="state" class="form-control" placeholder="State" value="{{ isset($e)?$e->state:'' }}"/>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="country">Country</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="country" name="country" class="form-control" placeholder="Country" value="{{ isset($e)?$e->country:'' }}"/>
										</div>
									</div>
								</div>
							</div>
							<h2 class="card-inside-title">
								Location Co-ordinates
								<small>Lattitude and Longitude</small>
							</h2>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="latitude">Latitude</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="latitude" name="latitude" class="form-control" placeholder="Latitude" value="{{ isset($e)?$e->latitude:'' }}"/>
										</div>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-lg-3 col-md-3 col-sm-5 col-xs-5 form-control-label">
									<label for="longitude">Longitude</label>
								</div>
								<div class="col-lg-9 col-md-9 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" id="longitude" name="longitude" class="form-control" placeholder="Longitude" value="{{ isset($e)?$e->longitude:'' }}"/>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-sm-7 form-horizontal">
							<h2 class="card-inside-title">
								Location Map
							</h2>
							<div class="pac-card" id="pac-card">
								<div>
									<div id="title">
										Location search
									</div>
									<div id="type-selector" class="pac-controls">
										<input type="radio" name="type" id="changetype-all" checked="checked">
										<label for="changetype-all">All</label>

										<input type="radio" name="type" id="changetype-establishment">
										<label for="changetype-establishment">Establishments</label>

										<input type="radio" name="type" id="changetype-address">
										<label for="changetype-address">Addresses</label>

										<input type="radio" name="type" id="changetype-geocode">
										<label for="changetype-geocode">Geocodes</label>
									</div>
									<div id="strict-bounds-selector" class="pac-controls">
										<input type="checkbox" id="use-strict-bounds" value="">
										<label for="use-strict-bounds">Strict Bounds</label>
									</div>
								</div>
								<div id="pac-container">
									<input id="pac-input" type="text"
									placeholder="Enter a location">
								</div>
							</div>
							<div id="map" style="width: 98%; height:500px;"></div>
							<div id="infowindow-content">
								<img src="" width="16" height="16" id="place-icon">
								<span id="place-name"  class="title"></span><br>
								<span id="place-address"></span>
							</div>
							{{-- <div id="map-canvas" style="width: 98%; height:500px;"></div> --}}
						</div>
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" name="id" value="{{ isset($e)?Helper::encryptor('encrypt',$e->id):0 }}">
	</form>
</div>


@endsection

@section('plugin.scripts')
<!-- Select Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-select/js/bootstrap-select.js') }}"></script>
<!-- Input Mask Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>
<!-- Moment Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('momentjs/moment.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<!-- Bootstrap Tags Input Plugin Js -->
<script src="{{ ViewHelper::ThemePlugin('bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>

{{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCvPCONT3odYk2HGHeP3hGhtwXZqx5rekQ&libraries=places"></script> --}}
@endsection

@section('page.scripts')
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBFVRxoHdxhkIqVPiLUPe2kGDJWRmZgIS4&libraries=places&region=IN"></script>
	<script>
		initMap();

		@if(isset($e))
			$('#relationship_with_patient').selectpicker('val','{{ $e->relationship_with_patient }}');
		@endif

		var componentForm = {
			sublocality_level_2: 'short_name',
			street_number: 'short_name',
			route: 'long_name',
			locality: 'long_name',
			administrative_area_level_1: 'long_name',
			country: 'long_name',
			postal_code: 'short_name'
		  };

		var componentIds = {
		  sublocality_level_2: 'area',
		  locality: 'city',
		  administrative_area_level_1: 'state',
		  country: 'country',
		  postal_code: 'zipcode'
		};

		function handleLocationError(browserHasGeolocation, infoWindow, pos) {
			// infoWindow.setPosition(pos);
			// infoWindow.setContent(browserHasGeolocation ?
			//                       'Error: The Geolocation service failed.' :
			//                       'Error: Your browser doesn\'t support geolocation.');
		}

		function initMap() {
			var map = new google.maps.Map(document.getElementById('map'), {
				center: {lat: 16.28004385258969, lng: 78.10594640625004},
				zoom: 6
			});
			var card = document.getElementById('pac-card');
			var input = document.getElementById('pac-input');
			var types = document.getElementById('type-selector');
			var strictBounds = document.getElementById('strict-bounds-selector');

			map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

			var autocomplete = new google.maps.places.Autocomplete(input);

			// Bind the map's bounds (viewport) property to the autocomplete object,
			// so that the autocomplete requests use the current map bounds for the
			// bounds option in the request.
			autocomplete.bindTo('bounds', map);

			var infowindow = new google.maps.InfoWindow();
			var infowindowContent = document.getElementById('infowindow-content');
			infowindow.setContent(infowindowContent);
			var marker = new google.maps.Marker({
				map: map,
				animation: google.maps.Animation.DROP,
				draggable: true,
				anchorPoint: new google.maps.Point(0, -29)
			});

			// Try HTML5 geolocation.
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function(position) {
					var pos = {
						lat: position.coords.latitude,
						lng: position.coords.longitude
					};
					map.setCenter(pos);
				}, function() {
					handleLocationError(true, infowindow, map.getCenter());
				});
			} else {
				// Browser doesn't support Geolocation
				handleLocationError(false, infowindow, map.getCenter());
			}

			google.maps.event.addListener(
				marker,
				'drag',
				function(event) {
					infowindow.close();
					document.getElementById('latitude').value = this.position.lat();
					document.getElementById('longitude').value = this.position.lng();
					//alert('drag');
				});


			google.maps.event.addListener(marker,'dragend',function(event) {
					infowindow.close();
					document.getElementById('latitude').value = this.position.lat();
					document.getElementById('longitude').value = this.position.lng();
					//alert('Drag end');
				});

			autocomplete.addListener('place_changed', function() {
				infowindow.close();
				marker.setVisible(false);
				var place = autocomplete.getPlace();
				if (!place.geometry) {
					// User entered the name of a Place that was not suggested and
					// pressed the Enter key, or the Place Details request failed.
					window.alert("No details available for input: '" + place.name + "'");
					return;
				}

				// If the place has a geometry, then present it on a map.
				if (place.geometry.viewport) {
					map.fitBounds(place.geometry.viewport);
				} else {
					map.setCenter(place.geometry.location);
					map.setZoom(17);  // Why 17? Because it looks good.
				}
				marker.setPosition(place.geometry.location);
				marker.setVisible(true);

				// Update Location Co-ordinates Values
				$('#latitude').val(place.geometry.location.lat());
				$('#longitude').val(place.geometry.location.lng());

				var address = '';
				if (place.address_components) {
					address = [
						(place.address_components[0] && place.address_components[0].short_name || ''),
						(place.address_components[1] && place.address_components[1].short_name || ''),
						(place.address_components[2] && place.address_components[2].short_name || '')
					].join(' ');

					// Get each component of the address from the place details
					// and fill the corresponding field on the form.
					for (var i = 0; i < place.address_components.length; i++) {
					  var addressType = place.address_components[i].types[0];
					  if (componentForm[addressType]) {
						var val = place.address_components[i][componentForm[addressType]];
						if(typeof componentIds[addressType] != 'undefined'){
							document.getElementById(componentIds[addressType]).value = val;
						}
					  }
					}
				}

				infowindowContent.children['place-icon'].src = place.icon;
				infowindowContent.children['place-name'].textContent = place.name;
				infowindowContent.children['place-address'].textContent = address;
				infowindow.open(map, marker);
			});

			// Sets a listener on a radio button to change the filter type on Places
			// Autocomplete.
			function setupClickListener(id, types) {
				var radioButton = document.getElementById(id);
				radioButton.addEventListener('click', function() {
					autocomplete.setTypes(types);
				});
			}

			setupClickListener('changetype-all', []);
			setupClickListener('changetype-address', ['address']);
			setupClickListener('changetype-establishment', ['establishment']);
			setupClickListener('changetype-geocode', ['geocode']);

			document.getElementById('use-strict-bounds')
			.addEventListener('click', function() {
				console.log('Checkbox clicked! New state=' + this.checked);
				autocomplete.setOptions({strictBounds: this.checked});
			});
		}
	</script>
<script>
	$(function(){
		$('.tags-input').tagsinput({
			allowDuplicates: false
		});

		@if(isset($e))
		$('#relationship_with_patient').selectpicker('val','{{ $e->relationship_with_patient }}');
		@endif

		$('.content').on('change', '.searchCol', function(){
			checkIfExistingCustomer();
		});

		$('.content').on('change','input[name=billable]', function(){
			if($(this).val() == 1){
				$('#billable_block').show();
			}else{
				$('#billable_block').hide();
			}
		});

		$('.content').on('change','input[name=discount_applicable]', function(){
			if($(this).val() == 1){
				$('#discount_block').show();
			}else{
				$('#discount_block').hide();
			}
		});


		$('.content').on('click', '.btnSelectStaff', function(){
			emp = $('input[name=choosen_emp]:checked').val();
			name = $('input[name=choosen_emp]:checked').data('name');
			mobile = $('input[name=choosen_emp]:checked').data('mobile');

			 if(typeof emp != 'undefined' && emp != ''){
				 $('#staffSearchModal').modal('hide');
				 $('#selectedStaffName').html('<div class="col-sm-2"><img src="/user.png" width="40" height="40" class="img-circle"></div><div class="col-sm-9">'+name+'<br>Mob: '+mobile+'</div>');
			 }else{
				 alert("Please select staff !");
			 }
		});

		$('.date').inputmask('dd-mm-yyyy', { placeholder: '__-__-____' });
		/* $('.money-inr').inputmask('₹ 999999.99', { numericInput: true, rightAlign: false }); */
	});

	function checkIfExistingCustomer(){
		var fname = $('#first_name').val();
		var lname = $('#last_name').val();
		var mobile = $('#mobile').val();
		var email = $('#email').val();
		var dob = $('#date_of_birth').val();

		if(fname != '' || lname != '' || mobile != '' || email != '' || dob != ''){
			$.ajax({
				url: '{{ route('patient.check-for-existing-patient') }}',
				type: 'POST',
				data: {f: fname, l: lname, m: mobile, e: email, d: dob, _token: '{{ csrf_token() }}'},
				success: function (data){
					console.log(data);
				},
				error: function (error){
					console.log(error);
				}
			});
		}
	}

	function checkSkills(){
		return false;
	}
</script>
@endsection
