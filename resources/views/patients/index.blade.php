@extends('layouts.main-layout')

@section('page_title','Patients - ')

@section('active_patients','active')

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header clearfix">
                <h2 class="col-sm-6">
                    Patients
                    <small>List of all patients</small>
                </h2>
                <div class="col-sm-6 text-right">
                    <a href="{{ route('patient.create') }}" class="btn-group" role="group">
                        <button type="button" class="btn btn-success waves-effect"><i class="material-icons">add_circle</i></button>
                        <button type="button" class="btn btn-success waves-effect" style="line-height: 1.9;padding-left:0">New Patient</button>
                    </a>&nbsp;&nbsp;
                    {{-- <a href="{{ route('patient.create') }}" class="pull-right btn btn-success waves-effect waves-float">
                        <i class="material-icons">add</i> New Patient
                    </a> --}}
                </div>
            </div>
            <div class="body">
                <table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="18%">Name</th>
                            <th width="5%">Gender</th>
                            <th width="5%">Age</th>
                            <th width="12%">Mobile</th>
                            <th width="12%">Email</th>
                            <th width="12%">Enquirer</th>
                            <th width="20%">Location</th>
                            <th width="15%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($patients as $i => $p)
                        <tr>
                            <td>{{ $i + 1 }}</td>
                            <td>
                                <div style="display: inline-block; vertical-align: top;">
                                    <img src="{{ asset('img/default-avatar.jpg') }}" width="40" height="40" class="img-circle img-thumbnail" alt="{{ $p->full_name }}" title="{{ $p->full_name }}" />
                                </div>
                                <div style="display: inline-block; margin-left: 2%">
                                    {!! $p->first_name.'<br><small>'.$p->last_name.'</small>' !!}
                                </div>
                            </td>
                            <td>{{ $p->gender }}</td>
                            <td>{{ $p->patient_age.' yrs' }}</td>
                            <td>{{ $p->contact_number }}</td>
                            <td>{{ $p->email }}</td>
                            <td>{{ $p->enquirer_name }}</td>
                            <td>{!! $p->city.'<br>'.$p->state  !!}</td>
                            <td>
                                <a href="{{ route('patient.edit',Helper::encryptor('encrypt',$p->id)) }}" class="btn btn-warning btn-sm"><i class="material-icons">edit</i></a>
                                <form style="display: inline" action="{{ route('patient.destroy',Helper::encryptor('encrypt',$p->id)) }}" method="POST">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" onclick="javascript: return confirm('Are you sure?');" class="btn btn-danger btn-sm"><i class="material-icons">delete</i></a>
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="8" class="text-center">No patient record(s) found. <a class="btn btn-info btn-sm">Create Patient</a></td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
