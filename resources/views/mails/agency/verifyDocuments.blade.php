@extends('layouts.agencyMail')

@section('content')
<div class="row clearfix">
	<p>We hereby attach the required Documents for verification for -<br> 
	Name : {{ $caregiver->first_name.' '.$caregiver->last_name}}<br>
	Please find the necessary documents attached below</p>

	<p>Please revert back to us via our official email or phone number once the verification process is completed.</p><br>

	<p>
		Regards,<br><br>

		<b>{{session('contact_person')}}</b><br>
		<b style="color: #187fc0">{{session('organization_name')}}</b>
	</p>
	<br>
	<i style="color: #ffc107">PS: This is an auto-generated mail. Do not reply</i><br>
</div>
@endsection