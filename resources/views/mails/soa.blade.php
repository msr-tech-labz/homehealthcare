<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style>
        .border-top{
            border-top: 1px solid #333 !important;
        }
        .border-bottom{
            border-bottom: 1px solid #333 !important;
        }
        .border-right{
            border-right: 1px solid #333 !important;
        }
        .border-left{
            border-left: 1px solid #333 !important;
        }

         .no-padding, .no-padding tbody tr td, .no-padding tbody tr th{
            padding: 0 !important;
            border: 1px solid #333;
        }
        .no-border, .no-border tbody tr td, .no-border tbody tr th{
            border: 0;
        }
         .services-table, .services-table thead tr th, .services-table tfoot tr th, .services-table tfoot tr td, .services-table  tbody tr th,.services-table tbody tr td{
             padding: 1px !important;
             border: 0;
         }

         .services-table{
            margin-left: 5px;
         }

         .services-table thead tr th, .text-center{
             text-align: center !important;
         }

         .text-right{
            text-align: right !important;
         }
        .tbl-text{
            margin-top:1px !important;
            padding:2px !important;
            margin-bottom:1px !important;
        }

    </style>
</head>
<body style="background: #e2e1e0; font-family: 'Roboto', sans-serif !important;">
    <div style="display:block; width: 95%; margin: 0 auto; background: #fff; border-radius: 2px; box-shadow: 0 10px 20px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);">
        <!-- Header -->
        <div style="display: block; max-width: 99%; min-height: 50px; border-bottom: 1px solid #E0E0E0; padding: 10px 10px 5px 10px; ">
            <div style="display: inline-block;text-align: center;">
                <div style="font-size: 28px; padding-left: 15px; font-weight: 300; height: 45px; color: #009688; line-height: 45px; vertical-align: middle; text-align: center; margin-right: 10px; letter-spacing: 1px">{{ $profile->organization_name ?? '' }}</div>
            </div>
        </div>

        <!-- Main Content -->
        <div style="min-height: 200px; padding: 10px 15px; font-size: 14px; line-height: 22px; text-align: justify; margin: 10px 0; border-bottom: 1px solid #E0E0E0">
            @if(isset($patient->first_name))
            <span style="font-size: 16px; font-weight: 500; margin-bottom: 20px; display: block;">Dear {{ isset($patient)?$patient->first_name:'Customer' }},</span>
            @else
            <span style="font-size: 16px; font-weight: 500; margin-bottom: 20px; display: block;">Dear Sir/Madam,</span>
            @endif

            <div class="row clearfix">
                <div style="display: block; font-size: 16px;text-align:justify;">
                    Greetings from <b>{{ $profile->organization_name ?? '' }}</b>.<br><br>
                    
                    Hope this mail finds you in good health and we trust that you find our services to patient <b>{{ isset($patient)?$patient->full_name:'' }}</b> your utmost satisfaction.<br><br>
                    
                    Kindly find the attached Statement of Accounts as of {{ $toDate ?? '' }}.<br><br>
                    
                    As of {{ $toDate ?? '' }} your net balance with {{ $profile->organization_name ?? '' }} is {!! isset($openingBalance)?'<b>Rs. '.number_format($openingBalance, 2).'</b>':'' !!}<br><br>

                    To ensure ease of process, please quote your patient ID ({{ $patient->patient_id ?? '' }}) for all future correspondence.<br><br>
                    
                    @if(isset($profile) && $profile->tenant_id == 142)
                    In case of any queries please write to us at billdesk@onelifehealthcare.in within 24 hours. <br><br>

                    Do reach out to us at nursing@onelifehomehealthcare.in if you have any service related feedback<br><br>

                    Assuring you of our best service at all time<br>
                    @endif
                    
                    (Note: Services cannot be rendered if you have a payment outstanding)<br>
                    <small>A (-ve) amount indicates, the customer is to receive the indicated amount as service or refund.</small><br>
                    <small>A (+ve) amount indicates, the customer is to pay the indicated amount to the organization.</small>
                </div>
                <br>
                <i style="color: #ffc107">PS: This is an auto-generated mail. Do not reply</i><br>
            </div>

            <br>
            Regards,<br><br>
            <table>
                <tr>
                    <td>
                        <img src="{{ isset($profile)?asset('img/provider/'.$profile->organization_logo):'' }}" style="max-height: 110px; margin: 5px 0 0 10px" title="{{ session('orgnization_name') }}">
                    </td>
                    <td width="4%"></td>
                    <td style="vertical-align: top; font-size: 13px !important;width: 100%;">
                        <span style="font-size: 14px !important;font-weight: bold">{{ $profile->organization_name ?? '' }}</span><br>
                        {!! !empty($profile->organization_address)?$profile->organization_address.'<br>':'' !!}
                        {!! !empty($profile->organization_area)?$profile->organization_area.'<br>':'' !!}
                        {!! !empty($profile->organization_city)?$profile->organization_city.'-':'' !!}
                        {!! !empty($profile->organization_zipcode)?$profile->organization_zipcode.'<br>':'' !!}
                        Tel No.: {!! !empty($profile->phone_number)?'+91-'.$profile->phone_number:'' !!}  {!! !empty($profile->landline_number)?'+91-'.$profile->landline_number:'' !!}<br>
                        {!! !empty($profile->website)?'Website: '.$profile->website:'' !!}
                    </td>
                </tr>
            </table>
        </div>

        <!-- Footer -->
        <div style="display: block; position:relative; bottom: 0; max-width: 99%; min-height: 90px; padding: 5px 10px 0px 10px; ">
            <div style="width: 50%; display: inline-block; font-size: 12px;">
                <span>Copyright &copy; {{date('Y')}} {{ $profile->organization_name ?? '' }} All Rights Reserved</span><br><br>
                <span style="font-size: 12px; font-weight: 600">
                    <a href="" style="color: #2899dc !important">Terms and Conditions</a> |
                    <a href="" style="color: #2899dc !important">Privacy Policy</a>
                </span>
            </div>
            <div style="width: 49%; display: inline-block; text-align: right; vertical-align: top; font-size: 14px">
                <p style="margin-bottom: 10px;margin-top:0;padding-top:0">Powered by</p>
                <b><small>Smart Health Connect</small></b>
            </div>
            <br><br>
        </div>
    </div>
</body>
</html>
