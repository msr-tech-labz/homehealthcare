Dear sir,<br>

{{ $subscriber->org_code }} have {{ count($caregivers) }} active users in the month of {{ $yesterday->format('M') }} - {{ $yesterday->format('Y') }}.