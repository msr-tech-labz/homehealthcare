@extends('layouts.cronjob-mail')

@section('content')
<div class="row clearfix">
	<p>Dear {{ $data['patient'] }},<br> 
	<div>
		Greetings from {{ $data['organization_name'] }}. This is a friendly reminder about the Service Order you had requested. Your payment will soon be overdue within the next few days. Below are the details of the Service.
	</div><br>
	<table class="table table-bordered" style="width: 100% !important;border:1px solid black;">
		<thead style="border:1px solid black;">
			<tr style="border:1px solid black;">
				<th style="border:1px solid black;">Episode</th>
				<th style="border:1px solid black;">Service Name</th>
				<th style="border:1px solid black;">From Date</th>
				<th style="border:1px solid black;">To Date</th>
				<th style="border:1px solid black;">Amount (in Rs.)<sup>*</sup></th>
			</tr>
		</thead>
			<tbody style="border:1px solid black;">
				<tr style="border:1px solid black;">
					<td style="border:1px solid black;">{{ $data['episode'] }}</td>
					<td style="border:1px solid black;">{{ $data['service'] }}</td>
					<td style="border:1px solid black;">{{ $data['from_date'] }}</td>
					<td style="border:1px solid black;">{{ $data['to_date'] }}</td>
					<td style="border:1px solid black;">{{ $data['amount'] }}<sup>*</sup></td>
				</tr>
			</tbody>
		</table>
	<p><i><b>* The Final Amount may vary based on the number of days the services were provider successfully.</b></i></p>
	<p>Please disregard this note if already paid.</p><br>
	<p>We hope the service is upto the mark and is helping you to recuperate</p>

	<p>
		Regards,<br>
		<b style="color: #187fc0">{{ $data['organization_name'] }}</b>
	</p>
	<br>
	<i style="color: #ffc107">PS: This is an auto-generated mail. Do not reply</i><br>
</div>
@endsection