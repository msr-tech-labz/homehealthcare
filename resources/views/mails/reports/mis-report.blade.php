<!DOCTYPE html>
<html>
<head>
    <title>MIS Report</title>
    <style>
        .left-cont {width:50%; float:left; padding:5px;}
        .right-cont {width:47%; float:left;}
        @media only screen and (max-width: 550px) {
            .left-cont {width:100%;}
            .right-cont {width:100%;}
        }
    </style>
</head>
<body>
<div style="padding:5px 8px;">
    <div style="text-align: center;"><span style="border: 1px solid black;border-radius: 5px;font-size: 20px;">Revenue</span></div>
    <div class="left-cont">
        <table style="width:100%;height: 180px;margin-top: 55px; border:1px solid #eaeaea; border-collapse:collapse;">
            <thead style="background:#f6f6f6;">
                <tr style="height: 30px;">
                    <th></th>
                    <th style="background-color:#ff9933; color:#fff;">TODATE</th>
                    <th style="background-color:#38859d; color:#fff;">MTD</th>
                    <th style="background-color:#3cb371; color:#fff;">YTD</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="border:1px solid #ddd;text-align: center;"><b>Gross</b></td>
                    <td style="border:1px solid #ddd;">Rs {{ $data['r-todate'][0] }}</td>
                    <td style="border:1px solid #ddd;">Rs {{ $data['r-mtdArr'][0] }}</td>
                    <td style="border:1px solid #ddd;">Rs {{ $data['r-ytdArr'][0] }}</td>
                </tr>
                <tr>
                    <td style="border:1px solid #ddd;text-align: center;"><b>Discount</b></td>
                    <td style="border:1px solid #ddd;">Rs {{ $data['r-todate'][1] }}</td>
                    <td style="border:1px solid #ddd;">Rs {{ $data['r-mtdArr'][1] }}</td>
                    <td style="border:1px solid #ddd;">Rs {{ $data['r-ytdArr'][1] }}</td>
                </tr>
                <tr>
                    <td style="border:1px solid #ddd;text-align: center;"><b>Revenue</b></td>
                    <td style="border:1px solid #ddd;">Rs {{ $data['r-todate'][2] }}</td>
                    <td style="border:1px solid #ddd;">Rs {{ $data['r-mtdArr'][2] }}</td>
                    <td style="border:1px solid #ddd;">Rs {{ $data['r-ytdArr'][2] }}</td>
                </tr>
                <tr>
                    <td style="border:1px solid #ddd;text-align: center;"><b>Taxed</b></td>
                    <td style="border:1px solid #ddd;">Rs {{ $data['r-todate'][3] }}</td>
                    <td style="border:1px solid #ddd;">Rs {{ $data['r-mtdArr'][3] }}</td>
                    <td style="border:1px solid #ddd;">Rs {{ $data['r-ytdArr'][3] }}</td>
                </tr>
                <tr>
                    <td style="border:1px solid #ddd;text-align: center;"><b>Manual Credit Raised</b></td>
                    <td style="border:1px solid #ddd;">Rs {{ $data['r-todate'][4] }}</td>
                    <td style="border:1px solid #ddd;">Rs {{ $data['r-mtdArr'][4] }}</td>
                    <td style="border:1px solid #ddd;">Rs {{ $data['r-ytdArr'][4] }}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="right-cont">
        {{-- <img src="{{ asset('MISCHARTS/'.$subId.'/'.date('Y-m-d').'-RevenueChart.png') }}" alt="Revenue chart" style="width:100%"> --}}
        @if(file_exists(public_path().'/MISCHARTS/'.$data['subID'].'/'.date('Y-m-d').'-RevenueChart.png'))
        <img src="{{ $message->embed(asset('MISCHARTS/'.$data['subID'].'/'.date('Y-m-d').'-RevenueChart.png')) }}" alt="Revenue chart" style="width:100%">
        @else
        <img src="{{ asset('img/not-available.png') }}" alt="Image not available" style="width: 100%;">
        @endif
    </div>
    <div style="width:100%; clear:both;"></div>
    <hr style="margin-bottom: 30px;">
    
    <div style="text-align: center;"><span style="border: 1px solid black;border-radius: 5px;font-size: 20px;">Leads</span></div>
    <div class="left-cont">
        <table style="width:100%;text-align: center; height:180px;margin-top:55px; border:1px solid #eaeaea; border-collapse:collapse;">
            <thead style="background:#f6f6f6;">
                <tr style="height: 30px;">
                    <th></th>
                    <th style="background-color:#ff9933; color:#fff;">TODATE</th>
                    <th style="background-color:#38859d; color:#fff;">MTD</th>
                    <th style="background-color:#3cb371; color:#fff;">YTD</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="border:1px solid #ddd;"><b>Pending</b></td>
                    <td style="border:1px solid #ddd;">{{ $data['l-todate'][0] }}</td>
                    <td style="border:1px solid #ddd;">{{ $data['l-mtdArr'][0] }}</td>
                    <td style="border:1px solid #ddd;">{{ $data['l-ytdArr'][0] }}</td>
                </tr>
                <tr>
                    <td style="border:1px solid #ddd;"><b>Converted</b></td>
                    <td style="border:1px solid #ddd;">{{ $data['l-todate'][1] }}</td>
                    <td style="border:1px solid #ddd;">{{ $data['l-mtdArr'][1] }}</td>
                    <td style="border:1px solid #ddd;">{{ $data['l-ytdArr'][1] }}</td>
                </tr>
                <tr>
                    <td style="border:1px solid #ddd;"><b>Closed</b></td>
                    <td style="border:1px solid #ddd;">{{ $data['l-todate'][2] }}</td>
                    <td style="border:1px solid #ddd;">{{ $data['l-mtdArr'][2] }}</td>
                    <td style="border:1px solid #ddd;">{{ $data['l-ytdArr'][2] }}</td>
                </tr>
                <tr>
                    <td style="border:1px solid #ddd;"><b>Dropped</b></td>
                    <td style="border:1px solid #ddd;">{{ $data['l-todate'][3] }}</td>
                    <td style="border:1px solid #ddd;">{{ $data['l-mtdArr'][3] }}</td>
                    <td style="border:1px solid #ddd;">{{ $data['l-ytdArr'][3] }}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="right-cont">
        {{-- <img src="{{ asset('MISCHARTS/'.$subId.'/'.date('Y-m-d').'-LeadsChart.png') }}" alt="Leads chart" style="width:100%"> --}}
        @if(file_exists(public_path().'/MISCHARTS/'.$data['subID'].'/'.date('Y-m-d').'-LeadsChart.png'))
        <img src="{{ $message->embed(asset('MISCHARTS/'.$data['subID'].'/'.date('Y-m-d').'-LeadsChart.png')) }}" alt="Leads chart" style="width:100%">
        @else
        <img src="{{ asset('img/not-available.png') }}" alt="Image not available" style="width: 100%;">
        @endif
    </div>
    <div style="width:100%; clear:both;"></div>
    <hr style="margin-bottom: 30px;">
    
    <div style="text-align: center;margin-bottom: 30px;"><span style="border: 1px solid black;border-radius: 5px;font-size: 20px;">HR</span></div>
    <div class="left-cont">
        <table style="width:100%;text-align: center; height:255px; border:1px solid #eaeaea; border-collapse:collapse;" id="hr_stats_table">
            <tr style="background-color: #f6f6f6">
                <th>Total Staff</th>
                <th style="width: 10%;">{{ $data['hrStats']['total'] }}</th>
                @foreach($data['hrStats']['cadre'] as $i => $cadre)
                    @if($cadre != '')
                    <th>{{ $i }}</th>
                    @endif
                @endforeach
            </tr>
            <tr>
                <th style="border:1px solid #ddd;">Available</th>
                <td style="border:1px solid #ddd;">{{ $data['hrStats']['available'] }}</td>
                @foreach($data['hrStats']['cadre'] as $i => $cadre)
                    <td style="border:1px solid #ddd; ">{{ strip_tags($cadre[1]) }}</th>
                @endforeach
            </tr>
            <tr>
                <th style="border:1px solid #ddd;">On Duty</th>
                <td style="border:1px solid #ddd;">{{ $data['hrStats']['onduty'] }}</td>
                @foreach($data['hrStats']['cadre'] as $i => $cadre)
                    <td style="border:1px solid #ddd; ">{{ strip_tags($cadre[2]) }}</th>
                @endforeach
            </tr>
            <tr>
                <th style="border:1px solid #ddd;">Training</th>
                <td style="border:1px solid #ddd;">{{ $data['hrStats']['training'] }}</td>
                @foreach($data['hrStats']['cadre'] as $i => $cadre)
                    <td style="border:1px solid #ddd; ">{{ strip_tags($cadre[3]) }}</th>
                @endforeach
            </tr>
            <tr>
                <th style="border:1px solid #ddd;">Leave</th>
                <td style="border:1px solid #ddd;">{{ $data['hrStats']['leave'] }}</td>
                @foreach($data['hrStats']['cadre'] as $i => $cadre)
                    <td style="border:1px solid #ddd; ">{{ strip_tags($cadre[4]) }}</th>
                @endforeach
            </tr>
            <tr>
                <th style="border:1px solid #ddd;">Absent</th>
                <td style="border:1px solid #ddd;">{{ $data['hrStats']['absent'] }}</td>
                @foreach($data['hrStats']['cadre'] as $i => $cadre)
                    <td style="border:1px solid #ddd; ">{{ strip_tags($cadre[5]) }}</th>
                @endforeach
            </tr>
        </table>
    </div>
    <div class="right-cont">
        {{-- <img src="{{ asset('MISCHARTS/'.$subId.'/'.date('Y-m-d').'-HrChart.png') }}" alt="Hr chart" style="width:100%"> --}}
        @if(file_exists(public_path().'/MISCHARTS/'.$data['subID'].'/'.date('Y-m-d').'-HrChart.png'))
        <img src="{{ $message->embed(asset('MISCHARTS/'.$data['subID'].'/'.date('Y-m-d').'-HrChart.png')) }}" alt="Hr chart" style="width:100%">
        @else
        <div style="width: 100%;height: 100%;margin-top: 15%;font-size: 30px;text-align: center;">No Attandance Marked for the selected cadres on the day.</div>
        @endif
    </div>
    <div style="width:100%; clear:both;"></div>
    <hr style="margin-bottom: 30px;">

    <div style="text-align: center;"><span style="border: 1px solid black;border-radius: 5px;font-size: 20px;">Revenue Per Service</span></div>
    <div style="overflow:auto;margin-top: 30px;">
        <table style="width:100%; border:1px solid #eaeaea; border-collapse:collapse;">
            <thead>
                <tr>
                    <th rowspan="2" style="background-color:#f6f6f6; padding:3px; min-width:130px;">Service Name</th>
                    <th colspan="3" style="background-color:#ffb3ba; padding:3px;">Today</th>
                    <th colspan="3" style="background-color:#baffc9; padding:3px;">MTD</th>
                    <th colspan="3" style="background-color:#bae1ff; padding:3px;">YTD</th>
                </tr>
                <tr>
                    <th style="background-color:#ffb3ba; padding:3px;">Shifts</th>
                    <th style="background-color:#ffb3ba; padding:3px;">Revenue</th>
                    <th style="background-color:#ffb3ba; padding:3px;">Avg Revenue</th>
                    <th style="background-color:#baffc9; padding:3px;">Shifts</th>
                    <th style="background-color:#baffc9; padding:3px;">Revenue</th>
                    <th style="background-color:#baffc9; padding:3px;">Avg Revenue</th>
                    <th style="background-color:#bae1ff; padding:3px;">Shifts</th>
                    <th style="background-color:#bae1ff; padding:3px;">Revenue</th>
                    <th style="background-color:#bae1ff; padding:3px;">Avg Revenue</th>
                </tr>
            </thead>
            <tbody>
                <?php $ts= $tr= $ta= $ms= $mr= $ma= $ys= $yr= $ya= 0; ?>
                @if(isset($data['servicesStats']['table']))
                    @foreach($data['servicesStats']['table'] as $key => $result1)
                    <tr>
                        <td style="border:1px solid #eee; padding:3px;"><p style="display:inline;">{{ $result1['service'] }}</p></td>
                        <td style="border:1px solid #eee; background-color:#ffb3ba; padding:3px; text-align:center;">{{ $result1['todayShifts'] }}</td>
                        <td style="border:1px solid #eee; background-color:#ffb3ba; padding:3px; text-align:center;">{{ $result1['todayRevenue'] }}</td>
                        <td style="border:1px solid #eee; background-color:#ffb3ba; padding:3px; text-align:center;">{{ $result1['todayAverageRevenue'] }}</td>
                        <td style="border:1px solid #eee; background-color:#baffc9; padding:3px; text-align:center;">{{ $result1['mtdShifts'] }}</td>
                        <td style="border:1px solid #eee; background-color:#baffc9; padding:3px; text-align:center;">{{ $result1['mtdRevenue'] }}</td>
                        <td style="border:1px solid #eee; background-color:#baffc9; padding:3px; text-align:center;">{{ $result1['mtdAverageRevenue'] }}</td>
                        <td style="border:1px solid #eee; background-color:#bae1ff; padding:3px; text-align:center;">{{ $result1['ytdShifts'] }}</td>
                        <td style="border:1px solid #eee; background-color:#bae1ff; padding:3px; text-align:center;">{{ $result1['ytdRevenue'] }}</td>
                        <td style="border:1px solid #eee; background-color:#bae1ff; padding:3px; text-align:center;">{{ $result1['ytdAverageRevenue'] }}</td>
                    </tr>
                    <?php   $ts= $result1['todayShifts']+$ts; $tr= $result1['todayRevenue']+$tr; $ta= $result1['todayAverageRevenue']+$ta;
                            $ms= $result1['mtdShifts']+$ms; $mr= $result1['mtdRevenue']+$mr; $ma= $result1['mtdAverageRevenue']+$ma;
                            $ys= $result1['ytdShifts']+$ys; $yr= $result1['ytdRevenue']+$yr; $ya= $result1['ytdAverageRevenue']+$ya; ?>
                    @endforeach
                @else
                    <td colspan="10" style="border:1px solid #eee; background-color:#bae1ff; padding:3px;">No Data</td>
                @endif
            </tbody>
            <tfoot style="border-top:2px solid #7e927d; background-color:#c4dfc3; text-align:center;">
                <tr>
                    <td style="border:1px solid #eee; padding:3px;">Total</td>
                    <td style="border:1px solid #eee; padding:3px;">{{ $ts }}</td>
                    <td style="border:1px solid #eee; padding:3px;">{{ $tr }}</td>
                    <td style="border:1px solid #eee; padding:3px;">{{ $ta }}</td>
                    <td style="border:1px solid #eee; padding:3px;">{{ $ms }}</td>
                    <td style="border:1px solid #eee; padding:3px;">{{ $mr }}</td>
                    <td style="border:1px solid #eee; padding:3px;">{{ $ma }}</td>
                    <td style="border:1px solid #eee; padding:3px;">{{ $ys }}</td>
                    <td style="border:1px solid #eee; padding:3px;">{{ $yr }}</td>
                    <td style="border:1px solid #eee; padding:3px;">{{ $ya }}</td>
                </tr>
            </tfoot>
        </table>
    </div>
    <h5 style="text-align:center; margin-bottom:2px;">MANUAL CREDIT AMOUNT</h5>
    <table style="width:100%; border:0; border-collapse:collapse; text-align:center;">
        <thead style="border-top:2px solid #aabcfe;">
            <tr>
                <th style="background-color:#f6f6f6;"></th>
                <th style="background-color:#f6c9ee; padding:3px;">TODATE</th>
                <th style="background-color:#f6c9ee; padding:3px;">MTD</th>
                <th style="background-color:#f6c9ee; padding:3px;">YTD</th>
            </tr>
        </thead>
        <tbody style="border-bottom:2px solid #aabcfe;">
            @foreach($data['servicesStats']['creditTable'] as $key => $result2)
            <tr>
                <td style="padding:3px;">MANUAL CREDITS RAISED</td>
                <td style="background-color:#f6c9ee; padding:3px;">{{ $result2['todate'] }}</td>
                <td style="background-color:#f6c9ee; padding:3px;">{{ $result2['mtd'] }}</td>
                <td style="background-color:#f6c9ee; padding:3px;">{{ $result2['ytd'] }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div style="width:100%; clear:left;"></div>
</div>
</body>
</html>