<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            border-collapse: collapse;
        }
        table, th, td {
            border: 1px solid #ccc;
            padding: 5px;
        }
        th{
            color: white;
            white-space: nowrap;
        }
        td {
            font-size: 14px;
        }
        h3{
            width:100%;
            text-align:center;
            border: 2px solid black;
            border-radius: 20px;
            padding: 5px 0px 5px 0px;
            background: repeating-linear-gradient(-45deg, #fbfbfb, #c9cce8 5px, white 1px, white 5px);
            font-weight: bold;
        }
        h4{
            text-align:center;
            vertical-align:middle;
            border: 1px solid darkgrey;
            border-radius: 20px;
            padding: 5px 5px 5px 5px;
        }
    </style>
</head>
<body style="font-family: 'Roboto', sans-serif !important; font-size: 16px">
    <div style="background: #fff; border-radius: 2px; box-shadow: 0 10px 20px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23); padding: 15px;">
        <h3>Leads details for {{ date('d-m-Y') }}</h3>
        <h4 style="background: repeating-linear-gradient(-45deg, #fbfbfb, #e2a6ec 4px, white 1px, white 4px);"><strong>Leads Summary</strong></h4>
        <table width="100%" border="1">
            <thead>
                <tr style="background: #c300e7">
                    <th>Received</th>
                    <th>Converted</th>
                    <th>Pending</th>
                    <th>Dropped</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <tbody style="text-align: center;">
                        <td>{{ count($data['received']) }}</td>
                        <td>{{ count($data['converted']) }}</td>
                        <td>{{ count($data['pending']) }}</td>
                        <td>{{ count($data['dropped']) }}</td>
                    </tbody>
                </tr>
            </tbody>
        </table>
        <br>
        <br>
        <h4 style="background: repeating-linear-gradient(-45deg, #fbfbfb, #aaefaa 4px, white 1px, white 4px);"><strong>Leads Converted</strong></h4>
        <div style="overflow:auto;">
            <table width="100%" border="1">
            <thead>
            <tr style="background: rgba(14,179,14,0.82);">
                <th>Patient</th>
                <th>Enquirer</th>
                <th>Email</th>
                <th>Area</th>
                <th>Gender</th>
                <th>Description</th>
                <th>Category</th>
                <th>Starting Date</th>
                <th>Actual Rate</th>
                <th>Rate Agreed</th>
            </tr>
            </thead>
            <tbody>
                @forelse($data['converted'] as $converted)
                <tr>
                    <td>{{ $converted->patient->fullname }}</td>
                    <td>{{ $converted->patient->enquirer_name }}</td>
                    <td>{{ $converted->patient->email }}</td>
                    <td>{{ $converted->patient->area }}</td>
                    <td>{{ $converted->patient->gender }}</td>
                    <td>{{ $converted->case_description }}</td>
                    <td>{{ isset($converted->serviceRequired)?$converted->serviceRequired->service_name:'NA' }}</td>
                    <td>{{ (isset($converted->schedules) && count($converted->schedules))?$converted->schedules->first()->schedule_date->format('d-m-Y'):'NA' }}</td>
                    <td>{{ isset($converted->serviceRequired->ratecard)?$converted->serviceRequired->ratecard->amount:'NA' }}</td>
                    <td>{{ $converted->rate_agreed }}</td>
                </tr>
                @empty
                    <td colspan="10" style="text-align:center;">No Converted Leads</td>
                @endforelse
            </tbody>
            </table>
        </div>
        <br>
        <br>
        <h4 style="background: repeating-linear-gradient(-45deg, #fbfbfb, #efc08d 4px, white 1px, white 4px);"><strong>Leads Pending</strong></h4>
        <div style="overflow:auto;">
            <table width="100%" border="1">
            <thead>
            <tr style="background: rgba(247,129,3,0.82);">
                <th>Patient</th>
                <th>Enquirer</th>
                <th>Email</th>
                <th>Area</th>
                <th>Gender</th>
                <th>Description</th>
                <th>Category</th>
                <th>Actual Rate</th>
                <th>Rate Agreed</th>
            </tr>
            </thead>
            <tbody>
                @forelse($data['pending'] as $pending)
                <tr>
                    <td>{{ $pending->patient->fullname }}</td>
                    <td>{{ $pending->patient->enquirer_name }}</td>
                    <td>{{ $pending->patient->email }}</td>
                    <td>{{ $pending->patient->area }}</td>
                    <td>{{ $pending->patient->gender }}</td>
                    <td>{{ $pending->case_description }}</td>
                    <td>{{ isset($pending->serviceRequired)?$pending->serviceRequired->service_name:'NA' }}</td>
                    <td>{{ isset($pending->serviceRequired->ratecard)?$pending->serviceRequired->ratecard->amount:'NA' }}</td>
                    <td>{{ $pending->rate_agreed }}</td>
                </tr>
                @empty
                    <td colspan="9" style="text-align:center;">No Pending Leads</td>
                @endforelse
            </tbody>
            </table>
        </div>
        <br>
        <br>
        <h4 style="background: repeating-linear-gradient(-45deg, #fbfbfb, #f1a5d6 4px, white 1px, white 4px);"><strong>Leads Dropped</strong></h4>
        <div style="overflow:auto;">
            <table width="100%" border="1" style="overflow-y: scroll;">
            <thead>
            <tr style="background: rgba(239,57,176,0.82);">
                <th>Patient</th>
                <th>Enquirer</th>
                <th>Email</th>
                <th>Area</th>
                <th>Gender</th>
                <th>Description</th>
                <th>Category</th>
                <th>Actual Rate</th>
                <th>Rate Agreed</th>
                <th>Reason</th>
            </tr>
            </thead>
            <tbody>
                @forelse($data['dropped'] as $dropped)
                <tr>
                    <td>{{ $dropped->patient->fullname }}</td>
                    <td>{{ $dropped->patient->enquirer_name }}</td>
                    <td>{{ $dropped->patient->email }}</td>
                    <td>{{ $dropped->patient->area }}</td>
                    <td>{{ $dropped->patient->gender }}</td>
                    <td>{{ $dropped->case_description }}</td>
                    <td>{{ isset($dropped->serviceRequired)?$dropped->serviceRequired->service_name:'NA' }}</td>
                    <td>{{ isset($dropped->serviceRequired->ratecard)?$dropped->serviceRequired->ratecard->amount:'NA' }}</td>
                    <td>{{ $dropped->rate_agreed }}</td>
                    <td>{{ $dropped->dropped_reason }}</td>
                </tr>
                @empty
                    <td colspan="10" style="text-align:center;">No Dropped Leads</td>
                @endforelse
            </tbody>
            </table>
        </div>
        <br>
        <br>
        <div style="border-top: 3px solid #009688 !important;"></div><br>
        <i style="color: #ffc107">PS: This is an auto-generated mail. Do not reply</i><br>
        <i style="color: red">NOTE: Please zoom the screen slightly to scroll and view the table contents</i><br>
    </div>
</body>
</html>