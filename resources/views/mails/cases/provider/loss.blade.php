<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>
	</style>
</head>
<body style="font-family: 'Roboto', sans-serif !important; font-size: 16px">
<div style="display:block; width: 95%; margin: 0 auto; background: #fff; border-radius: 2px; box-shadow: 0 10px 20px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);">
		<div style="display: block; text-align: center !important; max-width: 99%; min-height: 50px; border-bottom: 1px solid #E0E0E0; padding: 10px 10px 5px 10px; ">
		    <div style="display: inline-block; text-align: center !important;">
		        <div style="font-size: 28px; padding-left: 15px; font-weight: 300; height: 45px; color: #009688; line-height: 45px; vertical-align: middle; margin-right: 10px; letter-spacing: 1px; text-align: center !important;">{{ session('organization_name') }}</div>
		    </div>
		</div>
		<p>Dear {{ $patient->first_name.' '.$patient->last_name }},</p>
		<p>
		We thank you for contacting {{ session('organization_name') }}.<br>We are so sorry
		that we could not serve you.<br>Our customers are very important to us. We
		look forward to hearing from you again and hope you will give us another
		opportunity to serve you and your loved ones.<br>{{ session('organization_name') }} is always here to
		help you find reliable, high-quality healthcare and elderly care.<br><br>
		Regards,<br>{{ session('organization_name') }}<br>
		<table style="border-top: 3px solid #009688 !important;">
			<tr>
				<td>
					<img src="{{ asset('uploads/provider/'.session('tenant_id').'/'.session('organization_logo')) }}" style="max-height: 110px; margin: 5px 0 0 10px" title="{{ session('orgnization_name') }}">
				</td>
				<td width="4%"></td>
				<td style="vertical-align: top; font-size: 13px !important;width: 100%;">
					<span style="font-size: 14px !important;font-weight: bold">{{ session('org_name') }}</span><br>
					{{ session('organization_address') }}<br>
					{{ session('organization_city') }} - {{ session('organization_zipcode') }}<br>
					{{ session('organization_state') }}, {{ session('organization_country') }}<br><br>
					Phone: {{ session('phone_number') }}
				</td>
			</tr>
		</table>
		<i style="color: #ffc107">PS: This is an auto-generated mail. Do not reply</i><br>
	</div>
</body>
</html>