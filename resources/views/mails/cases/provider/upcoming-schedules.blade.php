<!DOCTYPE html>
<html>
<head>
	<style>
		table {
			border-collapse: collapse;
		}
		table, th, td {
			border: 1px solid #ccc;
			padding: 5px;
		}
	</style>
</head>
<body style="font-family: 'Roboto', sans-serif !important; font-size: 16px">
	<div style="display:block; width: 95%; margin: 0 auto; background: #fff; border-radius: 2px; box-shadow: 0 10px 20px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);">
		<h3 style="width:100%;display:block;text-align:center">Field Staff details on {{ date('d-m-Y') }}</h3>
		<table width="96%" border="1">
			<tr style="height: 40px;background: #ccc">
				<td colspan="2">
					<strong style="width:60%;display:inline-block;float:left;margin-left:10px;text-align: :center;vertical-align:middle;font-weight:bold">
						Schedules
					</strong>
					<span style="display:inline-block;float:right;margin-right:10px;">
						Total Clients: <b>{{ $total_clients ?? 0 }}</b>,
						Total Shifts: <b>{{ $total_schedules ?? 0 }}</b>
					</span>
				</td>
			</tr>
		@if(isset($schedules))
			@foreach ($schedules as $patient => $schedule)
				<tr>
					<td style="min-width:30%;text-align: left;">{{ ucwords($patient) }}</td>
					<td style="font-size: 14px;">
					@if(!empty($schedule)) {!! $schedule !!} @endif
					</td>
				</tr>
			@endforeach
		@else
			<tr>
				<td colspan="2">No data</td>
			</tr>
		@endif
		</table>
		<br>
		<table width="96%" border="1">
			<tr style="background: #ccc">
				<th colspan="2">Leaves</th>
			</tr>
		@if(isset($leaves))
			@foreach ($leaves as $type => $leave)
				<tr>
					<td style="min-width:30%;text-align: left;">{{ ucwords($type) }}</td>
					<td style="font-size: 14px;">
					@if(!empty($leave)) {!! $leave !!} @endif
					</td>
				</tr>
			@endforeach
		@else
			<tr>
				<td colspan="2">No data</td>
			</tr>
		@endif
		</table>
		<br>
		<table width="96%" border="1">
			<tr style="background: #ccc">
				<th colspan="2">Bench</th>
			</tr>
		@if(isset($bench) && !empty($bench))
			<tr>
				<td colspan="2">
					<div style="column-count: 3;column-rule: 1px solid lightblue;-webkit-column-rule: 1px solid lightblue;-moz-column-rule: 1px solid lightblue;">
					@if(!empty($bench))
					{? $benchArr = explode(", ",$bench); ?}
					@foreach ($benchArr as $b)
					<span style="display: block;line-height: 30px;font-size: 13px;">{!! $b !!}</span>
					@endforeach
					@endif
					</div>
				</td>
			</tr>
		@else
			<tr>
				<td colspan="2">No data</td>
			</tr>
		@endif
		</table>
		<br>
		<div style="border-top: 3px solid #009688 !important;"></div>
		<i style="color: #ffc107">PS: This is an auto-generated mail. Do not reply</i><br>
	</div>
</body>
</html>
