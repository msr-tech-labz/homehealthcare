<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style>
        .border-top{
            border-top: 1px solid #333 !important;
        }
        .border-bottom{
            border-bottom: 1px solid #333 !important;
        }
        .border-right{
            border-right: 1px solid #333 !important;
        }
        .border-left{
            border-left: 1px solid #333 !important;
        }

         .no-padding, .no-padding tbody tr td, .no-padding tbody tr th{
            padding: 0 !important;
            border: 1px solid #333;
        }
        .no-border, .no-border tbody tr td, .no-border tbody tr th{
            border: 0;
        }
         .services-table, .services-table thead tr th, .services-table tfoot tr th, .services-table tfoot tr td, .services-table  tbody tr th,.services-table tbody tr td{
             padding: 1px !important;
             border: 0;
         }

         .services-table{
            margin-left: 5px;
         }

         .services-table thead tr th, .text-center{
             text-align: center !important;
         }

         .text-right{
            text-align: right !important;
         }
        .tbl-text{
            margin-top:1px !important;
            padding:2px !important;
            margin-bottom:1px !important;
        }

    </style>
</head>
<body style="font-family: 'Roboto', sans-serif !important;">
    <div style="display:block; width: 95%; margin: 0 auto; background: #fff; border-radius: 2px; box-shadow: 0 10px 20px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);">
        <!-- Header -->
        <div style="display: block; text-align: center !important; max-width: 99%; min-height: 50px; border-bottom: 1px solid #E0E0E0; padding: 10px 10px 5px 10px; ">
            <div style="display: inline-block; text-align: center !important;">
                <div style="font-size: 28px; padding-left: 15px; font-weight: 300; height: 45px; color: #009688; line-height: 45px; vertical-align: middle; margin-right: 10px; letter-spacing: 1px; text-align: center !important;">{{ session('organization_name') }}</div>
            </div>
        </div>

        <!-- Main Content -->
        <div style="min-height: 200px; padding: 10px 15px; font-size: 14px; line-height: 22px; text-align: justify; margin: 10px 0; border-bottom: 1px solid #E0E0E0">

            <div class="row clearfix" style="font-size: 16px">
                <div style="display: block; font-size: 16px;text-align:justify;">
                    Dear {{ $patient->first_name.' '.$patient->last_name }},<br><br>
                    Greetings from <b>{{ session('organization_name') }}</b>.<br>
                    <br><br>
                    Thank you for choosing our services<br>
                    We assure you that we will be providing best in class services for you.<br>
                    You have requested for <b>@if($lead->aggregator_lead == 1) {{ $lead->aggregator_service }} @else {{ Helper::getServiceName($lead->service_required) }} @endif</b> in <b>{{$patient->area}} , {{$patient->city}}</b>.<br>
                    Our team members will contact you  at <b>{{$patient->contact_number}}/{{$patient->alternate_number}}</b> to coordinate with you.<br>
                    Looking forward to serving you,
                    <br>
                    Please click below link to fill the registration form
                    <br>
                    https://smarthealthconnect.com/regform/{{ Helper::encryptor('encrypt',$patient->id) }} 
                    <br>Please accept the Terms & Conditions by clicking the below link<br>
                        https://smarthealthconnect.com/terms&conditions/{{ Helper::encryptor('encrypt',$patient->id) }}<br>We encourage you to use our mobile application called Familia', connecting with your loved ones virtually.<br>
                        http://smarthealthconnect.localhost/famila/{{ Helper::encryptor('encrypt',$patient->id) }}/{{ session('dbName') }}
                    <br>
                </div><br><br>
                Regards,<br>{{ session('organization_name') }}<br>
                <table style="border-top: 3px solid #009688 !important;">
                <tr>
                    <td>
                        <img src="{{ asset('uploads/provider/'.session('tenant_id').'/'.session('organization_logo')) }}" style="max-height: 110px; margin: 5px 0 0 10px" title="{{ session('orgnization_name') }}">
                    </td>
                    <td width="4%"></td>
                    <td style="vertical-align: top; font-size: 13px !important;width: 100%;">
                        <span style="font-size: 14px !important;font-weight: bold">{{ session('org_name') }}</span><br>
                        {{ session('organization_address') }}<br>
                        {{ session('organization_city') }} - {{ session('organization_zipcode') }}<br>
                        {{ session('organization_state') }}, {{ session('organization_country') }}<br><br>
                        Phone: {{ session('phone_number') }}
                    </td>
                </tr>
                </table>
                <i style="color: #ffc107">PS: This is an auto-generated mail. Do not reply</i><br>
            </div>
        </div>
    </div>
</body>
</html>