<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style>
        .border-top{
            border-top: 1px solid #333 !important;
        }
        .border-bottom{
            border-bottom: 1px solid #333 !important;
        }
        .border-right{
            border-right: 1px solid #333 !important;
        }
        .border-left{
            border-left: 1px solid #333 !important;
        }

         .no-padding, .no-padding tbody tr td, .no-padding tbody tr th{
            padding: 0 !important;
            border: 1px solid #333;
        }
        .no-border, .no-border tbody tr td, .no-border tbody tr th{
            border: 0;
        }
         .services-table, .services-table thead tr th, .services-table tfoot tr th, .services-table tfoot tr td, .services-table  tbody tr th,.services-table tbody tr td{
             padding: 1px !important;
             border: 0;
         }

         .services-table{
            margin-left: 5px;
         }

         .services-table thead tr th, .text-center{
             text-align: center !important;
         }

         .text-right{
            text-align: right !important;
         }
        .tbl-text{
            margin-top:1px !important;
            padding:2px !important;
            margin-bottom:1px !important;
        }

    </style>
</head>
<body style="background: #e2e1e0; font-family: 'Roboto', sans-serif !important;">
    <div style="display:block; width: 95%; margin: 0 auto; background: #fff; border-radius: 2px; box-shadow: 0 10px 20px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);">
        <!-- Header -->
        <div style="display: block; max-width: 99%; min-height: 50px; border-bottom: 1px solid #E0E0E0; padding: 10px 10px 5px 10px; ">
            <div style="display: inline-block; float:left;">
                <div style="font-size: 25px; padding-left: 15px; font-weight: 300; height: 45px; color: #009688; line-height: 45px; vertical-align: middle; margin-right: 10px; letter-spacing: 1px">{{ session('organization_name') }}</div>
            </div>
        </div>

        <!-- Main Content -->
        <div style="min-height: 200px; padding: 10px 15px; font-size: 14px; line-height: 22px; text-align: justify; margin: 10px 0; border-bottom: 1px solid #E0E0E0">
            <span style="font-size: 16px; font-weight: 500; margin-bottom: 20px; display: block;">Dear Team,</span>

            <div class="row clearfix">
                <div style="display: block; font-size: 16px;text-align:justify;">
                    Please find the consumables requirement for the case.
                </div><br>

                <table class="tbl services-table no-padding" style="width: 90%">
                    <thead>
                        <th width="8%" class="text-center border-top border-left border-right border-bottom" style="font-size:10px !important">Sl.No</th>
                        <th class="text-center border-top border-right border-bottom" style="font-size:10px !important">Name</th>
                        <th class="text-center border-top border-right border-bottom" style="font-size:10px !important">Quantity</th>
                        <th class="text-center border-top border-right border-bottom" style="font-size:10px !important">Rate in Rs.</th>
                        <th class="text-center border-top border-right border-bottom" style="font-size:10px !important">Amount in Rs.</th>
                    </thead>
                    <tbody>
                        @if(count($billables))
                        {? $price = 0; $totalQuantity = 0; ?}
                        @foreach ($billables as $index => $item)
                        <tr>
                          <th class="text-center border-left border-right">{{ $index + 1 }}</th>
                          <td class="border-right" style="padding: 2px 5px !important;">
                              @if($item->category == 'Consumables')
                                  {{ $item->consumable->consumable_name }}
                              @endif
                              @if($item->category == 'Equipments')
                                  {{ $item->surgical->surgical_name }}
                              @endif
                              @if($item->category == 'Pharmaceuticals')
                                  {{ $item->item }}
                              @endif
                          </td>
                          <td class="text-center border-right">{{ $item->quantity }}</td>
                          <td class="text-center border-right">Rs. {{ $item->rate }}</td>
                          <td class="text-right border-right" style="padding-right: 20px !important;">Rs. {{ number_format(floatval($item->rate) * $item->quantity,2) }}</td>
                      </tr>
                      {? $price += floatval($item->amount); ?}
                      {? $totalQuantity += $item->quantity; ?}
                      @endforeach
                      @endif
                  </tbody>
                  <tfoot>
                    <tr>
                      <th colspan="2" class="text-center border-left border-right border-top border-bottom">Total</th>
                      <th class="text-right border-right border-top border-bottom" style="text-align:center !important;">{{ $totalQuantity }}</th>
                      <th class="text-right border-right border-top border-bottom"></th>
                      <th class="text-right border-right border-top border-bottom" style="padding-right: 20px !important;">Rs. {{ number_format($price,2) }}</th>
                    </tr>
                  </tfoot>
                </table>
                <br><br>
                <i style="color: #ffc107">PS: This is an auto-generated mail. Do not reply</i><br>
            </div>

            <br>
            Regards,{{ session('organization_name') }}<br><br>
            <table>
                <tr>
                    <td>
                        <img src="{{ asset("uploads/provider/".session('tenant_id')."/".session('organization_logo')) }}" style="max-height: 110px; margin: 5px 0 0 10px" title="{{ session('organization_name') }}">
                    </td>
                    <td width="4%"></td>
                    <td style="vertical-align: top; font-size: 13px !important;width: 100%;">
                        <span style="font-size: 14px !important;font-weight: bold">{{ session('organization_name') }}</span><br>
                        {{ session('organization_address') }}<br>
                        {{ session('organization_city') }} - {{ session('organization_zipcode') }}<br>
                        {{ session('organization_state') }}, {{ session('organization_country') }}<br><br>
                        Phone: {{ session('phone_number') }}
                    </td>
                </tr>
            </table>
        </div>

        <!-- Footer -->
        <div style="display: block; position:relative; bottom: 0; max-width: 99%; min-height: 90px; padding: 5px 10px 0px 10px; ">
            <div style="width: 50%; display: inline-block; font-size: 12px;">
                <span style="font-size: 12px; font-weight: 600">
                    <a href="" style="color: #2899dc !important">Terms and Conditions</a> |
                    <a href="" style="color: #2899dc !important">Privacy Policy</a>
                </span>
            </div>
            <div style="width: 49%; display: inline-block; text-align: right; vertical-align: top; font-size: 14px">
                <img src="{{ asset("uploads/provider/".session('tenant_id')."/".session('organization_logo')) }}" alt="{{ session('organization_name') }}" title="{{ session('organization_name') }}" style="width: 60px"/>
            </div>
            <br><br>
        </div>
    </div>
</body>
</html>
