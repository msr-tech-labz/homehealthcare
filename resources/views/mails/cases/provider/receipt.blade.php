<!DOCTYPE html>
<html>
<head>
	<title></title>
    <style>
        .border-top{
            border-top: 1px solid #333 !important;
        }
        .border-bottom{
            border-bottom: 1px solid #333 !important;
        }
        .border-right{
            border-right: 1px solid #333 !important;
        }
        .border-left{
            border-left: 1px solid #333 !important;
        }
        .no-padding, .no-padding tbody tr td, .no-padding tbody tr th{
            padding: 0 !important;
            border: 1px solid #333;
        }
        .no-border, .no-border tbody tr td, .no-border tbody tr th{
            border: 0;
        }
		.services-table, .services-table thead tr th, .services-table tfoot tr th, .services-table tfoot tr td, .services-table  tbody tr th,.services-table tbody tr td{
			padding: 1px !important;
			border: 0;
		}
		.services-table{
			margin-left: 5px;
		}
		.services-table thead tr th, .text-center{
			text-align: center !important;
		}
		.text-right{
			text-align: right !important;
		}
		.tbl-text{
			margin-top:1px !important;
			padding:2px !important;
			margin-bottom:1px !important;
		}
    </style>
</head>
<body style="background: #e2e1e0; font-family: 'Roboto', sans-serif !important;">
	<div style="display:block; width: 95%; margin: 0 auto; background: #fff; border-radius: 2px; box-shadow: 0 10px 20px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);">
		<!-- Header -->
		<div style="display: block; max-width: 99%; min-height: 50px; border-bottom: 1px solid #E0E0E0; padding: 10px 10px 5px 10px; ">
			<div style="display: inline-block; float:left;">
				<div style="font-size: 28px; padding-left: 15px; font-weight: 300; height: 45px; color: #009688; line-height: 45px; vertical-align: middle; margin-right: 10px; letter-spacing: 1px">{{ session('organization_name') }}</div>
			</div>
		</div>

		<!-- Main Content -->
		<div style="min-height: 200px; padding: 10px 15px; font-size: 14px; line-height: 22px; text-align: justify; margin: 10px 0; border-bottom: 1px solid #E0E0E0">
			@if(isset($receipt->patient->first_name))
			<span style="font-size: 16px; font-weight: 500; margin-bottom: 20px; display: block;">Dear {{ isset($receipt->patient)?$receipt->patient->full_name:'' }},</span>
			@else
			<span style="font-size: 16px; font-weight: 500; margin-bottom: 20px; display: block;">Dear Sir/Madam,</span>
			@endif

            <div class="row clearfix">
                <div style="display: block; font-size: 16px;text-align:justify;">
					Greetings from <b>{{ $profile->organization_name ?? '' }}</b>.<br><br>

                    Hope this mail finds you in good health and we trust that you find our services to patient <b>{{ isset($receipt->patient)?$receipt->patient->full_name:'' }}</b> with utmost satisfaction.<br><br>

                    Kindly find the attached Receipt against your service / memo.<br><br>

                    To ensure ease of process, please quote your patient ID ({{ $receipt->patient->patient_id ?? '' }}) for all future correspondence.<br>
                </div><br>
                <i style="color: #ffc107">PS: This is an auto-generated mail. Do not reply</i><br>
            </div>

            <br>
            Regards,<br><br>
            <table>
                <tr>
                    <td>
                        <img src="{{ asset('img/provider/'.session('organization_logo')) }}" style="max-height: 110px; margin: 5px 0 0 10px" title="{{ session('orgnization_name') }}">
                    </td>
                    <td width="4%"></td>
                    <td style="vertical-align: top; font-size: 13px !important;width: 100%;">
                        <span style="font-size: 14px !important;font-weight: bold">{{ session('organization_name') }}</span><br>
                        {{ session('organization_address') }}<br>
                        {{ session('organization_city') }} - {{ session('organization_zipcode') }}<br>
                        {{ session('organization_state') }}, {{ session('organization_country') }}<br><br>
                        Phone: {{ session('phone_number') }}
                    </td>
                </tr>
            </table>
		</div>

		<!-- Footer -->
		<div style="display: block; position:relative; bottom: 0; max-width: 99%; min-height: 90px; padding: 5px 10px 0px 10px; ">
			<div style="width: 50%; display: inline-block; font-size: 12px;">
				<span>Copyright &copy; {{date('Y')}} {{ session('organization_name') }} All Rights Reserved</span><br><br>
				<span style="font-size: 12px; font-weight: 600">
					<a href="" style="color: #2899dc !important">Terms and Conditions</a> |
					<a href="" style="color: #2899dc !important">Privacy Policy</a>
				</span>
			</div>
			<div style="width: 49%; display: inline-block; text-align: right; vertical-align: top; font-size: 14px">
				<p style="margin-bottom: 10px;margin-top:0;padding-top:0">Powered by</p>
				<b><small>Smart Health Connect</small></b>
			</div>
			<br><br>
		</div>
	</div>
</body>
</html>
