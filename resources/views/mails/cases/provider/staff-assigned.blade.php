<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style>
        .border-top{
            border-top: 1px solid #333 !important;
        }
        .border-bottom{
            border-bottom: 1px solid #333 !important;
        }
        .border-right{
            border-right: 1px solid #333 !important;
        }
        .border-left{
            border-left: 1px solid #333 !important;
        }

         .no-padding, .no-padding tbody tr td, .no-padding tbody tr th{
            padding: 0 !important;
            border: 1px solid #333;
        }
        .no-border, .no-border tbody tr td, .no-border tbody tr th{
            border: 0;
        }
         .services-table, .services-table thead tr th, .services-table tfoot tr th, .services-table tfoot tr td, .services-table  tbody tr th,.services-table tbody tr td{
             padding: 1px !important;
             border: 0;
         }

         .services-table{
            margin-left: 5px;
         }

         .services-table thead tr th, .text-center{
             text-align: center !important;
         }

         .text-right{
            text-align: right !important;
         }
        .tbl-text{
            margin-top:1px !important;
            padding:2px !important;
            margin-bottom:1px !important;
        }

    </style>
</head>
<body style="font-family: 'Roboto', sans-serif !important;">
    <div style="display:block; width: 95%; margin: 0 auto; background: #fff; border-radius: 2px; box-shadow: 0 10px 20px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);">
        <!-- Header -->
        <div style="display: block; text-align: center !important; max-width: 99%; min-height: 50px; border-bottom: 1px solid #E0E0E0; padding: 10px 10px 5px 10px; ">
            <div style="display: inline-block; text-align: center !important;">
                <div style="font-size: 28px; padding-left: 15px; font-weight: 300; height: 45px; color: #009688; line-height: 45px; vertical-align: middle; margin-right: 10px; letter-spacing: 1px; text-align: center !important;">{{ session('organization_name') }}</div>
            </div>
        </div>

        <!-- Main Content -->
        <div style="min-height: 200px; padding: 10px 15px; font-size: 14px; line-height: 22px; text-align: justify; margin: 10px 0; border-bottom: 1px solid #E0E0E0">

            <div class="row clearfix" style="font-size: 16px">
                <div style="display: block; font-size: 16px;text-align:justify;">
                    <?php if($caregiver->gender == 'Male'){$gender1='His'; $gender2='He';}else{$gender1='Her'; $gender2='She';}
                    if(isset($caregiver->professional->reportingmanager->gender)){
                    	if($caregiver->professional->reportingmanager->gender == 'Male'){$managergender1='His';}else{$managergender1='Her';}
                    } ?>
                    Dear {{ $lead->patient->full_name }},<br><br>
                    Greetings from <b>{{ session('organization_name') }}</b>.<br>
                    <br><br>
                    We are pleased to inform you that <b>{{ $caregiver->first_name.' '.$caregiver->last_name }}</b> has been allocated as your caregiver. 
                    {{ $gender1 }} specialization is <b>{{ $caregiver->professional->specializations->specialization_name }}</b> with {{ $caregiver->professional->experience_level }} year(s) experience. 
                    {{ $gender1 }} contact number is {{ $caregiver->mobile_number }}.
                    {{ isset($caregiver->professional->reportingmanager->full_name)?$gender1.' manager is '.$caregiver->professional->reportingmanager->full_name.'.':'' }} 
                    {{ isset($caregiver->professional->reportingmanager->mobile_number)?$managergender1.' contact number is '.$caregiver->professional->reportingmanager->mobile_number.'.':'' }} 
                    {{ $gender2 }} speaks {{ $caregiver->professional->languages_known }}.<br><br>
                    {{ isset($caregiver->professional->reportingmanager->mobile_number)?'If you need further information, please call the manager at '.$caregiver->professional->reportingmanager->mobile_number.'.':'' }}<br>
                </div><br><br>
                Regards,<br>{{ session('organization_name') }}<br>
                <table style="border-top: 3px solid #009688 !important;">
                <tr>
                    <td>
                        <img src="{{ asset('uploads/provider/'.session('tenant_id').'/'.session('organization_logo')) }}" style="max-height: 110px; margin: 5px 0 0 10px" title="{{ session('orgnization_name') }}">
                    </td>
                    <td width="4%"></td>
                    <td style="vertical-align: top; font-size: 13px !important;width: 100%;">
                        <span style="font-size: 14px !important;font-weight: bold">{{ session('org_name') }}</span><br>
                        {{ session('organization_address') }}<br>
                        {{ session('organization_city') }} - {{ session('organization_zipcode') }}<br>
                        {{ session('organization_state') }}, {{ session('organization_country') }}<br><br>
                        Phone: {{ session('phone_number') }}
                    </td>
                </tr>
                </table>
                <i style="color: #ffc107">PS: This is an auto-generated mail. Do not reply</i><br>
            </div>
        </div>
    </div>
</body>
</html>