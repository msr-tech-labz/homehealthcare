<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style>
        .border-top{
            border-top: 1px solid #333 !important;
        }
        .border-bottom{
            border-bottom: 1px solid #333 !important;
        }
        .border-right{
            border-right: 1px solid #333 !important;
        }
        .border-left{
            border-left: 1px solid #333 !important;
        }

         .no-padding, .no-padding tbody tr td, .no-padding tbody tr th{
            padding: 0 !important;
            border: 1px solid #333;
        }
        .no-border, .no-border tbody tr td, .no-border tbody tr th{
            border: 0;
        }
         .services-table, .services-table thead tr th, .services-table tfoot tr th, .services-table tfoot tr td, .services-table  tbody tr th,.services-table tbody tr td{
             padding: 1px !important;
             border: 0;
         }

         .services-table{
            margin-left: 5px;
         }

         .services-table thead tr th, .text-center{
             text-align: center !important;
         }

         .text-right{
            text-align: right !important;
         }
        .tbl-text{
            margin-top:1px !important;
            padding:2px !important;
            margin-bottom:1px !important;
        }

    </style>
</head>
<body style="font-family: 'Roboto', sans-serif !important;">
    <div style="display:block; width: 95%; margin: 0 auto; background: #fff; border-radius: 2px; box-shadow: 0 10px 20px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);">
        <!-- Header -->
        <div style="display: block; text-align: center !important; max-width: 99%; min-height: 50px; border-bottom: 1px solid #E0E0E0; padding: 10px 10px 5px 10px; ">
            <div style="display: inline-block; text-align: center !important;">
                <div style="font-size: 28px; padding-left: 15px; font-weight: 300; height: 45px; color: #009688; line-height: 45px; vertical-align: middle; margin-right: 10px; letter-spacing: 1px; text-align: center !important;">Smart Health Connect</div>
            </div>
        </div>

        <!-- Main Content -->
        <div style="min-height: 200px; padding: 10px 15px; font-size: 14px; line-height: 22px; text-align: justify; margin: 10px 0; border-bottom: 1px solid #E0E0E0">
            <span style="font-size: 16px; font-weight: 500; margin-bottom: 20px; display: block;">Dear Sir/Madam,</span>

            <div class="row clearfix">
                <div style="display: block; font-size: 16px;text-align:justify;">
                    Greetings from <b>Smart Health Connect</b>.You have declined the following lead.<br>The lead has been removed on your portal.
                </div><br>
                <table class="tbl services-table no-padding" style="width: 100% !important">
                        <tr>
                            <th colspan="2" class="text-center border-top border-left border-right border-bottom" style="font-size:12px !important;background-color: #f99696;color: white">Enquirer Details</th>
                        </tr>
                        <tr>
                            <th>Name</th>
                            <td style="float: right;">{{isset($patient)?$patient->enquirer_name:''}}</td>
                        </tr>
                        <tr>
                            <th colspan="2" class="text-center border-top border-left border-right border-bottom" style="font-size:12px !important;background-color: #f99696;color: white">Patient History</th>
                        </tr>
                        <tr>
                            <th>Patient Name</th>
                            <td style="float: right;">{{isset($patient)?$patient->first_name.' '.$patient->last_name:''}}</td>
                        </tr>
                        <tr>
                            <th>Patient Age</th>
                            <td style="float: right;">{{isset($patient)?$patient->patient_age.' yrs':''}}</td>
                        </tr>
                        <tr>
                            <th>Patient Weight</th>
                            <td style="float: right;">{{isset($patient)?$patient->patient_weight.' kgs':''}}</td>
                        </tr>
                        <tr>
                            <th>Requirement</th>
                            <td style="float: right;">{{isset($lead)?$lead->aggregator_service:''}}</td>
                        </tr>
                        <tr>
                            <th>Medical Conditions</th>
                            <td style="float: right;">{{isset($lead)?$lead->medical_conditions:''}}</td>
                        </tr>
                        <tr>
                            <th>Medications</th>
                            <td style="float: right;">{{isset($lead)?$lead->medications:''}}</td>
                        </tr>
                        <tr>
                            <th>Surgeries/Procedures</th>
                            <td style="float: right;">{{isset($lead)?$lead->procedures:''}}</td>
                        </tr>
                        <tr>
                            <th>Location</th>
                            <td style="float: right;">{{isset($patient)?$patient->area:''}},{{isset($patient)?$patient->city:''}},{{isset($patient)?$patient->state:''}}</td>
                        </tr>
                        <tr>
                            <th colspan="2" class="text-center border-top border-left border-right border-bottom" style="font-size:12px !important;background-color: #f99696;color: white;">Specific Requirements by Customer</th>
                        </tr>
                        <tr>
                            <th>Gender Preference</th>
                            <td style="float: right;">{{isset($lead)?$lead->gender_preference:''}}</td>
                        </tr>
                        <tr>
                            <th>Language Preference</th>
                            <td style="float: right;">{{isset($lead)?$lead->language_preference:''}}</td>
                        </tr>
                </table>
                <br>
                <br>
                <i style="color: #ffc107">PS: This is an auto-generated mail. Do not reply</i><br>
            </div>

            <br>
            Regards,<br>Smart Health Connect<br>
            <table style="border-top: 3px solid #009688 !important;">
                <tr>
                    <td>
                        <img src="{{ asset('uploads/provider/d0p4M1paYXJpU3pZeEN4dXlkUHQ2QT09/d0p4M1paYXJpU3pZeEN4dXlkUHQ2QT09.jpeg') }}" style="max-height: 110px; margin: 5px 0 0 10px" title="Smart Health Connect">
                    </td>
                    <td width="4%"></td>
                    <td style="vertical-align: top; font-size: 13px !important;width: 100%;">
                        <span style="font-size: 14px !important;font-weight: bold">Smart Health Connect</span><br>
                        #21, High Profils Building<br>
                        Church Street,Bengaluru 560001<br>
                        Karnataka,India<br><br>
                        Phone: +91 (080) 30752584
                    </td>
                </tr>
            </table>
        </div>

        <!-- Footer -->
        <div style="display: block; position:relative; bottom: 0; max-width: 99%; min-height: 90px; padding: 5px 10px 0px 10px; ">
            <div style="width: 50%; display: inline-block; font-size: 12px;">
                <span>Copyright &copy; {{date('Y')}} Apnacare India Pvt. Ltd. All Rights Reserved</span><br><br>
                <span style="font-size: 12px; font-weight: 600">
                    <a href="" style="color: #2899dc !important">Terms and Conditions</a> |
                    <a href="" style="color: #2899dc !important">Privacy Policy</a>
                </span>
            </div>
            <div style="width: 49%; display: inline-block; text-align: right; vertical-align: top; font-size: 14px">
                <p style="margin-bottom: 10px;margin-top:0;padding-top:0">Powered by</p>
                <b><small>Smart Health Connect</small></b>
                {{-- <img src="https://portal.apnacare.in/img/provider/d0p4M1paYXJpU3pZeEN4dXlkUHQ2QT09.png" alt=" Apnacare India Pvt. Ltd." title="Apnacare" style="width: 60px"/> --}}
            </div>
            <br><br>
        </div>
    </div>
</body>
</html>
