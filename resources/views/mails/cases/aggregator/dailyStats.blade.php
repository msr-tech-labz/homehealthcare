<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style>
        .border-top{
            border-top: 1px solid #333 !important;
        }
        .border-bottom{
            border-bottom: 1px solid #333 !important;
        }
        .border-right{
            border-right: 1px solid #333 !important;
        }
        .border-left{
            border-left: 1px solid #333 !important;
        }
        .border{
            border-top: 1px solid #333 !important;
            border-bottom: 1px solid #333 !important;
            border-right: 1px solid #333 !important;
            border-left: 1px solid #333 !important;
        }

         .no-padding, .no-padding tbody tr td, .no-padding tbody tr th{
            padding: 0 !important;
            border: 1px solid #333;
        }
        .no-border, .no-border tbody tr td, .no-border tbody tr th{
            border: 0;
        }
         .services-table, .services-table thead tr th, .services-table tfoot tr th, .services-table tfoot tr td, .services-table  tbody tr th,.services-table tbody tr td{
             padding: 1px !important;
             border: 0;
         }

         .services-table{
            margin-left: 5px;
         }

         .services-table thead tr th, .text-center{
             text-align: center !important;
         }

         .text-right{
            text-align: right !important;
         }
        .tbl-text{
            margin-top:1px !important;
            padding:2px !important;
            margin-bottom:1px !important;
        }

    </style>
</head>
<body style="font-family: 'Roboto', sans-serif !important;">
    <div style="width:100%;display: inline-block;">
        <table class="tbl services-table no-padding border" style="width:49%;margin-bottom:30px !important;font-size: 14px !important;float: left;">
            <thead>
                <tr class="border">
                    <th colspan="2" class="text-center border">Aggregator</th>
                </tr>
                <tr class="border">
                    <th class="border">Total Leads</th>
                    <td class="border">{{ $data['totalLeads'] }}</td>
                </tr>
                <tr class="border">
                    <th class="border">Total Leads Pending with Apnacare</th>
                    <td class="border">{{ $data['aggPending'] }}</td>
                </tr>
                <tr class="border">
                    <th class="border">Total Leads Converted by Apnacare</th>
                    <td class="border">{{ $data['aggConverted'] }}</td>
                </tr>
                <tr class="border">
                    <th class="border">Total Leads Lost by Apnacare</th>
                    <td class="border">{{ $data['aggDropped'] }}</td>
                </tr>
                <tr class="border">
                    <th class="border">Convertion Ratio</th>
                    <td class="border">{{ $data['aggConverted'] + $data['convertedProvider'] }}:{{ $data['totalLeads'] }}</td>
                </tr>
            </thead>
        </table>
        <table class="tbl services-table no-padding border" style="width:49%;margin-bottom:30px !important;font-size: 14px !important;float: right;">
            <thead>
                <tr class="border">
                    <th colspan="2" class="text-center border">Provider</th>
                </tr>
                <tr class="border">
                    <th class="border">Total Leads Transferred to Providers</th>
                    <td class="border">{{ $data['transProvider'] }}</td>
                </tr>
                <tr class="border">
                    <th class="border">Total Leads Pending with Providers</th>
                    <td class="border">{{ $data['pendingProvider'] }}</td>
                </tr>
                <tr class="border">
                    <th class="border">Total Leads Converted by Providers</th>
                    <td class="border">{{ $data['convertedProvider'] }}</td>
                </tr>
                <tr class="border">
                    <th class="border">Acceptance/Declination Ratio</th>
                    <td class="border">{{ $data['acceptanceRatioProvider'] }}</td>
                </tr>
            </thead>
        </table>
    </div>
    <div style="width:100%;display: inline-block;">
        <table class="tbl services-table no-padding border" style="width:49%;margin-bottom:30px !important;font-size: 14px !important;float: left;">
            <thead>
                <tr class="border">
                    <th colspan="2" class="text-center border">Freelancers</th>
                </tr>
                <tr class="border">
                    <th class="border">Total Leads Transferred to Freelancers</th>
                    <td class="border">0</td>
                </tr>
                <tr class="border">
                    <th class="border">Total Leads Pending with Freelancers</th>
                    <td class="border">0</td>
                </tr>
                <tr class="border">
                    <th class="border">Total Leads Converted by Freelancers</th>
                    <td class="border">0</td>
                </tr>
                <tr class="border">
                    <th class="border">Acceptance Ratio</th>
                    <td class="border">0/0</td>
                </tr>
            </thead>
        </table>
        <table class="tbl services-table no-padding border" style="width:49%;margin-bottom:30px !important;font-size: 14px !important;float: right;">
            <thead>
                <tr class="border">
                    <th colspan="2" class="text-center border">Exotel</th>
                </tr>
                <tr class="border">
                    <th class="border">Total Leads from Exotel</th>
                    <td class="border">{{ $data['leadExotelTotal'] }}</td>
                </tr>
                <tr class="border">
                    <th class="border">Leads Attended</th>
                    <td class="border">{{ $data['leadExotelAttended'] }}</td>
                </tr>
                <tr class="border">
                    <th class="border">Leads Unattended</th>
                    <td class="border">{{ $data['leadExotelUnattended'] }}</td>
                </tr>
                <tr class="border">
                    <th class="border">Leads Pending</th>
                    <td class="border">{{ $data['leadExotelPending'] }}</td>
                </tr>
                <tr class="border">
                    <th class="border">Leads Converted</th>
                    <td class="border">{{ $data['leadExotelConverted'] }}</td>
                </tr>
            </thead>
        </table>
    </div>
    <div style="width:100%;display: inline-block;">
        <table class="tbl services-table no-padding border" style="width:49%;margin-bottom:30px !important;font-size: 14px !important;float: left;">
            <thead>
                <tr class="border">
                    <th colspan="2" class="text-center border">Website</th>
                </tr>
                <tr class="border">
                    <th class="border">Total Leads from Website</th>
                    <td class="border">{{ $data['leadWebsiteTotal'] }}</td>
                </tr>
                <tr class="border">
                    <th class="border">Leads Attended</th>
                    <td class="border">{{ $data['leadWebsiteAttended'] }}</td>
                </tr>
                <tr class="border">
                    <th class="border">Leads Unattended</th>
                    <td class="border">{{ $data['leadWebsiteUnattended'] }}</td>
                </tr>
                <tr class="border">
                    <th class="border">Leads Pending</th>
                    <td class="border">{{ $data['leadWebsitePending'] }}</td>
                </tr>
                <tr class="border">
                    <th class="border">Leads Converted</th>
                    <td class="border">{{ $data['leadWebsiteConverted'] }}</td>
                </tr>
            </thead>
        </table>
        <table class="tbl services-table no-padding border" style="width:49%;margin-bottom:30px !important;font-size: 14px !important;float: right;">
            <thead>
                <tr class="border">
                    <th colspan="4" class="text-center border">MCM</th>
                </tr>
                <tr class="border">
                    <th class="border">Status</th>
                    <th class="border">Jaya Kumari</th>
                    <th class="border">Sheela</th>
                    <th class="border">Bhavya</th>
                </tr>
                <tr class="border">
                    <th class="border">Pending</th>
                    <th class="border">{{ $data['jayaPending'] }}</th>
                    <th class="border">{{ $data['sheelaPending'] }}</th>
                    <th class="border">{{ $data['bhavyaPending'] }}</th>
                </tr>
                <tr class="border">
                    <th class="border">Converted</th>
                    <th class="border">{{ $data['jayaConverted'] }}</th>
                    <th class="border">{{ $data['sheelaConverted'] }}</th>
                    <th class="border">{{ $data['bhavyaConverted'] }}</th>
                </tr>
                <tr class="border">
                    <th class="border">Case Loss</th>
                    <th class="border">{{ $data['jayaCaseLoss'] }}</th>
                    <th class="border">{{ $data['sheelaCaseLoss'] }}</th>
                    <th class="border">{{ $data['bhavyaCaseLoss'] }}</th>
                </tr>
            </thead>
        </table>
    </div>
</body>
</html>
