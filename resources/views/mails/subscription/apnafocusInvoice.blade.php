<!DOCTYPE html>
<html>

<head>
    <style>
    html,body
    {
        height: 100%;
        margin: 0px;
    }
    @page
    {
        size: auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }

    html, body{
        font-size: 13px;
        font-family: 'Roboto', Arial, Tahoma, sans-serif !important;
    }
    h1,h2,h3,h4,h5,h6{
        font-family: 'Roboto', Arial, Tahoma, sans-serif !important;
    }
    .border-grey{
        border-bottom: 0.1em solid #ccc !important;
    }
    .border-bottom{
        border-bottom: 2px solid #00b0e4 !important;
    }
    </style>
</head>

<body class="theme-blue" style="background: transparent !important;">
    <div style="position: absolute;top: 10px;">
        <div class="inv-foot-bar" style="margin-top:1%;width:100% !important;height:10px !important; background: #00b0e4 !important;text-align:center !important;line-height: 30px;font-size: 14px">
        </div>
    </div>
    <div style="border: 0px solid black;background: white;padding: 10px 30px !important;">
        <br>
        <div style="width:auto;display:inline-block;padding: 5px;margin-left:5px;font-size:13px;vertical-align:middle !important">
            <h4 style="display:inline-block;font-weight:500;font-size:25px;color:#00b0e4 !important;border-bottom:1px solid #00b0e4;margin-right:0 !important;padding-right: 20px !important">Smart Health Connect RECEIPT FOR - {{ date("F Y", strtotime($sip->invoiceGenerated->created_at)) }}</h4>
        </div>
        <div style="width:auto;float:right;display:inline-block;margin-right:5px;vertical-align:top;padding-top:0px">
            <center><img src="https://portal.apnacare.in/img/provider/d0p4M1paYXJpU3pZeEN4dXlkUHQ2QT09.png" class="org-img" style="width: auto;max-height:70px;clear:both;border-radius: 5px;box-shadow: 3px 3px;background: #fff;"></center>
        </div>
        <br>
    </div>
    <div style="font-size:15px !important;"><br><br>
        <div>
            <div style="font-weight:600;width: 25%;display: inline-block;">Invoice Date :</div>
            <div class="border-grey" style="width: 24%;display: inline-block;">{{ \Carbon\Carbon::parse($sip->invoiceGenerated->created_at)->format('d-m-Y') }}</div>

            <div style="font-weight:600;width: 25%;display: inline-block;">Payment Date :</div>
            <div class="border-grey" style="width: 24%;display: inline-block;">{{ \Carbon\Carbon::parse($sip->transaction_date)->format('d-m-Y') }}</div>
        </div>
        <br>
        <div>
            <div style="font-weight:600;width: 15%;display: inline-block;">Organization :</div>
            <div class="border-grey" style="width: 34%;display: inline-block;">{{ $profile->organization_name }}</div>

            <div style="font-weight:600;width: 15%;display: inline-block;">Name :</div>
            <div class="border-grey" style="width: 34%;display: inline-block;">{{ $profile->contact_person }}</div>
        </div>
        <br><br>
        <div>
            <div style="font-weight:600;width: 17%;display: inline-block;">Mode of payment :</div>
            <div class="border-grey" style="width: 32%;display: inline-block;">Online</div>

            <div style="font-weight:600;width: 17%;display: inline-block;">Payment Ref No. :</div>
            <div class="border-grey" style="width: 32%;display: inline-block;">{{ $sip->transaction_id }}</div>
        </div>
        <br><br>
        <div>
            <div style="font-weight:600;width: 17%;display: inline-block;">Payment Against :</div>
            <div class="border-grey" style="width: 32%;display: inline-block;">{{ date("F Y", strtotime($sip->invoiceGenerated->created_at)) }}</div>

            <div style="font-weight:600;width: 15%;display: inline-block;">Amount in Rs. :</div>
            <div class="border-grey" style="width: 34%;display: inline-block;">{{ $sip->total_amount }}</div>
        </div>
        <br>
        <div>
            <div style="font-weight:600;width: 17%;display: inline-block;">Amount in words :</div>
            <div class="border-grey" style="width: 32%;display: inline-block;">
                {? $f = new \NumberFormatter("en", \NumberFormatter::SPELLOUT); ?}
                {{ ucwords($f->format((round($sip->total_amount)))).' Rupees Only' }}
            </div>

            <div style="font-weight:600;width: 17%;display: inline-block;">Description of service :</div>
            <div class="border-grey" style="width: 32%;display: inline-block;">
                Payment for the Usage of Smart Health Connect Web Portal
            </div>
        </div>
    </div>

    <div style="position: absolute;bottom: 30px;">
        <div class="inv-foot-add" style="width:100% !important;color: #00b0e4 !important;text-align:center !important;line-height: 30px;font-size: 14px;margin-top:10px !important;">
            APNACARE INDIA PRIVATE LIMITED<br>
            <div style="color: #333 !important;font-size:12px !important;line-height:18px !important">
                13th Cross,20th Main,Sahakar Nagar,Bengaluru,Karnataka,India,560016<br>
                <b>Ph:</b> 8688279500  <b>Website:</b> https://www.apnacare.in
            </div>
        </div>

        <div class="inv-foot-bar" style="margin-top:1%;width:100% !important;height:10px !important; background: #00b0e4 !important;text-align:center !important;line-height: 30px;font-size: 14px">
        </div>

    </div>
</body>

</html>
