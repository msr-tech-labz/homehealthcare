@extends('layouts.channelpartner-layout')

@section('page_title','Dashboard - ')

@section('active_home', 'active')

@section('plugin.styles')
<link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />
@endsection

@section('content')
    <style>
        .text-red td, .text-red{
            color: #ff0000 !important;
        }
    </style>
    <div class="block-header">
        <h2>REFERRED USERS
                <span class="label bg-deep-purple" style="float: right;font-size: 16px;border-radius: 20px;">REFERRAL CODE : {{ $referralCode->referral_code }}</span>
                <span class="label bg-deep-purple" style="float: right;font-size: 16px;border-radius: 20px;margin-right: 15px;">CREDITS : {{ isset($creditBalance)?$creditBalance->credits:'0' }}</span>
        </h2>
    </div>
    
    <div class="row clearfix">
        <div class="body">
            <table class="table table-bordered table-striped table-hover table-condensed js-basic-example dataTable reflist">
                <thead>
                    <tr>
                        <th>Subscribed on</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Contact Number</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $p)
                        <tr>
                            <td>{{ \Carbon\Carbon::parse($p->created_at)->format('d-m-Y') }}</td>
                            <td>{{ $p->first_name.' '.$p->last_name }}</td>
                            <td>{{ $p->email }}</td>
                            <td>{{ $p->mobile_number }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('plugin.scripts')
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            @if(isset($data))
                $('.reflist').DataTable();
            @endif
        } );
    </script>
@endsection