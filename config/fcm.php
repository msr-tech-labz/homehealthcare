<?php

return [
	'driver'      => env('FCM_PROTOCOL', 'http'),
	'log_enabled' => true,

	'http' => [
		'server_key'       => env('FCM_SERVER_KEY', 'AAAAxUynUIs:APA91bHAOY9m7zKALZHAoG2VsJb23EyaUY7BcfKFLQDpR3npXpd1lpPOw2b8mT-41jHZ5g2U3yNQ3Ago3ZAonZAcV7kLaA8fBijbEBqx87yJLpBpI7iuozGxUzhBgphrFsZTJZHTkp3NQKcQtEVc8f0SlFgyjRPBZw'),
		'sender_id'        => env('FCM_SENDER_ID', '847394590859'),
		'server_send_url'  => 'https://fcm.googleapis.com/fcm/send',
		'server_group_url' => 'https://android.googleapis.com/gcm/notification',
		'timeout'          => 30.0, // in second
	]
];
