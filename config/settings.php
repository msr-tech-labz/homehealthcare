<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Database Prefix
    |--------------------------------------------------------------------------
    |
    | This value is the database prefix.
    */

    'db_name' => env('DB_DATABASE','apnacare_hhms'),

    'db_prefix' => env('DB_PREFIX','apnaca'),
];
