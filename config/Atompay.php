<?php

return [

    'gateway' => 'AtomPayment',

    'testMode'  => false,                                // True for Testing the Gateway [For production false]

    'Atom' => [
        'ATOM_LOGIN'        => env('ATOM_LOGIN', '160'),
        'ATOM_PASSWORD'     => env('ATOM_PASSWORD', 'Test@123'),
        'ATOM_PORT'         => env('ATOM_PORT', '443'),
        'ATOM_PRO_ID'       => env('ATOM_PRO_ID', 'NSE'),
        'ATOM_CLIENT_CODE'  => base64_encode(env('ATOM_CLIENT_CODE', '007')),

        // Should be route address for url() function
        'successUrl' => env('ATOM_SUCCESS_URL', 'payment/transaction/response'),

    ],

];
