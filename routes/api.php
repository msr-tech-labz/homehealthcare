<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
    Route::get('/','APIController@index');

    Route::group(['prefix' => 'v2', 'middleware' => 'checkapisession'], function(){
        Route::get('/', 'APIController@index');

        Route::get('checkVersion','API\V2Controller@checkVersion');

        Route::post('auth/login','Auth\APIAuthController@authenticate');
        /* Token Refresh API Calls */
        Route::any('auth/token','Auth\APIAuthController@token');

        /* Customer Signup */
        Route::post('auth/signup','Auth\APIAuthController@customerSignup');
        Route::post('auth/customerLogin','Auth\APIAuthController@customerLogin');

        /* FCM Token update */
        Route::post('fcm/updateRegistrationToken','API\V2Controller@updateRegistrationToken');
        Route::post('fcm/sendEmergencyAlert','API\V2Controller@sendEmergencyAlert');
        Route::post('fcm/updateRegistrationTokenFamily','API\V2Controller@updateRegistrationTokenFamily');

        /*---------------------------------------*/
        /*           API Calls - NEW             */
        /*---------------------------------------*/

        /* Leads API Calls */
        Route::any('leads/all','API\V2Controller@getLeads');
        Route::post('lead/create','API\V2Controller@createLead');
        Route::post('assessments','API\V2Controller@getAssessments');
        Route::post('lead/updateAssessmentTimings','API\V2Controller@updateAssessmentTimings');
        Route::post('lead/assessment/add','API\V2Controller@addAssessment');
        Route::post('lead/followup/add','API\V2Controller@addFollowup');
        Route::post('lead/assessment/signature/add','API\V2Controller@addAssessmentSignature');
        Route::post('lead/assessment/delete','API\V2Controller@deleteAssessment');
        Route::post('lead/followup/delete','API\V2Controller@deleteFollowUp');
        Route::post('lead/incident/save','API\V2Controller@saveIncident');
        Route::post('lead/incident/delete','API\V2Controller@deleteIncident');
        Route::post('lead/schedule/updateStatus','API\V2Controller@updateScheduleStatus');
        Route::post('lead/worklog/save','API\V2Controller@saveWorklog');
        Route::any('leads/invoices','API\V2Controller@getLeadInvoices');

        /* Leaves API Calls */
        Route::any('leaves/all','API\V2Controller@getLeaves');
        Route::any('leaves/add','API\V2Controller@addLeave');
        Route::any('leaves/updateRequest','API\V2Controller@updateLeaveRequest');

        /* Profile API Calls */
        Route::any('profile','API\V2Controller@getProfile');
	    Route::any('profile/update','API\V2Controller@updateProfile');

        /* Caregivers API Calls */
        Route::any('caregivers/all','API\V2Controller@getCaregivers');
        Route::any('caregiver/attendance','API\V2Controller@markAttendance');
        Route::any('caregiver/updateStatus','API\V2Controller@updateCaregiverStatus');
        Route::post('caregiver/addPatientWellnessFeedback','API\V2Controller@addPatientWellnessFeedback');
        Route::any('caregiver/getAttendance','API\V2Controller@getAttendance');
        Route::any('caregiver/markDailyAttendance','API\V2Controller@markDailyAttendance');

        /*---------------------------------------*/
        /*          API Calls - NEW END          */
        /*---------------------------------------*/

        /* Cases API Calls */
        Route::any('cases/all','API\V2Controller@getCases');
        Route::post('case/getDetailsById','API\V2Controller@getCases');
    	Route::post('case/schedule/updateCaregiver','API\V2Controller@updateScheduleCaregiver');

        Route::get('tracking/locationUpdate','API\V2Controller@locationUpdate');

        /* Customer API Calls */
        Route::post('customer/profile','API\V2Controller@getCustomerProfile');
        Route::get('patients/all','API\V2Controller@getPatients');
        Route::post('patient/update','API\V2Controller@updatePatient');
        Route::post('request/new','API\V2Controller@newCustomerRequest');
        Route::post('request/get','API\V2Controller@getCustomerRequest');
        Route::post('caregivers/get','API\V2Controller@getPatientCaregivers');
        Route::post('caregiver/documents','API\V2Controller@getAllDocuments');
        Route::post('caregiver/getPatientWellnessFeedback','API\V2Controller@getPatientWellnessFeedback');
        Route::post('worklog/feedback','API\V2Controller@worklogFeedback');
        Route::post('customer/bills','API\V2Controller@getCustomerBills');
        Route::get('customer/invoice','API\V2Controller@getInvoice');
        Route::post('customer/link','API\V2Controller@customerLink');
        Route::post('customer/passwordReset','Auth\APIAuthController@customerPasswordReset');
        Route::post('customer/profileUpload','API\V2Controller@customerProfileUpload');
    });

    Route::group(['prefix' => 'tally', 'middleware' => 'checkapisession'], function(){
        Route::get('patient/all','API\V2Controller@getAllPatients');
        Route::any('patient/patientId','API\V2Controller@getPatientByPatientId');
    });

    Route::group(['prefix' => 'aajicare', 'middleware' => 'checkapisession'], function(){
        Route::post('lead/save','API\V2Controller@saveLeadFromAajicareWebsite');
    });