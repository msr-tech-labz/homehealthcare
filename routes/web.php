<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
    Route::get('unauthorized','HomeController@unauthorized');
    
    Route::get('orghash', function(){
        dd(\Helper::encryptor('encrypt','shc_branch266'));
    });

    // Route::get('/',['as' => 'home', 'uses' => 'HomeController@login']);
    Route::get('regform/{id?}', 'HomeController@patientRegForm');
    Route::any('/regformSave',['as' => 'signaturepad.upload', 'uses' => 'HomeController@patientRegFormSave']);
    Route::any('/terms&conditions/{id?}', 'HomeController@patientTermsConditions');
    Route::any('/famila/{id}/{db?}', 'HomeController@familaLink');
    Route::any('/termsandcond',['as' => 'termsandcond.save', 'uses' => 'HomeController@patientTermsConditionsSave']);
    Route::get('/', function(){
        if(!session('uid') || !session('utype')){
            session()->flush();
            return redirect('/login');
        }
        return redirect('/dashboard');
    });

    Route::get('refresh-csrf', 'HomeController@refreshToken');

    Route::get('mobile', function () {
        return view('mobile');
    });

    Route::get('/renew' ,function(){
        return view('subscription.renew');
    });
    Route::post('renewPage',['as' => 'subscription.renewPaymentPage', 'uses' => 'SubscriptionController@renewPaymentPage']);
    Route::post('/payment/process',['as' => 'subscriptionRenew.process-payment', 'uses' => 'SubscriptionController@processPayment']);


    /*
    |--------------------------------------------------------------------------
    | Corporate Office Routes
    |--------------------------------------------------------------------------
    */
    Route::group(['prefix' => 'corporate','namespace' => 'Corporates'], function(){
        Route::get('/',['uses' => 'CorpAuthentication@login']);

        Route::get('login',['as' => 'corporate.login', 'uses' => 'CorpAuthentication@login']);
        Route::post('authenticate',['as' => 'corporate.authenticate', 'uses' => 'CorpAuthentication@authenticate']);
        Route::match(['get','post'],'logout',['as' => 'corporate.logout', 'uses' => 'CorpAuthentication@logout']);

        Route::group(['middleware' => 'corpuser'], function(){
            Route::get('dashboard', ['as' => 'corporate.dashboard', 'uses' => 'DashboardController@index']);

            Route::group(['prefix' => 'administration'], function(){
                Route::get('/', ['as' => 'corporate.settings','uses' => 'DashboardController@administration']);
                Route::get('viewProfile',['as' => 'corporate.profile.view', 'uses' => 'ProfileController@index']);
                Route::post('updateProfile',['as' => 'corporate.profile.update', 'uses' => 'ProfileController@update']);
            });

            Route::group(['prefix' => 'team'], function(){
                Route::get('/',['as' => 'corporate.team.index', 'uses' => 'TeamController@index']);
                Route::get('viewUsers',['as' => 'corporate.users.view', 'uses' => 'TeamController@viewUsers']);
                Route::post('addUsers',['as' => 'corporate.users.add', 'uses' => 'TeamController@addUsers']);
                Route::post('updateUsers',['as' => 'corporate.users.update', 'uses' => 'TeamController@updateUsers']);
                Route::any('employee',['as' => 'corporate.employee', 'uses' => 'TeamController@employee']);
                Route::get('createEmployee/{id?}',['as' => 'corporate.createEmployee', 'uses' => 'TeamController@createEmployee']);
                Route::post('storeEmployee',['as' => 'corporate.storeEmployee', 'uses' => 'TeamController@storeEmployee']);
                Route::get('editEmployee/{id}/{br?}',['as' => 'corporate.editEmployee', 'uses' => 'TeamController@editEmployee']);
                Route::any('updateEmployee/{id?}',['as' => 'corporate.updateEmployee', 'uses' => 'TeamController@updateEmployee']);
                Route::post('checkemail',['as' => 'corporate.employee.checkemail', 'uses' => 'TeamController@checkemail']);
                Route::post('checkEmpID',['as' => 'corporate.employee.checkEmpID', 'uses' => 'TeamController@checkEmpID']);
                Route::any('verifyDocuments',['as' => 'corporate.document-verify', 'uses' => 'TeamController@apply_for_verification']);
                Route::any('updateDocuments',['as' => 'corporate.update-documents', 'uses' => 'TeamController@updateDocuments']);
                Route::post('documentRemove',['as' => 'corporate.document-remove', 'uses' => 'TeamController@documentRemove']);
                Route::post('updateStatus',['as' => 'corporate.employee.update-status', 'uses' => 'TeamController@updateStatus']);
            });

            Route::group(['prefix' => 'leads'], function(){
                Route::any('/',['as' => 'corporate.lead.index', 'uses' => 'LeadsController@index']);
                Route::get('createLead/{id?}',['as' => 'corporate.lead.create', 'uses' => 'LeadsController@createLead']);
                Route::post('storeLead',['as' => 'corporate.lead.save', 'uses' => 'LeadsController@storeLead']);
                Route::get('viewLead/{id}/{br?}',['as' => 'corporate.lead.view', 'uses' => 'LeadsController@viewLead']);
                Route::post('searchPatient',['as' => 'corporate.lead.search-patient', 'uses' => 'LeadsController@searchPatient']);
                Route::post('lookUpPatientNumber',['as' => 'corporate.patient.checknumber', 'uses' => 'LeadsController@lookUpPatientNumber']);
                Route::post('checkemailLeads',['as' => 'corporate.leads.checkemail', 'uses' => 'LeadsController@checkemail']);
                Route::post('patient/uploadPicture',['as' => 'corporate.patient.upload-picture', 'uses' => 'LeadsController@uploadPicture']);
                Route::post('updateDisposition',['as' => 'corporate.lead.update-disposition', 'uses' => 'LeadsController@updateDisposition']);
                Route::post('savePatient',['as' => 'corporate.patient.save-patient', 'uses' => 'LeadsController@savePatient']);
                Route::get('prevservicerecords/{id}/{br?}',['as' => 'corporate.lead.prevservicerecords', 'uses' => 'LeadsController@prevServiceRecords']);
                
                Route::get('addemailaddress/{id}/{br?}',['as' => 'corporate.patient.addemailaddress', 'uses' => 'LeadsController@addEmailAddress']);
                Route::post('extraAddressStore',['as' => 'corporate.patient.extraAddressStore', 'uses' => 'LeadsController@extraAddressStore']);
                Route::post('extraAddressUpdate',['as' => 'corporate.patient.extraAddressUpdate', 'uses' => 'LeadsController@extraAddressUpdate']);
                Route::post('extraAddressUpdateStatus',['as' => 'corporate.patient.update-status-extra-addr', 'uses' => 'LeadsController@extraAddressUpdateStatus']);                
                Route::post('extraEmailStore',['as' => 'corporate.patient.extraEmailStore', 'uses' => 'LeadsController@extraEmailStore']);
                Route::post('extraEmailUpdate',['as' => 'corporate.patient.extraEmailUpdate', 'uses' => 'LeadsController@extraEmailUpdate']);
                Route::post('extraEmailUpdateStatus',['as' => 'corporate.patient.update-status-extra-email', 'uses' => 'LeadsController@extraEmailUpdateStatus']);
                    });

            Route::group(['prefix' => 'reports'], function(){
              Route::get('viewReports',['as' => 'corporate.reports', 'uses' => 'ReportsController@index']);
            });
       });
    });


    /*
    |--------------------------------------------------------------------------
    | Central Admin Routes
    |--------------------------------------------------------------------------
    */
    Route::group(['prefix' => 'admin','namespace' => 'Console'], function(){
        Route::get('/',['uses' => 'ConsoleAuthentication@login']);

       Route::get('login',['as' => 'console.login', 'uses' => 'ConsoleAuthentication@login']);
       Route::post('authenticate',['as' => 'console.authenticate', 'uses' => 'ConsoleAuthentication@authenticate']);
       Route::match(['get','post'],'logout',['as' => 'console.logout', 'uses' => 'ConsoleAuthentication@logout']);

       Route::group(['middleware' => 'consoleadmin'], function(){
           Route::get('dashboard', ['as' => 'console.dashboard', 'uses' => 'DashboardController@index']);

           /*
           |--------------------------------------------------------------------------
           | SaaS Subscribers(Corporates and Branch) Routes
           |--------------------------------------------------------------------------
           */
           Route::group(['prefix' => 'subscribers'], function(){
               //New Routes
               Route::get('/',['as' => 'console.corporates', 'uses' => 'SubscriberController@corporateIndex']);
               Route::get('view/{id?}',['as' => 'console.corporate.view', 'uses' => 'SubscriberController@viewCorporate']);
               Route::post('updateBranchStatus',['as' => 'console.update-branch-status', 'uses' => 'SubscriberController@updateBranchStatus']);
               Route::post('updateCorporate',['as' => 'console.subscribers.updateCorporate', 'uses' => 'SubscriberController@updateCorporate']);
               Route::get('viewCorporateForm',['as' => 'console.subscribers.viewCorporateForm', 'uses' => 'SubscriberController@viewCorporateForm']);
               Route::post('checkCorpID',['as' => 'console.subscribers.check-corp-id', 'uses' => 'SubscriberController@checkCorpID']);
               Route::post('createCorporate',['as' => 'console.subscribers.createCorporate', 'uses' => 'SubscriberController@createCorporate']);
               Route::post('branchUpdate',['as' => 'console.subscribers.branchUpdate', 'uses' => 'SubscriberController@branchUpdate']);
               Route::get('viewBranchForm/{id?}',['as' => 'console.subscribers.viewBranchForm', 'uses' => 'SubscriberController@viewBranchForm']);
               Route::post('checkBranchID',['as' => 'console.corporate.checkBranchId', 'uses' => 'SubscriberController@checkBranchID']);
               Route::post('createBranch',['as' => 'console.subscribers.createBranch', 'uses' => 'SubscriberController@createBranch']);
               Route::post('getScheduledStaffCount',['as' => 'console.subscribers.getScheduledStaffCount','uses' => 'SubscriberController@getScheduledStaffCount']);
               Route::post('addInvoice',['as' => 'console.subscribers.addInvoice','uses' => 'SubscriberController@addInvoice']);
               Route::post('updateInvoice',['as' => 'console.subscribers.updateInvoice','uses' => 'SubscriberController@updateInvoice']);
           });

           /*
           |--------------------------------------------------------------------------
           | Billing Routes
           |--------------------------------------------------------------------------
           */
           Route::group(['prefix' => 'billing'], function(){
               Route::any('/',['as' => 'console.billing', 'uses' => 'SubscriberController@viewBillingPage']);
           });

           /*
           |--------------------------------------------------------------------------
           | Active Users Routes
           |--------------------------------------------------------------------------
           */
           Route::group(['prefix' => 'active_users'], function(){
               Route::any('/',['as' => 'console.active_users', 'uses' => 'SubscriberController@viewActiveUsers']);
           });

           /*
           |--------------------------------------------------------------------------
           | Reports Routes
           |--------------------------------------------------------------------------
           */
           Route::group(['prefix' => 'reports'], function(){
               Route::any('/',['as' => 'console.reports', 'uses' => 'SubscriberController@viewProviderCreditPayments']);
           });

           /*
           |--------------------------------------------------------------------------
           | Console Utilities Routes
           |--------------------------------------------------------------------------
           */
           Route::group(['prefix' => 'utilities'], function(){
               Route::get('administration',['as' => 'console.utilities.administration', 'uses' => 'SubscriberController@administrationIndex']);
               Route::get('sqlExecutor',['as' => 'console.utilities.sqlExecutor', 'uses' => 'SubscriberController@administrationSqlExecutor']);
               Route::get('activeUsers',['as' => 'console.utilities.activeusers', 'uses' => 'SubscriberController@activeUSersList']);
               Route::any('activeUsersStatus',['as' => 'console.utilities.activeusersstatus', 'uses' => 'SubscriberController@activeUsersListStatus']);
               Route::post('runGlobalSqlQuery',['as' => 'console.utilities.runGlobalSqlQuery', 'uses' => 'SubscriberController@administrationRunGlobalSqlQuery']);
           });
       });
    });


    /*
    |--------------------------------------------------------------------------
    | Channel Partner Routes
    |--------------------------------------------------------------------------
    */
    Route::group(['prefix' => 'channelpartner','namespace' => 'ChannelPartner'], function(){
       Route::get('/',['uses' => 'ChannelPartnerAuthentication@login']);

       Route::get('login',['as' => 'channelpartner.login', 'uses' => 'ChannelPartnerAuthentication@login']);
       Route::post('authenticate',['as' => 'channelpartner.authenticate', 'uses' => 'ChannelPartnerAuthentication@authenticate']);
       Route::match(['get','post'],'logout',['as' => 'channelpartner.logout', 'uses' => 'ChannelPartnerAuthentication@logout']);

       Route::group(['middleware' => 'channeladmin'], function(){
           Route::get('dashboard', ['as' => 'channelpartner.dashboard', 'uses' => 'ChannelPartnerController@index']);
       });
    });


    /*
    |--------------------------------------------------------------------------
    | Subscriber Routes
    |--------------------------------------------------------------------------
    */
    Auth::routes();

    Route::get('flush', 'HomeController@flush');

    Route::group(['middleware' => 'checksession'], function () {
        Route::any('createid','LeadsController@createScheduleID');
        Route::get('notify',['uses' => 'PusherController@sendNotificationPendingLeads']);
        Route::any('test','HomeController@importSchedulesOneLife');
        Route::any('test1','HomeController@test1');

        Route::get('dashboard',['as' => 'dashboard', 'uses' => 'HomeController@index']);

        Route::get('searchindex',['uses' => 'HomeController@indexSearch']);
        Route::post('search',['as' => 'search','uses' => 'HomeController@search']);

        Route::get('profile',['as' => 'profile', 'uses' => 'HomeController@profile']);
        Route::post('saveProfile',['as' => 'profile.save', 'uses' => 'HomeController@saveProfile']);

        /*
        |--------------------------------------------------------------------------
        | AJAX Routes
        |--------------------------------------------------------------------------
        */
        Route::group(['prefix' => 'ajax'], function(){
            Route::any('leadcurve',['as' => 'ajax.getleadcurve','uses'=>'AJAXController@getLeadsCurveData']);
            Route::any('resourcecurve',['as' => 'ajax.getresourcecurve','uses'=>'AJAXController@getResourceCurveData']);
            Route::post('filter-caregiver',['as' => 'ajax.caregiver.filter', 'uses' => 'AJAXController@filterCaregiver']);
            Route::any('filter-provider',['as' => 'ajax.provider.filter', 'uses' => 'AJAXController@filterProvider']);
            Route::any('searchPatientCases',['as' => 'ajax.search-patient-cases', 'uses' => 'AJAXController@searchPatientCases']);
            Route::any('markLeadAsChecked',['as' => 'ajax.mark-lead-as-checked', 'uses' => 'AJAXController@markLeadAsChecked']);
            Route::any('saveLabTest',['as' => 'ajax.save-caselabTest', 'uses' => 'AJAXController@saveLabTest']);
            Route::any('getLabTestRateCard',['as' => 'ajax.get-ratecardLabTest', 'uses' => 'AJAXController@ratecardLabTest']);
            Route::any('verifyDocuments',['as' => 'ajax.document-verify', 'uses' => 'AJAXController@apply_for_verification']);
            Route::post('getWorklogByDate',['as' => 'ajax.get-worklog-by-date', 'uses' => 'AJAXController@getWorklogByDate']);
            Route::get('readNotifications',['as' => 'ajax.readNotifications', 'uses' => 'AJAXController@readNotifications']);
            Route::get('clearNotifications',['as' => 'ajax.clearNotifications', 'uses' => 'AJAXController@clearNotifications']);
            Route::post('getSchedulesByStatus',['as' => 'ajax.get-schedules-by-status', 'uses' => 'AJAXController@getSchedulesByStatus']);
            //Dashboard Ajax
            Route::post('getDeploymentsByDate',['as' => 'ajax.get-deployments-by-date', 'uses' => 'AJAXController@getDeploymentsByDate']);
            Route::post('getReplacementsByDate',['as' => 'ajax.get-replacements-by-date', 'uses' => 'AJAXController@getReplacementsByDate']);
            Route::post('getFollowUpsByDate',['as' => 'ajax.get-follow-ups-by-date', 'uses' => 'AJAXController@getFollowUpsByDate']);
            Route::post('getServiceExpiryByDate',['as' => 'ajax.get-service-expiry-by-date', 'uses' => 'AJAXController@getServiceExpiryByDate']);

        });

        /*
        |--------------------------------------------------------------------------
        | HR Routes
        |--------------------------------------------------------------------------
        */
        Route::group(['prefix' => 'hr'], function(){
            Route::get('/',['as' => 'hr.index', 'uses' => 'HRController@index']);

            /* Employee Master Routes */
            Route::group(['prefix' => 'employee'], function(){
                Route::get('import',['as' => 'employee.import', 'uses' => 'CaregiverController@import']);
                Route::post('bulkImport',['as' => 'employee.bulk-import', 'uses' => 'CaregiverController@bulkImport']);
                Route::post('updateStatus',['as' => 'employee.update-status', 'uses' => 'CaregiverController@updateStatus']);
                Route::post('updateAvailability',['as' => 'employee.update-availability', 'uses' => 'CaregiverController@updateAvailability']);
                Route::post('checkemail',['as' => 'employee.checkemail', 'uses' => 'CaregiverController@checkemail']);
                Route::post('checkEmpID',['as' => 'employee.checkEmpID', 'uses' => 'CaregiverController@checkEmpID']);
            });
            Route::resource('employee','CaregiverController',['middleware' => ['ability:admin,employees-list|employees-create|employees-edit|employees-view|employees-delete']]);

            /* Employee Leaves Routes */
            Route::group(['prefix' => 'leaves', 'middleware' => ['ability:admin,permission:leaves-create|leaves-edit|leaves-view|leaves-delete']], function(){
                Route::get('/',['as' => 'leaves.index', 'uses' => 'HRController@leaves']);

                Route::any('/PendingLeavePaginator',['as' => 'leaves.pending-leaves-paginator', 'uses' => 'HRController@getPendingLeavePaginator']);
                Route::any('/ApprovedLeavePaginator',['as' => 'leaves.approved-leaves-paginator', 'uses' => 'HRController@getApprovedLeavePaginator']);
                Route::any('/DeclinedLeavePaginator',['as' => 'leaves.declined-leaves-paginator', 'uses' => 'HRController@getDeclinedLeavePaginator']);

                Route::post('submitRequest',['as' => 'leaves.submit-request', 'uses' => 'HRController@submitLeaveRequest']);
                Route::post('processRequest',['as' => 'leaves.process-request', 'uses' => 'HRController@processLeaveRequest']);
                Route::post('getBlockedDates',['as' => 'leaves.get-blocked-dates', 'uses' => 'HRController@getBlockedDates']);
                Route::get('revoke',['as' => 'leaves.leavesrevoke', 'uses' => 'HRController@leavesrevoke']);
                Route::post('edit',['as' => 'leaves.leavesedit', 'uses' => 'HRController@leavesedit']);
            });

            /* Customer Feedback Routes */
            Route::group(['prefix' => 'customerfeedback'], function(){
                Route::any('/',['as' => 'customerfeedback.index', 'uses' => 'HRController@customerFeedback']);
                Route::post('feedbackaction',['as' => 'customerfeedback.actionsave', 'uses' => 'HRController@customerFeedbackActionSave']);
            });
            Route::any('dailyattendanceShifts',['as' => 'attendanceShifts.index', 'uses' => 'HRController@dailyAttendanceShifts']);
            Route::post('markattendance',['as' => 'attendance.mark-attendance', 'uses' => 'HRController@markAttendance']);
            Route::any('appraisalsView',['as' => 'appraisals.index', 'uses' => 'HRController@appraisalsView']);
            Route::post('appraisalsMarking',['as' => 'appraisals.mark', 'uses' => 'HRController@appraisalsMarking']);
        });

        Route::get('import',['as' => 'caregiver.import', 'uses' => 'CaregiverController@import']);
        Route::post('bulkImport',['as' => 'caregiver.bulk-import', 'uses' => 'CaregiverController@bulkImport']);
        Route::post('updateStatus',['as' => 'caregiver.update-status', 'uses' => 'CaregiverController@updateStatus']);
        Route::post('updateAvailability',['as' => 'caregiver.update-availability', 'uses' => 'CaregiverController@updateAvailability']);
        Route::resource('caregiver','CaregiverController',['middleware' => ['ability:admin,employees-list|employees-create|employees-edit|employees-view|employees-delete']]);
        Route::post('checkemail',['as' => 'caregiver.checkemail', 'uses' => 'CaregiverController@checkemail']);
        /*
        |--------------------------------------------------------------------------
        | Caregiver Scheduling Routes
        |--------------------------------------------------------------------------
        */
        Route::any('schedulingCaregiver',['as' => 'scheduling.caregiver.index', 'uses' => 'CaregiverController@caregiverScheduling']);
        Route::any('schedulingCaregiverfetch',['as' => 'scheduling.caregiver.fetch', 'uses' => 'CaregiverController@schedulingCaregiverfetch']);

        Route::any('schedulingPatient',['as' => 'scheduling.patient.index', 'uses' => 'CaregiverController@patientScheduling']);
        Route::any('schedulingPatientfetch',['as' => 'scheduling.patient.fetch', 'uses' => 'CaregiverController@schedulingPatientfetch']);

        /*
        |--------------------------------------------------------------------------
        | Professional Routes
        |--------------------------------------------------------------------------
        */
        Route::get('profilemanagement',['as' => 'profilemanagement', 'uses' => 'CaregiverController@editprofile']);
        Route::post('update-personal-details',['as' => 'update-personal-details', 'uses' => 'CaregiverController@update_personal_details']);
        Route::post('update-professional-details',['as' => 'update-professional-details', 'uses' => 'CaregiverController@update_professional_details']);
        Route::post('update-timings',['as' => 'update-timings', 'uses' => 'CaregiverController@update_timings']);
        Route::any('update-documents',['as' => 'update-documents', 'uses' => 'CaregiverController@update_documents']);
        Route::post('document-remove',['as' => 'ajax.document-remove', 'uses' => 'CaregiverController@documentRemove']);

        /*
        |--------------------------------------------------------------------------
        | Patient Routes
        |--------------------------------------------------------------------------
        */
        Route::post('savePatient',['as' => 'patient.save-patient', 'uses' => 'PatientController@savePatient']);
        Route::resource('patient','PatientController');
        Route::post('lookUpPatientNumber',['as' => 'patient.checknumber', 'uses' => 'PatientController@lookUpPatientNumber']);
        Route::post('patient/uploadPicture',['as' => 'patient.upload-picture', 'uses' => 'PatientController@uploadPicture']);
        Route::get('addemailaddress/{id?}',['as' => 'patient.addemailaddress', 'uses' => 'PatientController@addEmailAddress']);

        Route::post('extraAddressStore',['as' => 'patient.extraAddressStore', 'uses' => 'PatientController@extraAddressStore']);
        Route::post('extraAddressUpdate',['as' => 'patient.extraAddressUpdate', 'uses' => 'PatientController@extraAddressUpdate']);
        Route::post('extraAddressUpdateStatus',['as' => 'patient.update-status-extra-addr', 'uses' => 'PatientController@extraAddressUpdateStatus']);
        
        Route::post('extraEmailStore',['as' => 'patient.extraEmailStore', 'uses' => 'PatientController@extraEmailStore']);
        Route::post('extraEmailUpdate',['as' => 'patient.extraEmailUpdate', 'uses' => 'PatientController@extraEmailUpdate']);
        Route::post('extraEmailUpdateStatus',['as' => 'patient.update-status-extra-email', 'uses' => 'PatientController@extraEmailUpdateStatus']);


        /*
        |--------------------------------------------------------------------------
        | Leads Routes
        |--------------------------------------------------------------------------
        */
        Route::group(['prefix' => 'leads','middleware' => ['ability:admin,leads-index|leads-create|leads-edit|leads-view|leads-delete']], function(){
            Route::post('searchPatient',['as' => 'lead.search-patient', 'uses' => 'LeadsController@searchPatient']);

            Route::any('leadsUpcomingServicesExpiry',['as' => 'leads.get-upcoming-service-expiry-ajax', 'uses' => 'BillingController@getExpiringServicesAjax']);

            Route::post('markLeadAsDuplicate',['as' => 'lead.markDuplicate', 'uses' => 'AJAXController@markLeadAsDuplicate']);
            Route::post('markLeadAsOtherRequest',['as' => 'lead.otherRequest', 'uses' => 'CasesController@markLeadAsOtherRequest']);

            Route::get('create',['as' => 'lead.create', 'middleware' => ['ability:admin,leads-create'], 'uses' => 'LeadsController@create']);
            Route::post('save',['as' => 'lead.save-lead', 'middleware' => ['ability:admin,leads-edit'], 'uses' => 'LeadsController@saveLead']);
            Route::get('edit/{id}',['as' => 'lead.edit', 'middleware' => ['ability:admin,leads-edit'], 'uses' => 'LeadsController@edit']);
            Route::get('view/{id}',['as' => 'lead.view', 'middleware' => ['ability:admin,leads-view|cases-view'], 'uses' => 'LeadsController@view']);

            Route::any('schedulesData',['as' => 'lead.get-schedules-ajax', 'uses' => 'LeadsController@getSchedulesData']);

            Route::post('getDistanceFromMapMyIndia',['as' => 'lead.get-distance-from-map-my-india', 'uses' => 'LeadsController@getDistanceFromMapMyIndia']);
            Route::get('manually-update-road_distance-from-MapMyIndia/{date}',['as' => 'lead.get-distance-from-map-my-india-temporary', 'uses' => 'LeadsController@manuallyUpdateRoadDistanceFromMapMyIndia']);

            Route::post('saveCaseServices',['as' => 'lead.save-case-services', 'uses' => 'LeadsController@saveCaseServices']);
            Route::post('addDiscount',['as' => 'lead.add-discount', 'uses' => 'LeadsController@addDiscount']);

            Route::post('createSchedulesFromService',['as' => 'lead.create-schedule-from-service', 'uses' => 'LeadsController@createSchedulesFromService']);
            Route::post('assignStaffFromSchedule',['as' => 'lead.assign-staff-from-schedules', 'uses' => 'LeadsController@assignStaffFromSchedule']);
            Route::post('getServiceOrderForDiscount',['as' => 'lead.get-service-order-for-discount', 'uses' => 'LeadsController@getServiceOrderForDiscount']);

            Route::post('updateServiceStatus',['as' => 'lead.update-service-status', 'uses' => 'LeadsController@updateServiceStatus']);
            Route::post('updateAssessmentStatus',['as' => 'lead.update-assessment-status', 'uses' => 'LeadsController@updateAssessmentStatus']);
            Route::post('updateAssessmentDoneStatus',['as' => 'lead.assessent-done-status', 'uses' => 'LeadsController@updateAssessmentDoneStatus']);

            Route::post('schedulesByServiceRequest',['as' => 'lead.get-schedules-from-service-request', 'uses' => 'LeadsController@schedulesByServiceRequest']);
            Route::post('completeSchedule',['as' => 'lead.complete-schedule', 'uses' => 'LeadsController@completeSchedule']);
            Route::post('cancelSchedules',['as' => 'lead.cancel-schedules', 'uses' => 'LeadsController@cancelSchedules']);
            Route::post('unconfirmSchedules',['as' => 'lead.unconfirm-schedules', 'uses' => 'LeadsController@unconfirmSchedules']);
            Route::post('updateScheduleChargeable',['as' => 'lead.update-schedule-chargeable', 'uses' => 'LeadsController@updateScheduleChargeable']);
            Route::post('updateScheduleTasks',['as' => 'lead.update-schedule-tasks', 'uses' => 'LeadsController@updateScheduleTasks']);

            Route::post('saveServiceRequest',['as' => 'lead.save-service-request', 'uses' => 'LeadsController@saveServiceRequest']);
            Route::post('saveServiceOrder',['as' => 'lead.save-service-order', 'uses' => 'LeadsController@saveServiceOrder']);

            Route::post('saveComment',['as' => 'lead.save-comment', 'uses' => 'LeadsController@saveComment']);
            Route::post('updateDisposition',['as' => 'lead.update-disposition', 'uses' => 'LeadsController@updateDisposition']);

            Route::post('saveCaseRateCard',['as' => 'lead.save-case-ratecard', 'uses' => 'LeadsController@saveCaseRateCard']);
            Route::delete('deleteCaseRateCard/{id}',['as' => 'lead.delete-case-ratecard', 'uses' => 'LeadsController@deleteCaseRateCard']);

            Route::post('getSchedule',['as' => 'lead.get-schedule', 'uses' => 'LeadsController@getSchedule']);
            Route::post('saveSchedule',['as' => 'lead.save-schedule', 'uses' => 'LeadsController@saveSchedule']);

            Route::post('getServiceInterruption',['as' => 'lead.get-service-interruption', 'uses' => 'LeadsController@getServiceInterruption']);
            Route::post('saveServiceInterruption',['as' => 'lead.save-service-interruption', 'uses' => 'LeadsController@saveServiceInterruption']);

            Route::post('uploadCaseDocument',['as' => 'lead.upload-case-document', 'uses' => 'LeadsController@uploadCaseDocument']);
            Route::any('removeCaseDocument',['as' => 'lead.remove-case-document', 'uses' => 'LeadsController@removeCaseDocument']);

            Route::post('saveOtherRequest',['as' => 'lead.save-other-request', 'uses' => 'LeadsController@saveOtherRequests']);

            Route::get('assessment/detailed/add/{id?}', ['as' => 'lead.add-detailed-assessment', 'uses' => 'LeadsController@addDetailedAssessment']);
            Route::post('assessment/detailed/save', ['as' => 'lead.save-detailed-assessment', 'uses' => 'LeadsController@saveDetailedAssessment']);
            Route::get('assessment/{id?}',['as' => 'lead.detailed-assessment', 'uses' => 'LeadsController@viewDetailedAssessment']);
            
            Route::get('assessment/detailed/edit/{id?}',['as' => 'lead.view-detailed-assessment-edit', 'uses' => 'LeadsController@viewDetailedAssessmentEdit']);
            Route::post('assessment/detailed/update', ['as' => 'lead.update-detailed-assessment', 'uses' => 'LeadsController@updateDetailedAssessment']);
            Route::get('assessment/delete/{id?}',['as' => 'lead.delete-assessment', 'uses' => 'LeadsController@deleteAssessment']);
            Route::get('followup/delete/{id?}',['as' => 'lead.delete-followup', 'uses' => 'LeadsController@deleteCaseFollowUp']);

            Route::any('/',['as' => 'lead.index', 'middleware' => ['ability:admin,leads-index'], 'uses' => 'LeadsController@index']);

            Route::any('operations',['as' => 'lead.operations', 'middleware' => ['ability:admin,leads-index'], 'uses' => 'LeadsController@operations']);

            Route::any('leadSearch',['as' => 'lead.leads-search', 'middleware' => ['ability:admin,leads-index'], 'uses' => 'LeadsController@getLeadSearch']);
            Route::any('operationSearch',['as' => 'lead.operations-search', 'middleware' => ['ability:admin,leads-index'], 'uses' => 'LeadsController@getOperationSearch']);

            Route::post('aggregatorLeadStatus',['as' => 'lead.aggregator-lead-status', 'uses' => 'CasesController@aggregatorLeadStatus']);
            Route::get('aggregatorLeadRetract',['as' => 'lead.aggregator-retractLead', 'uses' => 'CasesController@retractLeadFromProvider']);

            Route::get('importLead',['as' => 'lead.import', 'uses' => 'LeadsController@importLead']);
            Route::any('bulkImportLeads',['as' => 'leads.bulk-import', 'uses' => 'LeadsController@bulkImportLeads']);
            Route::post('saveForm',['as' => 'lead.save-form', 'uses' => 'LeadsController@saveForm']);
            Route::post('saveFollowUpForm',['as' => 'lead.save-follow-up-form', 'uses' => 'LeadsController@saveFollowUpForm']);
            Route::post('savechecklist',['as' => 'lead.storechecklist', 'uses' => 'LeadsController@storeCheckList']);

            Route::post('addBillables',['as' => 'lead.add-billables', 'uses' => 'LeadsController@addBillables']);
            Route::post('removeBillableItem',['as' => 'lead.remove-billable-item', 'uses' => 'LeadsController@removeBillableItem']);
            Route::post('sendBillablesToVendor',['as' => 'lead.send-billables-to-vendor', 'uses' => 'LeadsController@sendBillablesToVendor']);
            Route::post('addConsumable',['as' => 'lead.add-caseconsumables', 'uses' => 'LeadsController@addConsumable']);
            Route::post('addSurgicals',['as' => 'lead.add-casesurgicals', 'uses' => 'LeadsController@addSurgicals']);
            Route::post('saveWorklogByDate',['as' => 'lead.save-worklog-by-date', 'uses' => 'LeadsController@saveWorklogByDate']);
            Route::post('checkemailLeads',['as' => 'leads.checkemail', 'uses' => 'LeadsController@checkemail']);
            Route::get('prevservicerecords/{id?}',['as' => 'lead.prevservicerecords', 'uses' => 'LeadsController@prevServiceRecords']);
            Route::get('auditlog/{id?}',['as' => 'lead.auditlog', 'middleware' => ['ability:admin,check-audit-log'], 'uses' => 'LeadsController@auditLog']);
            Route::get('patientIncidents',['as' => 'lead.view-patient-incidents', 'uses' => 'LeadsController@allIncidents']);
            Route::post('addIncident',['as' => 'lead.add-incident', 'uses' => 'LeadsController@addIncident']);
        });

        /*
        |--------------------------------------------------------------------------
        | Billing Routes
        |--------------------------------------------------------------------------
        */
        Route::group(['prefix' => 'billing', 'middleware' => ['ability:admin,permission:billing-invoice-list|billing-invoice-create|billing-invoice-view|billing-payments-list|billing-payments-view|billing-payments-update']], function(){
            Route::get('/',['as' => 'billing.index', 'uses' => 'BillingController@index']);
            Route::any('/proformaPaginator',['as' => 'billing.proforma-paginator', 'uses' => 'BillingController@getProformaPaginator']);
            Route::any('/servicePaginator',['as' => 'billing.service-paginator', 'uses' => 'BillingController@getServicePaginator']);
            Route::any('/receiptPaginator',['as' => 'billing.receipt-paginator', 'uses' => 'BillingController@getReceiptPaginator']);
            Route::get('invoice/view/{id}/{type?}',['as' => 'billing.view-invoice', 'uses' => 'BillingController@viewInvoice']);
            Route::get('invoice/print/{id}/{type?}',['as' => 'billing.print-invoice', 'uses' => 'BillingController@printInvoice']);
            Route::get('invoice/email/{id}/{type?}',['as' => 'billing.email-invoice', 'uses' => 'BillingController@emailInvoice']);
            Route::get('invoice/delete/{id}/{type?}',['as' => 'billing.delete-invoice', 'uses' => 'BillingController@deleteInvoice']);
            Route::get('receipt/view/{id}',['as' => 'billing.view-receipt', 'uses' => 'BillingController@viewReceipt']);

            Route::any('addCreditMemo',['as' => 'billing.add-credit-memo', 'uses' => 'BillingController@addCreditMemo']);
            Route::post('updateInvoicePayment',['as' => 'billing.update-invoice-payment', 'uses' => 'BillingController@updateInvoicePayment']);
            Route::post('settleOffInvoice',['as' => 'billing.settle-off-invoice', 'uses' => 'BillingController@settleOffInvoice']);
            Route::post('invoicePaymentHistory',['as' => 'billing.invoice-payment-history', 'uses' => 'BillingController@invoicePaymentHistory']);
            Route::post('getCreditMemoLog',['as' => 'billing.get-credit-memo-log-ajax','uses' => 'BillingController@getCreditMemoLog']);
            Route::post('getCreditMemoRefundLog',['as' => 'billing.get-credit-memo-refund-log-ajax','uses' => 'BillingController@getCreditMemoRefundLog']);
            Route::post('raiseCreditMemoReceipt',['as' => 'billing.raise-credit-memo-receipt','uses' => 'BillingController@raiseCreditMemoReceipt']);

            /* Old */
            Route::get('create-proforma-invoice/{pid?}',['as' => 'billing.create-proforma-invoice', 'uses' => 'BillingController@createProforma']);
            Route::get('create-service-invoice/{pid?}',['as' => 'billing.create-service-invoice', 'uses' => 'BillingController@createService']);

            Route::post('save-proforma-invoice', ['as' => 'billing.save-proforma-invoice', 'uses' => 'BillingController@saveProformaInvoice']);
            Route::post('save-service-invoice', ['as' => 'billing.save-service-invoice', 'uses' => 'BillingController@saveServiceInvoice']);

            Route::any('outstandingAmountAjax',['as' => 'billing.get-outstanding-amount-ajax', 'uses' => 'BillingController@getOutstandingAmountAjax']);
            Route::any('upcomingServicesExpiry',['as' => 'billing.get-upcoming-service-expiry-ajax', 'uses' => 'BillingController@getExpiringServicesAjax']);
            Route::any('unbilledServiceOrders',['as' => 'billing.get-unbilled-services-ajax', 'uses' => 'BillingController@getUnbilledServicesAjax']);
            Route::any('paymentsData',['as' => 'billing.get-payments-ajax', 'uses' => 'BillingController@getPaymentsDataAjax']);

            Route::post('patientSchedules',['as' => 'billing.patient-schedules', 'uses' => 'BillingController@patientSchedules']);
            //Route::post('createInvoice',['as' => 'billing.create-invoice', 'uses' => 'BillingController@createInvoice']);
            Route::post('cancelInvoice',['as' => 'billing.cancel-invoice', 'uses' => 'BillingController@cancelInvoice']);
            // Route::get('invoice/view/{id}/{type?}',['as' => 'billing.view-invoice', 'uses' => 'BillingController@viewInvoice']);
            Route::any('receipt/view/{id}',['as' => 'billing.view-receipt', 'uses' => 'BillingController@viewReceipt']);

            Route::post('getInvoiceItems',['as' => 'billing.get-invoice-items', 'uses' => 'BillingController@getInvoiceItems']);

            Route::any('getProformaForServiceOrderDates',['as' => 'ajax.getProformaForServiceOrderDates', 'uses' => 'AJAXController@getProformaForServiceOrderDates']);

            Route::any('statement-of-account',['as' => 'billing.statement-of-account', 'uses' => 'BillingController@statementOfAccount']);

            Route::any('soaSchedulesData',['as' => 'billing.get-soa-schedules-ajax', 'uses' => 'BillingController@getSOASchedulesData']);
            Route::any('soaPaymentsData',['as' => 'billing.get-soa-payments-ajax', 'uses' => 'BillingController@getSOAPaymentsData']);

            Route::any('credit-memo',['as' => 'billing.credit-memo', 'uses' => 'BillingController@creditMemo']);

            Route::any('record-payment',['as' => 'billing.record-payment', 'uses' => 'BillingController@recordPayment']);

            Route::post('newCaseInvoice',['as' => 'billing.new-invoice', 'uses' => 'BillingController@newInvoice']);
            Route::any('getInvoice',['as' => 'billing.get-invoice', 'uses' => 'BillingController@getInvoice']);
            Route::any('getInvoicePDF',['as' => 'billing.invoicepdf', 'uses' => 'BillingController@getInvoice']);

            Route::post('referral-commission-payout',['as' => 'billing.referral-commission-payout', 'uses' => 'BillingController@referralCommissionPayout']);

            Route::any('getBillableInvoicePDF',['as' => 'billable.invoicepdf', 'uses' => 'BillingController@getBillableInvoice']);

        });

        Route::any('getBillableInvoicePDF',['as' => 'billable.invoicepdf', 'uses' => 'BillingController@getBillableInvoice']);

        /*
        |--------------------------------------------------------------------------
        | Tracking  Routes
        |--------------------------------------------------------------------------
        */
        Route::group(['prefix' => 'tracking', 'middleware' => ['ability:admin,permission:tracking-view']], function(){
            Route::get('/',['as' => 'tracking.index', 'uses' => 'CaregiverController@tracking']);
            Route::get('tracking/live-tracking', ['as' => 'tracking.live-tracking', 'uses' => 'CaregiverController@liveTracking']);
            Route::get('tracking/getEmployeeLocationCoordinates', ['as' => 'tracking.employee-location-coordinates', 'uses' => 'CaregiverController@getEmployeeLocationCoordinates']);
        });

        /*
        |--------------------------------------------------------------------------
        | Report Routes
        |--------------------------------------------------------------------------
        */
        Route::group(['prefix' => 'report','middleware' => ['ability:admin,report-financial|report-hr|report-sales|report-clinical']], function(){
            Route::get('/',['as' => 'reports.index', 'uses' => 'ReportsController@index']);
            Route::any('resourceStats',['as' => 'reports.resource-stats', 'uses' => 'ReportsController@resourceStats']);

            Route::group(['prefix' => 'financial'], function(){
                Route::any('outstanding', ['as' => 'financial.outstanding', 'uses' => 'ReportsController@outstanding']);
                Route::any('revenue-schedules', ['as' => 'financial.revenue-schedules', 'uses' => 'ReportsController@revenueWithSchedules']);
                Route::any('manual-creditmemo', ['as' => 'financial.manual-creditmemo', 'uses' => 'ReportsController@manualCreditMemos']);
                Route::any('statement-of-account', ['as' => 'financial.statement-of-account', 'uses' => 'ReportsController@statementOfAccount']);
                Route::any('soa-email', ['as' => 'financial.soa-email', 'uses' => 'ReportsController@soaEmail']);
                Route::any('invoice-report', ['as' => 'financial.invoice-report', 'uses' => 'ReportsController@invoiceReport']);
                Route::any('invoice-report-with-service', ['as' => 'financial.invoice-report-with-service', 'uses' => 'ReportsController@invoiceReportWithService']);
                Route::any('receipt-report', ['as' => 'financial.receipt-report', 'uses' => 'ReportsController@receiptReport']);
            });

            Route::group(['prefix' => 'hr'], function(){
                Route::any('master-data', ['as' => 'hr.master-data', 'uses' => 'ReportsController@hrMasterDataReport']);
                Route::any('attendance-report',['as' => 'hr.attendance-report', 'uses' => 'ReportsController@attendanceReport']);
                Route::any('leaves-stats-report',['as' => 'hr.leaves-stats-report', 'uses' => 'ReportsController@leavesStatsReport']);
                Route::any('attendance-summary', ['as' => 'hr.attendance-summary', 'uses' => 'ReportsController@attendanceSummary']);
                Route::any('employee-worktime1', ['as' => 'hr.employee-worktime1', 'uses' => 'ReportsController@employeeWorkTimeReport1']);
                Route::any('employee-worktime2', ['as' => 'hr.employee-worktime2', 'uses' => 'ReportsController@employeeWorkTimeReport2']);
                Route::any('employee-worktime-old', ['as' => 'hr.employee-worktime-old', 'uses' => 'ReportsController@employeeWorkTimeReportOld']);
                Route::any('monthly-attendance-report',['as' => 'hr.monthly-attendance-report', 'uses' => 'ReportsController@monthlyAttendanceReport']);
                Route::any('leavesStats',['as' => 'hr.leaves.reports', 'uses' => 'ReportsController@leavesReport']);
                Route::any('oldLeavesStats',['as' => 'hr.old-leaves.reports', 'uses' => 'ReportsController@oldLeavesReport']);
                Route::get('warningStarUniformDocumentReport',['as' => 'hr.warning-star-uniform-document', 'uses' => 'ReportsController@warningStarUniformDocumentReport']);
                Route::any('customerfeedback',['as' => 'hr.customer-feedback', 'uses' => 'ReportsController@customerFeedback']);
            });

            Route::group(['prefix' => 'sales'], function(){
                Route::any('master-data', ['as' => 'sales.master-data', 'uses' => 'ReportsController@leadsMasterDataReport']);
                Route::any('leads-converted', ['as' => 'sales.leads-converted', 'uses' => 'ReportsController@convertedLeadsReport']);
                Route::any('referral-commission',['as' => 'sales.referral-commission', 'uses' => 'ReportsController@referralCommissionReport']);
                Route::any('daily-stats', ['as' => 'sales.daily-stats', 'uses' => 'ReportsController@dailyStats']);
                Route::any('conveyance',['as' => 'sales.conveyance', 'uses' => 'ReportsController@conveyance']);
            });

            Route::group(['prefix' => 'clinical'], function(){
                Route::any('service-summary', ['as' => 'clinical.service-summary', 'uses' => 'ReportsController@serviceSummary']);
                Route::any('clientnoservicedays', ['as' => 'clinical.clientnoservicedays', 'uses' => 'ReportsController@clientNoServiceDays']);
                Route::any('schedule-data', ['as' => 'clinical.schedule-data', 'uses' => 'ReportsController@scheduleData']);
                Route::any('renewable-service-orders', ['as' => 'clinical.renewable-service-orders', 'uses' => 'ReportsController@renewableServiceOrders']);
                Route::any('unconfirmed-schedules', ['as' => 'clinical.unconfirmed-schedules', 'uses' => 'ReportsController@uncnfirmedSchedules']);
                Route::any('billables', ['as' => 'clinical.billables', 'uses' => 'ReportsController@billables']);
                Route::any('bench-forecast-report',['as' => 'clinical.bench-forecast-report', 'uses' => 'ReportsController@benchForecast']);
            });

            Route::any('mis-report',['as' => 'report.mis-report', 'uses' => 'ReportsController@misReport']);
            Route::any('loginevent-report',['as' => 'report.loginevent-report', 'uses' => 'ReportsController@logineventReport']);
            Route::any('upload-chart-img',['as' => 'report.upload-chart-img', 'uses' => 'ReportsController@uploadChartImg']);

            Route::group(['prefix' => 'activity'], function(){
                Route::any('login-summary',['as' => 'activity.login-summary', 'uses' => 'ReportsController@loginSummary']);
            });

            Route::get('cases-consolidated',['as' => 'reports.cases-consolidated', 'uses' => 'ReportsController@casesConsolidatedReport']);
            Route::any('monthlyReport',['as' => 'case.reports', 'uses' => 'ReportsController@casesConsolidatedReport']);
        });

        /*
        |--------------------------------------------------------------------------
        | Case Record Routes
        |--------------------------------------------------------------------------
        */
        Route::group(['prefix' => 'cases','middleware' => ['ability:admin,leads-index|cases-create|cases-edit|cases-view|cases-delete']], function(){
            Route::get('create',['as' => 'case.create','middleware' => ['ability:admin,cases-create'], 'uses' => 'CasesController@create']);
            Route::get('edit',['as' => 'case.edit','middleware' => ['ability:admin,cases-edit'], 'uses' => 'CasesController@edit']);
            Route::get('createFromLead',['as' => 'case.create-from-lead', 'uses' => 'CasesController@create']);
            Route::post('save',['as' => 'case.save-case','middleware' => ['ability:admin,cases-edit'], 'uses' => 'CasesController@saveCase']);
            Route::post('saveAsAggregator',['as' => 'case.save-case-aggregator', 'uses' => 'CasesController@saveCaseAsAggregator']);
            Route::post('saveCaseDetails',['as' => 'case.update-case-details', 'uses' => 'CasesController@saveCaseDetails']);
            Route::post('updateDisposition',['as' => 'case.update-disposition', 'uses' => 'CasesController@updateDisposition']);
            Route::post('saveSchedule',['as' => 'case.save-schedule', 'uses' => 'CasesController@saveVisitSchedule']);

            Route::post('saveComment',['as' => 'case.save-comment', 'uses' => 'CasesController@saveComment']);
            Route::post('saveFeedbackComment',['as' => 'case.save-feedback-comment', 'uses' => 'CasesController@saveFeedbackComment']);
            //Route::any('monthlyReport',['as' => 'case.reports', 'uses' => 'CasesController@monthlyReport']);
            Route::get('view',['as' => 'case.view', 'uses' => 'CasesController@view']);
            Route::post('checkForExistingLead',['as' => 'case.check-for-existing-case', 'uses' => 'CasesController@checkForExistingCase']);
            // To be removed - Down
            Route::post('saveAssessment',['as' => 'case.save-assessment', 'uses' => 'CasesController@saveAssessment']);
            // To be removed - Up
            Route::post('saveForm',['as' => 'case.save-form', 'uses' => 'CasesController@saveForm']);
            Route::any('casedata', ['as' => 'case.ajax-data','uses' => 'CasesController@ajaxData']);
            Route::get('/',['as' => 'case.index', 'uses' => 'CasesController@index']);
        });

        /*
        |--------------------------------------------------------------------------
        | Plugin Routes
        |--------------------------------------------------------------------------
        */
        Route::group(['prefix' => 'plugin'],function(){
            Route::any('setDefault',['as' => 'plugin.set-default','uses' => 'PluginController@setDefault']);
            Route::any('knowlarity/call-log',['as' => 'knowlarity.call-log','uses' => 'PluginController@knowlarityGetCallLog']);
            Route::any('paymentgateway/ccavenue',['as' => 'paymentgateway.ccavenue','uses' => 'PluginController@saveCcavenueCredentials']);
            Route::any('paymentgateway/atom',['as' => 'paymentgateway.atom','uses' => 'PluginController@saveAtomCredentials']);
        });

        Route::resource('plugin','PluginController');


        /*
        |--------------------------------------------------------------------------
        | Administration Routes
        |--------------------------------------------------------------------------
        */

        Route::group(['prefix' => 'administration','middleware' => ['ability:admin,admin-view']], function(){
            Route::get('/', ['as' => 'settings','uses' => 'HomeController@administration']);

            Route::get('profile',['as' => 'profile.index', 'uses' => 'Administration\ProfileController@index']);
            Route::post('saveProfile',['as' => 'profile.updateCompanyDetails', 'uses' => 'Administration\ProfileController@updateCompanyDetails']);
            Route::post('saveSettings',['as' => 'profile.updateSettings', 'uses' => 'Administration\ProfileController@updateSettings']);
            Route::post('saveBankDetails',['as' => 'profile.updateBankingDetails', 'uses' => 'Administration\ProfileController@updateBankingDetails']);

            Route::group(['prefix' => 'acl'], function(){
                Route::get('/',['as' => 'acl.index','uses' => 'Administration\AccessControlListController@index']);
                Route::get('create',['as' => 'acl.create','uses' => 'Administration\AccessControlListController@create']);
                Route::post('save',['as' => 'acl.save','uses' => 'Administration\AccessControlListController@store']);
                Route::any('edit',['as' => 'acl.edit','uses' => 'Administration\AccessControlListController@edit']);
                Route::post('update',['as' => 'acl.update','uses' => 'Administration\AccessControlListController@update']);
                Route::post('delete',['as' => 'acl.delete','uses' => 'Administration\AccessControlListController@destroy']);
            });

            Route::get('user/reloadPrivileges',['as' => 'user.reload-privileges', 'uses' => 'Administration\UserController@reloadRolePrivileges']);
            Route::post('user/updateStatus',['as' => 'user.update-status', 'uses' => 'Administration\UserController@updateStatus']);

            Route::group(['prefix' => 'user'], function(){
                Route::get('/',['as' => 'user.index', 'uses' => 'Administration\UserController@index']);
                Route::post('save',['as' => 'user.store', 'uses' => 'Administration\UserController@store']);
                Route::post('update',['as' => 'user.update', 'uses' => 'Administration\UserController@update']);
                Route::any('destroy',['as' => 'user.destroy', 'uses' => 'Administration\UserController@destroy']);
            });

            Route::post('branch/updateStatus',['as' => 'branch.update-status', 'uses' => 'Administration\BranchController@updateStatus']);
            Route::resource('branch','Administration\BranchController');
            Route::resource('language','Administration\LanguageController');
            Route::resource('service','Administration\ServiceController');
            Route::resource('servicecategory','Administration\ServiceCategoryController');

            Route::post('ratecard/updateStatus',['as' => 'ratecard.update-status', 'uses' => 'Administration\RateCardController@updateStatus']);
            Route::resource('ratecard','Administration\RateCardController');

            Route::group(['prefix' => 'referral'], function(){
                Route::get('/',['as' => 'referral.index', 'uses' => 'Administration\ReferralController@index']);
                Route::post('saveCategory',['as' => 'referral.save-category', 'uses' => 'Administration\ReferralController@saveCategory']);
                Route::post('saveSource',['as' => 'referral.save-source', 'uses' => 'Administration\ReferralController@saveSource']);

                Route::post('deleteCategory',['as' => 'referral.destroy-category', 'uses' => 'Administration\ReferralController@deleteCategory']);
                Route::post('deleteSource',['as' => 'referral.destroy-source', 'uses' => 'Administration\ReferralController@deleteSource']);
            });

            Route::resource('feedback','Administration\FeedbackController');

            Route::any('compositionIndex',['as' => 'composition.index', 'uses' => 'Administration\CompositionController@index']);
            Route::any('compositionUpdate',['as' => 'composition.update', 'uses' => 'Administration\CompositionController@update']);

            Route::resource('mailers','Administration\EmailSubscriberController');
            Route::post('mailers/update',['as' => 'mailers.update', 'uses' => 'Administration\EmailSubscriberController@update']);
            Route::post('mailers/updateStatus',['as' => 'mailers.updateStatus', 'uses' => 'Administration\EmailSubscriberController@updateStatus']);
            Route::post('mailers/destroy',['as' => 'mailers.destroy', 'uses' => 'Administration\EmailSubscriberController@destroy']);

            Route::resource('tasks','Administration\TasksController');

            Route::resource('designation','Administration\DesignationController');
            Route::resource('department','Administration\DepartmentController');
            Route::resource('skill','Administration\SkillController');
            Route::resource('specialization','Administration\SpecializationController');
            Route::resource('holiday','Administration\HolidayController');
            Route::resource('employeesource','Administration\EmployeeSourceController');

            Route::resource('consumables','Administration\ConsumablesController');
            Route::resource('surgicals','Administration\SurgicalsController');
            Route::resource('labtests','Administration\LabtestsController');
            Route::resource('billingProfile','Administration\BillablesController');
            Route::post('billableProfileStore',['as' => 'billingProfile.store', 'uses' => 'Administration\BillablesController@store']);

            Route::resource('taxrate','Administration\TaxrateController');
            Route::resource('agency','Administration\AgencyController');
            Route::resource('consumables','Administration\ConsumablesController');
            Route::resource('surgicals','Administration\SurgicalsController');
            Route::resource('trainings','Administration\TrainingController');
            Route::post('trainingsStore',['as' => 'trainings.store', 'uses' => 'Administration\TrainingController@store']);
            Route::resource('pharmaceuticals','Administration\PharmaceuticalsController');
            Route::resource('billingProfile','Administration\BillablesController');
            Route::post('billableProfileStore',['as' => 'billingProfile.store', 'uses' => 'Administration\BillablesController@store']);
            Route::resource('holiday','Administration\HolidayController');
            Route::resource('costcentre','Administration\CostcentreController');
        });

        /*
        |--------------------------------------------------------------------------
        | Administration Routes
        |--------------------------------------------------------------------------
        */
        Route::group(['prefix' => 'subscription'], function(){
            Route::get('/',['as' => 'subscription.index', 'uses' => 'SubscriptionController@index']);
            Route::get('paymentHistory',['as' => 'subscription.payment-history', 'uses' => 'SubscriptionController@paymentHistory']);
            Route::get('paymentInvoices',['as' => 'subscription.payment-invoices', 'uses' => 'SubscriptionController@paymentInvoices']);
            Route::get('paymentInvoicesPay',['as' => 'subscription.payment-invoices-pay', 'uses' => 'SubscriptionController@paymentInvoicesPay']);
            Route::get('paymentInvoicesDownload',['as' => 'subscription.payment-invoice-download', 'uses' => 'SubscriptionController@paymentInvoiceDownload']);


            Route::get('/payment',['as' => 'subscription.make-payment', 'uses' => 'SubscriptionController@payment']);
            Route::post('/payment/process',['as' => 'subscription.process-payment', 'uses' => 'SubscriptionController@processPayment']);
            Route::any('/payment/response',['as' => 'subscription.payment-response', 'uses' => 'SubscriptionController@paymentResponse']);
            Route::get('/payment/receipt/{id?}',['as' => 'subscription.payment-receipt', 'uses' => 'SubscriptionController@paymentReceipt']);
        });

        Route::get('/home', 'HomeController@index');

    });

    /*
    |--------------------------------------------------------------------------
    | Exotel and Website Request Routes - Aggregator
    |--------------------------------------------------------------------------
    */
    Route::any('exotel/call-log',['as' => 'exotel.call-log','uses' => 'PluginController@exotelGetCallLog']);
    Route::any('website/lead',['as' => 'website.lead','uses' => 'PluginController@quickCaseFormSubmit']);

    /*
    |--------------------------------------------------------------------------
    | Payment Routes
    |--------------------------------------------------------------------------
    */
    Route::group(['prefix' => 'payment'], function(){
        Route::any('subscription-payment',['as' => 'payment.subscription-payment', 'uses' => 'PaymentsController@subscriptionPayment']);
        Route::post('get-company-info','PaymentsController@getCompanyInfo');
        Route::get('invoice',['as' => 'payment.invoice-payment', 'uses' => 'PaymentsController@invoice']);
        Route::any('transaction',['as' => 'payment.transaction', 'uses' => 'PaymentsController@transaction']);
        Route::any('transaction/response/{tenant_id?}',['as' => 'payment.transaction.response', 'uses' => 'PaymentsController@response']);
    });
