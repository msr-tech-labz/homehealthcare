<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
    Route::group(['prefix' => 'physio'], function(){
        Route::get('/','API\PhysioController@index');

        Route::group(['prefix' => 'v1', 'namespace' => 'API'], function(){
            Route::post('signup','PhysioController@signup');
            Route::post('login','PhysioController@login');
            Route::post('forgot-password','PhysioController@forgotPassword');

            Route::group(['middleware' => 'checkapisession'], function(){
                Route::get('/', 'PhysioController@index');

                Route::get('checkVersion','PhysioController@checkVersion');
                Route::post('subscription/payment','PhysioController@subscriptionPayment');
                Route::post('usePromo','PhysioController@usePromo');
                Route::post('mailInvoice','PhysioController@mailInvoice');
                
                Route::post('fcm/updateRegistrationToken','PhysioController@updateRegistrationToken');

                Route::post('profile/update','PhysioController@updateProfile');
                
                Route::post('doctorreport/add','PhysioController@addDoctorReport');
                Route::get('casedata/{user_type}/{tenant_id}','PhysioController@getCaseData');
                Route::post('assessment/create','PhysioController@createAssessment');
                Route::post('appointment/create','PhysioController@createAppointment');
                Route::post('patient/create','PhysioController@createPatient');
                Route::post('patient/uploadphoto','PhysioController@uploadPatientPhoto');
                Route::post('treatment/create','PhysioController@createTreatment');
                Route::post('painpoint/create','PhysioController@createPainPoints');

                //Billing Routes
                Route::post('bill/create','PhysioController@billCreate');
                Route::post('updateBillStatus','PhysioController@updateBillStatus');
            });
        });
    });
