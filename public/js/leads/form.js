$( '#schedule_details_type' ).change(function () {
    var str = ``;
    $( "#schedule_details_type option:selected" ).each(function() {
        str = $( this ).attr("data-specialization");
        document.getElementById("listed_form_type").value = str;
    });

    switch(str) {
        case "nursing":
        str=$.parseHTML(nurseForm);
        $('.forms').html( str );
        $('.forms #assessment_date_nursing').datetimepicker({
            format: 'DD-MM-YYYY',
            showClear: true,
            showClose: true,
            useCurrent: false,
            keepInvalid:true
        });
        break;
        case "physiotherapy":
        str=$.parseHTML(physioForm);
        $( '.forms' ).html( str );
        addSliderpain('pain_slider_value');
        addSlidermuscle('musclegrade_slider_value');
        $('.forms #assessment_date').datetimepicker({
            format: 'DD-MM-YYYY',
            clearButton: true
        });
        break;
        case "labtest":
        $( ".labhide" ).addClass('invisible');
        str=$.parseHTML(labForm);
        $( '.forms' ).html( str );
        $('.forms #test_id').selectpicker();
        $('.forms #price_type').selectpicker();
        $('.forms #assessment_date').datetimepicker({
            format: 'DD-MM-YYYY - hh:mm A',
            time: true,
            clearButton: true
        });
        break;
        default:
        str="Please Choose a Caregiver";
        $( '.forms' ).html( str );
        break;
    }
})

$('.content').on('click', 'input[name=assessform]', function(){
    if($(this).attr("id")=="ortho"){
        $(".showformtype").not(".formortho").addClass("hide");
        $(".formortho").removeClass("hide");
    }
    if($(this).attr("id")=="neuro"){
        $(".showformtype").not(".formoneuro").addClass("hide");
        $(".formneuro").removeClass("hide");
    }
});

function addSliderpain(element){
    var slider = document.getElementById(element);
    noUiSlider.create(slider, {
        start: [0],
        connect: true,
        step:1,
        range: {
            'min': 0,
            'max': 10
        }
    });
    getNoUISliderValuePain(slider, true);
    formshow();
}

function addSlidermuscle(element){
    var slider = document.getElementById(element);
    noUiSlider.create(slider, {
        start: [0],
        connect: true,
        step:1,
        range: {
            'min': 0,
            'max': 5
        }
    });
    getNoUISliderValueMuscleGrade(slider, true);
    formshow();
}

function formshow(){
    $("input[name=group2]").change(function() {
        var test = $(this).val();
        $(".desc").hide();
        $("#"+test).show();
    });
};

function getNoUISliderValuePain(slider, percentage) {
    slider.noUiSlider.on('update', function () {
        var val = slider.noUiSlider.get();
        if (percentage) {
            val = parseInt(val);
        }
        $(slider).parent().find('span.js-nouislider-value.pain').text(val);
    });
}

function getNoUISliderValueMuscleGrade(slider, percentage) {
    slider.noUiSlider.on('update', function () {
        var val = slider.noUiSlider.get();
        if (percentage) {
            val = parseInt(val);
        }
        $(slider).parent().find('span.js-nouislider-value.mus_gra').text(val);
    });
}

function viewAssessment(element)
{
    var form_data;
    $( ".assessmentId_"+element).each(function() {
        form_data = $( this ).attr("data-form-data");
    });
    var data = JSON.parse(form_data);
    var html = `<tbody>`;

    var blockedVariables = ['careplan_id','schedule_id','assessform'];
    $.each(data, function(key, data) {
        // alert(key + ' is ' + data);
        if(!blockedVariables.contains(key)){
            html += `<tr>
            <th>`+formatKey(key)+`</th>
            <td>`+data+`</td>
            </tr>`;
        }
    });

    html += `</tbody>`;
    $('#viewassessmentModal .modal-body #viewAssessmentTable').html(html);
}

function viewRequirement(element)
{
    var form_data;
    $( ".requirementId_"+element).each(function() {
        form_data = $( this ).attr("data-form-data");
    });
    var data = JSON.parse(form_data);
    var html = `<tbody>`;

    html = `<tr>
    <th style="text-align:left;width:40%;">Assessment Date<br>Patient's Name<br>Physiotheratist's Name</th>
    <th>`+data['0']['Date']+`<br>{{ isset($l)?$l->patient->first_name:'' }}  {{ isset($l)?$l->patient->last_name:'' }}<br>{{ isset($a->user)?$a->user->full_name:'' }}</th>
    </tr>`;
    $.each(data[0], function(key, data) {
        // alert(key + ' is ' + data);
        html += `<tr>
        <th><b>`+formatKey(key)+`</b></th>
        <td><b>`+data+`</b></td>
        </tr>`;
    });
    html += `</tbody>`;
    $('#viewrequirementModal .modal-body #viewRequirementTable').html(html);
}

function viewTreatment(element)
{
    var treatment_data;
    $( ".treatmentId_"+element).each(function() {
        treatment_data = $( this ).attr("data-treatment");
    });

    var data = JSON.parse(treatment_data);
    var html = `<tbody>`;

    html = `<tr>
    <th style="text-align:left;width:40%;">Treatment Date<br>Patient's Name<br>Physiotheratist's Name</th>
    <th>{{ isset($w)?Carbon\Carbon::parse($w->treatmentDate)->format('d-m-Y'):'' }}<br>{{ isset($l)?$l->patient->first_name:'' }}  {{ isset($l)?$l->patient->last_name:'' }}<br>{{ isset($a->user)?$a->user->full_name:'' }}</th>
    </tr>`;
    $.each(data[0], function(key, data) {
        // alert(key + ' is ' + data);
        html += `<tr>
        <th><b>`+formatKey(key)+`</b></th>
        <td><b>`+data+`</b></td>
        </tr>`;
    });
    html += `</tbody>`;
    $('#viewTreatmentModal .modal-body #viewTreatmentTable').html(html);
}