    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
    var elementID = 'pac-input';
    var autoCompleteType = [];
    var autoCompleteArray = [];

    function initAutoComplete(idList){
        if(idList){
            for (var key in idList) {
              if (idList.hasOwnProperty(key)) {
                //console.log(key + " -> " + idList[key]);
                autoCompleteArray.push({ele: key, id: idList[key]});
              }
            }

            initAutoCompleteListener();
        }
    }

    function initAutoCompleteListener() {
        if(autoCompleteArray.length){
            $.each(autoCompleteArray, function(i){
                var input = (document.getElementById(autoCompleteArray[i].id));

                var autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.setTypes(autoCompleteType);

                google.maps.event.addListener(autocomplete, 'place_changed', function() {
                    var place = autocomplete.getPlace();
                    if (!place.geometry) {
                        window.alert("Autocomplete's returned place contains no geometry");
                        return;
                    }

                    var address = '';
                    // console.log(extractFromAdress(place.address_components,"postal_code"));
                    // console.log(extractFromAdress(place.address_components,"route"));
                    // console.log(extractFromAdress(place.address_components,"administrative_area_level_1"));
                    // console.log(extractFromAdress(place.address_components,"locality"));
                    // console.log(extractFromAdress(place.address_components,"country"));
                    if (place.address_components) {
                        $(autoCompleteArray).each(function(i){
                            if(autoCompleteArray[i].ele == 'route'){
                                address = $('#'+autoCompleteArray[i].id).val().split(",");
                                address.pop(); address.pop(); address.pop();
                                document.getElementById(autoCompleteArray[i].id).value = address.join(",");
                            }else if(autoCompleteArray[i].ele == 'geometry'){
                                document.getElementById(autoCompleteArray[i].id).value = place.geometry.location.lat() + "," + place.geometry.location.lng();
                            }else{
                                document.getElementById(autoCompleteArray[i].id).value = extractFromAdress(place.address_components,autoCompleteArray[i].ele);
                            }
                        });
                    }
                });
            });
        }
    }

    function extractFromAdress(components, type){
        for (var i=0; i<components.length; i++)
            for (var j=0; j<components[i].types.length; j++)
                if (components[i].types[j]==type) return components[i].long_name;
        return "";
    }
